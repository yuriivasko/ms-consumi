package it.fai.ms.consumi.repository.contract;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.repository.storico_dml.StoricoContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackageClasses = StoricoContratto.class)
@DataJpaTest
@EntityScan(basePackageClasses = StoricoContratto.class)
@EnableJpaRepositories("it.fai.ms.consumi.repository.storico_dml")
@Tag("unit")
@Transactional
class ContractRepositoryImplTest {

  @Autowired
  private EntityManager em;
  
  @Autowired
  private StoricoContrattoRepository repo;

  private ContractRepository contractRepository;

  @BeforeEach
  void setUp() throws Exception {

    var contract_1 = new StoricoContratto();
    contract_1.setCodiceAzienda("::companyCode::");
    contract_1.setDmlUniqueIdentifier(UUID.randomUUID()
                                                   .toString());
    contract_1.setContrattoNumero("::contractNumber::");
    contract_1.setDataVariazione(Instant.ofEpochMilli(1));
    contract_1.setDmlRevisionTimestamp(Instant.ofEpochMilli(1));
    contract_1.setStatoContratto("::state-1::");

    var contract_2 = new StoricoContratto();
    contract_2.setCodiceAzienda("::companyCode::");
    contract_2.setDmlUniqueIdentifier(UUID.randomUUID()
                                                   .toString());
    contract_2.setContrattoNumero("::contractNumber::");
    contract_2.setDataVariazione(Instant.ofEpochMilli(2));
    contract_2.setDmlRevisionTimestamp(Instant.ofEpochMilli(2));
    contract_2.setStatoContratto("::state-2::");

    var contract_3 = new StoricoContratto();
    contract_3.setCodiceAzienda("::companyCode::");
    contract_3.setDmlUniqueIdentifier(UUID.randomUUID()
                                                   .toString());
    contract_3.setContrattoNumero("::contractNumber::");
    contract_3.setDataVariazione(Instant.ofEpochMilli(3));
    contract_3.setDmlRevisionTimestamp(Instant.ofEpochMilli(3));
    contract_3.setStatoContratto("::state-3::");

    em.persist(contract_1);
    em.persist(contract_2);
    em.persist(contract_3);

    em.flush();
  }

  @Test
  void testFindContractByUuid() {

    contractRepository = new ContractRepositoryImpl(em, new ContractEntityMapper(),repo);
    var optionalContractByUuid = contractRepository.findContractByUuid("::contractNumber::");

    assertThat(optionalContractByUuid).contains(lastContract());
  }

  @Test
  void testFindContractByUuid_notExists() {

    contractRepository = new ContractRepositoryImpl(em, new ContractEntityMapper(),repo);
    var optionalContractByUuid = contractRepository.findContractByUuid("::contractNotPresent::");

    assertThat(optionalContractByUuid).isNotPresent();

  }

  private static Contract lastContract() {
    Contract contract = new Contract("::contractNumber::");
    contract.setCompanyCode("::companyCode::");
    contract.setState("::state-3::");
    return contract;
  }

}
