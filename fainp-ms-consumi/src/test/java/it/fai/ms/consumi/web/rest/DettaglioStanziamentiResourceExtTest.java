package it.fai.ms.consumi.web.rest;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteJmsProducer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("unit")
class DettaglioStanziamentiResourceExtTest {

  private MockMvc mockMvc;
  private DettaglioStanziamantiRepository dettaglioStaziamentoRepository = mock(DettaglioStanziamantiRepository.class);
  private DettaglioCarburanteJmsProducer dettaglioCarburanteJmsProducer = mock(DettaglioCarburanteJmsProducer.class);
  private DettaglioStanziamentiResourceExt dettaglioStanziamentiResourceExt;

  @BeforeEach
  public void setUp(){
    dettaglioStanziamentiResourceExt = new DettaglioStanziamentiResourceExt(dettaglioStaziamentoRepository, dettaglioCarburanteJmsProducer);
    given(dettaglioStaziamentoRepository.findLastDettaglioCarburantiByGlobalIdentifierExcludingStorni(any())).willReturn(Optional.empty());
    given(dettaglioStaziamentoRepository.findLastDettaglioCarburantiByGlobalIdentifierExcludingStorni(eq("existingCode"))).willReturn(Optional.of(new DettaglioStanziamentoCarburanteEntity("gb")));
    this.mockMvc = MockMvcBuilders.standaloneSetup(dettaglioStanziamentiResourceExt).build();
  }

  @Test
  public void forceSendTransaction_with_CARBURANTI_type() {
    dettaglioStanziamentiResourceExt.forceSendTransaction(StanziamentiDetailsType.CARBURANTI, "existingCode");
    then(dettaglioCarburanteJmsProducer)
      .should()
      .sendDetailFuelMessage(any());
  }

  @Test
  public void forceSendTransaction_with_GENERICO_type_should_return_error() {
      dettaglioStanziamentiResourceExt.forceSendTransaction(StanziamentiDetailsType.GENERICO, "existingCode");
      then(dettaglioCarburanteJmsProducer)
        .shouldHaveZeroInteractions();
  }

  @Test
  public void call_API_forceSendTransaction_with_GENERICO_type_should_return_error() throws Exception {
    mockMvc.perform(get(DettaglioStanziamentiResourceExt.API_FORCE_PROPAGATE_TRANSACTION + "/" + StanziamentiDetailsType.GENERICO + "/existingCode"))
      .andExpect(status().isUnprocessableEntity());
  }

  @Test
  public void call_API_forceSendTransaction_with_CARBURANTI_type() throws Exception {
    mockMvc.perform(get(DettaglioStanziamentiResourceExt.API_FORCE_PROPAGATE_TRANSACTION + "/" + StanziamentiDetailsType.CARBURANTI + "/existingCode"))
      .andExpect(status().isOk());
  }

}
