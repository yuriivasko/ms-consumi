package it.fai.ms.consumi.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import feign.codec.Encoder;
import it.fai.ms.consumi.client.AuthorizationApiService;
import it.fai.ms.consumi.client.StanziamentiApiService;
import it.fai.ms.consumi.client.StanziamentiPostException;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.RestClientProperties;
import it.fai.ms.consumi.domain.navision.Stanziamento;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.model.StanziamentoToNavMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@Tag("develop")
public class NotificationToNavTest
  extends AbstractIntegrationTestSpringContext {

  private Logger log = LoggerFactory.getLogger(getClass());

  public Encoder feignEncoder() {
    var jacksonConverter = new MappingJackson2HttpMessageConverter();
    ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(jacksonConverter);
    return new SpringEncoder(objectFactory);
  }

  @Inject
  private StanziamentiApiService stanziamentiApi;

  @Inject
  private AuthorizationApiService authorization;

  @Inject
  private StanziamentoToNavMapper stanziamentoMapper;

  @Inject
  private ApplicationProperties applicationProperties;

  private String authorizationToken = null;

  @BeforeEach
  public void setUp() {

    RestClientProperties restclient = applicationProperties.getRestclient();
    log.info("CONFIGURATION REST CLIENT: {}", restclient.getUrl());
    authorizationToken = authorization.getAuthorizationToken(restclient.getGrant_type(), restclient.getUsername(),
                                                             restclient.getPassword());

    log.info("Authorization TOKEN: {}", authorizationToken);
  }

  @Test
  public void test() throws StanziamentiPostException {
    Stanziamento domainNav = stanziamentoMapper.toDomainNav(createStanziamentoDTO());
    List<Stanziamento> stanziamenti = new ArrayList<>();
    stanziamenti.add(domainNav);
    stanziamentiApi.stanziamentiPost(authorizationToken, stanziamenti);
  }

  private StanziamentoMessage createStanziamentoDTO() {
    StanziamentoMessage dto = new StanziamentoMessage(String.valueOf(Math.random()));

    dto.setStatoStanziamento(0);
    dto.setTipoFlusso(6);
    dto.setDataErogazioneServizio(LocalDate.now());
    dto.setNrFornitore("0046348");
    dto.setNrCliente("0046348");
    dto.setNrArticolo("2000");
    dto.setPaese("ITALIA");
    dto.setAnnoStanziamento(2018);
    dto.setTarga("BW291NA");
    dto.setClasseEuroVeicolo("4");
    dto.setPrezzo(200.5d);
    dto.setCosto(35.20d);
    dto.setQuantita(2.1d);
    dto.setNr(21);
    dto.setValuta("EURO");
    dto.setConguaglio(false);
    dto.setCodicestanziamentoProvvisorio("");
    dto.setDocumentoDaFornitore("");

    return dto;
  }

}
