package it.fai.ms.consumi.repository.device;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackageClasses = {ViewStoricoDispositivoVeicoloContratto.class,StoricoStatoServizio.class})
@DataJpaTest
@EntityScan(basePackageClasses = {ViewStoricoDispositivoVeicoloContratto.class,StoricoStatoServizio.class})
@EnableJpaRepositories(basePackages = "it.fai.ms.consumi.repository.storico_dml" )
@Tag("unit")
@Transactional
class DeviceRepositoryImplTest {

  @Autowired
  private EntityManager em;

  private DeviceRepository deviceRepository;
  
  static boolean soloLaPrimaVolta = true;

  @BeforeEach
  void setUp() throws Exception {
    deviceRepository = new DeviceRepositoryImpl(em, new DeviceEntityMapper());

      var device_1 = new ViewStoricoDispositivoVeicoloContratto();
      device_1.setId("::1::");
      device_1.setContrattoNumero("::contractId-1::");
      device_1.setDataVariazione(Instant.ofEpochMilli(1000));
      device_1.setSerialeDispositivo("::deviceId-1::");
      device_1.setStato("::state::");
      device_1.setVeicoloClasseEuro("::euroClass::");
      device_1.setVeicoloNazione("::country::");
      device_1.setVeicoloTarga("::licensePlate::");
      device_1.setTipoDispositivo(TipoDispositivoEnum.TELEPASS_EUROPEO);
      device_1.setDispositivoDmlUniqueIdentifier("::dml::");
  
      var device_2 = new ViewStoricoDispositivoVeicoloContratto();
      device_2.setId("::2::");
      device_2.setContrattoNumero("::contractId-2::");
      device_2.setDataVariazione(Instant.ofEpochMilli(4000));
      device_2.setSerialeDispositivo("::deviceId-1::");
      device_2.setStato("::state::");
      device_2.setVeicoloClasseEuro("::euroClass::");
      device_2.setVeicoloNazione("::country::");
      device_2.setVeicoloTarga("::licensePlate::");
      device_2.setTipoDispositivo(TipoDispositivoEnum.TELEPASS_EUROPEO);
  
      var device_3 = new ViewStoricoDispositivoVeicoloContratto();
      device_3.setId("::3::");
      device_3.setContrattoNumero("::contractId-1::");
      device_3.setDataVariazione(Instant.ofEpochMilli(7000));
      device_3.setSerialeDispositivo("::deviceId-2::");
      device_3.setStato("::state::");
      device_3.setVeicoloClasseEuro("::euroClass::");
      device_3.setVeicoloNazione("::country::");
      device_3.setVeicoloTarga("::licensePlate::");
      device_3.setTipoDispositivo(TipoDispositivoEnum.TELEPASS_EUROPEO);
      
      var service = new StoricoStatoServizio();
      service.setPan("::pan::");
      service.setTipoServizio(TipoServizioEnum.CARBURANTI.name());
      service.setDispositivoDmlUniqueIdentifier("::dml::");
  
      em.persist(device_1);
      em.persist(device_2);
      em.persist(device_3);
      em.persist(service);
  
      em.flush();
  }
  
  @Test
  void testFindDeviceBySeriale() {

    var optionalDeviceByUuid = deviceRepository.findDeviceBySeriale("::deviceId-1::",TipoDispositivoEnum.TELEPASS_EUROPEO, null);

    assertThat(optionalDeviceByUuid).contains(newDevice());
  }

  @Test
  void testFindCustomerByUuid_notExists() {

    var optionalDeviceByUuid = deviceRepository.findDeviceBySeriale("::deviceIdNotPresent::",TipoDispositivoEnum.TELEPASS_EUROPEO, null);

    assertThat(optionalDeviceByUuid).isNotPresent();
  }
  
  @Test
  void findDeviceByPan() {

    var optionalDeviceByUuid = deviceRepository.findDeviceByPan("::pan::", TipoServizioEnum.CARBURANTI, null);

    assertThat(optionalDeviceByUuid).isPresent();
  }

  private static Device newDevice() {
    var device = new Device("::deviceId-1::",TipoDispositivoEnum.TELEPASS_EUROPEO);
    device.setContractNumber("::contractId-2::");
    return device;
  }

}
