package it.fai.ms.consumi.service;

import static java.time.Instant.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.time.Instant;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.repository.vehicle.VehicleRepository;

@Tag("unit")
class VehicleServiceImplTest {

  private final VehicleRepository mockVehicleRepository = mock(VehicleRepository.class);
  private VehicleService          vehicleService;

  @BeforeEach
  void setUp() throws Exception {
    vehicleService = new VehicleServiceImpl(mockVehicleRepository);
  }
  
  private Device deviceForTest() {
    Device device = new Device("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO);
    device.setVehicleUUID("::vehicleUuid::");
    return device;
  }
  
  private Device deviceForTestWithNoVehicleUUID() {
    return new Device("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO);
   }
  
  @Test
  void when_deviceUuid_exists_then_return_emptyOptional() {

    given(mockVehicleRepository.findVehicleByVehicleUuid(anyString(), any(Instant.class))).willReturn(Optional.empty());

    var optionalVehicle = vehicleService.newVehicleByVehicleUUID_WithFareClass(deviceForTest(), now(), "");

    assertThat(optionalVehicle).isEmpty();
  }
  
  @Test
  void when_deviceUuid_isEmpty_then_return_emptyOptional() {

    given(mockVehicleRepository.findVehicleByVehicleUuid(anyString(), any(Instant.class))).willReturn(Optional.empty());

    var optionalVehicle = vehicleService.newVehicleByVehicleUUID_WithFareClass(deviceForTestWithNoVehicleUUID(), now(), "");

    assertThat(optionalVehicle).isEmpty();
  }

  @Test
  void when_deviceUuid_exists_then_return_emptyvehicle() {
    given(mockVehicleRepository.findVehicleByVehicleUuid(any(),any(Instant.class))).willReturn(Optional.of(mock(Vehicle.class)));

    var optionalVehicle = vehicleService.newVehicleByVehicleUUID_WithFareClass(deviceForTest(),now(),"");

    assertThat(optionalVehicle).isNotEmpty();
  }
}
