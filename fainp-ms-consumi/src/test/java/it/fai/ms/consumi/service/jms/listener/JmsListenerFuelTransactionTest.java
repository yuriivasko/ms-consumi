package it.fai.ms.consumi.service.jms.listener;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.fai.ms.common.jms.dto.consumi.fuel.DresserWSTransactionRecord;
import it.fai.ms.common.jms.dto.consumi.fuel.FuelTransactionDTO;
import it.fai.ms.common.jms.dto.consumi.fuel.Q8WSTransactionRecord;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.service.consumer.TrackyCardCarburantiStandardRecordConsumer;
import it.fai.ms.consumi.service.jms.mapper.FuelTransactionDTOMapper;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.record.RecordPersistenceService;

@Tag("unit")
public class JmsListenerFuelTransactionTest {

  private final StanziamentiParamsService                  stanziamentiParamsService                  = mock(StanziamentiParamsService.class);
  private final ServiceProviderRepository                  spService                                  = mock(ServiceProviderRepository.class);
  private final FuelTransactionDTOMapper                   fuelTransactionDTOMapper                   = mock(FuelTransactionDTOMapper.class);
  private final RecordPersistenceService                   persistenceService                         = mock(RecordPersistenceService.class);
  private final TrackyCardCarburantiStandardRecordConsumer trackyCardCarburantiStandardRecordConsumer = mock(TrackyCardCarburantiStandardRecordConsumer.class);

  JmsListenerFuelTransaction jmsListenerFuelTransaction;

  @BeforeEach
  void setUp() {
    jmsListenerFuelTransaction = new JmsListenerFuelTransaction(stanziamentiParamsService, spService,
                                                                fuelTransactionDTOMapper, persistenceService,
                                                                trackyCardCarburantiStandardRecordConsumer);
    final StanziamentiParams stanziamentiParamsMock = mock(StanziamentiParams.class);

    given(stanziamentiParamsService.findByQuery(any()))
      .willReturn(Optional.of(stanziamentiParamsMock));

    given(stanziamentiParamsMock.getSupplier())
      .willReturn(mock(Supplier.class));

    given(stanziamentiParamsMock.getArticle())
      .willReturn(mock(Article.class));

    final ServiceProvider serviceProviderMock = mock(ServiceProvider.class);
    given(spService.findByProviderCode(any()))
      .willReturn(serviceProviderMock);

    given(serviceProviderMock.getFormat())
      .willReturn(Format.DRESSER_WS);

    given(fuelTransactionDTOMapper.toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any()))
      .willReturn(mock(TrackyCardCarburantiStandardRecord.class));

  }

  @Test
  public void consumeMessage_q8() {

    Q8WSTransactionRecord q8WSTransactionRecord = new Q8WSTransactionRecord();

    FuelTransactionDTO fuelTransactionDTO = new FuelTransactionDTO();
    fuelTransactionDTO.setRecord(q8WSTransactionRecord);

    jmsListenerFuelTransaction.consumeMessage(fuelTransactionDTO);

    then(stanziamentiParamsService)
      .should(times(1))
      .findByQuery(any());

    then(spService)
      .should(times(1))
      .findByProviderCode(any());

    then(fuelTransactionDTOMapper)
      .should(times(1))
      .toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any());

    then(persistenceService)
      .should(times(1))
      .persist(any());

    then(trackyCardCarburantiStandardRecordConsumer)
      .should(times(1))
      .consume(any(), any(), any());

  }

  @Test
  public void consumeMessage_dresser() {

    DresserWSTransactionRecord dresserWSTransactionRecord = new DresserWSTransactionRecord();

    FuelTransactionDTO fuelTransactionDTO = new FuelTransactionDTO();
    fuelTransactionDTO.setRecord(dresserWSTransactionRecord);

    jmsListenerFuelTransaction.consumeMessage(fuelTransactionDTO);

    then(stanziamentiParamsService)
      .should(times(1))
      .findByQuery(any());

    then(spService)
      .should(times(1))
      .findByProviderCode(any());

    then(fuelTransactionDTOMapper)
      .should(times(1))
      .toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any());

    then(persistenceService)
      .should(times(1))
      .persist(any());

    then(trackyCardCarburantiStandardRecordConsumer)
      .should(times(1))
      .consume(any(), any(), any());
  }

  @Test
  public void consumeMessage_fail_with_runtime_exception_id_not_find_param_stanziamenti() {
    Q8WSTransactionRecord q8WSTransactionRecord = new Q8WSTransactionRecord();

    FuelTransactionDTO fuelTransactionDTO = new FuelTransactionDTO();
    fuelTransactionDTO.setRecord(q8WSTransactionRecord);

    Mockito.reset(stanziamentiParamsService);
    given(stanziamentiParamsService.findByQuery(any()))
      .willReturn(null);

    assertThrows(RuntimeException.class, () -> {
      jmsListenerFuelTransaction.consumeMessage(fuelTransactionDTO);

      then(stanziamentiParamsService)
        .should(times(1))
        .findByQuery(any());

      then(spService)
        .should(times(0))
        .findByProviderCode(any());

      then(fuelTransactionDTOMapper)
        .should(times(0))
        .toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any());

      then(persistenceService)
        .should(times(0))
        .persist(any());

      then(trackyCardCarburantiStandardRecordConsumer)
        .should(times(0))
        .consume(any(), any(), any());
    });
  }

  @Test
  public void consumeMessage_fail_with_runtime_exception_id_not_find_service_provider() {
    Q8WSTransactionRecord q8WSTransactionRecord = new Q8WSTransactionRecord();

    FuelTransactionDTO fuelTransactionDTO = new FuelTransactionDTO();
    fuelTransactionDTO.setRecord(q8WSTransactionRecord);

    Mockito.reset(spService);
    given(spService.findByProviderCode(any()))
      .willReturn(null);

    assertThrows(RuntimeException.class, () -> {
      jmsListenerFuelTransaction.consumeMessage(fuelTransactionDTO);

      then(stanziamentiParamsService)
        .should(times(1))
        .findByQuery(any());

      then(spService)
        .should(times(1))
        .findByProviderCode(any());

      then(fuelTransactionDTOMapper)
        .should(times(0))
        .toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any());

      then(persistenceService)
        .should(times(0))
        .persist(any());

      then(trackyCardCarburantiStandardRecordConsumer)
        .should(times(0))
        .consume(any(), any(), any());
    });
  }

  @Test
  public void consumeMessage_fail_with_runtime_exception_if_throw_exception_fuelTransactionDTOMapper() {
    Q8WSTransactionRecord q8WSTransactionRecord = new Q8WSTransactionRecord();

    FuelTransactionDTO fuelTransactionDTO = new FuelTransactionDTO();
    fuelTransactionDTO.setRecord(q8WSTransactionRecord);

    Mockito.reset(fuelTransactionDTOMapper);
    given(fuelTransactionDTOMapper.toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any()))
      .willThrow(RuntimeException.class);

    assertThrows(RuntimeException.class, () -> {
      jmsListenerFuelTransaction.consumeMessage(fuelTransactionDTO);

      then(stanziamentiParamsService)
        .should(times(1))
        .findByQuery(any());

      then(spService)
        .should(times(1))
        .findByProviderCode(any());

      then(fuelTransactionDTOMapper)
        .should(times(1))
        .toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any());

      then(persistenceService)
        .should(times(0))
        .persist(any());

      then(trackyCardCarburantiStandardRecordConsumer)
        .should(times(0))
        .consume(any(), any(), any());
    });
  }

  @Test
  public void consumeMessage_fail_with_runtime_exception_if_persistenceService_throw_exception() {
    Q8WSTransactionRecord q8WSTransactionRecord = new Q8WSTransactionRecord();

    FuelTransactionDTO fuelTransactionDTO = new FuelTransactionDTO();
    fuelTransactionDTO.setRecord(q8WSTransactionRecord);

    Mockito.reset(persistenceService);
    willThrow(RuntimeException.class)
      .given(persistenceService)
      .persist(any());

    assertThrows(RuntimeException.class, () -> {
      jmsListenerFuelTransaction.consumeMessage(fuelTransactionDTO);

      then(stanziamentiParamsService)
        .should(times(1))
        .findByQuery(any());

      then(spService)
        .should(times(1))
        .findByProviderCode(any());

      then(fuelTransactionDTOMapper)
        .should(times(1))
        .toTrackyCardCarburantiStandardRecord(any(), any(), any(), any(), any());

      then(persistenceService)
        .should(times(1))
        .persist(any());

      then(trackyCardCarburantiStandardRecordConsumer)
        .should(times(0))
        .consume(any(), any(), any());
    });
  }
}
