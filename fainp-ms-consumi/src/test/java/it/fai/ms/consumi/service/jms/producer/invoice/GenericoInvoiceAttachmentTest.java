package it.fai.ms.consumi.service.jms.producer.invoice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.fattura.GenericoAllegatoFattureMessage;
import it.fai.ms.common.jms.fattura.GenericoConsumo;
import it.fai.ms.common.jms.fattura.HeaderDispositivo;
import it.fai.ms.common.jms.fattura.ReportGenericoDTO;
import it.fai.ms.consumi.AbstractCommonTest;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.client.VehicleClientService;
import it.fai.ms.consumi.client.efservice.EfserviceRestClient;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;
import it.fai.ms.consumi.repository.device.TipoSupporto;
import it.fai.ms.consumi.repository.device.TipoSupportoRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepositoryImpl;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2EntityBuilder;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Repository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2RepositoryImpl;
import it.fai.ms.consumi.service.ViewAllegatiGenericoService;
import it.fai.ms.consumi.service.impl.ViewAllegatiGenericoServiceImpl;
import it.fai.ms.consumi.testutils.ConsumiTestUtility;

@ExtendWith(SpringExtension.class)
@DataJpaTest(showSql = false)
@Tag("integration")
public class GenericoInvoiceAttachmentTest extends AbstractCommonTest {

  private static final String CLASSE_TARIFFARIA = "::classeTariffaria::";
  private static final String CAUSALE           = "::causale::";

  private List<UUID> uuids = new ArrayList<>();
  @Autowired
  EntityManager      em;

  StanziamentiParamsRepository           stanziamentiParamsRepository;
  ViewAllegatoGenericoSezione2Repository attachmentRepository;
  GenericoInvoiceAttachment              attachment;

  ViewAllegatiGenericoService service;

  DettaglioStanziamantiRepository repository;

  StanziamentoEntity                  s;
  DettaglioStanziamentoGenericoEntity ds;

  List<ViewAllegatoGenericoSezione2Entity> viewData2;
  String                                   contratto1;
  Set<String>                              viewData1;

  static String CONTRACT_CODE     = "::contractCode" + RandomString();
  static String STANZIAMENTO_CODE = RandomString();

  RequestsCreationAttachments requestCreationAttachments;
  RaggruppamentoArticoliDTO   raggruppamentoArticoliDTO;

  private ConsumiTestUtility consumiTestUtility;

  private PetrolPumpService    petroPumpServiceMock     = mock(PetrolPumpService.class);
  private TipoSupportoRepository                        tipoSupportoRepoMock  = mock(TipoSupportoRepository.class);

  private PointDTO point;

  @BeforeEach
  void setUp() throws Exception {

    point = new PointDTO();
    point.setName("pointName");
    point.setCity("CityPoint");
    point.setCode("CodePoint");

    when(petroPumpServiceMock.getPoint(any())).thenReturn(point);
    TipoSupporto tipoSupporto = newTipoSupporto("::TipoSupporto_Mocked::");
    when(tipoSupportoRepoMock.findByTipoDispositivo(any())).thenReturn(tipoSupporto);

    attachmentRepository = Mockito.mock(ViewAllegatoGenericoSezione2RepositoryImpl.class);

    stanziamentiParamsRepository = new StanziamentiParamsRepositoryImpl(em, new StanziamentiParamsEntityMapper());

    service = new ViewAllegatiGenericoServiceImpl(attachmentRepository, stanziamentiParamsRepository, petroPumpServiceMock,
                                                  tipoSupportoRepoMock);

    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
                                                                                  new DettaglioStanziamentoGenericoEntityMapper(),
                                                                                  new DettaglioStanziamentoPedaggioEntityMapper(),
                                                                                  new DettaglioStanziamentoTrenoEntityMapper());
    repository = new DettaglioStanziamentiRepositoryImpl(em, dettaglioStanziamentoEntityMapper,
                                                         new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper));

    attachment = new GenericoInvoiceAttachment(service, repository);
    consumiTestUtility = new ConsumiTestUtility(em);
    fillData();

    List<String> codiciStanziamenti = uuids.stream()
                                           .map(UUID::toString)
                                           .collect(Collectors.toList());

    contratto1 = CONTRACT_CODE;

    viewData1 = new HashSet<String>();
    viewData1.add(contratto1);

    viewData2 = new ArrayList<ViewAllegatoGenericoSezione2Entity>();
    viewData2.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), null));
    viewData2.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), null));

    requestCreationAttachments = Mockito.mock(RequestsCreationAttachments.class);
    when(requestCreationAttachments.getCodiceFattura()).thenReturn(RandomNumeric(4));
    when(requestCreationAttachments.getCodiceRaggruppamentoArticolo()).thenReturn(RandomString(4));
    when(requestCreationAttachments.getDataFattura()).thenReturn(Instant.now());
    when(requestCreationAttachments.getNomeIntestazioneTessere()).thenReturn(RandomString());
    when(requestCreationAttachments.getDescrRaggruppamentoArticolo()).thenReturn(RandomString());
    when(requestCreationAttachments.getLang()).thenReturn("IT");

    raggruppamentoArticoliDTO = Mockito.mock(RaggruppamentoArticoliDTO.class);
    when(raggruppamentoArticoliDTO.getCodiceClienteFatturazione()).thenReturn(RandomString(6));
    when(raggruppamentoArticoliDTO.getCodiciStanziamenti()).thenReturn(codiciStanziamenti);
    when(raggruppamentoArticoliDTO.getRagioneSocialeClienteFatturazione()).thenReturn(RandomString());
    when(raggruppamentoArticoliDTO.getCodice()).thenReturn(RandomString());

  }
  
  private TipoSupporto newTipoSupporto(String tipoSupportoFix) {
    TipoSupporto tipoSupporto = new TipoSupporto();
    tipoSupporto.setTipoSupportoFix(tipoSupportoFix);
    return tipoSupporto;
  }

  @Test
  public void generateDtoToAttachmentInvoice() throws Exception {
    Object result = attachment.generateDtoToAttachmentInvoice(DettaglioStanziamentoType.GENERICO, requestCreationAttachments,
                                                              raggruppamentoArticoliDTO, TemplateAllegati.NO_DISPOSITIVO);
    assertThat(result).isNotNull();
    GenericoAllegatoFattureMessage genericoMessage = null;
    if (result instanceof GenericoAllegatoFattureMessage) {
      genericoMessage = (GenericoAllegatoFattureMessage) result;
    }
    assertThat(genericoMessage).isNotNull();
    assertThat(genericoMessage.getNomeDocumento()).contains(requestCreationAttachments.getCodiceFattura(),
                                                            requestCreationAttachments.getCodiceRaggruppamentoArticolo());
    assertThat(genericoMessage.getBody()).isEmpty();
  }

  private ViewAllegatoGenericoSezione2Entity addViewEntity(String contractCode, String id, String tipoDispositivo) {
    ViewAllegatoGenericoSezione2Entity e = new ViewAllegatoGenericoSezione2EntityBuilder().createViewAllegatoGenericoSezione2Entity();
    e.setId(id);
    e.setCodiceContratto(contractCode);
    e.setDataFine(LocalDateTime.now());
    e.setDataInizio(LocalDateTime.now());
    e.setDescrizioneAggiuntiva(RandomString());
    e.setPercIva(22.0);
    e.setQuantita(1.0);
    e.setImporto(RandomDouble());
    e.setImponibile(RandomDouble());
    e.setCodiceProdotto("PK001");
    e.setSerialNumber(RandomString());
    e.setPanNumber("");
    e.setTargaVeicolo(RandomLicense());
    e.setNazioneVeicolo("IT");
    e.setClassificazioneEuro("E");
    e.setTipoDispositivo(tipoDispositivo);
    e.setTratta(RandomString());
    e.setValuta("EUR");
    e.setCambio(RandomDouble());
    e.setClasseTariffaria("D");
    e.setCodiceFornitoreNav("F007227");
    e.setDocumentoDaFornitore("doc_fornitore");
    e.setPuntoErogazione("::puntoErogazione::");
    e.setRecordCode("DEFAULT");
    e.setRaggruppamentoArticoli(RandomString());
    return e;
  }

  private ReportGenericoDTO addReportGenericoElement(boolean b, int n) {
    List<GenericoConsumo> consumi = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      consumi.add(addGenericoDeviceConsumo());
    }

    ReportGenericoDTO dto = new ReportGenericoDTO();
    dto.setHeader(addDeviceHeader(b));
    dto.setConsumi(consumi);
    return dto;
  }

  private HeaderDispositivo addDeviceHeader(boolean b) {
    HeaderDispositivo h = new HeaderDispositivo();
    h.setClassificazioneEuro(randomElement("1", "2", "3", "4", "5", "6"));
    h.setNazioneVeicolo("IT");
    h.setTargaVeicolo(RandomLicense());
    if (b) {
      h.setPanNumber(RandomNumeric(14));
      h.setTipoDispositivo("TRACKYCARD");
    }
    return h;
  }

  private GenericoConsumo addGenericoDeviceConsumo() {
    GenericoConsumo c = new GenericoConsumo();
    c.setCambio(RandomDouble());
    c.setDataInizio(LocalDateTime.now()
                                 .minusDays(1L));
    c.setDataFine(LocalDateTime.now());
    c.setQuantita(new Double(randomElement("1", "2", "3")));
    c.setImponibile(RandomDouble());
    c.setPrezzoUnitario(RandomDouble());
    c.setCodiceProdotto(randomElement("VETRI", "GOMME", "PARCHEGGIO"));
    c.setValuta("EUR");
    c.setVeicoloClasseEuro("E");
    return c;
  }

  private void fillData() {
    int i = 0;
    i = createAllegato(i, TipoDispositivoEnum.DARTFORD_CROSSING);
    i = createAllegato(i, TipoDispositivoEnum.HGV);
    i = createAllegato(i, TipoDispositivoEnum.DARTFORD_CROSSING);
    i = createAllegato(i, TipoDispositivoEnum.HGV);
    i = createAllegato(i, TipoDispositivoEnum.HGV);
    i = createAllegato(i, TipoDispositivoEnum.HGV);
    i = createAllegato(i, TipoDispositivoEnum.DARTFORD_CROSSING);
    em.flush();
  }

  private int createAllegato(int i, TipoDispositivoEnum hgv) {
    UUID uuid;
    ViewAllegatoGenericoSezione2Entity allegatoGenerico;
    uuid = UUID.randomUUID();
    allegatoGenerico = new ViewAllegatoGenericoSezione2EntityBuilder().setCambio(1d)
                                                                      .setCausale(CAUSALE + i)
                                                                      .setClasseTariffaria(CLASSE_TARIFFARIA + i)
                                                                      .setClassificazioneEuro("classificazione euro " + i)
                                                                      .setCodiceContratto("" + i)
                                                                      .setDataFine(LocalDateTime.of(2019, 01, 01 + i, 10, 10))
                                                                      .setDataInizio(LocalDateTime.of(2019, 01, 01 + i, 07, 10))
                                                                      .setDescrizione("Descrizione 1")
                                                                      .setId(uuid.toString())
                                                                      .setImponibile(10d)
                                                                      .setImporto(10d)
                                                                      .setImportoConIva(12.2d)
                                                                      .setNazioneVeicolo("it")
                                                                      .setPercIva(22d)
                                                                      .setProdotto("Prodotto " + i)
                                                                      .setQuantita(1d)
                                                                      .setRaggruppamentoArticoli("RAG-1")
                                                                      .setSerialNumber("12345")
                                                                      .setTargaVeicolo("AB123CD")
                                                                      .setTipoDispositivo(hgv.name())
                                                                      .setTratta("IT123")
                                                                      .setValuta("EUR")
                                                                      .createViewAllegatoGenericoSezione2Entity();
    i++;
    uuids.add(uuid);
    consumiTestUtility.insertAllegatoGenerico(allegatoGenerico);
    return i;
  }

}
