package it.fai.ms.consumi.service.jms.producer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.dto.actico.DettaglioCarburantiDTO;
import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.service.dto.TransactionFuelDetailDTO;

@ExtendWith(SpringExtension.class)
class DettaglioCarburanteTransactionJmsProducerTest {

  private JmsProperties jmsProperties = mock(JmsProperties.class);

  private DettaglioStanziamantiRepository repositoryMock = mock(DettaglioStanziamantiRepository.class);

  private DettaglioCarburanteTransactionJmsProducer producer;

  private UUID idDettaglio = UUID.randomUUID();

  private StanziamentiDetailsType detailTypeCarburanti = StanziamentiDetailsType.CARBURANTI;

  private String globalIdentifierId = "globalidentifier";

  private String globalIdentifier = globalIdentifierId + "#INTERNAL";

  private TransactionFuelDetailDTO entity = mock(TransactionFuelDetailDTO.class);

  private BigDecimal quantity = new BigDecimal(125);
  private BigDecimal percIva  = new BigDecimal(22);

  private BigDecimal defaultValue = new BigDecimal(100);

  @BeforeEach
  void setUp() throws Exception {
    producer = new DettaglioCarburanteTransactionJmsProducer(jmsProperties, repositoryMock);
    given(jmsProperties.getUrl()).willReturn("no-host");
    given(entity.getDetailId()).willReturn(idDettaglio);
    given(entity.getTipoSorgente()).willReturn(Format.ACTICO);
    given(entity.getGlobalIdentifier()).willReturn(globalIdentifier);
    given(entity.getCodiceTrackyCard()).willReturn("::TRACKYCARD_CODE::");
    given(entity.getDataOraUtilizzo()).willReturn(Instant.EPOCH);
    given(entity.getPuntoErogazione()).willReturn("::punto_erogazione::");
    given(entity.getQuantity()).willReturn(quantity);
    given(entity.getPercIva()).willReturn(percIva);
  }

  @Test
  void sendDetailFuelMessageCostNotFound() {
    BigDecimal price = new BigDecimal(11.22);

    given(entity.getPrice()).willReturn(price);
    given(repositoryMock.findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(globalIdentifier, detailTypeCarburanti, idDettaglio,
                                                                               CostRevenue.COST)).willReturn(Optional.empty());

    DettaglioCarburantiDTO dto = producer.sendDetailFuelMessage(entity);

    assertThat(dto).isNotNull();
    assertThat(dto.getVendorAmt()).isNotNull();

    given(repositoryMock.findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(globalIdentifier, detailTypeCarburanti, idDettaglio,
                                                                               CostRevenue.COST)).willReturn(newOptDettaglioStanziamento());

    dto = producer.sendDetailFuelMessage(entity);

    verify(jmsProperties, times(2)).getUrl();

    assertThat(dto).isNotNull();
    assertThat(dto.getCode()).isEqualTo(globalIdentifier.toString());
    assertThat(dto.getQty()).isEqualTo(quantity);
    assertThat(dto.getBuyerAmt()).isEqualTo(DettaglioCarburanteTransactionJmsProducer.getAmount(price, quantity, percIva));
    assertThat(dto.getVendorAmt()).isEqualTo(DettaglioCarburanteTransactionJmsProducer.getAmount(defaultValue, quantity, percIva));
  }

  @Test
  void sendDetailFuelMessagePriceNotFound() {
    BigDecimal cost = new BigDecimal(11.22);

    given(entity.getCost()).willReturn(cost);
    given(repositoryMock.findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(globalIdentifier, detailTypeCarburanti, idDettaglio,
                                                                               CostRevenue.REVENUE)).willReturn(Optional.empty());

    DettaglioCarburantiDTO dto = producer.sendDetailFuelMessage(entity);

    assertThat(dto).isNotNull();
    assertThat(dto.getBuyerAmt()).isNotNull();

    given(repositoryMock.findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(globalIdentifier, detailTypeCarburanti, idDettaglio,
                                                                               CostRevenue.REVENUE)).willReturn(newOptDettaglioStanziamento());

    dto = producer.sendDetailFuelMessage(entity);

    verify(jmsProperties, times(2)).getUrl();

    assertThat(dto).isNotNull();
    assertThat(dto.getCode()).isEqualTo(globalIdentifier.toString());
    assertThat(dto.getQty()).isEqualTo(quantity);
    assertThat(dto.getBuyerAmt()).isEqualTo(DettaglioCarburanteTransactionJmsProducer.getAmount(defaultValue, quantity, percIva));
    assertThat(dto.getVendorAmt()).isEqualTo(DettaglioCarburanteTransactionJmsProducer.getAmount(cost, quantity, percIva));
  }

  @Test
  void sendDetailFuelMessagePriceCostFound() {
    BigDecimal cost = new BigDecimal(11.22);
    BigDecimal price = new BigDecimal(22.11);

    given(entity.getPrice()).willReturn(price);
    given(entity.getCost()).willReturn(cost);

    DettaglioCarburantiDTO dto = producer.sendDetailFuelMessage(entity);

    verify(jmsProperties).getUrl();

    assertThat(dto).isNotNull();
    assertThat(dto.getCode()).isEqualTo(globalIdentifier.toString());
    assertThat(dto.getQty()).isEqualTo(quantity);
    assertThat(dto.getBuyerAmt()).isEqualTo(DettaglioCarburanteTransactionJmsProducer.getAmount(price, quantity, percIva));
    assertThat(dto.getVendorAmt()).isEqualTo(DettaglioCarburanteTransactionJmsProducer.getAmount(cost, quantity, percIva));
  }

  private Optional<DettaglioStanziamentoEntity> newOptDettaglioStanziamento() {
    DettaglioStanziamentoCarburanteEntity dsc = new DettaglioStanziamentoCarburanteEntity(UUID.randomUUID()
                                                                                              .toString());
    dsc.setPrezzoUnitarioCalcolatoNoiva(defaultValue);
    dsc.setCostoUnitarioCalcolatoNoiva(defaultValue);
    return Optional.of(dsc);
  }

}
