package it.fai.ms.consumi.testutils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import javax.persistence.EntityManager;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;

@Service
public class ConsumiTestUtility {

  @Autowired
  private EntityManager em;

  private String CONTRACT_CODE     = "::contractCode::" + RandomString() + "::";
  private String STANZIAMENTO_CODE = "::" + RandomString() + "::";

  public ConsumiTestUtility(EntityManager em) {
    this.em = em;
  }

  public void insertAllegatoGenerico(ViewAllegatoGenericoSezione2Entity entity) {
    em.persist(entity);
  }

  public StanziamentoEntity createStanziamento(String stanziamentoCodeSuffix) {
    StanziamentoEntity s = new StanziamentoEntity(STANZIAMENTO_CODE + stanziamentoCodeSuffix);
    s.setTarga("::veicolo::");
    s.setTipoFlusso(TypeFlow.NONE);
    s.setGeneraStanziamento(GeneraStanziamento.COSTO);
    s.setStatoStanziamento(InvoiceType.P);
    s.setNumeroArticolo("::articles::");
    s.setNumeroFattura("numeroFattura");
    s.setDataErogazioneServizio(LocalDate.now());
    s.setConguaglio(false);
    em.persist(s);
    return s;
  }

  public DettaglioStanziamentoCarburanteEntity createDettaglioCarburante(String contractCodeSuffix) {
    DettaglioStanziamentoCarburanteEntity dsc = newDettaglioStanziamentoCarburante(CONTRACT_CODE + contractCodeSuffix);
    em.persist(dsc);
    return dsc;
  }

  public DettaglioStanziamentoGenericoEntity createDettaglioGenerico(String contractCodeSuffix, String vehicleLicenseId) {
    DettaglioStanziamentoGenericoEntity dsc = newDettaglioStanziamentoGenerico(CONTRACT_CODE + contractCodeSuffix, vehicleLicenseId);
    em.persist(dsc);
    return dsc;
  }

  private DettaglioStanziamentoGenericoEntity newDettaglioStanziamentoGenerico(String contractCode, String vehicleLicenseId) {
    DettaglioStanziamentoGenericoEntity entity = new DettaglioStanziamentoGenericoEntity("identifier#INTERNAL");
    entity.setQuantity(new BigDecimal(1));
    entity.setDeviceCode(RandomString());
    entity.setCustomerId("::codClientNAV::");
    entity.setContractCode(contractCode);
    entity.setStartDate(Instant.now());
    entity.setSourceType("::sorgente::");
    entity.setEndDate(Instant.now()
                             .plus(1, ChronoUnit.DAYS));
    entity.setSupplierCode("fornitore");
    entity.setVehicleLicenseId(vehicleLicenseId);
    entity.setVehicleLicenseCountryId("IT");
    return entity;
  }

  private DettaglioStanziamentoCarburanteEntity newDettaglioStanziamentoCarburante(String contractCode) {
    DettaglioStanziamentoCarburanteEntity entity = new DettaglioStanziamentoCarburanteEntity("identifier#INTERNAL");
    entity.setStorno(false);
    entity.setQuantita(new BigDecimal(1));
    entity.setKm(new BigDecimal(30));
    entity.setCustomerId("::codClientNAV::");
    entity.setTipoConsumo(InvoiceType.P);
    entity.setErogatore("::puntoErogazione::");
    entity.setPuntoErogazione("::punto_erogazione::");
    entity.setContractCode(contractCode);
    entity.setDataOraUtilizzo(Instant.now());
    return entity;
  }

  public void createViewDettaglioCarburanti(String id, String contractCodeSuffix) {
    ViewAllegatoCarburantiSezione2Entity view = new ViewAllegatoCarburantiSezione2Entity();
    view.setId(id);
    view.setTipoDispositivo("trackycard");
    view.setDataOraUtilizzo(Instant.now());
    view.setCodiceFornitore("AT02");
    view.setCodiceContratto(CONTRACT_CODE + contractCodeSuffix);
    view.setPuntoErogazione("::puntoErogazione::");
    view.setErogatore("::erogatore::");
    view.setQuantita(1d);
    view.setKm(152000d);

    view.setRecordCode("D0");
    view.setTransactionDetailCode("00001");

    view.setTargaVeicolo("::targa::");
    view.setNazioneVeicolo("IT");
    view.setImporto(200d);
    em.persist(view);
  }

  public void relatedStanziamentoOnDettagli(StanziamentoEntity s, DettaglioStanziamentoEntity ds) {
    s.getDettaglioStanziamenti()
     .add(ds);
    em.persist(s);

    ds.getStanziamenti()
      .add(s);
    em.persist(ds);
  }

  public static String RandomString() {
    return RandomStringUtils.random(5, true, true);
  }

  public void createViewDettaglioGenerico(String id, String contractCodeSuffix, String targa) {
    ViewAllegatoGenericoSezione2Entity view = new ViewAllegatoGenericoSezione2Entity();
    view.setId(id);
    view.setTipoDispositivo("trackycard");
    view.setDataInizio(LocalDateTime.now());
    view.setDataFine(view.getDataInizio()
                         .plusDays(1));
    view.setTargaVeicolo(targa);
    view.setNazioneVeicolo("IT");
    view.setImporto(200d);
    view.setCodiceContratto(CONTRACT_CODE + contractCodeSuffix);
    em.persist(view);
  }
}
