package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.EmbeddableDettaglioStanziamentoEntityGeneric;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.EmbeddableDettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.ViewDettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.DispositiviDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.PadaggiDaFatturareCSVDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.PedaggiDaFatturareDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.ReportPedaggiDaFatturare;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.ViewReportConsumiPedaggiEntity;
import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;
import it.fai.ms.consumi.service.report.ReportCsv;

@ExtendWith(SpringExtension.class)
// @ComponentScan(basePackages = { "it.fai.ms.consumi.repository.dettaglio_stanziamento",
// "it.fai.ms.consumi.repository.stanziamento" })
@DataJpaTest(showSql = false)
@EntityScan(basePackages = { "it.fai.ms.consumi", "it.fai.common.notification.domain" })
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.common.notification.repository" })
@Tag("unit")
class ReportPedaggiDaFatturareTest {

  ReportPedaggiDaFatturare subject;

  @Autowired
  private EntityManager em;

  private final String[] ARTICLE_GROUPS = { "AUT_DE", "AUT_AU", "AUT_FR", "AUT_IT" };

  @BeforeEach
  private void createSubject() {
    subject = new ReportPedaggiDaFatturare(em);

  }

  public static final String BASE_PATH       = "i18n/";
  public static final String REPORT_MESSAGES = "reportMessages";

  private ReportCsv reportJson = new ReportCsv(resourceBundleMessageSource());

  static MessageSource resourceBundleMessageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasename(BASE_PATH + REPORT_MESSAGES);
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }

  @Test
  void csvSerialization() {
    {
      String numeroFattura = "1000";
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa121";
      String deviceObu = "deviceObu_1";
      String numeroCliente = "00001";
      String articleGroup = randomArticleCode();
      Instant date = createInstant("10/11/2018 00:00:00");
      Instant dataFattura = date;
      LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault())
                                             .toLocalDate()
                                             .minus(30, ChronoUnit.DAYS);
      TipoDispositivoEnum deviceType = TipoDispositivoEnum.TELEPASS_EUROPEO;
      ViewReportConsumiPedaggiEntity entity = createViewEntity(numeroCliente, numeroFattura, numeroCliente, dataFattura,
                                                               dataErogazioneServizio, null, veicoloClasse, veicoloNazioneTarga,
                                                               veicoloTarga, deviceObu, articleGroup, deviceType, "11111", "0000",
                                                               new BigDecimal("10.000"), createInstant("10/10/2018 00:00:00"));
      em.persist(entity);
    }
    {
      String numeroFattura = "1001";
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa122";
      String deviceObu = "deviceObu_2";
      String numeroCliente = "00001";
      String articleGroup = randomArticleCode();
      Instant date = createInstant("11/11/2018 00:00:00");
      Instant dataFattura = date;
      LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault())
                                             .toLocalDate()
                                             .minus(30, ChronoUnit.DAYS);
      TipoDispositivoEnum deviceType = TipoDispositivoEnum.TRACKYCARD;
      ViewReportConsumiPedaggiEntity entity = createViewEntity(numeroCliente, numeroFattura, numeroCliente, dataFattura,
                                                               dataErogazioneServizio, null, veicoloClasse, veicoloNazioneTarga,
                                                               veicoloTarga, deviceObu, articleGroup, deviceType, "22222", "33333",
                                                               new BigDecimal("15.0000"), createInstant("12/10/2018 00:00:00"));
      em.persist(entity);
    }
    {
      String numeroFattura = "1002";
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa123";
      String deviceObu = "deviceObu_2";
      String numeroCliente = "00002";
      String articleGroup = "AUT_DE";
      Instant date = createInstant("16/11/2018 00:00:00");
      Instant dataFattura = date;
      LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault())
                                             .toLocalDate()
                                             .minus(30, ChronoUnit.DAYS);
      TipoDispositivoEnum deviceType = TipoDispositivoEnum.TRACKYCARD;
      ViewReportConsumiPedaggiEntity entity = createViewEntity(numeroCliente, numeroFattura, numeroCliente, dataFattura,
                                                               dataErogazioneServizio, null, veicoloClasse, veicoloNazioneTarga,
                                                               veicoloTarga, deviceObu, articleGroup, deviceType, "99999", "66666",
                                                               new BigDecimal("25.0000"), createInstant("16/10/2018 00:00:00"));
      em.persist(entity);
    }
    it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto d = new it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto();
    d.setCodiceClienteNav("00002");
    d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
    d.setRaggruppamentoArticoliNav("AUT_DE");
    d.setLicensePlate("aa123");
    d.setLicensePlateCountry("it");
    d.setObu("deviceObu_2");

    LocalDate invoiceDateFrom = createDate("14/11/2018");
    LocalDate invoiceDateTo = createDate("20/11/2018");

    List<ViewReportConsumiPedaggiEntity> consumi = subject.queryDetailPedaggi("00002", Arrays.asList(d), true, false, null, null,
                                                                              invoiceDateFrom, invoiceDateTo);
    consumi.forEach(c -> c.setExitTimestamp(null));
    ZoneId zoneId = ZoneId.systemDefault();
    Locale locale = Locale.getDefault();
    List<PadaggiDaFatturareCSVDto> reportData = consumi.stream()
                                                       .map(pedaggiDaFatturareDto -> mapPedaggiDaFatturareDto(pedaggiDaFatturareDto))
                                                       .collect(Collectors.toList());

    String json = reportJson.toCsv(PadaggiDaFatturareCSVDto.class, reportData, locale, zoneId);
    System.out.println(json);
  }

  private PadaggiDaFatturareCSVDto mapPedaggiDaFatturareDto(ViewReportConsumiPedaggiEntity consumo) {
    return new PadaggiDaFatturareCSVDto(consumo);
  }

  @Test
  void testGetRaggruppamentiIsEmpty() {
    List<AllegatiConfigurationEntity> raggruppamentoAllegatiConf = subject.getRaggruppamentiByAllegatiConfigurationAndDetails("");
    assertThat(raggruppamentoAllegatiConf).isNotNull();
    assertThat(raggruppamentoAllegatiConf).isEmpty();
  }

  String codiceClienteNav = "cli1";

  @Test
  @Disabled
  void testServiziContainsString() throws Exception {
    ViewDettaglioStanziamentoPedaggioEntity view = null;
    String codiceCliente = "0012345";
    for (String article : ARTICLE_GROUPS) {
      view = createViewDettaglioStanziamentoPedaggio(codiceCliente, article);
      em.persist(view);
      
//      view = createViewConsumiPedaggiToRaggruppamento(codiceCliente, article);
//      em.persist(view);
    }

    for (String article : ARTICLE_GROUPS) {
      view = createViewDettaglioStanziamentoPedaggio(codiceCliente, article);
      em.persist(view);
      
//      view = createViewConsumiPedaggiToRaggruppamento("::utente::", article);
//      em.persist(view);
    }

    AllegatiConfigurationEntity allegatiConfig1 = newAllegatiConfig(ARTICLE_GROUPS[1]);
    em.persist(allegatiConfig1);
    AllegatiConfigurationEntity allegatiConfig2 = newAllegatiConfig(ARTICLE_GROUPS[2]);
    em.persist(allegatiConfig2);

    List<AllegatiConfigurationEntity> raggruppamentiAllegatiConf = subject.getRaggruppamentiByAllegatiConfigurationAndDetails(codiceCliente);
    assertThat(raggruppamentiAllegatiConf).extracting(a -> tuple(a.getCodiceRaggruppamentoArticolo(), a.getDescrizione()))
                                          .containsExactly(tuple(allegatiConfig1.getCodiceRaggruppamentoArticolo(),
                                                                 allegatiConfig1.getDescrizione()),
                                                           tuple(allegatiConfig2.getCodiceRaggruppamentoArticolo(),
                                                                 allegatiConfig2.getDescrizione()));
  }

  private ViewDettaglioStanziamentoPedaggioEntity createViewDettaglioStanziamentoPedaggio(String codiceCliente, String article) {
    ViewDettaglioStanziamentoPedaggioEntity view = new ViewDettaglioStanziamentoPedaggioEntity();
    view.getGenericAttributes().setCustomerId(codiceCliente);
    EmbeddableDettaglioStanziamentoPedaggioEntity genericPedaggio = new EmbeddableDettaglioStanziamentoPedaggioEntity();
    genericPedaggio.setArticlesGroup(article);
    view.setGenericDettaglioStanziamentoCarburante(genericPedaggio);
    return view;
  }

  private AllegatiConfigurationEntity newAllegatiConfig(String codeRaggruppamento) {
    AllegatiConfigurationEntity allegatiConfigurationEntity = new AllegatiConfigurationEntity();
    allegatiConfigurationEntity.setCodiceRaggruppamentoArticolo(codeRaggruppamento);
    allegatiConfigurationEntity.setDescrizione(String.join(StringUtils.SPACE, "Descrizione", codeRaggruppamento));
    allegatiConfigurationEntity.setGeneraAllegato(true);
    allegatiConfigurationEntity.setTipoDettaglioStanziamento(DettaglioStanziamentoType.PEDAGGIO);
    return allegatiConfigurationEntity;
  }

  @Test
  void testMasterConsumiPedaggiNonFatturati() throws Exception {
    String vehicleClass = ":vehicleClass:";
    String vehicleLicencePlate = "plate1";
    String deviceObu = "numObu";
    String articleGroupCode = "group1";
    String vehicleCountry = "country";
    TipoDispositivoEnum deviceType = TipoDispositivoEnum.TELEPASS_ITALIANO;

    Instant dataFattura = Instant.now()
                                 .minus(10, ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now()
                                                .minus(10, ChronoUnit.DAYS);
    ViewReportConsumiPedaggiEntity entity = createViewEntity(codiceClienteNav, null, codiceClienteNav, dataFattura, dataErogazioneServizio,
                                                             null, vehicleClass, vehicleCountry, vehicleLicencePlate, deviceObu,
                                                             articleGroupCode, deviceType, null, null, null, dataFattura);
    em.persist(entity);

    LocalDate consumptionDateFrom = dataErogazioneServizio.minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateTo = dataErogazioneServizio.plus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateFrom = dataFattura.atZone(ZoneId.systemDefault())
                                           .toLocalDate()
                                           .minus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateTo = dataFattura.atZone(ZoneId.systemDefault())
                                         .toLocalDate()
                                         .plus(10, ChronoUnit.DAYS);
    var consumi = subject.getDispositiviPedaggio(codiceClienteNav, articleGroupCode, false, true, consumptionDateFrom, consumptionDateTo,
                                                 invoiceDateFrom, invoiceDateTo);
    assertThat(consumi).isNotEmpty();
    assertThat(consumi.size()).isEqualTo(1);
    var row1 = consumi.get(0);
    assertThat(row1).isEqualTo(new DispositiviDto(codiceClienteNav, articleGroupCode, vehicleLicencePlate, vehicleCountry,
                                                  TipoDispositivoEnum.TELEPASS_ITALIANO, deviceObu));
  }

  @Test
  void testGetDispositivi_not_invoiced_consumption_date() {
    LocalDate consumptionDateTo = LocalDate.now()
                                           .minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";

    Instant dataFattura = Instant.now()
                                 .minus(30, ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now()
                                                .minus(30, ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, dataFattura, dataErogazioneServizio,
                                                      (ViewReportConsumiPedaggiEntity entity) -> isInInterval(entity.getDataErogazioneServizio(),
                                                                                                              consumptionDateFrom,
                                                                                                              consumptionDateTo)
                                                                                                 && entity.getNumeroFattura() == null
                                                                                                 && entity.getArticlesGroup()
                                                                                                          .equals(ARTICLE_GROUPS[0]),
                                                      TipoDispositivoEnum.TELEPASS_EUROPEO);

    List<DispositiviDto> dispositivi = subject.getDispositiviPedaggio(numeroCliente, ARTICLE_GROUPS[0], false, true, consumptionDateFrom,
                                                                      consumptionDateTo, null, null);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
                           .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));

  }

  @Test
  void testGetDispositivi_invoiced_consumption_date() {
    LocalDate consumptionDateTo = LocalDate.now()
                                           .minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";

    Instant dataFattura = Instant.now()
                                 .minus(30, ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now()
                                                .minus(30, ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, dataFattura, dataErogazioneServizio,
                                                      (ViewReportConsumiPedaggiEntity entity) -> isInInterval(entity.getDataErogazioneServizio(),
                                                                                                              consumptionDateFrom,
                                                                                                              consumptionDateTo)
                                                                                                 && entity.getNumeroFattura() != null
                                                                                                 && entity.getArticlesGroup()
                                                                                                          .equals(ARTICLE_GROUPS[0]),
                                                      TipoDispositivoEnum.TELEPASS_EUROPEO);

    List<DispositiviDto> dispositivi = subject.getDispositiviPedaggio(numeroCliente, ARTICLE_GROUPS[0], true, false, consumptionDateFrom,
                                                                      consumptionDateTo, null, null);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
                           .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));
  }

  @Test
  void testGetDispositivi_invoiced_invoice_date() {
    LocalDate invoiceDateTo = LocalDate.now()
                                       .minus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateFrom = invoiceDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";

    Instant dataFattura = Instant.now()
                                 .minus(30, ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now()
                                                .minus(30, ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, dataFattura, dataErogazioneServizio,
                                                      (ViewReportConsumiPedaggiEntity entity) -> isInInterval(entity.getDataFattura(),
                                                                                                              invoiceDateFrom,
                                                                                                              invoiceDateTo)
                                                                                                 && entity.getNumeroFattura() != null
                                                                                                 && entity.getArticlesGroup()
                                                                                                          .equals(ARTICLE_GROUPS[0]),
                                                      TipoDispositivoEnum.TELEPASS_EUROPEO);

    List<DispositiviDto> dispositivi = subject.getDispositiviPedaggio(numeroCliente, ARTICLE_GROUPS[0], true, false, null, null,
                                                                      invoiceDateFrom, invoiceDateTo);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
                           .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));
  }

  @Test
  void testGetDispositivi_notinvoiced_invoice_date() {
    LocalDate invoiceDateTo = LocalDate.now()
                                       .minus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateFrom = invoiceDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";

    Instant dataFattura = Instant.now()
                                 .minus(30, ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now()
                                                .minus(30, ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, dataFattura, dataErogazioneServizio,
                                                      (ViewReportConsumiPedaggiEntity entity) -> isInInterval(entity.getDataFattura(),
                                                                                                              invoiceDateFrom,
                                                                                                              invoiceDateTo)
                                                                                                 && entity.getNumeroFattura() == null
                                                                                                 && entity.getArticlesGroup()
                                                                                                          .equals(ARTICLE_GROUPS[0]),
                                                      TipoDispositivoEnum.TELEPASS_EUROPEO);

    List<DispositiviDto> dispositivi = subject.getDispositiviPedaggio(numeroCliente, ARTICLE_GROUPS[0], false, true, null, null,
                                                                      invoiceDateFrom, invoiceDateTo);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
                           .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));
  }

  @Test
  public void testGetDetailPedaggi() {

    {
      String numeroFattura = "1000";
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa121";
      String deviceObu = "deviceObu_1";
      String numeroCliente = "00001";
      String articleGroup = randomArticleCode();
      Instant date = createInstant("10/11/2018 00:00:00");
      Instant dataFattura = date;
      LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault())
                                             .toLocalDate()
                                             .minus(30, ChronoUnit.DAYS);
      TipoDispositivoEnum deviceType = TipoDispositivoEnum.TELEPASS_EUROPEO;
      ViewReportConsumiPedaggiEntity entity = createViewEntity(numeroCliente, numeroFattura, numeroCliente, dataFattura,
                                                               dataErogazioneServizio, null, veicoloClasse, veicoloNazioneTarga,
                                                               veicoloTarga, deviceObu, articleGroup, deviceType, "11111", "0000",
                                                               new BigDecimal("10.000"), createInstant("10/10/2018 00:00:00"));
      em.persist(entity);
    }
    {
      String numeroFattura = "1001";
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa122";
      String deviceObu = "deviceObu_2";
      String numeroCliente = "00001";
      String articleGroup = randomArticleCode();
      Instant date = createInstant("11/11/2018 00:00:00");
      Instant dataFattura = date;
      LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault())
                                             .toLocalDate()
                                             .minus(30, ChronoUnit.DAYS);
      TipoDispositivoEnum deviceType = TipoDispositivoEnum.TRACKYCARD;
      ViewReportConsumiPedaggiEntity entity = createViewEntity(numeroCliente, numeroFattura, numeroCliente, dataFattura,
                                                               dataErogazioneServizio, null, veicoloClasse, veicoloNazioneTarga,
                                                               veicoloTarga, deviceObu, articleGroup, deviceType, "22222", "33333",
                                                               new BigDecimal("15.0000"), createInstant("12/10/2018 00:00:00"));
      em.persist(entity);
    }
    {
      String numeroFattura = "1002";
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa123";
      String deviceObu = "deviceObu_2";
      String numeroCliente = "00002";
      String articleGroup = "AUT_DE";
      Instant date = createInstant("16/11/2018 00:00:00");
      Instant dataFattura = date;
      LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault())
                                             .toLocalDate()
                                             .minus(30, ChronoUnit.DAYS);
      TipoDispositivoEnum deviceType = TipoDispositivoEnum.TRACKYCARD;
      ViewReportConsumiPedaggiEntity entity = createViewEntity(numeroCliente, numeroFattura, numeroCliente, dataFattura,
                                                               dataErogazioneServizio, null, veicoloClasse, veicoloNazioneTarga,
                                                               veicoloTarga, deviceObu, articleGroup, deviceType, "99999", "66666",
                                                               new BigDecimal("25.0000"), createInstant("16/10/2018 00:00:00"));
      em.persist(entity);
    }
    it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto d = new it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto();
    d.setCodiceClienteNav("00002");
    d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
    d.setRaggruppamentoArticoliNav("AUT_DE");
    d.setLicensePlate("aa123");
    d.setLicensePlateCountry("it");
    d.setObu("deviceObu_2");

    LocalDate invoiceDateFrom = createDate("14/11/2018");
    LocalDate invoiceDateTo = createDate("20/11/2018");

    List<PedaggiDaFatturareDto> result = subject.getDetailPedaggi("00002", Arrays.asList(d), true, false, null, null, invoiceDateFrom,
                                                                  invoiceDateTo);

    assertThat(result).extracting(pf -> tuple(pf.getCodiceCaselloIngresso(), pf.getCodiceCaselloUscita(), pf.getImportoLordoSoggettoIva()
                                                                                                            .getValore(),
                                              pf.getDataUscita()))
                      .containsExactlyInAnyOrder(tuple("99999", "66666", new BigDecimal("25.0000"), createInstant("16/10/2018 00:00:00")));

  }

  private Instant createInstant(String s) {
    return LocalDateTime.parse(s, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss", Locale.getDefault()))
                        .atZone(ZoneId.of("Europe/Rome"))
                        .toInstant();
  }

  private LocalDate createDate(String d) {
    return LocalDate.parse(d, DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.getDefault()));
  }

  private boolean isInInterval(LocalDate target, LocalDate from, LocalDate to) {

    boolean result = target.isAfter(from) && (target.isBefore(to));

    result = result || target.atStartOfDay()
                             .isEqual(from.atStartOfDay())
             || target.atStartOfDay()
                      .isEqual(to.atStartOfDay());

    return result;
  }

  private boolean isInInterval(Instant target, LocalDate from, LocalDate to) {
    return isInInterval(target.atZone(ZoneId.systemDefault())
                              .toLocalDate(),
                        from, to);
  }

  private ViewReportConsumiPedaggiEntity createViewEntity(String customerId, String numeroFattura, String numeroCliente,
                                                          Instant dataFattura, LocalDate dataErogazioneServizio, String id,
                                                          String veicoloClasse, String veicoloNazioneTarga, String veicoloTarga,
                                                          String deviceObu, String articleGroup, TipoDispositivoEnum deviceType,
                                                          String codiceCaselloIngresso, String codiceCaselloUscita, BigDecimal importoNoIva,
                                                          Instant dataUscita) {
    ViewReportConsumiPedaggiEntity entity = new ViewReportConsumiPedaggiEntity();
    entity.setCustomerId(customerId);
    entity.setNumeroFattura(numeroFattura);
    entity.setNumeroCliente(numeroCliente);
    entity.setDataFattura(dataFattura);
    entity.setDataErogazioneServizio(dataErogazioneServizio);
    entity.setId(UUID.randomUUID()
                     .toString());
    entity.setVeicoloClasse(veicoloClasse);
    entity.setVeicoloNazioneTarga(veicoloNazioneTarga);
    entity.setVeicoloTarga(veicoloTarga);
    entity.setDeviceObu(deviceObu);
    entity.setArticlesGroup(articleGroup);
    entity.setDeviceType(deviceType);
    entity.setEntryGlobalGateIdentifierDescription(codiceCaselloIngresso);
    entity.setExitGlobalGateIdentifierDescription(codiceCaselloUscita);
    entity.setAmountExcludedVat(importoNoIva);
    entity.setExitTimestamp(dataUscita);
    return entity;
  }

  private ViewReportConsumiPedaggiEntity createViewConsumiPedaggiToRaggruppamento(String codiceCliente, String articleGroup) {
    ViewReportConsumiPedaggiEntity entity = new ViewReportConsumiPedaggiEntity();
    entity.setId(UUID.randomUUID()
                     .toString());
    entity.setCustomerId(codiceCliente);
    entity.setArticlesGroup(articleGroup);
    entity.setVeicoloTarga(UUID.randomUUID()
                               .toString());
    entity.setVeicoloNazioneTarga("IT");
    entity.setVeicoloClasse("EURO X");
    return entity;
  }

  private List<Tuple> populateTestDataSet(String numeroCliente, Instant dataFattura, LocalDate dataErogazioneServizio,
                                          Function<ViewReportConsumiPedaggiEntity, Boolean> expectedResult,
                                          TipoDispositivoEnum deviceType) {
    List<Tuple> tupleResult = new ArrayList<>();
    for (int i = 0; i < 30; i++) {
      String numeroFattura = (i % 2 == 0) ? "123456" : null;
      String id = null;
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa12" + i;
      String deviceObu = "deviceObu_" + i;
      String articleGroup = randomArticleCode();

      ViewReportConsumiPedaggiEntity entity = createViewEntity(numeroCliente, numeroFattura, numeroCliente, dataFattura,
                                                               dataErogazioneServizio, id, veicoloClasse, veicoloNazioneTarga, veicoloTarga,
                                                               deviceObu, articleGroup, deviceType, null, null, null, dataFattura);
      em.persist(entity);
      if (expectedResult.apply(entity)) {
        tupleResult.add(tuple(veicoloTarga, veicoloNazioneTarga, entity.getDeviceObu()));
      }
      dataFattura = dataFattura.plus(1, ChronoUnit.DAYS);
      dataErogazioneServizio = dataErogazioneServizio.plus(1, ChronoUnit.DAYS);
    }
    return tupleResult;
  }

  private String randomArticleCode() {
    return ARTICLE_GROUPS[new Random().nextInt(ARTICLE_GROUPS.length)];
  }

}
