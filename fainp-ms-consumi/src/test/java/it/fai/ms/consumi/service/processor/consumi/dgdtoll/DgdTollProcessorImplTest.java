package it.fai.ms.consumi.service.processor.consumi.dgdtoll;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
class DgdTollProcessorImplTest {

  DgdTollProcessorImpl dgdTollProcessor;

  private final ConsumoValidatorService                           consumoValidatorService = mock(ConsumoValidatorService.class);
  private final StanziamentiProducer                              stanziamentiProducer    = mock(StanziamentiProducer.class);
  private final DgdTollConsumoDettaglioStanziamentoPedaggioMapper mapper                  = mock(
    DgdTollConsumoDettaglioStanziamentoPedaggioMapper.class);
  private final VehicleService                                    vehicleService          = mock(VehicleService.class);
  private final CustomerService                                   customerService         = mock(CustomerService.class);

  @BeforeEach
  public void setUp() {
    dgdTollProcessor = new DgdTollProcessorImpl(consumoValidatorService, stanziamentiProducer, mapper,  vehicleService,
                                                customerService);
  }

  @Test
  void validateAndProcess_if_fail_blocking_validation_will_sikp_stanziamento_production() {
    DgdToll consumo = mock(DgdToll.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TRACKYCARD);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isFatalError())
      .willReturn(true);

    dgdTollProcessor.validateAndProcess(consumo, bucket);

    then(stanziamentiProducer)
      .shouldHaveZeroInteractions();
  }

  @Test
  void validateAndProcess_if_fail_waring_validation_will_produce_notification_and_stanziamentos() {
    DgdToll consumo = mock(DgdToll.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TRACKYCARD);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet<>(Arrays.asList(new Message("").withText(""))));

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    given(stanziamentiParams.getArticle())
      .willReturn(mock(Article.class));

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(mapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamento.class));

    ProcessingResult processingResult = dgdTollProcessor.validateAndProcess(consumo, bucket);

    Optional<Message> code = processingResult.getValidationOutcome().getMessages().stream().findFirst();
    Assertions.assertTrue(code.isPresent());
    
    then(consumoValidatorService)
      .should(times(1))
      .notifyProcessingResult(any());

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }

  @Test
  void validateAndProcess_if_success_validation_will_produce_stanziamentos() {
    DgdToll consumo = mock(DgdToll.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TRACKYCARD);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    given(stanziamentiParams.getArticle())
      .willReturn(mock(Article.class));

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(mapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamento.class));

    ProcessingResult processingResult = dgdTollProcessor.validateAndProcess(consumo, bucket);

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }
}
