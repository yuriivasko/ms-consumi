package it.fai.ms.consumi.service.record.tollcollect;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TollCollectRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
public class TollCollectRecordConsumoMapperTest {

  private TollCollectRecordConsumoMapper mapper;




  @BeforeEach
  public void init() {
    mapper = new TollCollectRecordConsumoMapper();
  }


  @Test
  public void mapRecordToConsumo() throws Exception {
    final String EXPECTED_CARD_NUMBER="cardnumber";
    final String EXPECTED_FILE_NAME="filename";
    final String EXPECTED_ROW_NUMBER="1";
    final String EXPECTED_GROSS_AMOUNT_SC="44,27";
    final String EXPECTED_NET_AMOUNT="36,89";
    final String EXPECTED_VAT_PERCENTAGE="20";
    final String EXPECTED_REGISTRATION_NUMBER="FB912DS";
    final String EXPECTED_REGISTRATION_COUNTRY="IT";
    TollCollectRecord record = new TollCollectRecord("tollcollect0.csv",Integer.parseInt(EXPECTED_ROW_NUMBER));
    record.setCardNumber(EXPECTED_CARD_NUMBER);
    record.setFileName(EXPECTED_FILE_NAME);
    record.setGrossAmountSC(EXPECTED_GROSS_AMOUNT_SC);
    record.setNetAmountSC(EXPECTED_NET_AMOUNT);
    record.setVatPercentage(EXPECTED_VAT_PERCENTAGE);
    record.setRegistrationNumber(EXPECTED_REGISTRATION_NUMBER);
    record.setRegistrationCountry(EXPECTED_REGISTRATION_COUNTRY);
    record.setSupplierServiceDate("08.08.2017");

    TollCollect consumo = mapper.mapRecordToConsumo(record);
    assertThat(consumo.getInvoiceType()).isEqualTo(InvoiceType.D);
    assertThat(consumo.getSource()).isNotNull().satisfies(s->{
      assertThat(s.getFileName()).isEqualTo(EXPECTED_FILE_NAME);
      assertThat(s.getRowNumber()).isEqualTo(Long.parseLong(EXPECTED_ROW_NUMBER));
    });
    assertThat(consumo.getDevice()).isNotNull()
      .satisfies(d->{
        assertThat(d.getPan()).isNullOrEmpty();
        assertThat(d.getType()).isEqualTo(TipoDispositivoEnum.TOLL_COLLECT);
        assertThat(d.getServiceType()).isEqualTo(TipoServizioEnum.PEDAGGI_GERMANIA);
      });
    assertThat(consumo.getAmount()).isNotNull()
      .satisfies(amount->{
        assertThat(amount.getVatRateBigDecimal().doubleValue()).isEqualTo(new BigDecimal("0.0").doubleValue());
        assertThat(amount.getAmountIncludedVatBigDecimal().doubleValue()).isEqualTo(Double.parseDouble(EXPECTED_NET_AMOUNT.replace(",",".")));
        assertThat(amount.getAmountExcludedVatBigDecimal().doubleValue()).isEqualTo(Double.parseDouble(EXPECTED_NET_AMOUNT.replace(",",".")));
      });
    assertThat(consumo.getVehicle()).isNotNull()
      .satisfies(vehicle -> {
        assertThat(vehicle.getLicensePlate()).isNotNull()
          .satisfies(vehicleLicensePlate -> {
            assertThat(vehicleLicensePlate.getLicenseId()).isEqualTo(EXPECTED_REGISTRATION_NUMBER);
            assertThat(vehicleLicensePlate.getCountryId()).isEqualTo(EXPECTED_REGISTRATION_COUNTRY);
          });
      });
  }

  @Test
  public void mapRecordToConsumo_no_vat() throws Exception {
    final String EXPECTED_CARD_NUMBER="cardnumber";
    final String EXPECTED_FILE_NAME="filename";
    final String EXPECTED_ROW_NUMBER="1";
    final String EXPECTED_GROSS_AMOUNT_SC="44,27";
    final String EXPECTED_NET_AMOUNT="44,27";
    final String EXPECTED_VAT_PERCENTAGE=null;
    final String EXPECTED_REGISTRATION_NUMBER="FB912DS";
    final String EXPECTED_REGISTRATION_COUNTRY="IT";
    TollCollectRecord record = new TollCollectRecord("tollcollect0.csv",Integer.parseInt(EXPECTED_ROW_NUMBER));
    record.setCardNumber(EXPECTED_CARD_NUMBER);
    record.setFileName(EXPECTED_FILE_NAME);
    record.setGrossAmountSC(EXPECTED_GROSS_AMOUNT_SC);
    record.setNetAmountSC(EXPECTED_NET_AMOUNT);
    record.setVatPercentage(EXPECTED_VAT_PERCENTAGE);
    record.setRegistrationNumber(EXPECTED_REGISTRATION_NUMBER);
    record.setRegistrationCountry(EXPECTED_REGISTRATION_COUNTRY);
    record.setSupplierServiceDate("08.08.2017");
    Double expectedNetAmount = Double.parseDouble(EXPECTED_GROSS_AMOUNT_SC.replace(",","."));



    TollCollect consumo = mapper.mapRecordToConsumo(record);
    assertThat(consumo.getInvoiceType()).isEqualTo(InvoiceType.D);
    assertThat(consumo.getSource()).isNotNull().satisfies(s->{
      assertThat(s.getFileName()).isEqualTo(EXPECTED_FILE_NAME);
      assertThat(s.getRowNumber()).isEqualTo(Long.parseLong(EXPECTED_ROW_NUMBER));
    });
    assertThat(consumo.getDevice()).isNotNull()
      .satisfies(d->{
        assertThat(d.getPan()).isNullOrEmpty();
        assertThat(d.getType()).isEqualTo(TipoDispositivoEnum.TOLL_COLLECT);
      });
    assertThat(consumo.getAmount()).isNotNull()
      .satisfies(amount->{
        assertThat(amount.getVatRateBigDecimal().doubleValue()).isEqualTo(new BigDecimal("0.0").doubleValue());
        assertThat(amount.getAmountIncludedVatBigDecimal().doubleValue()).isEqualTo(Double.parseDouble(EXPECTED_NET_AMOUNT.replace(",",".")));
        assertThat(amount.getAmountExcludedVatBigDecimal().doubleValue()).isEqualTo(Double.parseDouble(EXPECTED_NET_AMOUNT.replace(",",".")));
      });
    assertThat(consumo.getVehicle()).isNotNull()
      .satisfies(vehicle -> {
        assertThat(vehicle.getLicensePlate()).isNotNull()
          .satisfies(vehicleLicensePlate -> {
            assertThat(vehicleLicensePlate.getLicenseId()).isEqualTo(EXPECTED_REGISTRATION_NUMBER);
            assertThat(vehicleLicensePlate.getCountryId()).isEqualTo(EXPECTED_REGISTRATION_COUNTRY);
          });
      });
  }



}
