package it.fai.ms.consumi.service;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGeneratorInterface;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;


@Primary
@Component
public class StanziamentoCodeGeneratorStub implements StanziamentoCodeGeneratorInterface {
  private int i=0;

  @Override
  public synchronized String generate(InvoiceType _invoiceType) {

    return (i++)+"_"+_invoiceType;
  }

  public int getI() {
    return i;
  }

  public void reset() {
    i = 0;
  }
}
