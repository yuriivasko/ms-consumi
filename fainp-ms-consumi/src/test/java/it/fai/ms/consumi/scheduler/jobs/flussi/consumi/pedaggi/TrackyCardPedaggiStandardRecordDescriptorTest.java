package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecordBody;

@Tag("unit")
public class TrackyCardPedaggiStandardRecordDescriptorTest {

  public static final String TRACKY_CARD_FILENAME = "SI01ASI20170701040625.txt.20170701093000";       // O_DTC5659_IZ_2_0411032037_S_051117_185190
  private TrackyCardPedaggiStandardRecordDescriptor subject; 
  
  // private String header="\"Invoice Recipient\",\"Recipient City\",\"Statement Level\",\"EDI Partner Code\",\"RAG Code\",\"Invoice Number\",\"Invoice Date\",\"Due Date\",\"Position Number\",\"Subposition Number\",\"Processing Date\",\"Supplier Name\",\"Supplier Address 1\",\"Supplier Address 2\",\"Supplier UIN\",\"Supplier Tax Number\",\"Card Number\",\"Supplier Number\",\"Registration Country\",\"Registration Number\",\"Service Description\",\"Branch\",\"Supplier Service Date\",\"Supplier Cost Center\",\"Supplier Invoice Number\",\"Supplier Invoice Date\",\"Supplier Statement Date\",\"Sale Currency (SC)\",\"Net Amount (SC)\",\"VAT Percentage\",\"VAT Percentage (SC)\",\"Gross Amount (SC)\",\"Currency Rate\",\"Statement Currency (IC)\",\"Net Amount (IC)\",\"VAT (IC)\"";
  // private final Date now = Date.from(Instant.parse("2018-08-29T10:37:30.00Z"));
  
  @BeforeEach
  private void init() {
    subject = new TrackyCardPedaggiStandardRecordDescriptor();
  }
  
  @Test
  public void testDetail()throws Exception{
    
    String data = "D000000002PE002SI01000001                              7896970006014900218              20170630145927                              TOROVO                           4918   1082   60002200+EUR0                                                                                                            ";
    TrackyCardPedaggiStandardRecord result = subject.decodeRecordCodeAndCallSetFromString(data, subject.extractRecordCode(data), TRACKY_CARD_FILENAME, 1);
    
    assertThat(result).isInstanceOfSatisfying(TrackyCardPedaggiStandardRecordBody.class, r->{
      assertThat(r.getRecordCode()).isEqualTo("D0");
      assertThat(r.getRecordNumber()).isEqualTo("00000002");
      assertThat(r.getTipoMovimento()).isEqualTo("PE002");
      assertThat(r.getCodiceAutostrada()).isEqualTo("SI01000001");
      assertThat(r.getCodiceISOIIN()).isEqualTo("789697");
      assertThat(r.getCodiceGruppo()).isEqualTo("00");
      assertThat(r.getCodiceCliente()).isEqualTo("060149");
      assertThat(r.getNumeroTessera()).isEqualTo("0021");
      assertThat(r.getCifradiControllo()).isEqualTo("8"); 
      assertThat(r.getDataIngresso()).isEqualTo("");
      assertThat(r.getOraIngresso()).isEqualTo ("");
      assertThat(r.getDataUscita()).isEqualTo("20170630");
      assertThat(r.getOraUscita()).isEqualTo("145927");
      assertThat(r.getCaselloIngresso()).isEqualTo("");
      assertThat(r.getCaselloUscita()).isEqualTo("TOROVO");
      assertThat(r.getImponibile()).isEqualTo("4918");
      assertThat(r.getImposta()).isEqualTo("1082");
      assertThat(r.getImponibileImposta()).isEqualTo("6000");
      assertThat(r.getPercentualeIVA()).isEqualTo("2200");
      assertThat(r.getSegno()).isEqualTo("+");
      assertThat(r.getValuta()).isEqualTo("EUR");
      assertThat(r.getClasseVeicolo()).isEqualTo("0"); 
      
    });
  }
  
 
  
  @Test
  public void testHeader()throws Exception{ 
    String data = "H000000001SI01      20170701040625                                                                                                                                                                                                                                                                          ";
    TrackyCardPedaggiStandardRecord result = subject.decodeRecordCodeAndCallSetFromString(data, subject.extractRecordCode(data), TRACKY_CARD_FILENAME, 1);
    assertThat(result).isInstanceOfSatisfying(TrackyCardPedaggiStandardRecord.class, r->{
      assertThat(r.getRecordCode()).isEqualTo("H0"); 
    });
  }
  
  @Test
  public void testFooter()throws Exception{ 
    String data = "T000000101SI01      20170701040626                                                                                                                                                                                                                                                                          ";
    TrackyCardPedaggiStandardRecord result = subject.decodeRecordCodeAndCallSetFromString(data, subject.extractRecordCode(data), TRACKY_CARD_FILENAME, 1);
    assertThat(result).isInstanceOfSatisfying(TrackyCardPedaggiStandardRecord.class, r->{
      assertThat(r.getRecordCode()).isEqualTo("T0"); 
    });
  }  
  
  
  
  
}
