package it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd;

import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcome;
import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcomeFailure;
import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcomeFailureWithFatalError;
import static java.time.Instant.ofEpochSecond;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.consumi.carburanti.fai.TrackyCarbStdTestBuilder;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti.DettaglioStanziamentoCarburantiProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
class TrackyCardCarbstdProcessorImplTest {

  private final ConsumoValidatorService                  mockConsumoValidatorService        = mock(ConsumoValidatorService.class);
  private final DettaglioStanziamentoCarburantiProcessor mockDettaglioStanziamentoProcessor = mock(DettaglioStanziamentoCarburantiProcessor.class);
  private final VehicleService                           mockVehicleService                 = mock(VehicleService.class);
  private final CustomerService                          mockCustomerService                = mock(CustomerService.class);
  private ConsumoProcessor<TrackyCardCarburantiStandard> processor;

  @BeforeEach
  void setUp() throws Exception {
    processor = new TrackyCardCarbStdProcessorImpl(mockConsumoValidatorService, mockDettaglioStanziamentoProcessor,
                                                   new TrackyCardCarbStdDettaglioStanziamentoMapper(),
                                                   mockVehicleService, mockCustomerService);
  }

  @Test
  void when_validationOutcome_isFalse_blocking() {

    // given

    final var trackyCardCarbstd = TrackyCarbStdTestBuilder.newInstance()
                                                          .build();
    final var bucket = new Bucket("::bucketId::");

    when(mockConsumoValidatorService.validate(trackyCardCarbstd)).thenReturn(newValidationOutcomeFailureWithFatalError());

    // when

    final var processingResult = processor.validateAndProcess(trackyCardCarbstd, bucket);

    // then

    then(mockConsumoValidatorService).should()
                                     .validate(trackyCardCarbstd);

    then(mockDettaglioStanziamentoProcessor).shouldHaveZeroInteractions();

    var arg = ArgumentCaptor.forClass(it.fai.ms.consumi.service.processor.ProcessingResult.class);
    then(mockConsumoValidatorService).should().notifyProcessingResult(arg.capture());
    
    System.out.println(arg.getValue().getValidationOutcome().getMessages());
    var optional = arg.getValue().getValidationOutcome().getMessages().stream().filter(p -> p.getCode().equals(("::failure::"))).findFirst();
    assertTrue(optional.isPresent());
    
    //(eq("CPV-::failure::"), anyMap());

    then(mockVehicleService).shouldHaveZeroInteractions();

    assertThat(processingResult.getConsumo()).isEqualTo(trackyCardCarbstd);
    assertThat(processingResult.getValidationOutcome()
                               .isValidationOk()).isFalse();
    assertThat(processingResult.getValidationOutcome()
                               .isFatalError()).isTrue();
  }

  @Test
  void when_validationOutcome_isFalse_warning() {

    // given

    final TrackyCardCarburantiStandard model = TrackyCarbStdTestBuilder.newInstance()
                                                                       .withTransaction()
                                                                       .withDevice()
                                                                       .withFuelStation()
                                                                       .withNavSupplier()
                                                                       .withDate(ofEpochSecond(0))
                                                                       .build();

    final var bucket = new Bucket("::bucketId::");

    given(mockConsumoValidatorService.validate(model)).willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    given(mockVehicleService.newVehicleByVehicleUUID_WithFareClass(new Device("::deviceId::",TipoDispositivoEnum.TRACKYCARD), ofEpochSecond(0), "")).willReturn(Optional.of(newVehicle()));

    given(mockCustomerService.findCustomerByDeviceSeriale("::deviceId::",TipoDispositivoEnum.TRACKYCARD)).willReturn(Optional.of(new Customer("::customerId::")));

    given(mockCustomerService.findCustomerByContrattoNumero(any())).willReturn(Optional.of(new Customer("::customerId::")));
    // when

    final var processingResult = processor.validateAndProcess(model, bucket);

    // then

    then(mockConsumoValidatorService).should().validate(model);

    // TODO dettglio stanziamento
    then(mockDettaglioStanziamentoProcessor).should()
                                            .produce(any(), any());
    then(mockDettaglioStanziamentoProcessor).shouldHaveNoMoreInteractions();

    then(mockConsumoValidatorService).should().notifyProcessingResult(any());
    then(mockConsumoValidatorService).shouldHaveNoMoreInteractions();

    then(mockVehicleService).should()
                            .newVehicleByVehicleUUID_WithFareClass(model.getDevice(), ofEpochSecond(0), "");
    then(mockVehicleService).shouldHaveNoMoreInteractions();

    assertThat(processingResult.getConsumo()).isEqualTo(model);
    assertThat(processingResult.getValidationOutcome()
                               .isValidationOk()).isFalse();
    assertThat(processingResult.getValidationOutcome()
                               .isFatalError()).isFalse();

  }

  @Test
  void when_validationOutcome_isTrue() {

    // given

    final var trackyCardCarbstd = TrackyCarbStdTestBuilder.newInstance()
                                                          .withTransaction()
                                                          .withDevice()
                                                          .withFuelStation()
                                                          .withNavSupplier()
                                                          .withDate(ofEpochSecond(0))
                                                          .build();
    trackyCardCarbstd.setVehicle(Vehicle.newEmptyVehicleWithFareClass(""));

    final var bucket = new Bucket("::bucketId::");

    given(mockConsumoValidatorService.validate(trackyCardCarbstd)).willReturn(newValidationOutcome());

    given(mockVehicleService.newVehicleByVehicleUUID_WithFareClass(new Device("::deviceId::",TipoDispositivoEnum.TRACKYCARD), ofEpochSecond(0), "")).willReturn(Optional.of(newVehicle()));

    given(mockCustomerService.findCustomerByDeviceSeriale("::deviceId::",TipoDispositivoEnum.TRACKYCARD)).willReturn(Optional.of(new Customer("::customerId::")));

    // when

    final var processingResult = processor.validateAndProcess(trackyCardCarbstd, bucket);

    // then

    then(mockConsumoValidatorService).should()
                                     .validate(trackyCardCarbstd);
    then(mockConsumoValidatorService).shouldHaveNoMoreInteractions();

    then(mockConsumoValidatorService).shouldHaveZeroInteractions();

    then(mockVehicleService).should()
                            .newVehicleByVehicleUUID_WithFareClass(trackyCardCarbstd.getDevice(), ofEpochSecond(0), "");
    then(mockVehicleService).shouldHaveNoMoreInteractions();

    assertThat(processingResult.getConsumo()).isEqualTo(trackyCardCarbstd);
    assertThat(processingResult.getValidationOutcome()
                               .isValidationOk()).isTrue();
    assertThat(processingResult.getValidationOutcome()
                               .isFatalError()).isFalse();
  }

  private Vehicle newVehicle() {
    var vehicle = new Vehicle();
    vehicle.setEuroClass("::euroClass::");
    vehicle.setFareClass(null);
    vehicle.setLicensePlate(new VehicleLicensePlate("::licenseId::", "::countryId::"));
    return vehicle;
  }

}
