package it.fai.ms.consumi.repository.storico_dml.jms;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.ApplicationEventPublisher;

import it.fai.ms.common.dml.efservice.dto.DispositivoServizioDMLDTO;
import it.fai.ms.consumi.repository.storico_dml.StoricoDispositivoRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoStatoServizioRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.service.DeviceService;

@Tag("unit")
class JmsDmlStoricoDispositivoListenerTest {

  private static final Instant REFERENCE_START = Instant.EPOCH.plus(365, ChronoUnit.DAYS);

  private static final Instant REFERENCE_END = REFERENCE_START.plus(30, ChronoUnit.DAYS);

  private static final String DEVICE_ID1 = "deviceID1";

  private StoricoDispositivoRepository   repository;
  private StoricoStatoServizioRepository repoServizi;
  private ApplicationEventPublisher      publisher;

  StoricoDispositivo               dispositivo = init(new StoricoDispositivo(), d -> {
                                                 d.setDmlUniqueIdentifier(DEVICE_ID1);
                                               });
  JmsDmlStoricoDispositivoListener subject;

  private DeviceService deviceService;

  @BeforeEach
  public void setup() throws Exception {
    repository  = mock(StoricoDispositivoRepository.class);
    repoServizi = mock(StoricoStatoServizioRepository.class);
    publisher   = mock(ApplicationEventPublisher.class);
    deviceService = mock(DeviceService.class);
    subject     = new JmsDmlStoricoDispositivoListener(repository, repoServizi, publisher,deviceService);
  }

  private DispositivoServizioDMLDTO openInterval() {
    DispositivoServizioDMLDTO ser = new DispositivoServizioDMLDTO();
    ser.setDataFine(null);
    ser.setDataInizio(REFERENCE_START);
    ser.setDmlUniqueIdentifier("device1");
    return ser;
  }

  private DispositivoServizioDMLDTO closedInterval() {
    DispositivoServizioDMLDTO ser = new DispositivoServizioDMLDTO();
    ser.setDataInizio(REFERENCE_START);
    ser.setDataFine(REFERENCE_END);
    ser.setDmlUniqueIdentifier("device1");
    return ser;
  }

  // Open Interval, Storico Empty
  @Test
  public void salvataggioStoricoStatoServizioAttivo() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Collections.emptyList());

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(openInterval(), dispositivo);

    assertThat(entityToPersistServizio.size()).isEqualTo(1);
    var entity = entityToPersistServizio.get(0);
    assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
    assertThat(entity.getDataFineVariazione()).isNull();
    assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    assertThat(entity.getStato()).isEqualTo("ATTIVO");

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(0)).delete(ac.capture());
  }



  // Closed Interval Storico Empty
  @Test
  public void salvataggioStoricoStatoServizioNonAttivo() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Collections.emptyList());

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(closedInterval(), dispositivo);

    assertThat(entityToPersistServizio.size()).isEqualTo(2);
    {
      var entity = entityToPersistServizio.get(0);
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(1);
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isNull();
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(0)).delete(ac.capture());
  }

  // Closed Interval Storico 1 Open Attivo same date
  @Test
  public void salvataggioStoricoStatoServizioNonAttivoStoricoAttivo() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDataVariazione(REFERENCE_START);
    })));

    DispositivoServizioDMLDTO ser = closedInterval();

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(ser, dispositivo);

    assertThat(entityToPersistServizio.size()).isEqualTo(2);
    {
      var entity = entityToPersistServizio.get(0);
      assertThat(entity.getId()).isEqualTo("id1");
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(1);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isNull();
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(0)).delete(ac.capture());
  }

  // Storico 1 non attivo same date
  @Test
  public void salvataggioStoricoStatoServizioAttivoStoricoNonAttivo() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("NON_ATTIVO");
      s.setDataVariazione(REFERENCE_START);
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(openInterval(), dispositivo);

    assertThat(entityToPersistServizio.size()).isEqualTo(1);
    var entity = entityToPersistServizio.get(0);
    assertThat(entity.getId()).isEqualTo("id1");
    assertThat(entity.getDataFineVariazione()).isNull();
    assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
    assertThat(entity.getStato()).isEqualTo("ATTIVO");
    assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(0)).delete(ac.capture());
  }

  // Storico 1 non attivo starting before
  @Test
  public void salvataggioStoricoStatoServizioAttivoStoricoNonAttivoBefore() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("NON_ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.minus(1, ChronoUnit.DAYS));
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(openInterval(), dispositivo);

    assertThat(entityToPersistServizio.size()).isEqualTo(2);
    {
      var entity = entityToPersistServizio.get(0);
      assertThat(entity.getId()).isEqualTo("id1");
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START.minus(1, ChronoUnit.DAYS));
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(1);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isNull();
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(0)).delete(ac.capture());
  }

  // Open inetrval sorico attivo start before end after
  // Edit current add iniactive before reference and after
  @Test
  public void salvataggioStoricoStatoServizioAttivoStoricoNonAttivoBetween() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.minus(10, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_END.plus(10, ChronoUnit.DAYS));
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(closedInterval(), dispositivo);

    int i = 0;
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isEqualTo("id1");
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START.minus(10, ChronoUnit.DAYS));
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END.plus(10, ChronoUnit.DAYS));
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    assertThat(entityToPersistServizio.size()).isEqualTo(i);

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(0)).delete(ac.capture());
  }

  @Test
  public void salvataggioStoricoStatoServizioAttivoStoricoNonAttivoStartAfterEndAfter() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.plus(10, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_END.plus(10, ChronoUnit.DAYS));
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(closedInterval(), dispositivo);

    int i = 0;
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END.plus(10, ChronoUnit.DAYS));
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    assertThat(entityToPersistServizio.size()).isEqualTo(i);

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(1)).delete(ac.capture());
    assertThat(ac.getValue().getId()).isEqualTo("id1");
  }

  @Test
  public void salvataggioStoricoStatoServizioAttivoMultipleStorico() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.plus(5, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_START.plus(10, ChronoUnit.DAYS));
    }), init(new StoricoStatoServizio(), s -> {
      s.setId("id2");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_END.minus(10, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_END.minus(5, ChronoUnit.DAYS));
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(closedInterval(), dispositivo);

    int i = 0;
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isNull();;
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    assertThat(entityToPersistServizio.size()).isEqualTo(i);

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(2)).delete(ac.capture());
  }
  @Test
  public void salvataggioStoricoStatoServizioAttivoMultipleStoricoOverlapBefore() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.minus(5, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_START.plus(10, ChronoUnit.DAYS));
    }), init(new StoricoStatoServizio(), s -> {
      s.setId("id2");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_END.minus(10, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_END.minus(5, ChronoUnit.DAYS));
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(closedInterval(), dispositivo);

    int i = 0;
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isEqualTo("id1");
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START.minus(5,ChronoUnit.DAYS));
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isNull();;
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    assertThat(entityToPersistServizio.size()).isEqualTo(i);

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(1)).delete(ac.capture());
  }
  @Test
  public void salvataggioStoricoStatoServizioAttivoMultipleStoricoOverlapAfter() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.plus(5, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_START.plus(10, ChronoUnit.DAYS));
    }), init(new StoricoStatoServizio(), s -> {
      s.setId("id2");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_END.minus(10, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_END.plus(5, ChronoUnit.DAYS));
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(closedInterval(), dispositivo);

    int i = 0;
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END.plus(5, ChronoUnit.DAYS));;
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    assertThat(entityToPersistServizio.size()).isEqualTo(i);

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(2)).delete(ac.capture());
  }

  @Test
  public void salvataggioStoricoStatoServizioAttivoMultipleStoricoOverlapBeforeAfter() {
    when(deviceService.findStatiServizioInterval(anyString(), any(), any(), any())).thenReturn(Arrays.asList(init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.minus(5, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_START.plus(10, ChronoUnit.DAYS));
    }),init(new StoricoStatoServizio(), s -> {
      s.setId("id1");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.plus(10, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_START.plus(15, ChronoUnit.DAYS));
    }), init(new StoricoStatoServizio(), s -> {
      s.setId("id2");
      s.setStato("ATTIVO");
      s.setDispositivoDmlUniqueIdentifier(DEVICE_ID1);
      s.setDataVariazione(REFERENCE_START.plus(15, ChronoUnit.DAYS));
      s.setDataFineVariazione(REFERENCE_END.plus(5, ChronoUnit.DAYS));
    })));

    List<StoricoStatoServizio> entityToPersistServizio = subject.getEntityToPersistServizio(closedInterval(), dispositivo);

    int i = 0;
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isEqualTo("id1");
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START.minus(5,ChronoUnit.DAYS));
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_START);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getStato()).isEqualTo("ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    {
      var entity = entityToPersistServizio.get(i++);
      assertThat(entity.getId()).isNull();
      assertThat(entity.getDataVariazione()).isEqualTo(REFERENCE_END);
      assertThat(entity.getDataFineVariazione()).isEqualTo(REFERENCE_END.plus(5, ChronoUnit.DAYS));;
      assertThat(entity.getStato()).isEqualTo("NON_ATTIVO");
      assertThat(entity.getDispositivoDmlUniqueIdentifier()).isEqualTo(DEVICE_ID1);
    }
    assertThat(entityToPersistServizio.size()).isEqualTo(i);

    var ac = ArgumentCaptor.forClass(StoricoStatoServizio.class);
    verify(repoServizi,times(2)).delete(ac.capture());
  }

  public static <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }
}
