package it.fai.ms.consumi.repository.fatturazione;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = { "it.fai.ms.consumi.domain.fatturazione", "it.fai.ms.consumi.repository.fatturazione" })
@DataJpaTest
@Tag("unit")
class AllegatiConfigurationRepositoryImplTest {

  @Autowired
  private EntityManager entityManager;

  private AllegatiConfigurationRepositoryImpl repo;
  private AllegatiConfigurationEntity         newAllegatiConfig;

  @BeforeEach
  public void setUp() {

    newAllegatiConfig = new AllegatiConfigurationEntity();
    newAllegatiConfig.setCodiceRaggruppamentoArticolo("AUT_CH");
    newAllegatiConfig.setDescrizione("Descrizione");
    newAllegatiConfig.setTipoDettaglioStanziamento(DettaglioStanziamentoType.PEDAGGIO);
    newAllegatiConfig.setGeneraAllegato(true);

    repo = new AllegatiConfigurationRepositoryImpl(entityManager);
  }

  @Test
  void testFindByCondiceRaggruppamentoArticoli() {

    entityManager.persist(newAllegatiConfig);

    var allegatiConfig1 = repo.findByCodiceRaggruppamentoArticoli("AUT_CH");
    assertThat(allegatiConfig1).isNotNull();

    var allegatiConfig2 = repo.findByCodiceRaggruppamentoArticoli("::TEST::");
    assertThat(allegatiConfig2).isNull();
  }

}
