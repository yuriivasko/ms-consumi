package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.sitaf;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.internal.verification.VerificationModeFactory.noMoreInteractions;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.RecordRepository;
import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.FrejusJob;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler", "it.fai.ms.consumi.job" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.job\\..*\\..*"),

               })
@ActiveProfiles(profiles = "integration")
@EnableFeignClients(basePackageClasses = { ElasticSearchFeignClient.class })
@Tag("integration")
// @Disabled
public class FrejusJobIntTest extends AbstractIntegrationTestSpringContext {

  private static final String fileName = "20180831_T4001036.188";
  private Path                filePath;

  @SpyBean
  private RecordRepository recordRepo;

  @Autowired
  private ElasticSearchFeignClient esClient;

  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private NotificationService notificationService;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @Autowired
  private FrejusJob job;

  @Autowired
  private ApplicationProperties applicationProperties;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @BeforeEach
  public void init() throws Exception {
    filePath = Paths.get(FrejusJobIntTest.class.getResource("/test-files/sitaf/").toURI());
    assertThat(job).isNotNull();

    applicationProperties.getFrejusJobProperties()
      .setFatturazioniFolder(filePath.resolve("fatturazioni").toAbsolutePath().toString());
    applicationProperties.getFrejusJobProperties()
      .setPrenotazioniFolder(filePath.resolve("prenotazioni").toAbsolutePath().toString());
  }

  @Test
  public void elaborate3Records_generate_3_stanziamenti_3_dettagli_stanziamento() {

    ServicePartner serviceProvider = newServicePartner("FJIT");

    String result = job.process(filePath.resolve(fileName).toAbsolutePath().toString(), System.currentTimeMillis(),
                                new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull().isEqualTo("SITAF");

    // verifyZeroInteractions(notificationService);

    // verify messages
    @SuppressWarnings("unchecked")
    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);
    verify(stanziamentiToNavJmsPublisher, times(1)).publish(argumentCaptor.capture());

    argumentCaptor.getValue().stream().forEach(sm -> System.out.println(sm));
    List<StanziamentoMessage> messages = argumentCaptor.getValue();
    // assertThat(messages).hasSize(3);

    assertThat(messages)
      .extracting(m -> tuple(m.getCosto(), m.getPrezzo(), m.getValuta(), m.getPaese(), m.getNrArticolo(), m.getNrFornitore(),
                             m.getDataErogazioneServizio(), m.getGeneraStanziamento(), m.getNr(), m.getNrCliente(),
                             m.getQuantita(), m.getStatoStanziamento(), m.getTipoFlusso(), m.getClasseEuroVeicolo(), m.getTarga(),
                             m.getAnnoStanziamento()))
      .containsExactlyInAnyOrder(tuple(215.58333, 215.58333, "EUR", "FR", "TFJPEFR", "F007039", LocalDate.parse("2018-08-22"), 1,
                                       1, "0041015", 1.0, 2, 1, "4", "FA903CM", 2018),
                                 tuple(182.25, 182.25, "EUR", "FR", "TFJPEFR", "F007039", LocalDate.parse("2018-08-23"), 1, 1,
                                       "0041015", 1.0, 2, 1, "4", "FA903CM", 2018),
                                 tuple(212.04167, 212.04167, "EUR", "FR", "TFJPEFR", "F007039", LocalDate.parse("2018-07-09"), 1,
                                       1, "100587", 1.0, 2, 1, "4", "DA010HE", 2018));

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(3);
    assertThat(dettaglioStanziamenti).extracting(ds -> (DettaglioStanziamentoPedaggioEntity) ds)
      .extracting(dsp -> tuple(dsp.getDevicePan(), dsp.getDeviceObu(), dsp.getDeviceType()))
      .containsExactlyInAnyOrder(tuple(null, "3081344001036599512", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO),
                                 tuple(null, "3081344001036599512", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO),
                                 tuple(null, "500205214042", TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_FR));

    // aggiorna stato a fatturato per un buono
    verify(changeStatoPublisher, times(1)).sendFatturato(any(), any());

    validateEsSerialize();

    Mockito.validateMockitoUsage();
  }

  @Test
  public void testPrenotazioneDispositivo() {

    ServicePartner serviceProvider = newServicePartner("FJIT");

    var result = job.process(filePath.resolve("20180831_T1000867.189").toAbsolutePath().toString(), System.currentTimeMillis(),
                             new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull().isEqualTo("SITAF");

    verifyZeroInteractions(notificationService);

    // verify messages
    @SuppressWarnings("unchecked")
    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);

    verify(stanziamentiToNavJmsPublisher, times(1)).publish(argumentCaptor.capture());

    argumentCaptor.getValue().stream().forEach(sm -> System.out.println(sm));
    List<StanziamentoMessage> messages = argumentCaptor.getValue();
    assertThat(messages).hasSize(1);

    StanziamentoMessage expected1 = new StanziamentoMessage("");
    expected1.setCosto(212.04918);
    expected1.setPaese("IT");
    expected1.setDataErogazioneServizio(LocalDate.parse("2018-08-31"));
    expected1.setGeneraStanziamento(1);
    expected1.setNr(1);
    expected1.setNrArticolo("TMBPE01");
    expected1.setNrCliente("130046576");
    expected1.setNrFornitore("F000441");
    expected1.setPrezzo(212.04918);
    expected1.setQuantita(1);
    expected1.setStatoStanziamento(2);
    expected1.setTipoFlusso(1);
    expected1.setValuta("EUR");
    expected1.setClasseEuroVeicolo("4");
    expected1.setTarga("FF409AG");
    expected1.setAnnoStanziamento(2018);

    StanziamentoMessage message1 = messages.get(0);
    assertThat(message1).isEqualToIgnoringGivenFields(expected1, "code", "dataAcquisizioneFlusso", "nazioneTarga");

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(1);
    assertThat(dettaglioStanziamenti).extracting(ds -> (DettaglioStanziamentoPedaggioEntity) ds)
      .extracting(dsp -> tuple(dsp.getDevicePan(), dsp.getDeviceObu()))
      .containsExactlyInAnyOrder(tuple(null, null));
    verify(changeStatoPublisher, noMoreInteractions()).sendFatturato(any(), any());
    validateEsSerialize();
    Mockito.validateMockitoUsage();
  }

  @Test
  public void testPrenotazioneDispositivoNonPrenotato() {

    ServicePartner serviceProvider = newServicePartner("MBIT");

    var result = job.process(filePath.resolve("20180831_T4001037.188").toAbsolutePath().toString(), System.currentTimeMillis(),
                             new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull().isEqualTo("SITAF");

    verifyZeroInteractions(stanziamentiToNavJmsPublisher);

//    var                                 code   = ArgumentCaptor.forClass(String.class);
    @SuppressWarnings("unchecked")
    ArgumentCaptor<EnrichedNotification> notificationArgumentCaptor = ArgumentCaptor.forClass(EnrichedNotification.class);
    verify(notificationService).notify(notificationArgumentCaptor.capture());

    assertThat(notificationArgumentCaptor.getValue().getCode()).isEqualTo("CPV-b007");
    assertThat(notificationArgumentCaptor.getValue().transformFiledsToParameters()).containsEntry("original_message", "Customer not found: null");

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(0);

    verifyNoMoreInteractions(notificationService);
    verify(changeStatoPublisher, noMoreInteractions()).sendFatturato(any(), any());
    validateEsSerialize();
    Mockito.validateMockitoUsage();
  }

  public ServicePartner newServicePartner(String codFornitoreLegacy) {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode(codFornitoreLegacy);
    serviceProvider.setPartnerName("FRIT");
    serviceProvider.setFormat(Format.SITAF);
    return serviceProvider;
  }

  @Test
  public void testPrenotazione_imponibileadr() throws URISyntaxException {

    filePath = Paths.get(FrejusJobIntTest.class.getResource("/test-files/sitaf/").toURI());
    assertThat(job).isNotNull();

    applicationProperties.getFrejusJobProperties()
      .setFatturazioniFolder(filePath.resolve("imponibile_adr_fatturazioni").toAbsolutePath().toString());
    applicationProperties.getFrejusJobProperties()
      .setPrenotazioniFolder(filePath.resolve("imponibile_adr_prenotazioni").toAbsolutePath().toString());

    ServicePartner serviceProvider = newServicePartner("MBIT");

    var result = job.process(filePath.resolve("T4001036.18c_imponibile_adr").toAbsolutePath().toString(),
                             System.currentTimeMillis(), new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull().isEqualTo("SITAF");

    // verifyZeroInteractions(notificationService);

    @SuppressWarnings("unchecked")
    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);
    verify(stanziamentiToNavJmsPublisher, times(1)).publish(argumentCaptor.capture());

    argumentCaptor.getValue().stream().forEach(sm -> System.out.println(sm));
    List<StanziamentoMessage> messages = argumentCaptor.getValue();
    assertThat(108.77 + 212.04918).isEqualByComparingTo(messages.get(0).getPrezzo());

    assertThat((108.77 + 212.04918) * 2).isEqualByComparingTo(messages.get(1).getPrezzo());

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(3);
    assertThat(dettaglioStanziamenti).extracting(ds -> (DettaglioStanziamentoPedaggioEntity) ds)
      .extracting(dsp -> tuple(dsp.getDevicePan(), dsp.getDeviceObu(), dsp.getDeviceType(), dsp.getAmountNoVat(),
                               dsp.getAdrTaxableAmount()))
      .containsExactlyInAnyOrder(tuple(null, "3081184009010003172", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO,
                                       new BigDecimal("212.04918"), new BigDecimal("108.77000")),
                                 tuple(null, "3081184009010003172", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO,
                                       new BigDecimal("212.04918"), new BigDecimal("108.77000")),
                                 tuple(null, "3081184009010003172", TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO,
                                       new BigDecimal("212.04918"), new BigDecimal("108.77000")));

    verify(changeStatoPublisher, noMoreInteractions()).sendFatturato(any(), any());
    validateEsSerialize();
    Mockito.validateMockitoUsage();
  }

  private void validateEsSerialize() {
    ArgumentCaptor<BaseRecord> record = ArgumentCaptor.forClass(BaseRecord.class);

    verify(recordRepo, atLeastOnce()).persist(record.capture());
    // create(Mockito.anyLong(), Mockito.anyString(), record.capture());
    record.getAllValues().forEach(r -> {
      var esr = esClient.getRecord(r.getClass().getSimpleName(), r.getUuid());
      assertThat(esr.getRawdata()).isEqualToComparingFieldByFieldRecursively(r);
    });
  }

}
