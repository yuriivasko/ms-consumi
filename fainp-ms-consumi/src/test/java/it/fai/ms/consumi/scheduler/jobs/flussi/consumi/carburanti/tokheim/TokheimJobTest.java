package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0201;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0301;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0302;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.tokheim.TokheimProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.carburanti.TokheimRecordConsumoMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.sitaf.FrejusJobIntTest;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.transaction.PlatformTransactionManager;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@Tag("unit")
class TokheimJobTest {

  private PlatformTransactionManager   transactionManager           = mock(PlatformTransactionManager.class);
  private StanziamentiParamsValidator  stanziamentiParamsValidator  = mock(StanziamentiParamsValidator.class);
  private RecordPersistenceService     persistenceService           = mock(RecordPersistenceService.class);
  private TokheimRecordDescriptor      recordDescriptor             = new TokheimRecordDescriptor();
  private TokheimProcessor             consumoGenericoProcessor     = mock(TokheimProcessor.class);
  private TokheimRecordConsumoMapper   recordConsumoMapper          = mock(TokheimRecordConsumoMapper.class);
  private StanziamentiToNavPublisher stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
  private NotificationService          notificationService          = mock(NotificationService.class);

  @Test
  void testProcess() throws Exception {
    TokheimJob subjet = new TokheimJob(transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, consumoGenericoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);

    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(new ValidationOutcome());

    ServicePartner servicePartner = new ServicePartner("id");

    Path filePath = Paths.get(FrejusJobIntTest.class.getResource("/test-files/tokheim/")
                         .toURI());

    String fileName = "00012921.T.20170415091001";
    subjet.process(filePath.resolve(fileName)
                   .toAbsolutePath()
                   .toString(), 0, Instant.EPOCH, servicePartner);

    var acRecord = ArgumentCaptor.forClass(TokheimRecord.class);
    verify(recordConsumoMapper,Mockito.atLeastOnce()).mapRecordToConsumo(acRecord.capture());
    List<TokheimRecord> records = acRecord.getAllValues();
    assertThat(records).allSatisfy(r->{
      assertThat(r).isInstanceOfSatisfying(TokheimRecord0302.class, rt302 ->{
        assertThat(rt302.getRecordCode()).isEqualTo("0302");
        assertThat(rt302.getParentRecord()).isInstanceOfSatisfying(TokheimRecord0301.class, rt301->{
          assertThat(rt301.getRecordCode()).isEqualTo("0301");
          assertThat(rt301.getParentRecord()).isInstanceOfSatisfying(TokheimRecord0201.class, rt201->{
            assertThat(rt201.getRecordCode()).isEqualTo("0201");
            assertThat(rt201.getParentRecord()).isNull();
          });
        });
      });
    });
  }

  @SafeVarargs
  public static <T> T init(T obj, Consumer<T>... consumers) {
    for (Consumer<T> consumer : consumers) {
      consumer.accept(obj);
    }
    return obj;
  }

}
