package it.fai.ms.consumi.service.record;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.service.record.telepass.ElceuRecordConsumoMapper;

@Tag("unit")
class AbstractRecordConsumoMapperTest {

  @Test
  void testCalculateInstantByDateAndTime() throws ParseException {

    var mapper = new ElceuRecordConsumoMapper();

    String date = "99991230";
    String time = "235900";

    var dateAndTime = mapper.calculateInstantByDateAndTime(date, time, "yyyyMMdd", "HHmmss");

    assertThat(dateAndTime).isPresent();

  }

}
