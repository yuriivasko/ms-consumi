package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import java.time.Instant;
import java.util.Optional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.repository.flussi.record.model.DummyRecord;

public class ElceuTestBuilder {

  public static ElceuTestBuilder newInstance() {
    return new ElceuTestBuilder();
  }

  private Elceu elceu;

  private ElceuTestBuilder() {
    elceu = new Elceu(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.of(new ElceuGlobalIdentifier("::globalIdentifier::")), new DummyRecord());
  }

  public Elceu build() {
    return elceu;
  }

  public ElceuTestBuilder withDevice() {
    elceu.setDevice(new Device("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO));
    elceu.getDevice()
         .setPan("::pan::");
    return this;
  }

  public ElceuTestBuilder withNavSupplier() {
    elceu.setNavSupplierCode("::codfornitorenav::");
    return this;

  }

  public ElceuTestBuilder withTollPointExit() {
    elceu.setTollPointExit(new TollPoint());
    elceu.getTollPointExit()
         .setTime(Instant.ofEpochMilli(666));
    elceu.getTollPointExit()
         .setGlobalGate(new GlobalGate("::globalGate::"));
    elceu.getTollPointExit()
         .getGlobalGate()
         .setDescription("::description::");
    return this;
  }

  public ElceuTestBuilder withTransaction() {
    elceu.setTransaction(new Transaction());
    elceu.getTransaction()
         .setDetailCode("::detailCode::");
    elceu.getTransaction()
         .setSign("::sign::");
    return this;
  }

}
