package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.sanbernardo;

import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.COSTO_RICAVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.SanBernardoJob;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@ComponentScan(
  basePackages = {
    "it.fai.ms.consumi.service",
    "it.fai.ms.consumi.client",
    "it.fai.ms.consumi.adapter",
    "it.fai.ms.consumi.repository",
    "it.fai.ms.consumi.scheduler",
    "it.fai.ms.consumi.domain.dettaglio_stanziamento",
    "it.fai.ms.consumi.service.navision"
  },
  excludeFilters = {
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"),
  }
)
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class SanBernardoJobIntTest extends AbstractIntegrationTestSpringContext {

  private String fileName = "20180531_elenco_viaggi_CM.csv";
  private String filePath = "";

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentiRepository;


  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @MockBean
  private PriceCalculatedByNavService priceCalculatedByNavService;

  @MockBean
  private NotificationService notificationService;

  @Autowired
  private SanBernardoJob job;

  @Autowired
  private StanziamentoRepository stanziamentoRepository;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @BeforeEach
  public void init() throws Exception {
    filePath = new File(SanBernardoJobIntTest.class.getResource("/test-files/sanbernardo/" + fileName).toURI()).getAbsolutePath();
    assertThat(job).isNotNull();

//        debug
//        dettaglioStanziamentiRepository
//                .findAllStorico()
//                .stream()
//                .filter(s->"SA903CM".equals(s.getVeicoloTarga()) || "NA903CM".equals(s.getVeicoloTarga()))
//                .forEach(s->System.out.println(s.getVeicoloTarga()+" > "+s.getContrattoNumero()+" "+s.getDispositivoDmlUniqueIdentifier()+" "+s.getSerialeDispositivo()+" "+s.getTipoDispositivo()));

  }

  @Test
  public void elaborateFile() {
    when(priceCalculatedByNavService.getCalculatedPriceByNav(any())).thenReturn(new BigDecimal(10d));
    doAnswer(p -> {
        for (Object o : p.getArguments()) {
          System.err.println(o);
        }
        return null;
      }
    ).when(notificationService).notify(any(), any());

    final ServicePartner servicePartner = newServicePartner();

    String result = job.process(filePath, System.currentTimeMillis(), new Date().toInstant(), servicePartner);
    ArgumentCaptor<EnrichedNotification> notificationArgCaptor = ArgumentCaptor.forClass(EnrichedNotification.class);

    //validation failed on san bernardo buoni that doesn't match the customer id with the one found in elenco_ordini and elenco_buoni
    verify(notificationService, atLeast(1))
      .notify(notificationArgCaptor.capture());

    List<EnrichedNotification> allValues = notificationArgCaptor.getAllValues();
    assertThat(allValues)
      .extracting(notificationArg -> {
        Map map = notificationArg.transformFiledsToParameters();
        return tuple(map.get("job_name"), map.get("original_message"));
      })
      .containsExactlyInAnyOrder(
        tuple("SAN_BERNARDO", "Device BUONI_GRAN_SAN_BERNARDO with pan/service 516343001060/PEDAGGI_GRAN_SAN_BERNARDO not found")
      );
//        assertThat(notificationArgCaptor.getValue().get("job_name")).isEqualTo("SAN_BERNARDO");
//        assertThat(notificationArgCaptor.getValue().get("original_message")).isEqualTo("error in  1 rows index 6 : Customer not found for device 516343001060");
    assertThat(result).isNotNull().isEqualTo("SAN_BERNARDO");
    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);
    verify(stanziamentiToNavJmsPublisher).publish(argumentCaptor.capture());
    List<StanziamentoMessage> expectedMessages = argumentCaptor.getValue();
    assertThat(expectedMessages)
      .hasSize(5)
      .extracting(sm -> tuple(sm.getTarga(), sm.getPaese(), sm.getCosto(), sm.getPrezzo(), sm.getValuta(), sm.getTipoFlusso(), sm.getGeneraStanziamento(), sm.getDataErogazioneServizio()))
      .containsExactlyInAnyOrder(
        tuple("FR003HE", "IT", 10d, 10d, "EUR", 1, 1, LocalDate.parse("2018-08-31")),
        tuple("NA903CM", "IT", 10d, 10d, "EUR", 1, 1, LocalDate.parse("2018-05-31")),
        tuple("FR003HE", "IT", 10d, 10d, "EUR", 1, 1, LocalDate.parse("2018-08-02")),
        tuple("BA002CM", "IT", 10d, 10d, "EUR", 1, 1, LocalDate.parse("2018-05-05")),
        tuple("BA003CM", "IT", 10d, 10d, "EUR", 1, 1, LocalDate.parse("2018-05-07"))
      );

    List<StanziamentoEntity> stanziamentoEntityList = dettaglioStanziamentiRepository.findAllStanziamenti();
    assertThat(stanziamentoEntityList)
      .hasSize(5)
      .extracting(s -> tuple(
        s.getNumeroCliente(),
        s.getTarga(),
        s.getPaese(),
        s.getCosto().doubleValue(),
        s.getPrezzo().doubleValue(),
        s.getValuta(),
        s.getTipoFlusso(),
        s.getGeneraStanziamento(),
        s.getStatoStanziamento(),
        s.getDataErogazioneServizio()))
      .containsExactlyInAnyOrder(
        tuple("1141015", "FR003HE", "IT", 10d, 10d, "EUR", TypeFlow.MYFAI, COSTO_RICAVO, InvoiceType.D, LocalDate.parse("2018-08-31")),
        tuple("1141017", "NA903CM", "IT", 10d, 10d, "EUR", TypeFlow.MYFAI, COSTO_RICAVO, InvoiceType.D, LocalDate.parse("2018-05-31")),
        tuple("1141015", "FR003HE", "IT", 10d, 10d, "EUR", TypeFlow.MYFAI, COSTO_RICAVO, InvoiceType.D, LocalDate.parse("2018-08-02")),
        tuple("0041015", "BA002CM", "IT", 10d, 10d, "EUR", TypeFlow.MYFAI, COSTO_RICAVO, InvoiceType.D, LocalDate.parse("2018-05-05")),
        tuple("0041015", "BA003CM", "IT", 10d, 10d, "EUR", TypeFlow.MYFAI, COSTO_RICAVO, InvoiceType.D, LocalDate.parse("2018-05-07"))
      );
    assertThat(stanziamentoEntityList.stream().flatMap(s -> s.getDettaglioStanziamenti().stream())
      .collect(Collectors.toList()))
      .hasSize(5)
      .extracting(ds -> {
        DettaglioStanziamentoPedaggioEntity dsp = (DettaglioStanziamentoPedaggioEntity) ds;
        return tuple(
          dsp.getVehicleLicenseLicenseId(),
          dsp.getVehicleLicenseCountryId(),
          dsp.getAmountNoVat().doubleValue(),
          dsp.getAmountIncludingVat().doubleValue(),
          dsp.getAmountVatRate().doubleValue(),
          dsp.getVehicleEuroClass(),
          dsp.getDevicePan(),
          dsp.getDeviceObu(),
          dsp.getDeviceType(),
          dsp.getArticlesGroup());
      }).containsExactlyInAnyOrder(
      tuple("FR003HE", "IT", 10d, 10d, 0d, "EURO6", "9756934101999003952", "9756934101999003952", TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO, "TNL_GRANSB"),
      tuple("NA903CM", "RO", 10d, 10d, 0d, "5", "9756934101999000933", "9756934101999000933", TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO, "TNL_GRANSB"),
      tuple("FR003HE", "IT", 10d, 10d, 0d, "EURO6", "9756934101999003952", "9756934101999003952", TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO, "TNL_GRANSB"),
      tuple("BA002CM", "IT", 10d, 10d, 0d, "5", "516343001058", "516343001058", TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO, "TNL_GRANSB"),
      tuple("BA003CM", "IT", 10d, 10d, 0d, "5", "513458542745", "513458542745", TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO, "TNL_GRANSB")
    );
    verify(changeStatoPublisher, times(2)).sendFatturato(any(), any());

  }

  public ServicePartner newServicePartner() {
    final ServicePartner servicePartner = new ServicePartner("");
    servicePartner.setFormat(Format.SAN_BERNARDO);
    servicePartner.setPartnerCode("");
    return servicePartner;
  }

}
