package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.persistence.EntityManager;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.CarburantiDetailDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.ReportCarburantiDaFatturare;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.ViewReportConsumiCarburanti;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiDetailsTypeEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsArticleEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsCostoRicavoEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsSupplierEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteJmsProducer;
import it.fai.ms.consumi.service.report.ReportCsv;

@ExtendWith(SpringExtension.class)
// @ComponentScan(basePackages = { "it.fai.ms.consumi.repository.dettaglio_stanziamento",
// "it.fai.ms.consumi.repository.stanziamento" })
@ComponentScan(basePackages = { "it.fai.ms.consumi.util" })
@DataJpaTest(showSql = false)
@EntityScan(basePackages = { "it.fai.ms.consumi", "it.fai.common.notification.domain", "it.fai.common.jms","it.fai.ms.consumi.repository.dettaglio_stanziamento" })
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.common.notification.repository" })
@ActiveProfiles(profiles = { "default", "swagger" })
@Tag("unit")
class ReportCarburantiDaFatturareTest {

  @MockBean
  private DettaglioCarburanteJmsProducer dettaglioCarburanteJmsProducer;

  @MockBean
  private PetrolPumpService petrolPumpService;

  @Autowired
  private EntityManager em;

  ReportCarburantiDaFatturare subject;
  String                      codiceClienteNav = "cli1";

  public static final String BASE_PATH = "i18n/";
  public static final String REPORT_MESSAGES = "reportMessages";


  private ReportCsv reportJson = new ReportCsv(resourceBundleMessageSource());

  static MessageSource resourceBundleMessageSource(){
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasename(BASE_PATH + REPORT_MESSAGES);
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }

  @SuppressWarnings("static-access")
  @BeforeEach
  private void createSubject() {
    when(petrolPumpService.getPoint("AT00001")).thenReturn(init(new PointDTO(), p->{p.setDescription("descr AT00001");}));
    subject = new ReportCarburantiDaFatturare(em,petrolPumpService);
  }





  public class TestAssert extends AbstractAssert<TestAssert, DispositiviDto> {

    public TestAssert(DispositiviDto actual) {
      super(actual, TestAssert.class);
    }

  }

  @Test
  void testCsvGeneration(){
    String obu = "obu1";
    String codiceTrackycard = obu;
    String licencePlate = "targa1";
    String licensePlateCountry = "IT";
    Instant date = LocalDateTime.parse("10/11/2018 00:00:00",
      DateTimeFormatter.ofPattern( "dd/MM/yyyy HH:mm:ss" , Locale.getDefault()))
      .atZone(ZoneId.of( "Europe/Rome" )).toInstant();
    Instant dataFattura = date;
    LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault()).toLocalDate().minus(30,ChronoUnit.DAYS);

    {
      ViewReportConsumiCarburanti entity = createViewEntity(codiceClienteNav, "1000",  dataFattura,
        dataErogazioneServizio, codiceTrackycard, null, "5", licensePlateCountry, licencePlate,
        "DIESEL", date.minus(1,ChronoUnit.DAYS), ":erogatore:", "LT", new BigDecimal("12.00000"));
      em.persist(entity);
    }
    {
      ViewReportConsumiCarburanti entity = createViewEntity(codiceClienteNav, "1001",
        date.minus(1,ChronoUnit.DAYS),
        date.atZone(ZoneId.systemDefault()).toLocalDate().minus(1,ChronoUnit.DAYS),
        "obu2", null, "5",
        "IT", "targa2",
        "DIESEL",
        date.minus(2,ChronoUnit.DAYS), ":erogatore2:",
        "LT",
        new BigDecimal("10.00000"));
      em.persist(entity);
    }
    {
      ViewReportConsumiCarburanti entity = createViewEntity(codiceClienteNav, "1002",
        date.minus(45,ChronoUnit.DAYS),
        date.atZone(ZoneId.systemDefault()).toLocalDate().minus(2,ChronoUnit.DAYS),
        "obu3", null, "5",
        "IT", "targa2",
        "DIESEL",
        date.minus(3,ChronoUnit.DAYS), ":erogatore2:",
        "LT",
        new BigDecimal("4.00000"));
      em.persist(entity);
    }


    LocalDate invoiceDateFrom = date.atZone(ZoneId.systemDefault()).toLocalDate().minus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateTo = date.atZone(ZoneId.systemDefault()).toLocalDate().plus(10, ChronoUnit.DAYS);
    DispositiviDto dispositivo = init(new DispositiviDto(), d -> {
      d.setCodiceClienteNav(codiceClienteNav);
      d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
      d.setLicensePlate(licencePlate);
      d.setLicensePlateCountry(licensePlateCountry);
      d.setObu(obu);
    });
    List<CarburantiDetailDto> consumi = subject.getDetail(dispositivo,codiceClienteNav,true,false,null,null, invoiceDateFrom, invoiceDateTo);

    List<CarburantiDetailDto> response = new ArrayList<>();
    response.addAll(consumi);



    ZoneId zoneId = ZoneId.systemDefault();
    Locale locale = Locale.getDefault();
    String json = reportJson.toCsv(CarburantiDetailDto.class, response, locale, zoneId);
    System.out.println(json);


  }

  @Test
  void testGetDispositivi_invoiced_consumption_date() {
    LocalDate consumptionDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";
    String codiceTrackycard = "";
    Instant dataFattura = Instant.now().minus(30,ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now().minus(30,ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente,codiceTrackycard,dataFattura,dataErogazioneServizio,
      (ViewReportConsumiCarburanti entity)->isInInterval(
        entity.getDataErogazioneServizio(),
        consumptionDateFrom,
        consumptionDateTo) && entity.getNumeroFattura() != null);

    List<DispositiviDto> dispositivi = subject.getDispositivi(numeroCliente, true, false, consumptionDateFrom, consumptionDateTo, null, null);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
                           .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));

  }

  @Test
  void testGetDispositivi_not_invoiced_consumption_date() {
    LocalDate consumptionDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";
    String codiceTrackycard = "";

    Instant dataFattura = Instant.now().minus(30,ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now().minus(30,ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, codiceTrackycard, dataFattura, dataErogazioneServizio,
      (ViewReportConsumiCarburanti entity) -> isInInterval(
        entity.getDataErogazioneServizio(),
        consumptionDateFrom,
        consumptionDateTo) && entity.getNumeroFattura() == null);

    List<DispositiviDto> dispositivi = subject.getDispositivi(numeroCliente, false, true, consumptionDateFrom, consumptionDateTo, null, null);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
      .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));

  }

  @Test
  void testGetDispositivi_both_invoiced_consumption_date() {
    LocalDate consumptionDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);



    String numeroCliente = "123456";
    String codiceTrackycard = "";

    Instant dataFattura = Instant.now().minus(30,ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now().minus(30,ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, codiceTrackycard, dataFattura, dataErogazioneServizio,
      (ViewReportConsumiCarburanti entity) -> isInInterval(
        entity.getDataErogazioneServizio(),
        consumptionDateFrom,
        consumptionDateTo) );

    List<DispositiviDto> dispositivi = subject.getDispositivi(numeroCliente, false, false, consumptionDateFrom, consumptionDateTo, null, null);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
      .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));

  }


  @Test
  void testGetDispositivi_invoiced_invoice_date() {
    LocalDate invoiceDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateFrom = invoiceDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";
    String codiceTrackycard = "";

    Instant dataFattura = Instant.now().minus(30,ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now().minus(30,ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, codiceTrackycard, dataFattura, dataErogazioneServizio,
      (ViewReportConsumiCarburanti entity) -> isInInterval(
        entity.getDataFattura(),
        invoiceDateFrom,
        invoiceDateTo) && entity.getNumeroFattura() != null);

    List<DispositiviDto> dispositivi = subject.getDispositivi(numeroCliente, true, false, null,null, invoiceDateFrom, invoiceDateTo);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
      .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));

  }
  @Test
  void testGetDispositivi_not_invoiced_invoice_date() {
    LocalDate invoiceDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateFrom = invoiceDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";
    String codiceTrackycard = "";

    Instant dataFattura = Instant.now().minus(30,ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now().minus(30,ChronoUnit.DAYS);
    List<Tuple> expectedResults = populateTestDataSet(numeroCliente, codiceTrackycard, dataFattura, dataErogazioneServizio,
      (ViewReportConsumiCarburanti entity) -> isInInterval(
        entity.getDataFattura(),
        invoiceDateFrom,
        invoiceDateTo) && entity.getNumeroFattura() == null);
    List<DispositiviDto> dispositivi = subject.getDispositivi(numeroCliente, false, true, null,null, invoiceDateFrom, invoiceDateTo);

    assertThat(dispositivi).isNotNull();
    assertThat(dispositivi).extracting(d -> tuple(d.getLicensePlate(), d.getLicensePlateCountry(), d.getObu()))
      .containsExactlyInAnyOrder(expectedResults.toArray(new Tuple[0]));

  }

  @Test
  void testEmptyGetDetail() {
    LocalDate consumptionDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);

    DispositiviDto dispositivo = init(new DispositiviDto(), d -> {
      d.setCodiceClienteNav(codiceClienteNav);
      d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
    });

    List<CarburantiDetailDto> detail = subject.getDetail(dispositivo,codiceClienteNav,true,false,consumptionDateFrom,consumptionDateTo,null,null);
    assertThat(detail).isEmpty();

  }

  @Test
  void testGetDetail() {
    LocalDate consumptionDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";
    String obu = "obu1";
    String codiceTrackycard = obu;
    Instant dataFattura = Instant.now().minus(30,ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now().minus(15,ChronoUnit.DAYS);
    String licencePlate = "targa1";
    String licensePlateCountry = "IT";

    ViewReportConsumiCarburanti entity = createViewEntity(numeroCliente, "1000", dataFattura,
      dataErogazioneServizio, codiceTrackycard, null, "5", licensePlateCountry, licencePlate,
      "DIESEL", Instant.now(), ":erogatore:", "LT", new BigDecimal("1.00000"));
    em.persist(entity);

    DispositiviDto dispositivo = init(new DispositiviDto(), d -> {
      d.setCodiceClienteNav(numeroCliente);
      d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
      d.setLicensePlate(licencePlate);
      d.setLicensePlateCountry(licensePlateCountry);
      d.setObu(obu);
    });

    List<CarburantiDetailDto> detail = subject.getDetail(dispositivo,numeroCliente,true,false,consumptionDateFrom,consumptionDateTo, null,null);

    assertThat(detail).extracting(d -> tuple(d.getQuantita().unita, d.getQuantita().valore, d.getDescrizioneArticolo(),d.getPuntoErogazione()))
                      .containsExactlyInAnyOrder(tuple("LT", BigDecimal.ONE.setScale(5), "DIESEL","descr AT00001"));

  }
  @Test
  void testGetDetailSkipNonTipoRicavo() {
    //tipo ricavo  -> 'prezzo_unitario_calcolato_noiva is not NULL'
    LocalDate consumptionDateTo = LocalDate.now().minus(10, ChronoUnit.DAYS);
    LocalDate consumptionDateFrom = consumptionDateTo.minus(10, ChronoUnit.DAYS);
    String numeroCliente = "123456";
    String obu = "obu1";
    String codiceTrackycard = obu;
    Instant dataFattura = Instant.now().minus(30,ChronoUnit.DAYS);
    LocalDate dataErogazioneServizio = LocalDate.now().minus(15,ChronoUnit.DAYS);
    String licencePlate = "targa1";
    String licensePlateCountry = "IT";

    ViewReportConsumiCarburanti entity = createViewEntity(numeroCliente, "1000", dataFattura,
                                                          dataErogazioneServizio, codiceTrackycard, null, "5", licensePlateCountry, licencePlate,
                                                          "DIESEL", Instant.now(), ":erogatore:", "LT", new BigDecimal("1.00000"));
    entity.setPrezzoUnitarioCalcolatoNoiva(null);
    em.persist(entity);

    DispositiviDto dispositivo = init(new DispositiviDto(), d -> {
      d.setCodiceClienteNav(numeroCliente);
      d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
      d.setLicensePlate(licencePlate);
      d.setLicensePlateCountry(licensePlateCountry);
      d.setObu(obu);
    });

    List<CarburantiDetailDto> detail = subject.getDetail(dispositivo,numeroCliente,true,false,consumptionDateFrom,consumptionDateTo, null,null);

    assertThat(detail).isEmpty();

  }

  @Test
  public void testGetDetailFatturatiByDispositivoInvoiceDate(){

    String obu = "obu1";
    String codiceTrackycard = obu;
    String licencePlate = "targa1";
    String licensePlateCountry = "IT";
    Instant date = LocalDateTime.parse("10/11/2018 00:00:00",
      DateTimeFormatter.ofPattern( "dd/MM/yyyy HH:mm:ss" , Locale.getDefault()))
      .atZone(ZoneId.of( "Europe/Rome" )).toInstant();
    Instant dataFattura = date;
    LocalDate dataErogazioneServizio = date.atZone(ZoneId.systemDefault()).toLocalDate().minus(30,ChronoUnit.DAYS);

    {
      ViewReportConsumiCarburanti entity = createViewEntity(codiceClienteNav, "1000",  dataFattura,
        dataErogazioneServizio, codiceTrackycard, null, "5", licensePlateCountry, licencePlate,
        "DIESEL", date.minus(1,ChronoUnit.DAYS), ":erogatore:", "LT", new BigDecimal("12.00000"));
      em.persist(entity);
    }
    {
      ViewReportConsumiCarburanti entity = createViewEntity(codiceClienteNav, "1001",
        date.minus(1,ChronoUnit.DAYS),
        date.atZone(ZoneId.systemDefault()).toLocalDate().minus(1,ChronoUnit.DAYS),
        "obu2", null, "5",
        "IT", "targa2",
        "DIESEL",
        date.minus(2,ChronoUnit.DAYS), ":erogatore2:",
        "LT",
        new BigDecimal("10.00000"));
      em.persist(entity);
    }
    {
      ViewReportConsumiCarburanti entity = createViewEntity(codiceClienteNav, "1002",
        date.minus(45,ChronoUnit.DAYS),
        date.atZone(ZoneId.systemDefault()).toLocalDate().minus(2,ChronoUnit.DAYS),
        "obu3", null, "5",
        "IT", "targa2",
        "DIESEL",
        date.minus(3,ChronoUnit.DAYS), ":erogatore2:",
        "LT",
        new BigDecimal("4.00000"));
      em.persist(entity);
    }


    LocalDate invoiceDateFrom = date.atZone(ZoneId.systemDefault()).toLocalDate().minus(10, ChronoUnit.DAYS);
    LocalDate invoiceDateTo = date.atZone(ZoneId.systemDefault()).toLocalDate().plus(10, ChronoUnit.DAYS);
    DispositiviDto dispositivo = init(new DispositiviDto(), d -> {
      d.setCodiceClienteNav(codiceClienteNav);
      d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
      d.setLicensePlate(licencePlate);
      d.setLicensePlateCountry(licensePlateCountry);
      d.setObu(obu);
    });
    List<CarburantiDetailDto> detail = subject.getDetail(dispositivo,codiceClienteNav,true,false,null,null, invoiceDateFrom, invoiceDateTo);
    assertThat(detail)
      .extracting(d -> tuple(
        d.getQuantita().unita, d.getQuantita().valore, d.getDescrizioneArticolo(),d.getDataUtilizzo(),d.getCodiceDispositivo(),d.getPuntoErogazione())
      ).containsExactlyInAnyOrder(
        tuple("LT", new BigDecimal("12.00000"), "DIESEL",date.minus(1,ChronoUnit.DAYS), "obu1","descr AT00001")
      );
  }


  private StanziamentiParamsEntity createStanziamentiParam() {
    return init(new StanziamentiParamsEntity(init(new StanziamentiParamsArticleEntity("F000001"), a -> {
      a.setDescription("DIESEL");
    })), p -> {
      p.setMeasurementUnit("LT");
      p.setSupplier(new StanziamentiParamsSupplierEntity("F007312"));
      p.setRecordCode("D0");
      p.setSource(Format.ELCIT.toString());
      p.setCountry("IT");
      p.setCurrency(Currency.getInstance("EUR"));
      p.setGrouping("group1");
      p.setCostoRicavo(StanziamentiParamsCostoRicavoEntity.CR);
      p.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.CARBURANTI);

    });
  }

  private StanziamentoEntity createStanziamento() {
    StanziamentoEntity stanziamento1 = init(new StanziamentoEntity("s1"), s -> {
      s.setArticleCode("F000001");
      s.getDettaglioStanziamenti()
       .add(init(new DettaglioStanziamentoCarburanteEntity("1"), d -> {
         d.setCustomerId(codiceClienteNav);
         d.setTipoSorgente(Format.ELCIT);
         d.setQuantita(BigDecimal.ONE);
         d.setErogatore("01");
         d.setRecordCode("D0");
         d.setCodiceFornitoreNav("F007312");
         d.setVeicoloTarga("targa1");
         d.setCodiceTrackycard("obu1");
         d.setVeicoloNazioneTarga("IT");
         d.setDataOraUtilizzo(Instant.now());
         d.getStanziamenti()
          .add(s);
       }));
    });
    return stanziamento1;
  }

  private void persistStanziamento(StanziamentoEntity stanziamento1) {
    em.persist(stanziamento1);
    stanziamento1.getDettaglioStanziamenti()
                 .stream()
                 .forEach(ds -> em.persist(ds));
  }

  public static <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }

  private boolean isInInterval(LocalDate target, LocalDate from, LocalDate to){


    boolean result = target.isAfter(from) && (target.isBefore(to));

      result = result || target.atStartOfDay().isEqual(from.atStartOfDay()) || target.atStartOfDay().isEqual(to.atStartOfDay()) ;

    return result;
  }
  private boolean isInInterval(Instant target, LocalDate from, LocalDate to){
    return isInInterval(target.atZone(ZoneId.systemDefault()).toLocalDate(),from,to);
  }

  private ViewReportConsumiCarburanti createViewEntity(String customerId, String numeroFattura, Instant dataFattura, LocalDate dataErogazioneServizio, String codiceTrackycard, String id, String veicoloClasse, String veicoloNazioneTarga, String veicoloTarga, String articoloDescrizione, Instant dataOraUtilizzo, String erogatore, String unitaMisura, BigDecimal quantita){
    ViewReportConsumiCarburanti entity = new ViewReportConsumiCarburanti();
    entity.setCustomerId(customerId);
    entity.setNumeroFattura(numeroFattura);
    entity.setDataFattura(dataFattura);
    entity.setDataErogazioneServizio(dataErogazioneServizio);
    entity.setCodiceTrackycard(codiceTrackycard);
    entity.setId(UUID.randomUUID().toString());
    entity.setVeicoloClasse(veicoloClasse);
    entity.setVeicoloNazioneTarga(veicoloNazioneTarga);
    entity.setVeicoloTarga(veicoloTarga);
    entity.setArticoloDescrizione(articoloDescrizione);
    entity.setDataOraUtilizzo(dataOraUtilizzo);
    entity.setErogatore(erogatore);
    entity.setMeasurementUnit(unitaMisura);
    entity.setQuantita(quantita);
    entity.setPrezzoUnitarioCalcolatoNoiva(BigDecimal.ONE);
    entity.setPuntoErogazione("AT00001");
    return entity;
  }
  private List<Tuple> populateTestDataSet(String numeroCliente, String codiceTrackycard, Instant dataFattura, LocalDate dataErogazioneServizio, Function<ViewReportConsumiCarburanti, Boolean> expectedResult) {
    List<Tuple> tupleResult = new ArrayList<>();
    for(int i = 0; i < 30; i++) {
      String numeroFattura = (i % 2 == 0) ? "123456" : null;
      String id = null;
      String veicoloClasse = "5";
      String veicoloNazioneTarga = "it";
      String veicoloTarga = "aa12" + i;
      ViewReportConsumiCarburanti entity = createViewEntity(numeroCliente, numeroFattura, dataFattura, dataErogazioneServizio, codiceTrackycard, id, veicoloClasse, veicoloNazioneTarga, veicoloTarga, null, null, null, null, null);
      em.persist(entity);
      if (expectedResult.apply(entity)) {
        tupleResult.add(tuple(veicoloTarga, veicoloNazioneTarga, ""));
      }
      dataFattura = dataFattura.plus(1, ChronoUnit.DAYS);
      dataErogazioneServizio = dataErogazioneServizio.plus(1, ChronoUnit.DAYS);
    }
    return tupleResult;
  }
}
