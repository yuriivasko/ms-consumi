package it.fai.ms.consumi.domain;

import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;

public class AllStanziamentiParamsTestBuilder {

  private AllStanziamentiParams allStanziamentiParams;

  private AllStanziamentiParamsTestBuilder() {
  }

  public static AllStanziamentiParamsTestBuilder newInstance() {
    return new AllStanziamentiParamsTestBuilder();
  }

  public AllStanziamentiParams build() {
    allStanziamentiParams = new AllStanziamentiParams();
    allStanziamentiParams.getStanziamentiParams()
                         .add(StanziamentiParamsTestBuilder.newInstance()
                                                           .withSupplierCode("::supplierCode-1::")
                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                          .withCode("::articleCode-1::")
                                                                                          .build())
                                                           .build());
    allStanziamentiParams.getStanziamentiParams()
                         .add(StanziamentiParamsTestBuilder.newInstance()
                                                           .withSupplierCode("::supplierCode-2::")
                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                          .withCode("::articleCode-2::")
                                                                                          .build())
                                                           .build());
    allStanziamentiParams.getStanziamentiParams()
                         .add(StanziamentiParamsTestBuilder.newInstance()
                                                           .withSupplierCode("::supplierCode-3::")
                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                          .withCode("::articleCode-3::")
                                                                                          .build())
                                                           .build());
    return allStanziamentiParams;
  }

}
