package it.fai.ms.consumi.domain;

public class SupplierTestBuilder {

  private Supplier supplier;

  public static SupplierTestBuilder newInstance() {
    return new SupplierTestBuilder();
  }

  public SupplierTestBuilder withCode(final String _code) {
    supplier = new Supplier(_code);
    supplier.setCodeLegacy("::supplierCodeLegacy::");
    return this;
  }

  public Supplier build() {
    return supplier;
  }

  public SupplierTestBuilder withLegacyCode(String legacyCode) {
    if(supplier == null);
    withCode("::supplierCode::");
    supplier.setCodeLegacy(legacyCode);
    return this;
  }

}
