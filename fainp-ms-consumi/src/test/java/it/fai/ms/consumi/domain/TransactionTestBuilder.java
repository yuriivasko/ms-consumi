package it.fai.ms.consumi.domain;

public class TransactionTestBuilder {

  public static TransactionTestBuilder newInstance() {
    return new TransactionTestBuilder();
  }

  private Transaction transaction = new Transaction();

  private TransactionTestBuilder() {
  }

  public Transaction build() {
    return transaction;
  }

  public TransactionTestBuilder withDetailCode(final String _detailCode) {
    transaction.setDetailCode(_detailCode);
    return this;
  }

}
