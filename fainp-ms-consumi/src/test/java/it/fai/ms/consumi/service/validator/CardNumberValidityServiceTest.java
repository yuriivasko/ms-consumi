package it.fai.ms.consumi.service.validator;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit")
class CardNumberValidityServiceTest {

  private String[][] testData =
    {
      {"789697000763300030","7"},
      {"789697000004660051","6"},
      {"789697000390890109","7"},
      //      {"789697000460000296","0"},
      {"789697000460000297","8"},
      {"789697000460000298","6"},
      {"789697000460000299","4"},
      //      {"789697000460000300","0"},
      {"789697000460000301","8"},
      {"789697000460000302","6"},
      {"789697000460000303","4"},
      {"789697000460000304","2"},
      {"789697000460000305","9"},
      {"789697000460000306","7"},
      {"789697000460000307","5"},
      {"789697000460000308","3"},
      {"789697000460000309","1"},
      {"789697000460000310","9"},
      {"789697000767200022","6"},
      {"789697000121920045","6"},
      {"789697000008770099","3"},
      {"789697000008770100","9"},
      {"789697000850200006","3"},
      {"789697000469510003","4"},
      {"789697000469510004","2"},
      {"789697000737200003","9"},
      {"789697000724280013","9"},
      {"789697000724280014","7"},
      {"789697000820940007","9"},
      {"789697000053170027","6"},
      {"789697000053170028","4"},
      {"789697000053170029","2"},
      {"789697000395820007","7"},
      {"789697000663690011","7"},
      {"789697000892330001","9"},
      {"789697000603730001","6"},
      {"789697000603730002","4"},
      {"789697000603730003","2"},
      //      {"789697000603730004","0"},
      {"789697000603730005","7"},
      {"789697000603730006","5"},
      {"789697000603730007","3"},
      {"789697000603730008","1"},
      {"789697000603730009","9"},
      {"789697000710170001","6"},
      {"789697000710170002","4"}
    };

  CardNumberValidityService cardNumberValidityService = new CardNumberValidityService();

  @BeforeEach
  void setUp() {
  }

  @Test
  void hasValidCheckingDigit() {
    for (int i = 0; i < testData.length; i++) {
      boolean ret = cardNumberValidityService.hasValidCheckingDigit(testData[i][0] + testData[i][1]);
      assertThat(ret)
        .isTrue();
    }
  }
}
