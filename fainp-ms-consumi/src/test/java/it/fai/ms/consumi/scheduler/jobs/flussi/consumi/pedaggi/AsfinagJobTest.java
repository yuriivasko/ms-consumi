package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.io.File;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.Asfinag;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.AsfinagRecord;
import it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.asfinag.AsfinagProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.asfinag.AsfinagRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Tag("unit")
class AsfinagJobTest {

  private final AsfinagRecordDescriptor          descriptor                   = new AsfinagRecordDescriptor();

  private final StanziamentiParamsValidator      stanziamentiParamsValidator  = mock(StanziamentiParamsValidator.class);
  private final NotificationService              notificationService          = mock(NotificationService.class);
  private final RecordPersistenceService         recordPersistenceService     = mock(RecordPersistenceService.class);
  private final AsfinagProcessor                 processor                    = mock(AsfinagProcessor.class);
  private final AsfinagRecordConsumoMapper       consumoMapper                = mock(AsfinagRecordConsumoMapper.class);
  private final StanziamentiToNavPublisher     stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
  private final PlatformTransactionManager transactionManager                 = mock(PlatformTransactionManager.class);


  private final ServicePartner servicePartner = mock(ServicePartner.class);
  private final RecordsFileInfo recordsFileInfo = mock(RecordsFileInfo.class);

  private Stanziamento stanziamento;

  private AsfinagJob job;
  private String     fileName = "LEPTCTTR.999.20180227063004";
  private String     filePath = "";

  @BeforeEach
  void setUp() throws Exception {
    filePath = new File(AsfinagJobTest.class.getResource("/test-files/asfinag/" + fileName).toURI()).getAbsolutePath();
    job = new AsfinagJob(transactionManager, stanziamentiParamsValidator, notificationService,
                         recordPersistenceService, descriptor, processor, consumoMapper, stanziamentiToNavJmsProducer);

    given(recordsFileInfo.fileIsValid())
      .willReturn(true);


    stanziamento = new Stanziamento("");
    stanziamento.setTipoFlusso(TypeFlow.NONE);
    stanziamento.setInvoiceType(InvoiceType.D);
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");

    given(stanziamentiParamsValidator.checkConsistency())
      .willThrow(runtimeExTest);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(atLeastOnce())
      .notify(any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_recordPersistenceService_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");
    willThrow(runtimeExTest)
      .given(recordPersistenceService)
      .persist(any());

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(atLeastOnce())
      .notify(any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_fail_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(2))
      .notify(any());
  }

  @Test
  public void process_if_all_ok_will_process_all_detail_rows() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(Asfinag.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(recordPersistenceService)
      .should(times(2))
      .persist(any());

  }

  @Test
  public void process_if_all_ok_will_map_all_rows_to_consumo() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(Asfinag.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);


    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(consumoMapper)
      .should(times(2))
      .mapRecordToConsumo(any());
  }

  @Test
  public void process_if_checks_success_will_send_stanziamenti_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(Asfinag.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(0))
      .notify(any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void process_if_processor_pratially_process_rows_but_throw_an_exception_will_send_notification_and_stanziamento_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(Asfinag.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult)
      .willThrow(new RuntimeException("This is an exception to test behaviour"));

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(1))
      .notify(any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void getJobQualifier() {
    assertThat(job.getJobQualifier())
      .isEqualTo(Format.ASFINAG);
  }

}
