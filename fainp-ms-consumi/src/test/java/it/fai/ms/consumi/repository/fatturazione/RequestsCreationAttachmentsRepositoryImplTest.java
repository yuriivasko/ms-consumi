package it.fai.ms.consumi.repository.fatturazione;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.repository.fatturazione.model.RequestsCreationAttachmentsEntityMapper;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = { "it.fai.ms.consumi.domain.fatturazione", "it.fai.ms.consumi.repository.fatturazione" })
@DataJpaTest
@Tag("unit")
class RequestsCreationAttachmentsRepositoryImplTest {

  @Autowired
  private EntityManager em;

  private RequestsCreationAttachmentsRepository repo;
  private RequestsCreationAttachments           rca;
  private String                                codiceFattura                = "::codice_fattura::";
  private String                                codiceRaggruppamentoArticolo = "::code_raggruppamento_articolo::";

  @BeforeEach
  public void setUp() {
    repo = new RequestsCreationAttachmentsRepositoryImpl(em, new RequestsCreationAttachmentsEntityMapper());
  }

  @AfterEach
  public void destroy() {
    repo.delete(rca.getId());
  }

  @Test
  void testSave() {
    rca = new RequestsCreationAttachments();
    rca.setCodiceFattura(codiceFattura);
    rca.setCodiceRaggruppamentoArticolo(codiceRaggruppamentoArticolo);
    rca.setImportoTotale(new BigDecimal(2.2d));
    rca.setUuidDocumento(RandomStringUtils.random(10));
    rca.setData(Instant.now());

    assertThat(rca.getId()).isNull();
    rca = repo.save(rca);
    assertThat(rca.getId()).isNotNull();
    assertThat(rca.getId()).isGreaterThan(0L);
  }

  @Test
  void testUpdate() {
    testSave();
    String newCodiceFattura = "::codice_fattura2::";
    rca.setCodiceFattura(newCodiceFattura);
    Long id = rca.getId();
    assertThat(id);
    RequestsCreationAttachments saved = repo.save(rca);
    assertThat(saved).isNotNull();
    assertThat(saved.getId()).isNotNull();
    assertThat(saved.getId()).isEqualTo(id);
    assertThat(saved.getCodiceFattura()).isEqualTo(newCodiceFattura);
  }

  @Test
  void testFindByCodiceFatturaAndRaggruppamentoArticolo() {
    testSave();
    RequestsCreationAttachments foundedEntity = repo.findByCodiceFatturaAndCodiceRaggruppamentoArticoli(codiceFattura,
                                                                                                        codiceRaggruppamentoArticolo);
    assertThat(foundedEntity).isNotNull();
    assertThat(foundedEntity.getCodiceFattura()).isEqualTo(codiceFattura);
    assertThat(foundedEntity.getCodiceRaggruppamentoArticolo()).isEqualTo(codiceRaggruppamentoArticolo);
  }

}
