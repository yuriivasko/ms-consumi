package it.fai.ms.consumi.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;

import it.fai.ms.consumi.FaiconsumiApp;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.ReportPedaggiDaFatturare;
import it.fai.ms.consumi.service.async.ReportAsyncService;
import it.fai.ms.consumi.service.jms.producer.report.JmsReportProducer;
import it.fai.ms.consumi.service.report.ReportCsv;

@Tag("integration")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FaiconsumiApp.class)
@DataJpaTest
public class SearchReportPedaggiResourceIntTest {

  @MockBean
  private ReportPedaggiDaFatturare reportPedaggiDaFatturare;

  @MockBean
  private ReportAsyncService reportAsyncService;

  @MockBean
  private JmsReportProducer jmsReportProducer;

  @MockBean
  private ReportCsv reportJson;

  private MockMvc mockMvc;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.initMocks(this);

    SearchReportPedaggiResource resource = new SearchReportPedaggiResource(reportPedaggiDaFatturare, reportAsyncService, jmsReportProducer,
                                                                           reportJson);
    this.mockMvc = MockMvcBuilders.standaloneSetup(resource)
                                  .build();
  }

  @Test
  public void validateRequest_missingTipoServizio_badRequest() throws Exception {
    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi").content(""))
           .andExpect(status().isNotFound());
  }

  @Test
  public void validateRequest_missingQueryParam_badRequest() throws Exception {
    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi/AUT_DE").content(""))
           .andExpect(status().isBadRequest());
  }

  @Test
  public void validateRequest_fatturato_mandatory_badRequest() throws Exception {
    SearchReportFilterDTO body = new SearchReportFilterDTO();
    body.setConsumptionDateFrom(LocalDate.now()
                                         .minus(30, ChronoUnit.DAYS));
    body.setConsumptionDateTo(LocalDate.now());
    body.setInvoiceDateFrom(LocalDate.now()
                                     .minus(40, ChronoUnit.DAYS));
    body.setInvoiceDateTo(LocalDate.now());
    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi/AUT_DE").header("Content-Type", "application/json")
                                                                   .content(new Gson().toJson(body)))
           .andExpect(status().isBadRequest());
  }

  @Test
  public void validateRequest_consumptionDateRange_gtOneYear_badRequest() throws Exception {
    SearchReportFilterDTO body = new SearchReportFilterDTO();
    body.setConsumptionDateFrom(LocalDate.now()
                                         .minus(367, ChronoUnit.DAYS));
    body.setConsumptionDateTo(LocalDate.now());
    body.setInvoiceDateFrom(LocalDate.now()
                                     .minus(10, ChronoUnit.DAYS));
    body.setInvoiceDateTo(LocalDate.now());
    body.setInvoiced(true);
    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi/AUT_DE").header("Content-Type", "application/json")
                                                                   .content(new Gson().toJson(body)))
           .andExpect(status().isBadRequest());
  }

  @Test
  public void validateRequest_consumptionDateRange_mandatory_badRequest() throws Exception {
    SearchReportFilterDTO body = new SearchReportFilterDTO();
    body.setConsumptionDateTo(LocalDate.now());
    body.setInvoiced(true);
    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi/AUT_DE").header("Content-Type", "application/json")
                                                                   .content(new Gson().toJson(body)))
           .andExpect(status().isBadRequest());
  }

  @Test
  public void validateRequest_invoiceDateRange_gtOneYear_badRequest() throws Exception {
    SearchReportFilterDTO body = new SearchReportFilterDTO();
    body.setConsumptionDateFrom(LocalDate.now()
                                         .minus(10, ChronoUnit.DAYS));
    body.setConsumptionDateTo(LocalDate.now());
    body.setInvoiceDateFrom(LocalDate.now()
                                     .minus(367, ChronoUnit.DAYS));
    body.setInvoiceDateTo(LocalDate.now());

    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi/AUT_DE").header("Content-Type", "application/json")
                                                                   .content(new Gson().toJson(body)))
           .andExpect(status().isBadRequest());
  }

  @Test
  public void validateRequest_invoiceDateRange_mandatory_badRequest() throws Exception {
    SearchReportFilterDTO body = new SearchReportFilterDTO();
    body.setInvoiceDateTo(LocalDate.now());
    body.setInvoiced(true);
    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi/AUT_DE").header("Content-Type", "application/json")
                                                                   .content(new Gson().toJson(body)))
           .andExpect(status().isBadRequest());
  }

  @Test
  public void validateRequest_oneRangeIsMandatory_badRequest() throws Exception {
    SearchReportFilterDTO body = new SearchReportFilterDTO();
    body.setInvoiceDateTo(LocalDate.now());
    body.setInvoiced(true);
    mockMvc.perform(post("/api/pedaggi/0073930/dispositivi/AUT_DE").header("Content-Type", "application/json")
                                                                   .content(new Gson().toJson(body)))
           .andExpect(status().isBadRequest());
  }
}
