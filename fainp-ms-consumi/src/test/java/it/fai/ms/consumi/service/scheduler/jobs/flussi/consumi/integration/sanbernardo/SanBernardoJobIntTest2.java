package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.sanbernardo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

import java.io.File;
import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.SanBernardoJob;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler",
                                "it.fai.ms.consumi.domain.dettaglio_stanziamento", "it.fai.ms.consumi.service.navision" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"), })
@ActiveProfiles(profiles = "integration")
@Tag("development")
public class SanBernardoJobIntTest2 extends AbstractIntegrationTestSpringContext {

  private String fileName = "20180930_elenco_viaggi_CM.csv";
  private String filePath = "";

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentiRepository;

  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @MockBean
  private PriceCalculatedByNavService priceCalculatedByNavService;

  @MockBean
  private NotificationService notificationService;

  @Autowired
  private SanBernardoJob job;

  @Autowired
  private StanziamentoRepository stanziamentoRepository;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @BeforeEach
  public void init() throws Exception {
    filePath = new File(SanBernardoJobIntTest2.class.getResource("/test-files/sanbernardo/tmp/" + fileName).toURI()).getAbsolutePath();
    assertThat(job).isNotNull();

    // debug
    // dettaglioStanziamentiRepository
    // .findAllStorico()
    // .stream()
    // .filter(s->"SA903CM".equals(s.getVeicoloTarga()) || "NA903CM".equals(s.getVeicoloTarga()))
    // .forEach(s->System.out.println(s.getVeicoloTarga()+" > "+s.getContrattoNumero()+"
    // "+s.getDispositivoDmlUniqueIdentifier()+" "+s.getSerialeDispositivo()+" "+s.getTipoDispositivo()));

  }

  @Test
  public void elaborateFile() {
    when(priceCalculatedByNavService.getCalculatedPriceByNav(any())).thenReturn(new BigDecimal(10d));

    doAnswer(p -> {
      for (Object o : p.getArguments()) {
        System.err.println(o);
      }
      return null;
    }).when(notificationService).notify(any(), any());

    final ServicePartner servicePartner = newServicePartner();

    String result = job.process(filePath, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    assertThat(result).isNotNull().isEqualTo("SAN_BERNARDO");


  }

  public ServicePartner newServicePartner() {
    final ServicePartner servicePartner = new ServicePartner("");
    servicePartner.setFormat(Format.SAN_BERNARDO);
    servicePartner.setPartnerCode("");
    return servicePartner;
  }

}
