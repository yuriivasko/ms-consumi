package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.io.File;
import java.util.Date;

import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Vacon;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconJob;
import it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.consumi.vacon.VaconProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.VaconRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Tag("unit")
public class VaconJobTest {

  private RecordConsumoMapper<VaconRecord, Vacon> consumoMapper;
  private ConsumoProcessor<Vacon>                 consumoProcessor;
  private VaconDescriptor                         descriptor;
  private String                                  filename;
  private VaconJob                                job;
  private RecordPersistenceService                recordPersistenceService;
  private StanziamentiParamsValidator             stanziamentiParamsValidator;
  private StanziamentiToNavPublisher            stanziamentiToNavJmsProducer;
  private final PlatformTransactionManager transactionManager  = mock(PlatformTransactionManager.class);
  private NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationService;


  @BeforeEach
  public void setUp() throws Exception {
    filename = new File(VaconJobTest.class.getResource("/test-files/vacon/DA06A284.VACON.EXAGE.UXXXXXXX")
                                                          .toURI()).getAbsolutePath();
    recordPersistenceService = mock(RecordPersistenceService.class);
    recordPersistenceService = mock(RecordPersistenceService.class);
    stanziamentiParamsValidator = mock(StanziamentiParamsValidator.class);
    consumoProcessor = mock(VaconProcessor.class);
    notConfirmedTemporaryAllocationService = mock(NotConfirmedTemporaryAllocationsService.class);
    doReturn(mock(ProcessingResult.class)).when(consumoProcessor)
                                          .validateAndProcess(any(), any());
    consumoMapper = new VaconRecordConsumoMapper();
    descriptor = new VaconDescriptor();
    stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
  }

  @Test
  public void test() {
    final ValidationOutcome vo = new ValidationOutcome();
    vo.setValidationOk(true);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(new ValidationOutcome());
    final NotificationService notificationService = mock(NotificationService.class);
    job = new VaconJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService, descriptor,
                       consumoProcessor, consumoMapper, stanziamentiToNavJmsProducer,notConfirmedTemporaryAllocationService);
    assertThat(job.getSource()).isEqualTo("VACON");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);
    verify(notConfirmedTemporaryAllocationService).processSource(eq("elceu"),any());

  }

}
