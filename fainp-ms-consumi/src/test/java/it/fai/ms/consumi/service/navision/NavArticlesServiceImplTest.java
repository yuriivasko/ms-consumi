package it.fai.ms.consumi.service.navision;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.adapter.nav.NavArticlesAdapter;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.mapper.ArticleToStanziamentiParamsMapper;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;

@Tag("unit")
class NavArticlesServiceImplTest {

  private final NavArticlesAdapter                mockNavArticlesAdapter        = mock(NavArticlesAdapter.class);
  private final StanziamentiParamsService         mockStanziamentiParamsService = mock(StanziamentiParamsService.class);
  private final ArticleToStanziamentiParamsMapper mockStanziamentiParamsMapper  = mock(ArticleToStanziamentiParamsMapper.class);

  private NavArticlesService navArticlesService;

  @BeforeEach
  void setUp() throws Exception {
    navArticlesService = new NavArticlesServiceImpl(mockNavArticlesAdapter, mockStanziamentiParamsService, mockStanziamentiParamsMapper, true);
  }

  @Test
  void testGetAllArticles() {

    // given
    final Article article = mock(Article.class);
    given(article.getCode())
        .willReturn("::articleCode::");

    AllStanziamentiParams allStanziamentiParams = mock(AllStanziamentiParams.class);
    given(allStanziamentiParams.getStanziamentiParamsGropupedByArticleCodes())
      .willReturn(new HashMap<>(){{
        put("::articleCode::", Arrays.asList(mock(StanziamentiParams.class)));
      }});
    given(mockNavArticlesAdapter.getAllArticles())
      .willReturn(Arrays.asList(article));

    // when

    navArticlesService.getAllArticlesAndUpdateStanziamentiParams(allStanziamentiParams);

    // then

    then(mockNavArticlesAdapter).should().getAllArticles();
    then(mockNavArticlesAdapter).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsService).should(times(1)).updateStanziamentiParams(any());
    then(mockStanziamentiParamsService).shouldHaveNoMoreInteractions();
  }

}
