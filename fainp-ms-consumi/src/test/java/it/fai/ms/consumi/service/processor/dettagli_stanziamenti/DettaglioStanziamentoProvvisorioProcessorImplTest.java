package it.fai.ms.consumi.service.processor.dettagli_stanziamenti;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoPedaggioTestBuilder;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.pedaggi.StanziamentoProcessorPedaggio;

@Tag("unit")
class DettaglioStanziamentoProvvisorioProcessorImplTest {

  private static DettaglioStanziamento newLastAllocationDetail() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                   .withInvoiceTypeD()
                                                   .build();
  }

  private static DettaglioStanziamento newTempAllocationDetail() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                   .withInvoiceTypeP()
                                                   .build();
  }

  private final DettaglioStanziamantiRepository           mockAllocationDetailRepository = mock(DettaglioStanziamantiRepository.class);
  private       DettaglioStanziamentoProvvisorioProcessor tempAllocationDetailProcessor;
  private       StanziamentoProcessorPedaggio             processor                      = mock(StanziamentoProcessorPedaggio.class);
  private final NotificationService                       notificationService            = mock(NotificationService.class);

  @BeforeEach
  void setUp() throws Exception {
    // TODO
    tempAllocationDetailProcessor = new DettaglioStanziamentoProvvisorioProcessorImpl(mockAllocationDetailRepository, processor,
                                                                                      notificationService);
  }

  @Test
  void when_thereAreBoth_TempAllocationDetail_and_LastAllocationDetail() {

    // given

    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(Set.of(newTempAllocationDetail(),
                                                                                                  newLastAllocationDetail()));

    // when

    final var optionalAllocationDetail = tempAllocationDetailProcessor.process(newTempAllocationDetail(), null);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).shouldHaveNoMoreInteractions();

    then(notificationService).should()
                             .notify(any(), any());
    then(notificationService).shouldHaveNoMoreInteractions();

    assertThat(optionalAllocationDetail).isEmpty();
  }

  @Test
  void when_thereArent_allocationDetails() {

    // given

    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(emptySet());

    // when

    final var optionalAllocationDetail = tempAllocationDetailProcessor.process(newTempAllocationDetail(), null);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).should()
                                        .save(newTempAllocationDetail());
    then(mockAllocationDetailRepository).shouldHaveNoMoreInteractions();

    then(notificationService).shouldHaveZeroInteractions();


    assertThat(optionalAllocationDetail).isPresent();
  }

  @Test
  void when_thereIs_LastAllocationDetail() {

    // given

    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(Set.of(newLastAllocationDetail()));

    // when

    final var optionalAllocationDetail = tempAllocationDetailProcessor.process(newTempAllocationDetail(), null);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).shouldHaveNoMoreInteractions();

    then(notificationService).should()
                             .notify(any(), any());
    then(notificationService).shouldHaveNoMoreInteractions();

    assertThat(optionalAllocationDetail).isEmpty();
  }

  @Test
  void when_thereIs_TempAllocationDetail() {

    // given

    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(Set.of(newTempAllocationDetail()));

    // when

    final var optionalAllocationDetail = tempAllocationDetailProcessor.process(newTempAllocationDetail(), null);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).shouldHaveNoMoreInteractions();

    then(notificationService).shouldHaveZeroInteractions();


    assertThat(optionalAllocationDetail).isEmpty();
  }

}
