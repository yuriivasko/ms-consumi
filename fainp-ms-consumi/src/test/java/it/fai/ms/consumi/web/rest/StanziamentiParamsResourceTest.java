package it.fai.ms.consumi.web.rest;

import it.fai.ms.consumi.FaiconsumiApp;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepositoryImpl;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.EntityManager;
import java.util.Currency;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("integration")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FaiconsumiApp.class)
@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class StanziamentiParamsResourceTest {

  @Autowired
  private EntityManager entityManager;

  private StanziamentiParamsRepository stanziamentiParamsRepository;

  private MockMvc mockMvc;

  @BeforeEach
  public void setUp(){
    StanziamentiParamsArticleEntity stanziamentiParamsArticleEntity = new StanziamentiParamsArticleEntity("testCode");
    stanziamentiParamsArticleEntity.setDescription("testDescrizione");

    StanziamentiParamsEntity stanziamentiParamsEntity = new StanziamentiParamsEntity(
      stanziamentiParamsArticleEntity
    );
    stanziamentiParamsEntity.setCountry("c");
    stanziamentiParamsEntity.setGrouping("g");
    stanziamentiParamsEntity.setCurrency(Currency.getInstance("EUR"));
    stanziamentiParamsEntity.setSource("s");
    stanziamentiParamsEntity.setCostoRicavo(StanziamentiParamsCostoRicavoEntity.CR);
    stanziamentiParamsEntity.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.PEDAGGI);
    stanziamentiParamsEntity.setSupplier(new StanziamentiParamsSupplierEntity("s"));

    entityManager.persist(stanziamentiParamsEntity);

    stanziamentiParamsRepository = new StanziamentiParamsRepositoryImpl(entityManager, new StanziamentiParamsEntityMapper());

    StanziamentiParamsResource resource = new StanziamentiParamsResource(stanziamentiParamsRepository);
    this.mockMvc = MockMvcBuilders.standaloneSetup(resource).build();
  }

  @Test
  void findAllStanziamentiParamsArticles() throws Exception {
    mockMvc.perform(get(StanziamentiParamsResource.API_STANZIAMENTI_PARAMS))
      .andExpect(jsonPath("$[*].code").value("testCode"))
      .andExpect(jsonPath("$[*].description").value("testDescrizione"))
      .andExpect(status().isOk());
  }
}
