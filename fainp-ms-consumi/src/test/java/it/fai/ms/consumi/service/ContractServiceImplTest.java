package it.fai.ms.consumi.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.repository.contract.ContractRepository;

@Tag("unit")
class ContractServiceImplTest {

  private static Contract newContract() {
    final var contract = new Contract("::contractUuid::");
    contract.setCompanyCode("::companyCode::");
    contract.setState("::state::");
    return contract;
  }

  private ContractService          contractService;
  private final ContractRepository mockContractRepository = mock(ContractRepository.class);

  @BeforeEach
  void setUp() throws Exception {
    contractService = new ContractServiceImpl(mockContractRepository,null);
  }

  @Test
  void when_contractUuid_doesntExist_then_return_emptyOptional() {

    // given

    given(mockContractRepository.findContractByUuid(anyString())).willReturn(Optional.empty());

    // when

    final var optionalContract = contractService.findContractByNumber("::contractUuid::");

    // then

    assertThat(optionalContract).isEmpty();
  }

  @Test
  void when_contractUuid_exists_then_return_contract() {

    // given

    given(mockContractRepository.findContractByUuid("::contractUuid::")).willReturn(Optional.of(newContract()));

    // when

    final var optionalContract = contractService.findContractByNumber("::contractUuid::");

    // then

    assertThat(optionalContract).contains(newContract());
  }

}
