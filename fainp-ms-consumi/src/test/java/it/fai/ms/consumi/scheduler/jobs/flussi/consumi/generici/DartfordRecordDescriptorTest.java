package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;

@Tag("unit")
public class DartfordRecordDescriptorTest {

  public static final String LINE0 = "\"Crossing Date\",\"Time\",\"Direction\",\"Vehicle\",\"Group Name\",\"Billed on\"";
  public static final String LINE1 = "\"25/04/2018\",\"14:30\",\"Southbound\",\"FC231VA\",\"\",\"25/04/2018\"";
  public static final String LINE2 = "\"24/04/2018\",\"10:55\",\"Northbound\",\"EY164FX\",\"130016451 \",\"25/04/2018\"";

  @Test
  void testDecodeHeader() {
    var subject = new DartfordRecordDescriptor();
    DartfordRecord record = subject.decodeRecordCodeAndCallSetFromString(LINE0, "DEFAULT", "test-file", 0);
    assertThat(record).isNotNull();

  }

  @Test
  void testDecodeLine1() {

    var subject = new DartfordRecordDescriptor();
    DartfordRecord record = subject.decodeRecordCodeAndCallSetFromString(LINE1, "DEFAULT", "test-file", 1);
    assertThat(record).isNotNull();
    DartfordRecord expected = new DartfordRecord("test-file", 1);
    expected.setRecordCode("DEFAULT");
    expected.setCrossingDate("25/04/2018");
    expected.setTime("14:30");
    expected.setDirection("Southbound");
    expected.setVehicle("FC231VA");
    expected.setGroupName("");
    expected.setBilledOn("25/04/2018");

    assertThat(record).isEqualToIgnoringGivenFields(expected,"globalIdentifier", "id", "ingestion_time", "uuid");
  }

  @Test
  void testDecodeLine2() {

    var subject = new DartfordRecordDescriptor();
    DartfordRecord record = subject.decodeRecordCodeAndCallSetFromString(LINE2, "DEFAULT", "test-file", 1);

    DartfordRecord expected = new DartfordRecord("test-file", 1);
    expected.setRecordCode("DEFAULT");
    expected.setCrossingDate("24/04/2018");
    expected.setTime("10:55");
    expected.setDirection("Northbound");
    expected.setVehicle("EY164FX");
    expected.setGroupName("130016451 ");
    expected.setBilledOn("25/04/2018");

    assertThat(record).isEqualToIgnoringGivenFields(expected,"globalIdentifier", "id", "ingestion_time", "uuid");
  }


}
