package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.net.URISyntaxException;
import java.time.Instant;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.service.validator.CardNumberValidityService;

@Tag("unit")
class DgdTollRecordFileFormalValidatorTest {

  DgdTollRecordFileFormalValidator dgdTollRecordFileFormalValidator;

  private final CardNumberValidityService cardNumberValidityService = mock(CardNumberValidityService.class);
  private final DgdTollRecordDescriptor   recordDescriptor          = new DgdTollRecordDescriptor();
  private       String                    filename;

  @BeforeEach
  public void setUp() throws URISyntaxException {
    filename = new File(DgdTollRecordFileFormalValidatorTest.class.getResource("/test-files/dgdtoll/FAI_RE170629-04.TXT.20170701100012")
                                                                 .toURI()).getAbsolutePath();
    dgdTollRecordFileFormalValidator = new DgdTollRecordFileFormalValidator(cardNumberValidityService, recordDescriptor);
  }

  @Test
  void validateFile_validation_success_will_return_fileIsValid_true() {

    given(cardNumberValidityService.hasValidCheckingDigit(any()))
      .willReturn(true);

    RecordsFileInfo validation = dgdTollRecordFileFormalValidator.validateFile(new RecordsFileInfo(filename, -1l, Instant.EPOCH));
    assertThat(validation.fileIsValid())
      .isTrue();
  }

  @Test
  void validateFile_validation_fail_will_return_fileIsValid_false() {

    given(cardNumberValidityService.hasValidCheckingDigit(any()))
      .willReturn(true);

    RecordsFileInfo validation = dgdTollRecordFileFormalValidator.validateFile(new RecordsFileInfo(filename, -1l, Instant.EPOCH));

    assertThat(validation.fileIsValid())
      .isTrue();
  }
}
