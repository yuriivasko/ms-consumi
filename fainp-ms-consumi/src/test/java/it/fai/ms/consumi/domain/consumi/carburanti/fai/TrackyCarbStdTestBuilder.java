package it.fai.ms.consumi.domain.consumi.carburanti.fai;

import java.time.Instant;
import java.util.Optional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;

public class TrackyCarbStdTestBuilder {

  public static TrackyCarbStdTestBuilder newInstance() {
    return new TrackyCarbStdTestBuilder();
  }

  private TrackyCardCarburantiStandard model;

  private TrackyCarbStdTestBuilder() {
    model = new TrackyCardCarburantiStandard(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.of(new TrackyCardGlobalIdentifier("::globalIdentifier::")), TestDummyRecord.instance1);
  }

  public TrackyCardCarburantiStandard build() {
    return model;
  }

  public TrackyCarbStdTestBuilder withDate(final Instant _instant) {
    model.setEntryDateTime(_instant);
    return this;
  }

  public TrackyCarbStdTestBuilder withDevice() {
    model.setDevice(new Device("::deviceId::",TipoDispositivoEnum.TRACKYCARD));
    model.getDevice()
         .setPan("::pan::");
    return this;
  }

  public TrackyCarbStdTestBuilder withNavSupplier() {
    model.setNavSupplierCode("::codfornitorenav::");
    return this;

  }

  public TrackyCarbStdTestBuilder withFuelStation() {
    model.setFuelStation(new FuelStation("1"));
    model.getFuelStation()
         .setFuelStationCode("::fuelStationCode::");
    model.getFuelStation()
         .setPumpNumber("::pumpNumber::");
    model.getFuelStation()
         .setDescription("::description::");
    return this;
  }

  public TrackyCarbStdTestBuilder withTransaction() {
    model.setTransaction(new Transaction());
    model.getTransaction()
         .setDetailCode("::detailCode::");
    model.getTransaction()
         .setSign("::sign::");
    return this;
  }

}
