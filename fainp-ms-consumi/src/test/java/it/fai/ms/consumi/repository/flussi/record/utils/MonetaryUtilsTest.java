package it.fai.ms.consumi.repository.flussi.record.utils;


import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.money.MonetaryAmount;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
public class MonetaryUtilsTest {



  @Test
  void monetaryUtilsPostParsing(){
    //current : -6
    //input: 00004509225
    //output: 4.509225
    //tobe -6 -> -5
    //input: 00004509225
    //wanted
    //4.509225
    //4.50923
    MonetaryAmount result = MonetaryUtils.strToMonetaryIsoNumeric("00004509225", -6, "978");
    assertThat(result.getNumber().doubleValue()).isEqualTo(4.509225d);
    result = MonetaryUtils.strToMonetaryIsoNumeric("00004509225", -6, "978",5);
    assertThat(result.getNumber().doubleValue()).isEqualTo(4.50923d);
  }
}
