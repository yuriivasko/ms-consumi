package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim.record;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0201;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0301;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0302;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim.TokheimRecordDescriptor;

@Tag("unit")
class TokehimRecordDescriptorTest {

  @Test
  void testHeader() {
    String line     = "0101002921620170415 Fai                 0000000001RPS                                                                                                                                                   ";
    String fileName = "testFile";

    var    subject    = new TokheimRecordDescriptor();
    String recordCode = subject.extractRecordCode(line);
    assertThat(recordCode).isEqualTo("0101");

    assertThat(subject.skipWellKnownRecordCodeToIgnore(recordCode)).isFalse();
    assertThat(subject.getHeaderBegin()).isEqualTo(recordCode);

    TokheimRecord result = subject.decodeRecordCodeAndCallSetFromString(line, recordCode, fileName, 0);

    assertThat(result).isNotNull();
    assertThat(result.getRecordCode()).isEqualTo("0101");
    assertThat(result.getCreationDateInFile()).isEqualTo("20170415");
    assertThat(result.toBeSkipped()).isFalse();

  }

  @Test
  void testFooter() {
    String line     = "010900292100000000019800000018000000000081+000001785678                                                                                                                                                 ";
    String fileName = "testFile";

    var subject = new TokheimRecordDescriptor();

    Map<Integer, Integer> startEndPosTotalRows = subject.getStartEndPosTotalRows();
    assertThat(startEndPosTotalRows).containsAllEntriesOf(Map.of(11, 22));

    String recordCode = subject.extractRecordCode(line);
    assertThat(recordCode).isEqualTo("0109");

    assertThat(subject.skipWellKnownRecordCodeToIgnore(recordCode)).isFalse();

    TokheimRecord result = subject.decodeRecordCodeAndCallSetFromString(line, recordCode, fileName, 0);

    assertThat(result).isNotNull();
    assertThat(result.getRecordCode()).isEqualTo("0109");
    assertThat(result.toBeSkipped()).isFalse();

    assertThat(subject.getFooterCodeBegin()).isEqualTo(recordCode);

  }

  @Test
  void test201() {
    String line     = "0201002921000001 010121372420170415Gazzola               MOROZZO                 12040 Via Mondovi 52                0039210101                                                                         ";
    String fileName = "testFile";

    var subject = new TokheimRecordDescriptor();

    String recordCode = subject.extractRecordCode(line);
    assertThat(recordCode).isEqualTo("0201");
    assertThat(subject.skipWellKnownRecordCodeToIgnore(recordCode)).isFalse();

    TokheimRecord result = subject.decodeRecordCodeAndCallSetFromString(line, recordCode, fileName, 0);

    assertThat(result).isInstanceOfSatisfying(TokheimRecord0201.class, r -> {
      assertThat(r.getRecordCode()).isEqualTo("0201");
      assertThat(r.getFileNumber()).isEqualTo("002921");
      assertThat(r.getNrProgressivo()).isEqualTo("00001");
      assertThat(r.getNomePoint()).isEqualTo("Gazzola");
      assertThat(r.getCittaPoint()).isEqualTo("MOROZZO");
      assertThat(r.getCapPoint()).isEqualTo("12040");
      assertThat(r.getIndirizzoPoint()).isEqualTo("Via Mondovi 52");
      assertThat(r.getCodiceEsternoPoint()).isEqualTo("0039210101");
    });
    assertThat(result.toBeSkipped()).isTrue();
    assertThat(result.getNestedLevel()).isEqualTo(0);
  }

  @Test
  void test209() {
    String line     = "0209002921000001 010121372420170415000000000006000000000002+000000116955                                                                                                                                ";

    var subject = new TokheimRecordDescriptor();

    String recordCode = subject.extractRecordCode(line);
    assertThat(recordCode).isEqualTo("0209");
    assertThat(subject.skipWellKnownRecordCodeToIgnore(recordCode)).isTrue();
  }

  @Test
  void test301() {
    String line     = "030102063724530192592017041320012900177896970007612500582+00000770850000000000000001000000DC32D9   00000001EUR                        23127896970007612500582=23120000250600000  00000000000000         ";
    String fileName = "testFile";

    var subject = new TokheimRecordDescriptor();

    String recordCode = subject.extractRecordCode(line);
    assertThat(recordCode).isEqualTo("0301");
    assertThat(subject.skipWellKnownRecordCodeToIgnore(recordCode)).isFalse();

    TokheimRecord result = subject.decodeRecordCodeAndCallSetFromString(line, recordCode, fileName, 0);

    assertThat(result).isInstanceOfSatisfying(TokheimRecord0301.class, r -> {
      assertThat(r.getRecordCode()).isEqualTo("0301");
      assertThat(r.getDataErogazione()).isEqualTo("20170413");
      assertThat(r.getOraErogazione()).isEqualTo("200129");
      assertThat(r.getNrTransazione1()).isEqualTo("0017");
      assertThat(r.getNrTrackycard()).isEqualTo("7896970007612500582");
      assertThat(r.getTipoTransazione()).isEqualTo("00");
      assertThat(r.getNrTransazione2()).isEqualTo("DC32D9");
      assertThat(r.getValuta()).isEqualTo("EUR");
    });

    assertThat(result.toBeSkipped()).isTrue();
    assertThat(result.getNestedLevel()).isEqualTo(1);
  }

  @Test
  void test302() {
    String line     = "0302070425+058002+000001329+000770852200+00013901+00000000+00000000000000+00000000                                                                                                                      ";
    String fileName = "testFile";

    var subject = new TokheimRecordDescriptor();

    String recordCode = subject.extractRecordCode(line);
    assertThat(recordCode).isEqualTo("0302");
    assertThat(subject.skipWellKnownRecordCodeToIgnore(recordCode)).isFalse();

    TokheimRecord result = subject.decodeRecordCodeAndCallSetFromString(line, recordCode, fileName, 0);

    assertThat(result).isInstanceOfSatisfying(TokheimRecord0302.class, r -> {
      assertThat(r.getRecordCode()).isEqualTo("0302");
      assertThat(r.getNrErogatore()).isEqualTo("07");
      assertThat(r.getCodiceArticoloFornitore()).isEqualTo("0425");
      assertThat(r.getSegno()).isEqualTo("+");
      assertThat(r.getQuantita()).isEqualTo("058002");
      assertThat(r.getSegnoUnitario()).isEqualTo("+");
      assertThat(r.getPrezzoUnitario()).isEqualTo("000001329");
      assertThat(r.getSegnoImponibile()).isEqualTo("+");
      assertThat(r.getImponibile()).isEqualTo("00077085");
      assertThat(r.getPercentualeIva()).isEqualTo("2200");
      assertThat(r.getSegnoImposta()).isEqualTo("+");
      assertThat(r.getImposta()).isEqualTo("00013901");
    });
    assertThat(result.toBeSkipped()).isFalse();
    assertThat(result.getNestedLevel()).isEqualTo(2);
  }


}
