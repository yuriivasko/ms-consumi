package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.money.Monetary;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.AmountTestBuilder;
import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.PriceTableTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoCarburanteTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.petrolpump.PriceCalculatedByPriceTableService;

@Tag("unit")
class DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImplTest {

  private DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl dettaglioStanziamentoProcessor;
  private final PriceCalculatedByPriceTableService                   mockPriceCalculatedByPriceTableService = mock(PriceCalculatedByPriceTableService.class);
  private final CurrencyRateService                                  mockCurrencyRateService                = mock(CurrencyRateService.class);;
  private final PriceCalculatedByNavService                          mockPriceCalculatedByNavClient         = mock(PriceCalculatedByNavService.class);;

  @BeforeEach
  void setUp() throws Exception {

    dettaglioStanziamentoProcessor = new DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl(mockPriceCalculatedByPriceTableService,
                                                                                                    mockCurrencyRateService,
                                                                                                    mockPriceCalculatedByNavClient);

  }

  @Test
  void when_dettaglioStanziamento_is_costoDefinitivo_ricavoProvvisorio() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withPriceRefernce(100)
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();
    var priceTableListCost = PriceTableTestBuilder.newInstance()
                                                  .withCostRevenueFlag(CostRevenue.COST)
                                                  .withReferencePrice(BigDecimal.ONE)
                                                  .withValidUntilNow()
                                                  .build();
    var priceTableListRev = PriceTableTestBuilder.newInstance()
                                                 .withCostRevenueFlag(CostRevenue.REVENUE)
                                                 .withReferencePrice(BigDecimal.ONE)
                                                 .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList(priceTableListCost,
                                                                                                                 priceTableListRev));

    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then

    assertEquals(2, list.size(), "La lista deve essere di 2 elementi");
    dettaglioStanziamento.setInvoiceType(InvoiceType.P);
    verify(mockPriceCalculatedByNavClient).setPriceCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));
    dettaglioStanziamento.setInvoiceType(InvoiceType.D);
    verify(mockPriceCalculatedByNavClient).setCostCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));

  }

  @Test
  void when_dettaglioStanziamento_is_costoProvvisorio_ricavoDefinitivo() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withPriceRefernce(200)
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    var priceTableListCost = PriceTableTestBuilder.newInstance()
                                                  .withCostRevenueFlag(CostRevenue.COST)
                                                  .withReferencePrice(BigDecimal.ONE)
                                                  .build();
    var priceTableListRev = PriceTableTestBuilder.newInstance()
                                                 .withCostRevenueFlag(CostRevenue.REVENUE)
                                                 .withReferencePrice(BigDecimal.ONE)
                                                 .withValidUntilNow()
                                                 .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList(priceTableListCost,
                                                                                                                 priceTableListRev));
    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then

    assertEquals(2, list.size(), "La lista deve essere di 2 elementi");
    dettaglioStanziamento.setInvoiceType(InvoiceType.D);
    verify(mockPriceCalculatedByNavClient).setPriceCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));
    dettaglioStanziamento.setInvoiceType(InvoiceType.P);
    verify(mockPriceCalculatedByNavClient).setCostCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));

  }

  @Test
  void when_dettaglioStanziamento_is_costoProvvisorio_ricavoDefinitivoDaConsumo() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withPriceRefernce(200)
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    var priceTableListCost = PriceTableTestBuilder.newInstance()
                                                  .withCostRevenueFlag(CostRevenue.COST)
                                                  .withReferencePrice(BigDecimal.ONE)
                                                  .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList(priceTableListCost));
    when(mockCurrencyRateService.convertAmount(any(), any(), any())).thenAnswer(new Answer<Amount>() {
      public Amount answer(InvocationOnMock invocation) {
        return ((Amount) invocation.getArguments()[0]).clone();
      }
    });
    // when(mockCurrencyRateService.convertAmount(any(), any(),
    // any())).thenReturn(dettaglioStanziamento.getPriceReference());

    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then

    assertEquals(2, list.size(), "La lista deve essere di 2 elementi");
    dettaglioStanziamento.setInvoiceType(InvoiceType.D);
    Amount amount = new Amount();
    amount.setVatRate(dettaglioStanziamento.getPriceReference()
                                           .getVatRate());
    amount.setAmountExcludedVat(Money.of(new BigDecimal(200), dettaglioStanziamento.getPriceReference()
                                                                                   .getAmountExcludedVat()
                                                                                   .getCurrency()));
    dettaglioStanziamento.setPriceReference(amount);
    verify(mockPriceCalculatedByNavClient).setPriceCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));
    dettaglioStanziamento.setInvoiceType(InvoiceType.P);
    amount = new Amount();
    amount.setVatRate(dettaglioStanziamento.getPriceReference()
                                           .getVatRate());
    amount.setAmountExcludedVat(Money.of(BigDecimal.ONE, dettaglioStanziamento.getPriceReference()
                                                                                   .getAmountExcludedVat()
                                                                                   .getCurrency()));
    dettaglioStanziamento.setPriceReference(amount);
    verify(mockPriceCalculatedByNavClient).setCostCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));

    Optional<DettaglioStanziamentoCarburante> D = list.stream().filter(rs -> rs.getInvoiceType().equals(InvoiceType.D)).findFirst();
    Optional<DettaglioStanziamentoCarburante> P = list.stream().filter(rs -> rs.getInvoiceType().equals(InvoiceType.P)).findFirst();

    assertTrue(D.isPresent(),"Il dettagllio provvisorio non è presente");
    assertTrue(P.isPresent(),"Il dettagllio definitivo non è presente");

    assertEquals(BigDecimal.ONE.compareTo(P.get().getPriceReference().getAmountExcludedVatBigDecimal()),0);
    assertEquals(new BigDecimal(200).compareTo(D.get().getPriceReference().getAmountExcludedVatBigDecimal()),0);
  }

  @Test
  void when_dettaglioStanziamento_is_costoDefinitivoDaConsumi_ricavoProvissorio() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withPriceRefernce(100)
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    var priceTableListRevenue = PriceTableTestBuilder.newInstance()
                                                     .withCostRevenueFlag(CostRevenue.REVENUE)
                                                     .withReferencePrice(BigDecimal.ONE)
                                                     .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList(priceTableListRevenue));
    when(mockCurrencyRateService.convertAmount(eq(dettaglioStanziamento.getPriceReference()), any(),
                                               any())).thenReturn(dettaglioStanziamento.getPriceReference());

    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then
    
    dettaglioStanziamento.setInvoiceType(InvoiceType.P);
    Amount amount = new Amount();
    amount.setVatRate(dettaglioStanziamento.getPriceReference().getVatRate());
    amount.setAmountExcludedVat(Money.of(BigDecimal.ONE, "EUR"));
    dettaglioStanziamento.setPriceReference(amount);
    verify(mockPriceCalculatedByNavClient).setPriceCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));
    dettaglioStanziamento.setInvoiceType(InvoiceType.D);
    amount = new Amount();
    amount.setVatRate(dettaglioStanziamento.getPriceReference().getVatRate());
    amount.setAmountExcludedVat(Money.of(new BigDecimal(100), "EUR"));
    dettaglioStanziamento.setPriceReference(amount);
    verify(mockPriceCalculatedByNavClient).setCostCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));

    Optional<DettaglioStanziamentoCarburante> D = list.stream().filter(rs -> rs.getInvoiceType().equals(InvoiceType.D)).findFirst();
    Optional<DettaglioStanziamentoCarburante> P = list.stream().filter(rs -> rs.getInvoiceType().equals(InvoiceType.P)).findFirst();

    assertTrue(D.isPresent(),"Il dettagllio provvisorio non è presente");
    assertTrue(P.isPresent(),"Il dettagllio definitivo non è presente");

    assertEquals(BigDecimal.ONE.compareTo(P.get().getPriceReference().getAmountExcludedVatBigDecimal()),0);
    assertEquals(new BigDecimal(100).compareTo(D.get().getPriceReference().getAmountExcludedVatBigDecimal()),0);

  }

  @Test
  void when_dettaglioStanziamento_is_costoDefinitivoDaConsumi_ricavoDefinitivoDaConsumo() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withPriceRefernce(150)
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();
    Amount ricavo = AmountTestBuilder.newInstance()
                                     .withAmountIncludedVat(Monetary.getDefaultAmountFactory()
                                                                    .setCurrency("EUR")
                                                                    .setNumber(200)
                                                                    .create())
                                     .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList());
    when(mockCurrencyRateService.convertAmount(any(), any(), any())).thenReturn(dettaglioStanziamento.getPriceReference());
    when(mockCurrencyRateService.convertAmount(any(), any(), any())).thenReturn(ricavo);

    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then

    assertEquals(2, list.size(), "La lista deve essere di 2 elementi");
    for (DettaglioStanziamentoCarburante rs : list) {
      assertEquals(InvoiceType.D, rs.getInvoiceType(), "Il dettaglio deve essere provvisorio");
    }
  }

  @Test
  void when_dettaglioStanziamento_isProvvisorio() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    var priceTableListCost = PriceTableTestBuilder.newInstance()
                                                  .withCostRevenueFlag(CostRevenue.COST)
                                                  .withReferencePrice(BigDecimal.ONE)
                                                  .build();
    var priceTableListRev = PriceTableTestBuilder.newInstance()
                                                 .withCostRevenueFlag(CostRevenue.REVENUE)
                                                 .withReferencePrice(BigDecimal.ONE)
                                                 .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList(priceTableListCost,
                                                                                                                 priceTableListRev));
    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then

    assertEquals(2, list.size(), "La lista deve essere di 2 elementi");
    for (DettaglioStanziamentoCarburante rs : list) {
      if (rs.getCostComputed() == null) {
        assertEquals(InvoiceType.P, rs.getInvoiceType(), "Il dettaglio deve essere provvisorio");
      } else if (rs.getPriceComputed() == null) {
        assertEquals(InvoiceType.P, rs.getInvoiceType(), "Il dettaglio deve essere provvisorio");
      } else {
        assertFalse(true, "Il dettaglio deve essere di tipo COSTO o di tipo RICAVO");
      }
    }
  }

  @Test
  void when_dettaglioStanziamento_isProvvisorio_SoloRicavo() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .withCostoRicavo(CostoRicavo.R)
                                                          .build();

    var priceTableListCost = PriceTableTestBuilder.newInstance()
                                                  .withCostRevenueFlag(CostRevenue.COST)
                                                  .withReferencePrice(BigDecimal.ONE)
                                                  .build();
    var priceTableListRev = PriceTableTestBuilder.newInstance()
                                                 .withCostRevenueFlag(CostRevenue.REVENUE)
                                                 .withReferencePrice(BigDecimal.ONE)
                                                 .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList(priceTableListCost,
                                                                                                                 priceTableListRev));
    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then

    verify(mockPriceCalculatedByNavClient).setPriceCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));

    assertEquals(1, list.size(), "La lista deve essere di 1 elemento");
    for (DettaglioStanziamentoCarburante rs : list) {
      // assertNotNull(rs.getPriceComputed());
      assertEquals(InvoiceType.P, rs.getInvoiceType(), "Il dettaglio deve essere provvisorio");
    }
  }

  @Test
  void when_dettaglioStanziamento_isDefinitivo() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withCurrencyEUR()
                                                                          .withFuelStation()
                                                                          .build();
    dettaglioStanziamento.setSupplier(new Supplier("::supplierCode::"));

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .withVatRate(1f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    var priceTableListCost = PriceTableTestBuilder.newInstance()
                                                  .withCostRevenueFlag(CostRevenue.COST)
                                                  .withReferencePrice(BigDecimal.ONE)
                                                  .withValidUntilNow()
                                                  .build();
    var priceTableListRev = PriceTableTestBuilder.newInstance()
                                                 .withCostRevenueFlag(CostRevenue.REVENUE)
                                                 .withReferencePrice(BigDecimal.TEN)
                                                 .withValidUntilNow()
                                                 .build();

    // when
    when(mockPriceCalculatedByPriceTableService.getPriceTableList(any(), any(), any())).thenReturn(Arrays.asList(priceTableListCost,
                                                                                                                 priceTableListRev));
    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then

    assertEquals(2, list.size(), "La lista deve essere di 2 elemento");
    for (DettaglioStanziamentoCarburante rs : list) {
      assertEquals(InvoiceType.D, rs.getInvoiceType(), "Il dettaglio deve essere definitivo");
      if (rs.getPriceReference()
            .getAmountExcludedVat()
            .getNumber()
            .numberValue(BigDecimal.class)
            .compareTo(priceTableListCost.getReferencePrice()) == 0) {
      } else if (rs.getPriceReference()
                   .getAmountExcludedVat()
                   .getNumber()
                   .numberValue(BigDecimal.class)
                   .compareTo(priceTableListRev.getReferencePrice()) == 0) {
      } else {
        fail("PriceReference " + rs.getPriceReference()
                                   .getAmountExcludedVat()
                                   .getNumber()
             + " is not equals to " + priceTableListRev.getReferencePrice() + " or " + priceTableListCost.getReferencePrice());
      }
    }
  }

}
