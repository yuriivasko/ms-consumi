package it.fai.ms.consumi.repository.viewallegati;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.testutils.ConsumiTestUtility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Tag("unit")
@Transactional
@ActiveProfiles(profiles = {"default", "swagger"})
class ViewAllegatoGenericoSezione2RepositoryImplTest {

  public static final String CAUSALE = "Causale ";
  public static final String CLASSE_TARIFFARIA = "classe tariffaria ";
  @Autowired
  private EntityManager em;
  private List<UUID> uuids = new ArrayList<>();


  private ViewAllegatoGenericoSezione2Repository viewAllegatoGenericoRepo;
  private StanziamentoEntity s;
  private DettaglioStanziamentoGenericoEntity dsg;
  private DettaglioStanziamentiRepositoryImpl repository;


  @BeforeEach
  void setUp() throws Exception {
    viewAllegatoGenericoRepo = new ViewAllegatoGenericoSezione2RepositoryImpl(em);

    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
      new DettaglioStanziamentoGenericoEntityMapper(),
      new DettaglioStanziamentoPedaggioEntityMapper(),
      new DettaglioStanziamentoTrenoEntityMapper());
    repository = new DettaglioStanziamentiRepositoryImpl(em, dettaglioStanziamentoEntityMapper,
      new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper));


  }

  @Test
  public void find() {
    ConsumiTestUtility consumiTestUtility = new ConsumiTestUtility(em);
    s = consumiTestUtility.createStanziamento("1");
    String vehicleLicenseId = ConsumiTestUtility.RandomString();
    dsg = consumiTestUtility.createDettaglioGenerico("1",vehicleLicenseId);
    consumiTestUtility.createViewDettaglioGenerico(dsg.getId().toString(), "1",vehicleLicenseId);
    consumiTestUtility.relatedStanziamentoOnDettagli(s, dsg);
    em.flush();
    List<UUID> dettagliStanziamenti = repository.findDettagliStanziamentoIdByCodiciStanziamento(Arrays.asList(s.getCode()));
    List<ViewAllegatoGenericoSezione2Entity> dettagliStanziamento = viewAllegatoGenericoRepo.findByDettaglioStanziamentoId(dettagliStanziamenti);
    assertThat(dettagliStanziamento).hasSize(1);
    assertThat(dettagliStanziamento.get(0).getTargaVeicolo()).isEqualTo(vehicleLicenseId);


  }


}
