package it.fai.ms.consumi.repository.consumo.model.telepass;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.time.Instant;
import java.util.Optional;

public class DummyConsumo extends Consumo {

  public DummyConsumo(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public InvoiceType getInvoiceType() {
    throw new UnsupportedOperationException();
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    return GlobalIdentifierTestBuilder.newInstance()
                                      .internal()
                                      .build();
  }

  @Override
  public Instant getDate() {
    return Instant.now();
  }

}
