package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import static it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElceuDescriptor.BEGIN_15;
import static it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElceuDescriptor.BEGIN_17;
import static org.assertj.core.api.Assertions.assertThat;

import javax.money.MonetaryAmount;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElceuRecord;

@Tag("unit")
public class ElceuDescriptorTest {

  private String recordData1 = "1705600003                                  000049000000799000906557303HWAYA14                      16270420180917+7005792105320000020201809171755280000093605978WGM 10480           616050600012355860000000012355860L6";
  private String recordData2 = "1705600003                                  000049000000799000906557303HWAYA14                      16270420180917+7005792105320000020201809171755280000093605978WGM 10480           616050600012355865000000012355865L6";
  private String recordDataGermany1 = "1527600001                                  00000000000116100004900000079963530519113920180927000169463000237263+700579211164500013320180928100159978**11EGC             6425 050000000087511610           2018092810015900000000875L2000237263        00000000118";

  @Test
  void testElceuDescriptor_Record17(){
    //record 1 is half down (last digit is 0)
    {
      ElceuDescriptor elceuDescriptor = new ElceuDescriptor();

      ElceuRecord elceuRecord = elceuDescriptor.decodeRecordCodeAndCallSetFromString(recordData1, BEGIN_17, "DA06A284.ELCEU.TEST", 1);
      //00012355860
      //12355860
      MonetaryAmount amountNoVat = elceuRecord.getAmount_novat();
      assertThat(amountNoVat.getNumber().doubleValue()).isEqualTo(12.35586d);
      //0000088777
      MonetaryAmount amountVat = elceuRecord.getAmount_vat();
      assertThat(amountVat.getNumber().doubleValue()).isEqualTo(12.35586d);
    }
    //record 2 is half up (last digit is 5)
    {
      ElceuDescriptor elceuDescriptor = new ElceuDescriptor();
      ElceuRecord elceuRecord = elceuDescriptor.decodeRecordCodeAndCallSetFromString(recordData2, BEGIN_17, "DA06A284.ELCEU.TEST", 1);
      //00012355860
      //12355860
      MonetaryAmount amountNoVat = elceuRecord.getAmount_novat();
      assertThat(amountNoVat.getNumber().doubleValue()).isEqualTo(12.35587d);
      //0000088777
      MonetaryAmount amountVat = elceuRecord.getAmount_vat();
      assertThat(amountVat.getNumber().doubleValue()).isEqualTo(12.35587d);
    }

  }

  @Test
  void testElceuDescriptor_Record15(){
    //record 1 is half down (last digit is 0)
    {
      ElceuDescriptor elceuDescriptor = new ElceuDescriptor();

      ElceuRecord elceuRecord = elceuDescriptor.decodeRecordCodeAndCallSetFromString(recordDataGermany1, BEGIN_15, "DA06A284.VACON.ELCEU.TEST", 1);

      MonetaryAmount amountNoVat = elceuRecord.getAmount_novat();
      assertThat(amountNoVat.getNumber().doubleValue()).isEqualTo(8.75d);

      MonetaryAmount amountVat = elceuRecord.getAmount_vat();
      assertThat(amountVat).isNull();

      assertThat(elceuRecord.getTripId()).isEqualTo("0000000000011610");

      assertThat(elceuRecord.getTotalAmount()).isEqualTo("00000000875");

      assertThat(elceuRecord.getVATRate()).isEqualTo("0");

      assertThat(elceuRecord.getAmountWithoutVAT()).isEqualTo(elceuRecord.getTotalAmount());
    }
  }
}
