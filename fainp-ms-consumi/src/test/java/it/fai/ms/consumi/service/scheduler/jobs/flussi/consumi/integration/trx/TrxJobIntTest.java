package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.trx;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.api.trx.model.TrxNoOilReq;
import it.fai.ms.consumi.api.trx.model.TrxOilReq;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.service.trx.TrxJob;
import it.fai.ms.consumi.util.BeanUtil;

@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler",
                                "it.fai.ms.consumi.domain.dettaglio_stanziamento", "it.fai.ms.consumi.api.trx.model.mapper" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"), })
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class TrxJobIntTest extends AbstractIntegrationTestSpringContext {

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @MockBean
  private NotificationService notificationService;

  @Autowired
  private TrxJob job;

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentoTestRepository;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Autowired
  protected ApplicationContext applicationContext;

  @BeforeEach
  public void init() throws Exception {
    BeanUtil.setApplicatioContext(applicationContext);
  }

  @Test
  public void elaborate_one_Oil_api_call_with_by_invoice_provider_will_generate_one_stanziamento_definitivo() {

    TrxOilReq trxOilReq = new TrxOilReq();
    final OffsetDateTime dateTime = OffsetDateTime.ofInstant(Instant.parse("2018-01-01T00:00:00Z"), ZoneOffset.UTC);
    final OffsetDateTime dateTime2 = OffsetDateTime.ofInstant(Instant.parse("2017-01-01T00:00:00Z"), ZoneOffset.UTC);

    trxOilReq.pump("1");
    trxOilReq.supplyDateTime(dateTime2);
    trxOilReq.unit("LT");

    trxOilReq.amount(new BigDecimal("1220.00"));
    trxOilReq.articleCode("DIESEL");
    trxOilReq.buyerCode("0000015");
    trxOilReq.cardNumber("7896970000312900038");
    trxOilReq.currency("EUR");
    trxOilReq.driverId("123");
    trxOilReq.intVRC("IT");
    trxOilReq.km(10);
    trxOilReq.outletCode("IT63000006");
    trxOilReq.plate("AA000AA");
    trxOilReq.productCode("CARBURANTI");
    trxOilReq.quantity(BigDecimal.TEN);
    trxOilReq.requestCode("1234567890");
    trxOilReq.submissioDateTime(dateTime);
    trxOilReq.vat(new BigDecimal("0.22000"));
    trxOilReq.vendorCode("F006808");
    trxOilReq.setVirtualCard(true);

    ValidationOutcome result = job.consumeMessage(trxOilReq);

    assertThat(result.isValidationOk()).isNotNull()
                                       .isTrue();

    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);

    verify(stanziamentiToNavJmsPublisher).publish(argumentCaptor.capture());

    argumentCaptor.getValue()
                  .stream()
                  .forEach(sm -> System.out.println(sm));

    List<StanziamentoMessage> expectedMessages = argumentCaptor.getValue();
    assertThat(expectedMessages).hasSize(1)
                                .extracting(sm -> tuple(sm.getNrCliente(), sm.getTarga(), sm.getPaese(), sm.getCosto(), sm.getPrezzo(),
                                                        sm.getValuta(), sm.getTipoFlusso(), sm.getGeneraStanziamento(),
                                                        sm.getStatoStanziamento(), sm.getDataErogazioneServizio()))
                                .containsExactlyInAnyOrder(tuple("0000015", "AA000AA", "IT", 100.00d, 100.00d, "EUR", 1, 1, 2,
                                                                 LocalDate.parse("2017-01-01")));

    List<StanziamentoEntity> stanziamentoEntities = dettaglioStanziamentoTestRepository.findAllStanziamenti();
    assertThat(stanziamentoEntities).hasSize(1)
                                    .extracting(s -> tuple(s.getNumeroCliente(), s.getTarga(), s.getPaese(), s.getPrezzo()
                                                                                                              .doubleValue(),
                                                           s.getCosto()
                                                            .doubleValue(),
                                                           s.getValuta(), s.getTipoFlusso(), s.getGeneraStanziamento(),
                                                           s.getStatoStanziamento(), s.getDataErogazioneServizio()))
                                    .containsExactlyInAnyOrder(

                                                               tuple("0000015", "AA000AA", "IT", 100.00d, 100.00d, "EUR", TypeFlow.MYFAI,
                                                                     GeneraStanziamento.COSTO_RICAVO, InvoiceType.D,
                                                                     LocalDate.parse("2017-01-01")));

    List<DettaglioStanziamentoEntity> dettaglioStanziamentoEntities = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    assertThat(dettaglioStanziamentoEntities).hasSize(1)
                                             // FIXME aggiungere assertion significative
                                             .extracting(ds -> (DettaglioStanziamentoCarburanteEntity) ds)
                                             .extracting(dsc -> tuple(dsc.getCostoUnitarioCalcolatoNoiva()
                                                                         .toPlainString(),
                                                                      dsc.getPrezzoUnitarioCalcolatoNoiva()
                                                                         .toPlainString(),
                                                                      dsc.getCodiceTrackycard(), dsc.getTipoSorgente()
                                                                                                    .name()))
                                             .containsExactlyInAnyOrder(tuple(new BigDecimal(100.00000, MathContext.DECIMAL32).toString(),
                                                                              new BigDecimal(100.00000, MathContext.DECIMAL32).toString(),
                                                                              "7896970000312900038", "API"));

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    int i = 0;
    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoCarburanteEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamentoEntity;

      // Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime).isNotNull();
      assertThat(dataAcquisizioneFlusso).isNotNull();
      assertThat(dataAcquisizioneFlusso).isEqualTo(dateTime.toInstant());

      final LocalDate now = LocalDate.now();
      LocalDate date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());
      i++;
    }
  }


  @Test
  @Disabled //non abbiamo un provider per il consumo generico
  public void elaborate_one_NoOil_api_call_will_generate_one_stanziamento_defintivo() {

    TrxNoOilReq trxNoOilReq = new TrxNoOilReq();
    final OffsetDateTime dateTime = OffsetDateTime.ofInstant(Instant.parse("2018-01-01T00:00:00Z"), ZoneOffset.UTC);
    final OffsetDateTime dateTimeStart = OffsetDateTime.ofInstant(Instant.parse("2017-01-01T00:00:00Z"), ZoneOffset.UTC);
    final OffsetDateTime dateTimeEnd = OffsetDateTime.ofInstant(Instant.parse("2017-12-31T00:00:00Z"), ZoneOffset.UTC);

    trxNoOilReq.unit("LT");

    trxNoOilReq.amount(new BigDecimal("1220.00"));
    trxNoOilReq.articleCode("CARBURANTI");
    trxNoOilReq.buyerCode("0000015");
    trxNoOilReq.cardNumber("7896970000312900038");
    trxNoOilReq.currency("EUR");
    trxNoOilReq.driverId("123");
    trxNoOilReq.intVRC("IT");
    trxNoOilReq.km(10);
    trxNoOilReq.outletCode("IT63000006");
    trxNoOilReq.plate("AA000AA");
    trxNoOilReq.productCode("CARBURANTI");
    trxNoOilReq.quantity(BigDecimal.TEN);
    trxNoOilReq.requestCode("1234567890");
    trxNoOilReq.submissioDateTime(dateTime);
    trxNoOilReq.vat(new BigDecimal("0.22000"));
    trxNoOilReq.vendorCode("F006808");
    trxNoOilReq.setVirtualCard(true);
    trxNoOilReq.setSupplyDateStart(dateTimeStart);
    trxNoOilReq.setSupplyDateEnd(dateTimeEnd);

    ValidationOutcome result = job.consumeMessage(trxNoOilReq);

    assertThat(result.isValidationOk()).isNotNull()
                                       .isTrue();

    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);

    verify(stanziamentiToNavJmsPublisher).publish(argumentCaptor.capture());

    argumentCaptor.getValue()
                  .stream()
                  .forEach(sm -> System.out.println(sm));

    // data erogazione servizio should be equal to date end;
    List<StanziamentoMessage> expectedMessages = argumentCaptor.getValue();
    assertThat(expectedMessages).hasSize(1)
                                .extracting(sm -> tuple(sm.getNrCliente(), sm.getTarga(), sm.getPaese(), sm.getCosto(), sm.getPrezzo(),
                                                        sm.getValuta(), sm.getTipoFlusso(), sm.getGeneraStanziamento(),
                                                        sm.getStatoStanziamento(), sm.getDataErogazioneServizio()))
                                .containsExactlyInAnyOrder(tuple("0000015", "AA000AA", "IT", 100.00d, 100.00d, "EUR", 1, 1, 2,
                                                                 LocalDate.parse("2017-12-31")));

    List<StanziamentoEntity> stanziamentoEntities = dettaglioStanziamentoTestRepository.findAllStanziamenti();
    assertThat(stanziamentoEntities).hasSize(1)
                                    .extracting(s -> tuple(s.getNumeroCliente(), s.getTarga(), s.getPaese(), s.getPrezzo()
                                                                                                              .doubleValue(),
                                                           s.getCosto()
                                                            .doubleValue(),
                                                           s.getValuta(), s.getTipoFlusso(), s.getGeneraStanziamento(),
                                                           s.getStatoStanziamento(), s.getDataErogazioneServizio()))
                                    .containsExactlyInAnyOrder(

                                                               tuple("0000015", "AA000AA", "IT", 100.00d, 100.00d, "EUR", TypeFlow.MYFAI,
                                                                     GeneraStanziamento.COSTO_RICAVO, InvoiceType.D,
                                                                     LocalDate.parse("2017-12-31")));

    List<DettaglioStanziamentoEntity> dettaglioStanziamentoEntities = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoGenerico();
    assertThat(dettaglioStanziamentoEntities).hasSize(1)
                                             // FIXME aggiungere assertion significative
                                             .extracting(ds -> (DettaglioStanziamentoGenericoEntity) ds)
                                             .extracting(dsg -> tuple(dsg.getTransactionSign(), dsg.getVehicleLicenseId(),
                                                                      dsg.getTransactionDetailCode(), dsg.getEndDate(), dsg.getStartDate(),
                                                                      dsg.getRecordCode(), dsg.getCurrencyCode(),
                                                                      dsg.getAmountExcludedVat(), dsg.getSourceType(),
                                                                      dsg.getAdditionalInformation()))
                                             .containsExactlyInAnyOrder(tuple("+", "AA000AA", "CARBURANTI", dateTimeEnd.toInstant(),
                                                                              dateTimeStart.toInstant(), "DEFAULT", "EUR",
                                                                              new BigDecimal("100.00000"),
                                                                              "ACTICO", null));

  }
}
