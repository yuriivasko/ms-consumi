package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoViaggiRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Tag("unit")
public class SanBernardoJobTest {


  private SanBernardoJob job;

  private PlatformTransactionManager transactionManager = mock(PlatformTransactionManager.class);
  private StanziamentiParamsValidator stanziamentiParamsValidator = mock(StanziamentiParamsValidator.class);
  private NotificationService notificationService = mock(NotificationService.class);
  private RecordPersistenceService persistenceService = mock(RecordPersistenceService.class);
  private RecordDescriptor<SanBernardoViaggiRecord> sanBernardoViaggiRecordDescriptor = mock(RecordDescriptor.class);
  private ConsumoProcessor<SanBernardo> sanBernardoConsumoProcessor = mock(ConsumoProcessor.class);
  private RecordConsumoMapper<SanBernardoViaggiRecord, SanBernardo> sanBernardoViaggiRecordConsumoProcessor = mock(RecordConsumoMapper.class);
  private StanziamentiToNavPublisher stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);


  @BeforeEach
  public void init() {
    job = new SanBernardoJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService, persistenceService,
      sanBernardoViaggiRecordDescriptor,
      sanBernardoConsumoProcessor,
      sanBernardoViaggiRecordConsumoProcessor,
      stanziamentiToNavJmsProducer);
  }

  @Test
  public void onValidateFileBuildSanBernardoRecordInfo() throws URISyntaxException {

    String fileName = "20180531_elenco_viaggi_CM.csv";
    String filePath = new File(SanBernardoJobTest.class.getResource("/test-files/sanbernardo/" + fileName).toURI()).getAbsolutePath();
    RecordsFileInfo recordsFileInfo = job.validateFile(filePath, System.currentTimeMillis(), new Date().toInstant());
    assertThat(recordsFileInfo).isNotNull().isOfAnyClassIn(SanBernardoRecordFileInfo.class);
    SanBernardoRecordFileInfo sanBernardoRecordFileInfo = (SanBernardoRecordFileInfo) recordsFileInfo;
    assertThat(sanBernardoRecordFileInfo.getBuoniOrdiniSanBernardoMap().entrySet())
      .hasSize(0);

  }
}
