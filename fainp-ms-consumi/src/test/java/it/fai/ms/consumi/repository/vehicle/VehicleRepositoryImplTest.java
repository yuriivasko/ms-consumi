package it.fai.ms.consumi.repository.vehicle;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.repository.storico_dml.ViewStoricoDispositivoVeicoloContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackageClasses = ViewStoricoDispositivoVeicoloContratto.class)
@DataJpaTest
@EntityScan(basePackageClasses = ViewStoricoDispositivoVeicoloContratto.class)
@EnableJpaRepositories(repositoryBaseClass = ViewStoricoDispositivoVeicoloContrattoRepository.class)
@Tag("unit")
@Transactional
class VehicleRepositoryImplTest {

  @Autowired
  private EntityManager em;

  private VehicleRepository vehicleRepository;
  private StoricoVeicolo v_1 = new StoricoVeicolo();
  private StoricoVeicolo v_2 = new StoricoVeicolo();
  private StoricoVeicolo v_3 = new StoricoVeicolo();

  @BeforeEach
  void setUp() throws Exception {


    v_1.setDmlUniqueIdentifier("::1::");
    v_1.setDmlRevisionTimestamp(Instant.ofEpochSecond(12));
    v_1.setClasseEuro("::euroClass::");
    v_1.setNazione("IT");
    v_1.setTarga("::licenseId_1::");

    v_2.setDmlUniqueIdentifier("::2::");
    v_2.setDmlRevisionTimestamp(Instant.ofEpochSecond(32));
    v_2.setClasseEuro("::euroClass::");
    v_2.setNazione("IT");
    v_2.setTarga("::targa::");
    
    v_3.setDmlUniqueIdentifier("::3::");
    v_3.setDmlRevisionTimestamp(Instant.now());
    v_3.setClasseEuro("::Classe::");
    v_3.setNazione("IT");
    v_3.setTarga("::targa::");

    em.persist(v_1);
    em.persist(v_2);
    em.persist(v_3);

    em.flush();
  }

  @Test
  void testFindVehicleByDeviceUuid() {

    // given

    vehicleRepository = new VehicleRepositoryImpl(em, new VehicleEntityMapper());

    // when

    var optionalVehicleByDeviceUuid = vehicleRepository.findVehicleByVehicleUuid("::1::", Instant.ofEpochSecond(12));

    // then

    assertThat(optionalVehicleByDeviceUuid).contains(newVehicle());
  }

  @Test
  void when_device_exists_and_endDate_isNull() {

    // given

    vehicleRepository = new VehicleRepositoryImpl(em, new VehicleEntityMapper());

    // when

    var optionalVehicleByDeviceUuid = vehicleRepository.findVehicleByVehicleUuid("::1::", Instant.ofEpochSecond(32));

    // then

    assertThat(optionalVehicleByDeviceUuid).contains(newVehicle());
  }

  private Vehicle newVehicle() {
    var vehicle = new Vehicle();
    vehicle.setEuroClass("::euroClass::");
    vehicle.setFareClass("");
    vehicle.setLicensePlate(new VehicleLicensePlate("::licenseId_1::", "IT"));
    return vehicle;
  }

  @Test
  void testFindVehicleByDeviceUuid_notExists() {

    // given

    vehicleRepository = new VehicleRepositoryImpl(em, new VehicleEntityMapper());

    // when

    var optionalVehicleByDeviceUuid = vehicleRepository.findVehicleByVehicleUuid("::deviceUuidNotPresent::", Instant.ofEpochSecond(12));

    // then

    assertThat(optionalVehicleByDeviceUuid).isNotPresent();
  }
}
