package it.fai.ms.consumi.service.processor;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;
import it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory;

@Tag("unit")
class ConsumoAbstractProcessorTest {

  private final   ConsumoValidatorService consumoValidatorService = mock(ConsumoValidatorService.class);
  protected final CustomerService         customerService = mock(CustomerService.class);
  protected final VehicleService          vehicleService = mock(VehicleService.class);

  private final Consumo consumoOK = mock(Consumo.class);
  private final Consumo consumoFAIL = mock(Consumo.class);

  private ConsumoProcessorTestable consumoAbstractProcessor;
  private class ConsumoProcessorTestable
    extends ConsumoAbstractProcessor{
    public ConsumoProcessorTestable(ConsumoValidatorService _consumoValidatorService,
                                    VehicleService _vehicleService,
                                    CustomerService _customerService) {
      super(_consumoValidatorService, _vehicleService, _customerService);
    }
  }

  @BeforeEach
  void setUp() {
    consumoAbstractProcessor = new ConsumoProcessorTestable(consumoValidatorService,  vehicleService, customerService);

    given(consumoValidatorService.validate(consumoOK))
                           .willReturn(ValidationOutcomeTestFactory.newValidationOutcome());

    given(consumoValidatorService.validate(consumoFAIL))
      .willReturn(ValidationOutcomeTestFactory.newValidationOutcomeFailure("::failure::", "::text::"));
  }

  @Test
  void validateAndNotify_ok_should_NOT_send_notitication() {
    consumoAbstractProcessor.validateAndNotify(consumoOK);

    verify(consumoValidatorService,times(0)).notifyProcessingResult(Mockito.any());
  }

  @Test
  void validateAndNotify_failiure_should_send_notitication() {
    ProcessingResult<?> rs = consumoAbstractProcessor.validateAndNotify(consumoFAIL);
    
    Optional<Message> code = rs.getValidationOutcome().getMessages().stream().filter(m -> m.getCode().contains("::failure::")).findFirst();
    Assertions.assertTrue(code.isPresent());
  }

}
