package it.fai.ms.consumi.service.record.hgv;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.client.CountryDTO;
import it.fai.ms.consumi.client.CountryService;
import it.fai.ms.consumi.client.CountryServiceFactory;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.repository.flussi.record.model.generici.HgvRecord;
import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@Tag("unit")
class HgvRecordConsumoGenericoMapperTest {

  CountryServiceFactory          countryServiceFactory  = mock(CountryServiceFactory.class);
  CountryService                 countryService  = mock(CountryService.class);
  HgvRecordConsumoGenericoMapper hgvRecordConsumoMapper = new HgvRecordConsumoGenericoMapper(countryServiceFactory);

  private HgvRecord hgvRecord;

  @BeforeEach
  public void setUp(){
    hgvRecord = newHgvRecord();
    given(countryServiceFactory.getCountryService())
      .willReturn(countryService);

    final CountryDTO t = new CountryDTO();
    t.setCodiceIso2("IT");
    given(countryService.getBySiglaAuto(any()))
      .willReturn(t);
  }

  private HgvRecord newHgvRecord() {
    HgvRecord hgvRecord = new HgvRecord("levy_periods.csv", -1);
    //EM532ED,I,D,EC5,2017-04-03,2017-04-09,17.5,Pre-pagato,2017-03-31,FAISERVICE

    hgvRecord.setIngestion_time(Instant.EPOCH);
    hgvRecord.setTransactionDetail("27");
    hgvRecord.setRecordCode("DEFAULT");
    hgvRecord.setTransactionDetail("");
    hgvRecord.setNumeroImmatricolazioneVeicolo("EM532ED");
    hgvRecord.setPaese("I");
    hgvRecord.setFascia("D");
    hgvRecord.setCategoriaEuro("EC5");
    hgvRecord.setDataInizio("2017-04-03");
    hgvRecord.setDataFine("2017-04-09");
    hgvRecord.setImporto("17.5");
    hgvRecord.setStato("Pre-pagato");
    hgvRecord.setDataAcquisto("2017-03-31");
    hgvRecord.setAcquistatoDa("FAISERVICE");

    return hgvRecord;
  }

  @Test
  void mapRecordToConsumo() throws Exception {
    Hgv consumo = hgvRecordConsumoMapper.mapRecordToConsumo(newHgvRecord());
    assertThat(consumo.getSource().getIngestionTime())
      .isEqualTo(Instant.parse("1970-01-01T00:00:00Z"));
    assertThat(consumo.getSource().getAcquisitionDate())
      .isEqualTo(Instant.parse("1970-01-01T00:00:00Z"));
    assertThat(consumo.getAmount().getAmountExcludedVat())
      .isEqualTo(Money.of(new BigDecimal("17.5"), "GBP"));
    assertThat(consumo.getNavSupplierCode())
      .isNull();
    assertThat(consumo.getContract().isPresent())
      .isFalse();

    assertThat(consumo.getStartDate())
      .isEqualTo(FaiConsumiDateUtil.calculateInstantByDate("2017-04-03", "yyyy-MM-dd").get());
    assertThat(consumo.getEndDate())
      .isEqualTo(FaiConsumiDateUtil.calculateInstantByDate("2017-04-09","yyyy-MM-dd").get());

    assertThat(consumo.getDate())
      .isEqualTo(FaiConsumiDateUtil.calculateInstantByDate("2017-04-09", "yyyy-MM-dd").get());

    assertThat(consumo.getDevice())
      .isNotNull();
    assertThat(consumo.getDevice().getType())
      .isEqualTo(TipoDispositivoEnum.HGV);

    assertThat(consumo.getDevice().getServiceType())
      .isEqualTo(TipoServizioEnum.PEDAGGI_INGHILTERRA);

    assertThat(consumo.getGlobalIdentifier().getId())
      .isNotNull();
    assertThat(consumo.getGroupArticlesNav())
      .isNull();
    assertThat(consumo.getInvoiceType())
      .isEqualTo(InvoiceType.D);
    assertThat(consumo.getPartnerCode())
      .isNull();
    assertThat(consumo.getProcessed())
      .isFalse();
    assertThat(consumo.getRecordCode())
      .isEqualTo("DEFAULT");
    assertThat(consumo.getRecordCodeToLookInParamStanz())
      .isEqualTo("DEFAULT");
    assertThat(consumo.getRegion())
      .isNull();
    assertThat(consumo.getRoute())
      .isNull();
    assertThat(consumo.getServicePartner())
      .isNull();
    assertThat(consumo.getSource().getFileName())
      .isEqualTo("levy_periods.csv");

    assertThat(consumo.getSource().getRowNumber())
      .isEqualTo(-1);
    assertThat(consumo.getSource().getType())
      .isEqualTo("HGV");
    assertThat(consumo.getTransaction().getSign())
      .isEqualTo("+");
    assertThat(consumo.getTransaction().getDetailCode())
      .isEqualTo("");
    assertThat(consumo.getAmount().getVatRate()).isEmpty();
    assertThat(consumo.getAmount().getVatRateBigDecimal()).isEqualTo(BigDecimal.ZERO);
    assertThat(consumo.getVehicle())
      .isNotNull();
    assertThat(consumo.getVehicle().getLicensePlate().getCountryId())
      .isEqualTo("IT");
    assertThat(consumo.getVehicle().getLicensePlate().getLicenseId())
      .isEqualTo("EM532ED");
    assertThat(consumo.getVehicle().getEuroClass())
      .isEqualTo("5");

    //FIXME
//    assertThat(consumo.getVehicle().getFareClass())
//      .isEqualTo("D");
  }

}
