package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.conf;

import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.money.Monetary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.adapter.nav.auth.NavAuthorizationTokenService;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.client.AuthorizationApiClient;
import it.fai.ms.consumi.client.GeoClient;
import it.fai.ms.consumi.client.PetrolPumpClient;
import it.fai.ms.consumi.client.StanziamentiApiAuthClient;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.navision.PricesByNavDTO;
import it.fai.ms.consumi.domain.navision.PricesByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.PutAllegatiByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.Stanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.dto.ArticoloDTO;
import it.fai.ms.consumi.dto.FornitoreDTO;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.service.navision.NavArticlesService;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;

@Profile({"default", "integration"})
@Configuration
public class MockedNavServicesConfiguration {

  private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.of("Europe/Berlin"));

  @Bean
  @Primary
  public PriceCalculatedByNavService priceCalculatedByNavService() {
    return new PriceCalculatedByNavServiceMocked();
  }

  @Bean
  @Primary
  public NavArticlesService navArticlesService() {
    System.out.println("Loading Nav Article service mocked");
    return new NavArticlesServiceMocked();
  }

  class NavArticlesServiceMocked
    implements NavArticlesService {

    @Override
    public List<Article> getAllArticlesAndUpdateStanziamentiParams(AllStanziamentiParams stanziamentiParams) {
      return new ArrayList<>();
    }
  }

  class NavAuthorizationTokenServiceMocked
    implements NavAuthorizationTokenService {

    @Override
    public String getAuthorizationToken() {
      return "mockedToken";
    }
  }

  class PriceCalculatedByNavServiceMocked
    extends PriceCalculatedByNavService {

    public PriceCalculatedByNavServiceMocked() {
      super(new NavAuthorizationTokenServiceMocked());
    }

    public PriceCalculatedByNavServiceMocked(NavAuthorizationTokenService navAuthorizationTokenService) {
      super(new NavAuthorizationTokenServiceMocked());
    }

    public void setPriceCalculatedAmountByNav(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                              StanziamentiParams _stanziamentiParams) {
      _dettaglioStanziamentoCarburante.setPriceComputed(new Amount());
      setCalculatedAmountByNav(_dettaglioStanziamentoCarburante, _stanziamentiParams, _dettaglioStanziamentoCarburante.getPriceReference(),
                               _dettaglioStanziamentoCarburante.getPriceComputed(), PricesByNavDTO.CostoRicavoEnum.NUMBER_1);
    }

    public void setCostCalculatedAmountByNav(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                             StanziamentiParams _stanziamentiParams) {
      _dettaglioStanziamentoCarburante.setCostComputed(new Amount());
      setCalculatedAmountByNav(_dettaglioStanziamentoCarburante, _stanziamentiParams, _dettaglioStanziamentoCarburante.getPriceReference(),
                               _dettaglioStanziamentoCarburante.getCostComputed(), PricesByNavDTO.CostoRicavoEnum.NUMBER_0);
    }

    private void setCalculatedAmountByNav(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                          StanziamentiParams _stanziamentiParams,
                                          Amount amountReference,
                                          Amount amountComputed,
                                          PricesByNavDTO.CostoRicavoEnum costoRicavoEnum) {
      Number importoNoIva = new BigDecimal(amountReference.getAmountExcludedVat().getNumber().toString(), MathContext.DECIMAL32);

      PricesByNavDTO req = new PricesByNavDTO();
      req.setClasseTariffaria(_stanziamentiParams.getFareClass());
      req.setCodiceArticoloNav(_stanziamentiParams.getArticle().getCode());
      req.setCodiceCliente(_dettaglioStanziamentoCarburante.getCustomer().getId());
      req.setCodiceFornitoreNav(_dettaglioStanziamentoCarburante.getNavSupplierCode());
      req.setCostoRicavo(costoRicavoEnum); // 0 = Costo (passivo), 1 = Ricavo (attivo)
      req.setDataDiRiferimento(FORMATTER.format(_dettaglioStanziamentoCarburante.getEntryDateTime()));
      req.setImportoNoIvaRiferimento(importoNoIva.doubleValue());

      Number tmp = importoNoIva;
      BigDecimal valueRes = new BigDecimal(tmp.toString(), MathContext.DECIMAL32);

      amountComputed.setAmountExcludedVat(
        Monetary.getDefaultAmountFactory().setCurrency(amountReference.getAmountExcludedVat().getCurrency()).setNumber(valueRes).create());
      amountComputed.setVatRate(BigDecimal.valueOf(_stanziamentiParams.getArticle().getVatRate()));
    }

    public BigDecimal getCalculatedAmountByNav(DettaglioStanziamentoCarburanteEntity _dettaglioStanziamentoCarburanteEntity,
                                               StanziamentiParams _stanziamentiParams,
                                               Number importoNoIva,
                                               PricesByNavDTO.CostoRicavoEnum costoRicavoEnum) {
      PricesByNavDTO req = new PricesByNavDTO();
      req.setClasseTariffaria(_stanziamentiParams.getFareClass());
      req.setCodiceArticoloNav(_stanziamentiParams.getArticle().getCode());
      req.setCodiceCliente(_dettaglioStanziamentoCarburanteEntity.getCustomerId());
      req.setCodiceFornitoreNav(_dettaglioStanziamentoCarburanteEntity.getCodiceFornitoreNav());
      req.setCostoRicavo(costoRicavoEnum); // 0 = Costo (passivo), 1 = Ricavo (attivo)
      req.setDataDiRiferimento(FORMATTER.format(_dettaglioStanziamentoCarburanteEntity.getDataOraUtilizzo()));
      req.setImportoNoIvaRiferimento(importoNoIva.doubleValue());

      Number tmp = importoNoIva;
      BigDecimal valueRes = new BigDecimal(tmp.toString(), MathContext.DECIMAL32);

      return valueRes;
    }
  }

  @Bean
  public StanziamentiApiAuthClient stanziamentiApiAuth() {
    return new StanziamentiApiAuthClientImpl();
  }

  class StanziamentiApiAuthClientImpl
    implements StanziamentiApiAuthClient {

    @Override
    public void stanziamentiPost(String authorizationToken, List<Stanziamento> righe) {
      // TODO Auto-generated method stub

    }

    @Override
    public PutAllegatiByNavReturnDTO putAllegatiByNav(String requestHeader, String numeroFattura, List<String> codiciAllegati) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public PricesByNavReturnDTO getPricesByNavPost(String requestHeader, PricesByNavDTO pricesByNavDTO) {
      // TODO Auto-generated method stub
      PricesByNavReturnDTO dto = new PricesByNavReturnDTO();
      dto.setPrezzoCalcolatoNoIva(pricesByNavDTO.getImportoNoIvaRiferimento().doubleValue() + 0.10);
      return dto;
    }

  }

  @Bean
  public AnagaziendeClient anagaziendeClient() {
    return new AnagaziendeClient() {

      @Override
      public List<ArticoloDTO> getAllArticoli(String authorizationToken) {
        // TODO Auto-generated method stub
        return null;
      }

      @Override
      public List<FornitoreDTO> getAllFornitori(String authorizationToken) {
        // TODO Auto-generated method stub
        return null;
      }

    };
  }

  @Bean
  public AuthorizationApiClient authorizationApiClient() {
    return mock(AuthorizationApiClient.class);
  }

  @Bean
  public GeoClient geoClient(){
    return mock(GeoClient.class);
  }

  @Bean
  public StanziamentiApiAuthClient stanziamentiApiAuthClient(){
    return mock(StanziamentiApiAuthClient.class);
  }

  @Bean
  public PetrolPumpClient petrolPumpClient(){
    return mock(PetrolPumpClient.class);
  }

  @Bean
  public ElasticSearchFeignClient elasticSearchFeignClient(){
    return mock(ElasticSearchFeignClient.class);
  }
}
