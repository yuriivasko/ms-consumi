package it.fai.ms.consumi.service.record;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaTestBuilder;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.job.JobRegistry;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.SingleRecordProcessingResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@Tag("unit")
class RecordConsumoReprocessorImplTest {

  private RecordConsumoReprocessor recordConsumoReprocessor;

  private final ElviaJob job = mock(ElviaJob.class);
  private final ElviaDescriptor elviaDescriptor = new ElviaDescriptor();

  private final JobRegistry jobRegistry = mock(JobRegistry.class);
  private final NotificationService notificationService =  mock(NotificationService.class);

  @BeforeEach
  public void setUp(){
    this.recordConsumoReprocessor = new RecordConsumoReprocessorImpl(jobRegistry, notificationService);
  }

  @Test
  void processSingleRecord_if_result_is_erro_should_return_not_ok_result() throws Exception {

    given(jobRegistry.findJobByServiceProviderFormat(any()))
      .willReturn(Optional.of(job));

    SingleRecordProcessingResult singleRecordProcessingResult = new SingleRecordProcessingResult(new RuntimeException("This is an exception to test behaviour"));
    given(job.processSingleRecord(any(), any(), any(), any()))
      .willReturn(singleRecordProcessingResult);

    ElviaRecord foundRecord = elviaDescriptor.decodeRecordCodeAndCallSetFromString(
      "DT276848690120609680PR2018-06-0417.58.24000000003200000000000000000032ATAPPARATO TELEPASS   0450561451          FIORENZUOLA  PIACENZA SUD           A 2018-07-23+010006000520180323IVA 22%   2200000G",
      "DT",
      "FILENAME",
      -1
    );
    foundRecord.setServicePartner(new ServicePartner(""));

    SingleRecordProcessingResult result = recordConsumoReprocessor.processSingleRecord(foundRecord);

    assertThat(result.isOK()).isFalse();
  }

  @Test
  void processSingleRecord_if_result_is_erro_should_return_ok_result() throws Exception {

    given(jobRegistry.findJobByServiceProviderFormat(any()))
      .willReturn(Optional.of(job));

    SingleRecordProcessingResult singleRecordProcessingResult = new SingleRecordProcessingResult(new ProcessingResult(ElviaTestBuilder.newInstance().build(), new ValidationOutcome()));
    given(job.processSingleRecord(any(), any(), any(), any()))
      .willReturn(singleRecordProcessingResult);

    ElviaRecord foundRecord = elviaDescriptor.decodeRecordCodeAndCallSetFromString(
      "DT276848690120609680PR2018-06-0417.58.24000000003200000000000000000032ATAPPARATO TELEPASS   0450561451          FIORENZUOLA  PIACENZA SUD           A 2018-07-23+010006000520180323IVA 22%   2200000G",
      "DT",
      "FILENAME",
      -1
    );
    foundRecord.setServicePartner(new ServicePartner(""));

    SingleRecordProcessingResult result = recordConsumoReprocessor.processSingleRecord(foundRecord);

    assertThat(result.isOK()).isTrue();
  }
}
