package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.notconfirmedtempallocation;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.GlobalIdentifierType;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.web.rest.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@ComponentScan(basePackages = {
  "it.fai.ms.consumi.service",
  "it.fai.ms.consumi.client",
  "it.fai.ms.consumi.adapter",
  "it.fai.ms.consumi.repository",
  "it.fai.ms.consumi.scheduler",
  "it.fai.ms.consumi.job",
  "it.fai.ms.consumi.domain.dettaglio_stanziamento"},
  excludeFilters = {
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"),
  })
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class NotConfirmedTemporaryAllocationsServiceIntTest
  extends AbstractIntegrationTestSpringContext {


  private String fileName = "O_DTC5659_IZ_2_0411032037_S_051117_185190";
  private String filePath = "";


  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @MockBean
  private NotificationService notificationService;

  @Autowired
  private NotConfirmedTemporaryAllocationsService service;
  //
  @Autowired
  private StanziamentoRepository stanziamentoRepository;

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentiRepository;

  @Autowired
  private EntityManager em;


  @BeforeEach
  public void init() throws Exception {
    assertThat(service).isNotNull();
    String stanziamentoCode = "codStanz_";
    String customerId = "0041015";
    Instant dataAcquisizioneFlusso = TestUtils.fromStringToInstant("2018-02-01 00:00:00");
    for (int i = 0; i < 10; i++) {
      int minimum = 20;
      int maximum = 200;
      int randomNum = minimum + (int) (Math.random() * (maximum - minimum));
      BigDecimal amountNoVat = new BigDecimal(randomNum);
      BigDecimal amountWithVat = amountNoVat.add(amountNoVat.multiply(new BigDecimal(22).divide(new BigDecimal(100))));

      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity = createDettagliostanziamentoPedaggio(
        customerId, dataAcquisizioneFlusso,
        UUID.randomUUID().toString() + "#" + GlobalIdentifierType.INTERNAL, "elceu", TipoDispositivoEnum.TELEPASS_EUROPEO,
        InvoiceType.P, "EUR", "DA06A284.ELVIA.FAI.N00002", "DT",
        "+", amountNoVat, new BigDecimal(22), amountWithVat,
        "AUT_ES", "00049000000646210021", "00646210021",
        "0610", "0006", "PE", "5",
        "5", "PL", "ST0947G");

      StanziamentoEntity stanziamentoEntity = persistStanziamento(stanziamentoCode + "_" + i,
        new BigDecimal(1l),
        InvoiceType.P,
        null,
        dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
        dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
        false,
        null,
        GeneraStanziamento.COSTO_RICAVO,
        "ITAPE01",
        "targa1",
        TypeFlow.MYFAI,
        TestUtils.fromStringToLocalDate("2018-01-01"),
        null,
        TestUtils.fromStringToInstant("2018-02-01 00:00:00"),
        dataAcquisizioneFlusso,
        2018,
        "5", "F000304", "IT", customerId,
        dettaglioStanziamentoPedaggioEntity,
        dataAcquisizioneFlusso,
        "IT");

      dettaglioStanziamentoPedaggioEntity.getStanziamenti().add(stanziamentoEntity);
      em.persist(dettaglioStanziamentoPedaggioEntity);
    }
    for (int i = 20; i < 50; i++) {
      int minimum = 20;
      int maximum = 200;
      int randomNum = minimum + (int) (Math.random() * (maximum - minimum));
      BigDecimal amountNoVat = new BigDecimal(randomNum);
      BigDecimal amountWithVat = amountNoVat.add(amountNoVat.multiply(new BigDecimal(22).divide(new BigDecimal(100))));
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity = createDettagliostanziamentoPedaggio(customerId, dataAcquisizioneFlusso,
        UUID.randomUUID().toString() + "#" + GlobalIdentifierType.INTERNAL, "elcit",
        TipoDispositivoEnum.TELEPASS_EUROPEO,
        InvoiceType.P, "EUR", "DA06A284.ELVIA.FAI.N00002", "DT",
        "+", amountNoVat, new BigDecimal(22), amountWithVat,
        "AUT_IT", "00049000000646210021", "00646210021",
        "0610", "0006", "PE", "5",
        "5", "PL", "ST0947G");
      StanziamentoEntity stanziamentoEntity = persistStanziamento(stanziamentoCode + "_" + i,
        new BigDecimal(1l),
        InvoiceType.P,
        null,
        dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
        dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
        false,
        null,
        GeneraStanziamento.COSTO_RICAVO,
        "ITAPE01",
        "targa1",
        TypeFlow.MYFAI,
        TestUtils.fromStringToLocalDate("2018-01-01"),
        null,
        TestUtils.fromStringToInstant("2018-02-01 00:00:00"),
        dataAcquisizioneFlusso,
        2018,
        "5", "F000304", "IT", customerId,
        dettaglioStanziamentoPedaggioEntity,
        dataAcquisizioneFlusso,
        "IT");
      dettaglioStanziamentoPedaggioEntity.getStanziamenti().add(stanziamentoEntity);
      em.persist(dettaglioStanziamentoPedaggioEntity);
    }
    dataAcquisizioneFlusso = TestUtils.fromStringToInstant("2018-07-01 00:00:00");
    int minimum = 20;
    int maximum = 200;
    int randomNum = minimum + (int) (Math.random() * (maximum - minimum));
    BigDecimal amountNoVat = new BigDecimal(randomNum);
    BigDecimal amountWithVat = amountNoVat.add(amountNoVat.multiply(new BigDecimal(22).divide(new BigDecimal(100))));
    DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity = createDettagliostanziamentoPedaggio(customerId, dataAcquisizioneFlusso,
      UUID.randomUUID().toString() + "#" + GlobalIdentifierType.INTERNAL, "elceu",
      TipoDispositivoEnum.TELEPASS_EUROPEO,
      InvoiceType.P, "EUR", "DA06A284.ELVIA.FAI.N00002", "DT",
      "+", amountNoVat, new BigDecimal(22), amountWithVat,
      "AUT_IT", "00049000000646210021", "00646210021",
      "0610", "0006", "PE", "5",
      "5", "PL", "ST0947G");
    StanziamentoEntity stanziamentoEntity = persistStanziamento(stanziamentoCode + "_200000",
      new BigDecimal(1l),
      InvoiceType.P,
      null,
      dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
      dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
      false,
      null,
      GeneraStanziamento.COSTO_RICAVO,
      "ITAPE01",
      "targa1",
      TypeFlow.MYFAI,
      TestUtils.fromStringToLocalDate("2018-01-01"),
      null,
      TestUtils.fromStringToInstant("2018-02-01 00:00:00"),
      dataAcquisizioneFlusso,
      2018,
      "5", "F000304", "IT", customerId,
      dettaglioStanziamentoPedaggioEntity,
      dataAcquisizioneFlusso,
      "IT");
    dettaglioStanziamentoPedaggioEntity.getStanziamenti().add(stanziamentoEntity);
    em.persist(dettaglioStanziamentoPedaggioEntity);


    randomNum = minimum + (int) (Math.random() * (maximum - minimum));
    amountNoVat = new BigDecimal(randomNum);
    amountWithVat = amountNoVat.add(amountNoVat.multiply(new BigDecimal(22).divide(new BigDecimal(100))));
    dettaglioStanziamentoPedaggioEntity = createDettagliostanziamentoPedaggio(customerId, dataAcquisizioneFlusso,
      UUID.randomUUID().toString() + "#" + GlobalIdentifierType.INTERNAL, "elcit",
      TipoDispositivoEnum.TELEPASS_EUROPEO,
      InvoiceType.P, "EUR", "DA06A284.ELVIA.FAI.N00002", "DT",
      "+", amountNoVat, new BigDecimal(22), amountWithVat,
      "AUT_IT", "00049000000646210021", "00646210021",
      "0610", "0006", "PE", "5",
      "5", "PL", "ST0947G");
    stanziamentoEntity = persistStanziamento(stanziamentoCode + "_100000",
      new BigDecimal(1l),
      InvoiceType.P,
      null,
      dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
      dettaglioStanziamentoPedaggioEntity.getAmountNoVat(),
      false,
      null,
      GeneraStanziamento.COSTO_RICAVO,
      "ITAPE01",
      "targa1",
      TypeFlow.MYFAI,
      TestUtils.fromStringToLocalDate("2018-01-01"),
      null,
      TestUtils.fromStringToInstant("2018-02-01 00:00:00"),
      dataAcquisizioneFlusso,
      2018,
      "5", "F000304", "IT", customerId,
      dettaglioStanziamentoPedaggioEntity,
      dataAcquisizioneFlusso,
      "IT");

    dettaglioStanziamentoPedaggioEntity.getStanziamenti().add(stanziamentoEntity);
    em.persist(dettaglioStanziamentoPedaggioEntity);


  }


  @Test
  public void elaborateTwoRecords_generate_two_stanziamenti_two_dettagli_stanziamento() {
    /*
      41 stanziamenti provvisori inseriti:
      - 10 vacon con data acquisizione  01 feb 2018
      - 29 elvia con data acquisizione 01 feb 2018
      - 1 vacon con data acquisizione 01 Lug 2018
      - 1 elvia con data acquisizione 01 Lug 2018

     */

    service.processSource("elceu", TestUtils.fromStringToInstant("2018-06-01 00:00:00"));
    service.processSource("elcit", TestUtils.fromStringToInstant("2018-06-01 00:00:00"));
    List<DettaglioStanziamentoPedaggioEntity> dettagliStanziamentiAll = dettaglioStanziamentiRepository.findAllDettaglioStanziamentiPedaggio();
    List<StanziamentoEntity> stanziamentiAll = dettaglioStanziamentiRepository.findAllStanziamenti();
    //totale di 82 stanziamenti
    assertThat(stanziamentiAll.size()).isEqualTo(82);
    assertThat(dettagliStanziamentiAll.size()).isEqualTo(82);
    //elceu
    assertElaborationDone(
      dettagliStanziamentiAll,
      21,
      "elceu",
      TestUtils.fromStringToInstant("2018-06-01 00:00:00"),
      20, //10 inseriti + 10 conguagli
      10,
      0.00d,
      10);
    //elcit
    assertElaborationDone(dettagliStanziamentiAll,
      61,
      "elcit",
      TestUtils.fromStringToInstant("2018-06-01 00:00:00"),
      60, //30 inseriti + 30 conguagli
      30,
      0.00d,
      30
    );

  }

  protected void assertElaborationDone(List<DettaglioStanziamentoPedaggioEntity> dettagliStanziamentiAll, int expectedTotalCount, String sourceType, Instant fromInstant, int expectedExpiredCount, int expectedCountDettagliInseriti, double expectedTotalDettagliStanziamenti, int expectedCountNegativeStanziamenti) {
    List<DettaglioStanziamentoPedaggioEntity> dettagliStanziamentiPedaggi = dettagliStanziamentiAll
      .stream()
      .filter(dsp -> dsp.getSourceType().equals(sourceType))
      .collect(Collectors.toList());
    assertThat(dettagliStanziamentiPedaggi).hasSize(expectedTotalCount);
    List<DettaglioStanziamentoPedaggioEntity> dettagliStanziamentiPrecedenti = dettagliStanziamentiPedaggi.stream().filter(dsp -> dsp.getDataAcquisizioneFlusso().isBefore(fromInstant)).collect(Collectors.toList());
    //10 esistenti + 10 di conguaglio
    assertThat(dettagliStanziamentiPrecedenti.size()).isEqualTo(expectedExpiredCount);
    //la somma dei valori di quelli esistenti deve pareggiare
    BigDecimal totaleProvvisorio = dettagliStanziamentiPrecedenti.stream()
      .map(dsp -> dsp.getAmountIncludingVat())
      .reduce(BigDecimal.ZERO, BigDecimal::add);
    //10 dettagli stanziamenti devono essere definitivi con segno negativo
    List<DettaglioStanziamentoPedaggioEntity> dettagliStanziamentoPedaggioInseriti = (dettagliStanziamentiPrecedenti.stream().filter(dsp -> dsp.getInvoiceType().equals(InvoiceType.D) && dsp.getAmountIncludingVat().signum() == -1).collect(Collectors.toList()));
    assertThat(dettagliStanziamentoPedaggioInseriti).hasSize(expectedCountDettagliInseriti);
    assertThat(totaleProvvisorio.doubleValue()).isEqualTo(expectedTotalDettagliStanziamenti);
    assertThat(dettagliStanziamentoPedaggioInseriti.stream().flatMap(dsp -> dsp.getStanziamenti().stream()).filter(
      s -> s.getPrezzo().signum() == -1 && !"".equals(s.getCodiceStanziamentoProvvisorio())
    ).count()).isEqualTo(expectedCountNegativeStanziamenti);
  }

  private void assertStanziamento(StanziamentoEntity stanziamentoEntity,
                                  String expectedNrCliente,
                                  String expectedTarga,
                                  String expectedTargaNazione,
                                  BigDecimal expectedCosto,
                                  BigDecimal expectedPrezzo,
                                  String expectedValuta,
                                  TypeFlow expectedTipoFlusso,
                                  GeneraStanziamento expectedGeneraStanziamento,
                                  InvoiceType expectedStatoStanziamento,
                                  LocalDate expectedDataErogazioneServizio) {
    assertThat(stanziamentoEntity.getNumeroCliente()).isEqualTo(expectedNrCliente);
    assertThat(stanziamentoEntity.getTarga()).isEqualTo(expectedTarga);
    assertThat(stanziamentoEntity.getPaese()).isEqualTo(expectedTargaNazione);
    assertThat(stanziamentoEntity.getCosto().setScale(2, RoundingMode.HALF_UP)).isEqualTo(expectedCosto.setScale(2, RoundingMode.HALF_UP));
    assertThat(stanziamentoEntity.getPrezzo().setScale(2, RoundingMode.HALF_UP)).isEqualTo(expectedPrezzo.setScale(2, RoundingMode.HALF_UP));
    assertThat(stanziamentoEntity.getValuta()).isEqualTo(expectedValuta);
    assertThat(stanziamentoEntity.getTipoFlusso()).isEqualTo(expectedTipoFlusso);
    assertThat(stanziamentoEntity.getGeneraStanziamento()).isEqualTo(expectedGeneraStanziamento);
    assertThat(stanziamentoEntity.getStatoStanziamento()).isEqualTo(expectedStatoStanziamento);
    assertThat(stanziamentoEntity.getDataErogazioneServizio()).isEqualTo(expectedDataErogazioneServizio);
  }

  private void assertStanziamentoMessage(StanziamentoMessage stanziamentoMessage,
                                         String expectedNrCliente,
                                         String expectedTarga,
                                         String expectedNazione,
                                         double expectedCosto,
                                         double expectedPrezzo,
                                         String expectedValuta,
                                         int expectedTipoFlusso,
                                         int expectedGeneraStanziamento,
                                         int expectedStatoStanziamento,
                                         LocalDate expectedDataErogazioneServizio) {
    assertThat(stanziamentoMessage.getNrCliente()).isEqualTo(expectedNrCliente);

    assertThat(stanziamentoMessage.getTarga()).isEqualTo(expectedTarga);
    assertThat(stanziamentoMessage.getPaese()).isEqualTo(expectedNazione);
    assertThat(stanziamentoMessage.getCosto()).isEqualTo(expectedCosto);
    assertThat(stanziamentoMessage.getPrezzo()).isEqualTo(expectedPrezzo);
    assertThat(stanziamentoMessage.getValuta()).isEqualTo(expectedValuta);
    assertThat(stanziamentoMessage.getTipoFlusso()).isEqualTo(expectedTipoFlusso);
    assertThat(stanziamentoMessage.getGeneraStanziamento()).isEqualTo(expectedGeneraStanziamento);
    assertThat(stanziamentoMessage.getStatoStanziamento()).isEqualTo(expectedStatoStanziamento);
    assertThat(stanziamentoMessage.getDataErogazioneServizio()).isEqualTo(expectedDataErogazioneServizio);
  }

  private void assertDettaglioStanziamento(DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoEntity,
                                           String expectedVehicleLicenceId,
                                           String expectedVehicleCountryId,
                                           BigDecimal expectedAmountExcludedVat,
                                           BigDecimal expectedAmountIncludedVat,
                                           BigDecimal expectedVat,
                                           String expectedVehicleEuroClass,
                                           String expectedDevicePan,
                                           String expectedDeviceObu,
                                           TipoDispositivoEnum expectedDeviceType,
                                           String expectedArticlesGroup
  ) {
    assertThat(dettaglioStanziamentoEntity.getVehicleLicenseLicenseId()).isEqualTo(expectedVehicleLicenceId);
    assertThat(dettaglioStanziamentoEntity.getVehicleLicenseCountryId()).isEqualTo(expectedVehicleCountryId);
    assertThat(dettaglioStanziamentoEntity.getVehicleEuroClass()).isEqualTo(expectedVehicleEuroClass);
    assertThat(dettaglioStanziamentoEntity.getAmountNoVat().setScale(2, RoundingMode.HALF_EVEN)).isEqualTo(expectedAmountExcludedVat.setScale(2, RoundingMode.HALF_EVEN));
    assertThat(dettaglioStanziamentoEntity.getAmountIncludingVat().setScale(2, RoundingMode.HALF_EVEN)).isEqualTo(expectedAmountIncludedVat.setScale(2, RoundingMode.HALF_EVEN));
    assertThat(dettaglioStanziamentoEntity.getAmountVatRate().setScale(2, RoundingMode.HALF_EVEN)).isEqualTo(expectedVat.setScale(2, RoundingMode.HALF_EVEN));

    assertThat(dettaglioStanziamentoEntity.getArticlesGroup()).isEqualTo(expectedArticlesGroup);

    assertThat(dettaglioStanziamentoEntity.getDevicePan()).isEqualTo(expectedDevicePan);
    assertThat(dettaglioStanziamentoEntity.getDeviceObu()).isEqualTo(expectedDeviceObu);
    assertThat(dettaglioStanziamentoEntity.getDeviceType()).isEqualTo(expectedDeviceType);
  }


  protected DettaglioStanziamentoPedaggioEntity createDettagliostanziamentoPedaggio(String customerId, Instant dataAcquisizioneFlusso, String globalIdentifier, String sourceType, TipoDispositivoEnum deviceType, InvoiceType invoiceType, String currencyCode, String fileName, String recordCode, String transactionSign, BigDecimal amountExcludedVat, BigDecimal amountVatRate, BigDecimal amountIncludedVat, String raggruppamentoArticoliNav, String deviceObu, String devicePan, String entryGlobalGateIdentifier, String exitGlobalGateIdentifier, String transactionDetailCode, String vehicleEuroClass, String vehicleFareClass, String vehicleLicenseCountryId, String vehicleLicenseLicenseId) {
    DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity = new DettaglioStanziamentoPedaggioEntity(globalIdentifier);
    dettaglioStanziamentoPedaggioEntity.setCustomerId(customerId);
    dettaglioStanziamentoPedaggioEntity.setDataAcquisizioneFlusso(dataAcquisizioneFlusso);
    dettaglioStanziamentoPedaggioEntity.setSourceType(sourceType);
    dettaglioStanziamentoPedaggioEntity.setDeviceType(deviceType);
    dettaglioStanziamentoPedaggioEntity.setInvoiceType(invoiceType);
    dettaglioStanziamentoPedaggioEntity.setCurrencyCode(currencyCode);
    dettaglioStanziamentoPedaggioEntity.setFileName(fileName);
    dettaglioStanziamentoPedaggioEntity.setCodiceFornitoreNav("::codiceFornitore::");
    dettaglioStanziamentoPedaggioEntity.setIngestionTime(dataAcquisizioneFlusso);
    dettaglioStanziamentoPedaggioEntity.setRecordCode(recordCode);
    dettaglioStanziamentoPedaggioEntity.setTransactionSign(transactionSign);
    dettaglioStanziamentoPedaggioEntity.setAmountExcludedVat(amountExcludedVat);
    dettaglioStanziamentoPedaggioEntity.setAmountVatRate(amountVatRate);
    dettaglioStanziamentoPedaggioEntity.setAmountIncludedVat(amountIncludedVat);
    dettaglioStanziamentoPedaggioEntity.setRaggruppamentoArticoliNav(raggruppamentoArticoliNav);
    dettaglioStanziamentoPedaggioEntity.setDeviceObu(deviceObu);
    dettaglioStanziamentoPedaggioEntity.setDevicePan(devicePan);
    dettaglioStanziamentoPedaggioEntity.setEntryGlobalGateIdentifier(entryGlobalGateIdentifier);
    dettaglioStanziamentoPedaggioEntity.setExitGlobalGateIdentifier(exitGlobalGateIdentifier);
    dettaglioStanziamentoPedaggioEntity.setTransactionDetailCode(transactionDetailCode);
    dettaglioStanziamentoPedaggioEntity.setVehicleEuroClass(vehicleEuroClass);
    dettaglioStanziamentoPedaggioEntity.setVehicleFareClass(vehicleFareClass);
    dettaglioStanziamentoPedaggioEntity.setVehicleLicenseCountryId(vehicleLicenseCountryId);
    dettaglioStanziamentoPedaggioEntity.setVehicleLicenseLicenseId(vehicleLicenseLicenseId);

    return dettaglioStanziamentoPedaggioEntity;
  }

  protected StanziamentoEntity persistStanziamento(String stanziamentoCode, BigDecimal quantity, InvoiceType invo, String codiceStanziamentoProvvisorio, BigDecimal costo, BigDecimal prezzo, boolean conguaglio, String numeroFattura, GeneraStanziamento costoRicavo, String numeroArticolo, String targa, TypeFlow typeFlow, LocalDate dataErogazioneServizio, String codiceClienteFatturazione, Instant dataFattura, Instant dateNavSent, int annoStanziamento, String vehicleEuroClass, String numeroFornitore, String paese, String numeroCliente, DettaglioStanziamentoEntity dettaglioStanziamento, Instant dataAcquisizioneFlusso, String nazioneTarga) {

    StanziamentoEntity stanziamentoEntity = new StanziamentoEntity(stanziamentoCode);

    //insert
    stanziamentoEntity.setQuantity(quantity);
    stanziamentoEntity.setStatoStanziamento(invo);
    stanziamentoEntity.setCodiceStanziamentoProvvisorio(codiceStanziamentoProvvisorio);
    stanziamentoEntity.setCosto(costo);
    stanziamentoEntity.setPrezzo(prezzo);
    stanziamentoEntity.setConguaglio(conguaglio);
    stanziamentoEntity.setNumeroFattura(numeroFattura);
    stanziamentoEntity.setGeneraStanziamento(costoRicavo);
    stanziamentoEntity.setNumeroArticolo(numeroArticolo);
    stanziamentoEntity.setTarga(targa);
    stanziamentoEntity.setTipoFlusso(typeFlow);
    stanziamentoEntity.setDataErogazioneServizio(dataErogazioneServizio);
    stanziamentoEntity.setCodiceClienteFatturazione(codiceClienteFatturazione);
    stanziamentoEntity.setDataFattura(dataFattura);
    stanziamentoEntity.setDateNavSent(dateNavSent);
    stanziamentoEntity.setAnnoStanziamento(annoStanziamento);
    stanziamentoEntity.setVehicleEuroClass(vehicleEuroClass);
    stanziamentoEntity.setNumeroFornitore(numeroFornitore);
    stanziamentoEntity.setPaese(paese);
    stanziamentoEntity.setNumeroCliente(numeroCliente);
    stanziamentoEntity.getDettaglioStanziamenti().add(dettaglioStanziamento);
    stanziamentoEntity.setDataAcquisizioneFlusso(dataAcquisizioneFlusso);
    stanziamentoEntity.setNazioneTarga(nazioneTarga);
    em.persist(stanziamentoEntity);
    return stanziamentoEntity;
  }
}
