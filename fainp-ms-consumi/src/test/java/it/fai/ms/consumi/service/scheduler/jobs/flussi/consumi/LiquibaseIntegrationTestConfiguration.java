package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
public class LiquibaseIntegrationTestConfiguration {

  @Autowired
  private DataSource dataSource;

  @Bean
  @Profile("!test")
  @DependsOn(value = "entityManagerFactory")
  public SpringLiquibase liquibase() {
    final LiquibaseProperties liquibaseProperties = liquibaseProperties();
    final SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setChangeLog(liquibaseProperties.getChangeLog());
    liquibase.setContexts(liquibaseProperties.getContexts());
    liquibase.setDataSource(getDataSource(liquibaseProperties));
    liquibase.setDefaultSchema(liquibaseProperties.getDefaultSchema());
    liquibase.setDropFirst(liquibaseProperties.isDropFirst());
    liquibase.setShouldRun(true);
    liquibase.setLabels(liquibaseProperties.getLabels());
    liquibase.setChangeLogParameters(liquibaseProperties.getParameters());
    return liquibase;
  }

  @Bean
  public LiquibaseProperties liquibaseProperties() {
    final LiquibaseProperties lp = new LiquibaseProperties();
    lp.setChangeLog("classpath:/config/liquibase/master.xml");
    return lp;
  }

  private DataSource getDataSource(final LiquibaseProperties liquibaseProperties) {
    if (liquibaseProperties.getUrl() == null) {
      return dataSource;
    }
    return DataSourceBuilder.create()
                            .url(liquibaseProperties.getUrl())
                            .username(liquibaseProperties.getUser())
                            .password(liquibaseProperties.getPassword())
                            .build();
  }

}
