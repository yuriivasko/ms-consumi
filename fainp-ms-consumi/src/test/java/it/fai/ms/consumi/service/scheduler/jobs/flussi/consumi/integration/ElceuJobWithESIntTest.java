package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import static java.time.Month.SEPTEMBER;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import it.fai.ms.consumi.service.CacheService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.repository.NotificationSetupRepository;
import it.fai.common.notification.service.NotificationMapper;
import it.fai.common.notification.service.NotificationMessageSender;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElceuRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElceuDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElceuJob;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;


@Tag("integration")
public class ElceuJobWithESIntTest
  extends AbstractTelepassIntTest {

  private transient final Logger log = LoggerFactory.getLogger(getClass());

  private ElceuJob                                job;

  //not work cause AbstractIntegrationTestSpringContext cannot include all services in it.fai.ms.consumi.scheduler.jobs.flussi.consumi
  private ElceuDescriptor elceuDescriptor = new ElceuDescriptor();

  @Inject
  private ConsumoProcessor<Elceu>                 elceuProcessor;

  @Inject
  private RecordConsumoMapper<ElceuRecord, Elceu> consumoMapper;

  @MockBean
  private CacheService cacheService;
  private HashMap<String, ConcurrentMapCache> caches = new HashMap<>();

  @BeforeEach
  public void setUp() {

    super.setUpMockContext("Elceu");

    job = new ElceuJob(
      transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      elceuDescriptor,
      elceuProcessor,
      consumoMapper,
      stanziamentiToNavJmsPublisher);
    ReflectionTestUtils.setField(job, "cacheService", cacheService);

    given(cacheService.getCache(any())).willAnswer((invocationOnMock) -> {
      String cacheName = (String) invocationOnMock.getArguments()[0];
      ConcurrentMapCache cache = caches.get(cacheName);
      if (cache == null) {
        cache = new ConcurrentMapCache(cacheName);
        caches.put(cacheName, cache);
      }
      return cache;
    });  }

  @Test
  @Transactional
  public void test() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elceu/DA06A284.ELCEU.FAI.N00001");

    assertThat(job.getSource()).isEqualTo("ELCEU");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();

    // FIXME
     assertThat(stanziamentoList).isNotEmpty();
    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
        .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
          .isNotNull();
      assertThat(dataAcquisizioneFlusso)
          .isNotNull();
      assertThat(ingestionTime)
          .isNotEqualTo(dataAcquisizioneFlusso);

      //from file hader 20180921
      LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(2018);
      assertThat(date.getMonth()).isEqualTo(SEPTEMBER);
      assertThat(date.getDayOfMonth()).isEqualTo(18);

      //can be broken using tests on midnight...
      date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());
    }
  }

  @Test
  public void testKOStanziamentiCheckConsistency() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elceu/DA06A284.ELCEU.FAI.N00001");

    ValidationOutcome vo = new ValidationOutcome().message(new Message("001").text("this is test error!"));
    vo.setValidationOk(false);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    assertThat(job.getSource()).isEqualTo("ELCEU");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isEmpty();

  }

  @Test
  public void parsingBelgio_6digits_scalePostProcess_2digits() throws URISyntaxException {

    final String fileAbsolutePath = getAbsolutePath("/test-files/elceu/DA06A284.ELCEU.FAI.N00001_parsingBelgium");


    job = new ElceuJob(transactionManager, stanziamentiParamsValidator, getNotificationServiceMocked(), recordPersistenceService, elceuDescriptor,
      elceuProcessor, consumoMapper, stanziamentiToNavJmsPublisher);

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .isNotEmpty()
      .extracting(s->tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getCosto(),s.getPrezzo()))
      .containsExactlyInAnyOrder(
        tuple("0_P","0000015",null,"TEPLPE",false, new BigDecimal("24.39024"),new BigDecimal("24.39024")),
        tuple("1_P","0000015",null,"TEPLPE",false, new BigDecimal("48.78048"),new BigDecimal("48.78048")),
        tuple("2_P","0064266",null,"TEBEANT",false, new BigDecimal("13.25148"),new BigDecimal("13.25148"))
      );

  }

  @Test
  public void parseGermanRecords() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/elceu/DA06A284.ELCEU_GERMANY_ELCON.EXAGE.N00001_slim");


    job = new ElceuJob(transactionManager, stanziamentiParamsValidator, getNotificationServiceMocked(), recordPersistenceService, elceuDescriptor,
      elceuProcessor, consumoMapper, stanziamentiToNavJmsPublisher);
    ReflectionTestUtils.setField(job, "cacheService", cacheService);

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .isNotEmpty()
      .extracting(s->tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getCosto(), s.getPrezzo()))
      .containsExactlyInAnyOrder(
        tuple("0_P","100589",null,"TBDELCEUDE",false, new BigDecimal("8.75000"),new BigDecimal("8.75000"))
      );

    List<DettaglioStanziamentoEntity> dettaglioStanziamentoPedaggio = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamentoPedaggio)
      .isNotEmpty()
      .extracting(d->{
        DettaglioStanziamentoPedaggioEntity dp = (DettaglioStanziamentoPedaggioEntity) d;
        return tuple(dp.getGlobalIdentifier(), dp.getAmountVatRate());
      })
      .containsExactlyInAnyOrder(
        tuple("0000000000011610#EXTERNAL", new BigDecimal("0.00"))
      );


  }

  public ServicePartner newServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.ELCEU);
    return serviceProvider;
  }
}
