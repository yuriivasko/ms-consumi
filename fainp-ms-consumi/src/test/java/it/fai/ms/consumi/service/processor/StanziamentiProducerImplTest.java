package it.fai.ms.consumi.service.processor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.AmountTestBuilder;
import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestBuilder;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.StanziamentoTestBuilder;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.DettaglioStanziamentoProcessor;

@Tag("unit")
class StanziamentiProducerImplTest {


  private static @NotNull DettaglioStanziamento newLastAllocationDetail() {
    return DettaglioStanziamentoTestBuilder.newInstance()
                                           .withInvoiceTypeD()
                                           .withAmount(AmountTestBuilder.newInstance()
                                                                        .withAmountExcludedVat(Money.of(BigDecimal.TEN, "EUR"))
                                                                        .build())
                                           .build();
  }

  private static @NotNull StanziamentiParams newStanziamentiParams() {
    return StanziamentiParamsTestBuilder.newInstance()
                                        .withSupplierCode("::supplierCode::")
                                        .withSource("::source::")
                                        .withArticle(ArticleTestBuilder.newInstance()
                                                                       .withCode("::articleCode::")
                                                                       .withCountry("::country::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .build())
                                        .build();
  }

  private final DettaglioStanziamentoProcessor mockAllocationaDetailProcessor = mock(DettaglioStanziamentoProcessor.class);
//  private final StanziamentoProcessor          mockAlocationProcessor         = mock(StanziamentoProcessor.class);
  private final CurrencyRateService            currencyRateService            = mock(CurrencyRateService.class);
  private       StanziamentiProducer           stanziamentiProducer;

  private List<Stanziamento> allocations() {
    return List.of(StanziamentoTestBuilder.newInstance()
                                          .withInvoiceTypeD()
                                          .build());
  }

  @BeforeEach
  void setUp() throws Exception {
    stanziamentiProducer = new StanziamentiProducerImpl(mockAllocationaDetailProcessor, currencyRateService);
  }

  @Test
  void testProduce() {

    // given
    given(currencyRateService.convertAmount(any(), any(), any()))
      .willAnswer(new Answer<Amount>() {
        @Override
        public Amount answer(InvocationOnMock invocationOnMock) throws Throwable {
          return invocationOnMock.getArgument(0);
        }
      });

    var newLastAllocationDetail = newLastAllocationDetail();
    var processedAllocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                                    .withInvoiceTypeD()
                                                                    .withAmount(AmountTestBuilder.newInstance()
                                                                                                 .withAmountExcludedVat(Money.of(BigDecimal.TEN,
                                                                                                                                 "EUR"))
                                                                                                 .build())
                                                                    .build();

    processedAllocationDetail.getStanziamenti().addAll(allocations());

    given(mockAllocationaDetailProcessor.process(newLastAllocationDetail,
                                                 newStanziamentiParams())).willReturn(Optional.ofNullable(processedAllocationDetail));

//    given(mockAlocationProcessor.processStanziamento(processedAllocationDetail, newStanziamentiParams())).willReturn(allocations());

    // when

    var allocations = stanziamentiProducer.produce(newLastAllocationDetail, newStanziamentiParams());

    // then

    then(mockAllocationaDetailProcessor).should()
                                        .process(eq(newLastAllocationDetail), eq(newStanziamentiParams()));

//    then(mockAlocationProcessor).should()
//                                .processStanziamento(processedAllocationDetail, newStanziamentiParams());

    assertThat(allocations).isEqualTo(allocations());
  }

  @Test
  public void process_addVatIfNeeded_will_set_vat_if_0(){

    given(currencyRateService.convertAmount(any(), any(), any()))
      .willAnswer(new Answer<Amount>() {
        @Override
        public Amount answer(InvocationOnMock invocationOnMock) throws Throwable {
          return invocationOnMock.getArgument(0);
        }
      });

    var allocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                                    .withInvoiceTypeD()
                                                                    .withAmount(AmountTestBuilder.newInstance()
                                                                                                 .withAmountExcludedVat(Money.of(BigDecimal.TEN,
                                                                                                                                 "EUR"))
                                                                                                 .withVatRate(new BigDecimal("0"))
                                                                                                 .build())
                                                                    .build();
    var stanziamentoParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withSource("::source::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCountry("::country::")
                                                                                         .withVatRate(22.0f)
                                                                                         .withGrouping("::grouping::")
                                                                                         .withMeasurementUnit("::measurementUnit::")
                                                                                         .build())
                                                          .build();

    stanziamentiProducer.produce(allocationDetail, stanziamentoParams);

    assertThat(allocationDetail.getAmount().getVatRateBigDecimal())
      .isEqualTo(new BigDecimal("22").setScale(2, RoundingMode.HALF_UP));

  }

  @Test
  public void process_addVatIfNeeded_will_NOT_set_vat_if_different_from_0(){
    given(currencyRateService.convertAmount(any(), any(), any()))
      .willAnswer(new Answer<Amount>() {
        @Override
        public Amount answer(InvocationOnMock invocationOnMock) throws Throwable {
          return invocationOnMock.getArgument(0);
        }
      });

    var allocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                           .withInvoiceTypeD()
                                                           .withAmount(AmountTestBuilder.newInstance()
                                                                                        .withAmountExcludedVat(Money.of(BigDecimal.TEN,
                                                                                                                        "EUR"))
                                                                                        .withVatRate(new BigDecimal("20"))
                                                                                        .build())
                                                           .build();
    var stanziamentoParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withSource("::source::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCountry("::country::")
                                                                                         .withVatRate(22.0f)
                                                                                         .withGrouping("::grouping::")
                                                                                         .withMeasurementUnit("::measurementUnit::")
                                                                                         .build())
                                                          .build();

    stanziamentiProducer.produce(allocationDetail, stanziamentoParams);

    assertThat(allocationDetail.getAmount().getVatRateBigDecimal())
      .isEqualTo(new BigDecimal("20").setScale(2, RoundingMode.HALF_UP));

  }

  @Test
  public void process_convertAmount(){
    var allocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                           .withInvoiceTypeD()
                                                           .withAmount(AmountTestBuilder.newInstance()
                                                                                        .withAmountExcludedVat(Money.of(BigDecimal.TEN,
                                                                                                                        "EUR"))
                                                                                        .withVatRate(new BigDecimal("20"))
                                                                                        .build())
                                                           .build();
    var stanziamentoParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withSource("::source::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCountry("::country::")
                                                                                         .withVatRate(22.0f)
                                                                                         .withGrouping("::grouping::")
                                                                                         .withMeasurementUnit("::measurementUnit::")
                                                                                         .build())
                                                          .build();
    final Amount convertedAmount = new Amount();
    given(currencyRateService.convertAmount(any(), any(), any()))
      .willAnswer(new Answer<Amount>() {
        @Override
        public Amount answer(InvocationOnMock invocationOnMock) throws Throwable {
          return convertedAmount;
        }
      });

    stanziamentiProducer.produce(allocationDetail, stanziamentoParams);

    assertThat(allocationDetail.getAmount())
      .isEqualTo(convertedAmount);
  }
}
