package it.fai.ms.consumi.service.jms.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.time.Instant;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.common.jms.dto.consumi.fuel.DresserWSTransactionRecord;
import it.fai.ms.common.jms.dto.consumi.fuel.Q8WSTransactionRecord;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;

@Tag("unit")
class FuelTransactionDTOMapperTest {

  private final FuelTransactionDTOMapper fuelTransactionDTOMapper = new FuelTransactionDTOMapper();
  private final Q8WSTransactionRecord q8WSTransactionRecord = new Q8WSTransactionRecord();
  private final DresserWSTransactionRecord dresserWSTransactionRecord = new DresserWSTransactionRecord();
  private final Instant ingestionTime = Instant.now();
  private final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
  private final ServicePartner servicePartner = mock(ServicePartner.class);
  private final String fileName = "";


  @BeforeEach
  void setUp() {
    q8WSTransactionRecord.setUsr("it63");
    // password
    q8WSTransactionRecord.setCod("3000");
    q8WSTransactionRecord.setLocal_PreAuthKey_Partner("97101807022313159");
    q8WSTransactionRecord.setLocal_PreAuthKey_FaiService("c65f6dd5d6689c360e6595b262037c4a");
    q8WSTransactionRecord.setLocal_TransactionKey_Partner("777");
    q8WSTransactionRecord.setData("2018-07-02T11:14:41.0000041+02:00");
    q8WSTransactionRecord.setUnique_TRANSACTION_FaiServiceKey("84f47e19e669ec10a7eef8e9d1629bd9");
    q8WSTransactionRecord.setNazPV("IT");
    q8WSTransactionRecord.setCodPVpartner("0988");
    q8WSTransactionRecord.setCodPVFaiService("IT63000013");
    q8WSTransactionRecord.setTerminalCode("55010988");
    q8WSTransactionRecord.setTerminalType("OUTDOOR");
    q8WSTransactionRecord.setLanguage("ITA");
    q8WSTransactionRecord.setCardNumber("7896970006629600328");
    q8WSTransactionRecord.setPin("");
    q8WSTransactionRecord.setService("GFP");
    q8WSTransactionRecord.setArticleCod_Partner("021");
    q8WSTransactionRecord.setArticleCod_FaiService("00001");
    q8WSTransactionRecord.setMessage("|");
    q8WSTransactionRecord.setUm("LT");
    q8WSTransactionRecord.setQuantity("26,06");
    q8WSTransactionRecord.setUnitAmount_VATincluded("1,535");
    q8WSTransactionRecord.setTotalAmount_VATincluded("40");
    q8WSTransactionRecord.setCurrency("EUR");
    q8WSTransactionRecord.setIvaTaxRate("2200");
    q8WSTransactionRecord.setTransactionType("+");
    q8WSTransactionRecord.setMessageNumber("1");
    q8WSTransactionRecord.setTotalMessages("1");
    q8WSTransactionRecord.setRequestType("IN");
    q8WSTransactionRecord.setVer("1.0.0");

    dresserWSTransactionRecord.setUser("IT25");
    // password
    dresserWSTransactionRecord.setFaipassNr("7896970008907100047");
    dresserWSTransactionRecord.setPointCod("IT18000010");
    dresserWSTransactionRecord.setTransData("20180702");
    dresserWSTransactionRecord.setTransOra("000718");
    dresserWSTransactionRecord.setTransArticoloCod("00001");
    dresserWSTransactionRecord.setTransQta("61,6");
    dresserWSTransactionRecord.setTransErogatore("2");
    dresserWSTransactionRecord.setTransPrezzoUnitario("1,623");
    dresserWSTransactionRecord.setTransImponibile("0");
    dresserWSTransactionRecord.setTransImposta("0");
    dresserWSTransactionRecord.setTransTotale("100");
    dresserWSTransactionRecord.setTransIvaPerc("0");
    dresserWSTransactionRecord.setTransValuta("EUR");
    dresserWSTransactionRecord.setTransKm("000001");
    dresserWSTransactionRecord.setTransNr("396");
    dresserWSTransactionRecord.setKey("84f47e19e669ec10a7eef8e9d1629bd9");
    dresserWSTransactionRecord.setLanguage("IT");

  }

  @Test
  void toTrackyCardCarburantiStandardRecord_dresser() {
    given(stanziamentiParams.getRecordCode())
      .willReturn("GFP");

    final Article article = mock(Article.class);
    given(stanziamentiParams.getArticle())
      .willReturn(article);
    given(article.getCode())
      .willReturn("GASVO01");
    given(article.getVatRate())
      .willReturn(22f);

    given(servicePartner.getPartnerCode())
      .willReturn("IT55");

    TrackyCardCarburantiStandardRecord record = fuelTransactionDTOMapper.toTrackyCardCarburantiStandardRecord(ingestionTime,
                                                                                                              stanziamentiParams,
                                                                                                              servicePartner, fileName,
                                                                                                              dresserWSTransactionRecord);
    assertThat(record.getPriceBaseVatIncluded())
      .isEqualTo("");

    assertThat(record.getMonetaryPriceReferenceVatIncluded())
      .isEqualTo(Money.of(1.623, "EUR"));

//    assertThat(record.getPriceExposedVatIncluded())
//      .isEqualTo("1,623");

//    assertThat(record.getPriceFaiReservedVatIncluded())
//      .isEqualTo(null);
//    assertThat(record.getPriceFaiReservedVatIncluded())
//      .isEqualTo("");

    assertThat(record.getCounter())
      .isEqualTo("");
    assertThat(record.getCurrency())
      .isEqualTo("EUR");
    assertThat(record.getDate())
      .isEqualTo("20180702");
    assertThat(record.getGlobalIdentifier())
      .isNotNull();
    assertThat(record.getKilometers())
      .isEqualTo("000001");
    assertThat(record.getLitres())
      .isEqualTo("61,6");
    assertThat(record.getPanField1IsoCode())
      .isEqualTo("789697");
    assertThat(record.getPanField2GroupCode())
      .isEqualTo("00");
    assertThat(record.getPanField3CustomerCode())
      .isEqualTo("089071");
    assertThat(record.getPanField4DeviceNumber())
      .isEqualTo("0004");
    assertThat(record.getPanField5ControlDigit())
      .isEqualTo("7");
    assertThat(record.getPartnerCode())
      .isEqualTo("IT55");
    assertThat(record.getPointCode())
      .isEqualTo("IT18000010");

    assertThat(record.getProductCode())
      .isEqualTo("GASVO01");
    assertThat(record.getPumpNumber())
      .isEqualTo("2");
    assertThat(record.getRecordCode())
      .isEqualTo("GFP");
    assertThat(record.getTicket())
      .isEqualTo("84f47e19e669ec10a7eef8e9d1629bd9");
    assertThat(record.getTime())
      .isEqualTo("000718");
    assertThat(record.getUnitOfMeasure())
      .isEqualTo("LT");

    assertThat(record.getVat_percentage_float())
      .isEqualTo(0f);
    assertThat(record.getVatPercentage())
      .isEqualTo("0");
    assertThat(record.getVatPercentageFloat())
      .isEqualTo(0f);
  }

  @Test
  void toTrackyCardCarburantiStandardRecord_q8() {
    given(stanziamentiParams.getRecordCode())
      .willReturn("3010");

    final Article article = mock(Article.class);
    given(stanziamentiParams.getArticle())
      .willReturn(article);
    given(article.getCode())
      .willReturn("GASVO01");
    given(article.getVatRate())
      .willReturn(22f);

    given(servicePartner.getPartnerCode())
      .willReturn("IT63");

    TrackyCardCarburantiStandardRecord record = fuelTransactionDTOMapper.toTrackyCardCarburantiStandardRecord(ingestionTime,
                                                                                                              stanziamentiParams,
                                                                                                              servicePartner, fileName,
                                                                                                              q8WSTransactionRecord);
    assertThat(record.getMonetaryPriceReferenceVatIncluded())
      .isEqualTo(Money.of(1.535, "EUR"));

//    assertThat(record.getPriceBaseVatIncluded())
//      .isEqualTo("1,535");

//    assertThat(record.getPriceFaiReservedVatIncluded())
//      .isEqualTo(null);
//    assertThat(record.getPriceExposedVatIncluded())
//      .isEqualTo("");

//    assertThat(record.getPriceFaiReservedVatIncluded())
//      .isEqualTo("");

    assertThat(record.getCounter())
      .isEqualTo("");
    assertThat(record.getCurrency())
      .isEqualTo("EUR");
    assertThat(record.getDate())
      .isEqualTo("20180702");
    assertThat(record.getGlobalIdentifier())
      .isNotNull();
    assertThat(record.getKilometers())
      .isEqualTo("");
    assertThat(record.getLitres())
      .isEqualTo("26,06");
    assertThat(record.getPanField1IsoCode())
      .isEqualTo("789697");
    assertThat(record.getPanField2GroupCode())
      .isEqualTo("00");
    assertThat(record.getPanField3CustomerCode())
      .isEqualTo("066296");
    assertThat(record.getPanField4DeviceNumber())
      .isEqualTo("0032");
    assertThat(record.getPanField5ControlDigit())
      .isEqualTo("8");
    assertThat(record.getPartnerCode())
      .isEqualTo("IT63");
    assertThat(record.getPointCode())
      .isEqualTo("IT63000013");
    assertThat(record.getProductCode())
      .isEqualTo("GASVO01");
    assertThat(record.getPumpNumber())
      .isEqualTo("");
    assertThat(record.getRecordCode())
      .isEqualTo("3010");
    assertThat(record.getTicket())
      .isEqualTo("777");
    assertThat(record.getTime())
      .isEqualTo("111441");
    assertThat(record.getUnitOfMeasure())
      .isEqualTo("LT");

    assertThat(record.getVat_percentage_float())
      .isEqualByComparingTo(22f);
//    assertThat(record.getVatPercentage())
//      .isEqualTo("22");
    assertThat(record.getVatPercentageFloat())
      .isEqualByComparingTo(22f);
  }

  @Test
  void toTrackyCardCarburantiStandardRecord_q8_use_parameter_vat_if_not_present() {
    given(stanziamentiParams.getRecordCode())
      .willReturn("3010");

    final Article article = mock(Article.class);
    given(stanziamentiParams.getArticle())
      .willReturn(article);
    given(article.getCode())
      .willReturn("GASVO01");

    q8WSTransactionRecord.setIvaTaxRate("");
    given(article.getVatRate())
      .willReturn(2500f);

    given(servicePartner.getPartnerCode())
      .willReturn("IT63");

    TrackyCardCarburantiStandardRecord record = fuelTransactionDTOMapper.toTrackyCardCarburantiStandardRecord(ingestionTime,
                                                                                                              stanziamentiParams,
                                                                                                              servicePartner, fileName,
                                                                                                              q8WSTransactionRecord);
    assertThat(record.getVat_percentage_float())
      .isEqualByComparingTo(25f);
//    assertThat(record.getVatPercentage())
//      .isEqualTo("25");
    assertThat(record.getVatPercentageFloat())
      .isEqualByComparingTo(25f);
  }

  @Test
  void mapTrackyCardPan() {
    final TrackyCardCarburantiStandardRecord tcsRecord = new TrackyCardCarburantiStandardRecord("", -1l);
    fuelTransactionDTOMapper.mapTrackyCardPan(tcsRecord, "7896970008907100047");
    assertThat(tcsRecord.getPanField1IsoCode())
      .isEqualTo("789697");
    assertThat(tcsRecord.getPanField2GroupCode())
      .isEqualTo("00");
    assertThat(tcsRecord.getPanField3CustomerCode())
      .isEqualTo("089071");
    assertThat(tcsRecord.getPanField4DeviceNumber())
      .isEqualTo("0004");
    assertThat(tcsRecord.getPanField5ControlDigit())
      .isEqualTo("7");
  }

  @Test
  void fromDateToTcsDate() {
    String date = fuelTransactionDTOMapper.fromDateToTcsDate(dresserWSTransactionRecord.getTransData());
    assertThat(date)
      .isEqualTo("20180702");
  }

  @Test
  void fromTimeToTcsTime() {
    String time = fuelTransactionDTOMapper.fromTimeToTcsTime(dresserWSTransactionRecord.getTransOra());
    assertThat(time)
      .isEqualTo("000718");
  }

  @Test
  void fromDateTimeToTcsDate() {
    String date = fuelTransactionDTOMapper.fromDateTimeToTcsDate(q8WSTransactionRecord.getData());
    assertThat(date)
      .isEqualTo("20180702");
  }

  @Test
  void fromDateTimeToTcsTime() {
    String time = fuelTransactionDTOMapper.fromDateTimeToTcsTime(q8WSTransactionRecord.getData());
    assertThat(time)
      .isEqualTo("111441");
  }

  @Test
  void fromDateToCreationDateInFile() {
    String date = fuelTransactionDTOMapper.fromDateToCreationDateInFile(dresserWSTransactionRecord.getTransData());
    assertThat(date)
      .isEqualTo("20180702");
  }

  @Test
  void fromTimeToCreationTimeInFile() {
    String time = fuelTransactionDTOMapper.fromTimeToCreationTimeInFile(dresserWSTransactionRecord.getTransOra());
    assertThat(time)
      .isEqualTo("000718");
  }


  @Test
  void fromDateTimeToCreationDateInFile() {
    String date = fuelTransactionDTOMapper.fromDateTimeToCreationDateInFile(q8WSTransactionRecord.getData());
    assertThat(date)
      .isEqualTo("20180702");
  }

  @Test
  void fromDateTimeToCreationTimeInFile() {
    String time = fuelTransactionDTOMapper.fromDateTimeToCreationTimeInFile(q8WSTransactionRecord.getData());
    assertThat(time)
      .isEqualTo("111441");
  }
}
