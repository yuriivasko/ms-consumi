package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.dartford;

import static it.fai.ms.consumi.domain.VehicleLicensePlate.NO_COUNTRY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Map;
import java.util.stream.Collectors;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.AmountBuilder;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.generici.dartford.Dartford;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici.DartfordJob;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.processor.consumi.dartford.DartfordProcessor;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"), })
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class DartfordJobIntTest extends AbstractIntegrationTestSpringContext {

  private static final BigDecimal ZERO = BigDecimal.ZERO.setScale(5);
  private static final String fileName = "20180430_1065024176_CrossingHistory.csv";
  private Path                filePath;


  @MockBean
  private ElasticSearchFeignClient elasticSearchFeignClient;

  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private NotificationService notificationService;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @SpyBean
  private DartfordProcessor processor;

  @Autowired
  private DartfordJob job;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;


  @BeforeEach
  public void init() throws Exception {
    filePath = Paths.get(DartfordJobIntTest.class.getResource("/test-files/dartford/")
                                                 .toURI());
    assertThat(job).isNotNull();

  }

  @Test
  public void elaborateTwoRecords_generate_two_stanziamenti_two_dettagli_stanziamento() {

    ServicePartner serviceProvider = newServicePartner();

    String result = job.process(filePath.resolve(fileName)
                                        .toAbsolutePath()
                                        .toString(),
                                System.currentTimeMillis(), new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull()
                      .isEqualTo("dartford");

    var type = ArgumentCaptor.forClass(String.class);
    // ArgumentCaptor<DartfordRecord> record = ArgumentCaptor.forClass(DartfordRecord.class);
    @SuppressWarnings("unchecked")
    ArgumentCaptor<ElasticSearchRecord<DartfordRecord>> captureRecord = ArgumentCaptor.forClass(ElasticSearchRecord.class);

    verify(elasticSearchFeignClient, times(2)).create(Mockito.anyString(), type.capture(), captureRecord.capture());
    List<ElasticSearchRecord<DartfordRecord>> esRecords = captureRecord.getAllValues();

    assertThat(esRecords).extracting(esR -> esR.getRawdata())
                         .extracting(r -> tuple(r.getCrossingDate(), r.getTime(), r.getDirection(), r.getVehicle(), r.getGroupName()))
                         .containsExactly(tuple("24/04/2018", "14:24", "Southbound", "EY164FX", "130016451 "),
                                          tuple("24/04/2018", "10:55", "Northbound", "EY164FX", "130016451 "));

    ArgumentCaptor<Dartford> acConsumo = ArgumentCaptor.forClass(Dartford.class);

    Amount  zeroAmount = AmountBuilder.builder()
                                      .amountExcludedVat(MonetaryUtils.strToMonetary("0", "EUR"))
                                      .amountIncludedVat(MonetaryUtils.strToMonetary("0", "EUR"))
                                      .vatRate(null)
                                      .build();
    Vehicle vehicle    = new Vehicle();
    vehicle.setLicensePlate(new VehicleLicensePlate("EY164FX", NO_COUNTRY));

    verify(processor, times(2)).validateAndProcess(acConsumo.capture(), any());

    assertThat(acConsumo.getAllValues()).extracting(c -> tuple(c.getAmount(), c.getCustomer(), c.getStartDate(), c.getEndDate(),
                                                               c.getVehicle(), c.getQuantity(), c.getLicencePlateOnRecord()))
                                        .containsExactlyInAnyOrder(tuple(zeroAmount, new Customer("130016451"),
                                                                         toInstant("2018-04-24T14:24:00"), toInstant("2018-04-24T14:24:00"),
                                                                         vehicle, BigDecimal.ONE, "EY164FX"),
                                                                   tuple(zeroAmount, new Customer("130016451"),
                                                                         toInstant("2018-04-24T10:55:00"), toInstant("2018-04-24T10:55:00"),
                                                                         vehicle, BigDecimal.ONE, "EY164FX"));

//      ArgumentCaptor<String> notificationCodeCapture = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<EnrichedNotification> notificationArgumentCaptor = ArgumentCaptor.forClass(EnrichedNotification.class);
    then(notificationService)
      .should(atLeastOnce())
      .notify(notificationArgumentCaptor.capture());

    assertThat(notificationArgumentCaptor.getAllValues().stream().map(enrichedNotification -> enrichedNotification.getCode()).collect(Collectors.toList()))
      .containsExactlyInAnyOrder("CPV-w005", "CPV-w005");
    assertThat(notificationArgumentCaptor.getAllValues())
      .hasSize(2)
      .extracting(notificationArg -> tuple(notificationArg.transformFiledsToParameters().get("job_name"), notificationArg.transformFiledsToParameters().get("original_message")))
      .containsExactlyInAnyOrder(
        tuple(Format.DARTFORD.name(), "Service [PEDAGGI_INGHILTERRA] with pan [null] for device [0000001111111111111/DARTFORD_CROSSING] in date [2018-04-24 14:24:00(offset: 24h)] was not active"),
        tuple(Format.DARTFORD.name(), "Service [PEDAGGI_INGHILTERRA] with pan [null] for device [0000001111111111111/DARTFORD_CROSSING] in date [2018-04-24 10:55:00(offset: 24h)] was not active"));
    verifyNoMoreInteractions(notificationService);

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoGenerico();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(2);

    assertThat(dettaglioStanziamenti).allMatch(s -> s.getStanziamenti()
                                                     .size() == 1);

    assertThat(dettaglioStanziamenti).allMatch(s -> s instanceof DettaglioStanziamentoGenericoEntity);
    assertThat(dettaglioStanziamenti).extracting(s -> (DettaglioStanziamentoGenericoEntity) s)
                                     .extracting(s -> tuple(s.getEndDate(),s.getAmountExcludedVat(),s.getAmountVatRate()))
                                     .containsExactlyInAnyOrder(
                                                                tuple(toInstant("2018-04-24T14:24:00"),ZERO,ZERO),
                                                                tuple(toInstant("2018-04-24T10:55:00"),ZERO,ZERO)
                                                                );

    // verify messages
    @SuppressWarnings("unchecked")
    ArgumentCaptor<List<StanziamentoMessage>> captureStanziamenti = ArgumentCaptor.forClass(List.class);
    verify(stanziamentiToNavJmsPublisher).publish(captureStanziamenti.capture());

    // assertThat(captureStanziamenti.getAllValues())

    Mockito.validateMockitoUsage();
  }

  public ServicePartner newServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.DARTFORD);
    return serviceProvider;
  }

  private Instant toInstant(String text1) {
    return LocalDateTime.parse(text1)
                        .atZone(ZoneId.of("Europe/Rome"))
                        .toInstant();
  }

}
