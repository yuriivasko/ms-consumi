package it.fai.ms.consumi.service.processor.consumi.tunnel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.noMoreInteractions;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import javax.validation.constraints.NotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.FrejusGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.FrejusRecord;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.jms.producer.CambioStatoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.processor.consumi.frejus.FrejusConsumoDettaglioStanziamentoPedaggioMapper;
import it.fai.ms.consumi.service.processor.consumi.frejus.FrejusProcessorImpl;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
class FrejusProcessorImplTest {

  FrejusProcessorImpl                                      subject;
  private ConsumoValidatorService                          validator;
  private StanziamentiProducer                             producer;
  private FrejusConsumoDettaglioStanziamentoPedaggioMapper mapper;
  private VehicleService                                   vehicleService;
  private CustomerService                                  customerService;
  private CambioStatoDaConsumoJmsProducer                  changeStatoPublisher;

  @BeforeEach
  private void setup() {

    validator            = mock(ConsumoValidatorService.class);
    producer             = mock(StanziamentiProducer.class);
    mapper               = mock(FrejusConsumoDettaglioStanziamentoPedaggioMapper.class);
    vehicleService       = mock(VehicleService.class);
    customerService      = mock(CustomerService.class);
    changeStatoPublisher = mock(CambioStatoDaConsumoJmsProducer.class);

    subject = new FrejusProcessorImpl(validator, producer, mapper, vehicleService, customerService, changeStatoPublisher);
  }

  @Test
  void test() {
    Frejus data   = new Frejus(new Source(Instant.now(), Instant.now(), "SITAF"), "", Optional.of(new FrejusGlobalIdentifier("id123")),
                               TestDummyRecord.instance1);
    Bucket bucket = new Bucket("Sitaf");

    when(validator.validate(data)).thenReturn(new ValidationOutcome());

    ProcessingResult<Frejus> result = subject.validateAndProcess(data, bucket);

    assertThat(result.getStanziamenti()).isEmpty();
    assertThat(result.getConsumo()).isNotNull();
    assertThat(result.getStanziamentiParams()).isNotNull();
    ValidationOutcome validationOutcome = result.getValidationOutcome();
    assertThat(validationOutcome).isNotNull();
    assertThat(validationOutcome.isValidationOk()).isEqualTo(true);

    verify(changeStatoPublisher,noMoreInteractions()).sendFatturato(any(), any());
  }

  @Test
  void testBuoni() {
    Frejus data   = init( new Frejus(new Source(Instant.now(), Instant.now(), "SITAF"), "", Optional.of(new FrejusGlobalIdentifier("id123")),
                               TestDummyRecord.instance1),c->{
                                 c.setDevice(new Device(TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_FR));
                               });
    Bucket bucket = new Bucket("Sitaf");

    when(validator.validate(data)).thenReturn(new ValidationOutcome());

    ProcessingResult<Frejus> result = subject.validateAndProcess(data, bucket);

    assertThat(result.getStanziamenti()).isEmpty();
    assertThat(result.getConsumo()).isNotNull();
    assertThat(result.getStanziamentiParams()).isNotNull();
    ValidationOutcome validationOutcome = result.getValidationOutcome();
    assertThat(validationOutcome).isNotNull();
    assertThat(validationOutcome.isValidationOk()).isEqualTo(true);

    verify(changeStatoPublisher,times(1)).sendFatturato(any(), any());
    verify(changeStatoPublisher,noMoreInteractions()).sendFatturato(any(), any());
  }

  @Test
  public void testFromRecord() throws Exception {
    FrejusRecord record = FrejusConsumoDettaglioStanziamentoPedaggioMapperTest.getRecord();
    Frejus       data   = FrejusConsumoDettaglioStanziamentoPedaggioMapperTest.getConsumo(record);
    Bucket       bucket = new Bucket("Sitaf");

    StanziamentiParams params = getParams();

    ValidationOutcome validationOutcome = getValidationOutcome(params);
    when(validator.validate(data)).thenReturn(validationOutcome);

    when(customerService.findCustomerByDeviceSeriale(record.getNumeroTessera(), TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO))
      .thenReturn(Optional.of(new Customer("customer1")));

    when(vehicleService
      .newVehicleByVehicleUUID_WithFareClass(eq(new Device(record.getNumeroTessera(), TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO)),
                                             any(), eq("3"))).thenReturn(Optional.ofNullable(new Vehicle()));

    producer = new StanziamentiProducer() {
               @Override
               public List<Stanziamento> produce(@NotNull DettaglioStanziamento allocationDetail,
                                                 @NotNull StanziamentiParams stanziamentiParams) {
                 Stanziamento stanziamento = new Stanziamento("stanz3iamento1");
                 stanziamento.getDettaglioStanziamenti().add(allocationDetail);
                 return Arrays.asList(stanziamento);
               }
             };
    producer = spy(producer);

    subject = new FrejusProcessorImpl(validator, producer, new FrejusConsumoDettaglioStanziamentoPedaggioMapper(), vehicleService,
                                      customerService, changeStatoPublisher);

    var result = subject.validateAndProcess(data, bucket);

    assertThat(result.getStanziamenti().size()).isEqualTo(1);
    var stanz = result.getStanziamenti().get(0);
    assertThat(stanz.getDettaglioStanziamenti().size()).isEqualTo(1);

    FrejusConsumoDettaglioStanziamentoPedaggioMapperTest.verifyDettaglioStanziamento(stanz.getDettaglioStanziamenti().iterator().next());

    assertThat(result.getConsumo()).isNotNull();
    assertThat(result.getStanziamentiParams()).isNotNull();
    ValidationOutcome resultValidation = result.getValidationOutcome();
    assertThat(resultValidation).isNotNull();
    assertThat(resultValidation.isValidationOk()).isEqualTo(true);

    verify(validator).validate(data);

    verify(vehicleService).newVehicleByVehicleUUID_WithFareClass(Mockito.eq(data.getDevice()), any(), Mockito.eq("4"));

    var acStanz = ArgumentCaptor.forClass(DettaglioStanziamento.class);
    verify(producer).produce(acStanz.capture(), eq(params));
    FrejusConsumoDettaglioStanziamentoPedaggioMapperTest.verifyDettaglioStanziamento(acStanz.getValue());
    verifyNoMoreInteractions(validator, vehicleService);

  }

  private ValidationOutcome getValidationOutcome(StanziamentiParams params) {
    ValidationOutcome validationOutcome = new ValidationOutcome();
    validationOutcome.setStanziamentiParams(params);
    return validationOutcome;
  }

  private StanziamentiParams getParams() {
    StanziamentiParams params  = new StanziamentiParams(new Supplier("frejusSupplier"));
    Article            article = new Article("articleCode");
    // article.setVatRate(20f);
    params.setArticle(article);
    return params;
  }

  public static <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }
}
