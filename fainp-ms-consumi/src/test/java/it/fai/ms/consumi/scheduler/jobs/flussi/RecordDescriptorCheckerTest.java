package it.fai.ms.consumi.scheduler.jobs.flussi;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


@Tag("unit")
public class RecordDescriptorCheckerTest {

  RecordDescriptorChecker recordDescriptorChecker;


  @Test
  void checkConfiguration() {
    RecordDescriptorChecker recordDescriptorChecker = new RecordDescriptorChecker(ElviaDescriptor.class, ElviaRecord.class);
    recordDescriptorChecker.checkConfiguration();
  }
}
