package it.fai.ms.consumi.service.record.carburanti;

import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiJollyRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@Tag("unit")
class TrackyCardCarbJollyRecordConsumoMapperTest {

  TrackyCardCarbJollyRecordConsumoMapper trackyCardCarbJollyRecordConsumoMapper;

  private final TrackyCardCarbStdRecordConsumoMapper trackyCardCarbStdRecordConsumoMapper = mock(TrackyCardCarbStdRecordConsumoMapper.class);
  private final TrackyCardCarburantiJollyRecord      trackyCardCarburantiJollyRecord      = mock(TrackyCardCarburantiJollyRecord.class);
  private final TrackyCardCarburantiStandard         trackyCardCarburantiStandard         = mock(TrackyCardCarburantiStandard.class);

  @BeforeEach
  public void setUp() throws Exception {
    trackyCardCarbJollyRecordConsumoMapper = new TrackyCardCarbJollyRecordConsumoMapper(trackyCardCarbStdRecordConsumoMapper);

    given(trackyCardCarbStdRecordConsumoMapper.mapRecordToConsumo(any()))
      .willReturn(trackyCardCarburantiStandard);

    given(trackyCardCarburantiJollyRecord.getGlobalIdentifier())
      .willReturn("fake_id");

    given(trackyCardCarburantiStandard.getSource())
      .willReturn(mock(Source.class));

    given(trackyCardCarburantiStandard.getDate())
      .willReturn(Instant.now());

    given(trackyCardCarburantiJollyRecord.getIngestion_time())
      .willReturn(Instant.now());
  }


  @Test
  void mapRecordToConsumo_ok() throws Exception {

    given(trackyCardCarburantiJollyRecord.getCustomerCode())
      .willReturn("pippo");

    given(trackyCardCarburantiJollyRecord.getEuroClass())
      .willReturn("6");
    given(trackyCardCarburantiJollyRecord.getLicencePlate())
      .willReturn("AA000AA");
    given(trackyCardCarburantiJollyRecord.getCountry())
      .willReturn("IT");

    TrackyCardCarburantiJolly recordConsumoJolly = trackyCardCarbJollyRecordConsumoMapper.mapRecordToConsumo(trackyCardCarburantiJollyRecord);

    assertThat(recordConsumoJolly.getVehicle().getEuroClass())
      .isEqualTo("6");
    assertThat(recordConsumoJolly.getVehicle().getLicensePlate().getLicenseId())
      .isEqualTo("AA000AA");
    assertThat(recordConsumoJolly.getVehicle().getLicensePlate().getCountryId())
      .isEqualTo("IT");
    assertThat(recordConsumoJolly.getCustomer().getId())
      .isEqualTo("pippo");
  }

  @Test
  void mapRecordToConsumo_partial_licence_plate_info() throws Exception {

    given(trackyCardCarburantiJollyRecord.getEuroClass())
      .willReturn("");
    given(trackyCardCarburantiJollyRecord.getLicencePlate())
      .willReturn("AA000AA");
    given(trackyCardCarburantiJollyRecord.getCountry())
      .willReturn("");


    TrackyCardCarburantiJolly recordConsumoJolly = trackyCardCarbJollyRecordConsumoMapper.mapRecordToConsumo(trackyCardCarburantiJollyRecord);

    assertThat(recordConsumoJolly.getVehicle().getEuroClass())
      .isEqualTo("");
    assertThat(recordConsumoJolly.getVehicle().getLicensePlate().getLicenseId())
      .isEqualTo("AA000AA");
    assertThat(recordConsumoJolly.getVehicle().getLicensePlate().getCountryId())
      .isEqualTo("");
  }

}
