package it.fai.ms.consumi.service.report;

import java.time.Instant;

import it.fai.ms.common.utils.report.CsvHeaderTranslate;

class TestClass {
  Instant a;
}

class TestClassWithHeader {
  @CsvHeaderTranslate(messageKey = "report.header.consumi.codiceDispositivo")
  Instant a;
}

class TestClassWithFormatter {
  @CsvHeaderTranslate(messageKey = "a", formatDate = "yyyy-MM-dd")
  Instant a;
}

class TestClassSplitted {
  @CsvHeaderTranslate(messageKey = "date",
      formatDate = "yyyy-MM-dd",
      splitDateTime = true,
      timeMessageKey = "time"

  )
  Instant a;
}
