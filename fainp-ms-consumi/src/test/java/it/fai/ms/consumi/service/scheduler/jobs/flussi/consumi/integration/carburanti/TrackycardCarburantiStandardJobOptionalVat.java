package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti;

import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

@Tag("integration")
public class TrackycardCarburantiStandardJobOptionalVat
  extends AbstractTrackycardCarburantiStandardJobWithEsIntTest {

  @Test
  @Transactional
  public void test_iva_opzionale_se_non_trovata_genera_stanziamenti() throws Exception {


    String filename = getAbsolutePath("/test-files/trackycardcarbstd/AT02GFP20140613215101.TXT.novat");
    ServicePartner servicePartner = findServicePartner("AT02");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .extracting(stanziamento -> tuple(stanziamento.getInvoiceType(), stanziamento.getGeneraStanziamento()))
      .hasSize(2);
  }

  @Test
  @Transactional
  public void test_iva_obbligatoria_se_non_trovata_NON_genera_stanziamenti() throws Exception {


    String filename = getAbsolutePath("/test-files/trackycardcarbstd/AT04GFP20140613215101.TXT.novat");
    ServicePartner servicePartner = findServicePartner("AT04");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .extracting(stanziamento -> tuple(stanziamento.getInvoiceType(), stanziamento.getGeneraStanziamento()))
      .hasSize(0);
  }



}
