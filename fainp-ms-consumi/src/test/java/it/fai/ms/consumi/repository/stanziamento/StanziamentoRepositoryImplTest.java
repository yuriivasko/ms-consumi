package it.fai.ms.consumi.repository.stanziamento;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Set;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.StanziamentoTestBuilder;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;

@ExtendWith(SpringExtension.class)
// @ComponentScan(basePackages = { "it.fai.ms.consumi.repository.stanziamento" })
@DataJpaTest
@EntityScan(basePackages = { "it.fai.ms.consumi", "it.fai.common.notification.domain" })
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.common.notification.repository" })
@Tag("integration")
class StanziamentoRepositoryImplTest {

  @Autowired
  private EntityManager em;

  private StanziamentoRepository repository;

  private StanziamentoEntityMapper stanziamentoMapper;

  @BeforeEach
  void setUp() throws Exception {
    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
                                                                                  new DettaglioStanziamentoGenericoEntityMapper(),
                                                                                  new DettaglioStanziamentoPedaggioEntityMapper(),
                                                                                  new DettaglioStanziamentoTrenoEntityMapper());
    stanziamentoMapper = new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper);
    repository = new StanziamentoRepositoryImpl(em, stanziamentoMapper, dettaglioStanziamentoEntityMapper, new StanziamentoCodeGenerator());
  }

  private static Stanziamento newStanziamento() {
    return StanziamentoTestBuilder.newInstance()
                                  .withInvoiceTypeP()
                                  .build();
  }

  private static Stanziamento newStanziamento(String codiceStanziamento) {
    Stanziamento stanziamento = new Stanziamento(codiceStanziamento);
    stanziamento.setInvoiceType(InvoiceType.P);
    return stanziamento;
  }

  @Test
  void testCloneAndModify() {

    // given
    var newStanziamento = newStanziamento();
    em.persist(stanziamentoMapper.toEntity(newStanziamento));

    // when
    var clonedStanziamento = repository.cloneAndModify(stanziamentoMapper.toEntity(newStanziamento), null);

    // then
    assertThat(clonedStanziamento.getCodiceStanziamentoProvvisorio()).isEqualTo(newStanziamento.getCode());
  }

  @Test
  void testFindByCodiceStanziamentoIn() {

    // given
    var s1 = newStanziamento("s1");
    em.persist(stanziamentoMapper.toEntity(s1));
    var s2 = newStanziamento("s2");
    em.persist(stanziamentoMapper.toEntity(s2));

    // when
    Set<String> codes = Arrays.asList("s1", "s2", "s3")
                              .stream()
                              .collect(toSet());
    Set<String> foundedCodes = repository.findByCodiceStanziamentoIn(codes);

    // then
    assertThat(foundedCodes).isNotNull();
    assertThat(foundedCodes).hasSize(2);
  }

}
