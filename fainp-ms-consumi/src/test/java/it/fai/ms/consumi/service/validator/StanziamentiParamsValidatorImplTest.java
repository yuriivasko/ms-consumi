package it.fai.ms.consumi.service.validator;

import static it.fai.ms.consumi.testutils.TestUtils.init;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsQuery;
import it.fai.ms.consumi.repository.consumo.model.telepass.DummyConsumo;
import it.fai.ms.consumi.service.navision.NavArticlesService;
import it.fai.ms.consumi.service.navision.NavSupplierCodesService;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;

@Tag("unit")
class StanziamentiParamsValidatorImplTest {

  private final NavArticlesService        mockNavArticlesService        = mock(NavArticlesService.class);
  private final NavSupplierCodesService   mockNavSupplierCodesService   = mock(NavSupplierCodesService.class);
  private final StanziamentiParamsService mockStanziamentiParamsService = mock(StanziamentiParamsService.class);
  private StanziamentiParamsValidator     validator;

  @Test
  void test_checkConsistency_when_stanziamentiparams_articles_and_supplierCodes_are_different() throws StanziamentiParamsNotFoundException {

    // given

    given(mockStanziamentiParamsService.getAll()).willReturn(AllStanziamentiParamsTestBuilder.newInstance()
                                                                                             .build());

    given(mockNavArticlesService.getAllArticlesAndUpdateStanziamentiParams(any())).willReturn(List.of(ArticleTestBuilder.newInstance()
                                                                                                                                     .withCode("::differentArticleCode-1::")
                                                                                                                                     .build(),
                                                                                                                   ArticleTestBuilder.newInstance()
                                                                                        .withCode("::differentArticleCode-2::")
                                                                                        .build()));

    given(mockNavSupplierCodesService.getAllCodes()).willReturn(Set.of("::supplierCode-1::", "::supplierCode-2::"));

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,null,mock(ApplicationProperties.class),
                                                    true);

    // when

    final var validationOutcome = validator.checkConsistency();

    // then

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();

    assertThat(validationOutcome.getMessages()).anyMatch(msg -> msg.getCode()
                                                                   .equals("001"));
    assertThat(validationOutcome.getMessages()).anyMatch(msg -> msg.getCode()
                                                                   .equals("002"));
  }

  @Test
  void test_checkConsistency_when_stanziamentiparams_articles_and_supplierCodes_are_equals() throws StanziamentiParamsNotFoundException {

    // given

    given(mockStanziamentiParamsService.getAll()).willReturn(AllStanziamentiParamsTestBuilder.newInstance()
                                                                                             .build());

    given(mockNavArticlesService.getAllArticlesAndUpdateStanziamentiParams(any())).willReturn(List.of(ArticleTestBuilder.newInstance()
                                                                                                                                     .withCode("::articleCode-1::")
                                                                                                                                     .build(),
                                                                                                                   ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-2::")
                                                                                        .build(),
                                                                                                                   ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-3::")
                                                                                        .build()));

    given(mockNavSupplierCodesService.getAllCodes()).willReturn(Set.of("::supplierCode-1::", "::supplierCode-2::", "::supplierCode-3::"));

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);

    // when

    final var validationOutcome = validator.checkConsistency();

    // then

    assertThat(validationOutcome.isValidationOk()).isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();

  }

  @Test
  void test_checkConsistency_when_stanziamentiparams_articles_and_supplierCodes_are_subset() throws StanziamentiParamsNotFoundException {

    // given

    given(mockStanziamentiParamsService.getAll()).willReturn(AllStanziamentiParamsTestBuilder.newInstance()
                                                                                             .build());

    given(mockNavArticlesService.getAllArticlesAndUpdateStanziamentiParams(any())).willReturn(List.of(ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-1::")
                                                                                        .build(), ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-2::")
                                                                                        .build(), ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-3::")
                                                                                        .build(), ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-4::")
                                                                                        .build()));

    given(mockNavSupplierCodesService.getAllCodes()).willReturn(Set.of("::supplierCode-1::", "::supplierCode-2::", "::supplierCode-3::",
                                                                       "::supplierCode-4::"));

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);

    // when

    final var validationOutcome = validator.checkConsistency();

    // then

    assertThat(validationOutcome.isValidationOk()).isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();

  }

  @Test
  void test_checkConsistency_when_stanziamentiparams_articles_are_different() throws StanziamentiParamsNotFoundException {

    // given

    given(mockStanziamentiParamsService.getAll()).willReturn(AllStanziamentiParamsTestBuilder.newInstance()
                                                                                             .build());

    final var navArticles = List.of(ArticleTestBuilder.newInstance()
                                                      .withCode("::differentArticleCode-1::")
                                                      .build(),
                                    ArticleTestBuilder.newInstance()
                                                      .withCode("::differentArticleCode-2::")
                                                      .build());
    given(mockNavArticlesService.getAllArticlesAndUpdateStanziamentiParams(any())).willReturn(navArticles);

    given(mockNavSupplierCodesService.getAllCodes()).willReturn(Set.of("::supplierCode-1::", "::supplierCode-2::", "::supplierCode-3::"));

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);

    // when

    final var validationOutcome = validator.checkConsistency();

    // then

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();

    assertThat(validationOutcome.getMessages()).allMatch(msg -> msg.getCode()
                                                                   .equals("002"));
  }

  @Test
  void test_checkConsistency_when_stanziamentiparams_articlesAttributes_are_different() throws StanziamentiParamsNotFoundException {

    // given

    given(mockStanziamentiParamsService.getAll()).willReturn(AllStanziamentiParamsTestBuilder.newInstance()
                                                                                             .build());

    final var navArticles = List.of(ArticleTestBuilder.newInstance()
                                                      .withCode("::articleCode-1::")
                                                      .withVatRate(99.9f)
                                                      .build(),
                                    ArticleTestBuilder.newInstance()
                                                      .withCode("::articleCode-2::")
                                                      .build(),
                                    ArticleTestBuilder.newInstance()
                                                      .withCode("::articleCode-3::")
                                                      .build());
    given(mockNavArticlesService.getAllArticlesAndUpdateStanziamentiParams(any())).willReturn(navArticles);

    given(mockNavSupplierCodesService.getAllCodes()).willReturn(Set.of("::supplierCode-1::", "::supplierCode-2::", "::supplierCode-3::"));

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);

    // when

    final var validationOutcome = validator.checkConsistency();

    // then

    assertThat(validationOutcome.isValidationOk()).isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();

  }

  @Test
  void test_checkConsistency_when_stanziamentiparams_not_found_on_repo_throws_exception() {

    given(mockStanziamentiParamsService.getAll()).willReturn(new AllStanziamentiParams());

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);

    // when + then
    assertThatExceptionOfType(StanziamentiParamsNotFoundException.class).isThrownBy(() -> validator.checkConsistency());
  }

  @Test
  void test_checkConsistency_when_stanziamentiparams_providers_are_different() throws StanziamentiParamsNotFoundException {

    // given

    given(mockStanziamentiParamsService.getAll()).willReturn(AllStanziamentiParamsTestBuilder.newInstance()
                                                                                             .build());

    given(mockNavArticlesService.getAllArticlesAndUpdateStanziamentiParams(any())).willReturn(List.of(ArticleTestBuilder.newInstance()
                                                                                                                                     .withCode("::articleCode-1::")
                                                                                                                                     .build(),
                                                                                                                   ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-2::")
                                                                                        .build(),
                                                                                                                   ArticleTestBuilder.newInstance()
                                                                                        .withCode("::articleCode-3::")
                                                                                        .build()));

    given(mockNavSupplierCodesService.getAllCodes()).willReturn(Set.of("::differentSupplierCode-1::", "::differentSupplierCode-2::",
                                                                       "::supplierCode-3::"));

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);

    // when

    final var validationOutcome = validator.checkConsistency();

    // then

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();

    assertThat(validationOutcome.getMessages()).allMatch(msg -> msg.getCode()
                                                                   .equals("001"));
  }

  @Test
  void test_existsParamStanziamento_when_Consumo_hasSourceAndRecordCode() {

    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "::recordCode::", Optional.empty(), TestDummyRecord.instance1);
    consumo.setTransaction(new Transaction());
    consumo.getTransaction()
           .setDetailCode("::detailCode::");

    // when

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);
    final var validationOutcome = validator.existsParamsStanziamento(consumo);

    // then

    final ArgumentCaptor<StanziamentiParamsQuery> captor = ArgumentCaptor.forClass(StanziamentiParamsQuery.class);

    then(mockStanziamentiParamsService).should()
                                       .findByQuery(captor.capture());
    then(mockStanziamentiParamsService).shouldHaveNoMoreInteractions();

    then(mockNavArticlesService).shouldHaveZeroInteractions();

    then(mockNavSupplierCodesService).shouldHaveZeroInteractions();

    assertThat(captor.getValue()
                     .getSource()).isEqualTo("::source::");
    assertThat(captor.getValue()
                     .getRecordCode()).isEqualTo("::recordCode::");
    assertThat(validationOutcome.isValidationOk()).isFalse();
  }

  @Test
  void test_existsParamStanziamento_when_Consumo_isNull() {

    // given

    final Consumo nullConsumo = null;

    // when

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService, mockNavSupplierCodesService,mock(ApplicationProperties.class),
                                                    true);
    final var validationOutcome = validator.existsParamsStanziamento(nullConsumo);

    // then

    then(mockStanziamentiParamsService).shouldHaveZeroInteractions();

    then(mockNavArticlesService).shouldHaveZeroInteractions();

    then(mockNavSupplierCodesService).shouldHaveZeroInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
  }

  @Test
  void test_existsParamStanziamenti_false_when_notFound_and_vatRate_is_null(){
    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "::recordCode::", Optional.empty(), TestDummyRecord.instance1);
    consumo.setTransaction(new Transaction());
    consumo.getTransaction()
      .setDetailCode("::detailCode::");
    consumo.setAmount(
      AmountBuilder.builder()
      .amountIncludedVat(MonetaryUtils.strToMonetary("12", "EUR"))
      .amountExcludedVat(MonetaryUtils.strToMonetary("10","EUR"))
      .vatRate(new BigDecimal("22"))
      .build()

    );
    // when

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService,
      mockNavArticlesService,
      mockNavSupplierCodesService,
      mock(ApplicationProperties.class),
      true);

    final var validationOutcome = validator.existsParamsStanziamento(consumo);

    // then

    final ArgumentCaptor<StanziamentiParamsQuery> captor = ArgumentCaptor.forClass(StanziamentiParamsQuery.class);

    then(mockStanziamentiParamsService).should()
      .findByQuery(captor.capture());
    then(mockStanziamentiParamsService).shouldHaveNoMoreInteractions();

    then(mockNavArticlesService).shouldHaveZeroInteractions();

    then(mockNavSupplierCodesService).shouldHaveZeroInteractions();

    assertThat(captor.getValue()
      .getSource()).isEqualTo("::source::");
    assertThat(captor.getValue()
      .getRecordCode()).isEqualTo("::recordCode::");
    assertThat(validationOutcome.isValidationOk()).isFalse();
  }

  @Test
  void test_existsParamStanziamenti_set_mandatory_false_when_vatRate_is_optional(){
    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "::recordCode::", Optional.empty(), TestDummyRecord.instance1);
    consumo.setTransaction(new Transaction());
    consumo.getTransaction()
      .setDetailCode("::detailCode::");

    consumo.setServicePartner(
      init(new ServicePartner("::id::"), (s) ->
        s.setPartnerCode("::code1::")
      )
    );

    consumo.setAmount(
      AmountBuilder.builder()
        .amountIncludedVat(MonetaryUtils.strToMonetary("12", "EUR"))
        .amountExcludedVat(MonetaryUtils.strToMonetary("10","EUR"))
        .vatRate(new BigDecimal("22"))
        .build()

    );
    ApplicationProperties applicationProperties = new ApplicationProperties();
    applicationProperties.setOptionalVatConfiguration(Map.of(
      "::source::".toUpperCase(), List.of("::code1::", "::code2::")
    ));


    // when

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService,
      mockNavSupplierCodesService, applicationProperties,true);

    validator.existsParamsStanziamento(consumo);

    // then

    final ArgumentCaptor<StanziamentiParamsQuery> captor = ArgumentCaptor.forClass(StanziamentiParamsQuery.class);

    then(mockStanziamentiParamsService)
      .should()
      .findByQuery(captor.capture());

    assertThat(captor.getValue()
      .getSource())
      .isEqualTo("::source::");

    assertThat(captor.getValue()
      .isVatMandatory())
      .isEqualTo(false);

  }

  @Test
  void test_existsParamStanziamenti_set_mandatory_true_when_vatRate_is_mandatory(){
    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "::recordCode::", Optional.empty(), TestDummyRecord.instance1);
    consumo.setTransaction(new Transaction());
    consumo.getTransaction()
      .setDetailCode("::detailCode::");

    consumo.setServicePartner(
      init(new ServicePartner("::id::"), (s) ->
        s.setPartnerCode("::code5::")
      )
    );

    consumo.setAmount(
      AmountBuilder.builder()
        .amountIncludedVat(MonetaryUtils.strToMonetary("12", "EUR"))
        .amountExcludedVat(MonetaryUtils.strToMonetary("10","EUR"))
        .vatRate(new BigDecimal("22"))
        .build()

    );
    ApplicationProperties applicationProperties = new ApplicationProperties();
    applicationProperties.setOptionalVatConfiguration(Map.of(
      "::source::".toUpperCase(), List.of("::code1::", "::code2::")
    ));


    // when

    validator = new StanziamentiParamsValidatorImpl(mockStanziamentiParamsService, mockNavArticlesService,
      mockNavSupplierCodesService, applicationProperties,true);

    validator.existsParamsStanziamento(consumo);

    // then

    final ArgumentCaptor<StanziamentiParamsQuery> captor = ArgumentCaptor.forClass(StanziamentiParamsQuery.class);

    then(mockStanziamentiParamsService)
      .should()
      .findByQuery(captor.capture());

    assertThat(captor.getValue()
      .getSource())
      .isEqualTo("::source::");

    assertThat(captor.getValue()
      .isVatMandatory())
      .isEqualTo(true);

  }

}
