package it.fai.ms.consumi.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.AbstractCommonTest;
import it.fai.ms.consumi.repository.TollChargerEntityRepository;
import it.fai.ms.consumi.repository.tollcharger.model.TollChargerEntity;
import it.fai.ms.consumi.service.TollChargerService;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@EnableJpaRepositories({ "it.fai.ms.consumi.repository" })
@Tag("unit")
@Transactional
public class TollChargerServiceTest extends AbstractCommonTest {

  @Autowired TollChargerEntityRepository tollChargerEntityRepository;

  TollChargerService tollChargerService;
  
  @Test
  public void findById() throws Exception {
    long size_before = tollChargerEntityRepository.count();
    
    String code = RandomString();
    String description = RandomString();
    
    TollChargerEntity entity = new TollChargerEntity().code(code).description(description);
    entity = tollChargerEntityRepository.save(entity);
    
    assertThat(size_before + 1).isEqualTo(tollChargerEntityRepository.count());
    
    entity = tollChargerService.findById(entity.getId()).get();
    assertThat(entity.getCode()).isEqualTo(code);
    assertThat(entity.getDescription()).isEqualTo(description);
    
    tollChargerEntityRepository.deleteById(entity.getId());
    assertThat(size_before).isEqualTo(tollChargerEntityRepository.count());
  }
  
  @Test
  public void findOneByCode() throws Exception {
    long size_before = tollChargerEntityRepository.count();
    
    String code = RandomString();
    String description = RandomString();
    
    TollChargerEntity entity = new TollChargerEntity().code(code).description(description);
    entity = tollChargerEntityRepository.save(entity);
    
    assertThat(size_before + 1).isEqualTo(tollChargerEntityRepository.count());
    
    entity = tollChargerService.findOneByCode(code).get();
    assertThat(entity.getCode()).isEqualTo(code);
    assertThat(entity.getDescription()).isEqualTo(description);
    
    tollChargerEntityRepository.deleteById(entity.getId());
    assertThat(size_before).isEqualTo(tollChargerEntityRepository.count());
  }

  @Test
  public void lastInsertId() throws Exception {
    long size_before = tollChargerEntityRepository.count();
    
    String code = RandomString();
    String description = RandomString();
    
    TollChargerEntity entity = new TollChargerEntity().code(code).description(description);
    entity = tollChargerEntityRepository.save(entity);
    
    assertThat(size_before + 1).isEqualTo(tollChargerEntityRepository.count());
    
    entity = tollChargerService.lastInsertId();
    assertThat(entity.getCode()).isEqualTo(code);
    assertThat(entity.getDescription()).isEqualTo(description);
    
    tollChargerEntityRepository.deleteById(entity.getId());
    assertThat(size_before).isEqualTo(tollChargerEntityRepository.count());
  }

  @BeforeEach
  public void setUp() throws Exception {
    tollChargerService = new TollChargerServiceImpl(tollChargerEntityRepository);
  }
  
}
