package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti;

import static java.time.Month.APRIL;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;

@Tag("integration")
public class TrackycardCarburantiJollyJobDaConsumoWithESIntTest
  extends AbstractTrackycardCarburantiJollyJobWithEsIntTest {

  final static String FILENAME = "/test-files/trackycardcarbjolly/JOLLY20170418092137.JOLLY.20170418103004";
  final static String PARTNERCODE = "JOLLY";


  public TrackycardCarburantiJollyJobDaConsumoWithESIntTest() throws URISyntaxException {
    super(FILENAME,PARTNERCODE );
  }

  @Test
  @Transactional
  @Disabled
  public void test() {

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList.size())
      .isEqualTo(2);

    for (Stanziamento stanziamento : stanziamentoList) {
      assertThat(stanziamento.getCosto()).isNull();
      assertThat(stanziamento.getPrezzo()).isGreaterThan(BigDecimal.ZERO);
      assertThat(stanziamento.getAmountCurrencyCode()).isEqualTo("EUR");
      assertThat(stanziamento.getVehicleLicensePlate()).startsWith("AA");
      Set<DettaglioStanziamento> dettagliStanziamento = stanziamento.getDettaglioStanziamenti();
      for (DettaglioStanziamento dettaglio : dettagliStanziamento) {
        DettaglioStanziamentoCarburante dettaglioCarburante = (DettaglioStanziamentoCarburante) dettaglio;
        assertThat(dettaglioCarburante.getCustomer().getId())
           //customer id fittizio inserito volutamente dei csv di test.
          .isEqualTo("FR03");
        assertThat(dettaglioCarburante.getVehicle().getLicensePlate().getCountryId())
          .isEqualTo("IT");
        assertThat(dettaglioCarburante.getVehicle().getLicensePlate().getLicenseId())
          .startsWith("AA");
        assertThat(dettaglioCarburante.getVehicle().getEuroClass())
          .startsWith("6");

      }
      assertThat(stanziamento.getVehicleLicensePlate())
      .startsWith("AA");
      //customer id fittizio inserito volutamente dei csv di test.
      assertThat(stanziamento.getNumeroCliente())
        .startsWith("FR03");
    }

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    assertThat(dettaglioStanziamenti)
        .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoCarburanteEntity dettaglioStanziamentoCarburante = (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoCarburante.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoCarburante.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
          .isNotNull();
      assertThat(dataAcquisizioneFlusso)
          .isNotNull();
      assertThat(ingestionTime)
          .isNotEqualTo(dataAcquisizioneFlusso);

      //from file hader 20170418
      LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(2017);
      assertThat(date.getMonth()).isEqualTo(APRIL);
      assertThat(date.getDayOfMonth()).isEqualTo(18);

      //can be broken using tests on midnight..
      date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());

    }
    assertThat(dettaglioStanziamenti.size())
        .isEqualTo(2);
  }
}
