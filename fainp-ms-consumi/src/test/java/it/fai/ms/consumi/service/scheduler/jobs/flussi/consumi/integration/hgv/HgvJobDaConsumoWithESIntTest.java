package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.hgv;

import static it.fai.ms.consumi.domain.InvoiceType.D;
import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.RICAVO;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;

@Tag("integration")
public class HgvJobDaConsumoWithESIntTest
  extends AbstractHgvJobWithEsIntTest {

  final static String FILENAME = "/test-files/hgv/levy_periods_2_rows.csv";
  final static String PARTNERCODE = "UK01";


  public HgvJobDaConsumoWithESIntTest() throws URISyntaxException {
    super(FILENAME, PARTNERCODE);
  }

  @Test
  @Transactional
  public void process_sample_file_producing_stanziamenti() {

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList.size())
      .isEqualTo(1);

    for (Stanziamento stanziamento : stanziamentoList) {
      assertThat(stanziamento.getCode()).isNotNull();

      //se dettaglio ha recuperato il veicolo
//      assertThat(stanziamento.getVehicleEuroClass()).isNotNull();
//      assertThat(stanziamento.getVehicleLicensePlate()).isNotNull();

      assertThat(stanziamento.getCodiceClienteFatturazione()).isNull();

      //vale solo se esiste uno stanziamento provvisorio - no pedaggi HGV
      assertThat(stanziamento.getCodiceStanziamentoProvvisorio()).isNull();
      assertThat(stanziamento.isConguaglio()).isFalse();

      assertThat(stanziamento.getCosto())
        .isNotNull();
      assertThat(stanziamento.getCosto())
        .isNotZero();

      assertThat(stanziamento.getDataFattura()).isNull();

      assertThat(stanziamento.getDocumentoDaFornitore()).isNull();

      assertThat(stanziamento.getGeneraStanziamento()).isEqualTo(RICAVO);

      assertThat(stanziamento.getInvoiceType()).isEqualTo(D);
      assertThat(stanziamento.getDettaglioStanziamenti()).isNotEmpty();
      assertThat(stanziamento.getTotalAllocationDetails()).isEqualTo(1);

      assertThat(stanziamento.getArticleCode()).isNotNull();
      assertThat(stanziamento.getNumeroCliente()).isNotNull();

      assertThat(stanziamento.getNumeroFattura()).isNull();
      assertThat(stanziamento.getNumeroFornitore()).isNotNull();

      assertThat(stanziamento.getPrezzo())
        .isNotNull();
      assertThat(stanziamento.getPrezzo())
        .isNotZero();

      assertThat(stanziamento.getPrezzo())
        .isEqualByComparingTo(stanziamento.getCosto());

      assertThat(stanziamento.getQuantita()).isEqualByComparingTo(BigDecimal.ONE);
      assertThat(stanziamento.getTipoFlusso()).isEqualTo(TypeFlow.MYFAI);

      assertThat(stanziamento.getYear()).isEqualTo(2018);

      Set<DettaglioStanziamento> dettagliStanziamento = stanziamento.getDettaglioStanziamenti();
      for (DettaglioStanziamento dettaglio : dettagliStanziamento) {
        DettaglioStanziamentoGenerico dettaglioStanziamentoGenerico = (DettaglioStanziamentoGenerico) dettaglio;
        assertThat(dettaglioStanziamentoGenerico.getAmount().getCurrency().getCurrencyCode())
          .isEqualTo("EUR");
        assertThat(dettaglioStanziamentoGenerico.getSource().getFileName())
          .isEqualTo("levy_periods_2_rows.csv");
        assertThat(dettaglioStanziamentoGenerico.getRecordCode())
          .isEqualTo("DEFAULT");
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getSign())
          .isEqualTo("+");

        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountExcludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isNotNull();
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountIncludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isNotZero();
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountExcludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(dettaglioStanziamentoGenerico.getAmount().getAmountIncludedVat().getNumber().numberValueExact(BigDecimal.class));

        assertThat(dettaglioStanziamentoGenerico.getAmount().getVatRateBigDecimal())
          .isEqualByComparingTo(new BigDecimal("0"));
        assertThat(dettaglioStanziamentoGenerico.getVehicle())
          .isNotNull();
        assertThat(dettaglioStanziamentoGenerico.getVehicle().getLicensePlate().getLicenseId())
          .isEqualTo("EM532ED");
        assertThat(dettaglioStanziamentoGenerico.getVehicle().getLicensePlate().getCountryId())
          .isEqualTo("IT");
      }
    }

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoGenerico();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoGenericoEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoGenericoEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
          .isNotNull();
      assertThat(dataAcquisizioneFlusso)
          .isNotNull();
      assertThat(ingestionTime)
          .isEqualTo(dataAcquisizioneFlusso);

      //can be broken using tests on midnight..
      LocalDate date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());
    }

    assertThat(dettaglioStanziamenti.size())
      .isEqualTo(1);
  }
}
