package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.telepass.dbunit;

import java.sql.SQLException;
import java.util.Date;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.core.connection.ConnectionHolderImpl;
import com.github.database.rider.core.replacers.NullReplacer;
import com.github.database.rider.junit5.DBUnitExtension;

import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractTelepassIntTest;

@Tag("integration")
@ExtendWith(DBUnitExtension.class)
@Disabled
public class ElviaJobWithESIntTest
  extends AbstractTelepassIntTest {

  private transient final Logger log = LoggerFactory.getLogger(getClass());

  private String PARTNERCODE = "CH01";

  private ElviaJob                     job;

  @Inject
  private ElviaProcessor defaultProcessor;

  @Inject
  private ElviaRecordConsumoMapper defaultRecordConsumoMapper;

  @Inject
  private ElviaGenericoProcessor consumoGenericoProcessor;

  @Inject
  private ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper;

  @Inject
  private StanziamentoRepository stanziamentiRepository;

  @Inject
  private RecordPersistenceService recordPersistenceService;

  @Inject
  protected ServiceProviderRepository serviceProviderRepository;

  @Inject
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Inject
  private PlatformTransactionManager transactionManager;
  
  @Inject
  private NotConfirmedTemporaryAllocationsService notConfirmedAllocationService;

  
  @Inject
  EntityManager em;
 

  public ConnectionHolder getConnectionHolder() throws SQLException {
    Session session = em.unwrap(Session.class);
    SessionImplementor sessionImplementor = (SessionImplementor) session;
   return new ConnectionHolderImpl(sessionImplementor.getJdbcConnectionAccess().obtainConnection());
  }

  @BeforeEach
  public void setUp() throws Exception {

    ElviaDescriptor descriptor = new ElviaDescriptor();

    super.setUpMockContext("Elvia");

    job = new ElviaJob(transactionManager,
                       stanziamentiParamsValidator,
                       notificationService,
                       recordPersistenceService,
                       descriptor,
                       defaultProcessor,
                       defaultRecordConsumoMapper,
                       consumoGenericoProcessor,
                       recordConsumoGenericoMapper,
                       stanziamentiToNavJmsPublisher,
                       notConfirmedAllocationService
    );
    
  }

  protected ServicePartner findServicePartner(String providerCode) {
    ServiceProvider sp = serviceProviderRepository.findByProviderCode(providerCode);
    return sp.toServicePartner();
  }

//  @Test
//  @Transactional
//  @DataSet(transactional = true)
//  @DBUnit(replacers=NullReplacer.class)
//  @ExpectedDataSet(
//                   value = "/test-files/elvia/int-test-elvia-only/stanziamento_DA06A284.ELVIA.FAI.N00002_NOAREAC.xml",
//                   ignoreCols = { "CODICE_STANZIAMENTO", "CODICESTANZIAMENTO_PROVVISORIO", "DATA_INVIO_NAV", "DATA_ACCODATO_NAV","CLASSE_VEICOLO_EURO"})
//  public void test_dettagli_and_stanzimenti_PEDAGGI_count() {
//    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/int-test-elvia-only/DA06A284.ELVIA.FAI.N00002_NOAREAC");
//
//    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), null);
//
//    
//    Class driverClass = Class.forName("org.hsqldb.jdbcDriver");
//    Connection jdbcConnection = DriverManager.getConnection(
//            "jdbc:hsqldb:sample", "sa", "");
//    IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
//
//    // partial database export
//    QueryDataSet partialDataSet = new QueryDataSet(connection);
//    partialDataSet.addTable("FOO", "SELECT * FROM TABLE WHERE COL='VALUE'");
//    partialDataSet.addTable("BAR");
//    FlatXmlDataSet.write(partialDataSet, new FileOutputStream("partial.xml"));
//  
//  }

}


