package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import java.time.Instant;
import java.util.Optional;

import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;

public class ElcitTestBuilder {

  private ElcitTestBuilder() {
  }

  public static ElcitTestBuilder newInstance() {
    return new ElcitTestBuilder();
  }

  public Elcit build() {
    var elcit = new Elcit(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);
    return elcit;
  }

}
