package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

public class StanziamentiParamsSupplierEntityTestBuilder {

  public static StanziamentiParamsSupplierEntityTestBuilder newInstance() {
    return new StanziamentiParamsSupplierEntityTestBuilder();
  }

  private StanziamentiParamsSupplierEntity supplier;

  private StanziamentiParamsSupplierEntityTestBuilder() {
  }

  public StanziamentiParamsSupplierEntity build() {
    return supplier;
  }

  public StanziamentiParamsSupplierEntityTestBuilder withCode(final String _code) {
    supplier = new StanziamentiParamsSupplierEntity(_code);
    supplier.setLegacyCode("::supplierCodeLegacy::");
    return this;
  }

}
