package it.fai.ms.consumi.service.record.telepass;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;

import static it.fai.ms.consumi.service.record.telepass.TelepassDeviceType.APPARATO_TELEPASS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@Tag("unit")
class ElcitRecordConsumoMapperTest {

  ElcitRecordConsumoMapper elcitRecordConsumoMapper = new ElcitRecordConsumoMapper();

  ElcitRecord commonRecord = mock(ElcitRecord.class);

  @BeforeEach
  public void setUp(){
    given(commonRecord.getParentRecord())
      .willReturn(commonRecord);

    given(commonRecord.getGlobalIdentifier())
      .willReturn("GlobalIdentifier");
    given(commonRecord.getAmount_vat())
      .willReturn(Money.of(new BigDecimal("12").setScale(2, RoundingMode.HALF_UP), "EUR"));
    given(commonRecord.getAmount_novat_base_discount())
      .willReturn(Money.of(new BigDecimal("12").setScale(2, RoundingMode.HALF_UP), "EUR"));
    given(commonRecord.getVatRateBigDecimal())
      .willReturn(new BigDecimal("20").setScale(2, RoundingMode.HALF_UP));
    given(commonRecord.getEntryGate())
      .willReturn("EntryGateCode");
    given(commonRecord.getExitGate())
      .willReturn("ExitGateCode");
    given(commonRecord.getIngestion_time())
      .willReturn(Instant.now());
    given(commonRecord.getRecordCode())
      .willReturn("30");

    given(commonRecord.getDate())
      .willReturn("2018-06-02");

    given(commonRecord.getTime())
      .willReturn("07.22.53");

    given(commonRecord.getNetCode())
      .willReturn("");

    given(commonRecord.getDeviceType())
      .willReturn(TelepassDeviceType.TESSERA_VIACARD);

    given(commonRecord.getDeviceCode())
      .willReturn("1234567890");
    
  }


  @Test
  void mapTollGateDescritption_WITH_TOLLS() throws Exception {

    given(commonRecord.getDescription())
      .willReturn("PIACENZA SUD MILANO SUD");


    Elcit consumo = elcitRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    final String entryDescription = consumo.getTollPointEntry().getGlobalGate().getDescription();
    assertThat(entryDescription)
      .isEqualTo("PIACENZA SUD");
    final String exitDescription = consumo.getTollPointExit().getGlobalGate().getDescription();
    assertThat(exitDescription)
      .isEqualTo("MILANO SUD");
    final String route = consumo.getRoute();
    assertThat(route)
      .isEqualTo("PIACENZA SUD MILANO SUD");
    final String routeName = consumo.getRouteName();
    assertThat(routeName)
      .isNull();
  }

  @Test
  void mapRecordToConsumo_if_NOT_PLATE_DEVICE_will_NOT_set_vehicle_BUT_device_with_seriale() throws Exception {

    given(commonRecord.getDeviceType())
      .willReturn(APPARATO_TELEPASS);

    given(commonRecord.getDeviceCode())
      .willReturn("0775823156");

    Elcit consumo = elcitRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getDevice())
      .isNotNull();

    assertThat(consumo.getDevice().getType())
      .isEqualTo(TipoDispositivoEnum.TELEPASS_ITALIANO);

    assertThat(consumo.getDevice().getSeriale())
      .isEqualTo("0775823156");

    assertThat(consumo.getDevice().getServiceType())
      .isEqualTo(TipoServizioEnum.PEDAGGI_ITALIA);

    assertThat(consumo.getVehicle().getLicensePlate())
      .isNull();

  }

  @Test
  void mapRecordToConsumo_if_PLATE_DEVICE_will_set_vehicle_and_secondary_device_BUT_NOT_device() throws Exception {
    given(commonRecord.getDeviceType())
      .willReturn(APPARATO_TELEPASS);

    given(commonRecord.getTransactionType())
      .willReturn("AC");

    given(commonRecord.getLicensePlate())
      .willReturn("AA000AA");

    Elcit consumo = elcitRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getDevice())
      .isNull();

    assertThat(consumo.getSecondaryDevice().isPresent())
      .isTrue();

    assertThat(consumo.getSecondaryDevice().get().getSeriale())
      .isEqualTo("AA000AA");

    assertThat(consumo.getSecondaryDevice().get().getServiceType())
      .isEqualTo(TipoServizioEnum.PEDAGGI_ITALIA);

    assertThat(consumo.getVehicle().getLicensePlate().getLicenseId())
      .isEqualTo("AA000AA");

    assertThat(consumo.getVehicle().getLicensePlate().getCountryId())
      .isEqualTo(VehicleLicensePlate.NO_COUNTRY);
  }

  @Test
  void mapRecordToConsumo_if_NOT_DEVICE_will_NOT_set_vehicle_NOR_secondary_device() throws Exception {
    given(commonRecord.getDeviceType())
      .willReturn("");

    given(commonRecord.getDeviceCode())
      .willReturn("");

    Elcit consumo = elcitRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getDevice())
      .isNull();

    assertThat(consumo.getSecondaryDevice().isPresent())
      .isFalse();

    assertThat(consumo.getVehicle().getLicensePlate())
      .isNull();
  }
  
  @Test
  void mapRecordToConsumo_truncateTo9Chars_if_viacard() throws Exception {
    
    given(commonRecord.getDeviceType())
    .willReturn(TelepassDeviceType.TESSERA_VIACARD);

    given(commonRecord.getDeviceCode())
    .willReturn("0123456789");
    
    Elcit consumo = elcitRecordConsumoMapper.mapRecordToConsumo(commonRecord);
    
    assertThat(consumo.getDevice()).isNotNull();
    
    
    assertThat(consumo.getDevice().getSeriale()).isEqualTo("123456789");
    
  }

}
