package it.fai.ms.consumi.service.validator;

import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.validation.Message;

import java.util.stream.Collectors;

public final class ValidationOutcomeTestFactory {

  public static ValidationOutcome newValidationOutcome() {
    final var validationOutcome = new ValidationOutcome();
    validationOutcome.setStanziamentiParams(StanziamentiParamsTestBuilder.newInstance()
                                                                         .withSupplierCode("::supplierCode::")
                                                                         .withArticle(ArticleTestBuilder.newInstance()
                                                                                                        .withCode("::code::")
                                                                                                        .withGrouping("::grouping::")
                                                                                                        .build())
                                                                         .build());
    return validationOutcome;
  }



  public static ValidationOutcome newValidationOutcomeWithStanzParam(final StanziamentiParams _stanziamentiParams) {
    final var validationOutcome = new ValidationOutcome();
    validationOutcome.setStanziamentiParams(_stanziamentiParams);
    return new ValidationOutcome();
  }



  public static ValidationOutcome newValidationOutcomeFailure(Message ...messages) {
    final var validationOutcome = new ValidationOutcome();
    validationOutcome.setValidationOk(false);
    for(Message m: messages) {
      validationOutcome.addMessage(m);
    }
    validationOutcome.setStanziamentiParams(StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode::")
      .withArticle(ArticleTestBuilder.newInstance()
        .withCode("::code::")
        .withGrouping("::grouping::")
        .build())
      .build());
    return validationOutcome;
  }
  public static ValidationOutcome newValidationOutcomeFailure(String code, String text) {
    final var validationOutcome = new ValidationOutcome();
    validationOutcome.setValidationOk(false);
    validationOutcome.addMessage(new Message(code).withText(text));
    validationOutcome.setStanziamentiParams(StanziamentiParamsTestBuilder.newInstance()
                                                                         .withSupplierCode("::supplierCode::")
                                                                         .withArticle(ArticleTestBuilder.newInstance()
                                                                                                        .withCode("::code::")
                                                                                                        .withGrouping("::grouping::")
                                                                                                        .build())
                                                                         .build());
    return validationOutcome;
  }

  public static ValidationOutcome newValidationOutcomeFailureWithFatalError() {
    final var validationOutcome = new ValidationOutcome();
    validationOutcome.setValidationOk(false);
    validationOutcome.addMessage(new Message("::failure::").withText("::text::"));
    validationOutcome.setFatalError(true);
    return validationOutcome;
  }

  public static String descriptionFromValidationOutcome(ValidationOutcome validationOutcome) {
    return "\nValidation is " + validationOutcome.getMessages().stream().map(m -> m.getCode() + "#" + m.getText()).collect(Collectors.joining(","))+"\n";
  }

}
