package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.performance.simpleprofiler;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class MethodCallInfo {

  public final String declaringTypeName;
  public final String name;
  public final Object[] args;
  public final Instant enterTimestamp;

  public final MethodCallInfo parent;
  public final List<MethodCallInfo> children;

  private Stack<MethodCallInfo> callStack;

  public Object result;
  public Instant exitTimestamp;

  private MethodCallInfo(Instant enterTimestamp, String declaringTypeName, String name, Object[] args, MethodCallInfo parent) {
    this.enterTimestamp = enterTimestamp;
    this.declaringTypeName = declaringTypeName;
    this.name = name;
    this.args = args;
    this.children = new LinkedList<>();

    this.parent = parent;
    if (parent != null) {
      this.parent.children.add(this);
    }
  }

  public static MethodCallInfo newRootInstance(Instant t0){
    return new MethodCallInfo(t0, "root", "root", new Object[0], null);
  }

  public static MethodCallInfo initTrace(String declaringTypeName, String name, Object[] args, MethodCallInfo parent) {
    return new MethodCallInfo(Instant.now(), declaringTypeName, name, args, parent == null ? newRootInstance(Instant.now()) : parent);
  }

  public void endTrace(Object result) {
    this.exitTimestamp = Instant.now();
    this.result = result;
  }

  public int nestingLevel() {
    return this.callStack().size();
  }

  public Stack<MethodCallInfo> callStack(){
    if (callStack == null) {
      this.callStack = new Stack<>();
      MethodCallInfo currentMethodCallInfo = parent;
      while (currentMethodCallInfo != null) {
        callStack.push(currentMethodCallInfo);
        currentMethodCallInfo = currentMethodCallInfo.parent;
      }
    }
    return this.callStack;
  }

  public String callStackStringTrace(){
    return this.declaringTypeName + "." + this.name + "§" + callStack().stream().map(methodCallInfo -> methodCallInfo.declaringTypeName + "." + methodCallInfo.name).collect(Collectors.joining("§"));

  }

  public long millisecondsFrom(Instant t0) {
    return enterTimestamp.toEpochMilli() - t0.toEpochMilli();
  }

  public double durationPercent(Instant t0, Instant tN) {
    return Double.valueOf(duration() * 100) / Double.valueOf((tN.toEpochMilli() - t0.toEpochMilli()));
  }

  public long duration() {
    return exitTimestamp.toEpochMilli() - enterTimestamp.toEpochMilli();
  }
}
