package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import java.math.BigDecimal;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.enumeration.PriceSource;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

public class DettaglioStanziamentoCarburanteTestBuilder {

  private static GlobalIdentifier newInternalGlobalIdentifier() {
    return GlobalIdentifierTestBuilder.newInstance()
                                      .internal()
                                      .build();
  }

  public static DettaglioStanziamentoCarburanteTestBuilder newInstance() {
    return new DettaglioStanziamentoCarburanteTestBuilder();
  }

  private DettaglioStanziamentoCarburante dettaglioStanziamento;

  private DettaglioStanziamentoCarburanteTestBuilder() {
  }

  public DettaglioStanziamentoCarburante build() {
    return dettaglioStanziamento;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withAmount(final Amount _amount) {
    dettaglioStanziamento.setAmount(_amount);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withDevice(final Device _device) {
    dettaglioStanziamento.setDevice(_device);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withGlobalIdentifierAndUniqueKey(final GlobalIdentifier _globalIdentifier) {
    dettaglioStanziamento = new DettaglioStanziamentoCarburante(_globalIdentifier);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withServicePartnerDaConsumo() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    dettaglioStanziamento.setServicePartner(new ServicePartner("1"));
    dettaglioStanziamento.getServicePartner()
                         .setPriceSoruce(PriceSource.by_invoice);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withServicePartnerDaTabellaPrezzi() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    dettaglioStanziamento.setServicePartner(new ServicePartner("1"));
    dettaglioStanziamento.getServicePartner()
                         .setPriceSoruce(PriceSource.by_prices_table);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withFuelStation() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    dettaglioStanziamento.setFuelStation(new FuelStation("1"));
    dettaglioStanziamento.getFuelStation()
                         .setFuelStationCode("1");
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withCurrencyEUR() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    CurrencyUnit EUR = Monetary.getCurrency("EUR");
    setAmount(dettaglioStanziamento, EUR);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withCurrencyPLN() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    CurrencyUnit PLN = Monetary.getCurrency("PLN");
    setAmount(dettaglioStanziamento, PLN);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withPriceRefernce(long value) {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    CurrencyUnit EUR = Monetary.getCurrency("EUR");
    dettaglioStanziamento.setPriceReference(new Amount());
    dettaglioStanziamento.getPriceReference()
                         .setVatRate(BigDecimal.TEN);
    dettaglioStanziamento.getPriceReference()
                         .setAmountExcludedVat(Monetary.getDefaultAmountFactory()
                                                       .setCurrency(EUR)
                                                       .setNumber(value)
                                                       .create());
    ;
    return this;
  }

  private void setAmount(DettaglioStanziamentoCarburante dettaglioStanziamento, CurrencyUnit currencyUnit) {

    dettaglioStanziamento.setPriceReference(new Amount());
    dettaglioStanziamento.getPriceReference()
                         .setVatRate(BigDecimal.TEN);
    dettaglioStanziamento.getPriceReference()
                         .setAmountExcludedVat(Monetary.getDefaultAmountFactory()
                                                       .setCurrency(currencyUnit)
                                                       .setNumber(200)
                                                       .create());
    ;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withInvoiceTypeD() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    dettaglioStanziamento.setInvoiceType(InvoiceType.D);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withInvoiceTypeP() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    dettaglioStanziamento.setInvoiceType(InvoiceType.P);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withSource(final Source _source) {
    if (_source != null && _source.getRowNumber() == null)
      _source.setRowNumber(0L);
    dettaglioStanziamento.setSource(_source);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withStanziamento(final String _codiceStanziamento) {
    final Stanziamento stanziamento = new Stanziamento(_codiceStanziamento);
    dettaglioStanziamento.getStanziamenti()
                         .add(stanziamento);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withSupplier(final Supplier _supplier) {
    dettaglioStanziamento.setSupplier(_supplier);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withTransaction(final Transaction _transaction) {
    dettaglioStanziamento.setTransaction(_transaction);
    return this;
  }

  public DettaglioStanziamentoCarburanteTestBuilder withCostComputed() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoCarburante(newInternalGlobalIdentifier());
    }
    CurrencyUnit EUR = Monetary.getCurrency("EUR");
    dettaglioStanziamento.setCostComputed(new Amount());
    dettaglioStanziamento.getCostComputed()
                         .setVatRate(BigDecimal.TEN);
    dettaglioStanziamento.getCostComputed()
                         .setAmountExcludedVat(Monetary.getDefaultAmountFactory()
                                                       .setCurrency(EUR)
                                                       .setNumber(100)
                                                       .create());
    ;
    return this;
  }

}
