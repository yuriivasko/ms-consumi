package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.pedaggi;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.withSettings;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoPedaggioTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.StanziamentoTestBuilder;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.DettaglioStanziamentoDefinitivoProcessor;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.DettaglioStanziamentoProcessor;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.DettaglioStanziamentoProvvisorioProcessor;

@Tag("unit")
class DettaglioStanziamentoPedaggiProcessorImplTest {

  private static DettaglioStanziamentoPedaggio newDettaglioStanziamento() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                   .withInvoiceTypeD()
                                                   .build();
  }

  private static Stanziamento newLastAllocation() {
    return StanziamentoTestBuilder.newInstance()
                                  .withInvoiceTypeD()
                                  .build();
  }

  private static StanziamentiParams newStanziamentiParams() {
    return StanziamentiParamsTestBuilder.newInstance()
                                        .withSupplierCode("::supplier::")
                                        .withFareClass("::fareClass::")
                                        .withRecordCode("::recordCode::")
                                        .withSource("::source::")
                                        .withTransactionDetail("::transactionDetail::")
                                        .withTransactionType("::transactionType::")
                                        .build();
  }

  private static Stanziamento newTempAllocation() {
    return StanziamentoTestBuilder.newInstance()
                                  .withInvoiceTypeD()
                                  .build();
  }

  private DettaglioStanziamentoProcessor                  allocationDetailProcessor;
  private final DettaglioStanziamentoDefinitivoProcessor  mockLastAllocationDetailProcessor = mock(DettaglioStanziamentoDefinitivoProcessor.class,
                                                                                                   withSettings().verboseLogging());
  private final DettaglioStanziamentoProvvisorioProcessor mockTempAllocationDetailProcessor = mock(DettaglioStanziamentoProvvisorioProcessor.class);

  @BeforeEach
  void setUp() throws Exception {
    allocationDetailProcessor = new DettaglioStanziamentoPedaggioProcessorImpl(mockTempAllocationDetailProcessor,
                                                                               mockLastAllocationDetailProcessor);
  }

  @Test
  void when_allocationDetail_is_Last() {

    // given

    final var allocationDetail = newDettaglioStanziamento();

    final var optionalStanziamentiParams = newStanziamentiParams();

    // TODO
    given(mockLastAllocationDetailProcessor.process(eq(allocationDetail), any())).willReturn(Optional.of(allocationDetail));


    // when

    var optionalAllocationDetail = allocationDetailProcessor.process(allocationDetail, optionalStanziamentiParams);

    // then

    then(mockLastAllocationDetailProcessor).should()
                                           .process(eq(allocationDetail), any());
    then(mockLastAllocationDetailProcessor).shouldHaveNoMoreInteractions();

    then(mockTempAllocationDetailProcessor).shouldHaveZeroInteractions();

    // FIXME assertThat(optionalAllocationDetail).contains(newAllocationDetail())
  }

  @Test
  void when_allocationDetail_is_Temp() {

    // given
    final var optionalStanziamentiParams = newStanziamentiParams();
    final var allocationDetail = DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                                         .withInvoiceTypeP()
                                                                         .build();

    // when

    allocationDetailProcessor.process(allocationDetail, optionalStanziamentiParams);

    // then

    then(mockTempAllocationDetailProcessor).should()
                                           .process(eq(allocationDetail), any());
    then(mockTempAllocationDetailProcessor).shouldHaveNoMoreInteractions();

    then(mockLastAllocationDetailProcessor).shouldHaveZeroInteractions();

  }

}
