package it.fai.ms.consumi.service.consumer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd.TrackyCardCarbStdProcessor;
import it.fai.ms.consumi.service.record.carburanti.TrackyCardCarbStdConsumerRecordConsumoMapper;

@Tag("unit")
class TrackyCardCarburantiStandardRecordConsumerTest {

  private final TrackyCardCarbStdConsumerRecordConsumoMapper trackyCardCarbStdConsumerRecordConsumoMapper = mock(TrackyCardCarbStdConsumerRecordConsumoMapper.class);
  private final TrackyCardCarbStdProcessor                   trackyCardCarbStdProcessor                   = mock(TrackyCardCarbStdProcessor.class);
  private final NotificationService                          notificationService                          = mock(NotificationService.class);
  private final ServicePartner                               servicePartner                               = mock(ServicePartner.class);
  private final StanziamentiParams                           stanziamentiParams                           = mock(StanziamentiParams.class);
  private final TrackyCardCarburantiStandardRecord           trackyCardCarburantiStandardRecord           = mock(TrackyCardCarburantiStandardRecord.class);
  private final TrackyCardCarburantiStandard                 consumo                                      = mock(TrackyCardCarburantiStandard.class);

  private       TrackyCardCarburantiStandardRecordConsumer trackyCardCarburantiStandardRecordConsumer;

  @BeforeEach
  void setUp() throws Exception {
    trackyCardCarburantiStandardRecordConsumer = new TrackyCardCarburantiStandardRecordConsumer(
      trackyCardCarbStdConsumerRecordConsumoMapper,
      trackyCardCarbStdProcessor,
      notificationService
    );

    given(trackyCardCarbStdConsumerRecordConsumoMapper.mapRecordToConsumo(any(), any(), eq(trackyCardCarburantiStandardRecord)))
                                                      .willReturn(consumo);

    given(servicePartner.getFormat())
      .willReturn(Format.DRESSER_WS);

  }

  @Test
  void consume() throws Exception {

    trackyCardCarburantiStandardRecordConsumer.consume(stanziamentiParams, servicePartner, trackyCardCarburantiStandardRecord);

    then(trackyCardCarbStdConsumerRecordConsumoMapper)
      .should(times(1))
      .mapRecordToConsumo(stanziamentiParams,servicePartner ,trackyCardCarburantiStandardRecord);

    then(trackyCardCarbStdProcessor)
      .should(times(1))
      .validateAndProcess(eq(consumo), any());

    then(consumo)
      .should(times(1))
      .setServicePartner(eq(servicePartner));

    then(notificationService)
      .should(times(0))
      .notify(eq("FCJOB-199"), any());

  }

  @Test
  void sendErrorNotification() throws Exception {
    given(trackyCardCarbStdConsumerRecordConsumoMapper.mapRecordToConsumo(any(), any(), any()))
      .willThrow(new RuntimeException());

    trackyCardCarburantiStandardRecordConsumer.consume(stanziamentiParams, servicePartner, trackyCardCarburantiStandardRecord);

    then(trackyCardCarbStdProcessor)
      .should(times(0))
      .validateAndProcess(eq(consumo), any());

    then(notificationService)
      .should(times(1))
      .notify(eq("FCJOB-199"), any());
  }
}
