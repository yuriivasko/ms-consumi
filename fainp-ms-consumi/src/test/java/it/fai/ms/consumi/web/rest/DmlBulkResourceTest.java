package it.fai.ms.consumi.web.rest;

import ch.qos.logback.classic.spi.ILoggingEvent;
import it.fai.ms.consumi.AbstractCommonTest;
import it.fai.ms.consumi.repository.storico_dml.*;
import it.fai.ms.consumi.repository.storico_dml.model.*;
import it.fai.ms.consumi.service.DmlBulkService;
import it.fai.ms.consumi.service.impl.DmlBulkServiceImpl;
import it.fai.ms.consumi.service.jms.mapper.*;
import it.fai.ms.consumi.service.jms.producer.StoricoJmsProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.Serializable;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest(showSql = false)
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.ms.consumi.repository.storico_dml" })
@Tag("unit")
public class DmlBulkResourceTest extends AbstractCommonTest {

//  Logger logger = (Logger) LoggerFactory.getLogger(JmsQueueSenderUtil.class);

  List<Object> messages = new LinkedList<>();

  @Autowired StoricoDispositivoRepository storicoDispositivoRepository;
  @Autowired StoricoContrattoRepository storicoContrattoRepository;
  @Autowired StoricoOrdiniClienteRepository storicoOrdiniClienteRepository;
  @Autowired StoricoRichiestaRepository storicoRichiestaRepository;
  @Autowired StoricoVeicoloRepository storicoVeicoloRepository;
  @Autowired StoricoAssociazioneDispositivoVeicoloRepository storicoAssociazioneDispositivoVeicoloRepository;

  StoricoJmsProducer storicoJmsProducer;

  DmlBulkService dmlBulkService;

  DmlBulkResource dmlBulkResource;

  @Test
  public void sendAllEuroClassByCustomerCode() throws Exception {
    Function<String, Void> addVehicleByCustomerCode = (String cliente) -> { 
      String uid = RandomString();
      String device_uid = RandomString();
      String contratto_uid = RandomString();
      ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(uid));
      ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(device_uid, contratto_uid));    
      ((JpaRepository<StoricoAssociazioneDispositivoVeicolo, DmlEmbeddedId>) storicoAssociazioneDispositivoVeicoloRepository).save(addStoricoAssociazione(device_uid, uid));
      ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(contratto_uid, cliente));
      return null;
    };

    long size = storicoVeicoloRepository.count();
    String cliente = RandomString();       

    addVehicleByCustomerCode.apply(cliente);
    addVehicleByCustomerCode.apply(cliente);
    addVehicleByCustomerCode.apply(cliente);
    addVehicleByCustomerCode.apply(cliente);
    addVehicleByCustomerCode.apply(cliente);
    addVehicleByCustomerCode.apply(cliente);

    assertThat(size + 6).isEqualTo(storicoVeicoloRepository.count());
    assertThat(6).isEqualTo(storicoVeicoloRepository.findAllByCustomerCode(cliente).size());

    ResponseEntity<Void> response = dmlBulkResource.sendAllEuroClassByCustomerCode(cliente);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(6);
  }

  @Test
  public void sendAllEuroClass() throws Exception {
    long size = storicoVeicoloRepository.count();

    ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(RandomString()));
    ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(RandomString()));
    ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(RandomString()));
    ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(RandomString()));
    ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(RandomString()));
    ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(RandomString()));
    ((JpaRepository<StoricoVeicolo, DmlEmbeddedId>) storicoVeicoloRepository).save(addStoricoVeicolo(RandomString()));
    assertThat(size + 7).isEqualTo(storicoVeicoloRepository.count());

    ResponseEntity<Void> response = dmlBulkResource.sendAllEuroClass();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(7);
  }

  @Test
  public void sendAllContractStatusByCustomerCode() throws Exception {
    long size = storicoContrattoRepository.count();
    String cliente = RandomString();

    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), RandomString()));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), RandomString()));
    assertThat(size + 7).isEqualTo(storicoContrattoRepository.count());
    assertThat(5).isEqualTo(storicoContrattoRepository.findAllByCodiceAzienda(cliente).size());

    ResponseEntity<Void> response = dmlBulkResource.sendAllContractStatusByCustomerCode(cliente);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(5);
  }

  @Test
  public void sendAllContractStatus() throws Exception {
    long size = storicoContrattoRepository.count();
    String cliente = RandomString();

    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(RandomString(), RandomString()));
    assertThat(size + 5).isEqualTo(storicoContrattoRepository.count());

    ResponseEntity<Void> response = dmlBulkResource.sendAllContractStatus();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(5);
  }

  @Test
  public void sendAllRequestByCustomerCode() throws Exception {
    long size = storicoRichiestaRepository.count();
    String cliente = RandomString();
    String ordineCliente = RandomString();
    String ordineCliente2 = RandomString();

    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(cliente, ordineCliente));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(cliente, ordineCliente2));

    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente2));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente2));
    assertThat(size + 7).isEqualTo(storicoRichiestaRepository.count());

    ResponseEntity<Void> response = dmlBulkResource.sendAllRequestByCustomerCode(cliente);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(7);
  }

  @Test
  public void sendAllRequests() throws Exception {
    long size = storicoRichiestaRepository.count();
    String ordineCliente = RandomString();

    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(RandomString(), ordineCliente));

    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    ((JpaRepository<StoricoRichiesta, DmlEmbeddedId>) storicoRichiestaRepository).save(addStoricoRichiesta(ordineCliente));
    assertThat(size + 5).isEqualTo(storicoRichiestaRepository.count());

    ResponseEntity<Void> response = dmlBulkResource.sendAllRequest();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(5);
  }

  @Test
  public void sendAllCustomerOrder() throws Exception {
    long size = storicoOrdiniClienteRepository.count();

    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(RandomString()));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(RandomString()));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(RandomString()));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(RandomString()));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(RandomString()));
    assertThat(size + 5).isEqualTo(storicoOrdiniClienteRepository.count());

    ResponseEntity<Void> response = dmlBulkResource.sendAllCustomerOrder();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(5);
  }

  @Test
  public void sendAllCustomerOrderByCustomerCode() throws Exception {
    long size = storicoOrdiniClienteRepository.count();
    String cliente = RandomString();

    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(cliente));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(cliente));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(cliente));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(cliente));
    ((JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>) storicoOrdiniClienteRepository).save(addStoricoOrdiniCliente(cliente));
    assertThat(size + 5).isEqualTo(storicoOrdiniClienteRepository.count());
    assertThat(5).isEqualTo(storicoOrdiniClienteRepository.findAllByCodiceCliente(cliente).size());

    ResponseEntity<Void> response = dmlBulkResource.sendAllCustomerOrder();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(5);
  }

  @Test
  public void sendAllDevices() throws Exception {
    long size = storicoDispositivoRepository.count();

    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(RandomString()));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(RandomString()));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(RandomString()));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(RandomString()));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(RandomString()));
    assertThat(size + 5).isEqualTo(storicoDispositivoRepository.count());

    ResponseEntity<Void> response = dmlBulkResource.sendAllDevices();
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(5);
  }

  @Test
  public void sendAllDevicesByCustomerCode() throws Exception {
    long size = storicoDispositivoRepository.count();
    String contratto = RandomString();
    String contratto2 = RandomString();
    String contratto3 = RandomString();
    String cliente = RandomString();

    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(contratto, cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(contratto, cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(contratto2, cliente));
    ((JpaRepository<StoricoContratto, DmlEmbeddedId>) storicoContrattoRepository).save(addStoricoContratto(contratto3, cliente));

    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto2));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto3));
    ((JpaRepository<StoricoDispositivo, DmlEmbeddedId>) storicoDispositivoRepository).save(addStoricoDispositivo(contratto3));
    assertThat(size + 8).isEqualTo(storicoDispositivoRepository.count());
    assertThat(8).isEqualTo(storicoDispositivoRepository.findAllByCustomerCode(cliente).size());

    ResponseEntity<Void> response = dmlBulkResource.sendAllDevicesByCustomerCode(cliente);
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

    countLogAssert(8);
  }

  private void countLogAssert(final int k) throws Exception {
    int n = 0;
    assertThat(messages.size()).isEqualTo(k);
  }

  private StoricoOrdiniCliente addStoricoOrdiniCliente(String randomString, String ordine) {
    StoricoOrdiniCliente e = new StoricoOrdiniCliente();
    e.setDmlRevisionTimestamp(Instant.now());
    e.setCodiceCliente(randomString);
    e.setRagioneSociale(RandomString());
    e.setClienteAssegnatario(RandomString());
    e.setDataUltimaVariazione(Instant.now());
    e.setNumeroOrdine(ordine);
    e.setOrdineClienteDmlUniqueIdentifier(ordine);
    return e;
  }

  private StoricoOrdiniCliente addStoricoOrdiniCliente(String randomString) {
    StoricoOrdiniCliente e = new StoricoOrdiniCliente();
    e.setDmlRevisionTimestamp(Instant.now());
    e.setCodiceCliente(randomString);
    e.setRagioneSociale(RandomString());
    e.setClienteAssegnatario(RandomString());
    e.setDataUltimaVariazione(Instant.now());
    e.setNumeroOrdine(RandomString());
    return e;
  }

  private StoricoContratto addStoricoContratto(String contratto, String cliente) {
    StoricoContratto e = new StoricoContratto();
    e.setDmlRevisionTimestamp(Instant.now());
    e.setDmlUniqueIdentifier(RandomString());
    e.setCodiceAzienda(cliente);
    e.setContrattoNumero(contratto);
    return e;
  }

  private static StoricoDispositivo addStoricoDispositivo(String uid, String contrattoDmlUniqueIdentifier) {
    StoricoDispositivo e = new StoricoDispositivo();
    e.setDmlRevisionTimestamp(Instant.now());
    e.setDmlUniqueIdentifier(uid);
    e.setNoteTecniche(RandomString());
    e.setTarga2(RandomString());
    e.setNoteOperatore(RandomString());
    e.setContrattoDmlUniqueIdentifier(contrattoDmlUniqueIdentifier);
    return e;
  }

  private static StoricoDispositivo addStoricoDispositivo(String contrattoDmlUniqueIdentifier) {
    StoricoDispositivo e = new StoricoDispositivo();
    e.setDmlRevisionTimestamp(Instant.now());
    e.setDmlUniqueIdentifier(RandomString());
    e.setTarga2(RandomString());
    e.setNoteOperatore(RandomString());
    e.setContrattoDmlUniqueIdentifier(contrattoDmlUniqueIdentifier);
    return e;
  }

  private static StoricoRichiesta addStoricoRichiesta(String ordineCliente) {
    StoricoRichiesta e = new StoricoRichiesta();
    e.setDmlRevisionTimestamp(Instant.now());
    e.setAnomalia(RandomString());
    e.setDispositivoDmlUniqueIdentifier(RandomString());
    e.setOrdineClienteDmlUniqueIdentifier(ordineCliente);
    e.setDataUltimaVariazione(Instant.now());
    return e;
  }

  private static StoricoVeicolo addStoricoVeicolo(String uid) {
    StoricoVeicolo e = new StoricoVeicolo();
    e.setClasseEuro(randomElement("1", "2", "3", "4", "5", "6"));
    e.setDmlRevisionTimestamp(Instant.now());
    e.setDmlUniqueIdentifier(uid);
    e.setNazione(randomElement("I", "F"));
    e.setTarga(RandomString());
    return e;
  }

  private static StoricoAssociazioneDispositivoVeicolo addStoricoAssociazione(String device_uid, String uid) {
    StoricoAssociazioneDispositivoVeicolo e = new StoricoAssociazioneDispositivoVeicolo();
    e.setDataAssociazione(Instant.now());
    e.setDmlRevisionTimestamp(Instant.now());
    e.setDmlUniqueIdentifier(device_uid);
    e.setVeicoloDmlUniqueIdentifier(uid);
    return e;
  }

  @BeforeEach
  public void setUp() throws Exception {
    messages.clear();

    storicoJmsProducer = new StoricoJmsProducer(mockJmsProperties(),
                                                new DispositivoDMLDTOMapper(),
                                                new StoricoOrdiniClienteDMLDTOMapper(),
                                                new StoricoRichiestaDMLDTOMapper(),
                                                new StoricoContrattoDMLDTOMapper(),
                                                new StoricoClasseEuroDMLDTOMapper()){
      @Override
      public void send(Serializable data) {
        messages.add(data);
      }

      @Override
      public void send(StoricoDispositivo data) {
        messages.add(data);
      }

      @Override
      public void send(StoricoOrdiniCliente data) {
        messages.add(data);
      }

      @Override
      public void send(StoricoRichiesta data) {
        messages.add(data);
      }

      @Override
      public void send(StoricoContratto data) {
        messages.add(data);
      }

      @Override
      public void send(StoricoVeicolo data) {
        messages.add(data);
      }
    };

    dmlBulkService = new DmlBulkServiceImpl(storicoDispositivoRepository,
                                            storicoOrdiniClienteRepository,
                                            storicoRichiestaRepository,
                                            storicoContrattoRepository,
                                            storicoVeicoloRepository);

    dmlBulkResource = new DmlBulkResource(storicoJmsProducer, dmlBulkService);    
  }


}
