package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import java.util.Currency;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoCarburanteTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;

@Tag("unit")
class DettaglioStanziamentoCaburantiDaConsumoProcessorImplTest {

  private DettaglioStanziamentoCaburantiDaConsumoProcessorImpl dettaglioStanziamentoProcessor;
  private final PriceCalculatedByNavService                    mockPriceCalculatedByNavClient = mock(PriceCalculatedByNavService.class);
  private final CurrencyRateService                            mockCurrencyRateService        = mock(CurrencyRateService.class);

  @BeforeEach
  void setUp() throws Exception {

    dettaglioStanziamentoProcessor = new DettaglioStanziamentoCaburantiDaConsumoProcessorImpl(mockPriceCalculatedByNavClient,
                                                                                              mockCurrencyRateService);

  }

  @Test
  void when_dettaglioStanziamento_carburante_Definitivo() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaConsumo()
                                                                          .withCurrencyEUR()
                                                                          .build();

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();
    
    Amount priceReference = dettaglioStanziamento.getPriceReference();

    // when
    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then
    // InOrder inOrder = Mockito.inOrder(mockCurrencyRateService);
    // inOrder.verify(mockCurrencyRateService).convertAmount(eq(dettaglioStanziamento.getCostFaiReserved()),eq(targetCurrency),
    // eq(dettaglioStanziamento.getEntryDateTime()));

    verify(mockCurrencyRateService).convertAmount(eq(priceReference), any(Currency.class),
                                                  eq(dettaglioStanziamento.getEntryDateTime()));

    verify(mockPriceCalculatedByNavClient).setCostCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));
    verify(mockPriceCalculatedByNavClient).setPriceCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));

    DettaglioStanziamentoCarburante rs = list.get(0);
    assertEquals(InvoiceType.D, rs.getInvoiceType());
  }
  
  @Test
  void when_dettaglioStanziamento_carburante_Definitivo_SoloRicavo() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaConsumo()
                                                                          .withCurrencyEUR()
                                                                          .build();

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .withCostoRicavo(CostoRicavo.R)
                                                          .build();

    Amount priceReference = dettaglioStanziamento.getPriceReference();

    // when
    List<DettaglioStanziamentoCarburante> list = dettaglioStanziamentoProcessor.process(dettaglioStanziamento, stanziamentiParams);

    // then
    // InOrder inOrder = Mockito.inOrder(mockCurrencyRateService);
    // inOrder.verify(mockCurrencyRateService).convertAmount(eq(dettaglioStanziamento.getCostFaiReserved()),eq(targetCurrency),
    // eq(dettaglioStanziamento.getEntryDateTime()));

    verify(mockCurrencyRateService).convertAmount(eq(priceReference), any(Currency.class),
                                                  eq(dettaglioStanziamento.getEntryDateTime()));

    verify(mockPriceCalculatedByNavClient,never()).setCostCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));
    verify(mockPriceCalculatedByNavClient).setPriceCalculatedAmountByNav(eq(dettaglioStanziamento), eq(stanziamentiParams));

    DettaglioStanziamentoCarburante rs = list.get(0);
    assertEquals(InvoiceType.D, rs.getInvoiceType());
  }

}
