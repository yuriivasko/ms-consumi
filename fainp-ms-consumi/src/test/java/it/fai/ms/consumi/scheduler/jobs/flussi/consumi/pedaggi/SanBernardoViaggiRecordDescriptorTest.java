package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoViaggiRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@Tag("unit")
public class SanBernardoViaggiRecordDescriptorTest {

    private String data;
    private final String FILENAME="20180531_elenco_viaggi_CM.csv";

    private SanBernardoViaggiRecordDescriptor recordDescriptor;

    @BeforeEach
    public void init(){
        data = "Carta SISEX;9756934101999003952;NORD;1;31/05/2018 18:17;4A;10888";
        recordDescriptor = new SanBernardoViaggiRecordDescriptor();
    }


    @Test
    public void readRecord(){
        SanBernardoViaggiRecord record = recordDescriptor.decodeRecordCodeAndCallSetFromString(data,"",FILENAME,1);
        assertThat(record.getClasse()).isEqualTo("4A");
        assertThat(record.getData()).isEqualTo("31/05/2018 18:17");
        assertThat(record.getNumeroPacchetto()).isEqualTo("10888");
        assertThat(record.getPiazzale()).isEqualTo("NORD");
        assertThat(record.getPista()).isEqualTo("1");
        assertThat(record.getTipoTitolo()).isEqualTo("Carta SISEX");
        assertThat(record.getPanNumber()).isEqualTo("9756934101999003952");

    }

    @Test
    public void getStartEndPosTotalRows() {
        assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(()->recordDescriptor.getStartEndPosTotalRows())
                .matches(e->e.getMessage().equals("Not implemented"));
    }

    @Test
    public void getHeaderBegin() {
        assertThat(recordDescriptor.getHeaderBegin()).isEqualTo("Tipo Titolo;Pan Number;Piazzale;Pista;Data;Classe;Numero Pacchetto");
    }

    @Test
    public void getFooterCodeBegin() {
        assertThat(recordDescriptor.getFooterCodeBegin()).isEqualTo("Numero Viaggi");
    }

    @Test
    public void getLinesToSubtractToMatchDeclaration() {
        assertThat(recordDescriptor.getLinesToSubtractToMatchDeclaration())
                .isEqualTo(0);
    }

    @Test
    public void extractRecordCode_() {
        assertThat(recordDescriptor.extractRecordCode(data))
                .isEqualTo("");
    }





}
