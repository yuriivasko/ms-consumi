package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import javax.inject.Inject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.StanziamentoCodeGeneratorStub;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;

@Tag("integration")
public class ElviaJobWithESand2ThreadIntTest
  extends ElviaJobWithESIntTest {
  
  @Inject
  private ElviaProcessor defaultProcessor;

  @Inject
  private ElviaRecordConsumoMapper defaultRecordConsumoMapper;

  @Inject
  private ElviaGenericoProcessor consumoGenericoProcessor;

  @Inject
  private ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper;

  @Inject
  private RecordPersistenceService recordPersistenceService;

  @Inject
  protected ServiceProviderRepository serviceProviderRepository;

  @Inject
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Inject
  private PlatformTransactionManager transactionManager;

  @Inject
  private NotConfirmedTemporaryAllocationsService notConfirmedAllocationService;


  @Autowired
  private StanziamentoCodeGeneratorStub stanziamentoCodeGeneratorStub;


  @BeforeEach
  public void setUp() throws Exception {

    ElviaDescriptor descriptor = new ElviaDescriptor();
    super.setUpMockContext("Elvia");

    stanziamentoCodeGeneratorStub.reset();

    job = new ElviaJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      descriptor,
      defaultProcessor,
      defaultRecordConsumoMapper,
      consumoGenericoProcessor,
      recordConsumoGenericoMapper,
      stanziamentiToNavJmsPublisher,
      notConfirmedAllocationService
    );
    job.THREAD_POOL_SIZE = 2;
    //job.DEVCONF_SKIP_ESPERSISTING = true;
    job.init();
  }

  @AfterEach
  public void tearDown(){
    Mockito.validateMockitoUsage();
  }

}


