package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.dgdtoll;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static it.fai.ms.consumi.domain.InvoiceType.D;
import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.COSTO_RICAVO;
import static java.time.Month.JUNE;
import static org.assertj.core.api.Assertions.assertThat;

@Tag("development")
public class DgdTollJobDaConsumoWithESIntTest2
  extends AbstractDgdTollJobWithEsIntTest {

  final static String FILENAME = "/test-files/dgdtoll/FAI_RE181210-01.TXT.20181211100014";
  final static String PARTNERCODE = "CH01";


  public DgdTollJobDaConsumoWithESIntTest2() throws URISyntaxException {
    super(FILENAME, PARTNERCODE);
  }

  @Test
  @Transactional
  public void process_sample_file_producing_stanziamenti() {

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList.size())
      .isEqualTo(-1);

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size())
      .isEqualTo(-1);

  }
}
