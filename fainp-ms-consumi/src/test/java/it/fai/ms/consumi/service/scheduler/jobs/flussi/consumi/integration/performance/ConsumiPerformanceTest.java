package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.performance;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import it.fai.common.notification.config.NotificationsConfiguration;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.consumi.adapter.nav.NavSupplierCodesAdapter;
import it.fai.ms.consumi.client.AnagaziendeService;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.SchedulerConfiguration;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.processor.consumi.vacon.VaconProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;
import it.fai.ms.consumi.service.record.telepass.VaconRecordConsumoMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractTelepassIntTest;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.conf.MockedNavServicesConfiguration;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.performance.simpleprofiler.MethodCallInfo;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.performance.simpleprofiler.SimpleProfilePrinter;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import liquibase.Liquibase;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Stack;

import static org.mockito.Mockito.mock;

@Tag("performance")
@ActiveProfiles(profiles = {"performance"})
@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties({ApplicationProperties.class})
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableScheduling
@Import({
  JmsConfiguration.class,
  NotificationsConfiguration.class,
  ConsumiPerformanceTest.ConsumiPerformanceConfiguration.class
})
@ComponentScan(
  basePackages = {
    "it.fai.ms.consumi.FaiconsumiApp"
  }
  ,
  excludeFilters = {
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.config\\.SchedulerConfiguration")
  }
)
@SpringBootTest
public class ConsumiPerformanceTest {
  private final Logger log = LoggerFactory.getLogger(this.getClass());
  private static final String BASE_PATH = "/test-files/consumi_performance/";

  //  private static Queue<MethodCallInfo> methodCallInfosQueue;
  private static Stack<MethodCallInfo> methodCallInfosStack;
//  private static AtomicLong progressiveNumber;

  @Autowired
  private ElviaJob elviaJob;

  @Autowired
  private VaconJob vaconJob;

  public static void main(String[] args) {
    int lines = 10000;
    Path logFile = Paths.get(BASE_PATH + "DA06A284.ELVIA.FAI.N00002_performance_" + lines);
    String targa1 = "AA000AA";
    int targa2 = 0;
    try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(logFile, StandardCharsets.UTF_8, StandardOpenOption.WRITE))) {

      writer.println("HR0000000002018-03-2370138276848690SAMPLE PARTNER                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ");
      for (int i = 0; i < 10; i++) {
        writer.println("DT276848690173302108AC2018-02-0400.00.01000000050000000000000000000500TGTARGA N.            " + targa1 + "             Accesso Area C di Milano              2018-07-23+000000000020180323ES.ART.4  0000000G");
      }
      writer.println("TR9999999992018-03-23" + StringUtils.leftPad("" + lines, 32, "0") + "000000000000000000000000000");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @BeforeAll
  public static void setUp() {
    LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    loggerContext.getLoggerList().forEach(logger -> logger.setLevel(Level.ERROR));
  }

  @Test
  @Transactional
  public void test_Elvia_performance_1() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_1";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printFlatTimeTableOnStdOut();

  }
  
  @Test
  @Transactional
  public void test_Elvia_performance_2() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_2";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printFlatTimeTableOnStdOut();

  }

  @Test
  @Transactional
  public void test_Elvia_performance_2_AREAC() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_2_AREAC";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printFlatTimeTableOnStdOut();

  }

  @Test
  @Transactional
  public void test_Elvia_performance_2_DISPOSITIVO() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_2_DISPOSITIVO";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printFlatTimeTableOnStdOut();

  }

  @Test
  @Transactional
  public void test_Elvia_performance_2_VEICOLO() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_2_VEICOLO";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printFlatTimeTableOnStdOut();

  }

  @Test
  @Transactional
  public void test_Elvia_performance_10() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_10";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printGroupedTimeTableOnStdOut();
  }

  @Test
  @Transactional
  public void test_Elvia_performance_100() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_100";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printGroupedTimeTableOnStdOut();
  }

  @Test
  @Transactional
  public void test_Elvia_performance_1000() {
    String filename = "DA06A284.ELVIA.FAI.N00002_performance_1000";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printGroupedTimeTableOnStdOut();

  }

  @Test
  @Transactional
  public void test_Vacon_performance_1_per_type() {
    String filename = "DA06A284.VACON.FAI.N00005.processed_1_per_type";

    Instant now = Instant.now();
    MethodCallInfo rootCall = MethodCallInfo.newRootInstance(now);
    methodCallInfosStack = new Stack<>();
    methodCallInfosStack.push(rootCall);

    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);

    SimpleProfilePrinter simpleProfilePrinter = new SimpleProfilePrinter(filename);
    simpleProfilePrinter.init(now, rootCall);

    vaconJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), vaconServicePartner());

    simpleProfilePrinter.end(Instant.now());
    simpleProfilePrinter.printFlatTimeTableOnStdOut();

  }

  public ServicePartner elviaServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.ELVIA);
    return serviceProvider;
  }

  public ServicePartner vaconServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.VACON);
    return serviceProvider;
  }


  protected String getAbsolutePath(String relativePath) {
    try {
      return new File(AbstractTelepassIntTest.class.getResource(relativePath).toURI()).getAbsolutePath();
    } catch (URISyntaxException e) {
      throw new RuntimeException("Cannot find file in given path!", e);
    }
  }

  @Aspect
  public static class ConsumiPerformanceLoggingAspect {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Pointcut that matches this test.
     */
    @Pointcut("within(it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.performance..*)")
    public void testPackagesPointcut() {
      // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Pointcut that matches all consumi packages.
     */
    @Pointcut("within(it.fai.ms.consumi..*)")
    public void consumiPackagesPointcut() {
      // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Pointcut that matches all persistence package.
     */
    @Pointcut("target(javax.persistence.EntityManager)")
    public void entityMangerPackagesPointcut() {
      // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Pointcut that matches all persistence package.
     */
    @Pointcut("target(javax.persistence.Query)")
    public void queryPackagesPointcut() {
      // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Pointcut("consumiPackagesPointcut() || entityMangerPackagesPointcut() || queryPackagesPointcut()")
    public void includedPointcuts() {
      // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    @Pointcut("testPackagesPointcut()")
    public void excludedPointcuts() {
      // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Advice that logs when a method is entered and exited.
     */
    @Around("includedPointcuts() && !excludedPointcuts()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
      String declaringTypeName = joinPoint.getSignature().getDeclaringTypeName();
      String name = joinPoint.getSignature().getName();
      Object[] args = joinPoint.getArgs();

      MethodCallInfo methodCallInfo = null;
      if (methodCallInfosStack != null) {
        methodCallInfo = MethodCallInfo.initTrace(declaringTypeName, name, args, methodCallInfosStack.peek());
        ConsumiPerformanceTest.methodCallInfosStack.push(methodCallInfo);
//        ConsumiPerformanceTest.methodCallInfosQueue.add(methodCallInfo);
      }

      Object result = null;
      try {
        result = joinPoint.proceed();
      } catch (IllegalArgumentException e) {
        log.error("Illegal argument: {} in {}.{}()",
          Arrays.toString(joinPoint.getArgs()),
          joinPoint.getSignature().getDeclaringTypeName(),
          joinPoint.getSignature().getName());

        throw e;
      } finally {
        if (methodCallInfosStack != null) {
          methodCallInfo.endTrace(result);
          ConsumiPerformanceTest.methodCallInfosStack.pop();
        }
      }
      return result;
    }
  }

  //@Configuration
  @Import(MockedNavServicesConfiguration.class)
  @EnableAspectJAutoProxy
  public static class ConsumiPerformanceConfiguration {

    @Bean
    @Primary
    public AnagaziendeService anagaziendeService() {
      return mock(AnagaziendeService.class);
    }

    @Bean
    @Primary
    public NavSupplierCodesAdapter navSupplierCodesAdapter() {
      return mock(NavSupplierCodesAdapter.class);
    }

    @Bean
    @Primary
    public SchedulerConfiguration schedulerConfiguration() {
      return mock(SchedulerConfiguration.class);
    }

    @Bean
    @Primary
    public ConsumiPerformanceLoggingAspect consumiPerformanceLoggingAspect() {
      return new ConsumiPerformanceLoggingAspect();
    }

    @Bean
    @Primary
    public Liquibase liquibase() {
      return mock(Liquibase.class);
    }

    @Bean
    @Primary
    protected ElviaDescriptor elviaDescriptor() {
      return new ElviaDescriptor();
    }

    @Bean
    @Primary
    public ElviaJob elviaJob(PlatformTransactionManager transactionManager, StanziamentiParamsValidator stanziamentiParamsValidator, NotificationService notificationService, RecordPersistenceService persistenceService, ElviaDescriptor recordDescriptor, ElviaProcessor defaultProcessor, ElviaRecordConsumoMapper defaultRecordConsumoMapper, ElviaGenericoProcessor consumoGenericoProcessor, ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper, StanziamentiToNavPublisher stanziamentiToNavJmsProducer, NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {
      return new ElviaJobForPerformanceTest(transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, defaultProcessor, defaultRecordConsumoMapper, consumoGenericoProcessor, recordConsumoGenericoMapper, stanziamentiToNavJmsProducer, notConfirmedTemporaryAllocationsService);
    }

    public class ElviaJobForPerformanceTest extends ElviaJob {

      public ElviaJobForPerformanceTest(PlatformTransactionManager transactionManager, StanziamentiParamsValidator stanziamentiParamsValidator, NotificationService notificationService, RecordPersistenceService persistenceService, ElviaDescriptor recordDescriptor, ElviaProcessor defaultProcessor, ElviaRecordConsumoMapper defaultRecordConsumoMapper, ElviaGenericoProcessor consumoGenericoProcessor, ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper, StanziamentiToNavPublisher stanziamentiToNavJmsProducer, NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {
        super(transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, defaultProcessor, defaultRecordConsumoMapper, consumoGenericoProcessor, recordConsumoGenericoMapper, stanziamentiToNavJmsProducer, notConfirmedTemporaryAllocationsService);
      }

      @Override
      protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {}
    }

    @Bean
    @Primary
    protected VaconDescriptor vaconDescriptor() {
      return new VaconDescriptor();
    }

    @Bean
    @Primary
    public VaconJob vaconJob(PlatformTransactionManager transactionManager, StanziamentiParamsValidator stanziamentiParamsValidator, NotificationService notificationService, RecordPersistenceService persistenceService, VaconDescriptor recordDescriptor, VaconProcessor defaultProcessor, VaconRecordConsumoMapper defaultRecordConsumoMapper, StanziamentiToNavPublisher stanziamentiToNavJmsProducer, NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {
      return new VaconJobForPerformanceTest(transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, defaultProcessor, defaultRecordConsumoMapper, stanziamentiToNavJmsProducer, notConfirmedTemporaryAllocationsService);
    }

    public class VaconJobForPerformanceTest extends VaconJob {

      public VaconJobForPerformanceTest(PlatformTransactionManager transactionManager, StanziamentiParamsValidator stanziamentiParamsValidator, NotificationService notificationService, RecordPersistenceService persistenceService, VaconDescriptor recordDescriptor, VaconProcessor defaultProcessor, VaconRecordConsumoMapper defaultRecordConsumoMapper, StanziamentiToNavPublisher stanziamentiToNavJmsProducer, NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {
        super(transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, defaultProcessor, defaultRecordConsumoMapper, stanziamentiToNavJmsProducer, notConfirmedTemporaryAllocationsService);
      }

      @Override
      protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
      }
    }


  }
  
  // CLEAN DB
  /*
   
delete from faiconsumi.dbo.dettagliostanziamento_stanziamento where stanziamenti_codice_stanziamento like '%[_]D';
delete from faiconsumi.dbo.stanziamento where codice_stanziamento like '%[_]D';

delete from faiconsumi.dbo.dettaglio_stanziamento_pedaggi where file_name like '%performance%';

delete from faiconsumi.dbo.dettaglio_stanziamento where id in (
select s.id from faiconsumi.dbo.dettaglio_stanziamento s
left join faiconsumi.dbo.dettaglio_stanziamento_carburanti c on  c.id=s.id
left join faiconsumi.dbo.dettaglio_stanziamento_treni t on  t.id=s.id
left join faiconsumi.dbo.dettaglio_stanziamento_pedaggi p on  p.id=s.id
left join faiconsumi.dbo.dettaglio_stanziamento_generico g on  g.id=s.id
where c.id is null and p.id is null and t.id is null and g.id is null);
   
   */
}


