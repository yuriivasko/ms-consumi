package it.fai.ms.consumi.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit")
public class OptionalWithNullET {

  private static class Root {
    Nested nested;
    public Root(Nested nested) {
      this.nested = nested;
    }

    public Nested getNested() {
      return nested;
    }
  }

  private static class Nested {
    Leaf leaf;

    public Nested(Leaf leaf) {
      this.leaf = leaf;
    }

    public Leaf getLeaf() {
      return leaf;
    }
  }

  private static class Leaf {
  }

  @Test
  public void given_legacy_object_with_null_nested_properties_it_should_return_empty() {
    Root rootObject = new Root(null);
    Optional<Leaf> res = Optional.ofNullable(rootObject)
                                 .map(root -> root.getNested())
                                 .map(nested -> nested.getLeaf());

    assertThat(res)
      .isNotNull();

    assertThat(res.isPresent())
      .isFalse();
  }

}
