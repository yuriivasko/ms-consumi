package it.fai.ms.consumi.service.processor.dettagli_stanziamenti;

import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoPedaggioTestBuilder;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.pedaggi.StanziamentoProcessorPedaggio;

@Tag("unit")
class DettaglioStanziamentoDefinitivoProcessorImplTest {

  private static DettaglioStanziamento newLastAllocationDetail() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                   .withInvoiceTypeD()
                                                   .build();
  }

  private static DettaglioStanziamento newLastAllocationDetailWithExternalGlobalIdentifier() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                   .withGlobalIdentifierAndUniqueKey(new GlobalIdentifier("::globalIdentifier::",
                                                                                                          GlobalIdentifierType.EXTERNAL) {
                                                     @Override
                                                     public GlobalIdentifier newGlobalIdentifier() {
                                                       return this;
                                                     }
                                                   })
                                                   .withInvoiceTypeD()
                                                   .build();
  }

  private static DettaglioStanziamento newLastAllocationDetailWithInternalGlobalIdentifier() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                   .withGlobalIdentifierAndUniqueKey(new GlobalIdentifier("::globalIdentifier::",
                                                                                                          GlobalIdentifierType.INTERNAL) {
                                                     @Override
                                                     public GlobalIdentifier newGlobalIdentifier() {
                                                       return this;
                                                     }
                                                   })
                                                   .withInvoiceTypeD()
                                                   .build();
  }

  private static DettaglioStanziamento newTempAllocationDetail() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
                                                   .withInvoiceTypeP()
                                                   .build();
  }

  private       DettaglioStanziamentoDefinitivoProcessor lastAllocationDetailProcessor;
  private final DettaglioStanziamantiRepository          mockAllocationDetailRepository = mock(DettaglioStanziamantiRepository.class);
  private       StanziamentoProcessorPedaggio            processor = mock(StanziamentoProcessorPedaggio.class);
  private final NotificationService                      notificationService = mock(NotificationService.class);


  @BeforeEach
  void setUp() throws Exception {
    lastAllocationDetailProcessor = new DettaglioStanziamentoDefinitivoProcessorImpl(mockAllocationDetailRepository, processor,
                                                                                     notificationService);
  }

  @Test
  void when_thereAreBoth_TempAllocationDetail_and_LastAllocationDetail() {

    // given

    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(Set.of(newTempAllocationDetail(),
                                                                                                  newLastAllocationDetail()));

    // when

    // TOOD
    final var optionalAllocationDetail = lastAllocationDetailProcessor.process(newLastAllocationDetail(), null);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).shouldHaveNoMoreInteractions();

    then(notificationService).should()
                             .notify(any(), any());
    then(notificationService).shouldHaveNoMoreInteractions();

    assertThat(optionalAllocationDetail).isEmpty();
  }

  @Test
  void when_thereArent_allocationDetails() {

    // given

    StanziamentiParams param = new StanziamentiParams(new Supplier("test"));
    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(emptySet());

    // when


    final var optionalAllocationDetail = lastAllocationDetailProcessor.process(newTempAllocationDetail(), param);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).should()
                                        .save(newTempAllocationDetail());
    then(mockAllocationDetailRepository).shouldHaveNoMoreInteractions();

    then(notificationService).shouldHaveZeroInteractions();

    assertThat(optionalAllocationDetail).isPresent();
  }

  @Test
  void when_thereIs_LastAllocationDetail_withExternallGlobalIdentifierType() {

    // given

    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(Set.of(newLastAllocationDetailWithExternalGlobalIdentifier()));

    // when

    final var optionalAllocationDetail = lastAllocationDetailProcessor.process(newLastAllocationDetailWithExternalGlobalIdentifier(),
                                                                               null);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).shouldHaveNoMoreInteractions();

    then(notificationService).should()
                             .notify(any(), any());
    then(notificationService).shouldHaveNoMoreInteractions();

    assertThat(optionalAllocationDetail).isEmpty();
  }

  @Test
  void when_thereIs_LastAllocationDetail_withInternalGlobalIdentifierType() {

    // given

    given(mockAllocationDetailRepository.findByGlobalIdentifierId(any(),any())).willReturn(Set.of(newLastAllocationDetailWithInternalGlobalIdentifier()));

    // when

    final var optionalAllocationDetail = lastAllocationDetailProcessor.process(newLastAllocationDetailWithInternalGlobalIdentifier(),
                                                                               null);

    // then

    then(mockAllocationDetailRepository).should()
                                        .findByGlobalIdentifierId(any(),any());
    then(mockAllocationDetailRepository).should()
                                        .save(newLastAllocationDetailWithInternalGlobalIdentifier());

    then(notificationService).shouldHaveZeroInteractions();

    assertThat(optionalAllocationDetail).isPresent();
  }

}
