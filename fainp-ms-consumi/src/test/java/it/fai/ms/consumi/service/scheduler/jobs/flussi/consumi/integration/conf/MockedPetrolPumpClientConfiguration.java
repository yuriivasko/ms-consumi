package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.conf;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.client.PetrolPumpClient;

@Profile("default")
@Configuration
public class MockedPetrolPumpClientConfiguration {

    @Bean
    public PetrolPumpClient petrolPumpClient() {
        return new PetrolPumpClientImpl();
    }

    class PetrolPumpClientImpl implements PetrolPumpClient{

      @Override
      public List<PriceTableDTO> getPriceTableList(String authorizationToken, String pointCode, String codiceArticolo, Instant date) {
       List<PriceTableDTO> list = new ArrayList<>();

       PriceTableDTO p = new PriceTableDTO();
       p.setReferencePrice(new BigDecimal(0.111));
       p.setCostRevenueFlag(CostRevenue.COST);
       list.add(p);

       // Per il fornitore ES00, solo il costo è provvisorio
       if(!codiceArticolo.equals("096S10") && !codiceArticolo.equals("096SA") ) {
         p = new PriceTableDTO();
         p.setReferencePrice(new BigDecimal(0.122));
         p.setCostRevenueFlag(CostRevenue.REVENUE);
         list.add(p);
       }

        return list;
      }

      @Override
      public PointDTO searchPoint(String authorizationToken, String externalCode) {
        return null;
      }

      @Override
      public PointDTO getPoint(String authorizationToken, String code) {
        return null;
      }



    }

}