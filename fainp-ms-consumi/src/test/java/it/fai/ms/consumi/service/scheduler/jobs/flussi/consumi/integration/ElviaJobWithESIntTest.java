package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import static it.fai.ms.consumi.domain.InvoiceType.D;
import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.COSTO;
import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.COSTO_RICAVO;
import static java.time.Month.FEBRUARY;
import static java.time.Month.MARCH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import ch.qos.logback.classic.Level;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.StanziamentoCodeGeneratorStub;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;

@Tag("integration")
public class ElviaJobWithESIntTest
  extends AbstractTelepassIntTest {

  private transient final Logger log = LoggerFactory.getLogger(getClass());


  private String PARTNERCODE = "CH01";
  protected ElviaJob job;

  @Inject
  private ElviaProcessor defaultProcessor;

  @Inject
  private ElviaRecordConsumoMapper defaultRecordConsumoMapper;

  @Inject
  private ElviaGenericoProcessor consumoGenericoProcessor;

  @Inject
  private ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper;

  @Inject
  private StanziamentoRepository stanziamentiRepository;

  @Inject
  private RecordPersistenceService recordPersistenceService;

  @Inject
  protected ServiceProviderRepository serviceProviderRepository;

  @Inject
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Inject
  private PlatformTransactionManager transactionManager;

  @Inject
  private NotConfirmedTemporaryAllocationsService notConfirmedAllocationService;


  @Autowired
  private StanziamentoCodeGeneratorStub stanziamentoCodeGeneratorStub;

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentoTestRepository;



  @BeforeEach
  public void setUp() throws Exception {

    ElviaDescriptor descriptor = new ElviaDescriptor();
    ElcitDescriptor elcitDescriptor = new ElcitDescriptor();
    super.setUpMockContext("Elvia");

    stanziamentoCodeGeneratorStub.reset();

    job = new ElviaJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      descriptor,
      defaultProcessor,
      defaultRecordConsumoMapper,
      consumoGenericoProcessor,
      recordConsumoGenericoMapper,
      stanziamentiToNavJmsPublisher,
      notConfirmedAllocationService
    );
    job.THREAD_POOL_SIZE = 1;
    //job.DEVCONF_SKIP_ESPERSISTING = true;
    job.init();
  }

  @AfterEach
  public void tearDown(){
    Mockito.validateMockitoUsage();
  }

  @Test
  public void testKOStanziamentiCheckConsistency() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI_slim.N00002");

    ValidationOutcome vo = new ValidationOutcome().message(new Message("001").text("this is test error!"));
    vo.setValidationOk(false);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    assertThat(job.getSource()).isEqualTo("ELVIA");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isEmpty();
  }

  @Test
  @Transactional
  public void test_dettagli_and_stanzimenti_PEDAGGI_count() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();
  }

  @Test
  @Transactional
  public void test_NOT_SEND_warning_notification() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI_slim.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    ArgumentCaptor< String> code = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Map<String, Object>> parameters = ArgumentCaptor.forClass(Map.class);

    verify(notificationService, times(0))
      .notify(code.capture(), parameters.capture());

    verifyNoMoreInteractions(notificationService);
  }

  @Test
  @Transactional
  public void test_SEND_warning_notification() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

//    ArgumentCaptor< String> code = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<EnrichedNotification> notificationArgumentCaptor = ArgumentCaptor.forClass(EnrichedNotification.class);

    verify(notificationService, atLeastOnce())
      .notify(notificationArgumentCaptor.capture());

    assertThat(notificationArgumentCaptor.getAllValues().stream().map(enrichedNotification -> enrichedNotification.getCode()))
      .containsExactlyInAnyOrder(
        "CPV-w005",
        "CPV-w001",
        "CPV-w002",
        "CPV-w001",
        "CPV-w001",
        "CPV-w005",
        "CPV-w001",
        "CPV-w001",
        "CPV-404",
        "CPV-404"
      );

//    assertThat(notificationArgumentCaptor.getAllValues())
//      .hasSize(11)
//      .extracting(notificationArg -> tuple(notificationArg.get("job_name"), notificationArg.get("original_message")))
//      .containsExactlyInAnyOrder(
//        tuple(Format.ELVIA.name(), "Service [PEDAGGI_ITALIA] with pan [null] for device [486908534/VIACARD] in date [2018-03-02 07:46:28(offset: 24h)] was not active"),
//        tuple(Format.ELVIA.name(), "Device 00460719958 with contract 173302108 is different from record contract number 120609680"),
//        tuple(Format.ELVIA.name(), "Customer 0067620 is not active in date 2018-03-04 17:58:24(offset: 24h)"),
//        tuple(Format.ELVIA.name(), "Customer 0067620 is not active in date 2018-03-05 07:52:53(offset: 24h)"),
//        tuple(Format.ELVIA.name(), "Service [PEDAGGI_ITALIA] with pan [00646210021] for device [00049000000646210021/TELEPASS_EUROPEO] in date [2018-03-14 17:32:21(offset: 24h)] was not active"),
//        tuple(Format.ELVIA.name(), "Contract 177454837 is not active in date 2018-01-31 01:00:00(offset: 24h)"),
//        tuple(Format.ELVIA.name(), "Customer 0064829 is not active in date 2018-01-31 01:00:00(offset: 24h)"),
//        tuple(Format.ELVIA.name(), "Customer 0071505 is not active in date 2018-01-31 01:00:00(offset: 24h)"),
//        tuple(Format.ELVIA.name(), "Customer 0087332 is not active in date 2018-01-31 01:00:00(offset: 24h)"),
//        tuple(Format.ELVIA.name(), "Customer 0067620 is not active in date 2018-02-04 00:00:01(offset: 24h)"),
//        tuple(Format.ELVIA.name(), "Customer 0067620 is not active in date 2018-02-04 00:00:01(offset: 24h)")
//      )
//      ;
  }

  @Test
  @Transactional
  public void test_dettagli_and_stanzimenti_TRAGHETTI_positive_total() {
	  ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger)LoggerFactory.getLogger("it.fai.ms");
	  root.setLevel(Level.TRACE);	 
	  
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI_test_traghetto_positivo.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .hasSize(2); //provvisorio e definitivo

    for (Stanziamento stanziamento : stanziamentoList) {
      assertThat(stanziamento.getPrezzo())
        .isGreaterThan(BigDecimal.ZERO);

      assertThat(stanziamento.getCosto())
        .isGreaterThan(BigDecimal.ZERO);

      assertThat(stanziamento.getPrezzo())
        .isEqualByComparingTo(stanziamento.getCosto());

      assertThat(stanziamento.getPrezzo())
        .isEqualByComparingTo(BigDecimal.TEN);
    }

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .hasSize(3);

    BigDecimal totalOfDettagliStanziamento = BigDecimal.ZERO;
    String sumOfSigns = "";
    List<DettaglioStanziamentoPedaggioEntity> dettaglioStanziamentiPedaggio = 
        dettaglioStanziamenti.stream().map(c -> (DettaglioStanziamentoPedaggioEntity) c)
                                      .sorted(Comparator.comparing(DettaglioStanziamentoPedaggioEntity::getSourceRowNumber))
                                      .collect(Collectors.toList());
    
    for (DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity : dettaglioStanziamentiPedaggio) {
      totalOfDettagliStanziamento = totalOfDettagliStanziamento.add(dettaglioStanziamentoPedaggioEntity.getAmountNoVat());
      sumOfSigns += dettaglioStanziamentoPedaggioEntity.getTransactionSign();
    }

    assertThat(BigDecimal.TEN)
      .isEqualByComparingTo(totalOfDettagliStanziamento);

    assertThat(sumOfSigns)
      .isEqualTo("+-+");
    
	  root.setLevel(Level.INFO);	 
  }

  @Test
  @Transactional
  public void test_dettagli_and_stanzimenti_TRAGHETTI_negative_total() {
	  
	  ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger)LoggerFactory.getLogger("it.fai.ms");
	  root.setLevel(Level.TRACE);	  
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI_test_traghetto_negativo.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .hasSize(2); //provvisorio e definitivo

    for (Stanziamento stanziamento : stanziamentoList) {
    	
      if(stanziamento.getPrezzo().compareTo(BigDecimal.ZERO) == 0 || stanziamento.getCosto().compareTo(BigDecimal.ZERO) == 0) {
    	  log.error(stanziamento.toString());
    	  for (DettaglioStanziamento dett : stanziamento.getDettaglioStanziamenti()) {
    		  log.error(dett.toString());
		}
      }
      assertThat(stanziamento.getPrezzo())
        .isLessThan(BigDecimal.ZERO);

      assertThat(stanziamento.getCosto())
        .isLessThan(BigDecimal.ZERO);

      assertThat(stanziamento.getPrezzo())
        .isEqualByComparingTo(stanziamento.getCosto());

      assertThat(stanziamento.getPrezzo().negate())
        .isEqualByComparingTo(BigDecimal.TEN);
    }

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti =  dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti).hasSize(3);

    BigDecimal totalOfDettagliStanziamento = BigDecimal.ZERO;
    String sumOfSigns = "";
    List<DettaglioStanziamentoPedaggioEntity> dettaglioStanziamentiPedaggio = 
        dettaglioStanziamenti.stream().map(c -> (DettaglioStanziamentoPedaggioEntity) c)
                                      .sorted(Comparator.comparing(DettaglioStanziamentoPedaggioEntity::getSourceRowNumber)).collect(Collectors.toList());
    for (DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity : dettaglioStanziamentiPedaggio) {
      totalOfDettagliStanziamento = totalOfDettagliStanziamento.add(dettaglioStanziamentoPedaggioEntity.getAmountNoVat());
      System.out.println(dettaglioStanziamentoPedaggioEntity);
      sumOfSigns += dettaglioStanziamentoPedaggioEntity.getTransactionSign();
    }

    assertThat(BigDecimal.TEN.negate())
      .isEqualByComparingTo(totalOfDettagliStanziamento);

    assertThat(sumOfSigns)
      .isEqualTo("-+-");
    
    root.setLevel(Level.INFO);
  }

  @Test
  @Transactional
  public void test_dettagli_and_stanzimenti_GENERICI_count() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.CANONI.N00004");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoGenerico();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    ArgumentCaptor< String> code = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Map<String, Object>> parameters = ArgumentCaptor.forClass(Map.class);
    verify(notificationService, times(0))
      .notify(code.capture(), parameters.capture());

    verifyNoMoreInteractions(notificationService);
  }

  @Test
  @Transactional
  public void test_dettagli_and_stanzimenti_PLATE_DEVICE_count() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.UAREAC.N00005");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .hasSize(2);

    //DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamenti.get(0);
    boolean tipoTI = false;
    boolean tipoNULL = false;
    
    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;
      
      if(TipoDispositivoEnum.TELEPASS_ITALIANO == dettaglioStanziamentoPedaggio.getDeviceType()) {
        tipoTI = true;
        assertThat(dettaglioStanziamentoPedaggio.getDeviceType())
          .isEqualTo(TipoDispositivoEnum.TELEPASS_ITALIANO);
    
        assertThat(dettaglioStanziamentoPedaggio.getDeviceObu())
          .isEqualTo("CT184EC");
      }

      if(null == dettaglioStanziamentoPedaggio.getDeviceType()) {
        tipoNULL = true;
        assertThat(dettaglioStanziamentoPedaggio.getDeviceType())
          .isNull();
    
        assertThat(dettaglioStanziamentoPedaggio.getDeviceObu())
          .isNull();
      }
    }
    
    assertTrue(tipoTI);
    assertTrue(tipoNULL);

    ArgumentCaptor< String> code = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Map<String, Object>> parameters = ArgumentCaptor.forClass(Map.class);
    verify(notificationService, times(0))
      .notify(code.capture(), parameters.capture());

    verifyNoMoreInteractions(notificationService);

  }

  @Test
  @Transactional
  public void test_injestion_and_acquisition_date_values() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI_slim.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
        .isNotNull();
      assertThat(dataAcquisizioneFlusso)
        .isNotNull();
      assertThat(ingestionTime)
        .isNotEqualTo(dataAcquisizioneFlusso);

      //from file hader 2018-03-23
      LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(2018);
      assertThat(date.getMonth()).isEqualTo(MARCH);
      assertThat(date.getDayOfMonth()).isEqualTo(23);

      //can be broken using tests on midnight..
      date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());
    }
  }

  @Test
  @Transactional
  public void test_toll_descriptions_values() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.FAI_slim.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      final String entryDescription = dettaglioStanziamentoPedaggio.getEntryGlobalGateIdentifierDescription();
      assertThat(entryDescription)
        .isNotNull();
      final String exitDescription = dettaglioStanziamentoPedaggio.getExitGlobalGateIdentifierDescription();
      assertThat(exitDescription)
        .isNotNull();
      final String tratta = dettaglioStanziamentoPedaggio.getTratta();
      assertThat(tratta)
        .isNotNull();
    }
  }

  @Test
  @Transactional
  public void test_process_sample_file_producing_stanziamenti_with_ESE() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elvia/DA06A284.ELVIA.CANONI.N00004");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList.size())
      .isEqualTo(4);
    
    boolean recordITAPTRC = false;
    boolean recordITAPTR = false;
    boolean recordKMBASE = false;
    boolean recordKMCON = false;
    DettaglioStanziamentoGenerico dettaglioStanziamentoGenerico = null;
    
    for (Stanziamento stanziamento : stanziamentoList) {
      
      //RECORD PREMIUM TRUCK
      if(stanziamento.getArticleCode().equals("ITAPTRC")) {
        recordITAPTRC = true;
        genericAssertion(stanziamento);
    
        assertThat(stanziamento.getArticleCode())
          .isEqualTo("ITAPTRC");
        assertThat(stanziamento.getPrezzo())
          .isEqualByComparingTo(new BigDecimal("21.00000"));
        assertThat(stanziamento.getCosto())
          .isEqualByComparingTo(new BigDecimal("21.00000"));
        assertThat(stanziamento.getDettaglioStanziamenti().size())
          .isEqualTo(1);
        assertThat(stanziamento.getGeneraStanziamento()).isEqualTo(COSTO_RICAVO);
    
        //validazioni generiche
        dettaglioStanziamentoGenerico = (DettaglioStanziamentoGenerico) stanziamento.getDettaglioStanziamenti().iterator().next();
        genericAssertion(dettaglioStanziamentoGenerico);
    
    
        //validazioni specifiche
        assertThat(dettaglioStanziamentoGenerico.getRecordCode())
          .isEqualTo("TOT");
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getDetailCode())
          .isEqualTo("60");
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountIncludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("25.62"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountExcludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("21"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getVatRateBigDecimal())
          .isEqualByComparingTo(new BigDecimal("22.00"));
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getSign())
          .isEqualTo("+");
        assertThat(dettaglioStanziamentoGenerico.getArticlesGroup())
          .isEqualTo("AUT_IT");
      }
      
      //RECORD PREMIUM
      if(stanziamento.getArticleCode().equals("ITAPTR")) {
        recordITAPTR = true;
        genericAssertion(stanziamento);
    
        assertThat(stanziamento.getArticleCode())
          .isEqualTo("ITAPTR");
        assertThat(stanziamento.getPrezzo())
          .isEqualByComparingTo(new BigDecimal("6.84000"));
        assertThat(stanziamento.getCosto())
          .isEqualByComparingTo(new BigDecimal("6.84000"));
        assertThat(stanziamento.getGeneraStanziamento()).isEqualTo(COSTO_RICAVO);
    
        assertThat(stanziamento.getDettaglioStanziamenti().size())
          .isEqualTo(1);
    
        //validazioni generiche
        dettaglioStanziamentoGenerico = (DettaglioStanziamentoGenerico) stanziamento.getDettaglioStanziamenti().iterator().next();
        genericAssertion(dettaglioStanziamentoGenerico);
    
        assertThat(dettaglioStanziamentoGenerico.getRecordCode())
          .isEqualTo("TOT");
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getDetailCode())
          .isEqualTo("58");
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountIncludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("6.84"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountExcludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("6.84"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getVatRateBigDecimal())
          .isEqualByComparingTo(new BigDecimal("0.00"));
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getSign())
          .isEqualTo("+");
        assertThat(dettaglioStanziamentoGenerico.getArticlesGroup())
          .isEqualTo("AUT_IT");
      }
      
      //RECORD KMASTER
      if(stanziamento.getArticleCode().equals("KMBASE")) {
        recordKMBASE = true;
        genericAssertion(stanziamento);
    
        assertThat(stanziamento.getArticleCode())
          .isEqualTo("KMBASE");
        assertThat(stanziamento.getPrezzo())
          .isEqualByComparingTo(new BigDecimal("24"));
        assertThat(stanziamento.getCosto())
          .isEqualByComparingTo(new BigDecimal("24"));
    
        assertThat(stanziamento.getGeneraStanziamento())
          .isEqualTo(COSTO);
    
        assertThat(stanziamento.getDettaglioStanziamenti().size())
          .isEqualTo(1);
    
        //validazioni generiche
        dettaglioStanziamentoGenerico = (DettaglioStanziamentoGenerico) stanziamento.getDettaglioStanziamenti().iterator().next();
        genericAssertion(dettaglioStanziamentoGenerico);
    
        assertThat(dettaglioStanziamentoGenerico.getRecordCode())
          .isEqualTo("TOT");
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getDetailCode())
          .isEqualTo("0A");
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountIncludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("29.28"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountExcludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("24"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getVatRateBigDecimal())
          .isEqualByComparingTo(new BigDecimal("22.00"));
        assertThat(dettaglioStanziamentoGenerico.getArticlesGroup())
          .isEqualTo("KMASTER");
      }
      
      //RECORD KMASTER CONNECTED
      if(stanziamento.getArticleCode().equals("KMCON")) {
        recordKMCON = true;
        genericAssertion(stanziamento);
  
        assertThat(stanziamento.getArticleCode())
          .isEqualTo("KMCON");
        assertThat(stanziamento.getPrezzo())
          .isEqualByComparingTo(new BigDecimal("-8.19672"));
        assertThat(stanziamento.getCosto())
          .isEqualByComparingTo(new BigDecimal("-8.19672"));
        assertThat(stanziamento.getGeneraStanziamento())
          .isEqualTo(COSTO);
    
        assertThat(stanziamento.getDettaglioStanziamenti().size())
          .isEqualTo(1);
    
        //validazioni generiche
        dettaglioStanziamentoGenerico = (DettaglioStanziamentoGenerico) stanziamento.getDettaglioStanziamenti().iterator().next();
        genericAssertion(dettaglioStanziamentoGenerico);
    
        assertThat(dettaglioStanziamentoGenerico.getRecordCode())
          .isEqualTo("TOT");
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getDetailCode())
          .isEqualTo("0B");
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountIncludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("-10.00"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getAmountExcludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isEqualByComparingTo(new BigDecimal("-8.19672"));
        assertThat(dettaglioStanziamentoGenerico.getAmount().getVatRateBigDecimal())
          .isEqualByComparingTo(new BigDecimal("22.00"));
        assertThat(dettaglioStanziamentoGenerico.getTransaction().getSign())
          .isEqualTo("-");
        assertThat(dettaglioStanziamentoGenerico.getArticlesGroup())
          .isEqualTo("KMASTER");
    
        List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoGenerico();
        assertThat(dettaglioStanziamenti)
          .isNotEmpty();
    
        for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
          DettaglioStanziamentoGenericoEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoGenericoEntity) dettaglioStanziamentoEntity;
    
          //Testing dates
          final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
          final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();
    
          assertThat(ingestionTime)
            .isNotNull();
          assertThat(dataAcquisizioneFlusso)
            .isNotNull();
          assertThat(ingestionTime)
            .isNotEqualTo(dataAcquisizioneFlusso);
    
          //from file hader 2018-03-23
          LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
          assertThat(date.getYear()).isEqualTo(2018);
          assertThat(date.getMonth()).isEqualTo(MARCH);
          assertThat(date.getDayOfMonth()).isEqualTo(23);
    
          //can be broken using tests on midnight..
          date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
          final LocalDate now = LocalDate.now();
          assertThat(date.getYear()).isEqualTo(now.getYear());
          assertThat(date.getMonth()).isEqualTo(now.getMonth());
          assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());
    
        }
      
        assertThat(dettaglioStanziamenti.size())
          .isEqualTo(4);
      }
    }
    
    assertThat(recordITAPTR);
    assertThat(recordITAPTRC);
    assertThat(recordKMBASE);
    assertThat(recordKMCON);
  }

  public void genericAssertion(Stanziamento stanziamento) {
    assertThat(stanziamento.getCode()).isNotNull();

    //se dettaglio ha recuperato il veicolo
    assertThat(stanziamento.getVehicleEuroClass()).isNull();
    assertThat(stanziamento.getVehicleLicensePlate()).isNull();

    assertThat(stanziamento.getCodiceClienteFatturazione()).isNull();

    //vale solo se esiste uno stanziamento provvisorio
    assertThat(stanziamento.getCodiceStanziamentoProvvisorio()).isNull();
    assertThat(stanziamento.isConguaglio()).isFalse();

    //con il cambio non so il valore preciso
    assertThat(stanziamento.getAmountCurrencyCode()).isEqualTo("EUR");
    assertThat(stanziamento.getDataErogazioneServizio().getYear()).isEqualTo(2018);
    assertThat(stanziamento.getDataErogazioneServizio().getMonth()).isEqualTo(FEBRUARY);
    assertThat(stanziamento.getDataErogazioneServizio().getDayOfMonth()).isEqualTo(28);

    assertThat(stanziamento.getDataFattura()).isNull();

    assertThat(stanziamento.getDocumentoDaFornitore()).isNull();

    assertThat(stanziamento.getInvoiceType()).isEqualTo(D);
    assertThat(stanziamento.getDettaglioStanziamenti()).isNotEmpty();
    assertThat(stanziamento.getTotalAllocationDetails()).isEqualTo(1);


    assertThat(stanziamento.getNumeroCliente())
      .isEqualTo("0000015");

    assertThat(stanziamento.getNumeroFattura()).isNull();

    assertThat(stanziamento.getNumeroFornitore())
      .isEqualTo("F006563");

    assertThat(stanziamento.getQuantita()).isEqualByComparingTo(BigDecimal.ONE);
    assertThat(stanziamento.getTipoFlusso()).isEqualTo(TypeFlow.MYFAI);
    assertThat(stanziamento.getYear()).isEqualTo(2018);
  }

  public void genericAssertion(DettaglioStanziamentoGenerico dettaglioStanziamentoGenerico) {
    assertThat(dettaglioStanziamentoGenerico.getSource().getType())
      .isEqualTo("elvia");
    assertThat(dettaglioStanziamentoGenerico.getInvoiceType())
      .isEqualTo(D);
    assertThat(dettaglioStanziamentoGenerico.getSource().getFileName())
      .isEqualTo("DA06A284.ELVIA.CANONI.N00004");
    assertThat(dettaglioStanziamentoGenerico.getCustomer().getId())
      .isEqualTo("0000015");
    assertThat(dettaglioStanziamentoGenerico.getSupplier().getCode())
      .isEqualTo("F006563");
    assertThat(dettaglioStanziamentoGenerico.getPartnerCode())
      .isNull();

    final LocalDate startDate = LocalDate.ofInstant(dettaglioStanziamentoGenerico.getStartDate(), ZoneId.systemDefault());
    assertThat(startDate.getYear()).isEqualTo(2018);
    assertThat(startDate.getMonth()).isEqualTo(FEBRUARY);
    assertThat(startDate.getDayOfMonth()).isEqualTo(1);

    final LocalDate date = LocalDate.ofInstant(dettaglioStanziamentoGenerico.getDate(), ZoneId.systemDefault());
    assertThat(date.getYear()).isEqualTo(2018);
    assertThat(date.getMonth()).isEqualTo(FEBRUARY);
    assertThat(date.getDayOfMonth()).isEqualTo(28);

    assertThat(dettaglioStanziamentoGenerico.getQuantity())
      .isEqualByComparingTo(BigDecimal.ONE);

    assertThat(dettaglioStanziamentoGenerico.getAmount().getCurrency().getCurrencyCode())
      .isEqualTo("EUR");


    final LocalDate acquisitionDate = LocalDate.ofInstant(dettaglioStanziamentoGenerico.getSource().getAcquisitionDate(), ZoneId.systemDefault());
    assertThat(acquisitionDate.getYear()).isEqualTo(2018);
    assertThat(acquisitionDate.getMonth()).isEqualTo(MARCH);
    assertThat(acquisitionDate.getDayOfMonth()).isEqualTo(23);

    assertThat(dettaglioStanziamentoGenerico.getDeviceCode())
      .isNull();

    assertThat(dettaglioStanziamentoGenerico.getRoute())
      .isNull();

    assertThat(dettaglioStanziamentoGenerico.getVehicle())
      .isNull();

    assertThat(dettaglioStanziamentoGenerico.getAdditionalInformation())
      .isNull();

    assertThat(dettaglioStanziamentoGenerico.getSupplier().getDocument())
      .isNull();
  }

  public ServicePartner newServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.ELVIA);
    return serviceProvider;
  }
}


