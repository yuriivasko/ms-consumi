package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoCarburanteTestBuilder;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.carburanti.StanziamentoProcessorCarburanteImpl;

@Tag("unit")
class DettaglioStanziamentoCarburantiProcessorImplTest {

  private DettaglioStanziamantiRepository                                  mockDettagliStanziamantiRepository = mock(DettaglioStanziamantiRepository.class);
  private DettaglioStanziamentoCarburantiProcessor                         dettaglioStanziamentoProcessor;
  private StanziamentoProcessorCarburanteImpl                              mockStanziamentoProcessor          = mock(StanziamentoProcessorCarburanteImpl.class);
  private final DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl mockDaTabellaPrezziProcessor       = mock(DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl.class);
  private final DettaglioStanziamentoCaburantiDaConsumoProcessorImpl       mockDaCosumoProcessor              = mock(DettaglioStanziamentoCaburantiDaConsumoProcessorImpl.class);

  @BeforeEach
  void setUp() throws Exception {

    dettaglioStanziamentoProcessor = new DettaglioStanziamentoCarburantiProcessorImpl(mockStanziamentoProcessor,
                                                                                      mockDettagliStanziamantiRepository,
                                                                                      mockDaTabellaPrezziProcessor, mockDaCosumoProcessor);

  }

  @Test
  void when_dettaglioStanziamento_carburante_daCosumo_senzaConversioneInEuro() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaConsumo()
                                                                          .withCurrencyEUR()
                                                                          .build();

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when
    when(mockDaCosumoProcessor.process(dettaglioStanziamento,
                                       stanziamentiParams)).thenReturn(java.util.Arrays.asList(dettaglioStanziamento));

    dettaglioStanziamentoProcessor.produce(dettaglioStanziamento, stanziamentiParams);

    // then
    verify(mockDaCosumoProcessor).process(dettaglioStanziamento, stanziamentiParams);
    verifyZeroInteractions(mockDaTabellaPrezziProcessor);
  }

  @Test
  void when_dettaglioStanziamento_carburante_daCosumo_conConversioneInEuro() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaConsumo()
                                                                          .withCurrencyPLN()
                                                                          .build();

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when
    when(mockDaCosumoProcessor.process(any(), any())).thenReturn(Arrays.asList(dettaglioStanziamento));

    dettaglioStanziamentoProcessor.produce(dettaglioStanziamento, stanziamentiParams);

    // then
    verify(mockDaCosumoProcessor).process(dettaglioStanziamento, stanziamentiParams);
    verifyZeroInteractions(mockDaTabellaPrezziProcessor);
  }

  @Test
  void when_dettaglioStanziamento_carburante_daTabellaPrezzi_senzaConversioneInEuro() {

    // given
    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withCurrencyEUR()
                                                                          .build();
    var dettaglioStanziamentoCost = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                              .withCurrencyEUR()
                                                                              .build();
    var dettaglioStanziamentoPrice = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                               .withCurrencyEUR()
                                                                               .build();

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when
    when(mockDaTabellaPrezziProcessor.process(dettaglioStanziamento,
                                              stanziamentiParams)).thenReturn(java.util.Arrays.asList(dettaglioStanziamentoCost,
                                                                                                      dettaglioStanziamentoPrice));

    dettaglioStanziamentoProcessor.produce(dettaglioStanziamento, stanziamentiParams);

    // then
    verify(mockDaTabellaPrezziProcessor).process(dettaglioStanziamento, stanziamentiParams);
    verifyZeroInteractions(mockDaCosumoProcessor);
  }

  @Test
  void when_dettaglioStanziamento_carburante_daTabellaPrezzi_conConversioneInEuro() {

    // given

    var dettaglioStanziamento = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                          .withServicePartnerDaTabellaPrezzi()
                                                                          .withCurrencyPLN()
                                                                          .build();
    var dettaglioStanziamentoCost = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                              .withCurrencyPLN()
                                                                              .build();
    var dettaglioStanziamentoPrice = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                               .withCurrencyPLN()
                                                                               .build();

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCurrencyCode("EUR")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when
    when(mockDaTabellaPrezziProcessor.process(dettaglioStanziamento,
                                              stanziamentiParams)).thenReturn(java.util.Arrays.asList(dettaglioStanziamentoCost,
                                                                                                      dettaglioStanziamentoPrice));
    dettaglioStanziamentoProcessor.produce(dettaglioStanziamento, stanziamentiParams);

    // then
    verify(mockDaTabellaPrezziProcessor).process(dettaglioStanziamento, stanziamentiParams);
    verifyZeroInteractions(mockDaTabellaPrezziProcessor);

  }

  @Test
  public void test(){
    LocalDate.ofInstant(Instant.now(), ZoneOffset.UTC);
  }
}
