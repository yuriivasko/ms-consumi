package it.fai.ms.consumi.domain;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElcitGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;

import java.util.Optional;
import java.util.UUID;

public class GlobalIdentifierTestBuilder {

  public static GlobalIdentifierTestBuilder newInstance() {
    return new GlobalIdentifierTestBuilder();
  }

  private GlobalIdentifier globalIdentifier;

  public GlobalIdentifier build() {
    return globalIdentifier;
  }

  public GlobalIdentifierTestBuilder internal() {
    globalIdentifier = new GlobalIdentifier(GlobalIdentifierType.INTERNAL) {
      @Override
      public GlobalIdentifier newGlobalIdentifier() {
        return newGlobalIdentifier();
      }

      @Override
      public String getId() {
        return "::globalIdentifier::";
      }
    };
    return this;
  }

  public GlobalIdentifierTestBuilder external() {
    globalIdentifier = new GlobalIdentifier(GlobalIdentifierType.EXTERNAL) {
      @Override
      public GlobalIdentifier newGlobalIdentifier() {
        return newGlobalIdentifier();
      }

      @Override
      public String getId() {
        return "::globalIdentifier::";
      }
    };
    return this;
  }

  public static Optional<GlobalIdentifier> newElviaGlobalIdentifier(){
    return Optional.of(new ElviaGlobalIdentifier(UUID.randomUUID().toString()));
  }

  public static Optional<GlobalIdentifier> newElcitGlobalIdentifier(){
    return Optional.of(new ElcitGlobalIdentifier(UUID.randomUUID().toString()));
  }
}
