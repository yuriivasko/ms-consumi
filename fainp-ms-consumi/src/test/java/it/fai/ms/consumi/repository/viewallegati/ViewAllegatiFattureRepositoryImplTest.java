package it.fai.ms.consumi.repository.viewallegati;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

@ExtendWith(SpringExtension.class)
// @ComponentScan(basePackageClasses = ViewAllegatiFattureEntity.class)
@DataJpaTest
// @EntityScan(basePackageClasses = ViewAllegatiFattureEntity.class)
// @EnableJpaRepositories(repositoryBaseClass = ViewAllegatiFattureRepository.class)
@Tag("integration")
@Transactional
class ViewAllegatiFattureRepositoryImplTest {

  @Autowired
  private EntityManager em;

  private ViewAllegatiFattureRepository viewAllegatiFattureRepo;

  @BeforeEach
  void setUp() throws Exception {
    viewAllegatiFattureRepo = new ViewAllegatiFattureRepositoryImpl(em);
  }

  @Test
  void testNotFoundRow() {
    BigDecimal total = viewAllegatiFattureRepo.findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(newRandomStringList());
    assertThat(total).isEqualTo(new BigDecimal(0));
  }
  @Test
  void testNoCodiciStanziamenti() {
    BigDecimal total = viewAllegatiFattureRepo.findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(new ArrayList<String>());
    assertThat(total).isEqualTo(new BigDecimal(0));
  }
  
  @Test
  void testFoundSum() {

    BigDecimal amount = new BigDecimal(10.5d);
    String codiceStanziamento = "codiceStanziamento";
    StanziamentoEntity stanziamentoEntity = new StanziamentoEntity(codiceStanziamento);
    em.persist(stanziamentoEntity);

    DettaglioStanziamentoPedaggioEntity dettaglio1 = new DettaglioStanziamentoPedaggioEntity(GlobalIdentifierTestBuilder.newInstance()
                                                                                                                        .internal()
                                                                                                                        .build()
                                                                                                                        .getId());
    dettaglio1.setSourceType("sourceType");
    dettaglio1.setCustomerId("0046348");
    dettaglio1.setAmountExcludedVat(amount);
    DettaglioStanziamentoPedaggioEntity dettaglio2 = new DettaglioStanziamentoPedaggioEntity(GlobalIdentifierTestBuilder.newInstance()
                                                                                                                        .internal()
                                                                                                                        .build()
                                                                                                                        .getId());
    dettaglio2.setSourceType("sourceType");
    dettaglio2.setCustomerId("0046348");
    dettaglio2.setAmountExcludedVat(amount);

    em.persist(dettaglio1);
    em.persist(dettaglio2);

    dettaglio2.getStanziamenti().add(stanziamentoEntity);
    dettaglio1.getStanziamenti().add(stanziamentoEntity);

    stanziamentoEntity.getDettaglioStanziamenti()
                      .add(dettaglio1);
    stanziamentoEntity.getDettaglioStanziamenti()
                      .add(dettaglio2);
    stanziamentoEntity.setPrezzo(new BigDecimal(10.5d));
    stanziamentoEntity.setQuantity(new BigDecimal(2));
    em.merge(stanziamentoEntity);

    em.flush();

    BigDecimal total = viewAllegatiFattureRepo.findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(Collections.singletonList(codiceStanziamento));
    total.setScale(2, RoundingMode.HALF_UP);
    System.out.println("TOTAL : " + total);
    BigDecimal multiply = amount.multiply(new BigDecimal(2));

    System.out.println("EXPECTED : " + multiply);
    assertThat(total.compareTo(multiply)).isEqualTo(0);
  }

  private List<String> newRandomStringList() {
    List<String> stanziamentiCode = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      stanziamentiCode.add(RandomStringUtils.random(5));
    }

    return stanziamentiCode;
  }

}
