package it.fai.ms.consumi.service.dettaglio_stranziamento;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepositoryImpl;
import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteJmsProducer;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = { "it.fai.ms.consumi.util" })
@DataJpaTest(showSql = false)
@ActiveProfiles(profiles = {"default", "swagger"})
@Tag("unit")
class DettaglioStanziamentoCarburanteServiceImplTest {

  @Autowired
  EntityManager em;

  private DettaglioStanziamentoCarburanteEntity dsc;

  private DettaglioStanziamentoCarburanteServiceImpl dscServiceImpl;

  @MockBean
  private DettaglioCarburanteJmsProducer dettaglioCarburanteJmsProducer;

  @BeforeEach
  void setUp() throws Exception {
    dsc = new DettaglioStanziamentoCarburanteEntity(GlobalIdentifierTestBuilder.newInstance()
                                                                               .internal()
                                                                               .build()
                                                                               .getId());
    dsc.setDataOraUtilizzo(Instant.now());
    dsc.setCustomerId("0046348");

    DettaglioStanziamentiRepositoryImpl _dettaglioStanziamentoRepo = new DettaglioStanziamentiRepositoryImpl(em, null, null);
    StanziamentoRepositoryImpl _stanziamentoRepository = new StanziamentoRepositoryImpl(em, null, null, new StanziamentoCodeGenerator());
    dscServiceImpl = new DettaglioStanziamentoCarburanteServiceImpl(_dettaglioStanziamentoRepo, _stanziamentoRepository);
  }

  @AfterEach
  void destroy() {
    DettaglioStanziamentoCarburanteEntity dscLoaded = em.find(DettaglioStanziamentoCarburanteEntity.class, dsc.getId());
    em.remove(dscLoaded);
  }

  @Test
  void testPerssitDettaglioStanziamento() {
    em.persist(dsc);
    assertThat(dsc.getId()).isNotNull();
  }

  @Test
  void testCloneDettaglioStanziamento() {
    em.persist(dsc);

    DettaglioStanziamentoCarburanteEntity clonedEntity = dscServiceImpl.cloneDettaglioStanziamento(dsc);
    assertThat(clonedEntity.getId()).isNull();
    assertThat(clonedEntity.getId()).isNotEqualTo(dsc.getId());
    em.remove(clonedEntity);
  }

  @Test
  void testCloneDettaglioStanziamentoIsEqual() {
    em.persist(dsc);

    DettaglioStanziamentoCarburanteEntity clonedEntity = dscServiceImpl.cloneDettaglioStanziamento(dsc);
    String strClonedEntity = clonedEntity.toString();
    System.out.println(strClonedEntity.indexOf(","));
    strClonedEntity = strClonedEntity.substring(strClonedEntity.indexOf(","));
    String strDsc = dsc.toString();
    System.out.println(strDsc.indexOf(","));
    strDsc = strDsc.substring(strDsc.indexOf(","));

    assertThat(strDsc).isEqualTo(strClonedEntity);

    em.remove(clonedEntity);
  }

}
