package it.fai.ms.consumi.domain.parametri_stanziamenti;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;

@Tag("unit")
class AllStanziamentiParamsTest {

  private AllStanziamentiParams allStanziamentiParams;

  @Test
  void testGetAllArticles() {

    allStanziamentiParams = new AllStanziamentiParams(List.of(StanziamentiParamsTestBuilder.newInstance()
                                                                                           .withSupplierCode("::supplierCode::")
                                                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                          .withCode("::articleCode::")
                                                                                                                          .build())
                                                                                           .build()));
    assertThat(allStanziamentiParams.getAllArticles()).contains(ArticleTestBuilder.newInstance()
                                                                                  .withCode("::articleCode::")
                                                                                  .build());
  }

  @Test
  void testGetAllProviderCodes() {

    allStanziamentiParams = new AllStanziamentiParams(List.of(StanziamentiParamsTestBuilder.newInstance()
                                                                                           .withSupplierCode("::supplierCode::")
                                                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                          .withCode("::articleCode::")
                                                                                                                          .build())
                                                                                           .build()));

    assertThat(allStanziamentiParams.getAllProviderCodes()).contains("::supplierCode::");
  }

  @Test
  void testGetStanziamentiParams() {

    allStanziamentiParams = new AllStanziamentiParams(List.of(StanziamentiParamsTestBuilder.newInstance()
                                                                                           .withSupplierCode("::supplierCode::")
                                                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                          .withCode("::articleCode::")
                                                                                                                          .build())
                                                                                           .build()));
    assertThat(allStanziamentiParams.getStanziamentiParams()).contains(StanziamentiParamsTestBuilder.newInstance()
                                                                                                    .withSupplierCode("::supplierCode::")
                                                                                                    .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                                   .withCode("::articleCode::")
                                                                                                                                   .build())
                                                                                                    .build());
  }

  @Test
  void testHasTheSameArticles() {

    allStanziamentiParams = new AllStanziamentiParams(List.of(StanziamentiParamsTestBuilder.newInstance()
                                                                                           .withSupplierCode("::supplierCode::")
                                                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                          .withCode("::articleCode::")
                                                                                                                          .build())
                                                                                           .build()));

    assertThat(allStanziamentiParams.isSubsetOfArticles(List.of(ArticleTestBuilder.newInstance()
                                                                                  .withCode("::articleCode::")
                                                                                  .build()))).isTrue();
  }

  @Test
  void testHasTheSameProviderCodes() {

    allStanziamentiParams = new AllStanziamentiParams(List.of(StanziamentiParamsTestBuilder.newInstance()
                                                                                           .withSupplierCode("::supplierCode::")
                                                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                          .withCode("::articleCode::")
                                                                                                                          .build())
                                                                                           .build()));

    assertThat(allStanziamentiParams.isSubsetOfProviderCodes(Set.of("::supplierCode::"))).isTrue();
  }

  @Test
  void testIsEmpty() {

    // given

    allStanziamentiParams = new AllStanziamentiParams();

    // when + then

    assertThat(allStanziamentiParams.isEmpty()).isTrue();
  }

  @Test
  void testIsNotEmpty() {

    // given

    allStanziamentiParams = new AllStanziamentiParams(List.of(StanziamentiParamsTestBuilder.newInstance()
                                                                                           .withSupplierCode("::supplierCode::")
                                                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                          .withCode("::articleCode::")
                                                                                                                          .build())
                                                                                           .build()));

    // when + then

    assertThat(allStanziamentiParams.isEmpty()).isFalse();
  }

}
