package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import static java.time.Month.JULY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.mockito.ArgumentCaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitJob;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Tag("integration")

public class ElcitJobWithESIntTest
  extends AbstractTelepassIntTest {

  private transient final Logger log = LoggerFactory.getLogger(getClass());

  protected ElcitJob job;

  //not work cause AbstractIntegrationTestSpringContext cannot include all services in it.fai.ms.consumi.scheduler.jobs.flussi.consumi
  protected ElcitDescriptor elcitDescriptor = new ElcitDescriptor();

  @Inject
  protected ConsumoProcessor<Elcit> elcitProcessor;

  @Inject
  protected RecordConsumoMapper<ElcitRecord, Elcit> consumoMapper;

  @Autowired
  private ElasticSearchFeignClient esClient;

  @BeforeEach
  public void setUp() throws Exception {
    super.setUpMockContext("Elcit");

    job = new ElcitJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      elcitDescriptor,
      elcitProcessor,
      consumoMapper,
      stanziamentiToNavJmsPublisher,
      "FILENAME_ASC"
    );
  }

  @Test
  public void testKOStanziamentiCheckConsistency(TestInfo info) {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/DA06A284.ELCIT.FAI_slim.N00002");

    ValidationOutcome vo = new ValidationOutcome().message(new Message("001").text("this is test error!"));
    vo.setValidationOk(false);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    assertThat(job.getSource()).isEqualTo("ELCIT");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isEmpty();
  }

  @Test
  @Transactional
  public void test_dettagli_and_stanzimenti_count() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/int-test-elcit-only/DA06A284.ELCIT.FAI.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();
  }

  @Test
  @Transactional
  public void test_dettagli_and_stanzimenti_PLATE_DEVICE_count() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/int-test-elcit-only/DA06A284.ELCIT.FAI_AREAC_dispositivo_tipo_targa.N0003");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();

    assertThat(stanziamentoList)
      .hasSize(3)
      .extracting(stanziamento -> tuple(stanziamento.getVehicleLicensePlate()))
      .containsExactlyInAnyOrder(
        tuple("CR393TW"),
        tuple("FG015KB"),
        tuple("")
      );

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    assertThat(dettaglioStanziamenti)
      .hasSize(3)
      .extracting(dettaglioStanziamentoEntity -> {
        DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;
        return tuple(dettaglioStanziamentoPedaggio.getInvoiceType().toString(), dettaglioStanziamentoPedaggio.getDeviceType().name(), dettaglioStanziamentoPedaggio.getDeviceObu());
      })
      .containsExactlyInAnyOrder(
        tuple("P", TipoDispositivoEnum.TELEPASS_ITALIANO.name(), "CR393TW"),
        tuple("P", TipoDispositivoEnum.TELEPASS_ITALIANO.name(), "FG015KB"),
        tuple("P", TipoDispositivoEnum.TELEPASS_ITALIANO.name(), "00150824662")
      );
  }


  @Test
  @Transactional
  public void test_ingestion_and_acquisition_date_values() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/DA06A284.ELCIT.FAI_slim.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
        .isNotNull();
      assertThat(dataAcquisizioneFlusso)
        .isNotNull();
      assertThat(ingestionTime)
        .isNotEqualTo(dataAcquisizioneFlusso);

      //from file hader 20180706
      LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(2018);
      assertThat(date.getMonth()).isEqualTo(JULY);
      assertThat(date.getDayOfMonth()).isEqualTo(06);

      //can be broken using tests on midnight..
      date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());

    }
  }

  @Test
  @Transactional
  public void test_toll_descriptions_values() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/DA06A284.ELCIT.FAI_slim.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      final String entryDescription = dettaglioStanziamentoPedaggio.getEntryGlobalGateIdentifierDescription();
      assertThat(entryDescription)
        .isNotNull();

      final String exitDescription = dettaglioStanziamentoPedaggio.getExitGlobalGateIdentifierDescription();
      assertThat(exitDescription)
        .isNotNull();

      final String tratta = dettaglioStanziamentoPedaggio.getTratta();
      assertThat(tratta)
        .isNotNull();
    }
  }

  @Test
  @Transactional
  public void test_retrieve_contract() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/DA06A284.ELCIT.FAI_missing_contract.N00002");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      final String contractCode = dettaglioStanziamentoPedaggio.getContractCode();
      assertThat(contractCode)
        .isNotNull();
    }
  }

  @Test
  @Transactional
  public void test_ese_records() {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/DA06A284.ELCIT.FAI.N00002_withese");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    String[][] expectedAmounts = {
      {"3.50000", "22.00", "2.86885"},
      {"3.50000", "22.00", "2.86885"},
      {"0.32001", "22.00", "0.26230"},
      {"0.32001", "22.00", "0.26230"},
      {"0.32001", "22.00", "0.26230"},
      {"0.59000", "22.00", "0.48361"},
      {"2.17000", "22.00", "1.77869"},
      {"1.04000", "22.00", "0.85246"},
      {"1.04000", "22.00", "0.85246"},
      {"1.22000", "22.00", "1.00000"},
      {"6.22000", "22.00", "5.09836"},
      {"0.50000", "0.00", "0.50000"},
      {"0.32001", "22.00", "0.26230"},
      {"3.50000", "22.00", "2.86885"},
      {"1.05000", "5.00", "1.00000"},
      {"2.44000", "22.00", "2.00000"},
    };

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    var i = 0;
    for (Stanziamento stanziamento : stanziamentoList) {
      log.warn("price: [{}], cost: [{}]", stanziamento.getPrezzo(), stanziamento.getCosto());
      assertThat(stanziamento.getPrezzo())
        .isEqualByComparingTo(stanziamento.getCosto());
    }

    assertThat(stanziamentoList)
      .extracting(stanziamento -> tuple(stanziamento.getCosto().toString()))
      .containsExactlyInAnyOrder(
        tuple("2.86885"),
        tuple("2.86885"),
        tuple("0.26230"),
        tuple("0.26230"),
        tuple("0.26230"),
        tuple("0.48361"),
        tuple("1.77869"),
        tuple("0.85246"),
        tuple("0.85246"),
        tuple("1.00000"),
        tuple("5.09836"),
        tuple("0.50000"),
        tuple("0.26230"),
        tuple("2.86885"),
        tuple("1.00000"),
        tuple("2.00000")
      );

    i = 0;
    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

//    for (DettaglioStanziamentoEntity dettaglioStanziamento : dettaglioStanziamenti) {
//      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamento;
//      log.warn("totale, iva, imponibile: [\"{}\", \"{}\", \"{}\"]", dettaglioStanziamentoPedaggio.getAmountIncludingVat(), dettaglioStanziamentoPedaggio.getAmountVatRate(), dettaglioStanziamentoPedaggio.getAmountNoVat());
//    }

    assertThat(dettaglioStanziamenti)
      .extracting(dettaglioStanziamento -> {
        DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamento;
        return tuple(dettaglioStanziamentoPedaggio.getAmountIncludingVat().toString(), dettaglioStanziamentoPedaggio.getAmountVatRate().toString(), dettaglioStanziamentoPedaggio.getAmountNoVat().toString());
      })
      .containsExactlyInAnyOrder(
        tuple("3.50000", "22.00", "2.86885"),
        tuple("3.50000", "22.00", "2.86885"),
        tuple("0.32001", "22.00", "0.26230"),
        tuple("0.32001", "22.00", "0.26230"),
        tuple("0.32001", "22.00", "0.26230"),
        tuple("0.59000", "22.00", "0.48361"),
        tuple("2.17000", "22.00", "1.77869"),
        tuple("1.04000", "22.00", "0.85246"),
        tuple("1.04000", "22.00", "0.85246"),
        tuple("1.22000", "22.00", "1.00000"),
        tuple("6.22000", "22.00", "5.09836"),
        tuple("0.50000", "0.00", "0.50000"),
        tuple("0.32001", "22.00", "0.26230"),
        tuple("3.50000", "22.00", "2.86885"),
        tuple("1.05000", "5.00", "1.00000"),
        tuple("2.44000", "22.00", "2.00000")
      );
  }

  @Test
  @Transactional
  @Disabled
  public void test_single_record_reprocessing() throws Exception {
    final String fileAbsolutePath = getAbsolutePath("/test-files/elcit/DA06A284.ELCIT.FAI_one_failure.N00002");

    ArgumentCaptor<Map<String, Object>> argumentCaptor = ArgumentCaptor.forClass(Map.class);
    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    verify(notificationService, times(1)).notify(any(), argumentCaptor.capture());
    List<Map<String, Object>> capturedMessagesParameters = argumentCaptor.getAllValues();
    log.error("capturedMessagesParameters:=[{}]", capturedMessagesParameters);
    String uuid = extractRecordUuidFromNotificationMessage(capturedMessagesParameters);
    log.info("uuid:=[{}]", uuid);

    ElasticSearchRecord<? extends CommonRecord> recordWrapper = esClient.getRecord(ElcitRecord.class.getSimpleName(), uuid);
    ElcitRecord foundRecord = (ElcitRecord) recordWrapper.getRawdata();
    ServicePartner servicePartner = foundRecord.getServicePartner();
    Map<String, Stanziamento> stanziamentiDaSpedireANAV = new HashMap<>();

    ArgumentCaptor<Map<String, Object>> argumentCaptor2 = ArgumentCaptor.forClass(Map.class);
    job.processSingleRecord(servicePartner, new Bucket(""), stanziamentiDaSpedireANAV, foundRecord);

    verify(notificationService, times(2)).notify(any(), argumentCaptor2.capture());
    capturedMessagesParameters = argumentCaptor2.getAllValues();
    log.info("capturedMessagesParameters:=[{}]", capturedMessagesParameters);

    assertThat(extractRecordUuidFromNotificationMessage(capturedMessagesParameters))
      .isEqualTo(uuid);
  }

  private String extractRecordUuidFromNotificationMessage(List<Map<String, Object>> capturedMessagesParameters) {
    String originalRecordMessage = (String) capturedMessagesParameters.get(0).get("original_record");
    return originalRecordMessage.split("recordUuid")[1].split("]")[0].replace(":=[", "");
  }

  public ServicePartner newServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.ELCIT);
    return serviceProvider;
  }
}

