package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti;

import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.COSTO;
import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.RICAVO;
import static it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti.TrackycardCarburantiStandardJobPriceTableChangeTest.CostoOrRicavo.IS_COSTO;
import static it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti.TrackycardCarburantiStandardJobPriceTableChangeTest.CostoOrRicavo.IS_RICAVO;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.client.PetrolPumpClient;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.service.petrolpump.consumer.PriceTableClosedIntervalConsumerImpl;
import it.fai.ms.consumi.service.petrolpump.consumer.PriceTableConsumer;
import it.fai.ms.consumi.service.petrolpump.consumer.PriceTableOpenIntervalConsumerImpl;

@Tag("integration")
public class TrackycardCarburantiStandardJobPriceTableChangeTest
  extends AbstractTrackycardCarburantiStandardJobWithEsIntTest {

  @SpyBean
  PriceTableOpenIntervalConsumerImpl openIntervalConsumer;

  @SpyBean
  PriceTableClosedIntervalConsumerImpl closeIntervalConsumer;

  @Inject
  PetrolPumpClient petrolPumpClient;

  public enum CostoOrRicavo{
    IS_COSTO,
    IS_RICAVO,
    IS_COSTORICAVO
  }

  @Test
  @Transactional
  public void test_aggiunta_intervallo_su_petrolpump() throws Exception {
    PriceTableDTO firstPrice = new PriceTableDTO();
    firstPrice.setReferencePrice(BigDecimal.ONE.setScale(5));
    firstPrice.setCostRevenueFlag(CostRevenue.COST);
    firstPrice.setValidFrom(Instant.parse("2018-06-05T16:00:00Z"));
    firstPrice.setValidUntil(null);

    Mockito.reset(petrolPumpClient);
    given(petrolPumpClient.getPriceTableList(any(), any(), any(), any()))
      .willReturn(Arrays.asList(firstPrice));

    PriceTableConsumer priceTableConsumer = new PriceTableConsumer(openIntervalConsumer, closeIntervalConsumer);

    String filename = getAbsolutePath("/test-files/trackycardcarbstd/AT02GFP20140613215101.TXT.unconsumo");
    ServicePartner servicePartner = findServicePartner("AT02");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .extracting(stanziamento -> tuple(stanziamento.getInvoiceType(), stanziamento.getGeneraStanziamento()))
      .hasSize(2)
      .containsExactlyInAnyOrder(
        tuple(InvoiceType.D, RICAVO),
        tuple(InvoiceType.P, COSTO)
      );

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    assertThat(dettaglioStanziamenti)
      .extracting(dettaglioStanziamento -> {
        DettaglioStanziamentoCarburanteEntity dettaglioCarburante = (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamento;
        return tuple(dettaglioCarburante.getTipoConsumo(), costoOrRicavoFrom(dettaglioCarburante), dettaglioCarburante.getCostoUnitarioCalcolatoNoiva(), dettaglioCarburante.getCodiceFornitoreNav());
      })
      .hasSize(2)
      .containsExactlyInAnyOrder(
        tuple(InvoiceType.D, IS_RICAVO, null, "F006554"),
        tuple(InvoiceType.P, IS_COSTO, BigDecimal.ONE.setScale(5), "F006554")
      );

    PriceTableDTO firstPriceClosed = new PriceTableDTO();
    firstPriceClosed.setReferencePrice(BigDecimal.ONE.setScale(5));
    firstPriceClosed.setCostRevenueFlag(CostRevenue.COST);
    firstPriceClosed.setValidFrom(Instant.parse("2018-06-05T16:00:00Z"));
    firstPriceClosed.setValidUntil(Instant.parse("2018-08-05T16:00:00Z"));

    PriceTableDTO secondPrice = new PriceTableDTO();
    secondPrice.setReferencePrice(BigDecimal.TEN.setScale(5));
    secondPrice.setCostRevenueFlag(CostRevenue.COST);
    secondPrice.setValidFrom(Instant.parse("2018-08-05T16:00:00Z"));
    secondPrice.setValidUntil(null);

    Mockito.reset(petrolPumpClient);
    given(petrolPumpClient.getPriceTableList(any(), any(), any(), any()))
      .willReturn(Arrays.asList(secondPrice, secondPrice));

    PriceTableDTO closingInterval = new PriceTableDTO();
    closingInterval.setId(-1l);
    closingInterval.setSupplierCode("F006554");
    closingInterval.setSupplierName("::supplierName::");
    closingInterval.setCodiceArticolo("L00001");
    closingInterval.setCostRevenueFlag(CostRevenue.COST);
    closingInterval.setReferencePrice(BigDecimal.ONE.setScale(5));
    closingInterval.setValidFrom(Instant.parse("2018-06-05T16:00:00Z"));
    closingInterval.setValidUntil(Instant.parse("2018-08-05T16:00:00Z"));

    PriceTableDTO openingInterval = new PriceTableDTO();
    openingInterval.setId(-1l);
    openingInterval.setSupplierCode("F006554");
    openingInterval.setSupplierName("::supplierName::");
    openingInterval.setCodiceArticolo("L00001");
    openingInterval.setCostRevenueFlag(CostRevenue.COST);
    openingInterval.setReferencePrice(BigDecimal.TEN.setScale(5));
    openingInterval.setValidFrom(Instant.parse("2018-08-05T16:00:00Z"));
    openingInterval.setValidUntil(null);

    priceTableConsumer.consumeMessage(openingInterval);
    priceTableConsumer.consumeMessage(closingInterval);

    stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .extracting(stanziamento -> tuple(stanziamento.getInvoiceType(), stanziamento.getGeneraStanziamento()))
      .hasSize(3)
      .containsExactlyInAnyOrder(
        tuple(InvoiceType.D, RICAVO),
        tuple(InvoiceType.D, COSTO),
        tuple(InvoiceType.P, COSTO)
      );

    dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    assertThat(dettaglioStanziamenti)
      .extracting(dettaglioStanziamento -> {
        DettaglioStanziamentoCarburanteEntity dettaglioCarburante = (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamento;
        return tuple(
          dettaglioCarburante.getTipoConsumo(), costoOrRicavoFrom(dettaglioCarburante), dettaglioCarburante.getCostoUnitarioCalcolatoNoiva());
      })
      .hasSize(2)
      .containsExactlyInAnyOrder(
        tuple(InvoiceType.D, IS_RICAVO, null),
        tuple(InvoiceType.D, IS_COSTO, BigDecimal.ONE.setScale(5))
      );
  }

  public CostoOrRicavo costoOrRicavoFrom(DettaglioStanziamentoCarburanteEntity entity){
    boolean isRicavo = entity.getPrezzoUnitarioCalcolatoNoiva() != null;
    boolean isCosto = entity.getCostoUnitarioCalcolatoNoiva() != null;
    if (isCosto && isRicavo){
      return CostoOrRicavo.IS_COSTORICAVO;
    } else if (isCosto){
      return IS_COSTO;
    } else if (isRicavo){
      return IS_RICAVO;
    } else {
      throw new IllegalArgumentException("");
    }
  }


}
