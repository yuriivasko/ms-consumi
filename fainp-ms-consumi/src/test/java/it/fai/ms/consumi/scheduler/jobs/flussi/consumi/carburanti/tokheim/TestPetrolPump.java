package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import feign.FeignException;
import it.fai.ms.consumi.FaiconsumiApp;
import it.fai.ms.consumi.client.PetrolPumpClient;
import it.fai.ms.consumi.config.ApplicationProperties;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FaiconsumiApp.class)
@Tag("development")
public class TestPetrolPump  {
  @Autowired
  PetrolPumpClient petrolpump;
  @Autowired
  ApplicationProperties apProp;
  @Test
  public void testClient() {
    var searchPoint1 = petrolpump.getPriceTableList(apProp.getAuthorizationHeader(), "ewgfw", "werg" , Instant.now());

    var e = assertThrows(FeignException.class, ()->{
    var  searchPoint = petrolpump.searchPoint(apProp.getAuthorizationHeader(), "non Esiste");
    assertThat(searchPoint).isNotNull();
    });
    assertThat(e.status()).isEqualTo(404);

  }
}
