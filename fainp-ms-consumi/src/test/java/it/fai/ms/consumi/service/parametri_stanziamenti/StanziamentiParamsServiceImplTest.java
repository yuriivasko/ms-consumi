package it.fai.ms.consumi.service.parametri_stanziamenti;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsQuery;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;

@Tag("unit")
class StanziamentiParamsServiceImplTest {

  public static final BigDecimal DEFAULT_VAT_RATE = new BigDecimal("22.0");

  private static StanziamentiParamsQuery newStanziamentiParamsQuery1(BigDecimal vatRate) {
    var stanziamentiParamsQuery = new StanziamentiParamsQuery("::source::", "::recordCode::");
    stanziamentiParamsQuery.setFareClass("::fareClass::");
    stanziamentiParamsQuery.setTransactionDetail("::transactionDetail::");
    stanziamentiParamsQuery.setVatRate(vatRate);
    stanziamentiParamsQuery.setCodiceFornitoreLegacy("::supplierCodeLegacy::");
    return stanziamentiParamsQuery;
  }

  private static StanziamentiParamsQuery newStanziamentiParamsQuery2(BigDecimal vatRate) {
    var stanziamentiParamsQuery = new StanziamentiParamsQuery("::source::", "::recordCode::");
    stanziamentiParamsQuery.setFareClass("::fareClass-2::");
    stanziamentiParamsQuery.setTransactionDetail("::transactionDetail-2::");
    stanziamentiParamsQuery.setVatRate(vatRate);
    stanziamentiParamsQuery.setCodiceFornitoreLegacy("::supplierCodeLegacy::");
    return stanziamentiParamsQuery;
  }

  private StanziamentiParamsRepository mockStanziamentiParamsRepository = mock(StanziamentiParamsRepository.class);
  private StanziamentiParamsService    stanziamentiParamsService;

  @BeforeEach
  void setUp() {
    stanziamentiParamsService = new StanziamentiParamsServiceImpl(mockStanziamentiParamsRepository);
  }

  @Test
  void test_findByQuery_when_thereareTwo_stanziamentiParams_by_source_and_recordCode() {

    // given

    var stanziamentiParams1 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-1::")
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .build();
    var stanziamentiParams2 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-2::")
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .build();

    given(mockStanziamentiParamsRepository.findBySourceAndRecordCode("::source::", "::recordCode::"))
      .willReturn(List.of(stanziamentiParams1, stanziamentiParams2));

    // when

    var optionalStanziamentiParam = stanziamentiParamsService.findByQuery(newStanziamentiParamsQuery1(null));

    // then

    then(mockStanziamentiParamsRepository).should().findBySourceAndRecordCode("::source::", "::recordCode::");
    then(mockStanziamentiParamsRepository).shouldHaveNoMoreInteractions();

    assertThat(optionalStanziamentiParam).contains(StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-1::")
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .build());
  }

  @Test
  void test_findByQuery_when_thereareTwo_stanziamentiParams_by_source_and_recordCode_and_fareClass_and_transactionDetail_and_vatRate_and_codFornitoreLegacy() {

    // given

    var stanziamentiParams1 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-1::")
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .withFareClass("::fareClass-1::")
      .withTransactionDetail("::transactionDetail-1::")
      .withArticle(ArticleTestBuilder.newInstance().withCode("::article-1::").withVatRate(1.0f).build())
      .build();
    var stanziamentiParams2 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-2::")
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .withFareClass("::fareClass-2::")
      .withTransactionDetail("::transactionDetail-2::")
      .withArticle(ArticleTestBuilder.newInstance().withCode("::article-2::").withVatRate(2.0f).build())
      .build();

    given(mockStanziamentiParamsRepository.findBySourceAndRecordCode("::source::", "::recordCode::"))
      .willReturn(List.of(stanziamentiParams1, stanziamentiParams2));

    // when

    var optionalStanziamentiParam = stanziamentiParamsService.findByQuery(newStanziamentiParamsQuery2(new BigDecimal("2.0")));

    // then

    then(mockStanziamentiParamsRepository).should().findBySourceAndRecordCode("::source::", "::recordCode::");
    then(mockStanziamentiParamsRepository).shouldHaveNoMoreInteractions();

    assertThat(optionalStanziamentiParam).contains(StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-2::")
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .withFareClass("::fareClass-2::")
      .withTransactionDetail("::transactionDetail-2::")
      .withArticle(ArticleTestBuilder.newInstance().withCode("::article-2::").withVatRate(2.0f).build())
      .build());
  }

  @Test
  void test_findByQuery_when_theresNo_stanziamentiParams_by_source_and_recordCode() {

    // given

    given(mockStanziamentiParamsRepository.findBySourceAndRecordCode(anyString(), anyString())).willReturn(emptyList());

    // when

    var optionalStanziamentiParam = stanziamentiParamsService.findByQuery(newStanziamentiParamsQuery1(DEFAULT_VAT_RATE));

    // then

    then(mockStanziamentiParamsRepository).should().findBySourceAndRecordCode("::source::", "::recordCode::");
    then(mockStanziamentiParamsRepository).shouldHaveNoMoreInteractions();

    assertThat(optionalStanziamentiParam).isEmpty();
  }

  @Test
  void test_getAll() {

    // given

    var allStanziamentiParamsFromRepository = new AllStanziamentiParams();
    allStanziamentiParamsFromRepository.getStanziamentiParams()
      .add(StanziamentiParamsTestBuilder.newInstance()
        .withSupplierCode("::supplierCode::")
        .withArticle(ArticleTestBuilder.newInstance().withCode("::articleCode::").build())
        .build());

    given(mockStanziamentiParamsRepository.getAll()).willReturn(allStanziamentiParamsFromRepository);

    // when

    var allStanziamentiParams = stanziamentiParamsService.getAll();

    // then

    then(mockStanziamentiParamsRepository).should().getAll();
    then(mockStanziamentiParamsRepository).shouldHaveNoMoreInteractions();

    assertThat(allStanziamentiParams).isEqualTo(allStanziamentiParamsFromRepository);
  }

  @Test
  void test_findByQuery_use_transactionDetails_only_if_vatRate_is_null_cod_fornitore_legacy_null_in_paramStanziamenti() {
    // given

    var stanziamentiParams1 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-1::", null)
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .withFareClass("::fareClass-1::")
      .withTransactionDetail("::transactionDetail-1::")
      .withArticle(ArticleTestBuilder.newInstance().withCode("::article-1::").withVatRate(1.0f).build())
      .build();
    var stanziamentiParams2 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-2::", null)
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .withFareClass("::fareClass-2::")
      .withTransactionDetail("::transactionDetail-2::")
      .withArticle(ArticleTestBuilder.newInstance().withCode("::article-2::").withVatRate(12.0f).build())
      .build();

    given(mockStanziamentiParamsRepository.findBySourceAndRecordCode("::source::", "::recordCode::"))
      .willReturn(List.of(stanziamentiParams1, stanziamentiParams2));

    // when

    StanziamentiParamsQuery stanziamentiParamsQuery = newStanziamentiParamsQuery2(null);
    // stanziamentiParamsQuery.setVatRate(new BigDecimal("2.0"));
    var optionalStanziamentiParam = stanziamentiParamsService.findByQuery(stanziamentiParamsQuery);
    assertThat(optionalStanziamentiParam).isPresent().contains(stanziamentiParams2);
    // then

    then(mockStanziamentiParamsRepository).should().findBySourceAndRecordCode("::source::", "::recordCode::");
    then(mockStanziamentiParamsRepository).shouldHaveNoMoreInteractions();
  }

  @Test
  void test_mandatory_iva_not_found() throws Exception {
 // given

    var stanziamentiParams1 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-1::", null)
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .withFareClass("::fareClass-1::")
      .withTransactionDetail("::transactionDetail-1::")
      .withArticle(ArticleTestBuilder.newInstance().withCode("::article-1::").withVatRate(1.0f).build())
      .build();
    var stanziamentiParams2 = StanziamentiParamsTestBuilder.newInstance()
      .withSupplierCode("::supplierCode-2::", null)
      .withSource("::source::")
      .withRecordCode("::recordCode::")
      .withFareClass("::fareClass-2::")
      .withTransactionDetail("::transactionDetail-2::")
      .withArticle(ArticleTestBuilder.newInstance().withCode("::article-2::").withVatRate(12.0f).build())
      .build();

    given(mockStanziamentiParamsRepository.findBySourceAndRecordCode("::source::", "::recordCode::"))
      .willReturn(List.of(stanziamentiParams1, stanziamentiParams2));

    // when

    StanziamentiParamsQuery stanziamentiParamsQuery = newStanziamentiParamsQuery2(new BigDecimal("1.0"));
    // stanziamentiParamsQuery.setVatRate(new BigDecimal("2.0"));
    var optionalStanziamentiParam = stanziamentiParamsService.findByQuery(stanziamentiParamsQuery);
    assertThat(optionalStanziamentiParam).isEmpty();
    // then

    then(mockStanziamentiParamsRepository).should().findBySourceAndRecordCode("::source::", "::recordCode::");
    then(mockStanziamentiParamsRepository).shouldHaveNoMoreInteractions();
  }
}
