package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;

public class DettaglioStanziamentoTestBuilder {

  public static DettaglioStanziamentoTestBuilder newInstance() {
    return new DettaglioStanziamentoTestBuilder();
  }

  private static GlobalIdentifier newInternalGlobalIdentifier() {
    return GlobalIdentifierTestBuilder.newInstance()
                                      .internal()
                                      .build();
  }

  private Amount                amount;
  private DettaglioStanziamento dettaglioStanziamento;
  private InvoiceType           invoiceType;

  private DettaglioStanziamentoTestBuilder() {
  }

  public DettaglioStanziamento build() {

    if (invoiceType == null) {
      throw new IllegalStateException("Missing invoice type");
    }
    if (amount == null) {
      throw new IllegalStateException("Missing amount");
    }

    dettaglioStanziamento = new DettaglioStanziamento(newInternalGlobalIdentifier(), invoiceType) {
    };
    dettaglioStanziamento.setAmount(amount);
    return dettaglioStanziamento;
  }

  public DettaglioStanziamentoTestBuilder withAmount(final Amount _amount) {
    amount = _amount;
    return this;
  }

  public DettaglioStanziamentoTestBuilder withInvoiceTypeD() {
    invoiceType = InvoiceType.D;
    return this;
  }

  public DettaglioStanziamentoTestBuilder withInvoiceTypeP() {
    invoiceType = InvoiceType.P;
    return this;
  }
}
