package it.fai.ms.consumi.service.petrolpump.consumer;

import static java.util.stream.Collectors.toList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoCarburanteTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.StanziamentoTestBuilder;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepositoryImpl;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.service.dettaglio_stranziamento.DettaglioStanziamentoCarburanteService;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;

@Tag("unit")
class PriceTableConsumerTest {

  private NotificationService notificationServiceMock = mock(NotificationService.class);

  private PriceCalculatedByNavService priceCalculatedByNavServiceMock = mock(PriceCalculatedByNavService.class);

  private DettaglioStanziamentoCarburanteService dsCarburanteServiceMock = mock(DettaglioStanziamentoCarburanteService.class);

  private StanziamentoRepositoryImpl stanziamentoRepoMock = mock(StanziamentoRepositoryImpl.class);

  private StanziamentiToNavPublisher stanziamentiNavJmsProducer = mock(StanziamentiToNavPublisher.class);

  private PriceTableClosedIntervalConsumerImpl priceTableClosedIntervalProcessor;

  private PriceTableOpenIntervalConsumerImpl priceTableOpenIntervalProcessor;

  private PriceTableConsumer priceTableProcessor;

  private DettaglioStanziamentoCarburanteEntityMapper dsMapper;

  private StanziamentoEntityMapper sMapper;

  @BeforeEach
  void setUp() throws Exception {
    dsMapper = new DettaglioStanziamentoCarburanteEntityMapper();
    sMapper = new StanziamentoEntityMapper(new DettaglioStanziamentoEntityMapper(dsMapper, null, null, null));
  }

  @Test
  void testConsumeMessageClosedInterval_NotFoundDettagliStanziamento() throws Exception {
    init();
    PriceTableDTO priceTableDto = newPriceTableDtoClosedInterval();
    PriceTableFilterDTO filterDTO = new PriceTableFilterDTO(priceTableDto);
    when(dsCarburanteServiceMock.findToUpdatePriceTableClosed(filterDTO)).thenReturn(new ArrayList<>());

    priceTableProcessor.consumeMessage(priceTableDto);

    verify(dsCarburanteServiceMock).findToUpdatePriceTableClosed(filterDTO);
    verify(dsCarburanteServiceMock, times(0)).cloneStanziamentoCarburanteFromProvvisorio(Mockito.any(StanziamentoEntity.class),
                                                                                         Mockito.eq(InvoiceType.D));
    verify(dsCarburanteServiceMock, times(0)).saveEntity(Mockito.any());
  }

  @Test
  void testConsumeMessageClosedInterval_with_anomaly() throws Exception {
    init();
    PriceTableDTO priceTableDto = newPriceTableDtoClosedInterval();
    PriceTableFilterDTO filterDTO = new PriceTableFilterDTO(priceTableDto);

    when(dsCarburanteServiceMock.findToUpdatePriceTableClosed(filterDTO)).thenReturn(newDettaglioStanziamentoCarburanteWithoutStanziamento().stream()
                                                                                                                                            .map(ds -> dsMapper.toEntity(ds))
                                                                                                                                            .collect(toList()));

    priceTableProcessor.consumeMessage(priceTableDto);

    verify(dsCarburanteServiceMock).findToUpdatePriceTableClosed(filterDTO);
    verify(dsCarburanteServiceMock, times(0)).saveEntity(Mockito.any());
    verify(notificationServiceMock).notify(Mockito.any(), Mockito.any());
  }

  @Test
  void testConsumeMessageClosedInterval_complete() throws Exception {
    init();
    PriceTableDTO priceTableDto = newPriceTableDtoClosedInterval();
    PriceTableFilterDTO filterDTO = new PriceTableFilterDTO(priceTableDto);

    var dettagliFuel = newDettaglioFuelComplete().stream()
                                                 .map(ds -> {
                                                   DettaglioStanziamentoCarburanteEntity dsEntity = dsMapper.toEntity(ds);
                                                   Set<StanziamentoEntity> sEntitySet = sMapper.toEntity(ds.getStanziamenti());
                                                   dsEntity.getStanziamenti()
                                                           .addAll(sEntitySet);
                                                   return dsEntity;
                                                 })
                                                 .collect(toList());
    DettaglioStanziamentoCarburanteEntity dsCloned = newDettaglioFuelComplete().stream()
                                                                               .map(ds -> {
                                                                                 DettaglioStanziamentoCarburanteEntity dsEntity = dsMapper.toEntity(ds);
                                                                                 Set<StanziamentoEntity> sEntitySet = sMapper.toEntity(ds.getStanziamenti());
                                                                                 dsEntity.getStanziamenti()
                                                                                         .addAll(sEntitySet);
                                                                                 return dsEntity;
                                                                               })
                                                                               .findFirst()
                                                                               .get();
    when(dsCarburanteServiceMock.findToUpdatePriceTableClosed(filterDTO)).thenReturn(dettagliFuel);

    var dettaglioStanziamentoCarburante = dettagliFuel.get(0);
    when(dsCarburanteServiceMock.cloneDettaglioStanziamento(dettaglioStanziamentoCarburante)).thenReturn(dsCloned);
    when(dsCarburanteServiceMock.cloneStanziamentoCarburanteFromProvvisorio(dsCloned.getStanziamenti()
                                                                                    .stream()
                                                                                    .findFirst()
                                                                                    .get())).thenReturn(dsCloned.getStanziamenti()
                                                                                                                .stream()
                                                                                                                .findFirst()
                                                                                                                .get());

    priceTableProcessor.consumeMessage(priceTableDto);

    verify(dsCarburanteServiceMock).findToUpdatePriceTableClosed(filterDTO);
    verify(dsCarburanteServiceMock).cloneStanziamentoCarburanteFromProvvisorio(dettaglioStanziamentoCarburante.getStanziamenti()
                                                                                                              .stream()
                                                                                                              .filter(s -> s != null)
                                                                                                              .findFirst()
                                                                                                              .get(),
                                                                               InvoiceType.D);

    dettaglioStanziamentoCarburante.setTipoConsumo(InvoiceType.D);
    verify(dsCarburanteServiceMock).update(dettaglioStanziamentoCarburante);
  }

  @Test
  void testConsumeMessageOpenInterval_001() throws Exception {
    init();
    PriceTableDTO priceTableDto = newPriceTableDtoOpenInterval();
    PriceTableFilterDTO filterDTO = new PriceTableFilterDTO(priceTableDto);
    when(dsCarburanteServiceMock.findToUpdatePriceTableOpen(filterDTO)).thenReturn(new ArrayList<>());

    priceTableProcessor.consumeMessage(priceTableDto);

    verify(dsCarburanteServiceMock).findToUpdatePriceTableOpen(filterDTO);
    verify(dsCarburanteServiceMock, times(0)).saveEntity(Mockito.any());

  }

  @Test
  void testConsumeMessageOpenInterval_with_anomaly() throws Exception {
    init();
    PriceTableDTO priceTableDto = newPriceTableDtoOpenInterval();
    PriceTableFilterDTO filterDTO = new PriceTableFilterDTO(priceTableDto);
    var dsCarbutantes = newDettaglioStanziamentoCarburanteWithoutStanziamento().stream()
                                                                               .map(ds -> dsMapper.toEntity(ds))
                                                                               .collect(toList());

    when(dsCarburanteServiceMock.findToUpdatePriceTableOpen(filterDTO)).thenReturn(dsCarbutantes);

    priceTableProcessor.consumeMessage(priceTableDto);

    verify(dsCarburanteServiceMock).findToUpdatePriceTableOpen(filterDTO);
    verify(notificationServiceMock).notify(Mockito.any(), Mockito.any());
  }

  @Test
  void testConsumeMessageOpenInterval_complete() throws Exception {
    init();
    PriceTableDTO priceTableDto = newPriceTableDtoOpenInterval();
    PriceTableFilterDTO filterDTO = new PriceTableFilterDTO(priceTableDto);

    var dettagliFuel = newDettaglioFuelComplete().stream()
                                                 .map(ds -> {
                                                   DettaglioStanziamentoCarburanteEntity dsEntity = dsMapper.toEntity(ds);
                                                   Set<StanziamentoEntity> sEntitySet = sMapper.toEntity(ds.getStanziamenti());
                                                   dsEntity.getStanziamenti()
                                                           .addAll(sEntitySet);
                                                   return dsEntity;
                                                 })
                                                 .collect(toList());
    var dsCloned = newDettaglioFuelComplete().stream()
                                             .map(ds -> {
                                               DettaglioStanziamentoCarburanteEntity dsEntity = dsMapper.toEntity(ds);
                                               Set<StanziamentoEntity> sEntitySet = sMapper.toEntity(ds.getStanziamenti());
                                               dsEntity.getStanziamenti()
                                                       .addAll(sEntitySet);
                                               return dsEntity;
                                             })
                                             .findFirst()
                                             .get();

    when(dsCarburanteServiceMock.findToUpdatePriceTableOpen(filterDTO)).thenReturn(dettagliFuel);

    when(dsCarburanteServiceMock.cloneDettaglioStanziamento(dettagliFuel.get(0))).thenReturn(dsCloned);

    when(stanziamentoRepoMock.cloneAndModify(dsCloned.getStanziamenti()
                                                     .stream()
                                                     .findFirst()
                                                     .get(),
                                             InvoiceType.P)).thenReturn(dsCloned.getStanziamenti()
                                                                                .stream()
                                                                                .findFirst()
                                                                                .get());

    when(dsCarburanteServiceMock.cloneStanziamentoCarburanteFromProvvisorio(dsCloned.getStanziamenti()
                                                                                    .stream()
                                                                                    .findFirst()
                                                                                    .get())).thenReturn(dsCloned.getStanziamenti()
                                                                                                                .stream()
                                                                                                                .findFirst()
                                                                                                                .get());

    var dsCarburanteDomainCloned = newDettaglioFuelComplete().get(0);
    when(dsCarburanteServiceMock.mapToDomain(dsCloned)).thenReturn(dsCarburanteDomainCloned);
    var dsCarburanteDomainOrig = newDettaglioFuelComplete().get(0);
    when(dsCarburanteServiceMock.mapToDomain(dettagliFuel.get(0))).thenReturn(dsCarburanteDomainOrig);

    when(dsCarburanteServiceMock.mapToEntity(dsCarburanteDomainCloned)).thenReturn(dsCloned);
    when(dsCarburanteServiceMock.mapToEntity(dsCarburanteDomainOrig)).thenReturn(dettagliFuel.get(0));

    priceTableProcessor.consumeMessage(priceTableDto);

    verify(dsCarburanteServiceMock).findToUpdatePriceTableOpen(filterDTO);

    verify(stanziamentoRepoMock, times(1)).cloneAndModify(Mockito.any(), Mockito.any());

    verify(dsCarburanteServiceMock, times(2)).update(Mockito.any());

  }

  void init() {
    priceTableClosedIntervalProcessor = new PriceTableClosedIntervalConsumerImpl(dsCarburanteServiceMock, notificationServiceMock,
                                                                                  stanziamentiNavJmsProducer, sMapper);
    priceTableOpenIntervalProcessor = new PriceTableOpenIntervalConsumerImpl(dsCarburanteServiceMock, notificationServiceMock,
                                                                              priceCalculatedByNavServiceMock, stanziamentoRepoMock,
                                                                              stanziamentiNavJmsProducer, sMapper);
    priceTableProcessor = new PriceTableConsumer(priceTableOpenIntervalProcessor, priceTableClosedIntervalProcessor);

  }

  private List<DettaglioStanziamentoCarburante> newDettaglioStanziamentoCarburanteWithoutStanziamento() {
    List<DettaglioStanziamentoCarburante> list = new ArrayList<>();
    DettaglioStanziamentoCarburante dsCarburante = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                                             .withCurrencyEUR()
                                                                                             .withInvoiceTypeP()
                                                                                             .withPriceRefernce(100)
                                                                                             .build();

    dsCarburante.setFuelStation(new FuelStation("::fuelStation::"));
    dsCarburante.setDevice(new Device("::seriale::", TipoDispositivoEnum.TRACKYCARD));
    dsCarburante.setCustomer(new Customer("::customerId::"));
    dsCarburante.setQuantity(BigDecimal.TEN);
    dsCarburante.setKilometers(new BigDecimal("100"));

    if (dsCarburante.getPriceComputed() == null) {
      dsCarburante.setAmount(new Amount());
    }
    MonetaryAmount monetaryAmount = Monetary.getDefaultAmountFactory()
                                            .setNumber(11d)
                                            .setCurrency("EUR")
                                            .create();
    dsCarburante.getPriceComputed()
                .setAmountExcludedVat(monetaryAmount);
    dsCarburante.setSource(new Source(Instant.now(), Instant.now(), "::FILE::"));
    dsCarburante.getSource().setRowNumber(1l);
    dsCarburante.setServicePartner(new ServicePartner("::servicepartner::"));
    Vehicle vehicle = new Vehicle();
    vehicle.setEuroClass("euroClass1");
    vehicle.setFareClass("::fareClass::");
    vehicle.setLicensePlate(new VehicleLicensePlate("::licensePlate::", "::country::"));
    dsCarburante.setVehicle(vehicle);

    list.add(dsCarburante);
    return list;
  }

  private List<DettaglioStanziamentoCarburante> newDettaglioFuelComplete() {
    List<DettaglioStanziamentoCarburante> dettagliStanziamenti = new ArrayList<>();
    DettaglioStanziamentoCarburante dsCarburante = DettaglioStanziamentoCarburanteTestBuilder.newInstance()
                                                                                             .withCurrencyEUR()
                                                                                             .withInvoiceTypeP()
                                                                                             .withPriceRefernce(100)
                                                                                             .withCostComputed()
                                                                                             .build();

    dsCarburante.setFuelStation(new FuelStation("::fuelStation::"));
    dsCarburante.setDevice(new Device("::seriale::", TipoDispositivoEnum.TRACKYCARD));
    dsCarburante.setCustomer(new Customer("::customerId::"));
    dsCarburante.setQuantity(BigDecimal.TEN);
    dsCarburante.setKilometers(new BigDecimal("100"));

    if (dsCarburante.getPriceComputed() == null) {
      dsCarburante.setAmount(new Amount());
    }
    MonetaryAmount monetaryAmount = Monetary.getDefaultAmountFactory()
                                            .setNumber(11d)
                                            .setCurrency("EUR")
                                            .create();
    dsCarburante.getPriceComputed()
                .setAmountExcludedVat(monetaryAmount);
    dsCarburante.setSource(new Source(Instant.now(), Instant.now(), "::FILE::"));
    dsCarburante.getSource().setRowNumber(1l);

    dsCarburante.setServicePartner(new ServicePartner("::servicepartner::"));
    Vehicle vehicle = new Vehicle();
    vehicle.setEuroClass("euroClass1");
    vehicle.setFareClass("::fareClass::");
    vehicle.setLicensePlate(new VehicleLicensePlate("::licensePlate::", "::country::"));
    dsCarburante.setVehicle(vehicle);
    Stanziamento s = StanziamentoTestBuilder.newInstance()
                                            .withInvoiceTypeP()
                                            .build();

    s.setArticleCode("::art_code::");

    dsCarburante.getStanziamenti()
                .clear();
    dsCarburante.getStanziamenti()
                .add(s);
    
    dettagliStanziamenti.add(dsCarburante);
    return dettagliStanziamenti;
  }

  PriceTableDTO newPriceTableDtoClosedInterval() {
    PriceTableDTO dto = new PriceTableDTO();
    dto.setId(1L);
    dto.setCodiceArticolo("::articleCode::");
    dto.setSupplierCode("::suppler_code::");
    dto.setCostRevenueFlag(CostRevenue.COST);
    dto.setReferencePrice(new BigDecimal(13));
    dto.setValidFrom(Instant.now());
    dto.setValidUntil(Instant.now()
                             .plus(1, ChronoUnit.DAYS));
    return dto;
  }

  PriceTableDTO newPriceTableDtoOpenInterval() {
    PriceTableDTO dto = newPriceTableDtoClosedInterval();
    dto.setValidUntil(null);
    return dto;
  }

}
