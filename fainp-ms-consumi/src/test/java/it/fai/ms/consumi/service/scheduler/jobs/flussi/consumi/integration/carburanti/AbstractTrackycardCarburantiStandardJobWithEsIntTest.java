package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import java.io.File;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd.TrackyCardCarbStdProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.repository.NotificationSetupRepository;
import it.fai.common.notification.service.NotificationMapper;
import it.fai.common.notification.service.NotificationMessageSender;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AuthorizationApiClient;
import it.fai.ms.consumi.client.GeoClient;
import it.fai.ms.consumi.client.StanziamentiApiAuthClient;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardJob;
import it.fai.ms.consumi.service.navision.NavArticlesService;
import it.fai.ms.consumi.service.navision.NavSupplierCodesService;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractTelepassIntTest;
import it.fai.ms.consumi.service.validator.ConsumoNoBlockingValidatorService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidatorImpl;
import it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory;

@ComponentScan(basePackages = {"it.fai.ms.consumi.util"})
@EnableFeignClients(clients = { ElasticSearchFeignClient.class, AuthorizationApiClient.class, StanziamentiApiAuthClient.class, GeoClient.class})
@Tag("integration")
public abstract class AbstractTrackycardCarburantiStandardJobWithEsIntTest
  extends AbstractIntegrationTestSpringContext {

  protected TrackyCardCarburantiStandardJob                                                       job;
  protected StanziamentiParamsValidator                                                           stanziamentiParamsValidator;
  @Inject
  protected TrackyCardCarbStdProcessor                                                            processor;
  @Inject
  protected RecordConsumoMapper<TrackyCardCarburantiStandardRecord, TrackyCardCarburantiStandard> consumoMapper;

  @Inject
  protected StanziamentoRepository stanziamentiRepository;

  @Inject
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Inject
  protected StanziamentiParamsService              stanziamentiParamsService;
  protected TrackyCardCarburantiStandardDescriptor descriptor;

  protected transient final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  protected RecordPersistenceService recordPersistenceService;

  @Autowired
  protected ServiceProviderRepository serviceProviderRepository;

  @Autowired
  private NavArticlesService navArticleService;

  @Autowired
  private PlatformTransactionManager transactionManager;
  
  @Autowired
  ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService;

  @BeforeEach
  public void setUp() throws Exception {

    descriptor = new TrackyCardCarburantiStandardDescriptor();
    stanziamentiParamsValidator = spy(new StanziamentiParamsValidatorImpl(stanziamentiParamsService, navArticleService,
                                                                          mock(NavSupplierCodesService.class), consumoNoBlockingValidatorService, mock(ApplicationProperties.class),true));
    AllStanziamentiParams stanzaParams = stanziamentiParamsService.getAll();
    assertThat(stanzaParams).isNotNull();
    List<StanziamentiParams> stanzParamList = stanzaParams.getStanziamentiParams();
    assertThat(stanzParamList).isNotEmpty();
    var oneStanziamentiParamForTest = stanzParamList.get(0);
    log.info("StanziamentiParam in use for Tests = " + oneStanziamentiParamForTest.toString());
    doReturn(ValidationOutcomeTestFactory.newValidationOutcomeWithStanzParam(oneStanziamentiParamForTest)).when(stanziamentiParamsValidator)
                                                                                                          .checkConsistency();


    NotificationService notificationService = spy(new NotificationService(mock(NotificationMessageSender.class),
                                                                          mock(NotificationSetupRepository.class),
                                                                          mock(NotificationMapper.class), "TrackycardCarburantiStandard"));
    doAnswer(invocation -> {
      log.info("notifying code: " + invocation.getArgument(0));
      Map<String, Object> errorMapCode = invocation.getArgument(1);
      errorMapCode.entrySet()
                  .stream()
                  .forEach(entry -> log.info("key for message:" + entry.getKey() + " " + entry.getValue()));
      return null;
    }).when(notificationService)
      .notify(anyString(), anyMap());
    job = new TrackyCardCarburantiStandardJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService,
                                              descriptor, processor, consumoMapper, stanziamentiToNavJmsPublisher);


  }

  protected String getAbsolutePath(String relativePath) {
    try {
      return new File(AbstractTelepassIntTest.class.getResource(relativePath).toURI()).getAbsolutePath();
    } catch (URISyntaxException e) {
      throw new RuntimeException("Cannot find file in given path!", e);
    }
  }

  protected ServicePartner findServicePartner(String providerCode) {
    ServiceProvider sp = serviceProviderRepository.findByProviderCode(providerCode);
    return sp.toServicePartner();
  }


}

