package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;


import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TollCollectRecord;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@Tag("unit")
public class TollCollectRecordDescriptorTest {

  public static final String TOLL_COLLECT_FILENAME = "O_DTC5659_IZ_2_0411032037_S_051117_185190";
  private TollCollectRecordDescriptor recordDescriptor;

  private String data;
  private String header="Invoice Recipient;Recipient City;Statement Level;EDI Partner Code;RAG Code;Invoice Number;Invoice Date;Due Date;Position Number;Subposition Number;Processing Date;Supplier Name;Supplier Address 1;Supplier Address 2;Supplier UIN;Supplier Tax Number;Card Number;Supplier Number;Registration Country;Registration Number;Service Description;Branch;Supplier Service Date;Supplier Cost Center;Supplier Invoice Number;Supplier Invoice Date;Supplier Statement Date;Sale Currency (SC);Net Amount (SC);VAT Percentage;VAT Percentage (SC);Gross Amount (SC);Currency Rate;Statement Currency (IC);Net Amount (IC);VAT (IC)";
  private final Date now = Date.from(Instant.parse("2018-08-29T10:37:30.00Z"));

  @BeforeEach
  public void init()throws Exception{
    data="F.A.I. Service Societa;12100 Cuneo, CN;'6016122000233800;'DTC5659;A;'A648974761-0-0;07.08.2017;21.08.2017;1;1;18.07.2017;ASFINAG Autobahnen- und Schnellstr.;Rotenturmstr. 5    - 9;1011 Wien;'ATU43143200;;'6016122000389149;;IT;FB912DS;Post-Pay toll payments;;17.07.2017;;;;;EUR;36,89;20;7,38;44,27;1;EUR;36,89;7,38";

    recordDescriptor = new TollCollectRecordDescriptor(){
      @Override
      protected Date getNow() {
        return now;
      }
    };
  }


  @Test
  public void getStartEndPosTotalRows() {
    assertThatExceptionOfType(RuntimeException.class)
      .isThrownBy(()->recordDescriptor.getStartEndPosTotalRows())
    .matches(e->e.getMessage().equals("Not implemented"));
  }

  @Test
  public void getHeaderBegin() {
    assertThat(recordDescriptor.getHeaderBegin()).isEqualTo("Invoice Recipient;Recipient City;Statement Level;EDI Partner Code;RAG Code;Invoice Number;Invoice Date;Due Date;Position Number;Subposition Number;Processing Date;Supplier Name;Supplier Address 1;Supplier Address 2;Supplier UIN;Supplier Tax Number;Card Number;Supplier Number;Registration Country;Registration Number;Service Description;Branch;Supplier Service Date;Supplier Cost Center;Supplier Invoice Number;Supplier Invoice Date;Supplier Statement Date;Sale Currency (SC);Net Amount (SC);VAT Percentage;VAT Percentage (SC);Gross Amount (SC);Currency Rate;Statement Currency (IC);Net Amount (IC);VAT (IC)");
  }

  @Test
  public void getFooterCodeBegin() {
    assertThat(recordDescriptor.getFooterCodeBegin()).isEqualTo("§ NO HEADER FILE §");
  }

  @Test
  public void getLinesToSubtractToMatchDeclaration() {
    assertThat(recordDescriptor.getLinesToSubtractToMatchDeclaration())
      .isEqualTo(0);
  }

  @Test
  public void extractRecordCode_header(){
    assertThat(recordDescriptor.extractRecordCode(data))
      .isEqualTo("");
  }

  @Test
  public void extractRecordCode_record(){
    assertThat(recordDescriptor.extractRecordCode(data))
      .isEqualTo("");
  }

  @Test
  public void extractRecordCode_footer(){
    assertThat(recordDescriptor.extractRecordCode(data))
      .isEqualTo("");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_header(){
    TollCollectRecord record = recordDescriptor.decodeRecordCodeAndCallSetFromString(header, "", TOLL_COLLECT_FILENAME, 0);

    assertThat(record.getRecordCode())
      .isEqualTo("DEFAULT");
    assertThat(record.getTransactionDetail())
      .isEqualTo("");
    assertThat(record.getCreationDateInFile())
      .isEqualTo("20180829");
    assertThat(record.getCreationTimeInFile())
      .isEqualTo("123730");
    assertThat(record.getInvoiceNumber()).isNullOrEmpty();

  }

  @Test
  void decodeRecordCodeAndCallSetFromString_record() {
    TollCollectRecord record = recordDescriptor.decodeRecordCodeAndCallSetFromString(data, "", TOLL_COLLECT_FILENAME, 1);

    assertThat(record.getRecordCode()).isEqualTo("DEFAULT");
    assertThat(record.getTransactionDetail()).isEqualTo("");
    assertThat(record.getInvoiceRecipient()).isEqualTo("F.A.I. Service Societa");
    assertThat(record.getRecipientCity()).isEqualTo("12100 Cuneo, CN");
    assertThat(record.getStatementLevel()).isEqualTo("'6016122000233800");
    assertThat(record.getEDIPartnerCode()).isEqualTo("'DTC5659");
    assertThat(record.getRAGCode()).isEqualTo("A");
    assertThat(record.getInvoiceNumber()).isEqualTo("'A648974761-0-0");
    assertThat(record.getInvoiceDate()).isEqualTo("07.08.2017");
    assertThat(record.getDueDate()).isEqualTo("21.08.2017");
    assertThat(record.getPositionNumber()).isEqualTo("1");
    assertThat(record.getSubPositionNumber()).isEqualTo("1");
    assertThat(record.getProcessingDate()).isEqualTo("18.07.2017");
    assertThat(record.getSupplierName()).isEqualTo("ASFINAG Autobahnen- und Schnellstr.");
    assertThat(record.getSupplierAddress1()).isEqualTo("Rotenturmstr. 5    - 9");
    assertThat(record.getSupplierAddress2()).isEqualTo("1011 Wien");
    assertThat(record.getSupplierUIN()).isEqualTo("'ATU43143200");
    assertThat(record.getSupplierTaxNumber()).isEqualTo("");
    assertThat(record.getCardNumber()).isEqualTo("'6016122000389149");
    assertThat(record.getSupplierNumber()).isEqualTo("");
    assertThat(record.getRegistrationCountry()).isEqualTo("IT");
    assertThat(record.getRegistrationNumber()).isEqualTo("FB912DS");
    assertThat(record.getServiceDescription()).isEqualTo("Post-Pay toll payments");
    assertThat(record.getBranch()).isEqualTo("");
    assertThat(record.getSupplierServiceDate()).isEqualTo("17.07.2017");
    assertThat(record.getSupplierCostCenter()).isEqualTo("");
    assertThat(record.getSupplierInvoiceNumber()).isEqualTo("");
    assertThat(record.getSupplierInvoiceDate()).isEqualTo("");
    assertThat(record.getSupplierStatementDate()).isEqualTo("");
    assertThat(record.getSaleCurrencySC()).isEqualTo("EUR");
    assertThat(record.getNetAmountSC()).isEqualTo("36,89");
    assertThat(record.getVatPercentage()).isEqualTo("20");
    assertThat(record.getGrossAmountSC()).isEqualTo("44,27");
    assertThat(record.getCurrencyRate()).isEqualTo("1");
    assertThat(record.getStatementCurrencyIC()).isEqualTo("EUR");
    assertThat(record.getNetAmountIC()).isEqualTo("36,89");
    assertThat(record.getVatIC()).isEqualTo("7,38");



  }



}
