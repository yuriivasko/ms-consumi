package it.fai.ms.consumi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.StanziamentoTestBuilder;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoEntityRepository;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;

@Tag("unit")
class SendStanziamentiToNavResourceTest {

  private final StanziamentoRepository stanziamentoRepository = mock(StanziamentoRepository.class);

  private final StanziamentiToNavPublisher stanziamentiToNavPublisher = mock(StanziamentiToNavPublisher.class);

	private final StanziamentoEntityMapper stanziamentoEntityMapper = mock(StanziamentoEntityMapper.class);
	private final StanziamentoEntityRepository stanziamentoEntityRepository = mock(StanziamentoEntityRepository.class);
	
  private SendStanziamentiToNavResource sendStanziamentiToNavResource;

  @BeforeEach
  void setUp() {
    sendStanziamentiToNavResource = new SendStanziamentiToNavResource(stanziamentoRepository, stanziamentiToNavPublisher,stanziamentoEntityMapper,stanziamentoEntityRepository);
  }

  @Test
  void sendAllStanziamentiToNav_ok() {
    given(stanziamentoRepository.find100NotQueuedToNav())
      .willReturn(Arrays.asList(
        StanziamentoTestBuilder.newInstance().withInvoiceTypeP().build(),
        StanziamentoTestBuilder.newInstance().withInvoiceTypeP().build(),
        StanziamentoTestBuilder.newInstance().withInvoiceTypeP().build()
      ))
    .willReturn(Collections.emptyList());

    ResponseEntity<List<SendStanziamentiToNavResource.StanziamentoSentDTO>> ret = sendStanziamentiToNavResource.sendAllStanziamentiToNav();

    assertThat(ret.getBody().size())
      .isEqualTo(0);

//    assertThat(ret.getBody().get(0).getException())
//      .isNull();
//
//    assertThat(ret.getBody().get(0).getOutcome())
//      .isEqualTo(SENT);
//
//    assertThat(ret.getBody().get(0).getStanziamentoMessage())
//      .isNotNull();
//
//    assertThat(ret.getBody().get(0).getStanziamentoMessage().getCode())
//      .isEqualTo("::code::");
  }


  @Test
  void sendAllStanziamentiToNav_error() {
    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");

    final Stanziamento stanziamentoInError = StanziamentoTestBuilder.newInstance().withInvoiceTypeP().build();
    given(stanziamentoRepository.find100NotQueuedToNav())
      .willReturn(Arrays.asList(
        StanziamentoTestBuilder.newInstance().withInvoiceTypeP().build(),
        stanziamentoInError,
        StanziamentoTestBuilder.newInstance().withInvoiceTypeP().build()
      ))
    .willReturn(Collections.emptyList());

    willDoNothing().willThrow(runtimeExTest).willDoNothing().
      given(stanziamentiToNavPublisher).publish(any());

    ResponseEntity<List<SendStanziamentiToNavResource.StanziamentoSentDTO>> ret = sendStanziamentiToNavResource.sendAllStanziamentiToNav();

    assertThat(ret.getBody().size())
      .isEqualTo(0);

//    assertThat(ret.getBody().get(1).getException())
//      .isNotNull();
//
//    assertThat(ret.getBody().get(1).getOutcome())
//      .isEqualTo(ERROR);
//
//    assertThat(ret.getBody().get(1).getStanziamentoMessage())
//      .isNotNull();
//
//    assertThat(ret.getBody().get(1).getStanziamentoMessage().getCode())
//      .isEqualTo("::code::");
  }
}
