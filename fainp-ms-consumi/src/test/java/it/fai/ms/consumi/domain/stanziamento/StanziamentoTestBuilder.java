package it.fai.ms.consumi.domain.stanziamento;

import it.fai.ms.consumi.domain.InvoiceType;

public class StanziamentoTestBuilder {

  public static StanziamentoTestBuilder newInstance() {
    return new StanziamentoTestBuilder();
  }

  private Stanziamento stanziamento;

  private StanziamentoTestBuilder() {
  }

  public StanziamentoTestBuilder withInvoiceTypeD() {
    if (stanziamento == null) {
      stanziamento = new Stanziamento("::code::");
    }
    stanziamento.setInvoiceType(InvoiceType.D);
    return this;
  }

  public StanziamentoTestBuilder withInvoiceTypeP() {
    if (stanziamento == null) {
      stanziamento = new Stanziamento("::code::");
    }
    stanziamento.setInvoiceType(InvoiceType.P);
    return this;
  }

  public Stanziamento build() {
    return stanziamento;
  }

}
