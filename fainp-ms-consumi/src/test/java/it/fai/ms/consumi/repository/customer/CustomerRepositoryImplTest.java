package it.fai.ms.consumi.repository.customer;

import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.repository.storico_dml.StoricoContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackageClasses = StoricoContratto.class)
@DataJpaTest
@EntityScan(basePackageClasses = StoricoContratto.class)
@EnableJpaRepositories("it.fai.ms.consumi.repository.storico_dml")
@Tag("unit")
@Transactional
class CustomerRepositoryImplTest {

  @Autowired
  private EntityManager em;

  private CustomerRepository customerRepository;
  
  @Autowired
  private StoricoContrattoRepository repo;

  @BeforeEach
  void setUp() throws Exception {

    customerRepository = new CustomerRepositoryImpl(em, new CustomerEntityMapper(),repo);

    var contract_1 = new StoricoContratto();
    contract_1.setCodiceAzienda("::companyCode::");
    contract_1.setDmlUniqueIdentifier(UUID.randomUUID()
                                                   .toString());
    contract_1.setContrattoNumero("::contractNumber::");
    contract_1.setDataVariazione(Instant.ofEpochMilli(1));
    contract_1.setDmlRevisionTimestamp(Instant.ofEpochMilli(1));
    contract_1.setStatoContratto("::state-1::");

    var contract_2 = new StoricoContratto();
    contract_2.setCodiceAzienda("::companyCode::");
    contract_2.setDmlUniqueIdentifier(UUID.randomUUID()
                                                   .toString());
    contract_2.setContrattoNumero("::contractNumber::");
    contract_2.setDataVariazione(Instant.ofEpochMilli(2));
    contract_2.setDmlRevisionTimestamp(Instant.ofEpochMilli(2));
    contract_2.setStatoContratto("::state-2::");

    var contract_3 = new StoricoContratto();
    contract_3.setCodiceAzienda("::companyCode::");
    contract_3.setDmlUniqueIdentifier(UUID.randomUUID()
                                                   .toString());
    contract_3.setContrattoNumero("::contractNumber::");
    contract_3.setDataVariazione(Instant.ofEpochMilli(3));
    contract_3.setDmlRevisionTimestamp(Instant.ofEpochMilli(3));
    contract_3.setStatoContratto("::state-3::");

    em.persist(contract_1);
    em.persist(contract_2);
    em.persist(contract_3);

    em.flush();
  }

  @Test
  void testFindCustomerByUuid() {

    var optionalCustomerByUuid = customerRepository.findCustomerByUuid("::companyCode::");

    assertThat(optionalCustomerByUuid).contains(newCustomer());
  }

  @Test
  void testFindCustomerByUuid_notExists() {

    var optionalCustomerByUuid = customerRepository.findCustomerByUuid("::companyCodeNotPresent::");

    assertThat(optionalCustomerByUuid).isNotPresent();
  }

  private static Customer newCustomer() {
    Customer customer = new Customer("::companyCode::");
    return customer;
  }

}
