package it.fai.ms.consumi.service.validator.internal;

import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.descriptionFromValidationOutcome;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import it.fai.ms.consumi.domain.consumi.DateSupplier;
import it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.service.DeviceService;

@Tag("unit")
class DeviceValidatorImplTest {

  private final Instant       _8_10_2018       = Instant.ofEpochMilli(1539001877000l);
  private final boolean       validatorEnabled = true;
  private final DeviceService deviceService    = mock(DeviceService.class);
  private final JmsProperties jmsProperties    = mock(JmsProperties.class);

  DeviceValidatorImpl serviceValidator;

  @BeforeEach
  void setUp() {
    serviceValidator = new DeviceValidatorImpl(deviceService, jmsProperties, validatorEnabled);
  }


  @Test
  void given_device_with_pan_find_by_pan(){
    Device device = newDevice("::pan::", "::seriale::");
    given(deviceService.findDeviceByPan(device.getPan(),device.getServiceType(),null))
      .willReturn(Optional.of(device));
    ValidationOutcome validationOutcome = serviceValidator.findAndSetDevice(device);
    then(deviceService).should(never()).findDeviceBySeriale(any(),any(),any());
    then(deviceService).should().findDeviceByPan(device.getPan(),device.getServiceType(),null);
    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isTrue();
    assertThat(validationOutcome.getMessages()).isEmpty();
  }
  @Test
  void given_device_with_id_no_pan_find_by_id(){
    Device device = newDevice(null, "::seriale::");
    given(deviceService.findDeviceBySeriale(device.getSeriale(),device.getType(),null))
      .willReturn(Optional.of(device));
    ValidationOutcome validationOutcome = serviceValidator.findAndSetDevice(device);
    then(deviceService).should().findDeviceBySeriale(device.getSeriale(),device.getType(),null);
    then(deviceService).should(never()).findDeviceByPan(any(),any(),any());
    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isTrue();
    assertThat(validationOutcome.getMessages()).isEmpty();
  }
  @Test
  void given_device_with_no_id_no_pan_skip(){
    Device device = newDevice(null, null);
    ValidationOutcome validationOutcome = serviceValidator.findAndSetDevice(device);
    then(deviceService).should(never()).findDeviceByPan(any(),any(),any());
    then(deviceService).should(never()).findDeviceBySeriale(any(),any(),any());
    assertThat(validationOutcome).isNull();
  }

  @Test
  void given_device_with_pan_find_by_pan_not_found_blocking_error(){
    Device device = newDevice("::pan::", "::seriale::");
    given(deviceService.findDeviceByPan(device.getPan(),device.getServiceType(),null))
      .willReturn(Optional.empty());
    ValidationOutcome validationOutcome = serviceValidator.findAndSetDevice(device);
    then(deviceService).should(never()).findDeviceBySeriale(any(),any(),any());
    then(deviceService).should().findDeviceByPan(device.getPan(),device.getServiceType(),null);
    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.getMessages())
      .isNotEmpty()
      .extracting(m->tuple(m.getCode(),m.getText()))
      .containsExactlyInAnyOrder(
        tuple("b001", "Device " + device.getType() + " with pan/service ::pan::/"+ device.getServiceType()+ " not found")
      );
  }
  @Test
  void given_device_with_id_no_pan_find_by_idnot_found_blocking_error(){
    Device device = newDevice(null, "::seriale::");
    given(deviceService.findDeviceBySeriale(device.getSeriale(),device.getType(),null))
      .willReturn(Optional.empty());
    ValidationOutcome validationOutcome = serviceValidator.findAndSetDevice(device);
    then(deviceService).should().findDeviceBySeriale(device.getSeriale(),device.getType(),null);
    then(deviceService).should(never()).findDeviceByPan(any(),any(),any());
    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.getMessages())
      .isNotEmpty()
      .extracting(m->tuple(m.getCode(),m.getText()))
      .containsExactlyInAnyOrder(
        tuple("b001", "Device " + device.getType() + " with serialCode ::seriale:: not found")
      );
  }


  @Test
  void given_no_device_serviceWasActiveInDate_should_throws_exception() {
    final Device device = null;
    assertThrows(IllegalArgumentException.class, () -> {
      ValidationOutcome validationOutcome = serviceValidator.serviceWasActiveInDate(device, _8_10_2018, DateSupplier.VALIDATOR_DATE_OFFSET);
    });
  }

  @Test
  void given_INCOMPLETE_device_serviceWasActiveInDate_should_return_validation_fail() {
    Device device = newDevice("::pan::", "::seriale::");
    device.setPan(null);
    ValidationOutcome validationOutcome = serviceValidator.serviceWasActiveInDate(device, _8_10_2018, DateSupplier.VALIDATOR_DATE_OFFSET);

    assertThat(validationOutcome.isValidationOk()).isFalse();

    assertThat(validationOutcome.getMessages()).isNotEmpty();

    device = newDevice("::pan::", "::seriale::");
    device.setType(null);
    validationOutcome = serviceValidator.serviceWasActiveInDate(device, _8_10_2018, DateSupplier.VALIDATOR_DATE_OFFSET);

    assertThat(validationOutcome.isValidationOk()).isFalse();

    assertThat(validationOutcome.getMessages()).isNotEmpty();
  }

  @Test
  void given_device_with_service_active_in_consumo_date_serviceWasActiveInDate_should_return_validation_OK_false() {
    Device device = newDevice("::pan::", "::seriale::");

    String _deviceSeriale = "::seriale::";
    String _pan = "::pan::";
    TipoServizioEnum _deviceType = TipoServizioEnum.PEDAGGI_AUSTRIA;
    Instant _date = _8_10_2018;

    given(deviceService.findStoricoStatoServizio(eq(_deviceSeriale), eq(_deviceType), eq(_date)))
      .willReturn(Optional.empty());

    ValidationOutcome validationOutcome = serviceValidator.serviceWasActiveInDate(device, _8_10_2018, DateSupplier.VALIDATOR_DATE_OFFSET);

    assertThat(validationOutcome.isValidationOk()).isFalse();

    assertThat(validationOutcome.getMessages()).isNotEmpty();

  }

  @Test
  void given_device_with_service_active_in_consumo_date_serviceWasActiveInDate_NON_ATTIVO_should_return_validation_OK_false() {
    Device device = newDevice("::pan::", "::seriale::");

    String _deviceSeriale = "::seriale::";
    String _pan = "::pan::";
    TipoServizioEnum _deviceType = TipoServizioEnum.PEDAGGI_AUSTRIA;
    Instant _date = _8_10_2018;

    final StoricoStatoServizio storicoStatoServizio = new StoricoStatoServizio();
    storicoStatoServizio.setDispositivoDmlUniqueIdentifier("::dispositivoDmlUniqueIdentifier::");
    storicoStatoServizio.setId("::id::");
    storicoStatoServizio.setPan(null);
    storicoStatoServizio.setStato("NON_ATTIVO");
    storicoStatoServizio.setTipoServizio(TipoServizioEnum.PEDAGGI_AUSTRIA.name());
    storicoStatoServizio.setDataVariazione(Instant.EPOCH);
    storicoStatoServizio.setDataFineVariazione(Instant.MAX);

    given(deviceService.findStoricoStatoServizio(eq(_deviceSeriale), eq(_deviceType), eq(_date)))
      .willReturn(Optional.of(storicoStatoServizio));

    ValidationOutcome validationOutcome = serviceValidator.serviceWasActiveInDate(device, _8_10_2018, DateSupplier.VALIDATOR_DATE_OFFSET);

    assertThat(validationOutcome.isValidationOk()).isFalse();

    assertThat(validationOutcome.getMessages()).isNotEmpty();

  }

  @Test
  void given_device_with_service_active_in_consumo_date_serviceWasActiveInDate_ATTIVO_should_return_validation_OK_true() {
    Device device = newDevice("::pan::", "::seriale::");

    String _deviceSeriale = "::seriale::";
    String _pan = "::pan::";
    TipoServizioEnum _deviceType = TipoServizioEnum.PEDAGGI_AUSTRIA;
    Instant _date = _8_10_2018;

    final StoricoStatoServizio storicoStatoServizio = new StoricoStatoServizio();
    storicoStatoServizio.setDispositivoDmlUniqueIdentifier("::dispositivoDmlUniqueIdentifier::");
    storicoStatoServizio.setId("::id::");
    storicoStatoServizio.setPan(null);
    storicoStatoServizio.setStato("ATTIVO");
    storicoStatoServizio.setTipoServizio(TipoServizioEnum.PEDAGGI_AUSTRIA.name());
    storicoStatoServizio.setDataVariazione(Instant.EPOCH);
    storicoStatoServizio.setDataFineVariazione(Instant.MAX);

    given(deviceService.findStoricoStatoServizio(eq(_deviceSeriale), eq(_deviceType), eq(_date)))
      .willReturn(Optional.of(storicoStatoServizio));

    ValidationOutcome validationOutcome = serviceValidator.serviceWasActiveInDate(device, _8_10_2018, DateSupplier.VALIDATOR_DATE_OFFSET);

    assertThat(validationOutcome.isValidationOk()).isFalse();

    assertThat(validationOutcome.getMessages()).isNotEmpty();

  }

  private Device newDevice(String pan, String seriale) {
    final Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setSeriale(seriale);
    device.setPan(pan);
    device.setServiceType(TipoServizioEnum.PEDAGGI_AUSTRIA);
    return device;
  }
}
