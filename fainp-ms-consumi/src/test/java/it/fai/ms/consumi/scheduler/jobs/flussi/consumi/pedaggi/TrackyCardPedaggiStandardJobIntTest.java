package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static it.fai.ms.consumi.testutils.TestUtils.init;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.byLessThan;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.money.Monetary;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.test.context.ActiveProfiles;


import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.enumeration.PriceSource;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecordBody;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTRecord;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;


@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"), })

@ActiveProfiles(profiles = "integration")
@Tag("integration")
@Disabled
public class TrackyCardPedaggiStandardJobIntTest extends AbstractIntegrationTestSpringContext {
 
  private static final String fileName = "trackycardstd";
  private Path                path;

  @MockBean
  private ElasticSearchFeignClient elasticSearchFeignClient;

  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private NotificationService notificationService;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @SpyBean
  private TrackyCardPedaggiStandardProcessor processor;

  @Autowired
  private TrackyCardPedaggiStandardJob job;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  
  @BeforeEach
  public void setup() throws Exception {
     path = Paths.get(TrackyCardPedaggiStandardJobIntTest.class.getResource("/test-files/trackycardped").toURI());
     assertThat(job).isNotNull(); 
  }

  @Test
  public void elaborateOneRecord() {

    ServicePartner serviceProvider = init(new ServicePartner(fileName), s -> {
      s.setPartnerCode("trackyCard");
      s.setPartnerName("trackyCard");
      s.setFormat(Format.TRACKY_CARD_PEDAGGI);
      s.setPriceSoruce(PriceSource.by_invoice);
    }); 

    String filePath = path.resolve(fileName)
        .toAbsolutePath()
        .toString();
    String result   = job.process(filePath, System.currentTimeMillis(), new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull()
    .isEqualTo("trackyCard");


    var type = ArgumentCaptor.forClass(String.class);
    @SuppressWarnings("unchecked")
    ArgumentCaptor<ElasticSearchRecord<TrackyCardPedaggiStandardRecordBody>> captureRecord = ArgumentCaptor.forClass(ElasticSearchRecord.class);
    verify(elasticSearchFeignClient, times(1)).create(anyString(), type.capture(), captureRecord.capture());

    ElasticSearchRecord<TrackyCardPedaggiStandardRecordBody> esRecords = captureRecord.getValue();
    TrackyCardPedaggiStandardRecordBody record = esRecords.getRawdata();
      assertThat(record).isInstanceOfSatisfying(TrackyCardPedaggiStandardRecordBody.class, r -> { 
      assertThat(r.getNumeroTessera()).isEqualTo("0021");
      assertThat(r.getImposta()).isEqualTo("1082");
    });
      
    ArgumentCaptor<TrackyCardPedaggiStandardConsumo> acConsumo = ArgumentCaptor.forClass(TrackyCardPedaggiStandardConsumo.class);
    verify(processor, times(1)).validateAndProcess(acConsumo.capture(), any());
    TrackyCardPedaggiStandardConsumo consumo = acConsumo.getValue();
    assertThat(consumo.getDevice().getSeriale()).isEqualTo("7896970006014900218");    
    assertThat(consumo.getAmount().getVatRateBigDecimal()).isEqualByComparingTo("22.00"); 
//    assertThat(consumo.getAmount().getAmountIncludedVat().toString()).isEqualTo("EUR 60.01");
    assertThat(consumo.getAmount().getAmountExcludedVat().toString()).isEqualTo("EUR 49.19");     
    
    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(1);
    
    assertThat(dettaglioStanziamenti).allMatch(s -> s.getStanziamenti().size() == 1);
    
    var dettaglioStanziamento = dettaglioStanziamenti.get(0); 

      assertThat(dettaglioStanziamento).isInstanceOfSatisfying(DettaglioStanziamentoPedaggioEntity.class, ds -> {
      assertThat(ds.getSourceRowNumber()).isEqualTo(1);
      assertThat(ds.getFileName()).isEqualTo(StringUtils.substringAfterLast(filePath, "/"));
      assertThat(ds.getVehicleLicenseLicenseId()).isEqualTo("FA903CM");
      assertThat(ds.getVehicleLicenseCountryId()).isEqualTo("I");
      assertThat(ds.getVehicleFareClass()).isEqualTo(record.getClasseVeicolo());
      assertThat(ds.getRecordCode()).isEqualTo("D0");
      assertThat(ds.getTransactionDetailCode()).isNull();
      assertThat(ds.getDevicePan()).isNull();
      assertThat(ds.getDeviceObu()).isEqualTo("7896970006014900218");
      assertThat(ds.getAmountVatRate()).isEqualByComparingTo((new BigDecimal("22")));
      
 //      assertThat(ds.getAmountIncludedVat()).isEqualByComparingTo((new BigDecimal("60.01")));
     // assertThat(ds.getAmountIncludedVat().subtract(new BigDecimal("60.01"))).isLessThan((new BigDecimal("0.001")));
      assertThat(ds.getAmountIncludedVat().multiply(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP)).isEqualByComparingTo((new BigDecimal("6001.18")));
      assertThat(ds.getInvoiceType()).isEqualTo(InvoiceType.D);
      assertThat(ds.getEntryGlobalGateIdentifier()).isEqualTo(record.getCaselloIngresso());
      assertThat(ds.getExitGlobalGateIdentifier()).isEqualTo(record.getCaselloUscita());
      assertThat(ds.getExitGlobalGateIdentifierDescription()).isEqualTo(record.getCaselloUscita());
      assertThat(ds.getEntryTimestamp()).isNull();
      assertThat(ds.getExitTimestamp()).isEqualTo(toInstant("2017-06-30T14:59:27"));   
      assertThat(ds.getSourceType()).isEqualTo("TrackyCard");
      assertThat(ds.getAdrTaxableAmount()).isNull(); 
    }); 
    
  } 
  
  private Instant toInstant(String text1) {
    return LocalDateTime.parse(text1)
                        .atZone(ZoneId.of("Europe/Rome"))
                        .toInstant();
  }
  
}
