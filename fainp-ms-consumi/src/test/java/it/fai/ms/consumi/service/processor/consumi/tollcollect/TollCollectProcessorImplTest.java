package it.fai.ms.consumi.service.processor.consumi.tollcollect;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.AmountBuilder;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
public class TollCollectProcessorImplTest {
  TollCollectProcessorImpl tollCollectProcessor;

  private final ConsumoValidatorService consumoValidatorService = mock(ConsumoValidatorService.class);
  private final StanziamentiProducer stanziamentiProducer    = mock(StanziamentiProducer.class);
  private final TollCollectConsumoDettaglioStanziamentoPedaggioMapper mapper                  = mock(
    TollCollectConsumoDettaglioStanziamentoPedaggioMapper.class);
  private final VehicleService vehicleService          = mock(VehicleService.class);
  private final CustomerService customerService         = mock(CustomerService.class);

  @BeforeEach
  public void setUp() {
    tollCollectProcessor = new TollCollectProcessorImpl(consumoValidatorService,
      vehicleService,
      customerService,
      stanziamentiProducer,
      mapper);
  }

  @Test
  void validateAndProcess_if_fail_blocking_validation_will_sikp_stanziamento_production() {
    TollCollect consumo = mock(TollCollect.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TOLL_COLLECT);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isFatalError())
      .willReturn(true);

    tollCollectProcessor.validateAndProcess(consumo, bucket);

    then(stanziamentiProducer)
      .shouldHaveZeroInteractions();
  }

  @Test
  void validateAndProcess_if_fail_waring_validation_will_produce_notification_and_stanziamentos() {
    TollCollect consumo = mock(TollCollect.class);
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TOLL_COLLECT);
    given(consumo.getAmount())
      .willReturn(AmountBuilder.builder().amountExcludedVat(MonetaryUtils.strToMonetary("10.00","EUR"))
        .vatRate(null)
        .build());

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    Bucket bucket = new Bucket("");

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet<>(Arrays.asList(new Message("").withText(""))));

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    given(stanziamentiParams.getArticle())
      .willReturn(mock(Article.class));

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(mapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamento.class));

    ProcessingResult<?> processingResult = tollCollectProcessor.validateAndProcess(consumo, bucket);

    Optional<Message> code = processingResult.getValidationOutcome().getMessages().stream().findFirst();
    Assertions.assertTrue(code.isPresent());

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }

  @Test
  void validateAndProcess_if_success_validation_will_produce_stanziamentos() {
    TollCollect consumo = mock(TollCollect.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);
    given(consumo.getAmount())
      .willReturn(AmountBuilder.builder().amountExcludedVat(MonetaryUtils.strToMonetary("10.00","EUR"))
        .vatRate(null)
        .build());

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TOLL_COLLECT);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    given(stanziamentiParams.getArticle())
      .willReturn(mock(Article.class));

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(mapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamento.class));

    ProcessingResult processingResult = tollCollectProcessor.validateAndProcess(consumo, bucket);

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }
  
}
