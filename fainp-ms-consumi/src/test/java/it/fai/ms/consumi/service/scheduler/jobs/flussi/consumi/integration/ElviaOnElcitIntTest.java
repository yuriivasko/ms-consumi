package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.atLeastOnce;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.StanziamentoCodeGeneratorStub;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;


@Tag("integration")
public class ElviaOnElcitIntTest
  extends AbstractTelepassIntTest {

  private ElviaJob elviaJob;

  @Inject
  private ElviaProcessor defaultProcessor;

  @Inject
  private ElviaRecordConsumoMapper defaultRecordConsumoMapper;

  @Inject
  private ElviaGenericoProcessor consumoGenericoProcessor;

  @Inject
  private ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper;

  @Inject
  private StanziamentoRepository stanziamentiRepository;

  @Inject
  private RecordPersistenceService recordPersistenceService;

  @Inject
  protected ServiceProviderRepository serviceProviderRepository;

  @Inject
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Inject
  private PlatformTransactionManager transactionManager;

  @Inject
  private NotConfirmedTemporaryAllocationsService notConfirmedAllocationService;

  private ElcitJob elcitJob;

  @Autowired
  private StanziamentoCodeGeneratorStub stanziamentoCodeGeneratorStub;

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentoTestRepository;
  @Inject
  private ConsumoProcessor<Elcit> elcitProcessor;
  @Inject
  private RecordConsumoMapper<ElcitRecord, Elcit> consumoMapperElcit;

  @Inject
  private EntityManager entityManager;



  private static final Logger log = LoggerFactory.getLogger(ElviaOnElcitIntTest.class);

  @BeforeEach
  public void setUp() throws Exception {

    ElviaDescriptor elviaDescriptor = new ElviaDescriptor();
    ElcitDescriptor elcitDescriptor = new ElcitDescriptor();
    super.setUpMockContext("Elvia");

    elviaJob = new ElviaJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      elviaDescriptor,
      defaultProcessor,
      defaultRecordConsumoMapper,
      consumoGenericoProcessor,
      recordConsumoGenericoMapper,
      stanziamentiToNavJmsPublisher,
      notConfirmedAllocationService
    );
    elcitJob = new ElcitJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      elcitDescriptor,
      elcitProcessor,
      consumoMapperElcit,
      stanziamentiToNavJmsPublisher,
      "FILENAME_ASC");
  }

  @Test
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void testCaso3() throws Exception {
    String fileNameElcit = new File(ElviaOnElcitIntTest.class.getResource("/test-files/elvia/int-test-elvia-on-elcit-caso3/DA06A284.ELCIT.FAI.OneToMatch")
      .toURI()).getAbsolutePath();

    List<DettaglioStanziamentoPedaggioEntity> before = dettaglioStanziamentoTestRepository.findAllDettaglioStanziamentiPedaggio();
    assertThat(before).hasSize(0);
    elcitJob.process(fileNameElcit, System.currentTimeMillis(), new Date().toInstant(), null);

    List<DettaglioStanziamentoPedaggioEntity> allDettaglioStanziamentiPedaggio = dettaglioStanziamentoTestRepository.findAllDettaglioStanziamentiPedaggio();
    assertThat(allDettaglioStanziamentiPedaggio).hasSize(2).allMatch(dsp -> dsp.getInvoiceType().equals(InvoiceType.P));

    List<StanziamentoEntity> stanziamentoEntities = dettaglioStanziamentoTestRepository.findAllStanziamenti();

    assertThat(stanziamentoEntities).hasSize(2);
    String fileNameElvia = new File(ElviaOnElcitIntTest.class.getResource("/test-files/elvia/int-test-elvia-on-elcit-caso3/DA06A284.ELVIA.oneToMatch")
      .toURI()).getAbsolutePath();

    elviaJob.process(fileNameElvia, System.currentTimeMillis(), new Date().toInstant(), null);
    stanziamentoEntities = dettaglioStanziamentoTestRepository.findAllStanziamenti();

    assertThat(stanziamentoEntities)
      .extracting(s->tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getStatoStanziamento(), s.getTarga(), s.getPaese(), s.getCosto(),s.getPrezzo(), s.getClasseVeicoloEuro(),s.getValuta()))
      .containsExactlyInAnyOrder(
        tuple("0_P","0000015",null,"ITAPE30",false,InvoiceType.P,"DT641CA","IT",new BigDecimal("0.26230"),new BigDecimal("0.26230"),"A","EUR"),
        tuple("1_P","0067620",null,"ITAPE01",false,InvoiceType.P,"BV650JM","IT",new BigDecimal("0.48361"),new BigDecimal("0.48361"),"V4","EUR"),
        tuple("2_D","0000015","0_P","ITAPE30",false,InvoiceType.D,"DT641CA","IT",new BigDecimal("0.26230"),new BigDecimal("0.26230"),"A","EUR"),
        tuple("3_P","0067620","1_P","ITAPE01",true,InvoiceType.P,"BV650JM","IT",new BigDecimal("0.48361").multiply(new BigDecimal(-1)),new BigDecimal("0.48361").multiply(new BigDecimal(-1)),"V4","EUR")
      );
    allDettaglioStanziamentiPedaggio = dettaglioStanziamentoTestRepository.findAllDettaglioStanziamentiPedaggio();
    List<DettaglioStanziamentoPedaggioEntity> collect = allDettaglioStanziamentiPedaggio.stream().filter(dsp -> dsp.getInvoiceType().equals(InvoiceType.D)).collect(Collectors.toList());
    assertThat(collect)
      .extracting(dsp->tuple(dsp.getArticlesGroup(),dsp.getAmountIncludingVat().setScale(2, RoundingMode.HALF_DOWN),dsp.getInvoiceType()))
      .containsExactlyInAnyOrder(
        tuple("AUT_IT", new BigDecimal("0.32"),InvoiceType.D),
        tuple("AUT_IT",new BigDecimal("0.59"),InvoiceType.D),
        tuple("AUT_IT",new BigDecimal("0.59").multiply(new BigDecimal("-1")),InvoiceType.D)
      );

  }

  @Test
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void test_found_parametri_stanziamenti_for_record_ESE_count() {

    final String elcitFile = getAbsolutePath("/test-files/elvia/int-test-elcit-on-elvia-barriera/DA06A284.ELCIT.FAI.N00002_solo_ESE");

    elcitJob.process(elcitFile, System.currentTimeMillis(), new Date().toInstant(), null);

    List<Stanziamento> stanziamentiElcitList = stanziamentiRepository.getAll();
    assertThat(stanziamentiElcitList)
      .hasSize(2);

    List<DettaglioStanziamentoEntity> dettaglioStanziamentiElcit = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamentiElcit)
      .hasSize(2);

    {
//      ArgumentCaptor<String> notificationCodeCapture = ArgumentCaptor.forClass(String.class);
      ArgumentCaptor<EnrichedNotification> notificationArgumentCaptor = ArgumentCaptor.forClass(EnrichedNotification.class);
      then(notificationService)
        .should(atLeastOnce())
        .notify(notificationArgumentCaptor.capture());

      assertThat(notificationArgumentCaptor.getAllValues().get(0).getCode())
        .doesNotContain("CPV-404");
    }

    final String elviaFile = getAbsolutePath("/test-files/elvia/int-test-elcit-on-elvia-barriera/DA06A284.ELVIA.FAI.N00003_solo_ESE");

    elviaJob.process(elviaFile, System.currentTimeMillis(), new Date().toInstant(), null);
//    non funz: entityManager.flush();

    List<Stanziamento> stanziamentiElviaList = stanziamentiRepository.getAll();
   assertThat(stanziamentiElviaList)
     .extracting(stanziamento -> tuple(stanziamento.getInvoiceType().name(), stanziamento.getPrezzo().toString())) //, stanziamento.getDettaglioStanziamenti().size()))
     .hasSize(6)
     .containsExactlyInAnyOrder(
       tuple("P", "5.09836"), //, 1),
       tuple("P", "0.50000"), //, 1),
       tuple("P", "-5.09836"), //, 1),
       tuple("P", "5.12295"), //, 1),
       tuple("D", "5.12295"), //, 1),
       tuple("D", "0.50000") //, 1)
     );

    List<DettaglioStanziamentoEntity> dettaglioStanziamentiElvia = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamentiElvia)
      .extracting(dettaglio -> {
        DettaglioStanziamentoPedaggioEntity dp = (DettaglioStanziamentoPedaggioEntity) dettaglio;
        return tuple(dp.getInvoiceType().name(), dp.getAmountNoVat().toString()); //, dp.getStanziamenti().size());
      })
      .hasSize(4)
      .containsExactlyInAnyOrder(
        tuple("D", "5.09836"), //, 1),
        tuple("D", "0.50000"), //, 2),
        tuple("D", "5.12295"), //, 3),
        tuple("D", "-5.09836") //, 0)
      );

    {
//      ArgumentCaptor<String> notificationCodeCapture = ArgumentCaptor.forClass(String.class);
      ArgumentCaptor<EnrichedNotification> notificationArgumentCaptor = ArgumentCaptor.forClass(EnrichedNotification.class);
      then(notificationService)
        .should(atLeastOnce())
        .notify(notificationArgumentCaptor.capture());

      assertThat(notificationArgumentCaptor.getAllValues().get(0).getCode())
        .doesNotContain("CPV-404");
    }
  }
}
