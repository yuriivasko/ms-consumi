package it.fai.ms.consumi.service.consumer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.document.allegatifattura.AllegatiFattureDTO;
import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.consumi.client.efservice.EfserviceRestClient;
import it.fai.ms.consumi.client.efservice.dto.ClienteFaiDTO;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.fatturazione.AllegatiConfigurationRepository;
import it.fai.ms.consumi.repository.fatturazione.RequestsCreationAttachmentsRepository;
import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatiFattureRepository;
import it.fai.ms.consumi.service.fatturazione.AllegatiFattureService;
import it.fai.ms.consumi.service.jms.producer.AttachmentInvoiceJmsProducer;

@Tag("unit")
class CreationAttachmentInvoiceConsumerTest {

  private CreationAttachmentInvoiceConsumer creationAttachmentInvoiceConsumer;

  private AllegatiConfigurationRepository       allegatiConfigRepo            = mock(AllegatiConfigurationRepository.class);
  private RequestsCreationAttachmentsRepository requestCreationAttachmentRepo = mock(RequestsCreationAttachmentsRepository.class);
  private EfserviceRestClient                   efServiceRestClient           = mock(EfserviceRestClient.class);

  private AttachmentInvoiceJmsProducer attachmentInvoiceProducer = mock(AttachmentInvoiceJmsProducer.class);

  private StanziamentoRepository stanziamentoRepository = mock(StanziamentoRepository.class);

  private ViewAllegatiFattureRepository viewAllegatiFattureRepo = mock(ViewAllegatiFattureRepository.class);

  private StanziamentiParamsRepository stanziamentiParamRepo = mock(StanziamentiParamsRepository.class);

  private NotificationService notificationService = mock(NotificationService.class);

  AllegatiFattureDTO attachInvoiceDTO;

  private Instant now;

  private final static String ART1 = "::art1::";
  private final static String ART2 = "::art2::";

  private final static String codiceClienteFatturazione = "::codClienteFatturazione::";

  @BeforeEach
  void setUp() throws Exception {
    now = Instant.now();
    attachInvoiceDTO = initAllegatiFattureDto();

    // AttachmentInvoiceJmsProducer attachmentInvoiceProducer = new AttachmentInvoiceJmsProducer(jmsProperties,
    // stanziamentiRepo, viewAllegatiPedaggiService);
    AllegatiFattureService allegatiFattureService = new AllegatiFattureService(allegatiConfigRepo, requestCreationAttachmentRepo,
                                                                               efServiceRestClient, attachmentInvoiceProducer,
                                                                               stanziamentiParamRepo);
    creationAttachmentInvoiceConsumer = new CreationAttachmentInvoiceConsumer(stanziamentoRepository, allegatiFattureService,
                                                                              viewAllegatiFattureRepo, notificationService);
  }

  private AllegatiFattureDTO initAllegatiFattureDto() {
    AllegatiFattureDTO dto = new AllegatiFattureDTO();
    dto.setNumeroFattura("::numFattura::");
    dto.setDataFattura(now);
    dto.setImportoTotale(new BigDecimal(100));
    List<RaggruppamentoArticoliDTO> raggruppamentiArticoli = new ArrayList<>();
    RaggruppamentoArticoliDTO raggr1 = initRaggruppamentoArticoli(ART1);
    RaggruppamentoArticoliDTO raggr2 = initRaggruppamentoArticoli(ART2);
    raggruppamentiArticoli.add(raggr1);
    raggruppamentiArticoli.add(raggr2);
    dto.setRaggruppamentiArticoli(raggruppamentiArticoli);
    return dto;
  }

  private RaggruppamentoArticoliDTO initRaggruppamentoArticoli(String codiceArticolo) {
    RaggruppamentoArticoliDTO raggr = new RaggruppamentoArticoliDTO();
    raggr.setCodice(codiceArticolo);
    raggr.setCodiceClienteFatturazione(codiceClienteFatturazione);
    raggr.setRagioneSocialeClienteFatturazione("::ragioneSociale::");
    List<String> codiciStanziamenti = new ArrayList<>(Arrays.asList(String.join(",", "s1", codiceArticolo),
                                                                    String.join(",", "s2", codiceArticolo)));
    raggr.setCodiciStanziamenti(codiciStanziamenti);
    return raggr;
  }

  @Test
  public void testCodiciNonPresenti() {

    List<String> codici = Arrays.asList("1549922011661P", "1549941847235P", "1549954216809P", "1549954715406P", "1549955176123P",
                                        "1549970485190P");

    Collections.shuffle(codici);

    Set<Stanziamento> stanziamenti = Stream.of("1549922011661P", "1549941847235P", "1549954216809P", "1549954715406P", "1549955176123P")
                                           .map(Stanziamento::new)
                                           .collect(Collectors.toSet());

    Set<Stanziamento> stanziamentiInput = codici.stream()
                                                .map(Stanziamento::new)
                                                .collect(Collectors.toSet());
    stanziamentiInput.removeAll(stanziamenti);

    if (!stanziamentiInput.isEmpty()) {
      stanziamentiInput.forEach(System.out::println);
    }

  }

  @Test
  void test() throws Exception {
    when(viewAllegatiFattureRepo.findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(new ArrayList<>(Arrays.asList(String.join(",",
                                                                                                                                     "s1",
                                                                                                                                     ART1),
                                                                                                                         String.join(",",
                                                                                                                                     "s2",
                                                                                                                                     ART1),
                                                                                                                         String.join(",",
                                                                                                                                     "s1",
                                                                                                                                     ART2),
                                                                                                                         String.join(",",
                                                                                                                                     "s2",
                                                                                                                                     ART2))))).thenReturn(new BigDecimal(100));

    AllegatiConfigurationEntity allegatiConfig1 = new AllegatiConfigurationEntity();
    allegatiConfig1.setGeneraAllegato(true);
    allegatiConfig1.setTipoDettaglioStanziamento(DettaglioStanziamentoType.PEDAGGIO);
    AllegatiConfigurationEntity allegatiConfig2 = new AllegatiConfigurationEntity();
    allegatiConfig2.setGeneraAllegato(true);
    allegatiConfig2.setTipoDettaglioStanziamento(DettaglioStanziamentoType.PEDAGGIO);
    when(allegatiConfigRepo.findByCodiceRaggruppamentoArticoli(ART1)).thenReturn(allegatiConfig1);
    when(allegatiConfigRepo.findByCodiceRaggruppamentoArticoli(ART2)).thenReturn(allegatiConfig2);

    when(efServiceRestClient.findByCodiceCliente(codiceClienteFatturazione)).thenReturn(new ClienteFaiDTO());
    creationAttachmentInvoiceConsumer.consume(attachInvoiceDTO);
    verify(viewAllegatiFattureRepo).findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(new ArrayList<>(Arrays.asList(String.join(",",
                                                                                                                                        "s1",
                                                                                                                                        ART1),
                                                                                                                            String.join(",",
                                                                                                                                        "s2",
                                                                                                                                        ART1),
                                                                                                                            String.join(",",
                                                                                                                                        "s1",
                                                                                                                                        ART2),
                                                                                                                            String.join(",",
                                                                                                                                        "s2",
                                                                                                                                        ART2))));
    verify(allegatiConfigRepo).findByCodiceRaggruppamentoArticoli("::art1::");
    verify(allegatiConfigRepo).findByCodiceRaggruppamentoArticoli("::art2::");
    requestCreationAttachmentRepo.save(Mockito.any());
    // verify(viewAllegatiPedaggiService).createBodyMessageAndSection2(Mockito.any(), Mockito.eq(null));
  }

}
