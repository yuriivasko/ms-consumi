package it.fai.ms.consumi.service.record.dartford;

import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.generici.dartford.Dartford;
import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
class DartfordRecordConsumoGenericoMapperTest {




  @Test
  void test1() throws Exception {
    String filename = "test-file";

    DartfordRecord record =   new DartfordRecord(filename, 1);
    record.setCrossingDate("25/04/2018");
    record.setTime("14:30");
    record.setDirection("Southbound");
    record.setVehicle("FC231VA");
    record.setGroupName("");
    record.setBilledOn("25/04/2018");

    var subject = new DartfordRecordConsumoGenericoMapper();
    Dartford consumo = subject.mapRecordToConsumo(record);
    assertThat(consumo).isNotNull();

    Instant consumoDate = LocalDateTime.parse("2018-04-25T14:30")
                                                              .atZone(ZoneId.of("Europe/Rome"))
                                                              .toInstant();
    assertThat(consumo.getStartDate()).isEqualTo(consumoDate);
    assertThat(consumo.getEndDate()).isEqualTo(consumoDate);

    assertThat(consumo.getCustomer()
                      .getId()).isEqualTo("");

    assertThat(consumo.getLicencePlateOnRecord()).isEqualTo("FC231VA");
    assertThat(consumo.getVehicle().getLicensePlate()).isEqualToComparingFieldByField(new VehicleLicensePlate("FC231VA", VehicleLicensePlate.NO_COUNTRY));

    assertThat(consumo.getAmount().getAmountExcludedVatBigDecimal()).isEqualByComparingTo(BigDecimal.ZERO);
    assertThat(consumo.getAmount().getAmountIncludedVatBigDecimal()).isEqualByComparingTo(BigDecimal.ZERO);
    assertThat(consumo.getAmount().getVatRateBigDecimal()).isEqualByComparingTo(BigDecimal.ZERO);

    assertThat(consumo.getQuantity()).isEqualByComparingTo(BigDecimal.ONE);

    assertThat(consumo.getCustomer().getId()).isEqualTo("");



  }
  @Test
  void test2() throws Exception {
    try {
    String filename = "test-file";

    DartfordRecord record =   new DartfordRecord(filename, 1);
    record.setCrossingDate("24/04/2018");
    record.setTime("10:55");
    record.setDirection("Northbound");
    record.setVehicle("EY164FX");
    record.setGroupName("130016451 ");
    record.setBilledOn("25/04/2018");


    var subject = new DartfordRecordConsumoGenericoMapper();
    Dartford consumo = subject.mapRecordToConsumo(record);
    assertThat(consumo).isNotNull();

    Instant consumoDate = LocalDateTime.parse("2018-04-24T10:55")
                                                              .atZone(ZoneId.of("Europe/Rome"))
                                                              .toInstant();
    assertThat(consumo.getStartDate()).isEqualTo(consumoDate);
    assertThat(consumo.getEndDate()).isEqualTo(consumoDate);

    assertThat(consumo.getCustomer()
                      .getId()).isEqualTo("130016451");
    assertThat(consumo.getContract()).isNotNull();

    assertThat(consumo.getLicencePlateOnRecord()).isEqualTo("EY164FX");
    assertThat(consumo.getVehicle().getLicensePlate()).isEqualToComparingFieldByField(new VehicleLicensePlate("EY164FX", VehicleLicensePlate.NO_COUNTRY));

    assertThat(consumo.getAmount().getAmountExcludedVatBigDecimal()).isEqualByComparingTo(BigDecimal.ZERO);
    assertThat(consumo.getAmount().getAmountIncludedVatBigDecimal()).isEqualByComparingTo(BigDecimal.ZERO);
    assertThat(consumo.getAmount().getVatRateBigDecimal()).isEqualByComparingTo(BigDecimal.ZERO);

    assertThat(consumo.getQuantity()).isEqualByComparingTo(BigDecimal.ONE);


    }catch (Throwable e) {
      e.printStackTrace();
      throw e;
    }

  }


}
