package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTRecordDescriptor;

@Tag("unit")
class LiberTRecordDescriptorTest {

  @Test
  void test() {
    var subject = new LiberTRecordDescriptor();
    String line = "193116;Liber't Pro Exception                             ;2;17;06;;06/06/2017 07:29:18;A8;A8;;LA TURBIE P/V;6;LA TURBIE P/V;20;2;3.60;20;";

    LiberTRecord result = subject.decodeRecordCodeAndCallSetFromString(line, "DEFAULT", "testFile", 1);
    assertThat(result).isNotNull();

    assertThat(result.getNAbbonne()).isEqualTo("193116");
    assertThat(result.getNBadge()).isEqualTo("2");

  }

  @Test
  void testHeader() {
    var subject = new LiberTRecordDescriptor();
    String line1 = "N° Abonné;Libellé Contrat;N°Badge;Année Facturation;Mois Facturation;Date/Heure Entrée;Date/Heure Sortie;Code Autoroute Entrée;Code Autoroute Sortie;N° Gare Entrée;NOM Gare Entrée;N°Gare Sortie;Nom Gare Sortie;Nb Kms;Classe Véhicule;Montant TTC;Taux TVA;";
    String line2 = "193116;Liber't Pro Exception                             ;2;17;06;;06/06/2017 07:29:18;A8;A8;;LA TURBIE P/V;6;LA TURBIE P/V;20;2;3.60;20;";
    assertThat(subject.isHeaderLine(line1)).isTrue();
    assertThat(subject.isHeaderLine(line2)).isFalse();




  }

}
