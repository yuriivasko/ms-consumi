package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.tollcollect;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.verification.Times;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.efservice.message.consumi.AllineamentoDaConsumoMessage;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.TollCollectJob;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.web.rest.util.TestUtils;


@ComponentScan(basePackages = {
  "it.fai.ms.consumi.service",
  "it.fai.ms.consumi.client",
  "it.fai.ms.consumi.adapter",
  "it.fai.ms.consumi.repository",
  "it.fai.ms.consumi.scheduler",
  "it.fai.ms.consumi.domain.dettaglio_stanziamento"},
  excludeFilters = {
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"),
  })
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class TollCollectJobIntTest
  extends AbstractIntegrationTestSpringContext {

  private String fileName = "O_DTC5659_IZ_2_0411032037_S_051117_185190_integration";
  private String filePath = "";


  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @MockBean
  private NotificationService notificationService;

  @Autowired
  private TollCollectJob job;

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentoTestRepository;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;


  @BeforeEach
  public void init() throws Exception {
    filePath = new File(TollCollectJobIntTest.class.getResource("/test-files/tollcollect/" + fileName).toURI()).getAbsolutePath();
    assertThat(job).isNotNull();

  }

  @Test
  public void elaborateTwoRecords_generate_two_stanziamenti_two_dettagli_stanziamento() {

    ServicePartner servicePartner = newServicePartner();

    String result = job.process(filePath, System.currentTimeMillis(), new Date().toInstant(), servicePartner);
    assertThat(result).isNotNull().isEqualTo("TOLL_COLLECT");
    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);
    //IT,FB912DS
    //RO,BH35BRU

//    ArgumentCaptor<Map<String,Object>> notificationArgCaptor = ArgumentCaptor.forClass(Map.class);
//    verify(notificationService, atLeastOnce()).notify(any(), notificationArgCaptor.capture());
//    assertThat(notificationArgCaptor.getAllValues())
//      .extracting(notificationArg -> tuple(notificationArg.get("job_name"), notificationArg.get("original_message")))
//      .containsExactlyInAnyOrder(
//        tuple(Format.TOLL_COLLECT.name(), "Contract 88197 is not active in date 2017-07-17 00:00:00(offset: 24h)"),
//        tuple(Format.TOLL_COLLECT.name(), "Contract 88197 is not active in date 2017-07-18 00:00:00(offset: 24h)"),
//        tuple(Format.TOLL_COLLECT.name(), "Contract 88197 is not active in date 2017-07-19 00:00:00(offset: 24h)")
//      );

    //verify messages
    verify(stanziamentiToNavJmsPublisher).publish(argumentCaptor.capture());
    argumentCaptor.getValue().stream().forEach(sm -> System.out.println(sm));
    List<StanziamentoMessage> expectedMessages = argumentCaptor.getValue();
    assertThat(expectedMessages)
      .hasSize(3)
      .extracting(sm->tuple(sm.getNrCliente(),sm.getTarga(),sm.getPaese(),sm.getCosto(),sm.getPrezzo(),sm.getValuta(),sm.getTipoFlusso(),sm.getGeneraStanziamento(),sm.getStatoStanziamento(),sm.getDataErogazioneServizio()))
      .containsExactlyInAnyOrder(
              tuple("0041015","BH35BRU","DE",46.97d,46.97d,"EUR",1,1,2,LocalDate.parse("2017-07-17")),
              tuple("0041015","BH35BRU","DE",15.37d,15.37d,"EUR",1,1,2,LocalDate.parse("2017-07-18")),
              tuple("0041015","BH35BRU","DE",104.1d,104.1d, "EUR",1,1,2,LocalDate.parse("2017-07-19"))
      );
    List<StanziamentoEntity> stanziamentoEntities = dettaglioStanziamentoTestRepository.findAllStanziamenti();
    assertThat(stanziamentoEntities)
            .hasSize(3)
            .extracting(s->tuple(s.getNumeroCliente(),s.getTarga(),s.getPaese(),s.getPrezzo().doubleValue(),s.getCosto().doubleValue(),s.getValuta(),s.getTipoFlusso(),s.getGeneraStanziamento(),s.getStatoStanziamento(),s.getDataErogazioneServizio()))
            .containsExactlyInAnyOrder(
                    tuple("0041015","BH35BRU","DE",46.97d,46.97d,"EUR",TypeFlow.MYFAI,GeneraStanziamento.COSTO_RICAVO,InvoiceType.D,LocalDate.parse("2017-07-17")),
                    tuple("0041015","BH35BRU","DE",15.37d,15.37d,"EUR",TypeFlow.MYFAI,GeneraStanziamento.COSTO_RICAVO,InvoiceType.D,LocalDate.parse("2017-07-18")),
                    tuple("0041015","BH35BRU","DE",104.1d,104.1d,"EUR",TypeFlow.MYFAI,GeneraStanziamento.COSTO_RICAVO,InvoiceType.D,LocalDate.parse("2017-07-19"))
            );

  List<DettaglioStanziamentoEntity> dettaglioStanziamentoEntities = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
  assertThat(dettaglioStanziamentoEntities)
          .hasSize(3)
          .extracting(ds-> (DettaglioStanziamentoPedaggioEntity)ds)
          .extracting(dsp->tuple(dsp.getVehicleLicenseLicenseId(),dsp.getVehicleLicenseCountryId(),dsp.getAmountExcludedVat(),dsp.getAmountIncludingVat(),dsp.getAmountVatRate(),dsp.getVehicleEuroClass(),dsp.getDevicePan(),dsp.getDeviceObu(),dsp.getDeviceType(),dsp.getArticlesGroup()))
          .containsExactlyInAnyOrder(
                  tuple("BH35BRU","RO", new BigDecimal("46.97000"), new BigDecimal("46.97000"),   new BigDecimal("0.00"), "5", null, "NO_SERIALE", TipoDispositivoEnum.TOLL_COLLECT,"AUT_DE"),
                  tuple("BH35BRU","RO", new BigDecimal("15.37000"), new BigDecimal("15.37000"),   new BigDecimal("0.00"), "5", null, "NO_SERIALE", TipoDispositivoEnum.TOLL_COLLECT,"AUT_DE"),
                  tuple("BH35BRU","RO", new BigDecimal("104.10000"), new BigDecimal("104.10000"), new BigDecimal("0.00"),"5", null, "NO_SERIALE", TipoDispositivoEnum.TOLL_COLLECT,"AUT_DE")
          );


    ArgumentCaptor<AllineamentoDaConsumoMessage> argumentCaptorAllineamento = ArgumentCaptor.forClass(AllineamentoDaConsumoMessage.class);
    verify(allineamentoDaConsumoJmsProducer, new Times(1))
      .sendMessage(argumentCaptorAllineamento.capture());
    assertThat(argumentCaptorAllineamento.getAllValues()).containsExactlyInAnyOrderElementsOf(
      Arrays.asList(
        new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder()
          .licensePlateNumber("BH35BRU")
          .licensePlateNation("RO")
          .deviceType(TipoDispositivoEnum.TOLL_COLLECT)
          .build()
      ));

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    int i=0;
    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso =  dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
          .isNotNull();
      assertThat(dataAcquisizioneFlusso)
          .isNotNull();
      //2017-07-16T22:00:00 17 18
      assertThat(dataAcquisizioneFlusso)
          .isEqualTo(TestUtils.fromStringToInstant("2017-07-17 00:00:00").plus(i, ChronoUnit.DAYS));

      //NON c'è HEADER

      final LocalDate now = LocalDate.now();
      LocalDate date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());
      i++;
    }

  }

  public ServicePartner newServicePartner() {
    ServicePartner servicePartner = new ServicePartner("");
    servicePartner.setPartnerCode("DE01");
    servicePartner.setFormat(Format.TOLL_COLLECT);
    return servicePartner;
  }

  private void assertStanziamento(StanziamentoEntity stanziamentoEntity,
                                  String expectedNrCliente,
                                  String expectedTarga,
                                  String expectedTargaNazione,
                                  BigDecimal expectedCosto,
                                  BigDecimal expectedPrezzo,
                                  String expectedValuta,
                                  TypeFlow expectedTipoFlusso,
                                  GeneraStanziamento expectedGeneraStanziamento,
                                  InvoiceType expectedStatoStanziamento,
                                  LocalDate expectedDataErogazioneServizio) {
    assertThat(stanziamentoEntity.getNumeroCliente()).isEqualTo(expectedNrCliente);
    assertThat(stanziamentoEntity.getTarga()).isEqualTo(expectedTarga);
    assertThat(stanziamentoEntity.getPaese()).isEqualTo(expectedTargaNazione);
    assertThat(stanziamentoEntity.getCosto().setScale(2, RoundingMode.HALF_UP)).isEqualTo(expectedCosto.setScale(2, RoundingMode.HALF_UP));
    assertThat(stanziamentoEntity.getPrezzo().setScale(2, RoundingMode.HALF_UP)).isEqualTo(expectedPrezzo.setScale(2, RoundingMode.HALF_UP));
    assertThat(stanziamentoEntity.getValuta()).isEqualTo(expectedValuta);
    assertThat(stanziamentoEntity.getTipoFlusso()).isEqualTo(expectedTipoFlusso);
    assertThat(stanziamentoEntity.getGeneraStanziamento()).isEqualTo(expectedGeneraStanziamento);
    assertThat(stanziamentoEntity.getStatoStanziamento()).isEqualTo(expectedStatoStanziamento);
    assertThat(stanziamentoEntity.getDataErogazioneServizio()).isEqualTo(expectedDataErogazioneServizio);
  }

  private void assertStanziamentoMessage(StanziamentoMessage stanziamentoMessage,
                                         String expectedNrCliente,
                                         String expectedTarga,
                                         String expectedNazione,
                                         double expectedCosto,
                                         double expectedPrezzo,
                                         String expectedValuta,
                                         int expectedTipoFlusso,
                                         int expectedGeneraStanziamento,
                                         int expectedStatoStanziamento,
                                         LocalDate expectedDataErogazioneServizio) {
    assertThat(stanziamentoMessage.getNrCliente()).isEqualTo(expectedNrCliente);

    assertThat(stanziamentoMessage.getTarga()).isEqualTo(expectedTarga);
    assertThat(stanziamentoMessage.getPaese()).isEqualTo(expectedNazione);
    assertThat(stanziamentoMessage.getCosto()).isEqualTo(expectedCosto);
    assertThat(stanziamentoMessage.getPrezzo()).isEqualTo(expectedPrezzo);
    assertThat(stanziamentoMessage.getValuta()).isEqualTo(expectedValuta);
    assertThat(stanziamentoMessage.getTipoFlusso()).isEqualTo(expectedTipoFlusso);
    assertThat(stanziamentoMessage.getGeneraStanziamento()).isEqualTo(expectedGeneraStanziamento);
    assertThat(stanziamentoMessage.getStatoStanziamento()).isEqualTo(expectedStatoStanziamento);
    assertThat(stanziamentoMessage.getDataErogazioneServizio()).isEqualTo(expectedDataErogazioneServizio);
  }

  private void assertDettaglioStanziamento(DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoEntity,
                                           String expectedVehicleLicenceId,
                                           String expectedVehicleCountryId,
                                           BigDecimal expectedAmountExcludedVat,
                                           BigDecimal expectedAmountIncludedVat,
                                           BigDecimal expectedVat,
                                           String expectedVehicleEuroClass,
                                           String expectedDevicePan,
                                           String expectedDeviceObu,
                                           TipoDispositivoEnum expectedDeviceType,
                                           String expectedArticlesGroup
  ) {
    assertThat(dettaglioStanziamentoEntity.getVehicleLicenseLicenseId()).isEqualTo(expectedVehicleLicenceId);
    assertThat(dettaglioStanziamentoEntity.getVehicleLicenseCountryId()).isEqualTo(expectedVehicleCountryId);
    assertThat(dettaglioStanziamentoEntity.getVehicleEuroClass()).isEqualTo(expectedVehicleEuroClass);
    assertThat(dettaglioStanziamentoEntity.getAmountNoVat().setScale(2,RoundingMode.HALF_EVEN)).isEqualTo(expectedAmountExcludedVat.setScale(2,RoundingMode.HALF_EVEN));
    assertThat(dettaglioStanziamentoEntity.getAmountIncludingVat().setScale(2,RoundingMode.HALF_EVEN)).isEqualTo(expectedAmountIncludedVat.setScale(2,RoundingMode.HALF_EVEN));
    assertThat(dettaglioStanziamentoEntity.getAmountVatRate().setScale(2,RoundingMode.HALF_EVEN)).isEqualTo(expectedVat.setScale(2,RoundingMode.HALF_EVEN));

    assertThat(dettaglioStanziamentoEntity.getArticlesGroup()).isEqualTo(expectedArticlesGroup);

    assertThat(dettaglioStanziamentoEntity.getDevicePan()).isEqualTo(expectedDevicePan);
    assertThat(dettaglioStanziamentoEntity.getDeviceObu()).isEqualTo(expectedDeviceObu);
    assertThat(dettaglioStanziamentoEntity.getDeviceType()).isEqualTo(expectedDeviceType);
  }
}
