package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiJollyRecord;

@Tag("unit")
class TrackyCardCarburantiJollyDescriptorTest {

  private TrackyCardCarburantiJollyDescriptor trackyCardCarburantiJollyDescriptor;
  private String[] data;

  @BeforeEach
  public void setUp(){
    data = new String[4];
    data[0] = "H000001AT04      20170418092137                                                                                                                                                                        ";
    data[1] = "D000002AT0400000178969700045349000732017041806250003040002000000000106925000001011930000000000000000       AT04      IT AA000AA             6                                                          ";
    trackyCardCarburantiJollyDescriptor = new TrackyCardCarburantiJollyDescriptor();
  }


  @Test
  public void getStartEndPosTotalRows() {
    assertThat(trackyCardCarburantiJollyDescriptor.getStartEndPosTotalRows().get(3))
      .isEqualTo(7);
  }

  @Test
  public void getHeaderBegin() {
    assertThat(trackyCardCarburantiJollyDescriptor.getHeaderBegin())
      .isEqualTo("H0");
  }

  @Test
  public void getFooterCodeBegin() {
    assertThat(trackyCardCarburantiJollyDescriptor.getFooterCodeBegin())
      .isEqualTo("T0");
  }

  @Test
  public void getLinesToSubtractToMatchDeclaration() {
    assertThat(trackyCardCarburantiJollyDescriptor.getLinesToSubtractToMatchDeclaration())
      .isEqualTo(0);
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_header() {
     TrackyCardCarburantiJollyRecord record = trackyCardCarburantiJollyDescriptor.decodeRecordCodeAndCallSetFromString(data[0], "H0", "", -1);
     assertThat(record.getRecordCode())
      .isEqualTo("H0");
    assertThat(record.getPartnerCode())
      .isEqualTo("");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_record() {
    TrackyCardCarburantiJollyRecord record = trackyCardCarburantiJollyDescriptor.decodeRecordCodeAndCallSetFromString(data[1], "D0", "", -1);
    assertThat(record.getRecordCode())
      .isEqualTo("D0");
    assertThat(record.getPointCode())
      .isEqualTo("AT04000001");
    assertThat(record.getProductCode())
      .isEqualTo("00001");
    assertThat(record.getCustomerCode())
      .isEqualTo("AT04");
    assertThat(record.getLicencePlate())
      .isEqualTo("AA000AA");
    assertThat(record.getEuroClass())
      .isEqualTo("6");
  }
}
