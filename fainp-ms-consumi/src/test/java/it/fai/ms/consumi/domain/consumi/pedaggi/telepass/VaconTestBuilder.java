package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import java.time.Instant;
import java.util.Optional;

import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;

public class VaconTestBuilder {

  private VaconTestBuilder() {
  }

  public static VaconTestBuilder newInstance() {
    return new VaconTestBuilder();
  }

  public Vacon build() {
    var vacon = new Vacon(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);
    return vacon;
  }

}
