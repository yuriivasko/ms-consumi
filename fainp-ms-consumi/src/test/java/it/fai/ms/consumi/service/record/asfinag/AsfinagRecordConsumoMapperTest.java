package it.fai.ms.consumi.service.record.asfinag;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.Asfinag;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.AsfinagRecord;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import static java.time.temporal.ChronoField.*;
import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
class AsfinagRecordConsumoMapperTest {

  AsfinagRecordConsumoMapper asfinagRecordConsumoMapper  = new AsfinagRecordConsumoMapper();
  private AsfinagRecord asfinagRecord;

  @BeforeEach
  public void setUp(){
    asfinagRecord = newAsfinagRecord();
  }

  private AsfinagRecord newAsfinagRecord() {
    AsfinagRecord asfinagRecord = new AsfinagRecord("LEPTCTTR.999", -1);
    //2027  9999           066520180227 000000007896970004662900184   2018022401590000220000000000000010000000096530000008044009321448384        C04001000114254628       20000000001609
    asfinagRecord.setRecordCode("20");
    asfinagRecord.setIngestion_time(Instant.EPOCH);
    asfinagRecord.setTransactionDetail("27");
    asfinagRecord.setNumeroCorsa("0665");
    asfinagRecord.setDataCreazioneFile("20180227");
    asfinagRecord.setNumeroBollaConsegna("00000000");
    asfinagRecord.setNumeroCarta("7896970004662900184");
    asfinagRecord.setDataConsegna("20180224");
    asfinagRecord.setTempoConsegna("0159");
    asfinagRecord.setTipoTransazione("00");
    asfinagRecord.setCodiceProdotto("0022");
    asfinagRecord.setKm("0000000");
    asfinagRecord.setDriverId("0000");
    asfinagRecord.setQuantitaProdotto("000100");
    asfinagRecord.setImportoIvaIncl("0000009653");
    asfinagRecord.setImportoIvaEscl("0000008044");
    asfinagRecord.setNumeroRicevuta("009321448384");
    asfinagRecord.setCampoInformazione("C04001000114254628");
    asfinagRecord.setAliquotaIva("2000");
    asfinagRecord.setImportoIva("0000001609");

    asfinagRecord.setCreationDateInFile("20180227");
    asfinagRecord.setCreationTimeInFile("0216");

    asfinagRecord.setRecordCode("20");
    return asfinagRecord;
  }

  @Test
  void mapRecordToConsumo() throws Exception {
    Asfinag consumo = asfinagRecordConsumoMapper.mapRecordToConsumo(newAsfinagRecord());
    assertThat(consumo.getSource().getAcquisitionDate())
      .isEqualTo(Instant.parse("2018-02-27T01:16:00Z"));
    assertThat(consumo.getAdditionalInfo())
      .isEqualTo("009321448384");
    assertThat(consumo.getAmount().getAmountExcludedVat())
      .isEqualTo(Money.of(new BigDecimal("80.44"), "EUR"));
    assertThat(consumo.getNavSupplierCode())
      .isNull();
    assertThat(consumo.getCompensationNumber())
      .isNull();
    assertThat(consumo.getContract().isPresent())
      .isFalse();
    assertThat(consumo.getDate())
      .isEqualTo(calculateInstantByDateAndTime("20180224", "0159", "yyyyMMdd", "HHmm").get());

    assertThat(consumo.getDevice().getPan())
      .isEqualTo("7896970004662900184");

    assertThat(consumo.getDevice().getServiceType())
      .isEqualTo(TipoServizioEnum.PEDAGGI_AUSTRIA);

    assertThat(consumo.getEntryDate())
      .isNull();
    assertThat(consumo.getEntryTime())
      .isNull();
    assertThat(consumo.getEseTotStatus())
      .isNull();
    assertThat(consumo.getExitTransitDateTime())
      .isNull();
    assertThat(consumo.getGlobalIdentifier().getId())
      .isNotNull();
    assertThat(consumo.getGroupArticlesNav())
      .isNull();
    assertThat(consumo.getImponibileAdr())
      .isNull();
    assertThat(consumo.getInvoiceType())
      .isEqualTo(InvoiceType.D);
    assertThat(consumo.getLaneId())
      .isNull();
    assertThat(consumo.getNetworkCode())
      .isNull();
    assertThat(consumo.getPartnerCode())
      .isNull();
    assertThat(consumo.getProcessed())
      .isFalse();
    assertThat(consumo.getRecordCode())
      .isEqualTo("20");
    assertThat(consumo.getRecordCodeToLookInParamStanz())
      .isEqualTo("20");
    assertThat(consumo.getRegion())
      .isNull();
    assertThat(consumo.getRoadType())
      .isNull();
    assertThat(consumo.getRoute())
      .isNull();
    //FIXME esce sempre " - "
//    assertThat(consumo.getRouteName())
//      .isNull();
    assertThat(consumo.getServicePartner())
      .isNull();
    assertThat(consumo.getSource().getFileName())
      .isEqualTo("LEPTCTTR.999");
    assertThat(consumo.getSource().getIngestionTime())
      .isEqualTo(Instant.parse("1970-01-01T00:00:00Z"));
    assertThat(consumo.getSource().getRowNumber())
      .isEqualTo(-1);
    assertThat(consumo.getSource().getType())
      .isEqualTo("ASFINAG");
//    assertThat(consumo.getSourceType())
//      .isEqualTo("");
    assertThat(consumo.getSplitNumber())
      .isNull();
    assertThat(consumo.getTollCharger())
      .isNull();
    assertThat(consumo.getTollGate())
      .isNull();
    assertThat(consumo.getTollPointEntry())
      .isNull();
    assertThat(consumo.getTollPointExit().getGlobalGate())
      .isNull();
    assertThat(consumo.getTollPointExit().getTime())
      .isEqualTo(calculateInstantByDateAndTime("20180224", "0159", "yyyyMMdd", "HHmm").get());
    assertThat(consumo.getTotalAmount())
      .isNull();
    assertThat(consumo.getTransaction().getSign())
      .isEqualTo("+");
    assertThat(consumo.getTransaction().getDetailCode())
      .isEqualTo("27");
//    assertThat(consumo.getTransactionDetailCode())
//      .isEqualTo("");
    assertThat(consumo.getTspExitCountryCode())
      .isNull();
    assertThat(consumo.getTspExitNumber())
      .isNull();
    assertThat(consumo.getTspRespCountryCode())
      .isNull();
    assertThat(consumo.getTspRespNumber())
      .isNull();
    assertThat(consumo.getAmount().getVatRateBigDecimal())
      .isEqualTo(new BigDecimal("20").setScale(2, RoundingMode.HALF_UP));
//    assertThat(consumo.getVatRateBigDecimal())
//      .isEqualTo(new BigDecimal("20").setScale(2, RoundingMode.HALF_UP));
    assertThat(consumo.getVehicle())
      .isNull();
  }


  public static String formatInstantToDate(Instant instant) {
    DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
    LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    return formatter.format(ldt);
  }

  public static Instant parseDateToInstant(String time) {

    DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;

    LocalDateTime ldt = LocalDateTime.from(formatter.parse(time));
    return ldt.toInstant(ZoneOffset.UTC);
  }

  public static String formatInstantToHour(Instant instant) {

    DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendValue(HOUR_OF_DAY, 2)
                                                                .appendValue(MINUTE_OF_HOUR, 2)
                                                                .optionalStart()
                                                                .appendValue(SECOND_OF_MINUTE, 2)
                                                                .toFormatter();

    LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    return formatter.format(ldt);
  }

  public static Instant parseTimeToInstant(String time) {

    DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendValue(HOUR_OF_DAY, 2)
                                                                .appendValue(MINUTE_OF_HOUR, 2)
                                                                .optionalStart()
                                                                .appendValue(SECOND_OF_MINUTE, 2)
                                                                .toFormatter();

    LocalDateTime ldt = LocalDateTime.from(formatter.parse(time));
    return ldt.toInstant(ZoneOffset.UTC);
  }

  protected Optional<Instant> calculateInstantByDateAndTime(final String entryDate, final String entryTime, final String datePattern,
                                                            final String timePattern) throws ParseException {
    if (entryDate != null && !entryDate.isEmpty()) {
      if (entryTime != null && !entryTime.isEmpty() && (timePattern == null || timePattern.isEmpty()))
        throw new IllegalStateException("entryTime specified required not-null timepattern");
      String completePattern = datePattern
                               + (entryTime != null && !entryTime.isEmpty() && timePattern != null && !timePattern.isEmpty() ? timePattern
                                                                                                                             : "");
      String completeDateAndTime = entryDate + (entryTime != null ? entryTime : "");
      var dateFormat = new SimpleDateFormat(completePattern);
      var date = dateFormat.parse(completeDateAndTime);
      if (date.after(maxDate())) {
        return Optional.of(maxDate().toInstant());
      } else {
        return Optional.of(date.toInstant());
      }
    } else {
      return Optional.ofNullable(null);
    }
  }

  private Date maxDate() {
    final var calendar = GregorianCalendar.getInstance();
    calendar.set(9999, 11, 30, 23, 59);
    return calendar.getTime();
  }
}
