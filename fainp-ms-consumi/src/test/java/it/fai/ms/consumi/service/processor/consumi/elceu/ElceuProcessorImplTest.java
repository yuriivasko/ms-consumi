package it.fai.ms.consumi.service.processor.consumi.elceu;

import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcome;
import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcomeFailure;
import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcomeFailureWithFatalError;
import static java.time.Instant.ofEpochSecond;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElceuTestBuilder;
import it.fai.ms.consumi.repository.CaselliEntityRepository;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
class ElceuProcessorImplTest {

  // todo verificare customer service

  private final ConsumoValidatorService mockConsumoValidatorService = mock(ConsumoValidatorService.class);
  private final CustomerService         mockCustomerService         = mock(CustomerService.class);
  private final StanziamentiProducer    mockStanziamentiProducer    = mock(StanziamentiProducer.class);
  private final VehicleService          mockVehicleService          = mock(VehicleService.class);
  private final CaselliEntityRepository mockCaselliRepository       = mock(CaselliEntityRepository.class);
  private ConsumoProcessor<Elceu>       processor;

  private Vehicle newVehicle() {
    var vehicle = new Vehicle();
    vehicle.setEuroClass("::euroClass::");
    vehicle.setFareClass("");
    vehicle.setLicensePlate(new VehicleLicensePlate("::licenseId::", "::countryId::"));
    return vehicle;
  }

  @BeforeEach
  void setUp() throws Exception {
    processor = new ElceuProcessorImpl(mockConsumoValidatorService, mockStanziamentiProducer,
                                       new ElceuConsumoDettaglioStanziamentoPedaggioMapper(mockCaselliRepository), mockVehicleService,
                                       mockCustomerService);
  }

  @Test
  void when_validationOutcome_isFalse_blocking() {

    // given

    final var elceu = ElceuTestBuilder.newInstance()
                                      .build();
    elceu.setVehicle(Vehicle.newEmptyVehicleWithFareClass(""));

    final var bucket = new Bucket("::bucketId::");

    given(mockConsumoValidatorService.validate(elceu)).willReturn(newValidationOutcomeFailureWithFatalError());

    // when

    final var processingResult = processor.validateAndProcess(elceu, bucket);

    // then

    then(mockConsumoValidatorService).should().validate(elceu);

    then(mockStanziamentiProducer).shouldHaveZeroInteractions();

    then(mockConsumoValidatorService).should().notifyProcessingResult(any());

    then(mockVehicleService).shouldHaveZeroInteractions();

    assertThat(processingResult.getConsumo()).isEqualTo(elceu);
    assertThat(processingResult.getValidationOutcome()
                               .isValidationOk()).isFalse();
    assertThat(processingResult.getValidationOutcome()
                               .isFatalError()).isTrue();
  }

  @Test
  void when_validationOutcome_isFalse_warning() {

    // given

    final var elceu = ElceuTestBuilder.newInstance()
                                      .withTransaction()
                                      .withDevice()
                                      .withTollPointExit()
                                      .withNavSupplier()
                                      .build();
    elceu.setVehicle(Vehicle.newEmptyVehicleWithFareClass(""));

    final var bucket = new Bucket("::bucketId::");

    given(mockConsumoValidatorService.validate(elceu)).willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    given(mockVehicleService.newVehicleByVehicleUUID_WithFareClass(new Device("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO), elceu.getTollPointExit().getTime(), "")).willReturn(Optional.of(newVehicle()));

//    given(mockCustomerService.findCustomerByDeviceSeriale("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO)).willReturn(Optional.of(new Customer("::customerId::")));

    // when

    final ProcessingResult processingResult = processor.validateAndProcess(elceu, bucket);

    // then

    then(mockConsumoValidatorService).should().validate(elceu);

    then(mockStanziamentiProducer).should()
                                  .produce(any(), eq(StanziamentiParamsTestBuilder.newInstance()
                                                                                  .withSupplierCode("::supplierCode::")
                                                                                  .withArticle(ArticleTestBuilder.newInstance()
                                                                                                                 .withCode("::code::")
                                                                                                                 .withGrouping("::grouping::")
                                                                                                                 .build())
                                                                                  .build()));
    then(mockStanziamentiProducer).shouldHaveNoMoreInteractions();

    then(mockConsumoValidatorService).should().notifyProcessingResult(any());

    then(mockVehicleService).should()
                            .newVehicleByVehicleUUID_WithFareClass(elceu.getDevice(), elceu.getTollPointExit().getTime(), "");
    then(mockVehicleService).shouldHaveNoMoreInteractions();

//    then(mockCustomerService).should().findCustomerByDeviceSeriale("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO);

    then(mockCustomerService).shouldHaveNoMoreInteractions();

    assertThat(processingResult.getConsumo()).isEqualTo(elceu);
    assertThat(processingResult.getValidationOutcome()
                               .isValidationOk()).isFalse();
    assertThat(processingResult.getValidationOutcome()
                               .isFatalError()).isFalse();

  }

  @Test
  void when_validationOutcome_isTrue() {

    // given

    final var elceu = ElceuTestBuilder.newInstance()
                                      .withTransaction()
                                      .withDevice()
                                      .withTollPointExit()
                                      .withNavSupplier()
                                      .build();
    elceu.setVehicle(Vehicle.newEmptyVehicleWithFareClass(""));

    final var bucket = new Bucket("::bucketId::");

    given(mockConsumoValidatorService.validate(elceu)).willReturn(newValidationOutcome());

    given(mockVehicleService.newVehicleByVehicleUUID_WithFareClass(new Device("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO), ofEpochSecond(0), "")).willReturn(Optional.of(newVehicle()));

//    given(mockCustomerService.findCustomerByDeviceSeriale("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO)).willReturn(Optional.of(new Customer("::customerId::")));

    // when

    final var processingResult = processor.validateAndProcess(elceu, bucket);

    // then

    then(mockConsumoValidatorService).should()
                                     .validate(elceu);
    then(mockConsumoValidatorService).shouldHaveNoMoreInteractions();

    then(mockConsumoValidatorService).should(times(0)).notifyProcessingResult(any());

    then(mockVehicleService).should()
                            .newVehicleByVehicleUUID_WithFareClass(elceu.getDevice(), elceu.getTollPointExit().getTime(), "");
    then(mockVehicleService).shouldHaveNoMoreInteractions();

//    then(mockCustomerService).should().findCustomerByDeviceSeriale("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO);

    then(mockCustomerService).shouldHaveNoMoreInteractions();

    assertThat(processingResult.getConsumo()).isEqualTo(elceu);
    assertThat(processingResult.getValidationOutcome()
                               .isValidationOk()).isTrue();
    assertThat(processingResult.getValidationOutcome()
                               .isFatalError()).isFalse();
  }

}
