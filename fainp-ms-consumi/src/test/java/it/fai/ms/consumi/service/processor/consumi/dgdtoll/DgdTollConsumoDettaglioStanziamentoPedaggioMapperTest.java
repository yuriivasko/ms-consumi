package it.fai.ms.consumi.service.processor.consumi.dgdtoll;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdTollGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;

@Tag("unit")
class DgdTollConsumoDettaglioStanziamentoPedaggioMapperTest {

  DgdTollConsumoDettaglioStanziamentoPedaggioMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new DgdTollConsumoDettaglioStanziamentoPedaggioMapper();
  }

  @Test
  void mapConsumoToDettaglioStanziamento() {
    final DgdToll consumo = mock(DgdToll.class);

    given(consumo.getGlobalIdentifier())
      .willReturn(new DgdTollGlobalIdentifier(""));

    given(consumo.getNavSupplierCode())
      .willReturn("");

    DettaglioStanziamento dettaglioStanziamento = mapper.mapConsumoToDettaglioStanziamento(consumo);

    //FIXME dettagliare test

    assertThat(dettaglioStanziamento)
      .isNotNull();
  }
}
