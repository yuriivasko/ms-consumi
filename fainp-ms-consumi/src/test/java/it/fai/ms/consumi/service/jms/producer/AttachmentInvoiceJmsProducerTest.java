package it.fai.ms.consumi.service.jms.producer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.consumi.AbstractCommonTest;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;
import it.fai.ms.consumi.repository.device.TipoSupporto;
import it.fai.ms.consumi.repository.device.TipoSupportoRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepositoryImpl;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepositoryImpl;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2EntityBuilder;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Repository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2RepositoryImpl;
import it.fai.ms.consumi.service.ViewAllegatiGenericoService;
import it.fai.ms.consumi.service.impl.ViewAllegatiGenericoServiceImpl;
import it.fai.ms.consumi.service.jms.producer.invoice.GenericoInvoiceAttachment;
import it.fai.ms.consumi.service.jms.producer.invoice.IGenerateInvoiceAttachment;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;

@ExtendWith(SpringExtension.class)
@DataJpaTest(showSql = false)
@Tag("integration")
public class AttachmentInvoiceJmsProducerTest extends AbstractCommonTest {

  @Autowired EntityManager em;

  @Autowired JmsProperties jmsProperties;

  StanziamentiParamsRepository stanziamentiParamsRepository;
  ViewAllegatoGenericoSezione2Repository attachmentRepository;
  IGenerateInvoiceAttachment attachment;

  ViewAllegatiGenericoService service;

  DettaglioStanziamantiRepository repository;

  StanziamentoEntity s;
  DettaglioStanziamentoGenericoEntity ds;

  List<String> codiciStanziamenti = new ArrayList<String>();
  List<ViewAllegatoGenericoSezione2Entity> viewData2;
  String contratto1;
  Set<String> viewData1;

  static String CONTRACT_CODE = "::contractCode" + RandomString();
  static String STANZIAMENTO_CODE = RandomString();

  RequestsCreationAttachments requestCreationAttachments;
  RaggruppamentoArticoliDTO raggruppamentoArticoliDTO;

  AttachmentInvoiceJmsProducer jmsProducer;
  StanziamentoRepository stanziamentiRepo;
  StanziamentoEntityMapper stanziamentoMapper;
  
  private PetrolPumpService                          petroPumpServiceMock     = mock(PetrolPumpService.class);
  private TipoSupportoRepository                        tipoSupportoRepoMock  = mock(TipoSupportoRepository.class);
  
  private PointDTO point;

  @BeforeEach
  void setUp() throws Exception {
    point = new PointDTO();
    point.setName("pointName");
    point.setCity("CityPoint");
    point.setCode("CodePoint");
    when(petroPumpServiceMock.getPoint(any())).thenReturn(point);
    TipoSupporto tipoSupporto = newTipoSupporto("::TipoSupporto_Mocked::");
    when(tipoSupportoRepoMock.findByTipoDispositivo(any())).thenReturn(tipoSupporto);
    
    attachmentRepository = Mockito.mock(ViewAllegatoGenericoSezione2RepositoryImpl.class);

    stanziamentiParamsRepository = new StanziamentiParamsRepositoryImpl(em, new StanziamentiParamsEntityMapper());

    service = new ViewAllegatiGenericoServiceImpl(attachmentRepository, stanziamentiParamsRepository, petroPumpServiceMock, tipoSupportoRepoMock);

    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
                                                                                  new DettaglioStanziamentoGenericoEntityMapper(),
                                                                                  new DettaglioStanziamentoPedaggioEntityMapper(),
                                                                                  new DettaglioStanziamentoTrenoEntityMapper());
    repository = new DettaglioStanziamentiRepositoryImpl(em, dettaglioStanziamentoEntityMapper,
                                                         new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper));

    attachment = new GenericoInvoiceAttachment(service,repository);

    stanziamentoMapper = new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper);
    stanziamentiRepo = new StanziamentoRepositoryImpl(em, stanziamentoMapper, dettaglioStanziamentoEntityMapper, new StanziamentoCodeGenerator());

    codiciStanziamenti = new ArrayList<String>();
    codiciStanziamenti.add(STANZIAMENTO_CODE);

    contratto1 = CONTRACT_CODE;

    viewData1 = new HashSet<String>();
    viewData1.add(contratto1);

    viewData2 = new ArrayList<ViewAllegatoGenericoSezione2Entity>();
    viewData2.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), null));
    viewData2.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), null));
    
    requestCreationAttachments = Mockito.mock(RequestsCreationAttachments.class);
    when(requestCreationAttachments.getCodiceFattura()).thenReturn(RandomNumeric(4));
    when(requestCreationAttachments.getCodiceRaggruppamentoArticolo()).thenReturn(RandomString(4));
    when(requestCreationAttachments.getDataFattura()).thenReturn(Instant.now());
    when(requestCreationAttachments.getNomeIntestazioneTessere()).thenReturn(RandomString());
    when(requestCreationAttachments.getDescrRaggruppamentoArticolo()).thenReturn(RandomString());
    when(requestCreationAttachments.getLang()).thenReturn("IT");

    raggruppamentoArticoliDTO = Mockito.mock(RaggruppamentoArticoliDTO.class);
    when(raggruppamentoArticoliDTO.getCodiceClienteFatturazione()).thenReturn(RandomString(6));
    when(raggruppamentoArticoliDTO.getCodiciStanziamenti()).thenReturn(codiciStanziamenti);
    when(raggruppamentoArticoliDTO.getRagioneSocialeClienteFatturazione()).thenReturn(RandomString());
    when(raggruppamentoArticoliDTO.getCodice()).thenReturn(RandomString());

    jmsProducer = new AttachmentInvoiceJmsProducer(jmsProperties, stanziamentiRepo, Arrays.asList(attachment));
  }

  private TipoSupporto newTipoSupporto(String tipoSupportoFix) {
    TipoSupporto tipoSupporto = new TipoSupporto();
    tipoSupporto.setTipoSupportoFix(tipoSupportoFix);
    return tipoSupporto;
  }

  @Test
  public void generateAttachmentInvoiceAndSendToQueue() throws Exception {
    jmsProducer.generateAttachmentInvoiceAndSendToQueue(raggruppamentoArticoliDTO, DettaglioStanziamentoType.GENERICO,  requestCreationAttachments, TemplateAllegati.NO_DISPOSITIVO);
  }

  private ViewAllegatoGenericoSezione2Entity addViewEntity(String contractCode, String id, String tipoDispositivo) {
    ViewAllegatoGenericoSezione2Entity e = new ViewAllegatoGenericoSezione2EntityBuilder().createViewAllegatoGenericoSezione2Entity();
    e.setId(id);
    e.setCodiceContratto(contractCode);
    e.setDataFine(LocalDateTime.now());
    e.setDataInizio(LocalDateTime.now());
    e.setDescrizioneAggiuntiva(RandomString());
    e.setPercIva(22.0);
    e.setQuantita(1.0);
    e.setImporto(RandomDouble());
    e.setImponibile(RandomDouble());
    e.setCodiceProdotto("PK001");
    e.setSerialNumber(RandomString());
    e.setPanNumber("");
    e.setTargaVeicolo(RandomLicense());
    e.setNazioneVeicolo("IT");
    e.setClassificazioneEuro("E");
    e.setTipoDispositivo(tipoDispositivo);
    e.setTratta(RandomString());
    e.setValuta("EUR");
    e.setCambio(RandomDouble());
    e.setClasseTariffaria("D");
    e.setCodiceFornitoreNav("F007227");
    e.setDocumentoDaFornitore("doc_fornitore");
    e.setPuntoErogazione("::puntoErogazione::");
    e.setRecordCode("DEFAULT");
    e.setRaggruppamentoArticoli(RandomString());
    return e;
  }

}
