package it.fai.ms.consumi.service.processor.consumi.hgv;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.AmountBuilder;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.generico.DettaglioStanziamentoGenericoProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
class HgvProcessorImplTest {

  HgvProcessorImpl hgvProcessor;

  private final ConsumoValidatorService                      consumoValidatorService = mock(ConsumoValidatorService.class);
  private final DettaglioStanziamentoGenericoProcessor       stanziamentiProducer    = mock(DettaglioStanziamentoGenericoProcessor.class);
  private final HgvConsumoDettaglioStanziamentoGenricoMapper mapper                  = mock(HgvConsumoDettaglioStanziamentoGenricoMapper.class);
  private final VehicleService                               vehicleService          = mock(VehicleService.class);
  private final CustomerService                              customerService         = mock(CustomerService.class);

  @BeforeEach
  public void setUp() {
    hgvProcessor = new HgvProcessorImpl(consumoValidatorService, stanziamentiProducer, mapper, vehicleService, customerService);
  }

  @Test
  void validateAndProcess_if_fail_blocking_validation_will_sikp_stanziamento_production() {
    Hgv consumo = mock(Hgv.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(consumo.getAmount())
      .willReturn(AmountBuilder.builder().amountExcludedVat(MonetaryUtils.strToMonetary("10.00", "EUR")).build());

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.HGV);

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isFatalError())
      .willReturn(true);

    hgvProcessor.validateAndProcess(consumo, bucket);

    then(stanziamentiProducer)
      .shouldHaveZeroInteractions();
  }

  @Test
  void validateAndProcess_if_fail_waring_validation_will_produce_notification_and_stanziamentos() {
    Hgv consumo = mock(Hgv.class);
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(consumo.getAmount())
      .willReturn(AmountBuilder.builder().amountExcludedVat(MonetaryUtils.strToMonetary("10.00", "EUR")).build());

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.HGV);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    Bucket bucket = new Bucket("");

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet<>(Arrays.asList(new Message("").withText(""))));

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    given(stanziamentiParams.getArticle())
      .willReturn(mock(Article.class));

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(mapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamentoGenerico.class));

    ProcessingResult processingResult = hgvProcessor.validateAndProcess(consumo, bucket);

    then(consumoValidatorService)
    .should(times(1))
    .notifyProcessingResult(any());

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }

  @Test
  void validateAndProcess_if_success_validation_will_produce_stanziamentos() {
    Hgv consumo = mock(Hgv.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(consumo.getAmount())
      .willReturn(AmountBuilder.builder().amountExcludedVat(MonetaryUtils.strToMonetary("10.00", "EUR")).build());

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.HGV);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    given(stanziamentiParams.getArticle())
      .willReturn(mock(Article.class));

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(mapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamentoGenerico.class));

    ProcessingResult processingResult = hgvProcessor.validateAndProcess(consumo, bucket);

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }
}
