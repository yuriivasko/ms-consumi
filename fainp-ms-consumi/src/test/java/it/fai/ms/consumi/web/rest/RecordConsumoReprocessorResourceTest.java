package it.fai.ms.consumi.web.rest;

import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.consumo.model.telepass.DummyConsumo;
import it.fai.ms.consumi.repository.flussi.record.model.DummyRecord;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.RecordConsumoReprocessorService;
import it.fai.ms.consumi.service.processor.RecordNotReprocessedException;
import it.fai.ms.consumi.service.processor.SingleRecordProcessingResult;
import it.fai.ms.consumi.service.record.RecordConsumoReprocessor;
import it.fai.ms.consumi.web.rest.errors.CustomException;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@Tag("unit")
class ORecordConsumoReprocessorResourceTest {

  private ElasticSearchFeignClient esClient = mock(ElasticSearchFeignClient.class);
  private RecordConsumoReprocessor recordConsumoReprocessor = mock(RecordConsumoReprocessor.class);
  private RecordConsumoReprocessorService recordConsumoReprocessorService = new RecordConsumoReprocessorService(esClient, recordConsumoReprocessor);

  RecordConsumoReprocessorResource recordConsumoReprocessorResource = new RecordConsumoReprocessorResource(recordConsumoReprocessorService);

  @Test
  void reprocessRecord_not_found_should_return_error(){

    given(esClient.getRecord(Mockito.any(), Mockito.any()))
      .willReturn(null);


    assertThrows(CustomException.class,
      () -> recordConsumoReprocessorResource.reprocessRecord("pippo", "pippo"),
      "It should throw RecordNotFoundException");
  }

  @Test
  void reprocessRecord_with_processing_errors_should_return_error(){

    ElasticSearchRecord<DummyRecord> record = new ElasticSearchRecord<>();
    record.setRawdata(new DummyRecord());

    given(esClient.getRecord(Mockito.any(), Mockito.any()))
      .willAnswer(invocationOnMock -> {
        return record;
      });

    given(recordConsumoReprocessor.processSingleRecord(record.getRawdata()))
      .willThrow(new RecordNotReprocessedException(record.getRawdata()));

    assertThrows(CustomException.class,
      () -> recordConsumoReprocessorResource.reprocessRecord("pippo", "pippo"),
      "It should throw RecordNotFoundException");
  }

  @Test
  void reprocessRecord_without_error_should_return_ok() throws CustomException {

    ElasticSearchRecord<DummyRecord> record = new ElasticSearchRecord<>();
    record.setRawdata(new DummyRecord());

    given(esClient.getRecord(Mockito.any(), Mockito.any()))
      .willAnswer(invocationOnMock -> {
        return record;
      });

    given(recordConsumoReprocessor.processSingleRecord(record.getRawdata()))
      .willReturn(new SingleRecordProcessingResult(new ProcessingResult(
        new DummyConsumo(
          new Source(Instant.now(), Instant.now(), ""),
          "",
          Optional.of(GlobalIdentifierTestBuilder.newInstance().internal().build()),
          record.getRawdata()
          ),
          new ValidationOutcome())));

    recordConsumoReprocessorResource.reprocessRecord("pippo", "pippo");
  }
}
