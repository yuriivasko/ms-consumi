package it.fai.ms.consumi.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.repository.device.DeviceRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoStatoServizioRepository;
import it.fai.ms.consumi.repository.storico_dml.ViewStoricoDispositivoVeicoloContrattoRepository;

@Tag("unit")
class DeviceServiceImplTest {

  private static Device newDevice() {
    final var device = new Device("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO);
    device.setPan("::pan::");
    return device;
  }

  private       DeviceService                                    deviceService;
  private final DeviceRepository                                 mockDeviceRepository = mock(DeviceRepository.class);
  private final ViewStoricoDispositivoVeicoloContrattoRepository storicoStatoDispositivoRepository = mock(ViewStoricoDispositivoVeicoloContrattoRepository.class);;
  private final StoricoStatoServizioRepository                   storicoStatoServizioRepository = mock(StoricoStatoServizioRepository.class);

  @BeforeEach
  void setUp() throws Exception {
    deviceService = new DeviceServiceImpl(mockDeviceRepository,storicoStatoDispositivoRepository, storicoStatoServizioRepository);
  }

  @Test
  void when_deviceUuid_doesntExist_then_return_emptyOptional() {

    // given

    when(mockDeviceRepository.findDeviceBySeriale(anyString(),any(TipoDispositivoEnum.class),anyString())).thenReturn(Optional.empty());

    // when

    var optionalDevice = deviceService.findDeviceBySeriale("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO,null);

    // then

    assertThat(optionalDevice).isEmpty();
  }

  @Test
  void when_deviceUuid_exists_then_return_device() {

    // given

    given(mockDeviceRepository.findDeviceBySeriale(anyString(),any(TipoDispositivoEnum.class),anyString())).willReturn(Optional.of(newDevice()));

    // when

    var optionalDevice = deviceService.findDeviceBySeriale("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO,"::companyCode::");

    // then

    assertThat(optionalDevice).contains(newDevice());
  }

}
