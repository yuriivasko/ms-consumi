package it.fai.ms.consumi.domain;

import java.math.BigDecimal;

import javax.money.MonetaryAmount;

public class AmountTestBuilder {

  public static AmountTestBuilder newInstance() {
    return new AmountTestBuilder();
  }

  private Amount amount = new Amount();

  private AmountTestBuilder() {
  }

  public Amount build() {
    return amount;
  }

  public AmountTestBuilder withAmountExcludedVat(final MonetaryAmount _monetaryAmount) {
    amount.setAmountExcludedVat(_monetaryAmount);
    return this;
  }

  public AmountTestBuilder withAmountIncludedVat(final MonetaryAmount _monetaryAmount) {
    amount.setAmountIncludedVat(_monetaryAmount);
    return this;
  }

  public AmountTestBuilder withExchangeRate(final BigDecimal _exchangeRate) {
    amount.setExchangeRate(_exchangeRate);
    return this;
  }

  public AmountTestBuilder withVatRate(final BigDecimal _vatRate) {
    amount.setVatRate(_vatRate);
    return this;
  }

}
