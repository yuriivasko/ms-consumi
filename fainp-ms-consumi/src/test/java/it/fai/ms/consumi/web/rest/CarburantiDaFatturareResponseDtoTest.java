package it.fai.ms.consumi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.CarburantiDetailDto;

public class CarburantiDaFatturareResponseDtoTest {

  @Test
  void test() {

    List<CarburantiDetailDto> list = Arrays.asList(init(new CarburantiDetailDto(), s -> {
      s.setQuantita(new CarburantiDetailDto.Quantita( new BigDecimal("10.000"),"LT"));
      s.setDescrizioneArticolo("DIESEL");
    }), init(new CarburantiDetailDto(), s -> {
      s.setQuantita(new CarburantiDetailDto.Quantita( new BigDecimal("2.2"),"KG"));
      s.setDescrizioneArticolo("GPL");
    }), init(new CarburantiDetailDto(), s -> {
      s.setQuantita(new CarburantiDetailDto.Quantita( new BigDecimal("10.01"),"LT"));
      s.setDescrizioneArticolo("DIESEL");
    }), init(new CarburantiDetailDto(), s -> {
      s.setQuantita(new CarburantiDetailDto.Quantita( new BigDecimal("2.3"),"KG"));
      s.setDescrizioneArticolo("GPL");
    }));

    CarburantiDaFatturareResponseDto obj = new CarburantiDaFatturareResponseDto(list);

    assertThat(obj.totals.entrySet()).containsExactlyInAnyOrder(entry("DIESEL", "20.010", "LT"), entry("GPL","KG", "4.5"));

  }

  private static SimpleEntry<String, CarburantiDetailDto.Quantita> entry(String key, String val,String unita) {
    return new AbstractMap.SimpleEntry<String, CarburantiDetailDto.Quantita>(key,new CarburantiDetailDto.Quantita( new BigDecimal(val),unita));
  }

  public static final <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }
}
