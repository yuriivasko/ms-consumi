package it.fai.ms.consumi.service.canoni;

import java.time.LocalDate;
import java.time.OffsetDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.common.jms.fattura.canoni.EsitoCanoni;
import it.fai.ms.consumi.client.CanoniApiClient;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@EnableFeignClients(basePackageClasses = CanoniApiClient.class)
@ActiveProfiles(profiles = {"integration","testCanoniNavApi"})
class CanoniClientServiceTest extends AbstractIntegrationTestSpringContext {

  @Autowired
  CanoniClientService canoniClient;

  @Test
  void test() {
    EsitoCanoni esito = new EsitoCanoni().codiceCliente("100600")
      .dataAttivazione(LocalDate.parse("2007-12-03"))
      .identificativo(TipoServizioEnum.PEDAGGI_ITALIA.name())
      .quantita(1)
      .targa("AB012CD")
      .targaNazione("I");

    esito.setDataA(OffsetDateTime.parse("2007-11-03T10:15:30+01:00"));
    esito.setDataDa(OffsetDateTime.parse("2007-12-03T10:15:30+01:00"));
    esito.setTransactionId("1234");

    canoniClient.postCanoniServizi(esito);
  }

}
