package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim.consumo;

import it.fai.ms.consumi.domain.consumi.carburante.TokheimConsumo;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0201;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0301;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0302;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim.TokheimRecordDescriptor;
import it.fai.ms.consumi.service.processor.consumi.tokheim.TokehimConsumoDettaglioStanziamentoMapper;
import it.fai.ms.consumi.service.record.carburanti.TokheimRecordConsumoMapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
class TokheimRecordConsumoMapperTest {

  @Test
  void test() throws Exception {
    String fileName = "fileName1";
    TokheimRecord record = init(new TokheimRecord0302(fileName, 3),r302->{
      r302.setParentRecord(init(new TokheimRecord0301(fileName, 2),r301->{
        r301.setParentRecord(init(new TokheimRecord0201(fileName, 1),r201->{
          r201.setCodiceEsternoPoint("point1");
          r301.setNrTrackycard("trackyN1");
        }));
      }));
    });

    TokheimRecordConsumoMapper subject = new TokheimRecordConsumoMapper();

    TokheimConsumo consumo = subject.mapRecordToConsumo(record);

    TokehimConsumoDettaglioStanziamentoMapper mapper = new TokehimConsumoDettaglioStanziamentoMapper();
    consumo.setNavSupplierCode("tokheim");
    DettaglioStanziamentoCarburante dettagliStanziamento = mapper.mapConsumoToDettaglioStanziamento(consumo);

    assertThat(dettagliStanziamento.getFuelStation().getFuelStationCode()).isEqualTo("point1");
    assertThat(dettagliStanziamento.getDevice().getSeriale()).isEqualTo("trackyN1");

  }

  @Test
  void testNegative() throws Exception {
    String line     = "0302070425-058002-000001329-000770852200-00013901-00000000-00000000000000-00000000                                                                                                                      ";
    String fileName = "testFile";

    var descriptor = new TokheimRecordDescriptor();

    String recordCode = descriptor.extractRecordCode(line);


    TokheimRecord record = descriptor.decodeRecordCodeAndCallSetFromString(line, recordCode, fileName, 3);
    TokheimRecord0301 r0301 = new TokheimRecord0301(fileName, 2);
    r0301.setValuta("EUR");
    record.setParentRecord(r0301);

    TokheimRecord0201 r0201 = new TokheimRecord0201(fileName, 1);
    r0201.setCodiceEsternoPoint("externPointCode");
    r0301.setParentRecord(r0201);

    TokheimRecordConsumoMapper subject = new TokheimRecordConsumoMapper();
    TokheimConsumo consumo = subject.mapRecordToConsumo(record);



    assertThat(consumo.getAmount().getAmountIncludedVat()).isEqualByComparingTo(MonetaryUtils.strToMonetary("-1.329", "EUR"));
    assertThat(consumo.getPriceReference().getAmountIncludedVat()).isEqualByComparingTo(MonetaryUtils.strToMonetary("-1.329", "EUR"));
    assertThat(consumo.getTransaction().getSign()).isEqualTo("-");
    assertThat(consumo.getPriceReference().getVatRateBigDecimal()).isEqualByComparingTo(new BigDecimal("22"));
  }


  public static <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }
}
