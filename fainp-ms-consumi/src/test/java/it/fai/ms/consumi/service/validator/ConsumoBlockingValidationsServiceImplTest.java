package it.fai.ms.consumi.service.validator;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElceuGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.validator.internal.ContractValidator;
import it.fai.ms.consumi.service.validator.internal.DeviceValidator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.time.Instant;
import java.util.Optional;

import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.*;

@Tag("unit")
class ConsumoBlockingValidationsServiceImplTest {

  private static Elceu newElceu(Device device, Vehicle vehicle) {
    Elceu elceu = new Elceu(
      new Source(Instant.now(),
        Instant.now(),
        "::recordCode::"),
      "::globalIdentifier::",
      Optional.of(
        new ElceuGlobalIdentifier("::globalIdentifier::")
      ),
      TestDummyRecord.instance1
    );
    if(device != null){
      elceu.setDevice(device);
    }
    if(vehicle!=null) {
      elceu.setVehicle(vehicle);
    }
    return elceu;
  }

  private static Elceu newElceuWithoutDevice() {
    var elceu = newElceu(null, null);
    elceu.setDevice(null);
    return elceu;
  }

  private final ContractValidator           mockContractValidator           = mock(ContractValidator.class);
  private final CustomerService             mockCustomerService             = mock(CustomerService.class);
  private final DeviceValidator             mockDeviceValidator             = mock(DeviceValidator.class);
  private final StanziamentiParamsValidator mockStanziamentiParamsValidator = mock(StanziamentiParamsValidator.class);
  private ConsumoBlockingValidatorService   validationsService;

  @Test
  void when_consumo_has_pan_find_device_by_pan_returns_exists(){
    // given
    Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setPan("::pan::");
    device.setSeriale("::seriale::");
    final var consumo = newElceu(device, null);
    Contract contract = new Contract("::contractUuid::");
    contract.setCompanyCode(":companyCode:");
    consumo.setContract(Optional.of(contract));


    // when
    given(mockDeviceValidator.findAndSetDevice(device)).willReturn(newValidationOutcome());
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then
    ArgumentCaptor<Device> deviceArgumentCaptor = ArgumentCaptor.forClass(Device.class);
    then(mockDeviceValidator).should().findAndSetDevice(deviceArgumentCaptor.capture());
    assertThat(deviceArgumentCaptor.getValue().getPan()).isEqualTo(device.getPan());
    assertThat(deviceArgumentCaptor.getValue().getId()).isEqualTo(device.getId());
    then(mockDeviceValidator).shouldHaveNoMoreInteractions();

    then(mockContractValidator).should()
      .findAndSetContract(eq(contract));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsValidator).should()
      .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();


    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isTrue();

    assertThat(validationOutcome.isFatalError()).isFalse();
  }
  @Test
  void when_consumo_has_pan_find_device_by_pan_returns_not_exists_blocking_error(){
    Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setPan("::pan::");
    device.setSeriale(null);
    final var consumo = newElceu(device, null);
    TollPoint tp =new TollPoint();
    Instant expectedTime = Instant.now();
    tp.setTime(expectedTime);
    consumo.setTollPointExit(tp);
    Contract contract = new Contract("::contractUuid::");
    contract.setCompanyCode(":companyCode:");
    consumo.setContract(Optional.of(contract));

    // when
    String errorMessageDeviceNotFound = "::errorMessage-devicenotfound::";
    String erroCodeDeviceNotFound = "::errorCode-devicenotfound::";
    given(mockDeviceValidator.findAndSetDevice(device)).willReturn(newValidationOutcomeFailure(erroCodeDeviceNotFound,errorMessageDeviceNotFound));
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then
    ArgumentCaptor<Device> deviceArgumentCaptor = ArgumentCaptor.forClass(Device.class);
    then(mockDeviceValidator).should().findAndSetDevice(deviceArgumentCaptor.capture());
    assertThat(deviceArgumentCaptor.getValue().getPan()).isEqualTo(device.getPan());
    assertThat(deviceArgumentCaptor.getValue().getId()).isEqualTo(device.getId());
    then(mockDeviceValidator).shouldHaveNoMoreInteractions();

    then(mockContractValidator).should()
      .findAndSetContract(eq(contract));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsValidator).should()
      .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();


    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isFalse();
    assertThat(validationOutcome.getMessages())
      .extracting(m->tuple(m.getCode(),m.getText()))
      .containsExactlyInAnyOrder(
        tuple(erroCodeDeviceNotFound,errorMessageDeviceNotFound)
      );
    assertThat(validationOutcome.isFatalError()).isTrue();
  }



  @Test
  void when_consumo_has_uuid_find_device_by_pan_exists(){
    // given consumo with no contract and no seriale and pan
    Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setPan(null);
    device.setSeriale("::seriale::");
    final var consumo = newElceu(device, null);
    TollPoint tp =new TollPoint();
    Instant expectedTime = Instant.now();
    tp.setTime(expectedTime);
    consumo.setTollPointExit(tp);
    Contract contract = new Contract("::contractUuid::");
    contract.setCompanyCode(":companyCode:");
    consumo.setContract(Optional.of(contract));

    // when
    given(mockDeviceValidator.findAndSetDevice(device)).willReturn(newValidationOutcome());
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then
    ArgumentCaptor<Device> deviceArgumentCaptor = ArgumentCaptor.forClass(Device.class);
    then(mockDeviceValidator).should().findAndSetDevice(deviceArgumentCaptor.capture());
    assertThat(deviceArgumentCaptor.getValue().getPan()).isEqualTo(device.getPan());
    assertThat(deviceArgumentCaptor.getValue().getId()).isEqualTo(device.getId());
    then(mockDeviceValidator).shouldHaveNoMoreInteractions();

    then(mockContractValidator).should().findAndSetContract(eq(contract));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsValidator).should()
      .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();


    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isTrue();

    assertThat(validationOutcome.isFatalError()).isFalse();

  }
  @Test
  void when_consumo_has_uuid_find_device_by_pan_not_exists(){
    // given
    Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setPan(null);
    device.setSeriale("::seriale::");
    final var consumo = newElceu(device, null);
    Contract contract = new Contract("::contractUuid::");
    contract.setCompanyCode(":companyCode:");
    consumo.setContract(Optional.of(contract));


    // when
    String noDeviceFoundMessage = "noDeviceFound";
    given(mockDeviceValidator.findAndSetDevice(device)).willReturn(newValidationOutcomeFailure("::failure1::", noDeviceFoundMessage));
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then
    ArgumentCaptor<Device> deviceArgumentCaptor = ArgumentCaptor.forClass(Device.class);
    then(mockDeviceValidator).should().findAndSetDevice(deviceArgumentCaptor.capture());
    assertThat(deviceArgumentCaptor.getValue().getPan()).isEqualTo(device.getPan());
    assertThat(deviceArgumentCaptor.getValue().getId()).isEqualTo(device.getId());
    then(mockDeviceValidator).shouldHaveNoMoreInteractions();

    then(mockContractValidator).should()
      .findAndSetContract(eq(contract));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsValidator).should()
      .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();


    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isFalse();
    assertThat(validationOutcome.getMessages())
      .extracting(m->tuple(m.getCode(),m.getText()))
      .containsExactlyInAnyOrder(
        tuple("::failure1::",noDeviceFoundMessage)
      );
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

  @Test
  void when_no_pan_no_uuid_consumo_has_contratto_find_contract(){
    // given
    Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setPan(null);
    device.setSeriale(null);
    final var consumo = newElceu(device, null);
    Contract contractOnConsumo = new Contract("::contractUuid::");
    Contract expectedContract = new Contract("::contractUuid::");
    expectedContract.setCompanyCode(":companyCode:");
    expectedContract.setState("::state::");
    consumo.setContract(Optional.of(contractOnConsumo));
    TollPoint tp =new TollPoint();
    Instant expectedTime = Instant.now();
    tp.setTime(expectedTime);
    consumo.setTollPointExit(tp);
    Vehicle vehicle = new Vehicle();
    vehicle.setEuroClass("::euroClass::");
    vehicle.setFareClass("::fareclass::");
    vehicle.setLicensePlate(new VehicleLicensePlate("::licenceId::","::coutry::"));

    consumo.setVehicle(vehicle);

    // when
    given(mockDeviceValidator.findAndSetDevice(device)).willReturn(null);
    given(mockContractValidator.findAndSetContract(contractOnConsumo)).willAnswer( invocationOnMock -> {
      Contract c = (Contract) invocationOnMock.getArguments()[0];
      c.setCompanyCode(expectedContract.getCompanyCode());
      c.setState(expectedContract.getState());
      return newValidationOutcome();
    });
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then
    then(mockDeviceValidator).should().findAndSetDevice(device);

    contractOnConsumo.setCompanyCode(":companyCode:");
    then(mockContractValidator).should()
      .findAndSetContract(eq(contractOnConsumo));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockDeviceValidator).should(never()).findAndSetDevice(any(),any(),any());
    then(mockStanziamentiParamsValidator).should()
      .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();


    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();
  }
  @Test
  void when_no_pan_no_uuid_consumo_has_contratto_no_device_contract_not_found(){
    // given
    Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setPan(null);
    device.setSeriale(null);
    final var consumo = newElceu(device, null);
    Contract contractOnConsumo = new Contract("::contractUuid::");
    Contract expectedContract = new Contract("::contractUuid::");
    expectedContract.setCompanyCode(":companyCode:");
    expectedContract.setState("::state::");
    consumo.setContract(Optional.of(contractOnConsumo));
    TollPoint tp =new TollPoint();
    Instant expectedTime = Instant.now();
    tp.setTime(expectedTime);
    consumo.setTollPointExit(tp);
    Vehicle vehicle = new Vehicle();
    vehicle.setEuroClass("::euroClass::");
    vehicle.setFareClass("::fareclass::");
    vehicle.setLicensePlate(new VehicleLicensePlate("::licenceId::","::coutry::"));

    consumo.setVehicle(vehicle);

    // when
    given(mockDeviceValidator.findAndSetDevice(device)).willReturn(null);
    given(mockContractValidator.findAndSetContract(contractOnConsumo)).willAnswer( invocationOnMock -> {
      Contract c = (Contract) invocationOnMock.getArguments()[0];
      c.setCompanyCode(expectedContract.getCompanyCode());
      c.setState(expectedContract.getState());
      return newValidationOutcome();
    });
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then
    then(mockDeviceValidator).should().findAndSetDevice(device);

    contractOnConsumo.setCompanyCode(":companyCode:");
    then(mockContractValidator).should()
      .findAndSetContract(eq(contractOnConsumo));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockDeviceValidator).should(never()).findAndSetDevice(any(),any(),any());
    then(mockStanziamentiParamsValidator).should()
      .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();


    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();
  }

  @Test
  void when_consumo_has_contract_and_device_contract_consistent(){
    // given
    Device device = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    device.setPan("::panNumber::");
    device.setSeriale(null);

    final var consumo = newElceu(device, null);
    Contract contractOnConsumo = new Contract("::contractUuid::");
    Contract expectedContract = new Contract("::contractUuid::");
    expectedContract.setCompanyCode(":companyCode:");
    expectedContract.setState("::state::");
    consumo.setContract(Optional.of(contractOnConsumo));
    TollPoint tp =new TollPoint();
    Instant expectedTime = Instant.now();
    tp.setTime(expectedTime);
    consumo.setTollPointExit(tp);
    Vehicle vehicle = new Vehicle();
    vehicle.setEuroClass("::euroClass::");
    vehicle.setFareClass("::fareclass::");
    vehicle.setLicensePlate(new VehicleLicensePlate("::licenceId::","::coutry::"));

    consumo.setVehicle(vehicle);

    // when
    given(mockDeviceValidator.findAndSetDevice(device)).willAnswer(
      invocationOnMock -> {
        Device d = (Device) invocationOnMock.getArguments()[0];
        d.setContractNumber(contractOnConsumo.getId());
       return newValidationOutcome();
      });
    given(mockContractValidator.findAndSetContract(contractOnConsumo)).willAnswer( invocationOnMock -> {
      Contract c = (Contract) invocationOnMock.getArguments()[0];
      c.setCompanyCode(expectedContract.getCompanyCode());
      c.setState(expectedContract.getState());
      return newValidationOutcome();
    });
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then
    then(mockDeviceValidator).should().findAndSetDevice(device);

    then(mockContractValidator).should()
      .findAndSetContract(eq(contractOnConsumo));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockDeviceValidator).should(never()).findAndSetDevice(any(),any(),any());
    then(mockStanziamentiParamsValidator).should()
      .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();


    assertThat(validationOutcome.isValidationOk())
      .as(descriptionFromValidationOutcome(validationOutcome))
      .isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();

  }


  @Test
  void when_contract_doesnt_exist_and_device_isntAvailable_return_blocking_error() {

    // given

    final var consumo = newElceuWithoutDevice();
    consumo.setContract(Optional.of(new Contract("::contractUuid::")));

    given(mockContractValidator.findAndSetContract(any())).willReturn(newValidationOutcomeFailure("::failure::", "::text::"));
    // when

    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,
                                                                 mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then

    then(mockDeviceValidator).should(never()).findAndSetDevice(any());
    then(mockDeviceValidator).should(never()).findAndSetDevice(any(),any(), any());

    then(mockContractValidator).should()
                               .findAndSetContract(eq(new Contract("::contractUuid::")));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsValidator).should()
                                         .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.getMessages())
      .extracting(m->tuple(m.getCode(),m.getText()))
      .contains(
        tuple("::failure::","::text::")
      );
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

  @Test
  void when_no_contract_and_no_device_findByVehicle() {

    Vehicle vehicle = new Vehicle();
    vehicle.setLicensePlate(new VehicleLicensePlate("::licenceId::","::countryId::"));
    vehicle.setFareClass("::fareClass::");
    vehicle.setEuroClass("::euroClass::");
    Device deviceWithNoPanAndSeriale = new Device(TipoDispositivoEnum.TELEPASS_ITALIANO);
    deviceWithNoPanAndSeriale.setSeriale(null);
    deviceWithNoPanAndSeriale.setPan(null);
    final var consumo = newElceu(deviceWithNoPanAndSeriale,vehicle);
    consumo.setContract(Optional.empty());
    Instant time = Instant.now();
    TollPoint tollPointExit = new TollPoint(new GlobalGate("::global_gate::"),time);
    consumo.setTollPointExit(tollPointExit);
    String expectedContractCode = "::contract::";
    Contract expectedContract = new Contract(expectedContractCode);
    expectedContract.setCompanyCode("::companyCode");
    expectedContract.setState("ACTIVE");

    //given
    given(mockDeviceValidator.findAndSetDevice(deviceWithNoPanAndSeriale)).willReturn(null);
    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcome());
    given(mockDeviceValidator.findAndSetDevice(vehicle,deviceWithNoPanAndSeriale,time)).willAnswer((invocationOnMock -> {
      Device device = (Device) invocationOnMock.getArguments()[1];
      device.setContractNumber(expectedContractCode);
      return newValidationOutcome();
    }));
    given(mockContractValidator.findAndSetContract(new Contract(expectedContractCode))).willAnswer(invocationOnMock -> {
        Contract contract = (Contract)invocationOnMock.getArguments()[0];
        contract.setState(expectedContract.getState());
        contract.setCompanyCode(expectedContract.getCompanyCode());
        return newValidationOutcome();
      }
    );

    // when

    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,
                                                                 mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then

    verify(mockDeviceValidator).findAndSetDevice(deviceWithNoPanAndSeriale);
    verify(mockDeviceValidator).findAndSetDevice(vehicle,deviceWithNoPanAndSeriale, time);
    verify(mockContractValidator).findAndSetContract(expectedContract);

    then(mockContractValidator).shouldHaveZeroInteractions();

    then(mockStanziamentiParamsValidator).should()
                                         .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();
  }

  @Test
  void when_device_doesnt_exist_return_blocking_error() {

    // given

    final var consumo = newElceu(null, null);
    consumo.setDevice(new Device("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO));
    consumo.setContract(Optional.of(new Contract("::contractUuid::")));

    given(mockDeviceValidator.findAndSetDevice(any(Device.class))).willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    given(mockContractValidator.findAndSetContract(new Contract("::contractUuid::"))).willReturn(newValidationOutcome());

    // when

    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,
                                                                 mockStanziamentiParamsValidator);

    final var validationOutcome = validationsService.validate(consumo);

    // then

    then(mockDeviceValidator).should()
                             .findAndSetDevice(eq(consumo.getDevice()));
    //cambiata l'implementazione, ora cerca anche associazione contratto dispositivo
//    then(mockDeviceValidator).shouldHaveNoMoreInteractions();

    then(mockContractValidator).should()
                               .findAndSetContract(eq(new Contract("::contractUuid::")));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsValidator).should()
                                         .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

  @Test
  void when_device_exist_and_contract_exist_and_device_hasntContract_return_blocking_error() {

    // given

    final var consumo = newElceu(null, null);
    consumo.setDevice(new Device("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO));
    consumo.getDevice().setContractNumber("::contractNumber::");
    consumo.setContract(Optional.of(new Contract("::contractUuid::")));

    given(mockDeviceValidator.findAndSetDevice(consumo.getDevice())).willReturn(newValidationOutcome());

    given(mockContractValidator.findAndSetContract(new Contract("::contractUuid::"))).willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,
                                                                 mockStanziamentiParamsValidator);

    // when

    final var validationOutcome = validationsService.validate(consumo);

    // then

    then(mockDeviceValidator).should()
                             .findAndSetDevice(any(Device.class));
    then(mockDeviceValidator).shouldHaveNoMoreInteractions();

    then(mockContractValidator).should()
                               .findAndSetContract(eq(new Contract("::contractUuid::")));
    then(mockContractValidator).shouldHaveNoMoreInteractions();

    then(mockStanziamentiParamsValidator).should()
                                         .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

  @Test
  void when_stanziamentiParams_doesnt_exist_return_blocking_error() {

    // given

    final var consumo = newElceu(null, null);
    consumo.setDevice(new Device("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO));
    consumo.setContract(Optional.of(new Contract("::contractId::")));

    given(mockStanziamentiParamsValidator.existsParamsStanziamento(consumo)).willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    given(mockDeviceValidator.findAndSetDevice(any(Device.class))).willReturn(newValidationOutcome());
    given(mockDeviceValidator.deviceIsAssociatedWithContract(new Device("::deviceUuid::",TipoDispositivoEnum.TELEPASS_EUROPEO), new Contract("::contractUuid::"))).willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    given(mockContractValidator.findAndSetContract(new Contract("::contractUuid::"))).willReturn(newValidationOutcome());

    given(mockCustomerService.findCustomerByDeviceSeriale("::deviceId::",TipoDispositivoEnum.TELEPASS_EUROPEO)).willReturn(Optional.of(new Customer("::customerId::")));

    validationsService = new ConsumoBlockingValidatorServiceImpl(mockContractValidator, mockDeviceValidator,
                                                                 mockStanziamentiParamsValidator);

    // when

    final var validationOutcome = validationsService.validate(consumo);

    // then

    then(mockStanziamentiParamsValidator).should()
                                         .existsParamsStanziamento(consumo);
    then(mockStanziamentiParamsValidator).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

}
