package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

/**
 * Builder to build {@link DettaglioStanziamentoPedaggioEntity}.
 */
public final class DettaglioStanziamentoPedaggioEntityBuilder {
  BigDecimal amountIncludingVat                   = ramdomAmount();
  BigDecimal amountNoVat                          = ramdomAmount();
  String     customerId                           = RandomStringUtils.random(10);
  String     codiceFornitoreNav = RandomStringUtils.random(10);
  // String codicePartner = RandomStringUtils.random(10);
  String     currencyCode                         = RandomStringUtils.random(10);
  // Instant dataAcquisizioneFlusso = randomInstant();
  // String documentoDaFornitore = RandomStringUtils.random(10);
  String     entryGlobalGateIdentifier            = RandomStringUtils.random(10);
  String     entryGlobalGateIdentifierDescription = RandomStringUtils.random(10);
  Instant    entryTimestamp                       = randomInstant();
  String     euroClass                            = RandomStringUtils.random(10);
  BigDecimal exchangeRate                         = new BigDecimal(Math.random());
  String     exitGlobalGateIdentifier             = RandomStringUtils.random(10);;
  String     exitGlobalGateIdentifierDescription  = RandomStringUtils.random(10);
  Instant    exitTimestamp                        = randomInstant();
  String     fileName                             = RandomStringUtils.random(10);
  // BigDecimal imponibileAdr = ramdomAmount();
  Instant     ingestionTime       = randomInstant();
  InvoiceType invoiceType         = InvoiceType.values()[(int) (Math.random() * InvoiceType.values().length)];
  String      licensePlate        = RandomStringUtils.random(10);
  String      licensePlateCountry = RandomStringUtils.random(10);
  String      obu                 = RandomStringUtils.random(10);
  String     region                    = RandomStringUtils.random(10);
  String     panNumber                 = RandomStringUtils.random(10);
  BigDecimal percIva                   = new BigDecimal(Math.random());
  // String pricingClass = RandomStringUtils.random(10);
  // boolean processed = Math.random()>0.5;
  String     raggruppamentoArticoliNav = RandomStringUtils.random(10);
  String     recordCode                = RandomStringUtils.random(10);
  long       rowNumber                 = (long) (Long.MAX_VALUE * Math.random());
  String     sourceType                = RandomStringUtils.random(10);
  BigDecimal totalAmount               = ramdomAmount();

  private BigDecimal ramdomAmount() {
    return new BigDecimal(100.0 * Math.random()).setScale(5, RoundingMode.HALF_EVEN);
  }

  // String signOfTransaction = RandomStringUtils.random(10);
  String                  transactionDetailCode = RandomStringUtils.random(10);
  String                  tratta                = RandomStringUtils.random(10);
  String                  networdCode           = RandomStringUtils.random(10);
  TipoDispositivoEnum                  deviceType            = TipoDispositivoEnum.TELEPASS_EUROPEO;
  Set<StanziamentoEntity> stanziamenti          = new HashSet<>();

  private String _globalIdentifier;
  private UUID   _uniqueKey;

  DettaglioStanziamentoPedaggioEntityBuilder(String _globalIdentifier, UUID _uniqueKey) {
    this._globalIdentifier = _globalIdentifier;
    this._uniqueKey = _uniqueKey;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withAmountIncludingVat(BigDecimal amountIncludingVat) {
    this.amountIncludingVat = amountIncludingVat;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withStanziamento(StanziamentoEntity stanziamento) {
    this.stanziamenti.add(stanziamento);
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withAmountNoVat(BigDecimal amountNoVat) {
    this.amountNoVat = amountNoVat;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withCodiceClienteNav(String codiceClienteNav) {
    this.customerId = codiceClienteNav;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withCodiceFornitoreNav(String codiceFornitoreNav) {
    this.codiceFornitoreNav = codiceFornitoreNav;
    return this;
  }

  // public DettaglioStanziamentoPedaggioEntityBuilder withCodicePartner(String codicePartner) {
  // this.codicePartner = codicePartner;
  // return this;
  // }

  public DettaglioStanziamentoPedaggioEntityBuilder withCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
    return this;
  }

  // public DettaglioStanziamentoPedaggioEntityBuilder withDataAcquisizioneFlusso(Instant dataAcquisizioneFlusso) {
  // this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
  // return this;
  // }

  // public DettaglioStanziamentoPedaggioEntityBuilder withDocumentoDaFornitore(String documentoDaFornitore) {
  // this.documentoDaFornitore = documentoDaFornitore;
  // return this;
  // }

  public DettaglioStanziamentoPedaggioEntityBuilder withEntryGlobalGateIdentifier(String entryGlobalGateIdentifier) {
    this.entryGlobalGateIdentifier = entryGlobalGateIdentifier;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withEntryGlobalGateIdentifierDescription(String entryGlobalGateIdentifierDescription) {
    this.entryGlobalGateIdentifierDescription = entryGlobalGateIdentifierDescription;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withEntryTimestamp(Instant entryTimestamp) {
    this.entryTimestamp = entryTimestamp;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withEuroClass(String euroClass) {
    this.euroClass = euroClass;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withExchangeRate(BigDecimal exchangeRate) {
    this.exchangeRate = exchangeRate;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withExitGlobalGateIdentifier(String exitGlobalGateIdentifier) {
    this.exitGlobalGateIdentifier = exitGlobalGateIdentifier;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withExitGlobalGateIdentifierDescription(String exitGlobalGateIdentifierDescription) {
    this.exitGlobalGateIdentifierDescription = exitGlobalGateIdentifierDescription;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withExitTimestamp(Instant exitTimestamp) {
    this.exitTimestamp = exitTimestamp;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withFileName(String fileName) {
    this.fileName = fileName;
    return this;
  }

  // public DettaglioStanziamentoPedaggioEntityBuilder withImponibileAdr(BigDecimal imponibileAdr) {
  // this.imponibileAdr = imponibileAdr;
  // return this;
  // }

  public DettaglioStanziamentoPedaggioEntityBuilder withIngestionTime(Instant ingestionTime) {
    this.ingestionTime = ingestionTime;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withInvoiceType(InvoiceType invoiceType) {
    this.invoiceType = invoiceType;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withLicensePlateCountry(String licensePlateCountry) {
    this.licensePlateCountry = licensePlateCountry;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withObu(String obu) {
    this.obu = obu;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withRegion(String region) {
    this.region = region;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withPanNumber(String panNumber) {
    this.panNumber = panNumber;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withPercIva(BigDecimal percIva) {
    this.percIva = percIva;
    return this;
  }

  // public DettaglioStanziamentoPedaggioEntityBuilder withPricingClass(String pricingClass) {
  // this.pricingClass = pricingClass;
  // return this;
  // }

  // public DettaglioStanziamentoPedaggioEntityBuilder withProcessed(boolean processed) {
  // this.processed = processed;
  // return this;
  // }

  public DettaglioStanziamentoPedaggioEntityBuilder withRaggruppamentoArticoliNav(String raggruppamentoArticoliNav) {
    this.raggruppamentoArticoliNav = raggruppamentoArticoliNav;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withRecordCode(String recordCode) {
    this.recordCode = recordCode;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withRowNumber(long rowNumber) {
    this.rowNumber = rowNumber;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withSourceType(String sourceType) {
    this.sourceType = sourceType;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withTotalAmount(BigDecimal totalAmount) {
    this.totalAmount = totalAmount;
    return this;
  }

  // public DettaglioStanziamentoPedaggioEntityBuilder withSignOfTransaction(String signOfTransaction) {
  // this.signOfTransaction = signOfTransaction;
  // return this;
  // }

  public DettaglioStanziamentoPedaggioEntityBuilder withTransactionDetailCode(String transactionDetailCode) {
    this.transactionDetailCode = transactionDetailCode;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withTratta(String tratta) {
    this.tratta = tratta;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withNetwordCode(String networdCode) {
    this.networdCode = networdCode;
    return this;
  }

  public DettaglioStanziamentoPedaggioEntityBuilder withDeviceType(TipoDispositivoEnum deviceType) {
    this.deviceType = deviceType;
    return this;

  }

  public DettaglioStanziamentoPedaggioEntity build() {
    DettaglioStanziamentoPedaggioEntity entity = new DettaglioStanziamentoPedaggioEntity(this._globalIdentifier);
    entity.setAmountIncludedVat(amountIncludingVat);
    entity.setAmountExcludedVat(amountNoVat);
    entity.setCustomerId(customerId);
    entity.setCodiceFornitoreNav(codiceFornitoreNav);
    // entity.set(codicePartner);
    entity.setCurrencyCode(currencyCode);
    // entity.setDataAcquisizioneFlusso(dataAcquisizioneFlusso);
    // entity.setDocumentoDaFornitore(documentoDaFornitore);
    entity.setEntryGlobalGateIdentifier(entryGlobalGateIdentifier);
    entity.setEntryGlobalGateIdentifierDescription(entryGlobalGateIdentifierDescription);
    entity.setEntryTimestamp(entryTimestamp);
    entity.setVehicleEuroClass(euroClass);
    entity.setExchangeRate(exchangeRate);
    entity.setExitGlobalGateIdentifier(exitGlobalGateIdentifier);
    entity.setExitGlobalGateIdentifierDescription(exitGlobalGateIdentifierDescription);
    entity.setExitTimestamp(exitTimestamp);
    entity.setFileName(fileName);
    // entity.set (imponibileAdr);
    entity.setIngestionTime(ingestionTime);
    entity.setInvoiceType(invoiceType);
    entity.setVehicleLicenseLicenseId(licensePlate);
    entity.setVehicleLicenseCountryId(licensePlateCountry);
    entity.setDeviceObu(obu);
    entity.setRegion(region);
    entity.setDevicePan(panNumber);
    entity.setAmountVatRate(percIva);
    // entity.set(pricingClass);
    // entity.setproc(processed);
    entity.setRaggruppamentoArticoliNav(raggruppamentoArticoliNav);
    entity.setRecordCode(recordCode);
    entity.setSourceRowNumber(rowNumber);
    entity.setSourceType(sourceType);
    entity.setAmountIncludedVat(totalAmount);
    // entity.setSignOfTransaction(signOfTransaction);
    entity.setTransactionDetailCode(transactionDetailCode);
    entity.setTratta(tratta);
    entity.setNetwordCode(networdCode);
    entity.setDeviceType(deviceType);
    entity.getStanziamenti()
          .addAll(stanziamenti);
    return entity;
  }

  private Instant randomInstant() {
    return Instant.ofEpochMilli((long) (Math.random() * new Date().getTime()));
  }

}
