package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti;

import static it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiJollyDescriptor.HEADER_BEGIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;

import java.io.File;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.trackycardcarbjolly.TrackyCardCarbJollyProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.carburanti.TrackyCardCarbJollyRecordConsumoMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.TrackyCardCarburantiStandardJobTest;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Tag("unit")
class TrackyCardCarburantiJollyJobTest {

  private final TrackyCardCarburantiJollyDescriptor    descriptor                   = spy(new TrackyCardCarburantiJollyDescriptor());

  private final StanziamentiParamsValidator            stanziamentiParamsValidator  = mock(StanziamentiParamsValidator.class);
  private final NotificationService                    notificationService          = mock(NotificationService.class);
  private final RecordPersistenceService               recordPersistenceService     = mock(RecordPersistenceService.class);
  private final TrackyCardCarbJollyProcessor           processor                    = mock(TrackyCardCarbJollyProcessor.class);
  private final TrackyCardCarbJollyRecordConsumoMapper consumoMapper                = mock(TrackyCardCarbJollyRecordConsumoMapper.class);
  private final StanziamentiToNavPublisher           stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
  private final PlatformTransactionManager transactionManager                       = mock(PlatformTransactionManager.class);

  private final ServicePartner servicePartner = mock(ServicePartner.class);

  private TrackyCardCarburantiJollyJob job;
  private String filename;

  private Stanziamento stanziamento;


  @BeforeEach
  void setUp() throws Exception {
    filename = new File(TrackyCardCarburantiStandardJobTest.class.getResource("/test-files/trackycardcarbjolly/JOLLY20170418092137.JOLLY.20170418103004")
                                                                 .toURI()).getAbsolutePath();

    job = new TrackyCardCarburantiJollyJob(transactionManager, stanziamentiParamsValidator, notificationService,
                                           recordPersistenceService, descriptor, processor, consumoMapper,
                                           stanziamentiToNavJmsProducer);

    stanziamento = new Stanziamento("");
    stanziamento.setTipoFlusso(TypeFlow.NONE);
    stanziamento.setInvoiceType(InvoiceType.D);



    final HashMap<Integer, Integer> startEndMap = new HashMap<>();
    startEndMap.put(3, 7);
  }

  @Test
  public void process_if_checkConsistency_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");

    given(stanziamentiParamsValidator.checkConsistency())
      .willThrow(runtimeExTest);


    given(validationOutcome.isValidationOk())
      .willReturn(true);

    job.process(filename, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(atLeastOnce())
      .notify(any());
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  @Test
  public void process_if_recordPersistenceService_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");
    willThrow(runtimeExTest)
      .given(recordPersistenceService)
      .persist(any());

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filename, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(atLeastOnce())
      .notify(any());
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Test
  public void process_if_checkConsistency_fail_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);


    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filename, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(2))
      .notify(any());
  }

  @Test
  public void process_if_all_ok_will_process_all_detail_rows() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);



    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TrackyCardCarburantiJolly.class));

    final ProcessingResult<?> processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filename, -1l, Instant.EPOCH, servicePartner);

    then(descriptor)
      .should(times(1))
      .decodeRecordCodeAndCallSetFromString(any(), eq(HEADER_BEGIN), any(), anyLong());

    then(descriptor)
      .should(times(9))
      .decodeRecordCodeAndCallSetFromString(any(), not(eq(HEADER_BEGIN)), any(), anyLong());

  }

  @Test
  public void process_if_all_ok_will_map_all_rows_to_consumo() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TrackyCardCarburantiJolly.class));

    final ProcessingResult<?> processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filename, -1l, Instant.EPOCH, servicePartner);

    then(consumoMapper)
      .should(times(9))
      .mapRecordToConsumo(any());
  }

  @Test
  public void process_if_checks_success_will_send_stanziamenti_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TrackyCardCarburantiJolly.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filename, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(0))
      .notify(any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void process_if_processor_pratially_process_rows_but_throw_an_exception_will_send_notification_and_stanziamento_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TrackyCardCarburantiJolly.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult)
      .willThrow(new RuntimeException("This is an exception to test behaviour"));

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filename, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(8))
      .notify(any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void getJobQualifier() {
    assertThat(job.getJobQualifier())
      .isEqualTo(Format.FAISERVICE_JOLLY);
  }

}
