package it.fai.ms.consumi.domain;

import java.util.Currency;

public class ArticleTestBuilder {

  public static ArticleTestBuilder newInstance() {
    return new ArticleTestBuilder();
  }

  private Article article;

  private ArticleTestBuilder() {
  }

  public Article build() {
    return article;
  }

  public ArticleTestBuilder withCode(final String _code) {
    article = new Article(_code);
    article.setDescription("::description::");
    return this;
  }

  public ArticleTestBuilder withCountry(final String _country) {
    article.setCountry(_country);
    return this;
  }

  public ArticleTestBuilder withGrouping(final String _grouping) {
    article.setGrouping(_grouping);
    return this;
  }

  public ArticleTestBuilder withMeasurementUnit(final String _measurementUnit) {
    article.setMeasurementUnit(_measurementUnit);
    return this;
  }

  public ArticleTestBuilder withVatRate(final float _vatRate) {
    article.setVatRate(_vatRate);
    return this;
  }
  
  public ArticleTestBuilder withCurrencyCode(final String currencyCode) {
    article.setCurrency(Currency.getInstance(currencyCode));
    return this;
  }

}
