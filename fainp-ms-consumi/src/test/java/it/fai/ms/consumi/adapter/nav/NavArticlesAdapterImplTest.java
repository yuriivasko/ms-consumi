package it.fai.ms.consumi.adapter.nav;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;

import it.fai.ms.consumi.client.AnagaziendeService;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.dto.ArticoloDTO;

@Tag("unit")
class NavArticlesAdapterImplTest {

    private final AnagaziendeService anagaziendeService = mock(AnagaziendeService.class);
    private NavArticlesAdapter navArticlesAdapter;

    @BeforeEach
    void setUp() {
        navArticlesAdapter = new NavArticlesAdapterImpl(anagaziendeService);
    }

    @Test
    void when_valuta_is_null_is_should_set_currency_EUR() {
        ArticoloDTO articoloWithoutValuta = new ArticoloDTO();
        articoloWithoutValuta.setNo("::code::");
        articoloWithoutValuta.setDescription("::description1::");
        articoloWithoutValuta.setDescription2("::description2::");
        articoloWithoutValuta.setCountryRegionOfOriginCode("IT");
        articoloWithoutValuta.setBaseUnitOfMeasure("LT");
        articoloWithoutValuta.setPercentualeIva(BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP));

        ArticoloDTO articoloWithValuta = new ArticoloDTO();
        BeanUtils.copyProperties(articoloWithoutValuta, articoloWithValuta);
        articoloWithValuta.setValuta("CHF");

        given(anagaziendeService.getAllArticoli())
            .willReturn(Arrays.asList(articoloWithoutValuta, articoloWithValuta));

        List<Article> list = navArticlesAdapter.getAllArticles();
        assertThat(list.get(0).getCode())
            .isEqualTo("::code::");
        assertThat(list.get(0).getDescription())
            .isEqualTo("::description1:: ::description2::");
        assertThat(list.get(0).getCountry())
            .isEqualTo("IT");
        assertThat(list.get(0).getMeasurementUnit())
            .isEqualTo("LT");
        assertThat(list.get(0).getVatRate())
            .isEqualTo(10f);
        assertThat(list.get(0).getCurrency())
            .isEqualTo(Currency.getInstance("EUR"));

        assertThat(list.get(1).getCode())
            .isEqualTo("::code::");
        assertThat(list.get(1).getDescription())
            .isEqualTo("::description1:: ::description2::");
        assertThat(list.get(1).getCountry())
            .isEqualTo("IT");
        assertThat(list.get(1).getMeasurementUnit())
            .isEqualTo("LT");
        assertThat(list.get(1).getVatRate())
            .isEqualTo(10f);
        assertThat(list.get(1).getCurrency())
            .isEqualTo(Currency.getInstance("CHF"));
    }

    @Test
    void when_description2_is_null_it_should_use_only_description1() {
        ArticoloDTO articoloWithoutValuta = new ArticoloDTO();
        articoloWithoutValuta.setNo("::code::");
        articoloWithoutValuta.setDescription("::description1::");

        articoloWithoutValuta.setCountryRegionOfOriginCode("IT");
        articoloWithoutValuta.setBaseUnitOfMeasure("LT");
        articoloWithoutValuta.setPercentualeIva(BigDecimal.TEN.setScale(2, RoundingMode.HALF_UP));


        given(anagaziendeService.getAllArticoli())
            .willReturn(Arrays.asList(articoloWithoutValuta));

        List<Article> list = navArticlesAdapter.getAllArticles();
        assertThat(list.get(0).getCode())
            .isEqualTo("::code::");
        assertThat(list.get(0).getDescription())
            .isEqualTo("::description1::");
        assertThat(list.get(0).getCountry())
            .isEqualTo("IT");
        assertThat(list.get(0).getMeasurementUnit())
            .isEqualTo("LT");
        assertThat(list.get(0).getVatRate())
            .isEqualTo(10f);
        assertThat(list.get(0).getCurrency())
            .isEqualTo(Currency.getInstance("EUR"));
    }
}
