package it.fai.ms.consumi.config;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ApplicationProperties.class)
public class NotConfirmedAllocationsConfigTest {

    @Autowired
    private ApplicationProperties properties;


    @Test
    public void readConfig(){
        NotConfirmedAllocationsConfig config = properties.getNotConfirmedAllocationsConfig();
        assertThat(config).isNotNull();
        assertThat(config.getArticleGroupExpirations())
                .isNotNull()
                .hasSize(4);
        assertThat(config.getDefaultExpiration()).isEqualTo(60);
    }


}
