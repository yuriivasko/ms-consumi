package it.fai.ms.consumi.service.validator;

import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.repository.consumo.model.telepass.DummyConsumo;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.Environment;

import java.time.Instant;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@Tag("unit")
class ConsumoValidatorServiceImplTest {

  private ConsumoValidatorService               consumoValidatorService;
  private ConsumoValidatorService               consumoValidatorServiceNoBlocking;

  private final Environment environment = mock(Environment.class);
  private final ConsumoBlockingValidatorService mockConsumoBlockingValidationService = mock(ConsumoBlockingValidatorService.class);
  private final ConsumoWarningValidatorService  mockConsumoWarningValidationService  = mock(ConsumoWarningValidatorService.class);
  private final ConsumoNoBlockingValidatorService  mockConsumoNoBlockingValidatorService  = mock(ConsumoNoBlockingValidatorService.class);


  @BeforeEach
  void init() {
    consumoValidatorService = new ConsumoValidatorServiceImpl(environment, mockConsumoBlockingValidationService, mockConsumoWarningValidationService, null);
    consumoValidatorServiceNoBlocking = new ConsumoValidatorServiceImpl(environment, mockConsumoBlockingValidationService, mockConsumoNoBlockingValidatorService,null);
  }
  
  @Test
  void when_noBlockingValidation() {

    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);

    given(mockConsumoBlockingValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcome());

    // when

    final var validationOutcome = consumoValidatorServiceNoBlocking.validate(consumo);

    // then

    then(mockConsumoBlockingValidationService).should()
                                              .validate(consumo);
    then(mockConsumoBlockingValidationService).shouldHaveNoMoreInteractions();

    then(mockConsumoNoBlockingValidatorService).should().storeForPostValidation(consumo);
    then(mockConsumoNoBlockingValidatorService).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();
  }
  
  @Test
  void when_noBlockingValidation_validationOutcome_isFalse() {

    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);

    given(mockConsumoBlockingValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcomeFailure("::failure::", "::text::"));

    // when

    final var validationOutcome = consumoValidatorServiceNoBlocking.validate(consumo);

    // then

    then(mockConsumoBlockingValidationService).should()
                                              .validate(consumo);
    then(mockConsumoBlockingValidationService).shouldHaveNoMoreInteractions();

    then(mockConsumoNoBlockingValidatorService).shouldHaveZeroInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

  @Test
  void when_blockingValidation_validationOutcome_isFalse_and_warningValidation_validationOutcome_isFalse() {

    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);

    given(mockConsumoBlockingValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcomeFailure("::failure::", "::text::"));
    given(mockConsumoWarningValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcomeFailure("::failure::", "::text::"));

    // when

    final var validationOutcome = consumoValidatorService.validate(consumo);

    // then

    then(mockConsumoBlockingValidationService).should()
                                              .validate(consumo);
    then(mockConsumoBlockingValidationService).shouldHaveNoMoreInteractions();

    then(mockConsumoWarningValidationService).shouldHaveZeroInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

  @Test
  void when_blockingValidation_validationOutcome_isFalse_and_warningValidation_validationOutcome_isTrue() {

    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);

    given(mockConsumoBlockingValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcomeFailure("::failure::", "::text::"));
    given(mockConsumoWarningValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcome());

    // when

    final var validationOutcome = consumoValidatorService.validate(consumo);

    // then

    then(mockConsumoBlockingValidationService).should()
                                              .validate(consumo);
    then(mockConsumoBlockingValidationService).shouldHaveNoMoreInteractions();

    then(mockConsumoWarningValidationService).shouldHaveZeroInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isTrue();
  }

  @Test
  void when_blockingValidation_validationOutcome_isTrue_and_warningValidation_validationOutcome_isFalse() {

    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);

    given(mockConsumoBlockingValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcome());
    given(mockConsumoWarningValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcomeFailure("::failure::", "::text::"));

    // when

    final var validationOutcome = consumoValidatorService.validate(consumo);

    // then

    then(mockConsumoBlockingValidationService).should()
                                              .validate(consumo);
    then(mockConsumoBlockingValidationService).shouldHaveNoMoreInteractions();

    then(mockConsumoWarningValidationService).should()
                                             .validate(consumo);
    then(mockConsumoWarningValidationService).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isFalse();
    assertThat(validationOutcome.isFatalError()).isFalse();
  }

  @Test
  void when_blockingValidation_validationOutcome_isTrue_and_warningValidation_validationOutcome_isTrue() {

    // given

    final var consumo = new DummyConsumo(new Source(Instant.now(), Instant.now(), "::source::"), "", Optional.empty(), TestDummyRecord.instance1);

    given(mockConsumoBlockingValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcome());
    given(mockConsumoWarningValidationService.validate(consumo)).willReturn(ValidationOutcomeTestFactory.newValidationOutcome());

    // when

    final var validationOutcome = consumoValidatorService.validate(consumo);

    // then

    then(mockConsumoBlockingValidationService).should()
                                              .validate(consumo);
    then(mockConsumoBlockingValidationService).shouldHaveNoMoreInteractions();

    then(mockConsumoWarningValidationService).should()
                                             .validate(consumo);
    then(mockConsumoWarningValidationService).shouldHaveNoMoreInteractions();

    assertThat(validationOutcome.isValidationOk()).isTrue();
    assertThat(validationOutcome.isFatalError()).isFalse();
  }

}
