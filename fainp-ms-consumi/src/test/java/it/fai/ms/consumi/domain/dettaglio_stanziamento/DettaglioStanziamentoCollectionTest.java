package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;
import java.util.Set;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;

@Tag("unit")
class DettaglioStanziamentoCollectionTest {

  private static GlobalIdentifier newInternalGlobalIdentifier() {
    return GlobalIdentifierTestBuilder.newInstance()
                                      .internal()
                                      .build();
  }

  private DettaglioStanziamento newAllocationDetail(final InvoiceType _invoiceType) {
    return new DettaglioStanziamento(newInternalGlobalIdentifier(), _invoiceType) {
    };
  }

  @Test
  void when_collection_has_both_allocationDetail_P_and_D() {

    // given
    var collection = Set.of(newAllocationDetail(InvoiceType.D), newAllocationDetail(InvoiceType.P));

    // when
    var allocationDetailCollection = new DettaglioStanziamentoCollection(collection);

    // then
    assertThat(allocationDetailCollection.thereAreBothDettaglioStanziamentoTypes()).isTrue();

  }

  @Test
  void when_collection_has_only_allocationDetail_D() {

    // given
    var collection = Set.of(newAllocationDetail(InvoiceType.D));

    // when
    var allocationDetailCollection = new DettaglioStanziamentoCollection(collection);

    // then
    assertThat(allocationDetailCollection.thereAreOnlyDettaglioStanziamentoDefinitivo()).isTrue();
  }

  @Test
  void when_collection_has_only_allocationDetail_P() {

    // given
    var collection = Set.of(newAllocationDetail(InvoiceType.P));

    // when
    var allocationDetailCollection = new DettaglioStanziamentoCollection(collection);

    // then
    assertThat(allocationDetailCollection.thereAreOnlyDettaglioStanziamentoProvvisorio()).isTrue();
  }

  @Test
  void when_collection_is_empty() {

    // given
    Set<DettaglioStanziamento> collection = Collections.emptySet();

    // when
    var allocationDetailCollection = new DettaglioStanziamentoCollection(collection);

    // then
    assertThat(allocationDetailCollection.isEmpty()).isTrue();
  }

}
