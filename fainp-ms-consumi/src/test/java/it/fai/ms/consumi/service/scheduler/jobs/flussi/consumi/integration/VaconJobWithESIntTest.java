package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Vacon;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconJob;
import it.fai.ms.consumi.service.CacheService;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static java.time.Month.AUGUST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

@Tag("integration")
public class VaconJobWithESIntTest
  extends AbstractTelepassIntTest {

  private VaconJob                                job;

  @Inject
  private ConsumoProcessor<Vacon> vaconConsumoProcessor;

  @Inject
  private RecordConsumoMapper<VaconRecord, Vacon> vaconRecordConsumoMapper;

  @Inject
  private NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationService;

  private VaconDescriptor           descriptor = new VaconDescriptor();

  private transient final Logger log = LoggerFactory.getLogger(getClass());

  @MockBean
  private CacheService cacheService;
  private HashMap<String, ConcurrentMapCache> caches = new HashMap<>();

  @BeforeEach
  public void setUp() {

    super.setUpMockContext("Vacon");

    job = new VaconJob(
      transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      descriptor,
      vaconConsumoProcessor,
      vaconRecordConsumoMapper,
      stanziamentiToNavJmsPublisher,
      notConfirmedTemporaryAllocationService);
    ReflectionTestUtils.setField(job, "cacheService", cacheService);

    given(cacheService.getCache(any())).willAnswer((invocationOnMock) -> {
      String cacheName = (String) invocationOnMock.getArguments()[0];
      ConcurrentMapCache cache = caches.get(cacheName);
      if (cache == null) {
        cache = new ConcurrentMapCache(cacheName);
        caches.put(cacheName, cache);
      }
      return cache;
    });
  }

  @Test
  @Transactional
  public void test() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/vacon/DA06A284.VACON.FAI.N00001_slim");

    assertThat(job.getSource()).isEqualTo("VACON");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();
    // assertThat(job.isRunning()).isEqualTo(true);

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
        .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
          .isNotNull();
      assertThat(dataAcquisizioneFlusso)
          .isNotNull();
      assertThat(ingestionTime)
          .isNotEqualTo(dataAcquisizioneFlusso);

      //from file hader 20180321
      LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(2018);
      assertThat(date.getMonth()).isEqualTo(AUGUST);
      assertThat(date.getDayOfMonth()).isEqualTo(10);

      //can be broken using tests on midnight..
      date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());

    }
  }

  @Test
  public void testKOStanziamentiCheckConsistency() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/vacon/DA06A284.VACON.FAI.N00001_slim");

    ValidationOutcome vo = new ValidationOutcome().message(new Message("001").text("this is test error!"));
    vo.setValidationOk(false);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    assertThat(job.getSource()).isEqualTo("VACON");

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), null);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isEmpty();
  }

  @Test
  public void parseFileWithSconti() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/vacon/DA06A284.VACON.EXAGE.N00001_sconti");


    job = new VaconJob(transactionManager, stanziamentiParamsValidator, getNotificationServiceMocked(), recordPersistenceService, descriptor,
      vaconConsumoProcessor, vaconRecordConsumoMapper, stanziamentiToNavJmsPublisher, notConfirmedTemporaryAllocationService);
    ReflectionTestUtils.setField(job, "cacheService", cacheService);

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .isNotEmpty()
      .extracting(s->tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getCosto(),s.getPrezzo()))
      .containsExactlyInAnyOrder(
        tuple("0_D","0006904",null,"TEESQUO2",true, new BigDecimal("0.55000"),new BigDecimal("0.55000")),
        tuple("1_D","0006904","0_D","TEESQUO2",false, new BigDecimal("0.55000"),new BigDecimal("0.55000")),
        tuple("2_D","100102",null,"FRQTTSC",true, new BigDecimal("-13.02000"),new BigDecimal("-13.02000")),
        tuple("3_D","100102","2_D","FRQTTSC",false, new BigDecimal("-13.02000"),new BigDecimal("-13.02000"))
      );

    List<DettaglioStanziamentoEntity> dettaglioStanziamentoPedaggio = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamentoPedaggio)
      .isNotEmpty()
      .extracting(d->{
        DettaglioStanziamentoPedaggioEntity dp = (DettaglioStanziamentoPedaggioEntity) d;
        return Assertions.tuple(dp.getAmountNoVat(), dp.getAmountVatRate());
      })
      .containsExactlyInAnyOrder(
        Assertions.tuple(new BigDecimal("0.55000"), new BigDecimal("21.00")),
        Assertions.tuple(new BigDecimal("-13.02000"), new BigDecimal("20.00"))
      );


  }

  @Test
  public void parseGermanRecords() {

    final String fileAbsolutePath = getAbsolutePath("/test-files/vacon/DA06A284.VACON_GERMANY.EXAGE.N00001_slim");


    job = new VaconJob(transactionManager, stanziamentiParamsValidator, getNotificationServiceMocked(), recordPersistenceService, descriptor,
      vaconConsumoProcessor, vaconRecordConsumoMapper, stanziamentiToNavJmsPublisher, notConfirmedTemporaryAllocationService);
    ReflectionTestUtils.setField(job, "cacheService", cacheService);

    job.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), newServicePartner());

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList)
      .isNotEmpty()
      .extracting(s->tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getCosto(),s.getPrezzo()))
      .containsExactlyInAnyOrder(
        tuple("0_D","100589",null,"TBDVACONDE",true, new BigDecimal("0.11000"),new BigDecimal("0.11000")),
        tuple("1_D","100589","0_D","TBDVACONDE",false, new BigDecimal("0.11000"),new BigDecimal("0.11000"))
      );

    List<DettaglioStanziamentoEntity> dettaglioStanziamentoPedaggio = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamentoPedaggio)
      .isNotEmpty()
      .extracting(d->{
        DettaglioStanziamentoPedaggioEntity dp = (DettaglioStanziamentoPedaggioEntity) d;
        return Assertions.tuple(dp.getGlobalIdentifier(), dp.getAmountVatRate());
      })
      .containsExactlyInAnyOrder(
        Assertions.tuple("11840#EXTERNAL", new BigDecimal("0.00"))
      );


  }

  public ServicePartner newServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.VACON);
    return serviceProvider;
  }
}
