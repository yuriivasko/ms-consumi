package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import java.time.Instant;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

public class DettaglioStanziamentoPedaggioTestBuilder {

  private static GlobalIdentifier newInternalGlobalIdentifier() {
    return GlobalIdentifierTestBuilder.newInstance()
                                      .internal()
                                      .build();
  }

  public static DettaglioStanziamentoPedaggioTestBuilder newInstance() {
    return new DettaglioStanziamentoPedaggioTestBuilder();
  }

  private DettaglioStanziamentoPedaggio dettaglioStanziamento;

  private DettaglioStanziamentoPedaggioTestBuilder() {
  }

  public DettaglioStanziamentoPedaggio build() {
    return dettaglioStanziamento;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withAmount(final Amount _amount) {
    dettaglioStanziamento.setAmount(_amount);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withDevice(final Device _device) {
    dettaglioStanziamento.setDevice(_device);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withVehicle(final Vehicle _device) {
    dettaglioStanziamento.setVehicle(_device);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withCustomer(final Customer _customer) {
    dettaglioStanziamento.setCustomer(_customer);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withDate(final Instant val) {
    dettaglioStanziamento.setDate(val);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withGlobalIdentifierAndUniqueKey(final GlobalIdentifier _globalIdentifier) {
    dettaglioStanziamento = new DettaglioStanziamentoPedaggio(_globalIdentifier);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withInvoiceTypeD() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoPedaggio(newInternalGlobalIdentifier());
    }
    dettaglioStanziamento.setInvoiceType(InvoiceType.D);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withInvoiceTypeP() {
    if (dettaglioStanziamento == null) {
      dettaglioStanziamento = new DettaglioStanziamentoPedaggio(newInternalGlobalIdentifier());
    }
    dettaglioStanziamento.setInvoiceType(InvoiceType.P);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withSource(final Source _source) {
    if (_source != null && _source.getRowNumber() == null)
      _source.setRowNumber(0L);
    dettaglioStanziamento.setSource(_source);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withTollPointExit(final TollPoint _tollPointExit) {
    dettaglioStanziamento.setTollPointExit(_tollPointExit);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withStanziamento(final String _codiceStanziamento) {
    final Stanziamento stanziamento = new Stanziamento(_codiceStanziamento);
    dettaglioStanziamento.getStanziamenti()
                         .add(stanziamento);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withSupplier(final Supplier _supplier) {
    dettaglioStanziamento.setSupplier(_supplier);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withTransaction(final Transaction _transaction) {
    dettaglioStanziamento.setTransaction(_transaction);
    return this;
  }

  public DettaglioStanziamentoPedaggioTestBuilder withContract(Contract contract) {
    dettaglioStanziamento.setContract(contract);
    return this;
  }
}
