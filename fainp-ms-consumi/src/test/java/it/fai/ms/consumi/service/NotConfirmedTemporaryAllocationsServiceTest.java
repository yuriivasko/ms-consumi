package it.fai.ms.consumi.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.NotConfirmedAllocationsConfig;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;

@Tag("test")
public class NotConfirmedTemporaryAllocationsServiceTest {

    private NotConfirmedTemporaryAllocationsService service;
    private DettaglioStanziamentiRepositoryImpl repository;
    private StanziamentiToNavPublisher stanziamentiToNavPublisher = mock(StanziamentiToNavPublisher.class);
    private StanziamentoRepository stanziamentoRepository = mock(StanziamentoRepository.class);
    private StanziamentoEntityMapper stanziamentoEntityMapper;// = spy(StanziamentoEntityMapper.class);
    private List<String> tipoFlussi = Arrays.asList("vacon","elvia");

    @BeforeEach
    public void init(){
        stanziamentoEntityMapper = new StanziamentoEntityMapper(mock(DettaglioStanziamentoEntityMapper.class));
        repository = spy(new DettaglioStanziamentiRepositoryImpl(
                mock(EntityManager.class),
                mock(DettaglioStanziamentoEntityMapper.class),
                mock(StanziamentoEntityMapper.class)
        ));
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        NotConfirmedAllocationsConfig expectedConfiguration = new NotConfirmedAllocationsConfig();
            /*
        notConfirmedAllocationsConfig:
        articleGroupExpirations:
            - sourceType: "vacon"
              articleGroup: "AUT_FR"
              expiration: 20
            - sourceType: "vacon"
              articleGroup: "AUT_ES"
              expiration: 30
            - sourceType: "elvia"
              articleGroup: "AUT_IT"
              expiration: 30
            - sourceType: "elvia"
              articleGroup: "AREA_C"
              expiration: 31
         */
        expectedConfiguration.setArticleGroupExpirations(List.of(
                new NotConfirmedAllocationsConfig.ArticleGroupExpiration("AUT_FR",20,"vacon"),
                new NotConfirmedAllocationsConfig.ArticleGroupExpiration("AUT_ES",20,"vacon"),
                new NotConfirmedAllocationsConfig.ArticleGroupExpiration("AUT_IT",30,"elvia"),
                new NotConfirmedAllocationsConfig.ArticleGroupExpiration("AREA_C",31,"elvia")
        ));
        when(applicationProperties.getNotConfirmedAllocationsConfig()).thenReturn(
                expectedConfiguration
        );
        service = new NotConfirmedTemporaryAllocationsService(
                repository,
                stanziamentiToNavPublisher,
                stanziamentoRepository, stanziamentoEntityMapper,
                applicationProperties,
                new StanziamentoCodeGenerator());

    }

    @Test
    public void readDettagliStanziamentiNonConguagliati(){


        LocalDate date = LocalDate.parse("2018-06-28");
        Instant expectedLastIngestionTime = date.atStartOfDay()
                .toInstant(ZoneOffset.UTC);
        String tipoFlusso = tipoFlussi.get(0);

        String stanziamentoCode = UUID.randomUUID().toString();
        String globalIdentifier = UUID.randomUUID().toString();
        List<DettaglioStanziamentoPedaggioEntity> expectedDettagliStanziamenti = Arrays.asList(

                createDettaglioStanziamentoPedaggioEntity(
                        createStanziamentoEntity(false,
                                new BigDecimal(200),
                                new BigDecimal(200),
                                null,
                                InvoiceType.P,
                                stanziamentoCode, new BigDecimal(1)),
                        InvoiceType.P,
                        new BigDecimal(120),
                        new BigDecimal(20),
                        new BigDecimal(100), globalIdentifier
                )
        );
        StanziamentoEntity expectedConguaglio = createStanziamentoEntity(false,new BigDecimal(-100), new BigDecimal(-100),stanziamentoCode,InvoiceType.P,UUID.randomUUID().toString(), new BigDecimal(1));
        doReturn(expectedDettagliStanziamenti).when(repository)
                .findDettagliStanziamentiProvvisoriBySorgenteAndCodRaggruppamentiBeforeDate(
                        tipoFlusso,
                        Map.of("AUT_FR",20,"AUT_ES",20),
                        expectedLastIngestionTime
                );
        doNothing().when(repository).save(any(DettaglioStanziamentoEntity.class));
        doReturn(mock(DettaglioStanziamentoEntity.class)).when(repository).update(any());
        when(stanziamentoRepository.create(any(StanziamentoEntity.class))).thenReturn(
                expectedConguaglio
        );
        service.processSource(tipoFlusso,expectedLastIngestionTime);
        verify(repository)
                .findDettagliStanziamentiProvvisoriBySorgenteAndCodRaggruppamentiBeforeDate(tipoFlusso,
                        Map.of("AUT_FR",20,"AUT_ES",20),
                        expectedLastIngestionTime);

        //verify insert 1 dettaglio stanziamento definitivo  -100
        ArgumentCaptor<DettaglioStanziamentoEntity> argumentCaptorDettaglioStanziamento = ArgumentCaptor.forClass(DettaglioStanziamentoEntity.class);
        verify(repository).save(argumentCaptorDettaglioStanziamento.capture());
        DettaglioStanziamentoEntity actualUpdateDettaglioSt=argumentCaptorDettaglioStanziamento.getValue();
        assertThat(actualUpdateDettaglioSt).isInstanceOf(DettaglioStanziamentoPedaggioEntity.class);
        assertThat(actualUpdateDettaglioSt.getGlobalIdentifier()).isEqualTo(globalIdentifier);
        assertThat(((DettaglioStanziamentoPedaggioEntity)actualUpdateDettaglioSt).getInvoiceType()).isEqualTo(InvoiceType.D);

        //verify persisted 1 stanziamento provvisorio with value -100
        ArgumentCaptor<StanziamentoEntity> argumentCaptorStanziamento = ArgumentCaptor.forClass(StanziamentoEntity.class);
        verify(stanziamentoRepository).create(argumentCaptorStanziamento.capture());
        StanziamentoEntity actualStanziamento = argumentCaptorStanziamento.getValue();
        assertThat(actualStanziamento.getPrezzo()).isEqualTo(new BigDecimal(-100));
        assertThat(actualStanziamento.getCosto()).isEqualTo(new BigDecimal(-100));
        assertThat(actualStanziamento.getCode()).isNotEqualTo(stanziamentoCode);
        assertThat(actualStanziamento.getCodiceStanziamentoProvvisorio()).isEqualTo(stanziamentoCode);
        assertThat(actualStanziamento.getStatoStanziamento()).isEqualTo(InvoiceType.P);
        //verify updated dettaglio stanziamento to Defintiivo 100
        argumentCaptorDettaglioStanziamento = ArgumentCaptor.forClass(DettaglioStanziamentoEntity.class);
        verify(repository).update(argumentCaptorDettaglioStanziamento.capture());
        actualUpdateDettaglioSt=argumentCaptorDettaglioStanziamento.getValue();
        assertThat(actualUpdateDettaglioSt).isInstanceOf(DettaglioStanziamentoPedaggioEntity.class);
        assertThat(actualUpdateDettaglioSt.getGlobalIdentifier()).isEqualTo(globalIdentifier);
        assertThat(((DettaglioStanziamentoPedaggioEntity)actualUpdateDettaglioSt).getInvoiceType()).isEqualTo(InvoiceType.D);
        //verify invia a nav stanziamento aggiornato
        ArgumentCaptor<List<StanziamentoMessage>> listArgumentCaptor = ArgumentCaptor.forClass(List.class);
        verify(stanziamentiToNavPublisher).publish(listArgumentCaptor.capture());
        assertThat(listArgumentCaptor.getValue()).hasSize(1);
        assertThat(listArgumentCaptor.getValue().get(0)).satisfies(stanziamentoMessage -> {
            assertThat(stanziamentoMessage.getCosto()).isEqualTo(-100d);
            assertThat(stanziamentoMessage.getPrezzo()).isEqualTo(-100d);
            assertThat(stanziamentoMessage.getCodicestanziamentoProvvisorio()).isEqualTo(stanziamentoCode);
        });


    }

    private StanziamentoEntity createStanziamentoEntity(boolean conguaglio, BigDecimal prezzo, BigDecimal costo, String codiceStanziamentoProvvisorio, InvoiceType statoStanziamento, String code, BigDecimal quantity){
        StanziamentoEntity stanziamentoEntity = new StanziamentoEntity(code);
        stanziamentoEntity.setConguaglio(conguaglio);
        stanziamentoEntity.setPrezzo(prezzo);
        stanziamentoEntity.setCosto(costo);
        stanziamentoEntity.setQuantity(quantity);
        stanziamentoEntity.setCodiceStanziamentoProvvisorio(codiceStanziamentoProvvisorio);
        stanziamentoEntity.setStatoStanziamento(statoStanziamento);
        return stanziamentoEntity;
    }

    private DettaglioStanziamentoPedaggioEntity createDettaglioStanziamentoPedaggioEntity(StanziamentoEntity stanziamentoEntity, InvoiceType invoiceType, BigDecimal amountIncludedVat, BigDecimal amountVatRate, BigDecimal amountExcludedVat, String globalIdentifier){
        DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity = new DettaglioStanziamentoPedaggioEntity(globalIdentifier);
        dettaglioStanziamentoPedaggioEntity.getStanziamenti().add(stanziamentoEntity);
        dettaglioStanziamentoPedaggioEntity.setInvoiceType(invoiceType);
        dettaglioStanziamentoPedaggioEntity.setAmountIncludedVat(amountIncludedVat);
        dettaglioStanziamentoPedaggioEntity.setAmountVatRate(amountVatRate);
        dettaglioStanziamentoPedaggioEntity.setAmountExcludedVat(amountExcludedVat);
        return dettaglioStanziamentoPedaggioEntity;
    }
}
