package it.fai.ms.consumi.service.processor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
class ProcessorTest {

  private ConsumoAbstractProcessor      processor;
  private final ConsumoValidatorService consumoValidatorService = mock(ConsumoValidatorService.class);
  private final CustomerService         customerService         = mock(CustomerService.class);
  private final VehicleService          vehicleService          = mock(VehicleService.class);

  final TrackyCardCarburantiJolly       trustedConsumo        = mock(TrackyCardCarburantiJolly.class);
  final TrackyCardCarburantiStandard    untrustedConsumo      = mock(TrackyCardCarburantiStandard.class);
  final DettaglioStanziamentoCarburante dettaglioStanziamento = mock(DettaglioStanziamentoCarburante.class);

  @BeforeEach
  public void setUp() {
    processor = new ConsumoAbstractProcessor(consumoValidatorService,  vehicleService, customerService) {
    };
  }

  @Test
  void addVehicle_trusted() {
    processor.addVehicle(trustedConsumo, dettaglioStanziamento);
    then(vehicleService).shouldHaveZeroInteractions();
  }

  @Test
  void addVehicle_unstrusted() {
    given(untrustedConsumo.getDevice()).willReturn(mock(Device.class));
    given(untrustedConsumo.getVehicle()).willReturn(Vehicle.newEmptyVehicleWithFareClass(""));
    processor.addVehicle(untrustedConsumo, dettaglioStanziamento);
    then(vehicleService).should()
                        .newVehicleByVehicleUUID_WithFareClass(any(), any(), any());
  }
}
