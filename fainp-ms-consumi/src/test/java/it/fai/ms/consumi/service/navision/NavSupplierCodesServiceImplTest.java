package it.fai.ms.consumi.service.navision;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.adapter.nav.NavSupplierCodesAdapter;

@Tag("unit")
class NavSupplierCodesServiceImplTest {

  private final NavSupplierCodesAdapter mockNavSupplierCodesAdapter = mock(NavSupplierCodesAdapter.class);
  private NavSupplierCodesService       navSupplierCodesService;

  @BeforeEach
  void setUp() throws Exception {
    navSupplierCodesService = new NavSupplierCodesServiceImpl(mockNavSupplierCodesAdapter);
  }

  @Test
  void testGetAllCodes() {

    // given

    // when

    navSupplierCodesService.getAllCodes();

    // then

    then(mockNavSupplierCodesAdapter).should()
                                     .getAllCodes();
    then(mockNavSupplierCodesAdapter).shouldHaveNoMoreInteractions();
  }

}
