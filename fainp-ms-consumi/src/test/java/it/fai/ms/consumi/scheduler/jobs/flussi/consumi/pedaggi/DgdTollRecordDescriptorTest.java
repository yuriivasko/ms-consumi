package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;

@Tag("unit")
class DgdTollRecordDescriptorTest {

  private DgdTollRecordDescriptor dgdTollRecordDescriptor;
  private String[]                data;

  @BeforeEach
  public void setUp(){
    data = new String[2];
    data[0] = "N4181Chiasso Brogeda M.  4182Brogeda Autostrada  20170629150623201706301439597896970006618900135=2512 CHF+0000464.20+0000000.00+00.00+0000464.2041818145276       000001";
    data[1] = "T    FAI_RE170629-02.TXT                         00008117000027                                       CHF+0008720.80+0000000.00      +0008720.80                  000028";

    dgdTollRecordDescriptor = new DgdTollRecordDescriptor();
  }

  @Test
  public void getStartEndPosTotalRows() {
    assertThat(dgdTollRecordDescriptor.getStartEndPosTotalRows().get(163))
      .isEqualTo(168);
  }

  @Test
  public void getHeaderBegin() {
    assertThat(dgdTollRecordDescriptor.getHeaderBegin())
      .isEqualTo("§ NO HEADER FILE §");
  }

  @Test
  public void getFooterCodeBegin() {
    assertThat(dgdTollRecordDescriptor.getFooterCodeBegin())
      .isEqualTo("T");
  }

  @Test
  public void getLinesToSubtractToMatchDeclaration() {
    assertThat(dgdTollRecordDescriptor.getLinesToSubtractToMatchDeclaration())
      .isEqualTo(0);
  }

  @Test
  public void extractRecordCode_record(){
    assertThat(dgdTollRecordDescriptor.extractRecordCode(data[0]))
      .isEqualTo("N");
  }

  @Test
  public void extractRecordCode_footer(){
    assertThat(dgdTollRecordDescriptor.extractRecordCode(data[1]))
      .isEqualTo("T");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_record() {
    DgdTollRecord record = dgdTollRecordDescriptor.decodeRecordCodeAndCallSetFromString(data[0], "N", "", -1);
    assertThat(record.getRecordCode())
      .isEqualTo("N");
    assertThat(record.getTollPointCodeEntry())
      .isEqualTo("4181");
    assertThat(record.getTollPointNameEntry())
      .isEqualTo("Chiasso Brogeda M.");
    assertThat(record.getTollPointCodeExit())
      .isEqualTo("4182");
    assertThat(record.getTollPointNameExit())
      .isEqualTo("Brogeda Autostrada");
    assertThat(record.getDateIn())
      .isEqualTo("20170629");
    assertThat(record.getTimeIn())
      .isEqualTo("150623");
    assertThat(record.getDateOut())
      .isEqualTo("20170630");
    assertThat(record.getTimeOut())
      .isEqualTo("143959");
    assertThat(record.getCardNumber())
      .isEqualTo("7896970006618900135");
    assertThat(record.getCurrency())
      .isEqualTo("CHF");
    assertThat(record.getSign())
      .isEqualTo("+");
    assertThat(record.getTaxableAmount())
      .isEqualTo("0000464.20");
    assertThat(record.getTaxAmount())
      .isEqualTo("0000000.00");
    assertThat(record.getTaxRate())
      .isEqualTo("00.00");
    assertThat(record.getTaxedAmount())
      .isEqualTo("0000464.20");
    assertThat(record.getDocumentNumber())
      .isEqualTo("41818145276");
    assertThat(record.getTotalTaxableAmount())
      .isEqualTo(null);
    assertThat(record.getTotalTaxAmount())
      .isEqualTo(null);
    assertThat(record.getTotalTaxedAmount())
      .isEqualTo(null);
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_footer() {
    //non ha header!!!
    DgdTollRecord record = dgdTollRecordDescriptor.decodeRecordCodeAndCallSetFromString(data[1], "T", "", -1);

    assertThat(record.getRecordCode())
      .isEqualTo("T");
    assertThat(record.getFileName())
      .isEqualTo("FAI_RE170629-02.TXT");
    assertThat(record.getSequenceNumber())
      .isEqualTo("00008117");
    assertThat(record.getTotalRows())
      .isEqualTo("000027");
    assertThat(record.getCurrency())
      .isEqualTo("CHF");
    assertThat(record.getTotalTaxableAmount())
      .isEqualTo("0008720.80");
    assertThat(record.getTotalTaxAmount())
      .isEqualTo("0000000.00");
    assertThat(record.getTotalTaxedAmount())
      .isEqualTo("0008720.80");
  }
}
