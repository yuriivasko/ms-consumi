package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

public class StanziamentiParamsArticleEntityTestBuilder {

  public static StanziamentiParamsArticleEntityTestBuilder newInstance() {
    return new StanziamentiParamsArticleEntityTestBuilder();
  }

  private StanziamentiParamsArticleEntity article;

  private StanziamentiParamsArticleEntityTestBuilder() {
  }

  public StanziamentiParamsArticleEntity build() {
    return article;
  }

  public StanziamentiParamsArticleEntityTestBuilder withCode(final String _code) {
    article = new StanziamentiParamsArticleEntity(_code);
    article.setDescription("::description::");
    return this;
  }

}
