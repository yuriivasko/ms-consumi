package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.sitaf;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.FrejusJob;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"), })
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class FrejusJobIntTest2 extends AbstractIntegrationTestSpringContext {

  private Path                filePath;

  @MockBean
  private ElasticSearchFeignClient elasticSearchFeignClient;

  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private NotificationService notificationService;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @Autowired
  private FrejusJob job;

  @Autowired
  private ApplicationProperties applicationProperties;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @BeforeEach
  public void init() throws Exception {
    filePath = Paths.get(FrejusJobIntTest2.class.getResource("/test-files/sitaf/")
                                               .toURI());
    assertThat(job).isNotNull();

    applicationProperties.getFrejusJobProperties()
                         .setFatturazioniFolder(filePath.resolve("fatturazioni2")
                                                        .toAbsolutePath()
                                                        .toString());
    applicationProperties.getFrejusJobProperties()
                         .setPrenotazioniFolder(filePath.resolve("prenotazioni2")
                                                        .toAbsolutePath()
                                                        .toString());
  }

//  @Test
//  public void elaborateFJFR() {
//
//    ServicePartner serviceProvider = elviaServicePartner("FJFR");
//
//
//    String result = job.process(filePath.resolve("20180731_T4009010.187_test")
//                                        .toAbsolutePath()
//                                        .toString(),
//                                System.currentTimeMillis(), new Date().toInstant(), serviceProvider);
//
//    assertThat(result).isNotNull()
//                      .isEqualTo("SITAF");
//
//    verifyZeroInteractions(notificationService);
//
//    // verify messages
//    @SuppressWarnings("unchecked")
//    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);
//    verify(stanziamentiToNavJmsProducer, times(3)).publish(argumentCaptor.capture());
//    List<List<StanziamentoMessage>> allValues = argumentCaptor.getAllValues();
//    Long nStanziamenti = allValues.stream().flatMap(m->m.stream()).collect(Collectors.counting());
//    assertThat(nStanziamenti).isEqualByComparingTo(55l);
//
//  }
//  @Test
//  public void elaborateFJIT() {
//
//    ServicePartner serviceProvider = elviaServicePartner("FJIT");
//
//
//    String result = job.process(filePath.resolve("20180831_T4001036.188_frejius_it")
//                                .toAbsolutePath()
//                                .toString(),
//                                System.currentTimeMillis(), new Date().toInstant(), serviceProvider);
//    assertThat(result).isNotNull().isEqualTo("SITAF");
//
//
//    ArgumentCaptor<Map<String, Object>> ac = ArgumentCaptor.forClass(Map.class);
//
//    verify(notificationService,atLeast(1)).notify(any(String.class), ac.capture());
//
//    ac.getAllValues().stream().forEach(m->System.out.println(m));
//
//    @SuppressWarnings("unchecked")
//    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);
//    verify(stanziamentiToNavJmsProducer, atLeast(1)).publish(argumentCaptor.capture());
//    List<List<StanziamentoMessage>> allValues = argumentCaptor.getAllValues();
//    Long nStanziamenti = allValues.stream().flatMap(m->m.stream()).collect(Collectors.counting());
//    assertThat(nStanziamenti).isEqualByComparingTo(26l);
//  }

  @Test
  public void elaborateMBIT() {

    ServicePartner serviceProvider = newServicePartner("FJIT");


    String result = job.process(filePath.resolve("20180831_T1000867.188")
                                .toAbsolutePath()
                                .toString(),
                                System.currentTimeMillis(), new Date().toInstant(), serviceProvider);
    assertThat(result).isNotNull().isEqualTo("SITAF");


    ArgumentCaptor<Map<String, Object>> ac = ArgumentCaptor.forClass(Map.class);

    verify(notificationService,atLeast(1)).notify(any(String.class), ac.capture());

    ac.getAllValues().stream().forEach(m->System.out.println(m));

    @SuppressWarnings("unchecked")
    ArgumentCaptor<List<StanziamentoMessage>> argumentCaptor = ArgumentCaptor.forClass(List.class);
    verify(stanziamentiToNavJmsPublisher, atLeast(1)).publish(argumentCaptor.capture());
    List<List<StanziamentoMessage>> allValues = argumentCaptor.getAllValues();
    Long nStanziamenti = allValues.stream().flatMap(m->m.stream()).collect(Collectors.counting());
    System.out.println(nStanziamenti);
  }

  public ServicePartner newServicePartner(String codFornitoreLegacy) {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode(codFornitoreLegacy);
    serviceProvider.setPartnerName(codFornitoreLegacy);
    serviceProvider.setFormat(Format.SITAF);
    return serviceProvider;
  }
}
