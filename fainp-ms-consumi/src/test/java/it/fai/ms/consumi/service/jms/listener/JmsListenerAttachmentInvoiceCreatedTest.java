package it.fai.ms.consumi.service.jms.listener;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.consumi.invoice.AttachmentInvoiceCreatedDTO;
import it.fai.ms.consumi.client.AnagaziendeService;
import it.fai.ms.consumi.client.AuthorizationApiService;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.client.StanziamentiApiService;
import it.fai.ms.consumi.client.efservice.EfserviceRestClient;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.repository.fatturazione.RequestsCreationAttachmentsRepository;
import it.fai.ms.consumi.service.consumer.AttachmentInvoiceCreatedConsumer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@ComponentScan(basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.adapter", "it.fai.ms.consumi.repository", "it.fai.ms.consumi.client" })
@Tag("integration")
@Disabled
class JmsListenerAttachmentInvoiceCreatedTest
  extends AbstractIntegrationTestSpringContext {

  private final String numeroFattura = "::codiceFattura::";

  private final String articolo1 = "::articolo1::";

  private final String articolo2 = "::articolo2::";

  @MockBean
  private StanziamentiApiService stanziamentiApiService;

  @MockBean
  private AuthorizationApiService authorizationApiService;

  @MockBean
  private EfserviceRestClient efserviceRestClient;

  @MockBean
  private AnagaziendeService anagaziendeService;

  @MockBean
  private PetrolPumpService petrolPumpService;

  @MockBean
  @Qualifier("reportMessages")
  private MessageSource messageSource;

  @Autowired
  private RequestsCreationAttachmentsRepository repository;
//
//  @Autowired
//  private PutAllegatiToNavService invoiceToNavService;

  private final NotificationService notificationService = mock(NotificationService.class);

  @Autowired
  private AttachmentInvoiceCreatedConsumer attachmentInvoiceService;

  private String uuidDocumento;

  @BeforeEach
  void setUp() throws Exception {

    uuidDocumento = UUID.randomUUID()
                        .toString();
    createRequestCreationAttachmentsByCodiceRaggruppamentoArticolo(articolo1);
    createRequestCreationAttachmentsByCodiceRaggruppamentoArticolo(articolo2);

//    attachmentInvoiceService = new AttachmentInvoiceCreatedConsumer(repository, invoiceToNavService, notificationService);
  }

  @Test
  @Transactional
  void testRequestWithoutUuidDocumento() {
    AttachmentInvoiceCreatedDTO attachmentInvoice = new AttachmentInvoiceCreatedDTO();
    attachmentInvoice.setNumeroFattura(numeroFattura);
    attachmentInvoice.setCodiceRaggruppamentoArticoli(articolo1);
    attachmentInvoice.setUuidDocumento(null);
    attachmentInvoiceService.consume(attachmentInvoice);

    RequestsCreationAttachments request = repository.findByCodiceFatturaAndCodiceRaggruppamentoArticoli(numeroFattura, articolo1);
    assertThat(request.getUuidDocumento()).isNull();
  }

  @Test
  @Transactional
  void testRequestWithUuidDocumento() {
    AttachmentInvoiceCreatedDTO attachmentInvoice = new AttachmentInvoiceCreatedDTO();
    attachmentInvoice.setNumeroFattura(numeroFattura);
    attachmentInvoice.setCodiceRaggruppamentoArticoli(articolo2);
    attachmentInvoice.setUuidDocumento(uuidDocumento);
    attachmentInvoiceService.consume(attachmentInvoice);

    RequestsCreationAttachments request = repository.findByCodiceFatturaAndCodiceRaggruppamentoArticoli(numeroFattura, articolo2);
    assertThat(request.getUuidDocumento()).isNotNull();
  }

  @Test
  @Transactional
  void testAllRequestHaveUuidDocumento() {
    AttachmentInvoiceCreatedDTO attachmentInvoice = new AttachmentInvoiceCreatedDTO();
    attachmentInvoice.setNumeroFattura(numeroFattura);
    attachmentInvoice.setCodiceRaggruppamentoArticoli(articolo2);
    attachmentInvoice.setUuidDocumento(uuidDocumento);
    attachmentInvoiceService.consume(attachmentInvoice);

    RequestsCreationAttachments request = repository.findByCodiceFatturaAndCodiceRaggruppamentoArticoli(numeroFattura, articolo2);
    assertThat(request.getUuidDocumento()).isNotNull();

    attachmentInvoice = new AttachmentInvoiceCreatedDTO();
    attachmentInvoice.setNumeroFattura(numeroFattura);
    attachmentInvoice.setCodiceRaggruppamentoArticoli(articolo1);
    attachmentInvoice.setUuidDocumento(uuidDocumento);
    attachmentInvoiceService.consume(attachmentInvoice);

    request = repository.findByCodiceFatturaAndCodiceRaggruppamentoArticoli(numeroFattura, articolo1);
    assertThat(request.getUuidDocumento()).isNotNull();

    List<RequestsCreationAttachments> requests = repository.findByCodiceFattura(numeroFattura);

    requests.stream()
            .filter(r -> StringUtils.isNotBlank(r.getUuidDocumento()))
            .findFirst()
            .isPresent();
    assertThat(requests.stream()
                       .filter(r -> StringUtils.isNotBlank(r.getUuidDocumento()))
                       .findFirst()
                       .isPresent()).isTrue();
  }

  private void createRequestCreationAttachmentsByCodiceRaggruppamentoArticolo(String codiceArticolo) {
    Instant now = Instant.now();
    RequestsCreationAttachments entity = new RequestsCreationAttachments().codiceFattura(numeroFattura)
                                                                          .dataFattura(now)
                                                                          .codiceRaggruppamentoArticolo(codiceArticolo)
                                                                          .data(now)
                                                                          .importoTotale(new BigDecimal(1000))
                                                                          .uuidDocumento(uuidDocumento);
    repository.save(entity);
  }

}
