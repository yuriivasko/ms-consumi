package it.fai.ms.consumi.domain;

import java.math.BigDecimal;
import java.time.Instant;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;

public class PriceTableTestBuilder {

  public static PriceTableTestBuilder newInstance() {
    return new PriceTableTestBuilder();
  }

  private PriceTableDTO dto = new PriceTableDTO();

  private PriceTableTestBuilder() {
  }

  public PriceTableDTO build() {
    return dto;
  }

  public PriceTableTestBuilder withCostRevenueFlag(final CostRevenue CostRevenue) {
    dto.setCostRevenueFlag(CostRevenue);
    return this;
  }
  
  public PriceTableTestBuilder withReferencePrice(final BigDecimal referencePrice) {
    dto.setReferencePrice(referencePrice);
    return this;
  }

  public PriceTableTestBuilder withValidUntilNow() {
    dto.setValidUntil(Instant.now());
    return this;
  }
}
