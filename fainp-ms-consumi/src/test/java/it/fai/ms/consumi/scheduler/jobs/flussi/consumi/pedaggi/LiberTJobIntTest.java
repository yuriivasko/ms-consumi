package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static it.fai.ms.consumi.testutils.TestUtils.init;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.enumeration.PriceSource;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTConsumo;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTProcessor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTRecord;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"), })
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class LiberTJobIntTest extends AbstractIntegrationTestSpringContext {

  private static final String fileName = "atmb_veicoli_leggeri1.csv";
  private Path                path;

  @MockBean
  private ElasticSearchFeignClient elasticSearchFeignClient;

  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private NotificationService notificationService;

  @MockBean
  private AnagaziendeClient anagaziendeClient;

  @SpyBean
  private LiberTProcessor processor;

  @Autowired
  private LiberTJob job;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @BeforeEach
  public void setup() throws Exception {
    path = Paths.get(LiberTJobIntTest.class.getResource("/test-files/liberT/")
                                           .toURI());
    assertThat(job).isNotNull();

  }

  @Test
  public void elaborateOneRecord() {

    ServicePartner serviceProvider = init(new ServicePartner(fileName), s -> {
      s.setPartnerCode("Liber-T");
      s.setPartnerName("Liber-T");
      s.setFormat(Format.LIBER_T);
      s.setPriceSoruce(PriceSource.by_invoice);
    });

    String filePath = path.resolve(fileName)
                          .toAbsolutePath()
                          .toString();
    String result   = job.process(filePath, System.currentTimeMillis(), new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull()
                      .isEqualTo("libert");

    var                                               type          = ArgumentCaptor.forClass(String.class);
    @SuppressWarnings("unchecked")
    ArgumentCaptor<ElasticSearchRecord<LiberTRecord>> captureRecord = ArgumentCaptor.forClass(ElasticSearchRecord.class);
    verify(elasticSearchFeignClient, times(1)).create(anyString(), type.capture(), captureRecord.capture());

    ElasticSearchRecord<LiberTRecord> esRecords = captureRecord.getValue();
    LiberTRecord record = esRecords.getRawdata();
    assertThat(record).isInstanceOfSatisfying(LiberTRecord.class, r -> {
      assertThat(r.getAnneeFacturation()).isEqualTo("17");
      assertThat(r.getNBadge()).isEqualTo("2");
      assertThat(r.getTauxTVA()).isEqualTo("20");
    });

    ArgumentCaptor<LiberTConsumo> acConsumo = ArgumentCaptor.forClass(LiberTConsumo.class);
    verify(processor, times(1)).validateAndProcess(acConsumo.capture(), any());
    LiberTConsumo consumo = acConsumo.getValue();
    assertThat(consumo.getDevice()
                      .getPan()).isEqualTo("2501001931160000200010000101");
    assertThat(consumo.getAmount().getVatRateBigDecimal()).isEqualByComparingTo("20");

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(1);

     assertThat(dettaglioStanziamenti).allMatch(s -> s.getStanziamenti()
     .size() == 1);

     var dettaglioStanziamento = dettaglioStanziamenti.get(0);

     assertThat(dettaglioStanziamento).isInstanceOfSatisfying(DettaglioStanziamentoPedaggioEntity.class, ds -> {
       assertThat(ds.getSourceRowNumber()).isEqualTo(1);
       assertThat(ds.getFileName()).isEqualTo(StringUtils.substringAfterLast(filePath, "/"));
       assertThat(ds.getVehicleLicenseLicenseId()).isEqualTo("BA003CM");
       assertThat(ds.getVehicleLicenseCountryId()).isEqualTo("IT");
       assertThat(ds.getVehicleFareClass()).isEqualTo(record.getClasseVehicule());
       assertThat(ds.getRecordCode()).isEqualTo("DEFAULT");
       assertThat(ds.getTransactionDetailCode()).isNull();;
       assertThat(ds.getDevicePan()).isEqualTo("2501001931160000200010000101");
       assertThat(ds.getDeviceObu()).isEqualTo("1010101010101");
       assertThat(ds.getAmountVatRate()).isEqualByComparingTo(new BigDecimal(record.getTauxTVA()));
       assertThat(ds.getInvoiceType()).isEqualTo(InvoiceType.D);
       assertThat(ds.getEntryGlobalGateIdentifier()).isEqualTo(record.getNGareEntree());
       assertThat(ds.getExitGlobalGateIdentifier()).isEqualTo(record.getNGareSortie());
       assertThat(ds.getEntryGlobalGateIdentifierDescription()).isEqualTo(record.getNomGareEntree());
       assertThat(ds.getExitGlobalGateIdentifierDescription()).isEqualTo(record.getNomGareSortie());
       assertThat(ds.getEntryTimestamp()).isNull();
       assertThat(ds.getExitTimestamp()).isEqualTo(toInstant("2018-06-06T07:29:18"));
       assertThat(ds.getSourceType()).isEqualTo("LiberT");
       assertThat(ds.getAdrTaxableAmount()).isNull();

     });

//    // verify messages
//     @SuppressWarnings("unchecked")
//     ArgumentCaptor<List<StanziamentoMessage>> captureStanziamenti = ArgumentCaptor.forClass(List.class);
//     verify(stanziamentiToNavJmsProducer).publish(captureStanziamenti.capture());


    verifyNoMoreInteractions(notificationService);
    Mockito.validateMockitoUsage();
  }

  private Instant toInstant(String text1) {
    return LocalDateTime.parse(text1)
                        .atZone(ZoneId.of("Europe/Rome"))
                        .toInstant();
  }


}
