package it.fai.ms.consumi.service.frejus;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.function.Predicate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.FrejusJobProperties;

class FrejusCustomerProviderTest {

  Path filePath;
  private FrejusCustomerProvider subject;

  @BeforeEach
  public void setup() throws URISyntaxException {
    filePath = Paths.get(FrejusCustomerProviderTest.class.getResource("/test-files/sitaf/")
                         .toURI());
    ApplicationProperties properties = new ApplicationProperties();
    properties.setFrejusJobProperties(new FrejusJobProperties());
    subject = new FrejusCustomerProvider(properties);
  }
  @Test
  public void loadCustomerDataTest() throws Exception{
    File[] prenotazioni = filePath.resolve("prenotazioni").toFile().listFiles();
    File[] fatturazioni = filePath.resolve("fatturazioni").toFile().listFiles();

    List<FrejusCustomerData> loadCustomerData = subject.loadCustomerData(fatturazioni, prenotazioni);

    assertThat(loadCustomerData).isNotEmpty();
    Predicate<FrejusCustomerData> containsTrc = t -> "0015360104110".equals(t.trc);
    assertThat(loadCustomerData).anyMatch(containsTrc);
    loadCustomerData.stream().filter(containsTrc).forEach(cust->{
      FrejusCustomerData expected = new FrejusCustomerData();

      expected.trc ="0015360104110";
      expected.azienda ="130046576";
      expected.data =LocalDate.parse("2018-08-31");
      expected.ora =LocalTime.parse("19:06:36");
      expected.numeroCarta ="3081344001036000008";
      expected.targa ="FF409AG";

      assertThat(cust).isEqualToComparingFieldByField(expected);
    });;

  }



}
