package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.AsfinagRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;

@Tag("unit")
class AsfinagRecordDescriptorTest {

  private AsfinagRecordDescriptor asfinagRecordDescriptor;
  private String[]                data;

  @BeforeEach
  public void setUp(){
    data = new String[3];
    data[0] = "10273000007896972018022702160665EUR                         040                                                                                                                                            ";
    data[1] = "2027  9999           066520180227 000000007896970004662900184   2018022401590000220000000000000010000000096530000008044009321448384        C04001000114254628       20000000001609                         ";
    data[2] = "9027000000296000000000029400000000002037090                                                                                                                                                                ";
    asfinagRecordDescriptor = new AsfinagRecordDescriptor();
  }

  @Test
  public void getStartEndPosTotalRows() {
    assertThat(asfinagRecordDescriptor.getStartEndPosTotalRows().get(5))
      .isEqualTo(13);
  }

  @Test
  public void getHeaderBegin() {
    assertThat(asfinagRecordDescriptor.getHeaderBegin())
      .isEqualTo("10");
  }

  @Test
  public void getFooterCodeBegin() {
    assertThat(asfinagRecordDescriptor.getFooterCodeBegin())
      .isEqualTo("90");
  }

  @Test
  public void getLinesToSubtractToMatchDeclaration() {
    assertThat(asfinagRecordDescriptor.getLinesToSubtractToMatchDeclaration())
      .isEqualTo(0);
  }

  @Test
  public void extractRecordCode_header(){
    assertThat(asfinagRecordDescriptor.extractRecordCode(data[0]))
      .isEqualTo("10");
  }

  @Test
  public void extractRecordCode_record(){
    assertThat(asfinagRecordDescriptor.extractRecordCode(data[1]))
      .isEqualTo("20");
  }

  @Test
  public void extractRecordCode_footer(){
    assertThat(asfinagRecordDescriptor.extractRecordCode(data[2]))
      .isEqualTo("90");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_header() {
    //non ha header!!!
    AsfinagRecord record = asfinagRecordDescriptor.decodeRecordCodeAndCallSetFromString(data[0], "10", "LEPTCTTR.999", -1);

    assertThat(record.getRecordCode())
      .isEqualTo("10");
    assertThat(record.getTransactionDetail())
      .isEqualTo("27");
    assertThat(record.getCreationDateInFile())
      .isEqualTo("20180227");
    assertThat(record.getCreationTimeInFile())
      .isEqualTo("0216");
    assertThat(record.getNumeroCorsa())
      .isEqualTo("0665");
//    assertThat(record.getCodiceValuta())
//      .isEqualTo("EUR");
//    assertThat(record.getDescrizione())
//      .isEqualTo("");
//    assertThat(record.getpaeseIsoCode())
//      .isEqualTo("040");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_record() {
    AsfinagRecord record = asfinagRecordDescriptor.decodeRecordCodeAndCallSetFromString(data[1], "20", "LEPTCTTR.999", -1);

    assertThat(record.getRecordCode())
      .isEqualTo("20");
    assertThat(record.getTransactionDetail())
      .isEqualTo("27");
//    assertThat(record.getIdSito())
//      .isEqualTo("9999");
    assertThat(record.getNumeroCorsa())
      .isEqualTo("0665");
    assertThat(record.getDataCreazioneFile())
      .isEqualTo("20180227");
    assertThat(record.getNumeroBollaConsegna())
      .isEqualTo("00000000");
    assertThat(record.getNumeroCarta())
      .isEqualTo("7896970004662900184");
    assertThat(record.getDataConsegna())
      .isEqualTo("20180224");
    assertThat(record.getTempoConsegna())
      .isEqualTo("0159");
    assertThat(record.getTipoTransazione())
      .isEqualTo("00");
    assertThat(record.getCodiceProdotto())
      .isEqualTo("0022");
    assertThat(record.getKm())
      .isEqualTo("0000000");
    assertThat(record.getDriverId())
      .isEqualTo("0000");
    assertThat(record.getQuantitaProdotto())
      .isEqualTo("000100");
    assertThat(record.getImportoIvaIncl())
      .isEqualTo("0000009653");
    assertThat(record.getImportoIvaEscl())
      .isEqualTo("0000008044");
    assertThat(record.getNumeroRicevuta())
      .isEqualTo("009321448384");
    assertThat(record.getCampoInformazione())
      .isEqualTo("C04001000114254628");
    assertThat(record.getAliquotaIva())
      .isEqualTo("2000");
    assertThat(record.getImportoIva())
      .isEqualTo("0000001609");

  }

  @Test
  void decodeRecordCodeAndCallSetFromString_footer() {
    //non ha header!!!
    AsfinagRecord record = asfinagRecordDescriptor.decodeRecordCodeAndCallSetFromString(data[2], "90", "LEPTCTTR.999", -1);

    assertThat(record.getRecordCode())
      .isEqualTo("90");
    assertThat(record.getTransactionDetail())
      .isEqualTo("27");
//    assertThat(record.getConteggioRecord())
//      .isEqualTo("296");
//    assertThat(record.getChecksumLotto())
//      .isEqualTo("294");
//    assertThat(record.getChecksumImporto())
//      .isEqualTo("2037090");
  }
}
