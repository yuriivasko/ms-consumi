package it.fai.ms.consumi.domain;

import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

public class StanziamentiParamsTestBuilder {

  public static StanziamentiParamsTestBuilder newInstance() {
    return new StanziamentiParamsTestBuilder();
  }

  private StanziamentiParams stanziamentiParams;

  private StanziamentiParamsTestBuilder() {
  }

  public StanziamentiParams build() {
    return stanziamentiParams;
  }

  public StanziamentiParamsTestBuilder withArticle(final Article _article) {
    stanziamentiParams.setArticle(_article);
    return this;
  }

  public StanziamentiParamsTestBuilder withFareClass(final String _fareClass) {
    stanziamentiParams.setFareClass(_fareClass);
    return this;
  }

  public StanziamentiParamsTestBuilder withRecordCode(final String _recordCode) {
    stanziamentiParams.setRecordCode(_recordCode);
    return this;
  }

  public StanziamentiParamsTestBuilder withSource(final String _source) {
    stanziamentiParams.setSource(_source);
    return this;
  }

  public StanziamentiParamsTestBuilder withSupplierCode(final String _supplierCode) {
    stanziamentiParams = new StanziamentiParams(SupplierTestBuilder.newInstance()
                                                                   .withCode(_supplierCode)
                                                                   .build());
    stanziamentiParams.setCostoRicavo(CostoRicavo.CR);
    return this;
  }
  public StanziamentiParamsTestBuilder withSupplierCode(final String _supplierCode,String legacyCode) {
    stanziamentiParams = new StanziamentiParams(SupplierTestBuilder.newInstance()
                                                .withCode(_supplierCode)
                                                .withLegacyCode(legacyCode)
                                                .build());
    stanziamentiParams.setCostoRicavo(CostoRicavo.CR);
    return this;
  }

  public StanziamentiParamsTestBuilder withCostoRicavo(final CostoRicavo _costoRicavo) {
    stanziamentiParams.setCostoRicavo(_costoRicavo);
    return this;
  }


  public StanziamentiParamsTestBuilder withTransactionDetail(final String _transactionDetail) {
    stanziamentiParams.setTransactionDetail(_transactionDetail);
    return this;
  }

  public StanziamentiParamsTestBuilder withTransactionType(final String _transactionType) {
    stanziamentiParams.setTransactionType(_transactionType);
    return this;
  }

  public StanziamentiParamsTestBuilder withStanzimentiDetailsType(StanziamentiDetailsType stanziamentiDetailsType) {
    stanziamentiParams.setStanzaimentiDetailsType(stanziamentiDetailsType);
    return this;
  }
}
