package it.fai.ms.consumi.web.rest.util;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

import javax.persistence.EntityManager;

public class TestUtils {

  public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

    JavaTimeModule module = new JavaTimeModule();
    mapper.registerModule(module);

    return mapper.writeValueAsBytes(object);
  }

  public static LocalDate fromStringToLocalDate(String s){
    return LocalDate.parse(s,DateTimeFormatter.ofPattern("yyyy-MM-dd"));
  }
  public static Instant fromStringToInstant(String s) {
    return new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd HH:mm:ss")
            .toFormatter()
            .withZone(ZoneId.systemDefault())
            .parse(s, Instant::from);
  }

  public static void persistDettaglioStanziamentoPedaggioWithStanziamento(Instant acquisitionDate, String sourceType, String code, String targa, TypeFlow tipoFlusso, GeneraStanziamento generaStanziamento,
                                                                          InvoiceType statoStanziamento, String numeroArticolo, String numeroFattura, LocalDate dataErogazioneServizio,
                                                                          boolean isConguaglio, InvoiceType invoiceType, TipoDispositivoEnum deviceType, EntityManager em) {
    StanziamentoEntity s = new StanziamentoEntity(code);
    s.setTarga(targa);
    s.setTipoFlusso(tipoFlusso);
    s.setGeneraStanziamento(generaStanziamento);
    s.setStatoStanziamento(statoStanziamento);
    s.setNumeroArticolo(numeroArticolo);
    s.setNumeroFattura(numeroFattura);
    s.setDataErogazioneServizio(dataErogazioneServizio);
    s.setConguaglio(isConguaglio);
    em.persist(s);
    DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = newDettaglioStanziamentoPedaggioEntity(
            invoiceType,
            deviceType,
            sourceType,
            acquisitionDate, numeroArticolo);
    dettaglioStanziamentoPedaggio.getStanziamenti().add(s);
    em.persist(dettaglioStanziamentoPedaggio);
  }

  public static DettaglioStanziamentoPedaggioEntity newDettaglioStanziamentoPedaggioEntity(InvoiceType invoiceType, TipoDispositivoEnum deviceType, String sourceType, Instant dataAcquisizione, String articleGroup){
    DettaglioStanziamentoPedaggioEntity entity = new DettaglioStanziamentoPedaggioEntity("::globalIdentifier::");
    entity.setInvoiceType(invoiceType);
    entity.setDeviceType(deviceType);
    entity.setSourceType(sourceType);
    entity.setRaggruppamentoArticoliNav(articleGroup);
    entity.setDataAcquisizioneFlusso(dataAcquisizione);
    entity.setCustomerId("::customerId::");
    return entity;
  }
}
