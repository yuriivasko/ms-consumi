package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.dgdtoll;

import static it.fai.ms.consumi.domain.InvoiceType.D;
import static it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento.COSTO_RICAVO;
import static java.time.Month.JUNE;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;

@Tag("integration")
public class DgdTollJobDaConsumoWithESIntTest
  extends AbstractDgdTollJobWithEsIntTest {

  final static String FILENAME = "/test-files/dgdtoll/FAI_RE170629-04.TXT.20170701100012";
  final static String PARTNERCODE = "CH01";


  public DgdTollJobDaConsumoWithESIntTest() throws URISyntaxException {
    super(FILENAME, PARTNERCODE);
  }

  @Test
  @Transactional
  public void process_sample_file_producing_stanziamenti() {

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList.size())
      .isEqualTo(1);

    for (Stanziamento stanziamento : stanziamentoList) {
      assertThat(stanziamento.getCode()).isNotNull();

      //se dettaglio ha recuperato il veicolo
//      assertThat(stanziamento.getVehicleEuroClass()).isNotNull();
//      assertThat(stanziamento.getVehicleLicensePlate()).isNotNull();

      assertThat(stanziamento.getCodiceClienteFatturazione()).isNull();

      //vale solo se esiste uno stanziamento provvisorio - no pedaggi DGD e ASFINAG
      assertThat(stanziamento.getCodiceStanziamentoProvvisorio()).isNull();
      assertThat(stanziamento.isConguaglio()).isFalse();

      //con il cambio non so il valore preciso
      assertThat(stanziamento.getCosto()).isGreaterThanOrEqualTo(BigDecimal.ZERO);
      assertThat(stanziamento.getAmountCurrencyCode()).isEqualTo("EUR");
      assertThat(stanziamento.getDataErogazioneServizio().getYear()).isEqualTo(2017);
      assertThat(stanziamento.getDataErogazioneServizio().getMonth()).isEqualTo(JUNE);
      //FIXME
//      assertThat(stanziamento.getDataErogazioneServizio().getDayOfMonth()).isEqualTo(29);

      assertThat(stanziamento.getDataFattura()).isNull();

      assertThat(stanziamento.getDocumentoDaFornitore()).isNotNull();

      assertThat(stanziamento.getGeneraStanziamento()).isEqualTo(COSTO_RICAVO);
      assertThat(stanziamento.getInvoiceType()).isEqualTo(D);
      assertThat(stanziamento.getDettaglioStanziamenti()).isNotEmpty();
      assertThat(stanziamento.getTotalAllocationDetails()).isEqualTo(2);

      assertThat(stanziamento.getArticleCode()).isNotNull();
      assertThat(stanziamento.getNumeroCliente()).isNotNull();

      assertThat(stanziamento.getNumeroFattura()).isNull();
      assertThat(stanziamento.getNumeroFornitore()).isNotNull();

      assertThat(stanziamento.getPrezzo()).isGreaterThanOrEqualTo(BigDecimal.ZERO);
      assertThat(stanziamento.getQuantita()).isEqualByComparingTo(BigDecimal.ONE);
      assertThat(stanziamento.getTipoFlusso()).isEqualTo(TypeFlow.MYFAI);
      assertThat(stanziamento.getYear()).isEqualTo(2017);

      Set<DettaglioStanziamento> dettagliStanziamento = stanziamento.getDettaglioStanziamenti();
      for (DettaglioStanziamento dettaglio : dettagliStanziamento) {
        DettaglioStanziamentoPedaggio dettaglioCarburante = (DettaglioStanziamentoPedaggio) dettaglio;
        assertThat(dettaglioCarburante.getAmount().getCurrency().getCurrencyCode())
          .isEqualTo("EUR");
//        assertThat(dettaglioCarburante.getSource().getFileName())
//          .isEqualTo("FAI_RE170629-04.TXT.20170701100012");
        assertThat(dettaglioCarburante.getRecordCode())
          .isEqualTo("DEFAULT");
        assertThat(dettaglioCarburante.getTransaction().getSign())
          .isEqualTo("+");
        assertThat(dettaglioCarburante.getAmount().getAmountExcludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isGreaterThanOrEqualTo(BigDecimal.ZERO);
        assertThat(dettaglioCarburante.getAmount().getAmountIncludedVat().getNumber().numberValueExact(BigDecimal.class))
          .isGreaterThanOrEqualTo(BigDecimal.ZERO);
        assertThat(dettaglioCarburante.getAmount().getVatRateBigDecimal())
          .isEqualByComparingTo(new BigDecimal("0.00"));
        assertThat(dettaglioCarburante.getDevice().getId())
          .isEqualTo("7896970000312900970");
      }
    }

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggioEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoPedaggio.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoPedaggio.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
          .isNotNull();
      assertThat(dataAcquisizioneFlusso)
          .isNotNull();
      assertThat(ingestionTime)
          .isNotEqualTo(dataAcquisizioneFlusso);

      //from file hader 170629
      LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(2017);
      assertThat(date.getMonth()).isEqualTo(JUNE);
      assertThat(date.getDayOfMonth()).isEqualTo(29);

      //can be broken using tests on midnight..
      date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());

    }

    assertThat(dettaglioStanziamenti.size())
      .isEqualTo(2);
  }
}
