package it.fai.ms.consumi.service.processor.consumi.tollcollect;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollectGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;



@Tag("unit")
public class TollCollectConsumoDettaglioStanziamentoPedaggioMapperTest {

  private TollCollectConsumoDettaglioStanziamentoPedaggioMapper mapper;

  @BeforeEach
  public void init(){
    mapper = new TollCollectConsumoDettaglioStanziamentoPedaggioMapper();
  }

  @Test
  void mapConsumoToDettaglioStanziamento() {
    final Source source = new Source(Instant.now(), Instant.now(), "TOLL_COLLECT");
    final TollCollect consumo = new TollCollect(source, "", Optional.of(new TollCollectGlobalIdentifier(UUID.randomUUID().toString())), TestDummyRecord.instance1);

    Amount amount = new Amount();
    amount.setVatRate(new BigDecimal("22"));
    MonetaryAmount expectedAmountExcludedVat = MonetaryUtils.strToMonetary("18,00", "EUR");
    amount.setAmountExcludedVat(expectedAmountExcludedVat);
    MonetaryAmount expectedAmountIncludedVat = MonetaryUtils.strToMonetary("21,96", "EUR");
    amount.setAmountIncludedVat(expectedAmountIncludedVat);
    consumo.setAmount(amount);
    consumo.setNavSupplierCode(":codFornitoreNav:");
    DettaglioStanziamento dettaglioStanziamento = mapper.mapConsumoToDettaglioStanziamento(consumo);

    assertThat(dettaglioStanziamento)
      .isNotNull();
    assertThat(dettaglioStanziamento.getAmount()).isNotNull()
      .satisfies(am->{
        assertThat(am.getAmountIncludedVat().getNumber().doubleValue()).isEqualTo(transformToMoney(expectedAmountIncludedVat,"EUR").getNumber().doubleValue());
        assertThat(am.getAmountExcludedVat().getNumber().doubleValue()).isEqualTo(transformToMoney(expectedAmountExcludedVat,"EUR").getNumber().doubleValue());
      });


  }

  private MonetaryAmount transformToMoney(MonetaryAmount amount, String currency){
    return Money.of(amount.getNumber()
        .numberValue(BigDecimal.class)
        .setScale(5, RoundingMode.HALF_UP),
      currency);
  }
}
