package it.fai.ms.consumi.domain;

import java.util.UUID;

public class UUIDTestBuilder {

  public static UUIDTestBuilder newInstance() {
    return new UUIDTestBuilder();
  }

  private String uuidString;

  private UUIDTestBuilder() {
  }

  public UUID build() {
    UUID uuid = null;
    if (uuidString == null) {
      uuid = UUID.randomUUID();
    } else {
      uuid = UUID.fromString(uuidString);
    }
    return uuid;
  }

  public UUIDTestBuilder fixed() {
    uuidString = "c3aba9bf-b831-48dd-8bc5-d651f93d2b44";
    return this;
  }

}
