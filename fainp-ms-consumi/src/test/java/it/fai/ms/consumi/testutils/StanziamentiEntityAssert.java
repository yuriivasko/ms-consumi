package it.fai.ms.consumi.testutils;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

import org.assertj.core.api.AbstractObjectAssert;

import java.math.BigDecimal;

public class StanziamentiEntityAssert extends AbstractObjectAssert<StanziamentiEntityAssert, StanziamentoEntity> {
  public StanziamentiEntityAssert(StanziamentoEntity stanziamentoEntity, Class<?> selfType) {
    super(stanziamentoEntity, selfType);
  }
  //s.getPaese(), s.getCosto(),s.getPrezzo(), s.getClasseVeicoloEuro(),s.getValuta()
  public StanziamentiEntityAssert hasAttributes(String code, String numeroCliente, String codiceStanziamentoProvvisorio,
                                                String articleCode, boolean isConguaglio, InvoiceType statoStanziamento,
                                                String targa, String paese, BigDecimal costo, BigDecimal prezzo,
                                                String classeVeicoloEuro,String valuta){

    return this;
  }
}
