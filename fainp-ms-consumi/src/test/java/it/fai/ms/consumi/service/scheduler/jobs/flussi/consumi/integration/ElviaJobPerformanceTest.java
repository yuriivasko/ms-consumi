package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import static org.mockito.Mockito.mock;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Map;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.config.NotificationsConfiguration;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.consumi.adapter.nav.NavSupplierCodesAdapter;
import it.fai.ms.consumi.client.AnagaziendeService;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.SchedulerConfiguration;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.conf.MockedNavServicesConfiguration;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import liquibase.Liquibase;

@Tag("performance")
@ActiveProfiles(profiles = {"performanceElvia"})
@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties({ApplicationProperties.class})
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableScheduling
@Import({
  JmsConfiguration.class,
  NotificationsConfiguration.class,
  ElviaJobPerformanceTest.ConsumiPerformanceConfiguration.class
})
@ComponentScan(
  basePackages = {
    "it.fai.ms.consumi.FaiconsumiApp"
  }
  ,
  excludeFilters = {
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.config\\.SchedulerConfiguration")
  }
)
@SpringBootTest
public class ElviaJobPerformanceTest {
  private static final String BASE_PATH = "/test-files/consumi_performance/";

  @Autowired
  private ElviaJob elviaJob;
  
  @Autowired
  private EntityManager em;

  @BeforeAll
  public static void setUp() {
    //LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
    //loggerContext.getLoggerList().forEach(logger -> logger.setLevel(Level.INFO));
  }

  public void clean() {
    em.createNativeQuery("truncate table faiconsumi.dbo.dettagliostanziamento_stanziamento").executeUpdate();
    em.flush();
    em.createNativeQuery("delete from faiconsumi.dbo.stanziamento").executeUpdate();
    em.flush();
    em.createNativeQuery("delete from faiconsumi.dbo.dettaglio_stanziamento_carburanti").executeUpdate();
    em.flush();
    em.createNativeQuery("delete from faiconsumi.dbo.dettaglio_stanziamento_generico").executeUpdate();
    em.flush();
    em.createNativeQuery("delete from faiconsumi.dbo.dettaglio_stanziamento_pedaggi").executeUpdate();
    em.flush();
    em.createNativeQuery("delete from faiconsumi.dbo.dettaglio_stanziamento").executeUpdate();
    em.flush();
  }

  @Test
  @Transactional
  public void test_Elvia_performance_2M() {
    //clean();
    String filename = "DA06A284.ELVIA.U2768486.N0023_performance_2M";
    final String fileAbsolutePath = getAbsolutePath(BASE_PATH + filename);
    elviaJob.process(fileAbsolutePath, System.currentTimeMillis(), new Date().toInstant(), elviaServicePartner());
  }
  
  

  public ServicePartner elviaServicePartner() {
    ServicePartner serviceProvider = new ServicePartner("::id::");
    serviceProvider.setPartnerCode("");
    serviceProvider.setPartnerName("");
    serviceProvider.setFormat(Format.ELVIA);
    return serviceProvider;
  }

  protected String getAbsolutePath(String relativePath) {
    try {
      return new File(AbstractTelepassIntTest.class.getResource(relativePath).toURI()).getAbsolutePath();
    } catch (URISyntaxException e) {
      throw new RuntimeException("Cannot find file in given path!", e);
    }
  }

  //@Configuration
  @Import(MockedNavServicesConfiguration.class)
  @EnableAspectJAutoProxy
  public static class ConsumiPerformanceConfiguration {

    @Bean
    @Primary
    public AnagaziendeService anagaziendeService() {
      return mock(AnagaziendeService.class);
    }

    @Bean
    @Primary
    public NavSupplierCodesAdapter navSupplierCodesAdapter() {
      return mock(NavSupplierCodesAdapter.class);
    }

    @Bean
    @Primary
    public SchedulerConfiguration schedulerConfiguration() {
      return mock(SchedulerConfiguration.class);
    }

    @Bean
    @Primary
    public Liquibase liquibase() {
      return mock(Liquibase.class);
    }

    @Bean
    @Primary
    protected ElviaDescriptor elviaDescriptor() {
      return new ElviaDescriptor();
    }

    @Bean
    @Primary
    public ElviaJob elviaJob(PlatformTransactionManager transactionManager, StanziamentiParamsValidator stanziamentiParamsValidator, NotificationService notificationService, RecordPersistenceService persistenceService, ElviaDescriptor recordDescriptor, ElviaProcessor defaultProcessor, ElviaRecordConsumoMapper defaultRecordConsumoMapper, ElviaGenericoProcessor consumoGenericoProcessor, ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper, StanziamentiToNavPublisher stanziamentiToNavJmsProducer, NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {
      return new ElviaJobForPerformanceTest(transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, defaultProcessor, defaultRecordConsumoMapper, consumoGenericoProcessor, recordConsumoGenericoMapper, stanziamentiToNavJmsProducer, notConfirmedTemporaryAllocationsService);
    }

    public class ElviaJobForPerformanceTest extends ElviaJob {

      public ElviaJobForPerformanceTest(PlatformTransactionManager transactionManager, StanziamentiParamsValidator stanziamentiParamsValidator, NotificationService notificationService, RecordPersistenceService persistenceService, ElviaDescriptor recordDescriptor, ElviaProcessor defaultProcessor, ElviaRecordConsumoMapper defaultRecordConsumoMapper, ElviaGenericoProcessor consumoGenericoProcessor, ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper, StanziamentiToNavPublisher stanziamentiToNavJmsProducer, NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {
        super(transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, defaultProcessor, defaultRecordConsumoMapper, consumoGenericoProcessor, recordConsumoGenericoMapper, stanziamentiToNavJmsProducer, notConfirmedTemporaryAllocationsService);
      }

      @Override
      protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {}
    }
  }
  
  // CLEAN DB
  /*
   
truncate table faiconsumi.dbo.dettagliostanziamento_stanziamento;
delete from faiconsumi.dbo.stanziamento;
delete from faiconsumi.dbo.dettaglio_stanziamento_carburanti;
delete from faiconsumi.dbo.dettaglio_stanziamento_generico;
delete from faiconsumi.dbo.dettaglio_stanziamento_pedaggi;
delete from faiconsumi.dbo.dettaglio_stanziamento;
   
   */
}


