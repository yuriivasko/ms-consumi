package it.fai.ms.consumi.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.AbstractCommonTest;
import it.fai.ms.consumi.repository.CaselliEntityRepository;
import it.fai.ms.consumi.repository.caselli.model.CaselliEntity;
import it.fai.ms.consumi.repository.caselli.model.CaselliEntityMapper;
import it.fai.ms.consumi.service.CaselliService;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@EnableJpaRepositories({ "it.fai.ms.consumi.repository" })
@Tag("unit")
@Transactional
public class CaselliServiceImplTest extends AbstractCommonTest {

  @Autowired CaselliEntityRepository caselliEntityRepository;

  CaselliService caselliService;

  Instant now = Instant.now();

  @Test
  public void findByGateIdentifier() throws Exception {    
    Long id = 0L;
    String gate = RandomString();
    long size_before = caselliEntityRepository.count(); 

    CaselliEntity entity = addGate(gate, "20991108");    
    assertThat(size_before + 1).isEqualTo(caselliEntityRepository.count());

    entity = addGate(gate, "20181008");    
    assertThat(size_before + 2).isEqualTo(caselliEntityRepository.count());    

    entity = addGate(gate, now.plus(2, ChronoUnit.DAYS));    
    assertThat(size_before + 3).isEqualTo(caselliEntityRepository.count());    

    id = entity.getId();

    entity = addGate(gate, now.plus(5, ChronoUnit.DAYS)); 
    assertThat(size_before + 4).isEqualTo(caselliEntityRepository.count());    

    entity = addGate(gate, "20171008");    
    assertThat(size_before + 5).isEqualTo(caselliEntityRepository.count());    
        
    entity = addGate(gate, "");    
    assertThat(size_before + 6).isEqualTo(caselliEntityRepository.count());    

    entity = caselliService.findByGateIdentifier(gate, now);

    assertThat(entity).isNotNull();
    assertThat(entity.getId()).isEqualTo(id);
    assertThat(entity.getGateIdentifier()).isEqualTo(gate);
    assertThat(entity.getGlobalGateIdentifier()).isEqualTo(gate);

    caselliEntityRepository.deleteByGateIdentifier(gate);
    assertThat(size_before).isEqualTo(caselliEntityRepository.count());
  }

  @Test
  public void findByGlobalGateIdentifier() throws Exception {    
    Long id = 0L;
    String gate = RandomString();
    long size_before = caselliEntityRepository.count(); 

    CaselliEntity entity = addGate(gate, "20991108");    
    assertThat(size_before + 1).isEqualTo(caselliEntityRepository.count());

    entity = addGate(gate, "20181008");    
    assertThat(size_before + 2).isEqualTo(caselliEntityRepository.count());    

    entity = addGate(gate, now.plus(2, ChronoUnit.DAYS));    
    assertThat(size_before + 3).isEqualTo(caselliEntityRepository.count());    

    id = entity.getId();

    entity = addGate(gate, now.plus(5, ChronoUnit.DAYS)); 
    assertThat(size_before + 4).isEqualTo(caselliEntityRepository.count());    

    entity = addGate(gate, "20171008");    
    assertThat(size_before + 5).isEqualTo(caselliEntityRepository.count());    
        
    entity = addGate(gate, "");    
    assertThat(size_before + 6).isEqualTo(caselliEntityRepository.count());    

    entity = caselliService.findByGlobalGateIdentifier(gate, now);

    assertThat(entity).isNotNull();
    assertThat(entity.getId()).isEqualTo(id);
    assertThat(entity.getGateIdentifier()).isEqualTo(gate);
    assertThat(entity.getGlobalGateIdentifier()).isEqualTo(gate);

    caselliEntityRepository.deleteByGateIdentifier(gate);
    assertThat(size_before).isEqualTo(caselliEntityRepository.count());
  }
  
  @Test
  public void findByGateIdentifier2() throws Exception {
    Long id = 0L;
    String gate = RandomString();
    long size_before = caselliEntityRepository.count(); 

    CaselliEntity entity = addGate(gate, "");    
    assertThat(size_before + 1).isEqualTo(caselliEntityRepository.count());

    id = entity.getId();

    entity = caselliService.findByGateIdentifier(gate, Instant.now());

    assertThat(entity).isNotNull();
    assertThat(entity.getId()).isEqualTo(id);
    assertThat(entity.getGateIdentifier()).isEqualTo(gate);
    assertThat(entity.getGlobalGateIdentifier()).isEqualTo(gate);

    caselliEntityRepository.deleteByGateIdentifier(gate);
    assertThat(size_before).isEqualTo(caselliEntityRepository.count());
  }

  @Test
  public void findByGateIdentifier3() throws Exception {
    String gate = RandomString();
    long size_before = caselliEntityRepository.count(); 

    CaselliEntity entity = addGate(gate, now.minus(2, ChronoUnit.DAYS));    
    assertThat(size_before + 1).isEqualTo(caselliEntityRepository.count());

    entity = caselliService.findByGateIdentifier(gate, Instant.now());

    assertThat(entity).isNull();

    caselliEntityRepository.deleteByGateIdentifier(gate);
    assertThat(size_before).isEqualTo(caselliEntityRepository.count());
  }

  private CaselliEntity addGate(String gate, String endDate) {
    CaselliEntity entity = new CaselliEntity()
        .gateIdentifier(gate)
        .globalGateIdentifier(gate)
        .motorways(RandomString())
        .gateNumber(RandomString())
        .endDate(endDate);
    return caselliEntityRepository.save(entity);    
  }

  private CaselliEntity addGate(String gate, Instant endDate) {
    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd").withZone(ZoneId.systemDefault());    

    CaselliEntity entity = new CaselliEntity()
        .gateIdentifier(gate)
        .globalGateIdentifier(gate)
        .motorways(RandomString())
        .gateNumber(RandomString())
        .endDate(DATE_TIME_FORMATTER.format(endDate));
    return caselliEntityRepository.save(entity);    
  }

  @BeforeEach
  public void setUp() throws Exception {
    caselliService = new CaselliServiceImpl(caselliEntityRepository, new CaselliEntityMapper());
  }

}
