package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.extractProperty;
import static org.junit.jupiter.api.Assertions.*;

import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;

import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

/*
  public static void main(String[] args) {
    final Class<Supplier> clazz = Supplier.class;
    List<PropertyDescriptor> list = Arrays.asList(BeanUtils.getPropertyDescriptors(clazz));
    list.stream()
        .filter(property -> property.getReadMethod() != null)
        .filter(propertyDescriptor -> propertyDescriptor.getReadMethod().getName() != "getClass")
        .forEach(property -> {
          try {
            final String name = property.getReadMethod().getName();
            System.out.println(String.format("        assertThat(mapped%1$s.%2$s())\n" + "            .isEqualTo(domain%1$s.%2$s());", clazz.getSimpleName(),  name));
          } catch (Exception e) {}
    });
  }
  */
class StanziamentiParamsEntityMapperTest {

    StanziamentiParamsEntityMapper stanziamentiParamsEntityMapper = new StanziamentiParamsEntityMapper();
    StanziamentiParams domain;
    StanziamentiParamsEntity entity;

    @BeforeEach
    void setUp() {
        domain = StanziamentiParamsTestBuilder.newInstance()
                                              .withSupplierCode("::supplierCode::")
                                              .withArticle(ArticleTestBuilder.newInstance()
                                                              .withCode("::articleCode-1::")
                                                              .withCountry("::country::")
                                                              .withVatRate(22.0f)
                                                              .withGrouping("::grouping::")
                                                              .withMeasurementUnit("::measurementUnit::")
                                                              .build())
                                              .withTransactionType("::transactionType::")
                                              .withFareClass("::fareClass::")
                                              .withSource("::source-1::")
                                              .withTransactionDetail("::transactionDetail::")
                                              .withCostoRicavo(CostoRicavo.C)
                                              .withRecordCode("::recordCode-1::")
                                              .withStanzimentiDetailsType(StanziamentiDetailsType.PEDAGGI)
                                              .build();

        entity = StanziamentiParamsEntityTestBuilder.newInstance()
                                                    .withArticleCode("::articleCode-1::")
                                                    .withSupplierCode("::supplierCode::")
                                                    .withTransactionType("::transactionType::")
                                                    .withMeasurementUnit("::measurementUnit::")
                                                    .withFareClass("::fareClass::")
                                                    .withSource("::source-1::")
                                                    .withTransactionDetail("::transactionDetail::")
                                                    .withRecordCode("::recordCode-1::")
                                                    .withVatRate(22.0f)
                                                    .withGrouping("::grouping::")
                                                    .build();
    }


    @Test
    void toDomain() {
        StanziamentiParams mappedDomain = stanziamentiParamsEntityMapper.toDomain(entity);

        var mappedArticle = mappedDomain.getArticle();
        var domainArticle = domain.getArticle();
        assertThat(mappedArticle.getClass())
            .isEqualTo(domainArticle.getClass());
        assertThat(mappedArticle.getCode())
            .isEqualTo(domainArticle.getCode());
        assertThat(mappedArticle.getCountry())
            .isEqualTo(domainArticle.getCountry());
        assertThat(mappedArticle.getCurrency())
            .isEqualTo(domainArticle.getCurrency());
        assertThat(mappedArticle.getDescription())
            .isEqualTo(domainArticle.getDescription());
        assertThat(mappedArticle.getGrouping())
            .isEqualTo(domainArticle.getGrouping());
        assertThat(mappedArticle.getMeasurementUnit())
            .isEqualTo(domainArticle.getMeasurementUnit());
        assertThat(mappedArticle.getVatRate())
            .isEqualTo(domainArticle.getVatRate());


        assertThat(mappedDomain.isCosto())
            .isEqualTo(domain.isCosto());
        assertThat(mappedDomain.getCostoRicavo())
            .isEqualTo(domain.getCostoRicavo());
        assertThat(mappedDomain.getFareClass())
            .isEqualTo(domain.getFareClass());
        assertThat(mappedDomain.getRecordCode())
            .isEqualTo(domain.getRecordCode());
        assertThat(mappedDomain.isRicavo())
            .isEqualTo(domain.isRicavo());
        assertThat(mappedDomain.getSource())
            .isEqualTo(domain.getSource());
        assertThat(mappedDomain.getStanziamentiDetailsType())
            .isEqualTo(domain.getStanziamentiDetailsType());

        var mappedSupplier = mappedDomain.getSupplier();
        var domainSupplier = domain.getSupplier();
        assertThat(mappedSupplier.getCode())
            .isEqualTo(domainSupplier.getCode());
        assertThat(mappedSupplier.getCodeLegacy())
            .isEqualTo(domainSupplier.getCodeLegacy());
        assertThat(mappedSupplier.getDocument())
            .isEqualTo(domainSupplier.getDocument());

        assertThat(mappedDomain.getTransactionDetail())
            .isEqualTo(domain.getTransactionDetail());
        assertThat(mappedDomain.getTransactionType())
            .isEqualTo(domain.getTransactionType());
        assertThat(mappedDomain.getUuid())
            .isEqualTo(domain.getUuid());
    }

    @Test
    void toEntity() {
        StanziamentiParamsEntity mapped = stanziamentiParamsEntityMapper.toEntity(domain);

        var mappedStanziamentiParamsArticleEntity = mapped.getArticle();
        var domainStanziamentiParamsArticleEntity = entity.getArticle();
        assertThat(mappedStanziamentiParamsArticleEntity.getCode())
            .isEqualTo(domainStanziamentiParamsArticleEntity.getCode());
        assertThat(mappedStanziamentiParamsArticleEntity.getDescription())
            .isEqualTo(domainStanziamentiParamsArticleEntity.getDescription());

        assertThat(mapped.getCostoRicavo())
            .isEqualTo(entity.getCostoRicavo());
        assertThat(mapped.getCountry())
            .isEqualTo(entity.getCountry());
        assertThat(mapped.getCurrency())
            .isEqualTo(entity.getCurrency());
        assertThat(mapped.getFareClass())
            .isEqualTo(entity.getFareClass());
        assertThat(mapped.getGrouping())
            .isEqualTo(entity.getGrouping());
        assertThat(mapped.getId())
            .isEqualTo(entity.getId());
        assertThat(mapped.getMeasurementUnit())
            .isEqualTo(entity.getMeasurementUnit());
        assertThat(mapped.getRecordCode())
            .isEqualTo(entity.getRecordCode());
        assertThat(mapped.getSource())
            .isEqualTo(entity.getSource());
        assertThat(mapped.getStanziamentiDetailsType())
            .isEqualTo(entity.getStanziamentiDetailsType());
        assertThat(mapped.getSupplier())
            .isEqualTo(entity.getSupplier());
        assertThat(mapped.getTransactionDetail())
            .isEqualTo(entity.getTransactionDetail());
        assertThat(mapped.getTransactionType())
            .isEqualTo(entity.getTransactionType());
        assertThat(mapped.getVatRate())
            .isEqualTo(entity.getVatRate());

    }


}
