package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

import java.util.Currency;

public class StanziamentiParamsEntityTestBuilder {

  public static StanziamentiParamsEntityTestBuilder newInstance() {
    return new StanziamentiParamsEntityTestBuilder();
  }

  private StanziamentiParamsEntity stanziamentiParams;

  private StanziamentiParamsEntityTestBuilder() {
  }

  public StanziamentiParamsEntity build() {
    stanziamentiParams.setCurrency(Currency.getInstance("EUR"));
    stanziamentiParams.setCostoRicavo(StanziamentiParamsCostoRicavoEntity.C);
    stanziamentiParams.setCountry("::country::");
    if(stanziamentiParams.getStanziamentiDetailsType() == null) {
      stanziamentiParams.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.PEDAGGI);
    }
    return stanziamentiParams;
  }

  public StanziamentiParamsEntityTestBuilder withArticleCode(final String _articleCode) {
    stanziamentiParams = new StanziamentiParamsEntity(StanziamentiParamsArticleEntityTestBuilder.newInstance()
                                                                                                .withCode(_articleCode)
                                                                                                .build());
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withVatRate(final float _vatRate) {
    stanziamentiParams.setVatRate(_vatRate);
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withGrouping(final String _grouping) {
    stanziamentiParams.setGrouping(_grouping);
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withMeasurementUnit(final String _measurementUnit) {
    stanziamentiParams.setMeasurementUnit(_measurementUnit);
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withSupplierCode(final String _supplierCode) {
    stanziamentiParams.setSupplier(StanziamentiParamsSupplierEntityTestBuilder.newInstance()
                                                                              .withCode(_supplierCode)
                                                                              .build());
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withTransactionType(final String _transactionType) {
    stanziamentiParams.setTransactionType(_transactionType);
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withFareClass(final String _fareClass) {
    stanziamentiParams.setFareClass(_fareClass);
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withSource(final String _source) {
    stanziamentiParams.setSource(_source);
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withTransactionDetail(final String _transactionDetail) {
    stanziamentiParams.setTransactionDetail(_transactionDetail);
    return this;
  }

  public StanziamentiParamsEntityTestBuilder withRecordCode(final String _recordCode) {
    stanziamentiParams.setRecordCode(_recordCode);
    return this;
  }
  
  public StanziamentiParamsEntityTestBuilder withStanziamentiDetailsType(final StanziamentiDetailsTypeEntity _detailsType) {
    stanziamentiParams.setStanziamentiDetailsType(_detailsType);
    return this;
  }

}
