package it.fai.ms.consumi.testutils;

import static org.assertj.core.api.Assertions.tuple;

import java.util.function.Consumer;

import org.assertj.core.groups.Tuple;

import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

public class TestUtils {


  public static Tuple fromStanziamentoToTuple(StanziamentoEntity s){
    return tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getStatoStanziamento(), s.getTarga(), s.getPaese(), s.getCosto(),s.getPrezzo(), s.getClasseVeicoloEuro(),s.getValuta());
  }

  public static <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }
}
