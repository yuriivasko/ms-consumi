package it.fai.ms.consumi.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import it.fai.ms.consumi.FaiconsumiApp;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.enumeration.PriceSource;
import it.fai.ms.consumi.repository.ServiceProviderRepository;

@Tag("integration")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = FaiconsumiApp.class)
@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class ServiceProviderResourceExtTest {

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private ServiceProviderRepository repository;

  private MockMvc mockMvc;
  
  private final static String PROVIDER_CODE_1 = "F004664";
  private final static String PROVIDER_CODE_2 = "F007887";
  
  private final static String PROVIDER_NAME_1 = "PROVIDER_1";
  private final static String PROVIDER_NAME_2 = "PROVIDER_2";

  @BeforeEach
  void setUp() throws Exception {
    ServiceProviderResourceExt resource = new ServiceProviderResourceExt(repository);
    this.mockMvc = MockMvcBuilders.standaloneSetup(resource)
                                  .build();
    
    createAndPersistServiceProvider(PROVIDER_CODE_1, PROVIDER_NAME_1, PriceSource.by_prices_table);
    createAndPersistServiceProvider(PROVIDER_CODE_2, PROVIDER_NAME_2, PriceSource.by_invoice);
  }

  private void createAndPersistServiceProvider(String providerCode, String providerName, PriceSource priceSource) {
    ServiceProvider sp = new ServiceProvider();
    sp.serviceCode("000001")
      .serviceName("OIL")
      .format(Format.FAISERVICE)
      .filePath("")
      .providerCode(providerCode)
      .providerName(providerName)
      .priceSource(priceSource);
    entityManager.persist(sp);
  }

  @Test
  void testFindServiceProviderToPriceTable() throws Exception {
    MvcResult resultMock = mockMvc.perform(get("/api" + ServiceProviderResourceExt.SERVICE_PROVIDER_PRICETABLE))
                                  .andExpect(status().isOk())
                                  .andReturn();

    String content = resultMock.getResponse()
                               .getContentAsString();
    
    assertThat(content).contains(PROVIDER_CODE_1, PROVIDER_NAME_1);
    assertThat(content).doesNotContain(PROVIDER_CODE_2, PROVIDER_NAME_2);
  }

}
