package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import static it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconDescriptor.BEGIN_15;
import static org.assertj.core.api.Assertions.assertThat;

import javax.money.MonetaryAmount;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;

@Tag("unit")
public class VaconDescriptorTest {

  private String recordDataGermany1 = "1527600001DE-6/A             00000000002438411840           0004900000077583926921095620181005000313243000313243+700579211164500002620181005211101978AR11BJU             6425 050000000001111840           2018100521110100000000011L200031324300000000001";

  @Test
  void testElceuDescriptor_Record15(){
    //record 1 is half down (last digit is 0)
    {
      VaconDescriptor vaconDescriptor = new VaconDescriptor();

      VaconRecord vaconRecord = vaconDescriptor.decodeRecordCodeAndCallSetFromString(recordDataGermany1, BEGIN_15, "DA06A284.VACON.TEST", 1);

      MonetaryAmount amountNoVat = vaconRecord.getAmount_novat();
      assertThat(amountNoVat.getNumber().doubleValue()).isEqualTo(0.11d);

      MonetaryAmount amountVat = vaconRecord.getAmount_vat();
      assertThat(amountVat).isNull();

      assertThat(vaconRecord.getTripId()).isEqualTo("11840");

      assertThat(vaconRecord.getTotalAmount()).isEqualTo("00000000011");

      assertThat(vaconRecord.getVATRate()).isEqualTo("0");

      assertThat(vaconRecord.getAmountWithoutVAT()).isEqualTo(vaconRecord.getTotalAmount());
    }
  }
}
