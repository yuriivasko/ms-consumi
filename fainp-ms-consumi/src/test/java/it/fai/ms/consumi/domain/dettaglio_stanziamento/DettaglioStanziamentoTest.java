package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.AmountTestBuilder;

@Tag("unit")
class DettaglioStanziamentoTest {

  @Test
  void amount_is_the_same() {

    // given

    var tempAllocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                               .withAmount(AmountTestBuilder.newInstance()
                                                                                            .withAmountExcludedVat(Money.of(new BigDecimal("10"),
                                                                                                                            "EUR"))
                                                                                            .withAmountIncludedVat(Money.of(new BigDecimal("11"),
                                                                                                                            "EUR"))
                                                                                            .withVatRate(new BigDecimal("10"))
                                                                                            .withExchangeRate(new BigDecimal("1.62"))
                                                                                            .build())
                                                               .withInvoiceTypeP()
                                                               .build();

    var lastAllocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                               .withAmount(AmountTestBuilder.newInstance()
                                                                                            .withAmountExcludedVat(Money.of(new BigDecimal("10"),
                                                                                                                            "EUR"))
                                                                                            .withAmountIncludedVat(Money.of(new BigDecimal("11"),
                                                                                                                            "EUR"))
                                                                                            .withVatRate(new BigDecimal("10"))
                                                                                            .withExchangeRate(new BigDecimal("1.62"))
                                                                                            .build())
                                                               .withInvoiceTypeD()
                                                               .build();

    // when

    boolean sameAmount = tempAllocationDetail.hasTheSameAmount(lastAllocationDetail);

    // then

    assertThat(sameAmount).isTrue();
  }

  @Test
  void amount_isnt_the_same() {

    // given

    var tempAllocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                               .withAmount(AmountTestBuilder.newInstance()
                                                                                            .withAmountExcludedVat(Money.of(new BigDecimal("10"),
                                                                                                                            "EUR"))
                                                                                            .withAmountIncludedVat(Money.of(new BigDecimal("11"),
                                                                                                                            "EUR"))
                                                                                            .withVatRate(new BigDecimal("10"))
                                                                                            .withExchangeRate(new BigDecimal("1.62"))
                                                                                            .build())
                                                               .withInvoiceTypeP()
                                                               .build();

    var lastAllocationDetail = DettaglioStanziamentoTestBuilder.newInstance()
                                                               .withAmount(AmountTestBuilder.newInstance()
                                                                                            .withAmountExcludedVat(Money.of(new BigDecimal("20"),
                                                                                                                            "EUR"))
                                                                                            .withAmountIncludedVat(Money.of(new BigDecimal("22"),
                                                                                                                            "EUR"))
                                                                                            .withVatRate(new BigDecimal("10"))
                                                                                            .withExchangeRate(new BigDecimal("1.62"))
                                                                                            .build())
                                                               .withInvoiceTypeD()
                                                               .build();

    // when

    boolean sameAmount = tempAllocationDetail.hasTheSameAmount(lastAllocationDetail);

    // then

    assertThat(sameAmount).isFalse();
  }

}
