package it.fai.ms.consumi.job;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import io.github.glytching.junit.extension.folder.TemporaryFolder;
import io.github.glytching.junit.extension.folder.TemporaryFolderExtension;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitJob;

@Tag("unit")
@ExtendWith(TemporaryFolderExtension.class)
class ServiceProviderScheduledTaskTest {


  private String root;
  private final AbstractJob                                                 unorderedFileJob = mock(AbstractJob.class);
  private final AbstractJob                                                 orderedFileJob = (AbstractJob) mock(ElcitJob.class);

  @BeforeEach
  public void setUp(TemporaryFolder temporaryFolder) throws IOException {

    List<String> fileList = Arrays.asList(
      "DA06A284.ELCIT.FAI.1",
      "DA06A284.ELCIT.FAI.6",
      "DA06A284.ELCIT.FAI.3",
      "DA06A284.ELCIT.FAI.4",
      "DA06A284.ELCIT.FAI.2",
      "DA06A284.ELCIT.FAI.5"
    );

    this.root = temporaryFolder.getRoot().getAbsolutePath();

    for (String fileName : fileList) {
      temporaryFolder.createFile(fileName);
    }

    given(unorderedFileJob.getJobQualifier())
      .willReturn(Format.FAISERVICE);

    given(unorderedFileJob.orderFileList(any()))
      .willCallRealMethod();

    given(orderedFileJob.getJobQualifier())
      .willReturn(Format.FAISERVICE);

    given(orderedFileJob.orderFileList(any()))
      .willCallRealMethod();
  }

  @Test
  void run_multiple_files_in_without_order() throws IOException {
    final ServiceProvider serviceProvider = new ServiceProvider();
    serviceProvider.setFilePath(".");
    serviceProvider.setFileMatchRegex("(?i:.*ELCIT.*)");
    serviceProvider.setFormat(Format.FAISERVICE);

    ServiceProviderScheduledTask serviceProviderScheduledTask = new ServiceProviderScheduledTask( new JobRegistry(Set.of(unorderedFileJob)), root);

    serviceProviderScheduledTask.run(serviceProvider);

    ArgumentCaptor<String> fileNameCaptor = ArgumentCaptor.forClass(String.class);
    then(unorderedFileJob)
      .should(atLeastOnce())
      .process(fileNameCaptor.capture(), anyLong(), any(Instant.class), any(ServicePartner.class)); //don't know why but not work with any()

    assertThat(fileNameCaptor.getAllValues())
      .containsExactlyInAnyOrder(
        root + "/" +"DA06A284.ELCIT.FAI.1.processing",
        root + "/" +"DA06A284.ELCIT.FAI.2.processing",
        root + "/" +"DA06A284.ELCIT.FAI.3.processing",
        root + "/" +"DA06A284.ELCIT.FAI.4.processing",
        root + "/" +"DA06A284.ELCIT.FAI.5.processing",
        root + "/" +"DA06A284.ELCIT.FAI.6.processing"
      );
  }

  @Test
  void run_multiple_files_in_deterministic_order() throws IOException {

    final ServiceProvider serviceProvider = new ServiceProvider();
    serviceProvider.setFilePath(".");
    serviceProvider.setFileMatchRegex("(?i:.*ELCIT.*)");
    serviceProvider.setFormat(Format.FAISERVICE);

    ServiceProviderScheduledTask serviceProviderScheduledTask = new ServiceProviderScheduledTask(new JobRegistry(Set.of(orderedFileJob)), root);

    serviceProviderScheduledTask.run(serviceProvider);

    Mockito.validateMockitoUsage();

    ArgumentCaptor<String> fileNameCaptor = ArgumentCaptor.forClass(String.class);
    then(orderedFileJob)
      .should(atLeastOnce())
      .process(fileNameCaptor.capture(), anyLong(), any(Instant.class), any(ServicePartner.class)); //don't know why but not work with any()

    assertThat(fileNameCaptor.getAllValues())
      .containsExactly(
        root + "/" +"DA06A284.ELCIT.FAI.1.processing",
        root + "/" +"DA06A284.ELCIT.FAI.2.processing",
        root + "/" +"DA06A284.ELCIT.FAI.3.processing",
        root + "/" +"DA06A284.ELCIT.FAI.4.processing",
        root + "/" +"DA06A284.ELCIT.FAI.5.processing",
        root + "/" +"DA06A284.ELCIT.FAI.6.processing"
      );
  }
}
