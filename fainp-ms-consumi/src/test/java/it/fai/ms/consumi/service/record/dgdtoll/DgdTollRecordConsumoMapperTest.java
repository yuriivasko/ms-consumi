package it.fai.ms.consumi.service.record.dgdtoll;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import static java.time.temporal.ChronoField.*;
import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
class DgdTollRecordConsumoMapperTest {

  DgdTollRecordConsumoMapper dgdTollRecordConsumoMapper  = new DgdTollRecordConsumoMapper();
  private DgdTollRecord dgdTollRecord;

  @BeforeEach
  public void setUp(){
    dgdTollRecord = newDgdTollRecord();
  }

  private DgdTollRecord newDgdTollRecord() {
    DgdTollRecord dgdTollRecord = new DgdTollRecord("FAI_RE170629-02.TXT", -1);
    //N4181Chiasso Brogeda M.  4182Brogeda Autostrada  20170629150623201706301439597896970006618900135=2512 CHF+0000464.20+0000000.00+00.00+0000464.2041818145276       000001
    dgdTollRecord.setRecordCode("DEFAULT");
    dgdTollRecord.setIngestion_time(Instant.EPOCH);
    dgdTollRecord.setTollPointCodeEntry("4181");
    dgdTollRecord.setTollPointNameEntry("Chiasso Brogeda M.");
    dgdTollRecord.setTollPointCodeExit("4182");
    dgdTollRecord.setTollPointNameExit("Brogeda Autostrada");
    dgdTollRecord.setDateIn("20170629");
    dgdTollRecord.setTimeIn("150623");
    dgdTollRecord.setDateOut("20170630");
    dgdTollRecord.setTimeOut("143959");
    dgdTollRecord.setCardNumber("7896970006618900135");
    dgdTollRecord.setCurrency("CHF");
    dgdTollRecord.setSign("+");
    dgdTollRecord.setTaxableAmount("0000464.20");
    dgdTollRecord.setTaxAmount("0000000.00");
    dgdTollRecord.setTaxRate("00.00");
    dgdTollRecord.setTaxedAmount("0000464.20");
    dgdTollRecord.setDocumentNumber("41818145276");
    dgdTollRecord.setTotalTaxableAmount(null);
    dgdTollRecord.setTotalTaxAmount(null);
    dgdTollRecord.setTotalTaxedAmount(null);

    dgdTollRecord.setCardNumber("7896970006618900135");
    dgdTollRecord.setCounter("000001");

    dgdTollRecord.setCreationDateInFile("20170629");
    dgdTollRecord.setCreationTimeInFile("000000");

    dgdTollRecord.setRecordCode("DEFAULT");
    return dgdTollRecord;
  }

  @Test
  void mapRecordToConsumo() throws Exception {
    DgdToll consumo = dgdTollRecordConsumoMapper.mapRecordToConsumo(newDgdTollRecord());
    assertThat(consumo.getSource().getAcquisitionDate())
      .isEqualTo(Instant.parse("2017-06-28T22:00:00Z"));
    assertThat(consumo.getAdditionalInfo())
      .isEqualTo("41818145276");
    assertThat(consumo.getAmount().getAmountExcludedVat())
      .isEqualTo(MonetaryUtils.strToMonetary("0000464.20", "CHF"));
    assertThat(consumo.getAmount().getVatRateBigDecimal())
      .isEqualTo(new BigDecimal("0").setScale(2, RoundingMode.HALF_UP));
    //    assertThat(consumo.getVatRateBigDecimal())
    //      .isEqualTo(new BigDecimal("0").setScale(2, RoundingMode.HALF_UP));
    assertThat(consumo.getNavSupplierCode())
      .isNull();
    assertThat(consumo.getCompensationNumber())
      .isNull();
    assertThat(consumo.getContract().isPresent())
      .isFalse();
    assertThat(consumo.getDate())
      .isEqualTo(calculateInstantByDateAndTime("20170630", "143959", "yyyyMMdd", "HHmmss").get());
    assertThat(consumo.getDevice().getId())
      .isEqualTo("7896970006618900135");

    assertThat(consumo.getDevice().getServiceType())
      .isEqualTo(TipoServizioEnum.PEDAGGI_SVIZZERA);

    assertThat(consumo.getEntryDate())
      .isNull();
    assertThat(consumo.getEntryTime())
      .isNull();
    assertThat(consumo.getEseTotStatus())
      .isNull();
    assertThat(consumo.getExitTransitDateTime())
      .isEqualTo("20170630143959");
    assertThat(consumo.getGlobalIdentifier().getId())
      .isNotNull();
    assertThat(consumo.getGroupArticlesNav())
      .isNull();
    assertThat(consumo.getImponibileAdr())
      .isNull();
    assertThat(consumo.getInvoiceType())
      .isEqualTo(InvoiceType.D);
    assertThat(consumo.getLaneId())
      .isNull();
    assertThat(consumo.getNetworkCode())
      .isNull();
    assertThat(consumo.getPartnerCode())
      .isNull();
    assertThat(consumo.getProcessed())
      .isFalse();
    assertThat(consumo.getRecordCode())
      .isEqualTo("DEFAULT");
    assertThat(consumo.getRecordCodeToLookInParamStanz())
      .isEqualTo("DEFAULT");
    assertThat(consumo.getRegion())
      .isNull();
    assertThat(consumo.getRoadType())
      .isNull();
    assertThat(consumo.getRoute())
      .isNull();
    assertThat(consumo.getRouteName())
      .isEqualTo("Chiasso Brogeda M. - Brogeda Autostrada");
    assertThat(consumo.getServicePartner())
      .isNull();
    assertThat(consumo.getSource().getFileName())
      .isEqualTo("FAI_RE170629-02.TXT");
    assertThat(consumo.getSource().getIngestionTime())
      .isEqualTo(Instant.parse("1970-01-01T00:00:00Z"));
    assertThat(consumo.getSource().getRowNumber())
      .isEqualTo(-1);
    assertThat(consumo.getSource().getType())
      .isEqualTo("DGD");
//    assertThat(consumo.getSourceType())
//      .isEqualTo("");
    assertThat(consumo.getSplitNumber())
      .isNull();
    assertThat(consumo.getTollCharger())
      .isNull();
    assertThat(consumo.getTollGate())
      .isNull();
    assertThat(consumo.getTollPointEntry().getGlobalGate().getId())
      .isEqualTo("4181");
    assertThat(consumo.getTollPointEntry().getGlobalGate().getDescription())
      .isEqualTo("Chiasso Brogeda M.");
    assertThat(consumo.getTollPointEntry().getTime())
      .isEqualTo(calculateInstantByDateAndTime("20170629", "150623", "yyyyMMdd", "HHmmss").get());
    assertThat(consumo.getTollPointExit().getGlobalGate().getId())
      .isEqualTo("4182");
    assertThat(consumo.getTollPointExit().getGlobalGate().getDescription())
      .isEqualTo("Brogeda Autostrada");
    assertThat(consumo.getTollPointExit().getTime())
      .isEqualTo(calculateInstantByDateAndTime("20170630", "143959", "yyyyMMdd", "HHmmss").get());
    assertThat(consumo.getTotalAmount())
      .isNull();
    assertThat(consumo.getTransaction().getSign())
      .isEqualTo("+");
    assertThat(consumo.getTransaction().getDetailCode())
      .isEqualTo("");
//    assertThat(consumo.getTransactionDetailCode())
//      .isEqualTo("");
    assertThat(consumo.getTspExitCountryCode())
      .isNull();
    assertThat(consumo.getTspExitNumber())
      .isNull();
    assertThat(consumo.getTspRespCountryCode())
      .isNull();
    assertThat(consumo.getTspRespNumber())
      .isNull();
    assertThat(consumo.getVehicle())
      .isNull();
  }


  public static String formatInstantToDate(Instant instant) {
    DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
    LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    return formatter.format(ldt);
  }

  public static Instant parseDateToInstant(String time) {

    DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;

    LocalDateTime ldt = LocalDateTime.from(formatter.parse(time));
    return ldt.toInstant(ZoneOffset.UTC);
  }

  public static String formatInstantToHour(Instant instant) {

    DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendValue(HOUR_OF_DAY, 2)
                                                                .appendValue(MINUTE_OF_HOUR, 2)
                                                                .optionalStart()
                                                                .appendValue(SECOND_OF_MINUTE, 2)
                                                                .toFormatter();

    LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    return formatter.format(ldt);
  }

  public static Instant parseTimeToInstant(String time) {

    DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendValue(HOUR_OF_DAY, 2)
                                                                .appendValue(MINUTE_OF_HOUR, 2)
                                                                .optionalStart()
                                                                .appendValue(SECOND_OF_MINUTE, 2)
                                                                .toFormatter();

    LocalDateTime ldt = LocalDateTime.from(formatter.parse(time));
    return ldt.toInstant(ZoneOffset.UTC);
  }

  protected Optional<Instant> calculateInstantByDateAndTime(final String entryDate, final String entryTime, final String datePattern,
                                                            final String timePattern) throws ParseException {
    if (entryDate != null && !entryDate.isEmpty()) {
      if (entryTime != null && !entryTime.isEmpty() && (timePattern == null || timePattern.isEmpty()))
        throw new IllegalStateException("entryTime specified required not-null timepattern");
      String completePattern = datePattern
                               + (entryTime != null && !entryTime.isEmpty() && timePattern != null && !timePattern.isEmpty() ? timePattern
                                                                                                                             : "");
      String completeDateAndTime = entryDate + (entryTime != null ? entryTime : "");
      var dateFormat = new SimpleDateFormat(completePattern);
      var date = dateFormat.parse(completeDateAndTime);
      if (date.after(maxDate())) {
        return Optional.of(maxDate().toInstant());
      } else {
        return Optional.of(date.toInstant());
      }
    } else {
      return Optional.ofNullable(null);
    }
  }

  private Date maxDate() {
    final var calendar = GregorianCalendar.getInstance();
    calendar.set(9999, 11, 30, 23, 59);
    return calendar.getTime();
  }
}
