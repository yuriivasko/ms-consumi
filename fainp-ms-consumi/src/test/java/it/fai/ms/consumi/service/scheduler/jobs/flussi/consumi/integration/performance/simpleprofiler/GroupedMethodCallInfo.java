package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.performance.simpleprofiler;

import java.time.Instant;
import java.util.List;

public class GroupedMethodCallInfo {

  List<MethodCallInfo> methodCallInfos;

  public GroupedMethodCallInfo(List<MethodCallInfo> methodCallInfos) {
    this.methodCallInfos = methodCallInfos;
  }

  public int callCount(){
    return methodCallInfos.size();
  }

  public long avgDuration(){
    return totalDuration() / callCount();
  }

  public long totalDuration(){
    return methodCallInfos.stream().mapToLong(methodCallInfo -> methodCallInfo.duration()).sum();
  }

  public double totalPercentage(Instant t0, Instant tN){
    return methodCallInfos.stream().mapToDouble(methodCallInfo -> methodCallInfo.durationPercent(t0, tN)).sum();
  }

  public MethodCallInfo firstMethodCallInfo(){
    return methodCallInfos.get(0);
  }
}
