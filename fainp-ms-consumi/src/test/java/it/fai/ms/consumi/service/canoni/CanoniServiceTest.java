package it.fai.ms.consumi.service.canoni;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoniDispositivi;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoniServizi;
import it.fai.ms.consumi.job.ServiceProviderScheduledTask;
import it.fai.ms.consumi.repository.canoni.CanoniRepository;
import it.fai.ms.consumi.service.jms.listener.CanoniFatturazioneJmsListener;
import it.fai.ms.consumi.service.jms.producer.CanoniClienteJmsProducer;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;

@Disabled //slow test
@SpringBootTest
class CanoniServiceTest extends AbstractIntegrationTestSpringContext {

  @MockBean
  ServiceProviderScheduledTask tmp;

  CanoniFatturazioneJmsListener subject;
  @Autowired
  private CanoniRepository canoniRepository;

  @Autowired
  private CanoniClienteJmsProducer sender;

  @BeforeEach
  public void setup() {
   subject = new CanoniFatturazioneJmsListener(canoniRepository, sender);

  }

  @Test
  @Transactional
  void testDispositiviAttivi() {
    RichiestaCanoniDispositivi msg = new RichiestaCanoniDispositivi().identificativo(TipoDispositivoEnum.TELEPASS_ITALIANO);
    subject.process( msg , Instant.now().minus(60,ChronoUnit.DAYS), Instant.now()
                                                                               );
//    List<DispositivoForCanoni>   list              = dispositiviAttivi.collect(Collectors.toMinimalInformationList());
    //start at 2019-02-07 19:47:57.383
  }
  @Test
  @Transactional
  void testServiziAttivi() {
    RichiestaCanoniServizi msg = new RichiestaCanoniServizi().identificativo(TipoServizioEnum.PEDAGGI_ITALIA);
    subject.process( msg , Instant.EPOCH, Instant.now()
        );
//    List<DispositivoForCanoni>   list              = dispositiviAttivi.collect(Collectors.toMinimalInformationList());
    //start at 2019-02-07 19:47:57.383
  }


  /*
   * @Test void testServiziAttivi() { Stream<DispositivoForCanoni> dispositiviAttivi =
   * subject.canoniServizi(Instant.EPOCH, Instant.now(), TipoServizioEnum.PEDAGGI_FRANCIA); List<DispositivoForCanoni>
   * list = dispositiviAttivi.collect(Collectors.toMinimalInformationList()); list.forEach(d -> System.out.println(d)); }
   */
}
