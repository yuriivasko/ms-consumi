package it.fai.ms.consumi.service.processor.consumi.tunnel;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.FrejusJobProperties;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.FrejusGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.FrejusRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.FrejusRecordDescriptor;
import it.fai.ms.consumi.service.frejus.FrejusCustomerProvider;
import it.fai.ms.consumi.service.processor.consumi.frejus.FrejusConsumoDettaglioStanziamentoPedaggioMapper;
import it.fai.ms.consumi.service.record.tunnel.FrejusRecordConsumoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

class FrejusConsumoDettaglioStanziamentoPedaggioMapperTest {

  private static final FrejusCustomerProvider      customerProvider = mock(FrejusCustomerProvider.class);
  FrejusConsumoDettaglioStanziamentoPedaggioMapper subject;

  @BeforeEach
  private void setup() {
    subject = new FrejusConsumoDettaglioStanziamentoPedaggioMapper();
  }

  @Test
  void testMapConsumoToDettaglioStanziamentoMinimalData() {
    Frejus consumo = new Frejus(new Source(Instant.now(), Instant.now(), "frejus"), "",
                                                                                 Optional.of(new FrejusGlobalIdentifier("id123")), TestDummyRecord.instance1);
    consumo.setNavSupplierCode("code");
    DettaglioStanziamento mapConsumoToDettaglioStanziamento = subject.mapConsumoToDettaglioStanziamento(consumo);

    assertThat(mapConsumoToDettaglioStanziamento).isNotNull();
  }

  @Test
  void testChain() throws Exception {

    FrejusRecord record = getRecord();

    Frejus consumo = getConsumo(record);

    assertThat(consumo.getImponibileAdr()).isNotNull();
    assertThat(consumo.getImponibileAdr()).isEqualByComparingTo("0.10");

    consumo.setNavSupplierCode("frejus");

    DettaglioStanziamento dettaglioStanziamento = subject.mapConsumoToDettaglioStanziamento(consumo);

    verifyDettaglioStanziamento(dettaglioStanziamento);

  }

  @Test
  void testChainNegative() throws Exception {

    FrejusRecord record = getRecord();
    record.setSegnoImporto("-");

    Frejus consumo = getConsumo(record);

    consumo.setNavSupplierCode("frejus");

    DettaglioStanziamento dettaglioStanziamento = subject.mapConsumoToDettaglioStanziamento(consumo);

    assertThat(dettaglioStanziamento).isInstanceOf(DettaglioStanziamentoPedaggio.class);
    DettaglioStanziamentoPedaggio dsp = (DettaglioStanziamentoPedaggio) dettaglioStanziamento;
    assertThat(dsp.getAmount()
                  .getAmountIncludedVat()).isEqualTo(MonetaryUtils.strToMonetary("-27165", -2, "EUR"));
    assertThat(dsp.getAmount()
                  .getVatRateBigDecimal()).isEqualTo(BigDecimal.valueOf(2200, 2));
    assertThat(dsp.getAmount()
                  .getAmountExcludedVat()).isEqualTo(MonetaryUtils.strToMonetary("-22266393", -5, "EUR"));
    assertThat(dsp.getTransaction()
                  .getSign()).isEqualTo("-");
    assertThat(dsp.getInvoiceType()).isEqualTo(InvoiceType.D);
  }

  public static void verifyDettaglioStanziamento(DettaglioStanziamento dettaglioStanziamento) {
    assertThat(dettaglioStanziamento).isInstanceOf(DettaglioStanziamentoPedaggio.class);
    DettaglioStanziamentoPedaggio dsp = (DettaglioStanziamentoPedaggio) dettaglioStanziamento;
    assertThat(dsp.getVehicle()
                  .getFareClass()).isEqualTo("4");
    assertThat(dsp.getRecordCode()).isEqualTo("1");
    assertThat(dsp.getDevice()
                  .getPan()).isNull();
    assertThat(dsp.getAmount()
                  .getAmountIncludedVat()).isEqualTo(MonetaryUtils.strToMonetary("27165", -2, "EUR"));
    assertThat(dsp.getAmount()
                  .getVatRateBigDecimal()).isEqualTo(BigDecimal.valueOf(2200, 2));
    assertThat(dsp.getAmount()
                  .getAmountExcludedVat()).isEqualTo(MonetaryUtils.strToMonetary("22266393", -5, "EUR"));
    assertThat(dsp.getTransaction()
                  .getSign()).isEqualTo("+");
    assertThat(dsp.getInvoiceType()).isEqualTo(InvoiceType.D);
    assertThat(dsp.getTollPointEntry()
                  .getGlobalGate()
                  .getId()).isEqualTo("25053391");
    assertThat(dsp.getTollPointExit()
                  .getGlobalGate()
                  .getId()).isEqualTo("25002999");
    assertThat(dsp.getTollPointEntry()
                  .getGlobalGate()
                  .getDescription()).isEqualTo("Francia");
    assertThat(dsp.getTollPointExit()
                  .getGlobalGate()
                  .getDescription()).isEqualTo("vs Francia");
    assertThat(dsp.getTollPointExit()
                  .getTime()).isEqualTo(LocalDateTime.parse("2018-08-02T18:07:18")
                                                     .atZone(ZoneId.systemDefault())
                                                     .toInstant());
    assertThat(dsp.getSource()
                  .getType()).isEqualTo("SITAF");

    assertThat(dsp.getRouteName()).isEqualTo("Francia - vs Francia");

    assertThat(dsp.getImponibileAdr()).isEqualByComparingTo("0.10");
  }

  public static Frejus getConsumo(FrejusRecord record) throws Exception {
    ApplicationProperties properties = new ApplicationProperties();
    properties.setFrejusJobProperties(new FrejusJobProperties());
    properties.getFrejusJobProperties()
              .setTessereJolly(Arrays.asList());

    var    record2consumo = new FrejusRecordConsumoMapper(properties, customerProvider);
    //il record parent è gestito da gerarchia
    record.setParentRecord(getHeader("000867"));

    Frejus consumo        = record2consumo.mapRecordToConsumo(record);
    return consumo;
  }

  private static FrejusRecord getHeader(String codCli) {
    String       fileName = "file";
    FrejusRecord header   = new FrejusRecord(fileName, 0);
    header.setNumeroCliente(codCli);
    return header;
  }

  public static FrejusRecord getRecord() {
    var data          = "1" + "308134" + "4" + "00" + "1036" + "27786" + "1" + "20180802180718" + "+" + "027165" + "022266" + "+" + "000010"
                        + "000010" + "2200" + "000280/TRF_1808     " + "00001" + "1" + "01" + "0" + "3" + "4" + "03" + "25002999"
                        + "               " + "                   " + "           ";
    var string2record = new FrejusRecordDescriptor(customerProvider);

    FrejusRecord record = string2record.decodeRecordCodeAndCallSetFromString(data, string2record.extractRecordCode(data), "LEPTCTTR.999",
                                                                             -1);
    return record;
  }

}
