package it.fai.ms.consumi.service.record.telepass;

import static it.fai.ms.consumi.service.record.telepass.TelepassDeviceType.APPARATO_TELEPASS;
import static it.fai.ms.consumi.service.record.telepass.TelepassDeviceType.PLATE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;

@Tag("unit")
class ElviaRecordConsumoMapperTest {

  ElviaRecordConsumoMapper elviaRecordConsumoMapper = new ElviaRecordConsumoMapper();

  ElviaRecord commonRecord = mock(ElviaRecord.class);

  @BeforeEach
  public void setUp(){
    given(commonRecord.getGlobalIdentifier())
      .willReturn("GlobalIdentifier");
    given(commonRecord.getTotal_amount_value())
      .willReturn(Money.of(new BigDecimal("12").setScale(2, RoundingMode.HALF_UP), "EUR"));
    given(commonRecord.getDiscount_value())
      .willReturn(Money.of(new BigDecimal("12").setScale(2, RoundingMode.HALF_UP), "EUR"));
    given(commonRecord.getVatRateBigDecimal())
      .willReturn(new BigDecimal("20").setScale(2, RoundingMode.HALF_UP));
    given(commonRecord.getEntryGateCode())
      .willReturn("EntryGateCode");
    given(commonRecord.getExitGateCode())
      .willReturn("ExitGateCode");
    given(commonRecord.getIngestion_time())
      .willReturn(Instant.now());
  }

  @Test
  void mapRecordToConsumo_if_NOT_PLATE_DEVICE_will_NOT_set_vehicle_but_device_with_seriale() throws Exception {
    //prevent null pointer
    given(commonRecord.getAmount_not_subjected_to_vat())
      .willReturn(Money.of(BigDecimal.ZERO, "EUR"));

    given(commonRecord.getTypeMovement())
      .willReturn("PE");

    given(commonRecord.getTypeSupport())
      .willReturn(APPARATO_TELEPASS);

    given(commonRecord.getSupportCode())
      .willReturn("0775823156");

    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getDevice().getSeriale())
      .isEqualTo("0775823156");

    assertThat(consumo.getVehicle().getLicensePlate())
      .isNull();
  }

  @Test
  void mapRecordToConsumo_if_PLATE_DEVICE_will_set_vehicle_and_secondary_device_BUT_not_device() throws Exception {
    //prevent null pointer
    given(commonRecord.getAmount_not_subjected_to_vat())
      .willReturn(Money.of(BigDecimal.ZERO, "EUR"));

    given(commonRecord.getTypeMovement())
      .willReturn("AC");

    given(commonRecord.getTypeSupport())
      .willReturn(PLATE);

    given(commonRecord.getRawSupportCode())
      .willReturn("AA000AA");

    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getDevice())
      .isNull();

    assertThat(consumo.getSecondaryDevice().isPresent())
      .isTrue();

    assertThat(consumo.getSecondaryDevice().get().getSeriale())
      .isEqualTo("AA000AA");

    assertThat(consumo.getSecondaryDevice().get().getServiceType())
      .isEqualTo(TipoServizioEnum.PEDAGGI_ITALIA);

    assertThat(consumo.getVehicle().getLicensePlate().getLicenseId())
      .isEqualTo("AA000AA");

    assertThat(consumo.getVehicle().getLicensePlate().getCountryId())
      .isEqualTo(VehicleLicensePlate.NO_COUNTRY);
  }

  @Test
  void mapRecordToConsumo_if_NOT_DEVICE_will_NOT_set_vehicle_NOR_device() throws Exception {
    //prevent null pointer
    given(commonRecord.getAmount_not_subjected_to_vat())
      .willReturn(Money.of(BigDecimal.ZERO, "EUR"));

    given(commonRecord.getTypeMovement())
      .willReturn("A4");

    given(commonRecord.getTypeSupport())
      .willReturn("");

    given(commonRecord.getSupportCode())
      .willReturn("");

    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getDevice())
      .isNull();

    assertThat(consumo.getSecondaryDevice().isPresent())
      .isFalse();
  }

  @Test
  void mapTOTrecord() throws Exception {
    //prevent null pointer
    given(commonRecord.getTypeMovement())
      .willReturn("PE");
    given(commonRecord.getTypeSupport())
      .willReturn(APPARATO_TELEPASS);
    given(commonRecord.getSupportCode())
      .willReturn("0775823156");

    given(commonRecord.getAmount_not_subjected_to_vat())
      .willReturn(Money.of(BigDecimal.ZERO, "EUR"));

    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getAmount().getAmountIncludedVat().toString())
      .isEqualTo("EUR 12");
    assertThat(consumo.getAmount().getAmountExcludedVat().toString())
      .isEqualTo("EUR 10");

  }

  @Test
  void mapESErecord_when_have_amount_no_wat() throws Exception {
    //prevent null pointer
    preventNullPointers();

    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    assertThat(consumo.getAmount().getAmountIncludedVat().toString())
      .isEqualTo("EUR 2");
    assertThat(consumo.getAmount().getAmountExcludedVat().toString())
      .isEqualTo("EUR 2");
  }

  @Test
  void mapTollGateDescritption_WITH_TOLLS() throws Exception {
    //prevent null pointer
    preventNullPointers();

    given(commonRecord.getMovementDescription())
      .willReturn("PIACENZA SUD MILANO SUD");

    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    final String entryDescription = consumo.getTollPointEntry().getGlobalGate().getDescription();
    assertThat(entryDescription)
      .isEqualTo("PIACENZA SUD");
    final String exitDescription = consumo.getTollPointExit().getGlobalGate().getDescription();
    assertThat(exitDescription)
      .isEqualTo("MILANO SUD");
    final String route = consumo.getRoute();
    assertThat(route)
      .isEqualTo("PIACENZA SUD MILANO SUD");
    final String routeName = consumo.getRouteName();
    assertThat(routeName)
      .isNull();
  }

  @Test
  void mapTollGateDescritption_WITH_DESCRIPTION() throws Exception {
    preventNullPointers();

    given(commonRecord.getMovementDescription())
      .willReturn("SCONTO APL TRATTA A36");

    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    final String entryDescription = consumo.getTollPointEntry().getGlobalGate().getDescription();
    assertThat(entryDescription)
      .isEqualTo("SCONTO APL TR");
    final String exitDescription = consumo.getTollPointExit().getGlobalGate().getDescription();
    assertThat(exitDescription)
      .isEqualTo("ATTA A36");
    final String route = consumo.getRoute();
    assertThat(route)
      .isEqualTo("SCONTO APL TRATTA A36");
    final String routeName = consumo.getRouteName();
    assertThat(routeName)
      .isNull();
  }

  private void preventNullPointers() {
    //prevent null pointer
    given(commonRecord.getTypeMovement())
      .willReturn("PE");

    given(commonRecord.getTypeSupport())
      .willReturn(APPARATO_TELEPASS);

    given(commonRecord.getSupportCode())
      .willReturn("0775823156");

    given(commonRecord.getAmount_not_subjected_to_vat())
      .willReturn(Money.of(new BigDecimal("2").setScale(2, RoundingMode.HALF_UP), "EUR"));
  }
  
  
  @Test
  void mapRecordToConsumo_truncateTo9Chars_if_viacard() throws Exception {
    
    preventNullPointers();
    
    given(commonRecord.getTypeSupport())
    .willReturn(TelepassDeviceType.TESSERA_VIACARD);

    given(commonRecord.getSupportCode())
    .willReturn("0123456789");
    
    Elvia consumo = elviaRecordConsumoMapper.mapRecordToConsumo(commonRecord);
    
    assertThat(consumo.getDevice()).isNotNull();
    
    
    assertThat(consumo.getDevice().getSeriale()).isEqualTo("123456789");
    
  }
}
