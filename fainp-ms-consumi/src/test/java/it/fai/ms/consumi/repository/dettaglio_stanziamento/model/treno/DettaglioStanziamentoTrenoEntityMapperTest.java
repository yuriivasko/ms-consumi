package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.GlobalIdentifierType;

class DettaglioStanziamentoTrenoEntityMapperTest {

  private static DettaglioStanziamentoTrenoEntity newEntityWithExternalGlobalIdentifierType() {
    var entity = new DettaglioStanziamentoTrenoEntity("::globalIdentifier::#EXTERNAL");
    return entity;
  }

  private static DettaglioStanziamentoTrenoEntity newEntityWithInternalGlobalIdentifierType() {
    var entity = new DettaglioStanziamentoTrenoEntity("::globalIdentifier::#INTERNAL");
    return entity;
  }

  private DettaglioStanziamentoTrenoEntityMapper mapper = new DettaglioStanziamentoTrenoEntityMapper();

  @Test
  void testToDomain_when_globalIdentifier_isExternal() {

    var dettaglioStanziamentoTreno = mapper.toDomain(newEntityWithExternalGlobalIdentifierType());

    assertThat(dettaglioStanziamentoTreno.getGlobalIdentifier()
                                         .getId()).isEqualTo("::globalIdentifier::");
    assertThat(dettaglioStanziamentoTreno.getGlobalIdentifier()
                                         .getType()).isEqualTo(GlobalIdentifierType.EXTERNAL);
  }

  @Test
  void testToDomain_when_globalIdentifier_isInternal() {

    var dettaglioStanziamentoTreno = mapper.toDomain(newEntityWithInternalGlobalIdentifierType());

    assertThat(dettaglioStanziamentoTreno.getGlobalIdentifier()
                                         .getId()).isEqualTo("::globalIdentifier::");
    assertThat(dettaglioStanziamentoTreno.getGlobalIdentifier()
                                         .getType()).isEqualTo(GlobalIdentifierType.INTERNAL);
  }

  @Test
  void testToEntity() {
  }

}
