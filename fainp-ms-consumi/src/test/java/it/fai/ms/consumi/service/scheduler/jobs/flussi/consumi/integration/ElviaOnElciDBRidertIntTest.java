package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.core.replacers.NullReplacer;
import com.github.database.rider.spring.api.DBRider;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.StanziamentoCodeGeneratorStub;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;

@DBRider
@Tag("integration")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ElviaOnElciDBRidertIntTest
  extends AbstractTelepassIntTest {

  private ElviaJob elviaJob;

  @Inject
  private ElviaProcessor defaultProcessor;

  @Inject
  private ElviaRecordConsumoMapper defaultRecordConsumoMapper;

  @Inject
  private ElviaGenericoProcessor consumoGenericoProcessor;

  @Inject
  private ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper;

  @Inject
  private StanziamentoRepository stanziamentiRepository;

  @Inject
  private RecordPersistenceService recordPersistenceService;

  @Inject
  protected ServiceProviderRepository serviceProviderRepository;

  @Inject
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Inject
  private PlatformTransactionManager transactionManager;

  @Inject
  private NotConfirmedTemporaryAllocationsService notConfirmedAllocationService;

  private ElcitJob elcitJob;

  @Autowired
  private StanziamentoCodeGeneratorStub stanziamentoCodeGeneratorStub;

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentoTestRepository;
  @Inject
  private ConsumoProcessor<Elcit> elcitProcessor;
  @Inject
  private RecordConsumoMapper<ElcitRecord, Elcit> consumoMapperElcit;
  
  private static final Logger log = LoggerFactory.getLogger(ElviaOnElciDBRidertIntTest.class);
  
  @Inject
  private EntityManager em;

  @BeforeEach
  public void setUp() throws Exception {

    ElviaDescriptor descriptor = new ElviaDescriptor();
    ElcitDescriptor elcitDescriptor = new ElcitDescriptor();

    stanziamentoCodeGeneratorStub.reset();

    super.setUpMockContext("Elvia");

    elviaJob = new ElviaJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      descriptor,
      defaultProcessor,
      defaultRecordConsumoMapper,
      consumoGenericoProcessor,
      recordConsumoGenericoMapper,
      stanziamentiToNavJmsPublisher,
      notConfirmedAllocationService
    );
    elcitJob = new ElcitJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      elcitDescriptor,
      elcitProcessor,
      consumoMapperElcit,
      stanziamentiToNavJmsPublisher,
      "FILENAME_ASC");
  }



  private File replaceStringAndGetATempFile(File originalFile, String toFind, String newString) throws IOException {
    Path newFile = Files.createTempFile(originalFile.getName(), "test");
    try (Stream<String> lines = Files.lines(originalFile.toPath())) {
      List<String> replaced = lines
          .map(line-> line.replaceAll(toFind, newString))
          .collect(Collectors.toList());
      Files.write(newFile, replaced);
   }
    return newFile.toFile();
  }



  /*@Test
  @Transactional(propagation=Propagation.NOT_SUPPORTED)
  @DataSet(transactional = true, executeScriptsBefore= {"test-files/clearStanziamentiCascade-h2.sql"})
  @ExpectedDataSet(value="test-files/elvia/int-test-elvia-on-elcit-no-match/stanziamento_DA06A284.ELCIT-ELVIA-nomatch_dbunit.xml", replacers=NullReplacer.class)
  public void testCaso_elcitN002_elviaN002noareac_elviaN003() throws Exception { //ELCIT N00002 + ELVIA N0002(noareac) + ELVIA N0003
    File ElcitN002 = new File(ElviaOnElcitIntTest.class.getResource("/test-files/elcit/DA06A284.ELCIT.FAI.N00002").toURI());
    elcitJob.process(ElcitN002.getAbsolutePath(), System.currentTimeMillis(), new Date().toInstant(), null);
    File ElviaN0002NoareaC = new File(ElviaOnElcitIntTest.class.getResource("/test-files/elvia/int-test-elvia-only/DA06A284.ELVIA.FAI.N00002_NOAREAC").toURI());
    elviaJob.process(ElviaN0002NoareaC.getAbsolutePath(), System.currentTimeMillis(), new Date().toInstant(), null);
    File ElviaN0003 = new File(ElviaOnElcitIntTest.class.getResource("/test-files/elvia/DA06A284.ELVIA.FAI.N00003").toURI());
    elviaJob.process(ElviaN0003.getAbsolutePath(), System.currentTimeMillis(), new Date().toInstant(), null);
  }*/

  @Test
  @Transactional(propagation=Propagation.NOT_SUPPORTED)
  @DataSet(transactional = true, executeScriptsBefore= {"test-files/clearStanziamentiCascade-h2.sql"})
  @ExpectedDataSet(value="test-files/elvia/int-test-elvia-on-elcit-caso3/elvia_on_elcit_caso3_expected.xml", replacers=NullReplacer.class, ignoreCols = {"DETTAGLIO_STANZIAMENTI_ID"})
  public void testCaso3_elcit_on_elvia() throws URISyntaxException {
    String fileNameElcit = new File(ElviaOnElcitIntTest.class.getResource("/test-files/elvia/int-test-elvia-on-elcit-caso3/DA06A284.ELCIT.FAI.OneToMatch").toURI()).getAbsolutePath();
    elcitJob.process(fileNameElcit, System.currentTimeMillis(), new Date().toInstant(), null);
    String fileNameElvia = new File(ElviaOnElcitIntTest.class.getResource("/test-files/elvia/int-test-elvia-on-elcit-caso3/DA06A284.ELVIA.oneToMatch").toURI()).getAbsolutePath();
    elviaJob.process(fileNameElvia, System.currentTimeMillis(), new Date().toInstant(), null);
  }


  @Test
  @Transactional(propagation=Propagation.NOT_SUPPORTED)
  @DataSet(transactional = true, executeScriptsBefore= {"test-files/clearStanziamentiCascade-h2.sql"})
  @ExpectedDataSet(value="test-files/elvia/int-test-elvia-on-elcit-no-match/stanziamento_DA06A284.ELCIT-ELVIA-nomatch_dbunit.xml", replacers=NullReplacer.class)
  public void testCaso1_no_match() throws Exception {
    File fileNameElcit = new File(ElviaOnElciDBRidertIntTest.class.getResource("/test-files/elvia/int-test-elvia-on-elcit-no-match/DA06A284.ELCIT.FAI.N00002").toURI());
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYYMMdd");
    String newFileNameElcit = replaceStringAndGetATempFile(fileNameElcit,"201807061044250642TOLL",LocalDate.now().format(formatter)+"1044250642TOLL").getAbsolutePath();
    elcitJob.process(newFileNameElcit, System.currentTimeMillis(), new Date().toInstant(), null);
    String fileNameElvia = new File(ElviaOnElciDBRidertIntTest.class.getResource("/test-files/elvia/int-test-elvia-on-elcit-no-match/DA06A284.ELVIA.FAI.N00002_NOAREAC")
                                    .toURI()).getAbsolutePath();
    
    elviaJob.process(fileNameElvia, System.currentTimeMillis(), new Date().toInstant(), null);
    
  }
  




}
