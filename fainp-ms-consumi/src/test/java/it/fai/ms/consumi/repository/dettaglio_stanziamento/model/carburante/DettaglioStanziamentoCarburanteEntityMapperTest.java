package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;

class DettaglioStanziamentoCarburanteEntityMapperTest {

  DettaglioStanziamentoCarburanteEntityMapper dettaglioStanziamentoCarburanteEntityMapper = new DettaglioStanziamentoCarburanteEntityMapper();

  @BeforeEach
  void setUp() {
  }

  @Test
  void when_is_prensent_measurementUnit_toDomain_should_map_it_in_domain() {
    final DettaglioStanziamentoCarburanteEntity entity = new DettaglioStanziamentoCarburanteEntity("#EXTERNAL");
    entity.setUnitaMisura("GAL");

    //necessary settings to prevent null pointer
    entity.setTipoSorgente(Format.ELVIA);
    entity.setIngestionTime(Instant.EPOCH);
    entity.setDataAcquisizioneFlusso(Instant.EPOCH);
    entity.setPuntoErogazione("");
    entity.setCustomerId("");
    entity.setContractCode("");
    entity.setCodiceFornitoreNav("");
    entity.setVeicoloTarga("");
    entity.setVeicoloNazioneTarga("");

    DettaglioStanziamentoCarburante domain = dettaglioStanziamentoCarburanteEntityMapper.toDomain(entity);
    assertThat(domain.getMesaurementUnit())
      .isEqualTo("GAL");
  }

  @Test
  void when_is_prensent_measurementUnit_toEntity_should_map_it_in_entity() {
    final DettaglioStanziamentoCarburante domain = new DettaglioStanziamentoCarburante(new ElviaGlobalIdentifier(""));
    domain.setMesaurementUnit("GAL");

    //necessary settings to prevent null pointer
    domain.setFuelStation(new FuelStation(""));
    domain.setSource(new Source(Instant.EPOCH, Instant.EPOCH, "ELVIA"));
    final Amount amount = new Amount();
    amount.setAmountExcludedVat(MonetaryUtils.strToMonetary("1.00", "EUR"));
    domain.setAmount(amount);

    DettaglioStanziamentoCarburanteEntity entity = dettaglioStanziamentoCarburanteEntityMapper.toEntity(domain);
    assertThat(entity.getUnitaMisura())
      .isEqualTo("GAL");
  }
}
