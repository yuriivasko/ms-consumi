package it.fai.ms.consumi.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.repository.customer.CustomerRepository;

@Tag("unit")
class CustomerServiceImplTest {

  private static Customer newCustomer() {
    return new Customer("::customerUuid::");
  }

  private CustomerService          customerService;
  private final ContractService    mockContractService    = mock(ContractService.class);
  private final CustomerRepository mockCustomerRepository = mock(CustomerRepository.class);
  private final DeviceService      mockDeviceService      = mock(DeviceService.class);

  @BeforeEach
  void setUp() throws Exception {
    customerService = new CustomerServiceImpl(mockCustomerRepository, mockDeviceService, mockContractService, null);
  }

  @Test
  void when_customerUuid_doesntExist_then_return_emptyOptional() {

    // given

    given(mockCustomerRepository.findCustomerByUuid(anyString())).willReturn(Optional.empty());

    // when

    var optionalCustomer = customerService.findCustomerByUuid("::customerUuid::");

    // then

    then(mockCustomerRepository).should()
                                .findCustomerByUuid(eq("::customerUuid::"));
    then(mockCustomerRepository).shouldHaveNoMoreInteractions();

    then(mockDeviceService).shouldHaveZeroInteractions();

    then(mockContractService).shouldHaveZeroInteractions();

    assertThat(optionalCustomer).isEmpty();
  }

  @Test
  void when_customerUuid_exists_then_return_customer() {

    // given

    given(mockCustomerRepository.findCustomerByUuid("::customerUuid::")).willReturn(Optional.of(newCustomer()));

    // when

    var optionalCustomer = customerService.findCustomerByUuid("::customerUuid::");

    // then

    then(mockCustomerRepository).should()
                                .findCustomerByUuid(eq("::customerUuid::"));
    then(mockCustomerRepository).shouldHaveNoMoreInteractions();

    then(mockDeviceService).shouldHaveZeroInteractions();

    then(mockContractService).shouldHaveZeroInteractions();

    assertThat(optionalCustomer).contains(newCustomer());
  }

}
