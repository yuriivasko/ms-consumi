package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.repository.NotificationSetupRepository;
import it.fai.common.notification.service.NotificationMapper;
import it.fai.common.notification.service.NotificationMessageSender;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.repository.CaselliEntityRepository;
import it.fai.ms.consumi.repository.caselli.model.CaselliEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.flussi.record.model.stcon.StconRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.stcon.StconDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.stcon.StconJob;
import it.fai.ms.consumi.service.CaselliService;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.stcon.StconRecordConsumoMapper;

@Tag("integration")
public class StconJobIntTest extends AbstractIntegrationTestSpringContext {

  private String   filename;
  private String   filename2;
  private StconJob job;

  @Inject
  private StconRecordConsumoMapper recordConsumooMapper;

  @SpyBean
  private CaselliService caselliService;

  @Inject
  private CaselliEntityRepository caselliRepository;

  private StconDescriptor descriptor;

  private transient final Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private RecordPersistenceService recordPersistenceService;

  @Autowired
  private PlatformTransactionManager transactionManager;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  NotificationService notificationService = spy(new NotificationService(mock(NotificationMessageSender.class),
                                                                        mock(NotificationSetupRepository.class),
                                                                        mock(NotificationMapper.class), "Elvia"));

  @BeforeEach
  public void setUp() throws Exception {
    // filename = new
    // File(StconJobIntTest.class.getResource("/test-files/stcon/DA06A284.STCON.U2768488.N00090_intTest").toURI()).getAbsolutePath();
    filename = new File(StconJobIntTest.class.getResource("/test-files/stcon/STCON_SPAGNA.txt").toURI())
      .getAbsolutePath();
    filename2 = new File(StconJobIntTest.class.getResource("/test-files/stcon/STCON_SPAGNA2.txt").toURI())
        .getAbsolutePath();
    // ApplicationProperties appProperties = newJobProperties();
    descriptor = new StconDescriptor();

    doAnswer(invocation -> {
      log.info("notifying code: " + invocation.getArgument(0));
      Map<String, Object> errorMapCode = invocation.getArgument(1);
      errorMapCode.entrySet().stream().forEach(entry -> log.info("key for message:" + entry.getKey() + " " + entry.getValue()));
      return null;
    }).when(notificationService).notify(anyString(), anyMap());
    job = new StconJob(transactionManager, null, notificationService, recordPersistenceService, descriptor, recordConsumooMapper,
                       caselliService);
  }

  @Test
  @Transactional
  public void test() {

    assertThat(job.getSource()).isEqualTo("STCON");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);

    ArgumentCaptor<StconRecord> recorCapture = ArgumentCaptor.forClass(StconRecord.class);
    verify(caselliService,times(42)).save(recorCapture.capture(), anyString());

    StconRecord record = recorCapture.getAllValues().get(0);
    assertThat(record.getGateIdentifier()).isEqualTo("9");
    assertThat(record.getMotorways()).isEqualTo("");
    assertThat(record.getGateNumber()).isEqualTo("");
    assertThat(record.getLaneCode()).isEqualTo("");
    assertThat(record.getDescription()).isEqualTo("Aguiar de Sousa");
    assertThat(record.getEndDate()).isEqualTo("99991231");
    assertThat(record.getMainTypeOfGate()).isEqualTo("0");
    assertThat(record.getSubTypeOfGate()).isEqualTo("");
    assertThat(record.getDistance()).isEqualTo("0");
    assertThat(record.getGlobalGateIdentifier()).isEqualTo("000020616");

    List<CaselliEntity> caselli = caselliRepository.findAll();
    assertThat(caselli).isNotEmpty();
    assertThat(caselli.size()).isEqualByComparingTo(42);
    // assertThat(job.isRunning()).isEqualTo(true);

    verifyZeroInteractions(notificationService);
    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();

    assertThat(dettaglioStanziamenti).isEmpty();

    job.process(filename2, System.currentTimeMillis(), new Date().toInstant(), null);
    caselli = caselliRepository.findAll();
    assertThat(caselli).isNotEmpty();
    assertThat(caselli.size()).isEqualByComparingTo(43);
  }

}
