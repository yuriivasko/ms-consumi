package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.frejus.FrejusCustomerProvider;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.tunnel.FrejusRecordConsumoMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.sitaf.FrejusJobIntTest;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import it.fai.ms.consumi.utils.LambdaMatcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;

@Tag("unit")
class FrejusJobTest {

  private final StanziamentiParamsValidator  stanziamentiParamsValidator  = mock(StanziamentiParamsValidator.class);
  private final NotificationService          notificationService          = mock(NotificationService.class);
  private final RecordPersistenceService     recordPersistenceService     = mock(RecordPersistenceService.class);
  private final FrejusCustomerProvider       customerProvider             = mock(FrejusCustomerProvider.class);
  private final FrejusRecordDescriptor       descriptor1                  = new FrejusRecordDescriptor(customerProvider);
  private final ConsumoProcessor<Frejus>     processor                    = mock(ConsumoProcessor.class);
  private final FrejusRecordConsumoMapper    consumoMapper                = mock(FrejusRecordConsumoMapper.class);
  private final StanziamentiToNavPublisher stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
  private final PlatformTransactionManager   transactionManager           = mock(PlatformTransactionManager.class);


  private final ServicePartner  servicePartner  = mock(ServicePartner.class);
  private final RecordsFileInfo recordsFileInfo = mock(RecordsFileInfo.class);

  private Stanziamento stanziamento;

  private FrejusJob job;
  private String    fileName = "20180831_T4001036.188";
  private Path      filePath;

  @BeforeEach
  void setUp() throws Exception {
    filePath = Paths.get(FrejusJobIntTest.class.getResource("/test-files/sitaf/")
                                               .toURI());

    job = new FrejusJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService,
                        descriptor1, processor, consumoMapper, stanziamentiToNavJmsProducer);

    given(recordsFileInfo.fileIsValid()).willReturn(true);
//    given(customerProvider.loadCustomerData(any(),any())).willReturn(Arrays.asList());

    stanziamento = new Stanziamento("");
    stanziamento.setTipoFlusso(TypeFlow.NONE);
    stanziamento.setInvoiceType(InvoiceType.D);
  }


  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");

    given(stanziamentiParamsValidator.checkConsistency()).willThrow(runtimeExTest);

    given(validationOutcome.isValidationOk()).willReturn(true);

    job.process(filePath.resolve(fileName)
                        .toAbsolutePath()
                        .toString(),
                -1l, Instant.EPOCH, servicePartner);

    then(notificationService).should(atLeastOnce())
                             .notify(any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_recordPersistenceService_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency()).willReturn(validationOutcome);

    given(validationOutcome.isValidationOk()).willReturn(true);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");
    willThrow(runtimeExTest).given(recordPersistenceService)
                            .persist(any());

    given(validationOutcome.getMessages()).willReturn(new HashSet(Arrays.asList(new Message("1"),
                                                                                new Message("2"))));

    job.process(filePath.resolve(fileName)
                        .toAbsolutePath()
                        .toString(),
                -1l, Instant.EPOCH, servicePartner);

    then(notificationService).should(atLeastOnce())
                             .notify(any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_fail_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency()).willReturn(validationOutcome);

    given(validationOutcome.isValidationOk()).willReturn(false);

    given(validationOutcome.getMessages()).willReturn(new HashSet(Arrays.asList(new Message("1"),
                                                                                new Message("2"))));

    job.process(filePath.resolve(fileName)
                        .toAbsolutePath()
                        .toString(),
                -1l, Instant.EPOCH, servicePartner);

    then(notificationService).should(times(2))
                             .notify(any());
  }

  @Test
  public void process_if_all_ok_will_process_all_detail_rows() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency()).willReturn(validationOutcome);

    given(validationOutcome.isValidationOk()).willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any())).willReturn(mock(Frejus.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any())).willReturn(processingResult);

    given(processingResult.getStanziamenti()).willReturn(Arrays.asList(stanziamento));

    job.process(filePath.resolve(fileName)
                        .toAbsolutePath()
                        .toString(),
                -1l, Instant.EPOCH, servicePartner);

    then(recordPersistenceService).should(times(3))
                                  .persist(any());

  }

  @Test
  public void process_if_all_ok_will_map_all_rows_to_consumo() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency()).willReturn(validationOutcome);

    given(validationOutcome.isValidationOk()).willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any())).willReturn(mock(Frejus.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any())).willReturn(processingResult);

    given(processingResult.getStanziamenti()).willReturn(Arrays.asList(stanziamento));

    job.process(filePath.resolve(fileName)
                        .toAbsolutePath()
                        .toString(),
                -1l, Instant.EPOCH, servicePartner);

    then(consumoMapper).should(times(3))
                       .mapRecordToConsumo(any());
  }

  @Test
  public void process_if_checks_success_will_send_stanziamenti_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency()).willReturn(validationOutcome);

    given(validationOutcome.isValidationOk()).willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any())).willReturn(mock(Frejus.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any())).willReturn(processingResult);

    given(processingResult.getStanziamenti()).willReturn(Arrays.asList(stanziamento));

    job.process(filePath.resolve(fileName)
                        .toAbsolutePath()
                        .toString(),
                -1l, Instant.EPOCH, servicePartner);

    then(notificationService).should(times(0))
                             .notify(any(), any());

    then(stanziamentiToNavJmsProducer).should(times(1))
                                      .publish(any());
  }

  @Test
  public void process_if_processor_pratially_process_rows_but_throw_an_exception_will_send_notification_and_stanziamento_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency()).willReturn(validationOutcome);

    given(validationOutcome.isValidationOk()).willReturn(true);

    given(validationOutcome.getMessages()).willReturn(new HashSet(Arrays.asList(new Message("1"),
                                                                                new Message("2"))));


    given(consumoMapper.mapRecordToConsumo(any())).willReturn(mock(Frejus.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any())).willReturn(processingResult)
                                                     .willThrow(new RuntimeException("This is an exception to test behaviour"));

    given(processingResult.getStanziamenti()).willReturn(Arrays.asList(stanziamento));

    job.process(filePath.resolve(fileName)
                        .toAbsolutePath()
                        .toString(),
                -1l, Instant.EPOCH, servicePartner);

    then(notificationService).should(times(2))
                             .notify(argThat(new LambdaMatcher<>(o -> o.getCode().equals("FCJOB-200"))));

    then(stanziamentiToNavJmsProducer).should(times(1))
                                      .publish(any());
  }

  @Test
  public void getJobQualifier() {
    assertThat(job.getJobQualifier()).isEqualTo(Format.SITAF);
  }

}
