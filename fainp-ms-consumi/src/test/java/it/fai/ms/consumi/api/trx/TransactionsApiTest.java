package it.fai.ms.consumi.api.trx;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.api.trx.model.TrxNoOilReq;
import it.fai.ms.consumi.api.trx.model.TrxOilReq;
import it.fai.ms.consumi.api.trx.model.TrxResp;
import it.fai.ms.consumi.api.trx.model.mapper.TrxReqConsumoMapper;
import it.fai.ms.consumi.client.GeoClient;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.StoricoInformationService;
import it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.trx.TrxNoOilProcessor;
import it.fai.ms.consumi.service.processor.consumi.trx.TrxOilProcessor;
import it.fai.ms.consumi.service.trx.TrxJob;

@Tag("unit")
class TransactionsApiTest {

  private TransactionsApi            api;
  private StanziamentiParamsService  stanziamentiParamsService  = mock(StanziamentiParamsService.class);
  private ServiceProviderRepository  spService                  = mock(ServiceProviderRepository.class);
  private TrxReqConsumoMapper        trxReqConsumoMapper;
  private TrxOilProcessor            trxOilProcessor            = mock(TrxOilProcessor.class);
  private TrxNoOilProcessor          trxNoOilProcessor          = mock(TrxNoOilProcessor.class);
  private NotificationService        notificationService        = mock(NotificationService.class);
  private StanziamentiToNavPublisher stanziamentiToNavPublisher = mock(StanziamentiToNavJmsProducer.class);
  private StoricoInformationService  storicoInformationService  = mock(StoricoInformationService.class);
  private DettaglioStanziamantiRepository  dettaglioStanziamentoRepo = mock(DettaglioStanziamantiRepository.class);
  private SimpleDateFormat sdf;
  @Autowired
  private GeoClient                  geoClient;

  @BeforeEach
  void setUp() throws Exception {
    trxReqConsumoMapper = new TrxReqConsumoMapper(geoClient,"");
    TrxJob trxJob = new TrxJob(stanziamentiParamsService, spService, trxReqConsumoMapper, trxOilProcessor, trxNoOilProcessor,
                               notificationService, stanziamentiToNavPublisher, storicoInformationService, dettaglioStanziamentoRepo);
    api = new TransactionsApi(trxJob);

    sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
    when(spService.findByProviderCode(any())).thenReturn( new ServiceProvider());
  }

  @Test
  void trxOilFailedAValidation() {
    @Valid
    TrxOilReq req = createRequestOil();
    ResponseEntity<TrxResp> response = api.trxOil(req);
    assertThat(response.getStatusCode()
                       .is2xxSuccessful()).isTrue();
    
    assertThat(response.getBody().getSuccess()).isEqualTo(0);
    assertThat(response.getBody().getErrorCode()).isEqualTo(102);
    assertThat(response.getBody().getErrorMessage()).startsWith("Non è stata trovata la configurazione");
  }

  @Test
  void trxOilFailedValidationStanziamentiParams() {
    @Valid
    TrxOilReq req = createRequestOil();
    
    when(dettaglioStanziamentoRepo.findByGlobalIdentifierId(any(), any())).thenReturn(null);
    
    when(stanziamentiParamsService.findByTrx("OIL", StanziamentiDetailsType.CARBURANTI, req.getArticleCode(), req.getVendorCode(),
                                             req.getUnit(), req.getCurrency(), req.getVat(), req.getProductCode())).thenReturn(Optional.empty());
    ResponseEntity<TrxResp> response = api.trxOil(req);
    verify(stanziamentiParamsService).findByTrx("OIL", StanziamentiDetailsType.CARBURANTI, req.getArticleCode(), req.getVendorCode(),
                                                req.getUnit(), req.getCurrency(), req.getVat().multiply(new BigDecimal(100)), req.getProductCode());
    assertThat(response.getStatusCode()
                       .is2xxSuccessful()).isTrue();
    
    assertThat(response.getBody().getSuccess()).isEqualTo(0);
    assertThat(response.getBody().getErrorCode()).isEqualTo(102);
    assertThat(response.getBody().getErrorMessage()).startsWith("Non è stata trovata la configurazione");
  }

  @Test
  void trxOilFailedValidationServiceProvider() {
    when(spService.findByProviderCode(any())).thenReturn(null);
    @Valid
    TrxOilReq req = createRequestOil();
    ResponseEntity<TrxResp> response = api.trxOil(req);
    verify(spService).findByProviderCode(any());
    assertThat(response.getStatusCode()
                       .is2xxSuccessful()).isTrue();
    
    assertThat(response.getBody().getSuccess()).isEqualTo(0);
    assertThat(response.getBody().getErrorCode()).isEqualTo(101);
    assertThat(response.getBody().getErrorMessage()).startsWith("Non è stato trovato il servizio");
  }

  private @Valid TrxOilReq createRequestOil() {
    TrxOilReq trxOilReq = new TrxOilReq();
    trxOilReq.requestCode("RQ_CODE" + sdf.format(new Date()))
             .articleCode("::article-code::")
             .currency("EUR")
             .buyerCode("0046348")
             .cardNumber("00000123456789012")
             .amount(new BigDecimal(100))
             .virtaulCard(false)
             .vendorCode("AT01")
             .vat(new BigDecimal("0.30"))
             .submissioDateTime(OffsetDateTime.now())
             .quantity(new BigDecimal(10))
             .productCode("CARBURANTI")
             .plate("AA000AA")
             .outletCode("ES0000111")
             .km(10)
             .intVRC("IT")
             .driverId("AUTISTA");

    trxOilReq.setUnit("kg");

    return trxOilReq;
  }

  @Test
  void trxNoOilAValidation() {
    @Valid
    TrxNoOilReq req = createRequestNoOil();
    ResponseEntity<TrxResp> response = api.trxNoOil(req);
    assertThat(response.getStatusCode()
                       .is2xxSuccessful()).isTrue();
  }

  @Test
  void trxNoOilFailedValidationStanziamentiParams() {
    @Valid
    TrxNoOilReq req = createRequestNoOil();
    
    when(dettaglioStanziamentoRepo.findByGlobalIdentifierId(any(), any())).thenReturn(null);
    
    when(stanziamentiParamsService.findByTrx("NO_OIL", StanziamentiDetailsType.GENERICO, req.getArticleCode(), req.getVendorCode(),
                                             req.getUnit(), req.getCurrency(), req.getVat(), req.getProductCode())).thenReturn(Optional.empty());
    ResponseEntity<TrxResp> response = api.trxNoOil(req);
    verify(stanziamentiParamsService).findByTrx("NO_OIL", StanziamentiDetailsType.GENERICO, req.getArticleCode(), req.getVendorCode(),
                                                req.getUnit(), req.getCurrency(), req.getVat().multiply(new BigDecimal(100)), req.getProductCode());
    assertThat(response.getStatusCode()
                       .is2xxSuccessful()).isTrue();
  }

  @Test
  void trxNoOilFailedValidationServiceProvider() {
    @Valid
    TrxNoOilReq req = createRequestNoOil();
    ResponseEntity<TrxResp> response = api.trxNoOil(req);
    assertThat(response.getStatusCode()
                       .is2xxSuccessful()).isTrue();
  }

  private @Valid TrxNoOilReq createRequestNoOil() {
    TrxNoOilReq trxNoOilReq = new TrxNoOilReq();
    trxNoOilReq.requestCode("RQ_CODE" + sdf.format(new Date()))
               .articleCode("::article-code::")
               .currency("EUR")
               .buyerCode("0046348")
               .cardNumber("00000123456789012")
               .amount(new BigDecimal(100))
               .virtaulCard(false)
               .vendorCode("AT01")
               .vat(new BigDecimal("0.30"))
               .submissioDateTime(OffsetDateTime.now())
               .quantity(new BigDecimal(10))
               .productCode("GENERICO")
               .plate("AA000AA")
               .outletCode("ES0000111")
               .km(10)
               .intVRC("IT")
               .driverId("AUTISTA");

    trxNoOilReq.setUnit("kg");
    return trxNoOilReq;
  }

}
