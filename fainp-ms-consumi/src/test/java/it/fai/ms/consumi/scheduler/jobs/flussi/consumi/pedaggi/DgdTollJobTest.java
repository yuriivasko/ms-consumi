package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.BDDMockito.willThrow;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.dgdtoll.DgdTollProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.dgdtoll.DgdTollRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Tag("unit")
class DgdTollJobTest {

  private final DgdTollRecordDescriptor          descriptor                   = spy(new DgdTollRecordDescriptor());

  private final StanziamentiParamsValidator      stanziamentiParamsValidator  = mock(StanziamentiParamsValidator.class);
  private final NotificationService              notificationService          = mock(NotificationService.class);
  private final RecordPersistenceService         recordPersistenceService     = mock(RecordPersistenceService.class);
  private final DgdTollProcessor                 processor                    = mock(DgdTollProcessor.class);
  private final DgdTollRecordConsumoMapper       consumoMapper                = mock(DgdTollRecordConsumoMapper.class);
  private final StanziamentiToNavPublisher     stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
  private final DgdTollRecordFileFormalValidator dgdTollFormalFileValidator   = mock(DgdTollRecordFileFormalValidator.class);
  private final PlatformTransactionManager       transactionManager           = mock(PlatformTransactionManager.class);


  private final ServicePartner servicePartner = mock(ServicePartner.class);

  private Stanziamento stanziamento;

  private DgdTollJob job;
  private final String     fileName = "FAI_RE170629-02.TXT.20170701100012";
  private final String     fileLogName = "FAI_RE170629-02.LOG.20170701100012";
  private  String     filePath = "";

  @BeforeEach
  void setUp() throws Exception {
    filePath = new File(DgdTollJobTest.class.getResource("/test-files/dgdtoll/" + fileName).toURI()).getAbsolutePath();
    job = new DgdTollJob(transactionManager, stanziamentiParamsValidator, dgdTollFormalFileValidator, notificationService,
                         recordPersistenceService, descriptor, processor, consumoMapper, stanziamentiToNavJmsProducer);



    given(dgdTollFormalFileValidator.validateFile(any()))
      .willAnswer(invocation -> invocation.getArgument(0));


    stanziamento = new Stanziamento("");
    stanziamento.setTipoFlusso(TypeFlow.NONE);
    stanziamento.setInvoiceType(InvoiceType.D);
  }

  @Test
  public void validateFile_will_set_creation_date_and_time_by_filename(){
    reset(dgdTollFormalFileValidator);
    //non torno il mock ma il risultato della prima validazione formale
    given(dgdTollFormalFileValidator.validateFile(any()))
      .willAnswer(new Answer<RecordsFileInfo>() {
        @Override
        public RecordsFileInfo answer(InvocationOnMock invocationOnMock) throws Throwable {
          return invocationOnMock.getArgument(0);
        }
      });

    RecordsFileInfo recordsFileInfo = job.validateFile(filePath, -1, Instant.EPOCH);
    assertThat(recordsFileInfo.getHeader().getCreationDateInFile())
      .isEqualTo("20170629");
    assertThat(recordsFileInfo.getHeader().getCreationTimeInFile())
      .isEqualTo("000000");
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");

    given(stanziamentiParamsValidator.checkConsistency())
      .willThrow(runtimeExTest);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(atLeastOnce())
      .notify(any()); //, any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_recordPersistenceService_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");
    willThrow(runtimeExTest)
      .given(recordPersistenceService)
      .persist(any());

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(atLeastOnce())
      .notify(any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_fail_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(2))
      .notify(any());
  }

  @Test
  public void process_if_all_ok_will_process_all_detail_rows() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(DgdToll.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(recordPersistenceService)
      .should(times(27))
      .persist(any());

  }

  @Test
  public void process_if_all_ok_will_map_all_rows_to_consumo() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(DgdToll.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);


    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(consumoMapper)
      .should(times(27))
      .mapRecordToConsumo(any());
  }

  @Test
  public void process_if_checks_success_will_send_stanziamenti_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(DgdToll.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(0))
      .notify(any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void process_if_processor_pratially_process_rows_but_throw_an_exception_will_send_notification_and_stanziamento_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(DgdToll.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult)
      .willThrow(new RuntimeException("This is an exception to test behaviour"));

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    //FIXME notification
    then(notificationService)
      .should(times(26))
      .notify(any()); // Mockito.eq("FCJOB-200"), any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void process_if_success_will_create_log_file() throws Exception {
    final Path originalFile = Paths.get(filePath);
    final Path tmpFile = Paths.get(System.getProperty("java.io.tmpdir"), fileName);
    final Path tmpLogFile = Paths.get(System.getProperty("java.io.tmpdir"), fileLogName);
    Files.copy(originalFile, tmpFile, StandardCopyOption.REPLACE_EXISTING);

    job = new DgdTollJob(transactionManager, stanziamentiParamsValidator, dgdTollFormalFileValidator, notificationService,
                         recordPersistenceService, new DgdTollRecordDescriptor(), processor, consumoMapper, stanziamentiToNavJmsProducer);

    job.postFileElaboration(tmpFile.toString(), Instant.EPOCH, servicePartner, new HashMap<>(), new ArrayList<>(), new RecordsFileInfo(fileName, 0, Instant.EPOCH));

    assertThat(Files.exists(tmpLogFile))
      .isTrue();
  }

  @Test
  public void getJobQualifier() {
    assertThat(job.getJobQualifier())
      .isEqualTo(Format.DGD);
  }

}
