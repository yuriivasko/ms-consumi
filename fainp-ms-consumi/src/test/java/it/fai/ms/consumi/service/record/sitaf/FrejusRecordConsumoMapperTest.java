package it.fai.ms.consumi.service.record.sitaf;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.FrejusJobProperties;
import it.fai.ms.consumi.domain.TrustedCustomerSupplier;
import it.fai.ms.consumi.domain.TrustedVechicleSupplier;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.FrejusRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.FrejusRecordDescriptor;
import it.fai.ms.consumi.service.frejus.FrejusCustomerProvider;
import it.fai.ms.consumi.service.record.tunnel.FrejusRecordConsumoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@Tag("unit")
class FrejusRecordConsumoMapperTest {

  private static final String COD_CLI_MBIT = "000867";
  private static final String COD_CLI_FJFR = "009010";
  private static final String FILE_NAME = "T1000867.189";
  FrejusRecordConsumoMapper subject;
  private FrejusCustomerProvider customerProvider = mock(FrejusCustomerProvider.class);

  @BeforeEach
  void setup() {
    subject = new FrejusRecordConsumoMapper(getApplicationProperties(),customerProvider );
  }

  private ApplicationProperties getApplicationProperties() {
    ApplicationProperties applicationProperties = new ApplicationProperties();
    applicationProperties.setFrejusJobProperties(new FrejusJobProperties());
    applicationProperties.getFrejusJobProperties().setTessereJolly(Arrays.asList("jolly00000000000001"));
    return applicationProperties;
  }

  @Test
  void testMapRecordToConsumoMinalData() throws Exception {

    FrejusRecord record = new FrejusRecord(FILE_NAME, 0);
    record.setNumeroTessera("01234567891234567879");
    record.setDataTransazione("20180906114100");
    record.setParentRecord(getHeader(COD_CLI_FJFR));

    Frejus consumo = subject.mapRecordToConsumo(record);

    assertThat(consumo.getDevice().getSeriale()).isEqualTo("01234567891234567879");
    assertThat(consumo.getDate()).isEqualTo(LocalDateTime.parse("2018-09-06T11:41:00.00").atZone(ZoneId.systemDefault()).toInstant());
    assertThat(consumo.getTransactionDetailCode()).isEqualTo("FJFR");
  }

  @Test
  void testMapRecordToConsumo() throws Exception {

    FrejusRecord record = new FrejusRecord(FILE_NAME, 0);
    record.setNumeroTessera("01234567891234567879");
    record.setDataTransazione("20180906114100");
    record.setSenso("1");
    record.setParentRecord(getHeader(COD_CLI_MBIT));

    Frejus consumo = subject.mapRecordToConsumo(record);

    assertThat(consumo).isNotInstanceOf(TrustedCustomerSupplier.class);
    assertThat(consumo).isNotInstanceOf(TrustedVechicleSupplier.class);

    assertThat(consumo.getDevice().getSeriale()).isEqualTo("01234567891234567879");
    assertThat(consumo.getDate()).isEqualTo(LocalDateTime.parse("2018-09-06T11:41:00.00").atZone(ZoneId.systemDefault()).toInstant());
    assertThat(consumo.getTollPointEntry().getGlobalGate().getId()).isEqualTo("25053391");
    assertThat(consumo.getTollPointExit().getGlobalGate().getId()).isEqualTo("25002999");
    assertThat(consumo.getTransactionDetailCode()).isEqualTo("MBIT");
  }

  @Test
  void testMapRecordToConsumoPrenotazione() throws Exception {

    FrejusRecord record = new FrejusRecord(FILE_NAME, 0);
    record.setNumeroTessera("jolly00000000000001");
    record.setDataTransazione("20180906114100");
    record.setParentRecord(getHeader(COD_CLI_MBIT));

    Frejus consumo = subject.mapRecordToConsumo(record);

    assertThat(consumo).isInstanceOf(TrustedCustomerSupplier.class);
    assertThat(consumo).isInstanceOf(TrustedVechicleSupplier.class);
    assertThat(consumo.getDevice()).isNull();
    assertThat(consumo.getDate()).isEqualTo(LocalDateTime.parse("2018-09-06T11:41:00.00").atZone(ZoneId.systemDefault()).toInstant());
    assertThat(consumo.getTransactionDetailCode()).isEqualTo("MBIT");
  }

  @Test
  void testMapRecordToConsumoBuonoItalia() throws Exception {


    FrejusRecordDescriptor desc = new FrejusRecordDescriptor(mock(FrejusCustomerProvider.class));

    String data = "1"+"500600713010       "+"20180801064231"+"+"+"025870"+"021205"+"+"+"000000"+"000000"+"2200000"+"280/TRF_1808     "+"000012010340638032193                                             ";
    FrejusRecord record = desc.decodeRecordCodeAndCallSetFromString(data, "1", FILE_NAME, 0);
    record.setParentRecord(getHeader(COD_CLI_MBIT));

    Frejus consumo = subject.mapRecordToConsumo(record);

    assertThat(consumo).isNotInstanceOf(TrustedCustomerSupplier.class);
    assertThat(consumo).isNotInstanceOf(TrustedVechicleSupplier.class);
    assertThat(consumo.getDevice()).isNotNull();
    assertThat(consumo.getDevice().getSeriale()).isEqualTo("500600713010");
    assertThat(consumo.getDevice().getPan()).isBlank();

    assertThat(consumo.getDate()).isEqualTo(LocalDateTime.parse("2018-08-01T06:42:31.00").atZone(ZoneId.systemDefault()).toInstant());
    assertThat(consumo.getTransactionDetailCode()).isEqualTo("MBIT");
  }

  @Test
  void testMapRecordToConsumoBuonoFrancia() throws Exception {


    FrejusRecordDescriptor desc = new FrejusRecordDescriptor(mock(FrejusCustomerProvider.class));

    String data = "13081185009010=5002020180709184145+025445021204+0000000000002000005761/TUN-1807     000001010340525002999500205214042   ";
    FrejusRecord record = desc.decodeRecordCodeAndCallSetFromString(data, "1", FILE_NAME, 0);
    record.setParentRecord(getHeader(COD_CLI_FJFR) );
    Frejus consumo = subject.mapRecordToConsumo(record);

    assertThat(consumo).isNotInstanceOf(TrustedCustomerSupplier.class);
    assertThat(consumo).isNotInstanceOf(TrustedVechicleSupplier.class);
    assertThat(consumo.getDevice()).isNotNull();
    assertThat(consumo.getTransactionDetailCode()).isEqualTo("FJFR");
    assertThat(consumo.getDevice().getSeriale()).isEqualTo("500205214042");
    assertThat(consumo.getDevice().getPan()).isBlank();

    assertThat(consumo.getDate()).isEqualTo(LocalDateTime.parse("2018-07-09T18:41:45.00").atZone(ZoneId.systemDefault()).toInstant());
  }

  private static FrejusRecord getHeader(String codCli) {
    String       fileName = FILE_NAME;
    FrejusRecord header   = new FrejusRecord(fileName, 0);
    header.setNumeroCliente(codCli);
    return header;
  }

}
