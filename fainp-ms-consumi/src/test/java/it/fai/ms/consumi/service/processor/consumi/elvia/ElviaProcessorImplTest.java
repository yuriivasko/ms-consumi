package it.fai.ms.consumi.service.processor.consumi.elvia;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.ESE_TOT_STATUS;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
class ElviaProcessorImplTest {


  private final ConsumoValidatorService                         consumoValidatorService = mock(ConsumoValidatorService.class);
  private final StanziamentiProducer                            stanziamentiProducer = mock(StanziamentiProducer.class);
  private final ElviaConsumoDettaglioStanziamentoPedaggioMapper mapper               = mock(
    ElviaConsumoDettaglioStanziamentoPedaggioMapper.class);
  private final VehicleService                                  vehicleService       = mock(VehicleService.class);
  private final CustomerService                                 customerService      = mock(CustomerService.class);

  ElviaProcessorImpl elviaProcessor;

  private Elvia elvia;

  @BeforeEach
  void setUp() throws Exception {
    elviaProcessor = new ElviaProcessorImpl(consumoValidatorService,
                                            stanziamentiProducer,
                                            mapper,
                                            vehicleService,
                                            customerService);

    elvia = new Elvia(new Source(Instant.now(), Instant.now(), "test"), "test", Optional.of(new ElviaGlobalIdentifier("test")), TestDummyRecord.instance1);
    elvia.setContract(Optional.of(new Contract("test")));
    TollPoint tp = new TollPoint();
    tp.setTime(Instant.now());
    elvia.setTollPointExit(tp);

    final ValidationOutcome validationOutcome = new ValidationOutcome();
    final StanziamentiParams stanziamentiParams = new StanziamentiParams(new Supplier("test"));
    stanziamentiParams.setArticle(new Article("test"));
    validationOutcome.setStanziamentiParams(stanziamentiParams);

    given(consumoValidatorService.validate(any()))
      .willReturn(validationOutcome);

    given(customerService.findCustomerByContrattoNumero(any()))
      .willReturn(Optional.of(new Customer("test")));

    given(customerService.findCustomerByUuid(any()))
      .willReturn(Optional.of(new Customer("test")));

    given(customerService.findCustomerByDeviceSeriale(any(), any(TipoDispositivoEnum.class)))
      .willReturn(Optional.of(new Customer("test")));

    given(mapper.mapConsumoToDettaglioStanziamento(any()))
      .willReturn(mock(DettaglioStanziamento.class));

    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(Arrays.asList(mock(Stanziamento.class)));
  }

  @Test
  void validateAndProcessTOT() {

    elvia.setEseTotStatus(ESE_TOT_STATUS.TOT);
    final Amount amount = new Amount();
    amount.setVatRate(new BigDecimal("20").setScale(2, RoundingMode.HALF_UP));
    amount.setAmountExcludedVat(MonetaryUtils.strToMonetary("10","EUR"));
    elvia.setAmount(amount);

    ProcessingResult processingResult = elviaProcessor.validateAndProcess(elvia, new Bucket("testTOT"));

    assertThat(processingResult.getConsumo())
      .isEqualTo(elvia);

    assertThat(processingResult.getStanziamenti().size())
      .isEqualTo(1);

    ArgumentCaptor<Consumo> consumoArgumentCaptor = ArgumentCaptor.forClass(Consumo.class);
    then(mapper)
      .should()
      .mapConsumoToDettaglioStanziamento(consumoArgumentCaptor.capture());

    Elvia consumoTOT = (Elvia) consumoArgumentCaptor.getValue();
    assertThat(consumoTOT.getAmount().getAmountIncludedVat().toString())
      .isEqualTo("EUR 12");
  }

  @Test
  void validateAndProcessESE() {

    elvia.setEseTotStatus(ESE_TOT_STATUS.ESE);
    elvia.setEseGlobalIdentifier("::eseGlobalIdentifier::");

    final Amount amount = new Amount();
    amount.setVatRate(new BigDecimal("20").setScale(2, RoundingMode.HALF_UP));
    amount.setAmountExcludedVat(MonetaryUtils.strToMonetary("10", "EUR"));
    elvia.setAmount(amount);

    final Amount amountEse = new Amount();
    amountEse.setVatRate(new BigDecimal("0").setScale(2, RoundingMode.HALF_UP));
    amountEse.setAmountExcludedVat(MonetaryUtils.strToMonetary("2","EUR"));
    elvia.setEseAmount(amountEse);

    ProcessingResult processingResult = elviaProcessor.validateAndProcess(elvia, new Bucket("testESE"));

    final Consumo eseConsumo = processingResult.getConsumo();
    assertThat(tuple(eseConsumo.getGlobalIdentifier().getId(), eseConsumo.getAmount().getAmountExcludedVat()))
      .isEqualTo(tuple("::eseGlobalIdentifier::", elvia.getAmount().getAmountExcludedVat()));

    assertThat(processingResult.getStanziamenti().size())
      .isEqualTo(2);

    ArgumentCaptor<Consumo> consumoArgumentCaptor = ArgumentCaptor.forClass(Consumo.class);
    then(mapper)
      .should(times(2))
      .mapConsumoToDettaglioStanziamento(consumoArgumentCaptor.capture());

    Elvia consumoTOT = (Elvia) consumoArgumentCaptor.getAllValues().get(0);
    assertThat(consumoTOT.getAmount().getAmountIncludedVat().toString())
      .isEqualTo("EUR 12");
    assertThat(consumoTOT.getEseTotStatus())
      .isEqualTo(ESE_TOT_STATUS.TOT);

    Elvia consumoESE = (Elvia) consumoArgumentCaptor.getAllValues().get(1);
    assertThat(consumoESE.getAmount().getAmountIncludedVat().toString())
      .isEqualTo("EUR 2");
    assertThat(consumoESE.getEseTotStatus())
      .isEqualTo(ESE_TOT_STATUS.ESE);
  }
}
