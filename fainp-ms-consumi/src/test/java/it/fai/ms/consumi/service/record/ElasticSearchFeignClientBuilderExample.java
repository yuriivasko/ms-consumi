package it.fai.ms.consumi.service.record;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import feign.Feign;
import feign.codec.Decoder;
import feign.codec.Encoder;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;

@ExtendWith(SpringExtension.class)
@Tag("integration")
class ElasticSearchFeignClientBuilderExample {

  private static Encoder feignEncoder() {
    var jacksonConverter = new MappingJackson2HttpMessageConverter();

//    jacksonConverter.setObjectMapper(new JacksonConfiguration().jacksonObjectMapper());
    ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(jacksonConverter);
    return new SpringEncoder(objectFactory);
  }
  private Decoder feignDecoder() {
    var jacksonConverter = new MappingJackson2HttpMessageConverter();
    ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(jacksonConverter);
    return new SpringDecoder(objectFactory);
  }
  private ElasticSearchFeignClient esClient;

  @BeforeEach
  void setUp() throws Exception {
    esClient = Feign.builder()
                    .contract(new SpringMvcContract())
                    .encoder(feignEncoder())
                    .decoder(feignDecoder())
                    .target(ElasticSearchFeignClient.class, "http://exage-fai-05.northeurope.cloudapp.azure.com:5292/");
  }



  @Test
  void testCreate() {

    var record = createRecord();


    esClient.create(record.getId(),record.getRecordType(),record);
  }

  private ElasticSearchRecord<ElcitRecord> createRecord() {
    var record = new ElasticSearchRecord<ElcitRecord>();
    record.setIngestionTime(Instant.now().truncatedTo(ChronoUnit.SECONDS));
    ElcitRecord er = new ElcitRecord("Filename-example.txt",2);
    er.setParentRecord(getParent());
    er.setAnno(2017);
    er.setAmount("232938");
    er.setIngestion_time(new Date().toInstant().truncatedTo(ChronoUnit.SECONDS));
    er.setFileName("pippo.csv");

    record.setFileName(er.getFileName());
    record.setRawdata(er);
    record.setIngestionTime(er.getIngestion_time().truncatedTo(ChronoUnit.SECONDS));

    record.setRecordType(er.getClass()
                            .getSimpleName());

    return record;
  }
  private ElcitRecord getParent() {
    ElcitRecord parent = new ElcitRecord("Filename-example.txt",1);
    parent.setIngestion_time(Instant.now().truncatedTo(ChronoUnit.SECONDS));
    parent.setDate("2017-10-21");
    return parent;
  }
  @Test
  void testGet() {
    var record = createRecord();
    System.out.println(record.getId());
    esClient.create(record.getId(), record.getRecordType(), record);
    var response = esClient.getRecord(record.getRecordType(), "" + record.getId());
    assertThat(response.getRawdata()).isInstanceOf(ElcitRecord.class);
    assertThat(response.getFileName()).isEqualTo(record.getFileName());

    assertThat(response.getRawdata()).isEqualToComparingFieldByFieldRecursively(record.getRawdata());
    assertThat(response.getRawdata().getFileName()).isEqualTo(record.getRawdata().getFileName());
    assertThat(response.getRawdata().getRowNumber()).isEqualTo(record.getRawdata().getRowNumber());
    assertThat(response.getRawdata().getServicePartner()).isEqualToComparingFieldByField(record.getRawdata().getServicePartner());
  }

}
