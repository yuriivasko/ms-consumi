package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

public class ElviaTestBuilder {

  private ElviaTestBuilder() {
  }

  public static ElviaTestBuilder newInstance() {
    return new ElviaTestBuilder();
  }

  public Elvia build() {
    var elvia = new Elvia(new Source(Instant.now(), Instant.now(), "::source::"), "", GlobalIdentifierTestBuilder.newElviaGlobalIdentifier(), TestDummyRecord.instance1);
    return elvia;
  }

}
