package it.fai.ms.consumi.domain.parametri_stanziamenti;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;

@Tag("unit")
class StanziamentiParamsTest {

  @Test
  void transactionDetail_doesnot_match() {

    // given

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when

    var match = stanziamentiParams.matchTransactionDetail("::transactionDetail-nomatching::");

    // then

    assertThat(match).isFalse();
  }

  @Test
  void transactionDetail_match() {

    // given

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when

    var match = stanziamentiParams.matchTransactionDetail("::transactionDetail::");

    // then

    assertThat(match).isTrue();
  }

  @Test
  void transactionDetail_vatRate_doesnot_match() {

    // given

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withVatRate(1.0f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when

    var match = stanziamentiParams.match("::transactionDetail-nomatch::", new BigDecimal("2.0"));

    // then

    assertThat(match).isFalse();
  }

  @Test
  void transactionDetail_vatRate_fareClass_doesnot_match() {

    // given

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withVatRate(1.0f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .withFareClass("::fareClass::")
                                                          .build();

    // when

    var match = stanziamentiParams.match("::transactionDetail-nomatch::", new BigDecimal("2.0"), "::fareClass-nomatch::");

    // then

    assertThat(match).isFalse();
  }

  @Test
  void transactionDetail_vatRate_fareClass_match() {

    // given

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withVatRate(1.0f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .withFareClass("::fareClass::")
                                                          .build();

    // when

    var match = stanziamentiParams.match("::transactionDetail::", new BigDecimal("1.0"), "::fareClass::");

    // then

    assertThat(match).isTrue();
  }

  @Test
  void transactionDetail_vatRate_match() {

    // given

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withVatRate(1.0f)
                                                                                         .build())
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .build();

    // when

    var match = stanziamentiParams.match("::transactionDetail::", new BigDecimal("1.0"));

    // then

    assertThat(match).isTrue();
  }

}
