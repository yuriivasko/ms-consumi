package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.Date;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.repository.NotificationSetupRepository;
import it.fai.common.notification.service.NotificationMapper;
import it.fai.common.notification.service.NotificationMessageSender;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardJob;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd.TrackyCardCarbStdProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.carburanti.TrackyCardCarbStdRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Tag("unit")
public class TrackyCardCarburantiStandardJobTest {

  private RecordConsumoMapper<TrackyCardCarburantiStandardRecord, TrackyCardCarburantiStandard> consumoMapper;
  private TrackyCardCarburantiStandardDescriptor                                                descriptor;
  private String                                                                                filename;
  private TrackyCardCarburantiStandardJob                                                       job;
  private TrackyCardCarbStdProcessor                                        processor;
  private RecordPersistenceService                                                              recordPersistenceService;
  private StanziamentiParamsValidator                                                           stanziamentiParamsValidator;
  private StanziamentiToNavPublisher                                                          stanziamentiToNavJmsProducer;
  private final PlatformTransactionManager transactionManager                       = mock(PlatformTransactionManager.class);

  @BeforeEach
  public void setUp() throws Exception {
    filename = new File(TrackyCardCarburantiStandardJobTest.class.getResource("/test-files/trackycardcarbstd/AT02GFP20170123223338.TXT.20170124031647")
                                                                 .toURI()).getAbsolutePath();
    recordPersistenceService = mock(RecordPersistenceService.class);
    stanziamentiParamsValidator = mock(StanziamentiParamsValidator.class);
    processor = mock(TrackyCardCarbStdProcessor.class);
    doReturn(mock(ProcessingResult.class)).when(processor)
                                          .validateAndProcess(any(), any());
    consumoMapper = new TrackyCardCarbStdRecordConsumoMapper();
    descriptor = new TrackyCardCarburantiStandardDescriptor();
    stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);

  }

  @Test
  public void testEmulateExceptionInSingleRowInJob() {
    final ValidationOutcome vo = new ValidationOutcome();
    vo.setValidationOk(true);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(new ValidationOutcome());
    final NotificationService notificationService = spy(new NotificationService(mock(NotificationMessageSender.class),
                                                                                mock(NotificationSetupRepository.class),
                                                                                mock(NotificationMapper.class), "TrackycardCarbStd"));
    job = new TrackyCardCarburantiStandardJob(transactionManager,  stanziamentiParamsValidator, notificationService, recordPersistenceService,
                                              descriptor, processor, consumoMapper, stanziamentiToNavJmsProducer);


    assertThat(job.getSource()).isEqualTo("TRACKYCARD_STANDARD");

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");
    doThrow(runtimeExTest).when(recordPersistenceService)
                          .persist(any());
    doThrow(runtimeExTest).when(recordPersistenceService)
                          .persist(any());

    doAnswer(invocation -> {
      assertThat(((EnrichedNotification) invocation.getArgument(0)).getCode()).contains("FCJOB-200"); // warning
      // log.info("Error: "+invocation.getArgument(0)+" values "+invocation.getArgument(1));
      return null;
    }).when(notificationService)
      .notify(any()); //anyString(), anyMap());

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);

    verify(notificationService, atLeastOnce()).notify(any());
  }

  @Test
  public void testEmulateUnhandledExceptionInJob() {
    final ValidationOutcome vo = new ValidationOutcome();
    vo.setValidationOk(true);
    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");
    when(stanziamentiParamsValidator.checkConsistency()).thenThrow(runtimeExTest);
    doThrow(runtimeExTest).when(stanziamentiParamsValidator)
                          .checkConsistency();
    final NotificationService notificationService = spy(new NotificationService(mock(NotificationMessageSender.class),
                                                                                mock(NotificationSetupRepository.class),
                                                                                mock(NotificationMapper.class), "TrackycardCarbStd"));
    job = new TrackyCardCarburantiStandardJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService,
                                              descriptor, processor, consumoMapper, stanziamentiToNavJmsProducer);

    assertThat(job.getSource()).isEqualTo("TRACKYCARD_STANDARD");

    doAnswer(invocation -> {
      assertThat(((EnrichedNotification) invocation.getArgument(0)).getCode()).contains("-500"); // unhandled exception
      return null;
    }).when(notificationService)
      .notify(any()); //anyString(), anyMap());

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);

    verify(notificationService, atLeastOnce()).notify(any()); //anyString(), anyMap());
  }

  @Test
  public void testKOStanziamentiCheckConsistency() {

    final ValidationOutcome vo = new ValidationOutcome().message(new Message("XXX").text("this is test error!"));
    vo.setValidationOk(false);

    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    final NotificationService notificationService = spy(new NotificationService(mock(NotificationMessageSender.class),
                                                                                mock(NotificationSetupRepository.class),
                                                                                mock(NotificationMapper.class), "TrackycardCarbStd"));

    job = new TrackyCardCarburantiStandardJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService,
                                              descriptor, processor, consumoMapper, stanziamentiToNavJmsProducer);

    assertThat(job.getSource()).isEqualTo("TRACKYCARD_STANDARD");

    doAnswer(invocation -> {
      assertThat(((EnrichedNotification) invocation.getArgument(0)).getCode()).contains("CHKPS-XXX");
      return null;
    }).when(notificationService)
      .notify(any()); //anyString(), anyMap());

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);

    verify(notificationService, atLeastOnce()).notify(any());
  }

  @Test
  public void testOK() {
    final ValidationOutcome vo = new ValidationOutcome();
    vo.setValidationOk(true);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(new ValidationOutcome());
    final NotificationService notificationService = mock(NotificationService.class);

    job = new TrackyCardCarburantiStandardJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService,
                                              descriptor, processor, consumoMapper, stanziamentiToNavJmsProducer);

    assertThat(job.getSource()).isEqualTo("TRACKYCARD_STANDARD");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);
    verifyZeroInteractions(notificationService);

  }


}
