package it.fai.ms.consumi.service.report;

import static it.fai.ms.consumi.web.rest.CarburantiDaFatturareResponseDtoTest.init;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.config.MessageSourceConfiguration;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.CarburantiDetailDto;

@Tag("unit")
public class ReportCsvTest {

  @Test
  public void testToCsvCarburantiDetailDto() {
    ReportCsv rj = new ReportCsv(new MessageSourceConfiguration().resourceBundleMessageSource());

    List<CarburantiDetailDto> data = Arrays.asList(init(new CarburantiDetailDto(), d -> {
      d.setCodiceDispositivo("1234");
      d.setDataUtilizzo(ZonedDateTime.parse("2007-12-03T10:15:30+01:00[Europe/Rome]").toInstant());
      d.setDescrizioneArticolo("DIESEL");
      d.setErogatore("01");
      d.setId("1213212");
      d.setNazioneTarga("IT");
      d.setQuantita(new CarburantiDetailDto.Quantita(new BigDecimal("32.254"), "LT"));
      d.setTarga("CD345AG");
      d.setPuntoErogazione("Test 123");
    }));

    String json = rj.toCsv(CarburantiDetailDto.class, data, Locale.ITALIAN, null);
    System.out.println(json);
    assertThat(json).isEqualTo(
    /*@formatter:off*/
         "FAIPASS/TRACKY CARD;NAZIONE TARGA;TARGA;STAZIONE;DATA;ORA;U.M.;QTA;PRODOTTO;DESCRIZIONE PUNTO\r\n" +
         "1234;IT;CD345AG;01;03/12/2007;10:15:30;LT;32,254;DIESEL;Test 123\r\n"
    /*@formatter:on*/
    );

  }

  @Test
  public void testToCsvSplitted() {
    ReportCsv rj = new ReportCsv(new MessageSourceConfiguration().resourceBundleMessageSource());

    List<TestClassSplitted> data = Arrays.asList(init(new TestClassSplitted(), d -> {
      d.a = Instant.EPOCH;
    }));

    String json = rj.toCsv(TestClassSplitted.class, data, Locale.ITALIAN, null);
    System.out.println(json);
    assertThat(json).isEqualTo(
     /*@formatter:off*/
          "date;time\r\n" +
          "1970-01-01;01:00:00\r\n"
     /*@formatter:on*/
     );
  }

  @Test
  public void testToCsvSplittedZoned() {
    ReportCsv rj = new ReportCsv(new MessageSourceConfiguration().resourceBundleMessageSource());

    List<TestClassSplitted> data = Arrays.asList(init(new TestClassSplitted(), d -> {
      d.a = Instant.EPOCH;
    }));

    String json = rj.toCsv(TestClassSplitted.class, data, Locale.ITALIAN, ZoneId.of("America/New_York"));
    System.out.println(json);
    assertThat(json).isEqualTo(
    /*@formatter:off*/
        "date;time\r\n" +
        "1969-12-31;19:00:00\r\n"
    /*@formatter:on*/
    );

  }

  @Test
  public void testToCsv() {
    ReportCsv rj = new ReportCsv(new MessageSourceConfiguration().resourceBundleMessageSource());

    List<TestClass> data = Arrays.asList(init(new TestClass(), d -> {
      d.a = Instant.EPOCH;
    }));

    String json = rj.toCsv(TestClass.class, data, Locale.ITALIAN, null);
    System.out.println(json);
    assertThat(json).isEqualTo(
     /*@formatter:off*/
         "a\r\n" +
         "01/01/1970 01:00:00\r\n"
     /*@formatter:on*/
     );

  }

  @Test
  public void testToCsvWithHeader() {
    ReportCsv rj = new ReportCsv(new MessageSourceConfiguration().resourceBundleMessageSource());

    List<TestClassWithHeader> data = Arrays.asList(init(new TestClassWithHeader(), d -> {
      d.a = Instant.EPOCH;
    }));

    String json = rj.toCsv(TestClassWithHeader.class, data, Locale.ITALIAN, null);
    System.out.println(json);
    assertThat(json).isEqualTo(
     /*@formatter:off*/
         "FAIPASS/TRACKY CARD\r\n" +
         "01/01/1970 01:00:00\r\n"
     /*@formatter:on*/
     );
  }

  @Test
  public void testToCsvWithHeaderFormatter() {
    ReportCsv rj = new ReportCsv(new MessageSourceConfiguration().resourceBundleMessageSource());

    List<TestClassWithFormatter> data = Arrays.asList(init(new TestClassWithFormatter(), d -> {
      d.a = Instant.EPOCH;
    }));

    String json = rj.toCsv(TestClassWithFormatter.class, data, Locale.ITALIAN, null);
    System.out.println(json);
    assertThat(json).isEqualTo(
   /*@formatter:off*/
       "a\r\n" +
       "1970-01-01\r\n"
   /*@formatter:on*/
   );

  }

  @Test
  public void testToCsvWithHeaderFormatterZoned() {
    ReportCsv rj = new ReportCsv(new MessageSourceConfiguration().resourceBundleMessageSource());

    List<TestClass> data = Arrays.asList(init(new TestClass(), d -> {
      d.a = Instant.EPOCH;
    }));

    String json = rj.toCsv(TestClass.class, data, Locale.ITALIAN, ZoneId.of("America/New_York"));
    System.out.println(json);
    assertThat(json).isEqualTo(
     /*@formatter:off*/
         "a\r\n" +
         "31/12/1969 19:00:00\r\n"
     /*@formatter:on*/
     );

  }
}
