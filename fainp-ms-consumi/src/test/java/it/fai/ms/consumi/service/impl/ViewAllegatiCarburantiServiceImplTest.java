package it.fai.ms.consumi.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.fattura.CarburantiBodyDTO;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.client.VehicleClientService;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;
import it.fai.ms.consumi.repository.device.TipoSupporto;
import it.fai.ms.consumi.repository.device.TipoSupportoRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepositoryImpl;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2Repository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2RepositoryImpl;
import it.fai.ms.consumi.service.ViewAllegatiCarburantiService;
import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteJmsProducer;
import it.fai.ms.consumi.testutils.ConsumiTestUtility;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = { "it.fai.ms.consumi.util" })
@DataJpaTest(showSql = false)
@ActiveProfiles(profiles = { "default", "swagger" })
@Tag("unit")
public class ViewAllegatiCarburantiServiceImplTest {

  @MockBean
  private DettaglioCarburanteJmsProducer dettaglioCarburanteJmsProducer;

  @Autowired
  EntityManager em;

  StanziamentiParamsRepository             stanziamentiParamsRepository;
  ViewAllegatoCarburantiSezione2Repository viewAllegatoCarburantiSezione2Repository;

  ViewAllegatiCarburantiService viewAllegatiCarburantiService;

  DettaglioStanziamantiRepository repository;

  PetrolPumpService    petrolpumpClient     = mock(PetrolPumpService.class);
  
  TipoSupportoRepository    tipoSupportoRepo     = mock(TipoSupportoRepository.class);

  StanziamentoEntity                    s;
  DettaglioStanziamentoCarburanteEntity dsc;

  @BeforeEach
  void setUp() throws Exception {

    String authToken = UUID.randomUUID()
                           .toString();
    when(petrolpumpClient.getPoint(any())).thenReturn(newPoint());

    viewAllegatoCarburantiSezione2Repository = new ViewAllegatoCarburantiSezione2RepositoryImpl(em);
    stanziamentiParamsRepository = mock(StanziamentiParamsRepositoryImpl.class);
    viewAllegatiCarburantiService = new ViewAllegatiCarburantiServiceImpl(viewAllegatoCarburantiSezione2Repository,
                                                                          stanziamentiParamsRepository, petrolpumpClient, tipoSupportoRepo);

    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
                                                                                  new DettaglioStanziamentoGenericoEntityMapper(),
                                                                                  new DettaglioStanziamentoPedaggioEntityMapper(),
                                                                                  new DettaglioStanziamentoTrenoEntityMapper());
    repository = new DettaglioStanziamentiRepositoryImpl(em, dettaglioStanziamentoEntityMapper,
                                                         new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper));

    ConsumiTestUtility consumiTestUtility = new ConsumiTestUtility(em);
    s = consumiTestUtility.createStanziamento("1");
    dsc = consumiTestUtility.createDettaglioCarburante("1");
    consumiTestUtility.createViewDettaglioCarburanti(dsc.getId()
                                                        .toString(),
                                                     "1");
    consumiTestUtility.relatedStanziamentoOnDettagli(s, dsc);
    em.flush();
  }

  private PointDTO newPoint() {
    PointDTO point = new PointDTO();
    point.setName("Esso Milano 2");
    return point;
  }

  @Test
  public void createBodyMessageAndSection2() throws Exception {
    when(tipoSupportoRepo.findByTipoDispositivo(any())).thenReturn(newTipoSupporto());
    List<UUID> idDettagli = repository.findDettagliStanziamentoIdByCodiciStanziamento(Arrays.asList(s.getCode()));
    assertThat(idDettagli).hasSize(1);

    List<ViewAllegatoCarburantiSezione2Entity> dettagliStanziamento = viewAllegatiCarburantiService.getDettagliStanziamento(idDettagli);
    assertThat(dettagliStanziamento).hasSize(1);

    ViewAllegatoCarburantiSezione2Entity carburante = dettagliStanziamento.get(0);
    StanziamentiParams paramStanziamenti = new StanziamentiParams(new Supplier(carburante.getCodiceFornitore()));
    Article article = new Article("DIESEL");
    article.setDescription("GASOLIO");
    paramStanziamenti.setArticle(article);
    Optional<StanziamentiParams> paramStanziamentiOpt = Optional.of(paramStanziamenti);
    when(stanziamentiParamsRepository.findByTransactionDetailAndCodiceFornitoreAndRecordCodeAndTipoDettaglio(any(), any(), any(),
                                                                                                             any())).thenReturn(paramStanziamentiOpt);

    RequestsCreationAttachments request = mock(RequestsCreationAttachments.class);
    when(request.getNomeIntestazioneTessere()).thenReturn("TEST");
    when(request.getTipoServizio()).thenReturn("::servizio::");
    List<CarburantiBodyDTO> bodies = viewAllegatiCarburantiService.createBodyMessageAndSection2(dettagliStanziamento, request);

    assertThat(bodies).isNotEmpty();
    em.detach(dsc);
    em.detach(s);
  }

  private TipoSupporto newTipoSupporto() {
    TipoSupporto tipoSupporto = new TipoSupporto();
    tipoSupporto.setTipoSupportoFix("::deviceType::");
    tipoSupporto.setTipoSupportoFix("::tipo-supporto-fix::");
    return tipoSupporto;
  }

}
