package it.fai.ms.consumi.config;

import it.fai.ms.consumi.client.GeoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.Cache;
import org.springframework.cache.concurrent.ConcurrentMapCacheFactoryBean;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import it.fai.ms.consumi.api.trx.model.mapper.TrxReqConsumoMapper;
import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteJmsProducer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class AdditionalTestContextBeans {

  @MockBean
  private DettaglioCarburanteJmsProducer dettaglioCarburanteJmsProducer;
  
  @Autowired
  private GeoClient geoClient;



  @Bean
  public TrxReqConsumoMapper trxReqConsumoMapper(){
    return new TrxReqConsumoMapper(geoClient, "");
  }

  @Bean
  public SimpleCacheManager cacheManager(){
    SimpleCacheManager cacheManager = new SimpleCacheManager();
    List<Cache> caches = new ArrayList<Cache>();
    caches.add(cacheBean().getObject());
    cacheManager.setCaches(caches );
    return cacheManager;
  }

  @Bean
  public ConcurrentMapCacheFactoryBean cacheBean(){
    ConcurrentMapCacheFactoryBean cacheFactoryBean = new ConcurrentMapCacheFactoryBean();
    cacheFactoryBean.setName("default");
    return cacheFactoryBean;
  }

}
