package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.byLessThan;
import static org.assertj.core.api.Assertions.tuple;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.ActiveProfiles;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.client.PetrolPumpClient;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ParamStanziamentoMatchingParamsSupplier;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.enumeration.PriceSource;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0201;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0301;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0302;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.processor.consumi.tokheim.TokheimProcessor;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@ComponentScan(
               basePackages = { "it.fai.ms.consumi.service", "it.fai.ms.consumi.client", "it.fai.ms.consumi.adapter",
                                "it.fai.ms.consumi.repository", "it.fai.ms.consumi.scheduler", "it.fai.ms.consumi.util" },
               excludeFilters = { @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.fatturazione\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.report\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.jms\\.listener\\.JmsListenerCreationAttachmentInvoiceRequest"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.scheduler\\.jobs\\.flussi\\.consumi\\.integration\\tollcollect\\..*"),
                                  @ComponentScan.Filter(
                                                        type = FilterType.REGEX,
                                                        pattern = "it\\.fai\\.ms\\.consumi\\.service\\.consumer\\.CreationAttachmentInvoiceConsumer"), })
@ActiveProfiles(profiles = "integration")
@Tag("integration")
public class TokheimJobIntTest extends AbstractIntegrationTestSpringContext {

  private static final String fileName = "00012921.T2.2017041509102";
  private Path                path;

  @MockBean
  private ElasticSearchFeignClient elasticSearchFeignClient;

  @MockBean
  private AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;

  @MockBean
  private NotificationService notificationService;

  @MockBean
  private AnagaziendeClient anagaziendeClient;
  @MockBean
  private PetrolPumpClient  petrolPump;

  @SpyBean
  private TokheimProcessor processor;

  @SpyBean
  private StanziamentiParamsValidator stanziamentiParamsValidator;

  @SpyBean
  private PriceCalculatedByNavService priceNav;
  @SpyBean
  private StanziamentiParamsService paramStanziamentiService;

  @Autowired
  private TokheimJob job;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @BeforeEach
  public void setup() throws Exception {
    path = Paths.get(TokheimJobIntTest.class.getResource("/test-files/tokheim/")
                                            .toURI());
    assertThat(job).isNotNull();

  }

  @Test
  public void elaborateOneRecord() {

    ServicePartner serviceProvider = init(new ServicePartner("::id::"), s -> {
      s.setPartnerCode("tokheim");
      s.setPartnerName("tokheim");
      s.setFormat(Format.TOKHEIM);
      s.setPriceSoruce(PriceSource.by_invoice);
    });

    String filePath = path.resolve(fileName)
                          .toAbsolutePath()
                          .toString();
    when(petrolPump.searchPoint(any(), Mockito.eq("0039210101"))).thenReturn(init(new PointDTO(),
                                                                                              p ->{ p.setProviderCode("IT13");
                                                                                              p.setCode("ppcode392");})
    );

    String result = job.process(filePath, System.currentTimeMillis(), new Date().toInstant(), serviceProvider);

    assertThat(result).isNotNull()
                      .isEqualTo("tokheim");

    var record = validatePersitRecord();

    verify(processor, times(1)).validateAndProcess(any(), any());

    verify(priceNav, times(1)).setCostCalculatedAmountByNav(any(), any());

    validateParametriStanziamentyQuery();

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    assertThat(dettaglioStanziamenti.size()).isEqualTo(2);

    assertThat(dettaglioStanziamenti).allMatch(s -> s.getStanziamenti()
                                                     .size() == 1);



    var dettaglioStanziamento = dettaglioStanziamenti.get(0);

    assertThat(dettaglioStanziamento).isInstanceOfSatisfying(DettaglioStanziamentoCarburanteEntity.class, ds -> {
      assertThat(ds.getPuntoErogazione()).isEqualTo("ppcode392");
      assertThat(ds.getCodiceTrackycard()).isEqualTo(record.get0301()
                                                           .getNrTrackycard());

      // se non presente FAIPASSCARD viene solitamente valorizzato con la targa (caso da portale partner). Default
      // vuoto.
      assertThat(ds.getAltroSupporto()).isNull();

      assertThat(ds.getDataOraUtilizzo()).isEqualTo(toInstant("2018-04-13T20:01:29"));
      assertThat(ds.getErogatore()).isEqualTo(record.getNrErogatore());
      assertThat(ds.getTransactionDetailCode()).isEqualTo(record.getCodiceArticoloFornitore());
      assertThat(ds.getRecordCode()).isEqualTo("0302");
      assertThat(ds.getQuantita()).isEqualByComparingTo(new BigDecimal(record.getQuantita()).divide(new BigDecimal("100")));
      assertThat(ds.getKm()).isEqualByComparingTo(BigDecimal.ZERO);
      String prezzoUnitario = record.getPrezzoUnitario();
      assertThat(ds.getPrezzoUnitarioDiRiferimentoNoiva()).isCloseTo(removeVat(prezzoUnitario, "1000", new BigDecimal("22")),
                                                                     byLessThan(new BigDecimal("0.00001")));
      // costo proveniente da interrogazione nav
      if(ds.getPrezzoUnitarioCalcolatoNoiva()!=null)
        assertThat(ds.getPrezzoUnitarioCalcolatoNoiva()).isEqualByComparingTo("1.08934");// verificare da Nav
      else if(ds.getCostoUnitarioCalcolatoNoiva()!=null)
        assertThat(ds.getCostoUnitarioCalcolatoNoiva()).isEqualByComparingTo("1.08934");// verificare da Nav
      else
        assertFalse(true,"Costo e Ricavo entrambi non valorizzati");

      assertThat(ds.getPercIva()).isEqualByComparingTo(new BigDecimal(record.getPercentualeIva()).divide(new BigDecimal("100")));
      assertThat(ds.getCurrencyCode()).isEqualTo(record.get0301()
                                                       .getValuta());
      assertThat(ds.getExchangeRate()).isNull();
      assertThat(ds.getFileName()).isEqualTo(StringUtils.substringAfterLast(filePath, "/"));
      assertThat(ds.getTipoConsumo()).isEqualTo(InvoiceType.D);
      assertThat(ds.getTipoSorgente()).isEqualTo(Format.TOKHEIM);
      assertThat(ds.getCodiceFornitoreNav()).isEqualTo("F006716");
      assertThat(ds.getPartnerCode()).isEqualTo("IT13");
      assertThat(ds.getDataAcquisizioneFlusso()).isEqualTo(toInstant("2018-04-15T00:00:00"));
      assertThat(ds.getVeicoloClasse()).isEqualTo("6");
      assertThat(ds.getVeicoloTarga()).isEqualTo("FJ976BV");
      assertThat(ds.getVeicoloNazioneTarga()).isEqualTo("I");
      assertThat(ds.getTransactionSign()).isEqualTo("+");// TODO: verificare se deve provenire da record

    });


    verifyZeroInteractions(notificationService);
    Mockito.validateMockitoUsage();
  }

  @Test
  public void testFile12() {
    ServicePartner serviceProvider = init(new ServicePartner("::id::"), s -> {
      s.setPartnerCode("tokheim");
      s.setPartnerName("tokheim");
      s.setFormat(Format.TOKHEIM);
      s.setPriceSoruce(PriceSource.by_invoice);
    });

    String filePath = path.resolve("test12")
                          .toAbsolutePath()
                          .toString();
    when(petrolPump.searchPoint(any(), Mockito.eq("386080010"))).thenReturn(init(new PointDTO(),
                                                                                              p ->{ p.setProviderCode("IT13");
                                                                                              p.setCode("ppcode392");})
    );

    String result = job.process(filePath, System.currentTimeMillis(), new Date().toInstant(), serviceProvider);


  }



  private void validateParametriStanziamentyQuery() {
    ArgumentCaptor<ParamStanziamentoMatchingParamsSupplier> captor = ArgumentCaptor.forClass(ParamStanziamentoMatchingParamsSupplier.class);
    verify(stanziamentiParamsValidator).existsParamsStanziamento(captor.capture());
    assertThat(captor.getValue()
                     .getServicePartner()
                     .getPartnerCode()).isEqualTo("IT13");
  }

  private TokheimRecord0302 validatePersitRecord() {
    var type = ArgumentCaptor.forClass(String.class);

    @SuppressWarnings("unchecked")
    ArgumentCaptor<ElasticSearchRecord<TokheimRecord>> captureRecord = ArgumentCaptor.forClass(ElasticSearchRecord.class);
    verify(elasticSearchFeignClient, times(1)).create(anyString(), type.capture(), captureRecord.capture());

    List<ElasticSearchRecord<TokheimRecord>> esRecords = captureRecord.getAllValues();
    validateHierarchy(esRecords);

    var record = validateRecord(esRecords);
    return record;
  }

  private TokheimRecord0302 validateRecord(List<ElasticSearchRecord<TokheimRecord>> esRecords) {
    assertThat(esRecords).extracting(esR -> esR.getRawdata())
                         .extracting(r -> tuple(((TokheimRecord0302) r).get0301()
                                                                       .getNrTrackycard()))
                         .containsExactly(tuple("7896970000312901002"));

    var record = (TokheimRecord0302) esRecords.get(0)
                                              .getRawdata();
    return record;
  }

  private BigDecimal removeVat(String prezzoUnitario, String scale, BigDecimal vat) {
    return new BigDecimal(prezzoUnitario).divide(new BigDecimal(scale), 5, RoundingMode.HALF_EVEN)
                                         .multiply(new BigDecimal("100").divide(vat.add(new BigDecimal("100")), 5, RoundingMode.HALF_EVEN));
  }

  private void validateHierarchy(List<ElasticSearchRecord<TokheimRecord>> esRecords) {
    assertThat(esRecords).allSatisfy(r -> {
      assertThat(r.getRawdata()).isInstanceOfSatisfying(TokheimRecord0302.class, r302 -> {
        assertThat(r302.getParentRecord()).isInstanceOfSatisfying(TokheimRecord0301.class, r301 -> {
          assertThat(r301.getParentRecord()).isInstanceOf(TokheimRecord0201.class);
        });
      });
    });
  }

  private Instant toInstant(String text1) {
    return LocalDateTime.parse(text1)
                        .atZone(ZoneId.of("Europe/Rome"))
                        .toInstant();
  }

  @SafeVarargs
  public static <T> T init(T obj, Consumer<T>... consumers) {
    for (Consumer<T> consumer : consumers) {
      consumer.accept(obj);
    }
    return obj;
  }
}
