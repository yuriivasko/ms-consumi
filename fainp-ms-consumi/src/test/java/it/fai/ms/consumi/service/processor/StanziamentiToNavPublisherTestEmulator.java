package it.fai.ms.consumi.service.processor;

import java.util.List;

import org.mockito.Mockito;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsPropertiesResolver;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.service.consumer.StanziamentiToNavConsumer;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.model.StanziamentoToNavMapper;
import it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer;
import it.fai.ms.consumi.service.navision.StanziamentiToNavService;

@Profile({"default", "integration"})
@Primary
@Service
@Transactional
public class StanziamentiToNavPublisherTestEmulator implements StanziamentiToNavPublisher {

  private StanziamentiToNavConsumer stanziamentiToNavConsumer;
  private StanziamentiToNavJmsProducer stanziamentiToNavJmsProducer;
  
  public StanziamentiToNavPublisherTestEmulator(final StanziamentoToNavMapper _stanziamentoToNavMapper,
                                                final StanziamentoRepository _stanziamentoRepository) {
    this.stanziamentiToNavConsumer = new StanziamentiToNavConsumer(Mockito.mock(StanziamentiToNavService.class), _stanziamentoToNavMapper, _stanziamentoRepository, stanziamentiToNavJmsProducer);
    JmsProperties jmsProperties = new JmsProperties(new JmsPropertiesResolver(Mockito.mock(Environment.class)));
    jmsProperties.setUrl("no-host");
    this.stanziamentiToNavJmsProducer = new StanziamentiToNavJmsProducer(jmsProperties,_stanziamentoRepository);
  }

  public void publish(final List<StanziamentoMessage> _stanziamentoMessages) {
    stanziamentiToNavJmsProducer.publish(_stanziamentoMessages);
    stanziamentiToNavConsumer.consume(_stanziamentoMessages);
  }
  
  

}
