package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

@Tag("integration")
@ActiveProfiles(profiles= "default")
public class TrackycardCarburantiStandardJobDaTabellaPrezziProvvisorioProvvisorioWithESIntTest
  extends AbstractTrackycardCarburantiStandardJobWithEsIntTest {

  @Test
  @Transactional
  public void test() {
    String filename = getAbsolutePath("/test-files/trackycardcarbstd/AT02GFP20170123223338.TXT.20170124031647");
    ServicePartner servicePartner = findServicePartner("AT02");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(),servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    int countRicavi = 0;
    int countCosti = 0;

    for (Stanziamento stanziamento : stanziamentoList) {
      // tutti provvisori
      assertThat(stanziamento.getInvoiceType()).isEqualTo(InvoiceType.P);

      if(stanziamento.getGeneraStanziamento() == GeneraStanziamento.COSTO) {
        countCosti++;
        assertThat(stanziamento.getCosto()).isGreaterThan(BigDecimal.ZERO);
      }
      if(stanziamento.getGeneraStanziamento() == GeneraStanziamento.RICAVO) {
        countRicavi++;
        assertThat(stanziamento.getPrezzo()).isGreaterThan(BigDecimal.ZERO);
      }
      assertThat(stanziamento.getAmountCurrencyCode()).isEqualTo("EUR");
    }

    assertThat(countRicavi).isEqualTo(3);
    assertThat(countCosti).isEqualTo(3);

    // assertThat(job.isRunning()).isEqualTo(true);
  }
}
