package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.efservice.message.consumi.AllineamentoDaConsumoMessage;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollectGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.tollcollect.TollCollectProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.tollcollect.TollCollectRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.File;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.*;

@Tag("unit")
class TollCollectJobTest {

  private final TollCollectRecordDescriptor      descriptor                   = new TollCollectRecordDescriptor();

  private final StanziamentiParamsValidator      stanziamentiParamsValidator  = mock(StanziamentiParamsValidator.class);
  private final NotificationService              notificationService          = mock(NotificationService.class);
  private final RecordPersistenceService         recordPersistenceService     = mock(RecordPersistenceService.class);

  private final TollCollectRecordConsumoMapper consumoMapper                  = mock(TollCollectRecordConsumoMapper.class);
  private final TollCollectProcessor             processor                    = mock(TollCollectProcessor.class);
  private final StanziamentiToNavPublisher     stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
  private final AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer = mock(AllineamentoDaConsumoJmsProducer.class);
  private final PlatformTransactionManager       transactionManager           = mock(PlatformTransactionManager.class);


  private final ServicePartner servicePartner = mock(ServicePartner.class);
  private final RecordsFileInfo recordsFileInfo = mock(RecordsFileInfo.class);

  private Stanziamento stanziamento;

  private TollCollectJob job;
  private String     fileName = "O_DTC5659_IZ_2_0411032037_S_051117_185190";
  private String     filePath = "";

  @BeforeEach
  void setUp() throws Exception {
    filePath = new File(TollCollectJobTest.class.getResource("/test-files/tollcollect/" + fileName).toURI()).getAbsolutePath();
    job = new TollCollectJob(
      transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      descriptor,
      processor,
      consumoMapper,
      stanziamentiToNavJmsProducer,
      allineamentoDaConsumoJmsProducer);

    given(recordsFileInfo.fileIsValid())
      .willReturn(true);

    stanziamento = new Stanziamento("");
    stanziamento.setTipoFlusso(TypeFlow.NONE);
    stanziamento.setInvoiceType(InvoiceType.D);
  }


  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");

    given(stanziamentiParamsValidator.checkConsistency())
      .willThrow(runtimeExTest);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);
    then(notificationService)
      .should(atLeastOnce())
      .notify(any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_recordPersistenceService_throw_exception_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    final RuntimeException runtimeExTest = new RuntimeException("This is an exception to test behaviour");
    willThrow(runtimeExTest)
      .given(recordPersistenceService)
      .persist(any());

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(atLeastOnce())
      .notify(any());
  }

  @SuppressWarnings("Duplicates")
  @Test
  public void process_if_checkConsistency_fail_will_send_notification() {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(2))
      .notify(any());
  }

  @Test
  public void process_if_all_ok_will_process_all_detail_rows() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TollCollect.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(
        buildStanziamento("1", Arrays.asList(
          buildDettaglioStanziamentoPedaggio("FB912DS","IT"),
          buildDettaglioStanziamentoPedaggio("BH35BRU","RO")
        ))
      ));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    //IT,FB912DS
    //RO,BH35BRU
    then(allineamentoDaConsumoJmsProducer)
      .should()
      .sendMessage(
        new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder()
          .licensePlateNation("IT")
          .deviceType(TipoDispositivoEnum.TOLL_COLLECT)
          .licensePlateNumber("FB912DS")
          .build());
    then(allineamentoDaConsumoJmsProducer)
      .should()
      .sendMessage(
      new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder()
        .licensePlateNation("RO")
        .deviceType(TipoDispositivoEnum.TOLL_COLLECT)
        .licensePlateNumber("BH35BRU")
        .build());

    then(recordPersistenceService)
      .should(times(2))
      .persist(any());

  }

  @Test
  public void process_if_all_ok_will_map_all_rows_to_consumo() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TollCollect.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(
        buildStanziamento("1", Arrays.asList(
          buildDettaglioStanziamentoPedaggio("FB912DS","IT"),
          buildDettaglioStanziamentoPedaggio("BH35BRU","RO")
        ))
      ));

    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);


    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(consumoMapper)
      .should(times(2))
      .mapRecordToConsumo(any());
  }

  @Test
  public void process_if_checks_success_will_send_stanziamenti_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);


    given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TollCollect.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult);

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(0))
      .notify(any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void process_if_processor_pratially_process_rows_but_throw_an_exception_will_send_notification_and_stanziamento_to_nav() throws Exception {
    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);

    given(stanziamentiParamsValidator.checkConsistency())
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet(Arrays.asList(new Message("1"), new Message("2"))));

     given(consumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(TollCollect.class));

    final ProcessingResult processingResult = mock(ProcessingResult.class);
    given(processor.validateAndProcess(any(), any()))
      .willReturn(processingResult)
      .willThrow(new RuntimeException("This is an exception to test behaviour"));

    given(processingResult.getStanziamenti())
      .willReturn(Arrays.asList(stanziamento));

    job.process(filePath, -1l, Instant.EPOCH, servicePartner);

    then(notificationService)
      .should(times(1))
      .notify(any());

    then(stanziamentiToNavJmsProducer)
      .should(times(1))
      .publish(any());
  }

  @Test
  public void getJobQualifier() {
    assertThat(job.getJobQualifier())
      .isEqualTo(Format.TOLL_COLLECT);
  }

  private Stanziamento buildStanziamento(String cod,
                                         Collection<? extends DettaglioStanziamento> dettagliStanziamento){
    Stanziamento stanziamento = new Stanziamento(cod);
    stanziamento.getDettaglioStanziamenti().addAll(dettagliStanziamento);
    stanziamento.setInvoiceType(InvoiceType.D);
    return stanziamento;
  }

  private DettaglioStanziamento buildDettaglioStanziamentoPedaggio(String licenceId, String countryId){
    DettaglioStanziamentoPedaggio dettaglioStanziamentoPedaggio = new DettaglioStanziamentoPedaggio(
      new TollCollectGlobalIdentifier(UUID.randomUUID().toString()));
    Vehicle vehicle = new Vehicle();
    VehicleLicensePlate licencePlate = new VehicleLicensePlate(licenceId,countryId);
    vehicle.setLicensePlate(licencePlate);
    dettaglioStanziamentoPedaggio.setVehicle(vehicle);

    return dettaglioStanziamentoPedaggio;
  }
}
