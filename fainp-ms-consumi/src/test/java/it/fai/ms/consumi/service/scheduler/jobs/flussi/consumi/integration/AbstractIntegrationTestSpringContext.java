package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import it.fai.ms.consumi.client.VehicleClient;
import it.fai.ms.consumi.client.efservice.EfserviceRestClient;
import it.fai.ms.consumi.web.rest.RecordConsumoReprocessorResource;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.ribbon.FeignRibbonClientAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.common.notification.config.NotificationsConfiguration;
import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.consumi.config.AdditionalTestContextBeans;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.MessageSourceConfiguration;
import it.fai.ms.consumi.config.RestClientConfiguration;
import it.fai.ms.consumi.job.JobRegistry;
import it.fai.ms.consumi.service.jms.producer.CambioStatoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisherTestEmulator;
import it.fai.ms.consumi.service.record.RecordConsumoReprocessorImpl;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.LiquibaseIntegrationTestConfiguration;

@ExtendWith(SpringExtension.class)
@Import({
  ApplicationProperties.class,
  LiquibaseIntegrationTestConfiguration.class,
  JmsConfiguration.class,
  NotificationsConfiguration.class,
  HttpMessageConvertersAutoConfiguration.class,
  ValidationAutoConfiguration.class,
  AdditionalTestContextBeans.class,

})
@ImportAutoConfiguration({
  RibbonAutoConfiguration.class,
  FeignRibbonClientAutoConfiguration.class,
  FeignAutoConfiguration.class,
  RestClientConfiguration.class,
  MessageSourceConfiguration.class
})
@ComponentScan(
  basePackages = {
    "it.fai.ms.consumi.service",
    "it.fai.ms.consumi.client",
    "it.fai.ms.consumi.adapter",
    "it.fai.ms.consumi.repository",
    "it.fai.ms.consumi.domain.dettaglio_stanziamento" //stanziamento test utils
  },
  excludeFilters = {
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.service\\.(consumer|fatturazione|jms\\.listener|jms\\.producer|report|petrolpump\\.consumer)\\..*") ,
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.web\\.rest\\..*"),
    @ComponentScan.Filter(type = FilterType.REGEX, pattern = "it\\.fai\\.ms\\.consumi\\.client\\.efservice\\..*")
  }
)
// need to redefine repository and repository after NotificationConfiguration inclusion
@EntityScan(basePackages = { "it.fai.ms.consumi", "it.fai.common.notification.domain" })
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.ms.consumi.repository.contract", "it.fai.common.notification.repository" })

@DataJpaTest(showSql = false)
@ContextConfiguration(classes = { JmsConfiguration.class, NotificationsConfiguration.class})
@SpringBootTest(properties = { "feign.hystrix.enabled=false", "ribbon.eureka.enabled=false" })
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles(profiles = "integration")
public abstract class AbstractIntegrationTestSpringContext {

  @SpyBean
  protected StanziamentiToNavPublisherTestEmulator stanziamentiToNavJmsPublisher;

  @MockBean
  protected JobRegistry jobRegistry;

  @MockBean
  protected RecordConsumoReprocessorImpl recordConsumoReprocessor;

  @MockBean
  protected CambioStatoDaConsumoJmsProducer changeStatoPublisher;

  @MockBean
  protected RecordConsumoReprocessorResource recordConsumoReprocessorResource;

  @MockBean
  protected EfserviceRestClient efserviceRestClient;

  @MockBean
  protected VehicleClient vehicleClient;

  @AfterEach
  public void afterEach(){
    Mockito.validateMockitoUsage();
  }


}
