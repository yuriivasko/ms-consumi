package it.fai.ms.consumi.repository.dettaglio_stanziamento;

import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.money.Monetary;
import javax.persistence.EntityManager;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.AmountTestBuilder;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.TransactionTestBuilder;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoPedaggioTestBuilder;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.web.rest.util.TestUtils;

@ComponentScan(basePackages = {"it.fai.ms.consumi.util"})
@DataJpaTest
@EntityScan(basePackages = {"it.fai.ms.consumi", "it.fai.common.notification.domain"})
@EnableJpaRepositories({"it.fai.ms.consumi.repository", "it.fai.common.notification.repository"})
@Tag("integration")
class DettaglioStanziamentoRepositoryImplTest extends AbstractIntegrationTestSpringContext {

  private String globalIdentifierCarburante = "identifier#INTERNAL";

  @Autowired
  private DettaglioStanziamentoTestRepository testRepository;

  private static DettaglioStanziamentoPedaggio newDettaglioStanziamentoPedaggio() {
    return DettaglioStanziamentoPedaggioTestBuilder.newInstance()
      .withGlobalIdentifierAndUniqueKey(newInternalGlobalIdentifier())
      .withInvoiceTypeP()
      .withAmount(AmountTestBuilder.newInstance()
        .withAmountExcludedVat(Monetary.getDefaultAmountFactory()
          .setCurrency("EUR")
          .setNumber(new BigDecimal("666.666"))
          .create())
        .withAmountIncludedVat(Monetary.getDefaultAmountFactory()
          .setCurrency("EUR")
          .setNumber(new BigDecimal("999.999"))
          .create())
        .withExchangeRate(new BigDecimal("6.66"))
        .withVatRate(new BigDecimal("22.0"))
        .build())
      .withSource(new Source(newFileInDate(), newFileInDate(), "::sourceType::"))
      .withContract(Contract.newUnsafeContract("::contract_id::", "::customer::", null))
      .withTollPointExit(new TollPoint(new GlobalGate("::globalGateExit"),
        newTollPointExitTime()))
      .withTransaction(TransactionTestBuilder.newInstance()
        .withDetailCode("::transactionDetailCode::")
        .build())
      .withSupplier(new Supplier("::codeSupplier::"))
      .withDevice(new Device("::newobu::", TipoDispositivoEnum.TELEPASS_EUROPEO))
      .build();
  }

  private static Instant newFileInDate() {
    return Instant.ofEpochMilli(666);
  }

  private static Instant newTollPointExitTime() {
    return Instant.ofEpochMilli(999);
  }

  private static GlobalIdentifier newInternalGlobalIdentifier() {
    return GlobalIdentifierTestBuilder.newInstance()
      .internal()
      .build();
  }

  private static UUID newUuid() {
    return UUID.fromString("c3aba9bf-b831-48dd-8bc5-d651f93d2b44");
  }

  @BeforeEach
  void setUp() throws Exception {
    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
      new DettaglioStanziamentoGenericoEntityMapper(),
      new DettaglioStanziamentoPedaggioEntityMapper(),
      new DettaglioStanziamentoTrenoEntityMapper());
    repository = new DettaglioStanziamentiRepositoryImpl(em, dettaglioStanziamentoEntityMapper,
      new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper));
  }

  @Autowired
  private EntityManager em;

  private DettaglioStanziamantiRepository repository;

  private DettaglioStanziamentoCarburanteEntity newDettaglioStanziamentoCarburante(String supplierCode) {
    DettaglioStanziamentoCarburanteEntity entity = new DettaglioStanziamentoCarburanteEntity(globalIdentifierCarburante);
    entity.setStorno(false);
    entity.setQuantita(new BigDecimal(1));
    entity.setKm(new BigDecimal(30));
    entity.setCustomerId("::codClientNAV::");
    entity.setTipoConsumo(InvoiceType.P);
    entity.setCodiceFornitoreNav(supplierCode);
    entity.setDataOraUtilizzo(Instant.now());
    entity.setIngestionTime(Instant.now());
    return entity;
  }

  @Test
  @Transactional
  void findDettagliStanziamentiBySorgenteBeforeDate() {
    LocalDate date = LocalDate.parse("2017-12-12");
    Instant acquisitionDate = date.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant();
    int x = 0;
    // create 10 dettaglio stanziamento elvia with data 2017-12-31
    List<DettaglioStanziamentoPedaggioEntity> expecedResults = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      StanziamentoEntity s = new StanziamentoEntity("::stanziamento::" + x);
      s.setTarga("::veicolo::");
      s.setTipoFlusso(TypeFlow.NONE);
      s.setGeneraStanziamento(GeneraStanziamento.COSTO);
      s.setStatoStanziamento(InvoiceType.P);
      s.setNumeroArticolo("AUT_FR");
      s.setNumeroFattura("numeroFattura");
      s.setDataErogazioneServizio(LocalDate.now());
      s.setConguaglio(false);
      em.persist(s);
      DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggio = TestUtils.newDettaglioStanziamentoPedaggioEntity(InvoiceType.P,
        TipoDispositivoEnum.TELEPASS_EUROPEO,
        "elvia",
        acquisitionDate,
        "AUT_FR");
      dettaglioStanziamentoPedaggio.getStanziamenti()
        .add(s);
      em.persist(dettaglioStanziamentoPedaggio);
      x++;
      expecedResults.add(dettaglioStanziamentoPedaggio);
    }
    date = LocalDate.parse("2018-01-02");
    acquisitionDate = date.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant();

    // create 10 dettaglio stanziamento elvia with data 2018-01-02
    for (int i = 0; i < 10; i++) {
      TestUtils.persistDettaglioStanziamentoPedaggioWithStanziamento(acquisitionDate, "elvia", "::stanziamento::" + x, "::veicolo::",
        TypeFlow.NONE, GeneraStanziamento.COSTO, InvoiceType.P, "AUT_FR",
        "numeroFattura", LocalDate.now(), false, InvoiceType.P,
        TipoDispositivoEnum.TELEPASS_EUROPEO, em);
      x++;
    }

    testRepository.findAllDettaglioStanziamentiPedaggio()
      .forEach(ds -> {
        System.out.println(ds.getDataAcquisizioneFlusso() + " " + ds.getInvoiceType() + " " + ds.getSourceType() + " "
          + ds.getArticlesGroup());
      });

    LocalDate targetDate = LocalDate.parse("2018-01-01");
    List<DettaglioStanziamentoPedaggioEntity> results = repository.findDettagliStanziamentiProvvisoriBySorgenteAndCodRaggruppamentiBeforeDate("elvia",
      Map.of("AUT_FR",
        5,
        "AUT_ES",
        40),
      targetDate.atStartOfDay(ZoneId.of("Europe/Rome"))
        .toInstant());
    assertThat(results).isEqualTo(expecedResults);
    assertThat(results.stream()
      .flatMap(dettaglioStanziamentoPedaggioEntity -> dettaglioStanziamentoPedaggioEntity.getStanziamenti()
        .stream())
      .collect(Collectors.toList())).isNotEmpty();
  }

  @Test
  @Transactional
  void findLastDettagliStanziamentoDate() {

    LocalDate date = LocalDate.parse("2017-12-31");
    Instant acquisitionDate = date.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant();
    int x = 0;
    // create 10 dettaglio stanziamento elvia with data 2017-12-31
    for (int i = 0; i < 10; i++) {
      TestUtils.persistDettaglioStanziamentoPedaggioWithStanziamento(acquisitionDate, "elvia", "::stanziamento::" + x, "::veicolo::",
        TypeFlow.NONE, GeneraStanziamento.COSTO, InvoiceType.P, "::articles::",
        "numeroFattura", LocalDate.now(), false, InvoiceType.D,
        TipoDispositivoEnum.TELEPASS_EUROPEO, em);
      x++;
    }
    date = LocalDate.parse("2018-01-02");
    acquisitionDate = date.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant();
    // create 10 dettaglio stanziamento vacon with data 2018-01-02
    for (int i = 0; i < 20; i++) {
      TestUtils.persistDettaglioStanziamentoPedaggioWithStanziamento(acquisitionDate, "vacon", "::stanziamento::" + x, "::veicolo::",
        TypeFlow.NONE, GeneraStanziamento.COSTO, InvoiceType.P, "::articles::",
        "numeroFattura", LocalDate.now(), false, InvoiceType.D,
        TipoDispositivoEnum.TELEPASS_EUROPEO, em);
      x++;
    }
    date = LocalDate.parse("2018-01-02");
    acquisitionDate = date.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant();
    // create 5 dettaglio stanziamento elvia with data 2018-01-02
    for (int i = 0; i < 5; i++) {
      TestUtils.persistDettaglioStanziamentoPedaggioWithStanziamento(acquisitionDate, "elvia", "::stanziamento::" + x, "::veicolo::",
        TypeFlow.NONE, GeneraStanziamento.COSTO, InvoiceType.P, "::articles::",
        "numeroFattura", LocalDate.now(), false, InvoiceType.D,
        TipoDispositivoEnum.TELEPASS_EUROPEO, em);
      x++;
    }
    date = LocalDate.parse("2018-03-02");
    acquisitionDate = date.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant();
    // create 5 dettaglio stanziamento elvia with data 2018-03-02
    for (int i = 0; i < 5; i++) {
      TestUtils.persistDettaglioStanziamentoPedaggioWithStanziamento(acquisitionDate, "vacon", "::stanziamento::" + x, "::veicolo::",
        TypeFlow.NONE, GeneraStanziamento.COSTO, InvoiceType.P, "::articles::",
        "numeroFattura", LocalDate.now(), false, InvoiceType.D,
        TipoDispositivoEnum.TELEPASS_EUROPEO, em);
      x++;
    }
    Instant actual = repository.findLastDettagliStanziamentoDate("elvia");
    LocalDate expectedElvia = LocalDate.parse("2018-01-02");
    assertThat(actual).isEqualTo(expectedElvia.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant());
    actual = repository.findLastDettagliStanziamentoDate("vacon");
    LocalDate expectedVacon = LocalDate.parse("2018-03-02");
    assertThat(actual).isEqualTo(expectedVacon.atStartOfDay(ZoneId.of("Europe/Rome"))
      .toInstant());

  }

  @Test
  @Transactional
  void findDettaglioStazionamentoCarburante() {

    String codeArticle = "::articles::";
    String supplierCode = "::suppler_code::";

    InvoiceType invoiceType = InvoiceType.P;
    GeneraStanziamento generaStanziamento = GeneraStanziamento.COSTO;

    StanziamentoEntity s = new StanziamentoEntity("::stanziamento::");
    s.setTarga("::veicolo::");
    s.setTipoFlusso(TypeFlow.NONE);
    s.setGeneraStanziamento(generaStanziamento);
    s.setStatoStanziamento(invoiceType);
    s.setNumeroArticolo(codeArticle);
    s.setNumeroFattura("numeroFattura");
    s.setDataErogazioneServizio(LocalDate.now());
    s.setConguaglio(false);
    em.persist(s);

    DettaglioStanziamentoCarburanteEntity dsp = newDettaglioStanziamentoCarburante(supplierCode);
    dsp.setTipoConsumo(invoiceType);
    Set<StanziamentoEntity> stanziamenti = new HashSet<>();
    stanziamenti.add(s);
    dsp.getStanziamenti()
      .addAll(stanziamenti);
    em.persist(dsp);

    s.getDettaglioStanziamenti()
      .add(dsp);
    em.persist(s);
    em.flush();

    PriceTableFilterDTO filterDto = new PriceTableFilterDTO();
    filterDto.setArticleCode(codeArticle);
    filterDto.setSupplierCode(supplierCode);
    filterDto.setInvoiceType(invoiceType);
    filterDto.setCostRevenueFlag(generaStanziamento == GeneraStanziamento.COSTO ? CostRevenue.COST : CostRevenue.REVENUE);
    filterDto.setDateFrom(Instant.now()
      .minus(2, ChronoUnit.DAYS));
    filterDto.setDateTo(null);

    List<DettaglioStanziamentoCarburanteEntity> list = repository.findToUpdatePriceTable(filterDto);

    assertThat(list).isNotNull()
      .isNotEmpty();
  }

  @Test
  void persistTest() {
    var newDettaglioStanziamentoPedaggio = newDettaglioStanziamentoPedaggio();
    newDettaglioStanziamentoPedaggio.setCustomer(new Customer("::customer::"));
    repository.save(newDettaglioStanziamentoPedaggio);

    em.flush();
  }

  @Test
  void when_arent_dettagliStanziamenti_findByGlobalIdentifier_returns_emptySet() {

    // given

    // when

    var dettagliStanziamenti = repository.findByCodiceStanziamento("::globalIdentifier::");

    // then

    assertThat(dettagliStanziamenti).isEmpty();

  }

  @Test
  void when_arent_dettagliStanziamenti_findByIdStanziamento_returns_emptySet() {

    // given

    // when

    var dettagliStanziamenti = repository.findByCodiceStanziamento("::idStanziamento_notPresent::");

    // then

    assertThat(dettagliStanziamenti).isEmpty();
  }

  @Test
  @Transactional
  void when_find_stanziamento_by_codici_return_dettaglio_stanziamento() {

    var stanziamentoEntity = new StanziamentoEntity("::stanziamento::");
    stanziamentoEntity.setTarga("::veicolo::");
    stanziamentoEntity.setTipoFlusso(TypeFlow.ACCETTAZIONE_DOMANDA_SCONTI_ITALIA);
    stanziamentoEntity.setGeneraStanziamento(GeneraStanziamento.COSTO_RICAVO);
    stanziamentoEntity.setStatoStanziamento(InvoiceType.D);
    stanziamentoEntity.setNumeroFattura("numeroFattura");

    em.persist(stanziamentoEntity);

    var dettaglioStanziamentoPedaggio = newDettaglioStanziamentoPedaggio();
    dettaglioStanziamentoPedaggio.setCustomer(new Customer("::customer::"));

    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
      new DettaglioStanziamentoGenericoEntityMapper(),
      new DettaglioStanziamentoPedaggioEntityMapper(),
      new DettaglioStanziamentoTrenoEntityMapper());

    DettaglioStanziamentoEntity entity = dettaglioStanziamentoEntityMapper.toEntity(dettaglioStanziamentoPedaggio);

    var stanziamenti = Set.of(stanziamentoEntity);

    entity.getStanziamenti()
      .addAll(stanziamenti);

    em.persist(entity);

    var dettagliStanziamento = repository.findByCodiciStanziamentoAndTipoDettaglio(Set.of("::stanziamento::"),
      DettaglioStanziamentoType.PEDAGGIO);

    assertThat(dettagliStanziamento).isNotEmpty();
    dettagliStanziamento.stream()
      .map(ds -> assertThat(ds instanceof DettaglioStanziamentoPedaggio).isTrue())
      .collect(toSet());
  }

  @Test
  void when_thereAre_dettagliStanziamenti_findGlobalIdentifier_returns_setWithOneItem() {

    // given

    persistTest();

    // when

    var dettagliStanziamenti = repository.findByGlobalIdentifierId(GlobalIdentifierTestBuilder.newInstance()
      .internal()
      .build());

    // then

    var amount = new Amount();
    amount.setAmountExcludedVat(Monetary.getDefaultAmountFactory()
      .setCurrency("EUR")
      .setNumber(new BigDecimal("666.666"))
      .create());
    amount.setAmountIncludedVat(Monetary.getDefaultAmountFactory()
      .setCurrency("EUR")
      .setNumber(new BigDecimal("999.999"))
      .create());
    amount.setExchangeRate(new BigDecimal("6.66"));
    amount.setVatRate(new BigDecimal("22.0"));

    var transaction = TransactionTestBuilder.newInstance()
      .withDetailCode("::transactionDetailCode::")
      .build();

    var device = new Device("::newobu::", TipoDispositivoEnum.TELEPASS_EUROPEO);

    assertThat(dettagliStanziamenti).contains(DettaglioStanziamentoPedaggioTestBuilder.newInstance()
      .withGlobalIdentifierAndUniqueKey(newInternalGlobalIdentifier())
      .withInvoiceTypeP()
      .withAmount(amount)
      .withDevice(device)
      .withCustomer(new Customer("::customer::"))
      .withDate(newTollPointExitTime())
      .withSource(new Source(newFileInDate(),
        newFileInDate(),
        "::sourceType::"))
      .withContract(Contract.newUnsafeContract("::contract_id::",
        "::customer::",
        null))
      .withSupplier(new Supplier("::codeSupplier::"))
      .withTransaction(transaction)
      .withVehicle(new Vehicle())
      .build());
  }

  private UUID uuidCarburante;

  private StanziamentiDetailsType detailTypeCarburanti = StanziamentiDetailsType.CARBURANTI;

  @Test
  void test_query_find_stanziamento_detail_by_global_identifier_andNot_is_id() {
    persistCarburantePrezzoTest();

    Optional<DettaglioStanziamentoEntity> dettagliStanziamenti = repository.findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(globalIdentifierCarburante,
      detailTypeCarburanti,
      uuidCarburante,
      CostRevenue.COST);

    assertThat(dettagliStanziamenti.isPresent()).isFalse();

    persistCarburanteCostoTest();

    dettagliStanziamenti = repository.findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(globalIdentifierCarburante,
      detailTypeCarburanti, uuidCarburante,
      CostRevenue.REVENUE);

    assertThat(dettagliStanziamenti.isPresent()).isTrue();
  }

  private void persistCarburantePrezzoTest() {
    var carburante = newDettaglioStanziamentoCarburante("::supplier_code::");
    carburante.setPrezzoUnitarioCalcolatoNoiva(new BigDecimal(11.22));
    DettaglioStanziamentoEntity ds = repository.update(carburante);

    uuidCarburante = ds.getId();

    em.flush();
  }

  private void persistCarburanteCostoTest() {
    var carburante = newDettaglioStanziamentoCarburante("::supplier_code::");
    carburante.setCostoUnitarioCalcolatoNoiva(new BigDecimal(22.11));
    DettaglioStanziamentoEntity ds = repository.update(carburante);

    uuidCarburante = ds.getId();
    em.flush();
  }

  @Transactional
  @Test
  public void test_findByGlobalIdentifierId() {

    em.createNativeQuery("INSERT INTO dettaglio_stanziamento\n" +
      "(tipo_dettaglio_stanziamento, id, global_identifier)\n" +
      "VALUES('PEDAGGI', '3cea2720-f3a7-4c6d-8ca9-662251e7ab59', '110336690§00665098661§AZ§2019-06-24§19.43.50§P9§37§0428§-§2200#EXTERNAL')"
    ).executeUpdate();

    em.createNativeQuery("" +
      "INSERT INTO dettaglio_stanziamento_pedaggi " +
      "(contract_code, currency_code, codice_cliente_nav, data_acquisizione_flusso, exchange_rate, file_name, ingestion_time, record_code, sign_of_transaction, imponibile_adr, amount_novat, amount_includingvat, perc_iva, raggruppamento_articoli_nav, obu, pan_number, device_type, entry_global_gate_identifier, entry_global_gate_identifier_description, entry_timestamp, exit_global_gate_identifier, exit_global_gate_identifier_description, exit_timestamp, tipo_consumo, network_code, region, row_number, tipo_sorgente, codice_fornitore_nav, codice_fornitore_legacy, documento_da_fornitore, transaction_detail_code, tratta, veicolo_classe_euro, veicolo_classe_tariffaria, veicolo_nazione_targa, veicolo_targa, id) " +
      "VALUES('110336690', 'EUR', '0076007', '2019-06-30 17:32:05.000', NULL, 'DA06A284.ELCIT.U2768486.N01099', '2019-07-02 05:36:45.805', '20', '+', NULL, 9.34426, 11.40000, 22.00, 'AUT_IT', '00049000000665098661', '00665098661', 'TELEPASS_EUROPEO', '0998', 'DIREZ. USCIT', NULL, '0428', 'SV MORGEX', '2019-06-24 19:43:50.000', 'P', '37', NULL, 1789779, 'elcit', 'F000304', NULL, NULL, 'P9', 'DIREZ. USCIT SV MORGEX', 'EURO6', '5', 'IT', 'FG306EE', '3cea2720-f3a7-4c6d-8ca9-662251e7ab59')"
    ).executeUpdate();

    em.createNativeQuery("INSERT INTO stanziamento\n" +
      "(codice_stanziamento, nr_articolo, codice_cliente_fatturazione, codicestanziamento_provvisorio, conguaglio, costo, valuta, data_acquisizione_flusso, data_erogazione_servizio, data_fattura, data_registrazione, data_invio_nav, data_accodato_nav, desc_1, desc_2, desc_3, genera_stanziamento, stato_stanziamento, nazione_targa, nr_cliente, numero_fattura, nr_fornitore, paese, prezzo, provvigioni_aquisite, quantita, storno, documento_da_fornitore, targa, tipo_flusso, nr_, classe_veicolo_euro, anno_stanziamento)\n" +
      "VALUES('37B86ECA-3022-46C5-A902-3C9348F563DCP', 'ITAPE01', NULL, NULL, 0, 79.01639, 'EUR', '2019-06-30 17:32:05.000', '2019-06-24', NULL, NULL, '2019-07-03 03:57:36.190', '2019-07-03 03:48:01.694', '', '', '', 1, 'P', 'IT', '0076007', NULL, 'F000304', 'IT', 79.01639, 0, 1.00, NULL, NULL, 'FG306EE', 1, 3, '5', 2019);\n"
    ).executeUpdate();

    em.createNativeQuery("INSERT INTO dettagliostanziamento_stanziamento\n" +
      "(dettaglio_stanziamenti_id, stanziamenti_codice_stanziamento)\n" +
      "VALUES('3cea2720-f3a7-4c6d-8ca9-662251e7ab59', '37B86ECA-3022-46C5-A902-3C9348F563DCP')"
    ).executeUpdate();

    Set<DettaglioStanziamento> dettaglioStanziamentos = repository.findByGlobalIdentifierId(new ElviaGlobalIdentifier("110336690§00665098661§AZ§2019-06-24§19.43.50§P9§37§0428§-§2200"));
    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
      new DettaglioStanziamentoGenericoEntityMapper(),
      new DettaglioStanziamentoPedaggioEntityMapper(),
      new DettaglioStanziamentoTrenoEntityMapper());
    List<DettaglioStanziamentoEntity> entities = dettaglioStanziamentos.stream().map(d -> dettaglioStanziamentoEntityMapper.toEntity(d)).collect(Collectors.toList());
    entities.forEach(DettaglioStanziamentoEntity::toString);
  }
}
