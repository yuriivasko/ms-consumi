package it.fai.ms.consumi.job;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Paths;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit")
class FileNameResolverTest {

  @Test
  void testResolveFileName() {

    var fileName = new FileNameResolver().resolveFileName(Paths.get("/tmp"));

    assertThat(fileName).isEqualTo("/tmp");
  }

}
