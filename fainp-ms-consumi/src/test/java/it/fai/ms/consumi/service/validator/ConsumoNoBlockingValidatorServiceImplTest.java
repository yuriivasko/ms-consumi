package it.fai.ms.consumi.service.validator;

import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcome;
import static it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory.newValidationOutcomeFailure;
import static java.time.Instant.ofEpochSecond;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.domain.consumi.generici.hgv.HgvGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElceuGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollectGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.service.record.telepass.TelepassSourceName;
import it.fai.ms.consumi.service.validator.internal.ConsumoValidator;
import it.fai.ms.consumi.service.validator.internal.ContractValidator;
import it.fai.ms.consumi.service.validator.internal.CustomerValidator;
import it.fai.ms.consumi.service.validator.internal.DeviceValidator;

@Tag("unit")
class ConsumoNoBlockingValidatorServiceImplTest {

  private final CustomerValidator customerValidator = mock(CustomerValidator.class);
  private final DeviceValidator   deviceValidator = mock(DeviceValidator.class);
  private ContractValidator contractValidator = mock(ContractValidator.class);
  private final NotificationService _notificationService = mock(NotificationService.class);
  final ConsumoValidator consumoValidator = mock(ConsumoValidator.class);
  
  
  ConsumoNoBlockingData consumo = new ConsumoNoBlockingData();
  String fileName = "";
  String ingestion_time = "";
  String servicePartner = "";


  ConsumoNoBlockingValidatorService consumoWarningValidatorService = new ConsumoNoBlockingValidatorService(
    customerValidator,
    deviceValidator,
    contractValidator,
    consumoValidator,
    _notificationService
  );

  @BeforeEach
  void setUp() {
    consumo.setCompanyCode("::customerUuid::");
    consumo.setConsumoDate(Instant.now());
    consumo.setDevice(new Device( "::deviceUuid::", TipoDispositivoEnum.TELEPASS_EUROPEO));
    consumo.setContractCode("::contractUuid::");
    consumo.setHasDevice(true);
  }

  @Test
  void when_customer_wasntActive_inDate_returns_warning_error() {
    
    // given

    given(customerValidator.customerActiveInDate("::customerUuid::", consumo.getConsumoDate()))
      .willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    given(deviceValidator.deviceWasActiveAndAssociatedWithContractInDate(
      "::deviceUuid::",
      TipoDispositivoEnum.TELEPASS_EUROPEO,
      "::contractUuid::", consumo.getConsumoDate(),
      Duration.ofHours(24))
    ).willReturn(newValidationOutcome());

    // when
    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    // then
    then(customerValidator)
      .should()
      .customerActiveInDate("::customerUuid::", consumo.getConsumoDate());
    then(customerValidator).shouldHaveNoMoreInteractions();

    then(deviceValidator)
      .should()
      .deviceWasActiveAndAssociatedWithContractInDate(
        "::deviceUuid::",
        TipoDispositivoEnum.TELEPASS_EUROPEO,
        "::contractUuid::",
        consumo.getConsumoDate(),
        Duration.ofHours(24));

    assertThat(validationOutcome.isValidationOk())
      .isFalse();

    verifyAllValidatorAreCalled();
  }

  @Test
  void validate_if_consumo_is_TrustedStatoDispositivo_it_should_call_deviceWasAssociatedWithContractInDate() {
    
    consumo.setTrustedStatoDispositivo(true);

    given(customerValidator.customerActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(contractValidator.contractActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    verifyAllValidatorAreCalled_for_TrustedStatoDispositivo();

  }


  @Test
  void validate_if_consumo_is_NOT_TrustedStatoDispositivo_it_should_call_deviceWasActiveAndAssociatedWithContractInDate() {

    given(customerValidator.customerActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(contractValidator.contractActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    verifyAllValidatorAreCalled();

  }

  @Test
  void validate_if_customerWas_NOT_ActiveInDate_it_should_addValidationOutcomeMessages_validation_fail() {

    given(customerValidator.customerActiveInDate(any(), any()))
      .willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    assertThat(validationOutcome.isValidationOk())
      .isFalse();
  }

  @Test
  void validate_if_contract_NOT_ActiveInDate_it_should_addValidationOutcomeMessages_validation_fail() {

    given(customerValidator.customerActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(contractValidator.contractActiveInDate(any(), any()))
      .willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    assertThat(validationOutcome.isValidationOk())
      .isFalse();

    verifyAllValidatorAreCalled();

  }

  @Test
  void validate_if_deviceWas_NOT_ActiveAndAssociatedWithContractInDate_it_should_addValidationOutcomeMessages_validation_fail() {

    given(customerValidator.customerActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(contractValidator.contractActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(deviceValidator.deviceWasActiveAndAssociatedWithContractInDate(any(), any() ,any() ,any() ,any()))
      .willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    assertThat(validationOutcome.isValidationOk())
      .isFalse();

    verifyAllValidatorAreCalled();

  }

  @Test
  void validate_if_serviceWas_NOT_ActiveInDate_it_should_addValidationOutcomeMessages_validation_fail() {

    given(customerValidator.customerActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(contractValidator.contractActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(deviceValidator.deviceWasActiveAndAssociatedWithContractInDate(any(), any() ,any() ,any() ,any()))
      .willReturn(newValidationOutcome());

    given(deviceValidator.serviceWasActiveInDate(any(), any(), any()))
      .willReturn(newValidationOutcomeFailure("::failure::", "::text::"));

    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    assertThat(validationOutcome.isValidationOk())
      .isFalse();

    verifyAllValidatorAreCalled();

  }

  @Test
  void validate_if_all_validation_od_it_should_addValidationOutcomeMessages_validation_success() {

    given(customerValidator.customerActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(contractValidator.contractActiveInDate(any(), any()))
      .willReturn(newValidationOutcome());

    given(deviceValidator.deviceWasActiveAndAssociatedWithContractInDate(any(), any() ,any() ,any() ,any()))
      .willReturn(newValidationOutcome());

    given(deviceValidator.serviceWasActiveInDate(any(), any(), any()))
      .willReturn(newValidationOutcome());

    var validationOutcome = consumoWarningValidatorService.validateAndNotify(consumo, fileName, ingestion_time, servicePartner);

    assertThat(validationOutcome.isValidationOk())
      .isTrue();

    verifyAllValidatorAreCalled();

  }

  private void verifyAllValidatorAreCalled() {
    then(deviceValidator)
      .should()
      .deviceWasActiveAndAssociatedWithContractInDate(any(), any(), any(), any(),any());

    verifyCommonsValidatorAreCalled();

    then(customerValidator).shouldHaveNoMoreInteractions();
    then(contractValidator).shouldHaveNoMoreInteractions();
    then(deviceValidator).shouldHaveNoMoreInteractions();  }

  private void verifyAllValidatorAreCalled_for_TrustedStatoDispositivo() {
    verifyCommonsValidatorAreCalled();

    then(deviceValidator)
      .should(times(1))
      .deviceWasAssociatedWithContractInDate(any(), any() ,any() ,any() ,any());

    then(customerValidator).shouldHaveNoMoreInteractions();
    then(contractValidator).shouldHaveNoMoreInteractions();
    then(deviceValidator).shouldHaveNoMoreInteractions();

  }

  private void verifyCommonsValidatorAreCalled() {
    then(customerValidator)
      .should(times(1))
      .customerActiveInDate(any(), any());

    then(contractValidator)
      .should(times(1))
      .contractActiveInDate(any(), any());

    then(deviceValidator)
      .should(times(1))
      .serviceWasActiveInDate(any(), any(), any());
  }

  private Elceu newElceu() {
    Elceu consumo = new Elceu(new Source(Instant.now(), Instant.now(), "::recordCode::"), "::globalIdentifier::",
                              Optional.of(new ElceuGlobalIdentifier("::globalIdentifier::")), TestDummyRecord.instance1);
    consumo.setDevice(new Device("::deviceUuid::", TipoDispositivoEnum.TELEPASS_EUROPEO));
    consumo.setContract(Optional.of(new Contract("::contractUuid::")));
    consumo.getContract().get().setCompanyCode("::customerUuid::");
    TollPoint tp = new TollPoint();
    tp.setTime(ofEpochSecond(123));
    consumo.setTollPointExit(tp);

    return consumo;
  }

  private Hgv newHgv() {
    final Hgv consumo = new Hgv(mock(Source.class), "::recordCode::", Optional.of(new HgvGlobalIdentifier("::id::")), TestDummyRecord.instance1);
    consumo.setContract(Optional.of(Contract.newUnsafeContract("::contract_id::", "::company_code", "::state::")));
    consumo.setDevice(new Device(TipoDispositivoEnum.HGV));
    return consumo;
  }


  private TollCollect newTollCollect() {
    final TollCollect consumo = new TollCollect(mock(Source.class),  "::recordCode::", Optional.of(new TollCollectGlobalIdentifier("::id::")), TestDummyRecord.instance1);
    consumo.setContract(Optional.of(Contract.newUnsafeContract("::contract_id::", "::company_code", "::state::")));
    consumo.setDevice(new Device(TipoDispositivoEnum.HGV));
    return consumo;
  }


  private Elvia newElvia() {
    Source source = new Source(Instant.EPOCH, Instant.EPOCH, TelepassSourceName.ELVIA);
    final Optional<Contract> optionalContract = Optional.of(Contract.newUnsafeContract("::contract_id::", "::company_code", "::state::"));

    final Device device = new Device(TipoDispositivoEnum.TELEPASS_EUROPEO);
    device.setPan("::pan::");

    final Optional<GlobalIdentifier> optionalGlobalIdentifier = Optional.of(new ElviaGlobalIdentifier("::id::"));
    final Elvia consumo = new Elvia(source,  "::recordCode::", optionalGlobalIdentifier, TestDummyRecord.instance1);
    consumo.setContract(optionalContract);
    consumo.setDevice(device);
    return consumo;
  }
}
