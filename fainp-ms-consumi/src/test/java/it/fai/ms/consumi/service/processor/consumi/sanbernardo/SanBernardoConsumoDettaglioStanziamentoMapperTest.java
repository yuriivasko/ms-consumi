package it.fai.ms.consumi.service.processor.consumi.sanbernardo;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.TessereSanBernardoGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.web.rest.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import javax.money.MonetaryAmount;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
public class SanBernardoConsumoDettaglioStanziamentoMapperTest {

    private TessereSanBernardoConsumoDettaglioStanziamentoMapper mapper;

    @BeforeEach
    public void init(){
        mapper = new TessereSanBernardoConsumoDettaglioStanziamentoMapper();
    }

    @Test
    public void testMapConsumoToDettaglioStanziamento() throws ParseException {

        @NotNull Consumo consumo = createConsumo(
                TestUtils.fromStringToInstant("2018-01-01 20:00:00"),
                "elenco_viaggi_gsb",
                "YYYYMMDD_elenco_viaggi.csv",
                1l,
                "DEFAULT",
                UUID.randomUUID().toString(),
                "9756934101999003952",
                "02/01/2018 10:10:00"

        );
        Amount amount = new Amount();
        amount.setVatRate(new BigDecimal("22"));
        MonetaryAmount expectedAmountExcludedVat = MonetaryUtils.strToMonetary("10,00", 1, "EUR");
        amount.setAmountExcludedVat(expectedAmountExcludedVat);
        MonetaryAmount expectedAmountIncludedVat = MonetaryUtils.strToMonetary("12,20", 1, "EUR");
        amount.setAmountIncludedVat(expectedAmountIncludedVat);
        consumo.setAmount(amount);
        consumo.setNavSupplierCode(":codFornitoreNav:");
        DettaglioStanziamento dettaglioStanziamento = mapper.mapConsumoToDettaglioStanziamento(consumo);
        assertThat(dettaglioStanziamento).isNotNull();
        assertThat(dettaglioStanziamento).isInstanceOf(DettaglioStanziamentoPedaggio.class);
        assertThat(dettaglioStanziamento.getInvoiceType()).isEqualTo(InvoiceType.D);
        assertThat(dettaglioStanziamento.getAmount()).isNotNull().satisfies(a->{
            assertThat(a.getAmountExcludedVatBigDecimal()).isEqualByComparingTo("10.00");
            assertThat(a.getAmountIncludedVatBigDecimal()).isEqualByComparingTo("12.20");
        });

    }

    private SanBernardo createConsumo(Instant ingestionTime, String sourceType, String fileName, Long rowNumber, String recordCode,
                                      String uuid, String panNumber, String d) throws ParseException {
        Source source = new Source(ingestionTime, ingestionTime, sourceType);
        source.setFileName(fileName);
        source.setRowNumber(rowNumber);
        SanBernardo consumo = new SanBernardo(source,
                recordCode,
                Optional.of(new TessereSanBernardoGlobalIdentifier(uuid)), TestDummyRecord.instance1);
        Device device = new Device(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);
        device.setPan(panNumber);
        consumo.setDevice(device);
        //tipo consumo by default is D
        TollPoint tp = new TollPoint();
        Instant date = new SimpleDateFormat("dd/MM/yyyy HH:mm")
                .parse(d).toInstant();
        tp.setTime(date);
        consumo.setTollPointExit(tp);
        return consumo;
    }

}
