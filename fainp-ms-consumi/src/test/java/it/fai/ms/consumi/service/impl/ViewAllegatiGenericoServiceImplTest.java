package it.fai.ms.consumi.service.impl;

import static org.assertj.core.api.Assertions.tuple;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.config.NotificationsConfiguration;
import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.fattura.AbstractAllegatoFatturaMessage;
import it.fai.ms.common.jms.fattura.GenericoAllegatoFattureMessage;
import it.fai.ms.common.jms.fattura.GenericoDTO;
import it.fai.ms.common.jms.fattura.GenericoWithDevice;
import it.fai.ms.consumi.AbstractCommonTest;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.client.efservice.EfserviceRestClient;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;
import it.fai.ms.consumi.repository.device.TipoSupportoRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepositoryImpl;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2EntityBuilder;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2RepositoryImpl;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.LiquibaseIntegrationTestConfiguration;

@ExtendWith(SpringExtension.class)
@Import({ ApplicationProperties.class, LiquibaseIntegrationTestConfiguration.class, JmsConfiguration.class,
          NotificationsConfiguration.class, ValidationAutoConfiguration.class })
@DataJpaTest(showSql = false)
@EntityScan(basePackages = { "it.fai.ms.consumi", "it.fai.common.notification.domain" })
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.common.notification.repository" })
@EnableCaching
@Tag("integration")
public class ViewAllegatiGenericoServiceImplTest extends AbstractCommonTest {

  static String CONTRACT_CODE     = "::contractCode::" + RandomString() + "::";
  static String CONTRACT_CODE_1   = CONTRACT_CODE + "1";
  static String STANZIAMENTO_CODE = "::" + RandomString() + "::";

  private @Autowired EntityManager em;

  private ViewAllegatoGenericoSezione2RepositoryImpl attachmentRepository;
  private StanziamentiParamsRepositoryImpl           stanziamentiParamsRepository;
  private ViewAllegatiGenericoServiceImpl            service;
  private PetrolPumpService                          petroPumpServiceMock     = mock(PetrolPumpService.class);
  private TipoSupportoRepository                        tipoSupportoRepoMock  = mock(TipoSupportoRepository.class);
  private DettaglioStanziamentiRepositoryImpl        repository;

  private PointDTO point;

  private Double quantita = 10d;

  @BeforeEach
  void setUp() throws Exception {
    point = new PointDTO();
    point.setName("pointName");
    point.setCity("CityPoint");
    point.setCode("CodePoint");

    when(petroPumpServiceMock.getPoint(any())).thenReturn(point);

    attachmentRepository = Mockito.mock(ViewAllegatoGenericoSezione2RepositoryImpl.class);

    stanziamentiParamsRepository = new StanziamentiParamsRepositoryImpl(em, new StanziamentiParamsEntityMapper());

    service = new ViewAllegatiGenericoServiceImpl(attachmentRepository, stanziamentiParamsRepository, petroPumpServiceMock,
                                                  tipoSupportoRepoMock);

    var dettaglioStanziamentoEntityMapper = new DettaglioStanziamentoEntityMapper(new DettaglioStanziamentoCarburanteEntityMapper(),
                                                                                  new DettaglioStanziamentoGenericoEntityMapper(),
                                                                                  new DettaglioStanziamentoPedaggioEntityMapper(),
                                                                                  new DettaglioStanziamentoTrenoEntityMapper());
    repository = new DettaglioStanziamentiRepositoryImpl(em, dettaglioStanziamentoEntityMapper,
                                                         new StanziamentoEntityMapper(dettaglioStanziamentoEntityMapper));
  }

  @Test
  public void createBodyMessageNoDevice() throws Exception {
    List<ViewAllegatoGenericoSezione2Entity> codiciStanziamenti = new ArrayList<>();
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE_1, RandomNumeric(), null, 1d));
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), null, 2d));
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), null, 3d));
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE_1, RandomNumeric(), null, 4d));

    RequestsCreationAttachments request = mock(RequestsCreationAttachments.class);
    when(request.getNomeIntestazioneTessere()).thenReturn("TEST");
    when(request.getTipoServizio()).thenReturn("::servizio::");
    List<GenericoDTO> results = service.createBodyMessage(codiciStanziamenti, request, TemplateAllegati.NO_DISPOSITIVO);
    GenericoAllegatoFattureMessage generico = new GenericoAllegatoFattureMessage("Documento");
    generico.setBody(results);
    printToJsonFormat(generico);
    
    assertThat(results).hasSize(2);
    GenericoDTO result = results.get(0);
    assertThat(result.getHeaderContractSection()).isNotNull();
    assertThat(result.getHeaderContractSection()
                     .getCodiceContrattoCliente()).isEqualTo(CONTRACT_CODE_1);
    assertThat(result.getBodyContractSection()).hasSize(1)
                                               .flatExtracting(GenericoWithDevice::getConsumoDevice)
                                               .extracting(c -> tuple(c.getValuta(), c.getCodicePuntoErogazione(), c.getNazioneTarga(),
                                                                      c.getPrezzoUnitario()))
                                               .contains(tuple("EUR", "::puntoErogazione::", null, 1d),
                                                         tuple("EUR", "::puntoErogazione::", null, 4d));

    result = results.get(1);
    assertThat(result.getHeaderContractSection()).isNotNull();
    assertThat(result.getHeaderContractSection()
                     .getCodiceContrattoCliente()).isEqualTo(CONTRACT_CODE);
    assertThat(result.getBodyContractSection()).hasSize(1)
                                               .flatExtracting(GenericoWithDevice::getConsumoDevice)
                                               .extracting(c -> tuple(c.getValuta(), c.getCodicePuntoErogazione(), c.getNazioneTarga(), c.getPrezzoUnitario()))
                                               .contains(tuple("EUR", "::puntoErogazione::", null, 2d),
                                                         tuple("EUR", "::puntoErogazione::", null, 3d));
  }

  @Test
  public void createBodyMessageWithDevice() throws Exception {
    List<ViewAllegatoGenericoSezione2Entity> codiciStanziamenti = new ArrayList<>();
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE_1, RandomNumeric(), TipoDispositivoEnum.TRACKYCARD.name(), 1d));
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), TipoDispositivoEnum.TRACKYCARD.name(), 2d));
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE, RandomNumeric(), TipoDispositivoEnum.TRACKYCARD.name(), 3d));
    codiciStanziamenti.add(addViewEntity(CONTRACT_CODE_1, RandomNumeric(), TipoDispositivoEnum.TRACKYCARD.name(), 4d));

    RequestsCreationAttachments request = mock(RequestsCreationAttachments.class);
    when(request.getNomeIntestazioneTessere()).thenReturn("TEST");
    when(request.getTipoServizio()).thenReturn("::servizio::");
    List<GenericoDTO> results = service.createBodyMessage(codiciStanziamenti, request, TemplateAllegati.CON_DISPOSITIVO);
    GenericoAllegatoFattureMessage generico = new GenericoAllegatoFattureMessage("Documento");
    generico.setBody(results);
    printToJsonFormat(generico);

    assertThat(results).hasSize(2);
    GenericoDTO result = results.get(0);
    assertThat(result.getHeaderContractSection()).isNotNull();
    assertThat(result.getHeaderContractSection()
                     .getCodiceContrattoCliente()).isEqualTo(CONTRACT_CODE_1);
    assertThat(result.getBodyContractSection()).hasSize(2)
                                               .flatExtracting(GenericoWithDevice::getConsumoDevice)
                                               .extracting(c -> tuple(c.getValuta(), c.getCodicePuntoErogazione(), c.getNazioneTarga(),
                                                                      c.getPrezzoUnitario()))
                                               .contains(tuple("EUR", "::puntoErogazione::", null, 1d),
                                                         tuple("EUR", "::puntoErogazione::", null, 4d));

    result = results.get(1);
    assertThat(result.getHeaderContractSection()).isNotNull();
    assertThat(result.getHeaderContractSection()
                     .getCodiceContrattoCliente()).isEqualTo(CONTRACT_CODE);
    assertThat(result.getBodyContractSection()).hasSize(2)
                                               .flatExtracting(GenericoWithDevice::getConsumoDevice)
                                               .extracting(c -> tuple(c.getValuta(), c.getCodicePuntoErogazione(), c.getNazioneTarga(), c.getPrezzoUnitario()))
                                               .contains(tuple("EUR", "::puntoErogazione::", null, 2d),
                                                         tuple("EUR", "::puntoErogazione::", null, 3d));
  }

  private void printToJsonFormat(AbstractAllegatoFatturaMessage message) {
    StringBuilder sb = new StringBuilder("Start Message generated:\n");
    try {
      sb.append(new ObjectMapper().writerWithDefaultPrettyPrinter()
                                  .writeValueAsString(message));
    } catch (Exception e) {
      sb.append(message);
    }
    sb.append("\nEnd message generated");
    System.out.println(sb.toString());

  }

  private ViewAllegatoGenericoSezione2Entity addViewEntity(String contractCode, String id, String tipoDispositivo, Double importo) {
    ViewAllegatoGenericoSezione2Entity e = new ViewAllegatoGenericoSezione2EntityBuilder().createViewAllegatoGenericoSezione2Entity();
    e.setId(id);
    e.setCodiceContratto(contractCode);
    e.setDataFine(LocalDateTime.now());
    e.setDataInizio(LocalDateTime.now());
    e.setDescrizioneAggiuntiva(RandomString());
    e.setPercIva(22.0);
    e.setImporto(importo);
    e.setQuantita(quantita);
    Double imponibile = importo * quantita;
    e.setImponibile(imponibile);
    e.setCodiceProdotto("PK001");
    e.setSerialNumber(RandomString());
    e.setPanNumber("");
    e.setTargaVeicolo(RandomLicense());
    e.setNazioneVeicolo("IT");
    e.setClassificazioneEuro("E");
    e.setTipoDispositivo(StringUtils.isNotBlank(tipoDispositivo) ? tipoDispositivo : "");
    e.setTratta(RandomString());
    e.setValuta("EUR");
    e.setCambio(RandomDouble());
    e.setClasseTariffaria("D");
    e.setCodiceFornitoreNav("F007227");
    e.setDocumentoDaFornitore("doc_fornitore");
    e.setPuntoErogazione("::puntoErogazione::");
    e.setRecordCode("DEFAULT");
    e.setRaggruppamentoArticoli(RandomString());
    return e;
  }

}
