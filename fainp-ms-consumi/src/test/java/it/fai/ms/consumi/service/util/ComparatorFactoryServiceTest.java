package it.fai.ms.consumi.service.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("unit")
class ComparatorFactoryServiceTest {

  @Test
  void when_called_with_unkown_label_getComparatorByLabel_should_return_DEFAULT_comparator() {
    assertThat(ComparatorFactoryService.getPathComparatorByLabel("NONE"))
      .isEqualTo(ComparatorFactoryService.FILENAME_ASC);
  }

  @Test
  void when_called_without_null_label_getComparatorByLabel_should_return_DEFAULT_comparator() {
    assertThat(ComparatorFactoryService.getPathComparatorByLabel(null))
      .isEqualTo(ComparatorFactoryService.FILENAME_ASC);
  }

  @Test
  void when_called_with_FILENAME_DESC_label_getComparatorByLabel_should_return_FILENAME_DESC_comparator() {
    assertThat(ComparatorFactoryService.getPathComparatorByLabel("FILENAME_DESC"))
      .isEqualTo(ComparatorFactoryService.FILENAME_DESC);
  }
}
