package it.fai.ms.consumi.repository.viewallegati;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Tag("unit")
@Transactional
class ViewAllegatoPedaggioSezione2RepositoryImplTest {

  @Autowired
  private EntityManager em;

  private DettaglioStanziamentiRepositoryImpl dettaglioRepo;

  private ViewAllegatoPedaggioSezione2Repository sezione2Repository;

  @BeforeEach
  void setUp() throws Exception {
    sezione2Repository = new ViewAllegatoPedaggioSezione2RepositoryImpl(em);
    dettaglioRepo = new DettaglioStanziamentiRepositoryImpl(em, null, null);
  }

  @Test
  void testFindDettagliStanziamenti() {
    List<String> newCodiciStanziamento = Arrays.asList("d1", "d2");
    List<UUID> list = dettaglioRepo.findDettagliStanziamentoIdByCodiciStanziamento(newCodiciStanziamento);
    assertThat(list).hasSize(0);

    UUID randomUUID = UUID.randomUUID();
    Query createNativeQuery = em.createNativeQuery("INSERT INTO stanziamento (codice_stanziamento) VALUES ('ABCD')");
    int executeUpdate = createNativeQuery.executeUpdate();
    System.out.println("EXECUTE: " + executeUpdate);
    
    createNativeQuery = em.createNativeQuery("INSERT INTO dettaglio_stanziamento (id, tipo_dettaglio_stanziamento, global_identifier) VALUES ('" + randomUUID + "', 'PEDAGGI', 'globalidentifier')");
    executeUpdate = createNativeQuery.executeUpdate();
    System.out.println("EXECUTE: " + executeUpdate);
    createNativeQuery = em.createNativeQuery("INSERT INTO dettagliostanziamento_stanziamento (stanziamenti_codice_stanziamento, dettaglio_stanziamenti_id) VALUES ('ABCD', '"
                                             + randomUUID + "')");
    executeUpdate = createNativeQuery.executeUpdate();
    System.out.println("EXECUTE: " + executeUpdate);
    newCodiciStanziamento = Arrays.asList("ABCD", "d2");
    list = dettaglioRepo.findDettagliStanziamentoIdByCodiciStanziamento(newCodiciStanziamento);
    assertThat(list).hasSize(1);
  }

  @Test
  void testDettagli() {
    Set<ViewAllegatoPedaggioSezione2Entity> dettagli = sezione2Repository.findDettagliBy(TipoDispositivoEnum.TELEPASS_EUROPEO, "::targa::",
                                                                                         "IT", "::pannumber::", "2");
    assertThat(dettagli).isEmpty();
  }

}
