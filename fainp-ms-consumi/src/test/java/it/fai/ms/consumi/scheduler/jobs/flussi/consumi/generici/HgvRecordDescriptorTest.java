package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;
import it.fai.ms.consumi.repository.flussi.record.model.generici.HgvRecord;

@Tag("unit")
class HgvRecordDescriptorTest {

  private HgvRecordDescriptor hgvRecordDescriptor;
  private String[]            data;

  @BeforeEach
  public void setUp() {
    data = new String[1];
    data[0] = "EM532ED,I,D,EC5,2017-04-03,2017-04-09,17.5,Pre-pagato,2017-03-31,FAISERVICE";
    hgvRecordDescriptor = new HgvRecordDescriptor();
  }

  @Test
  public void getStartEndPosTotalRows() {
    assertThrows(RuntimeException.class, () -> {
      hgvRecordDescriptor.getStartEndPosTotalRows();
    });
  }

  @Test
  public void getHeaderBegin() {
    assertThat(hgvRecordDescriptor.getHeaderBegin()).isEqualTo(
      "Numero di immatricolazione del veicolo,Paese,Fascia,Categoria Euro,Data di inizio,Data di fine,Importo,Stato,Data di acquisto,Acquistato da");
  }

  @Test
  public void getFooterCodeBegin() {
    assertThat(hgvRecordDescriptor.getFooterCodeBegin()).isEqualTo("§ NO HEADER / FOOTER FILE §");
  }

  @Test
  public void getLinesToSubtractToMatchDeclaration() {
    assertThat(hgvRecordDescriptor.getLinesToSubtractToMatchDeclaration()).isEqualTo(0);
  }

  @Test
  public void extractRecordCode() {
    assertThat(hgvRecordDescriptor.extractRecordCode(data[0])).isEqualTo("");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_header() {
    HgvRecord record = hgvRecordDescriptor.decodeRecordCodeAndCallSetFromString(data[0], "", "levy_periods.csv", 0);

    assertThat(record.getRecordCode()).isEqualTo("DEFAULT");
    assertThat(record.getTransactionDetail()).isEqualTo("");
    assertThat(record.getNumeroImmatricolazioneVeicolo()).isNull();
    assertThat(record.getPaese()).isNull();
    assertThat(record.getFascia()).isNull();
    assertThat(record.getCategoriaEuro()).isNull();
    assertThat(record.getDataInizio()).isNull();
    assertThat(record.getDataFine()).isNull();
    assertThat(record.getImporto()).isNull();
    assertThat(record.getStato()).isNull();
    assertThat(record.getDataAcquisto()).isNull();
    assertThat(record.getAcquistatoDa()).isNull();
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_record() {
    HgvRecord record = hgvRecordDescriptor.decodeRecordCodeAndCallSetFromString(data[0], "20", "levy_periods.csv", -1);

    assertThat(record.getRecordCode()).isEqualTo("DEFAULT");
    assertThat(record.getTransactionDetail()).isEqualTo("");
    assertThat(record.getNumeroImmatricolazioneVeicolo()).isEqualTo("EM532ED");
    assertThat(record.getPaese()).isEqualTo("I");
    assertThat(record.getFascia()).isEqualTo("D");
    assertThat(record.getCategoriaEuro()).isEqualTo("EC5");
    assertThat(record.getDataInizio()).isEqualTo("2017-04-03");
    assertThat(record.getDataFine()).isEqualTo("2017-04-09");
    assertThat(record.getImporto()).isEqualTo("17.5");
    assertThat(record.getStato()).isEqualTo("Pre-pagato");
    assertThat(record.getDataAcquisto()).isEqualTo("2017-03-31");
    assertThat(record.getAcquistatoDa()).isEqualTo("FAISERVICE");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_footer() {
    //Non ha footer
  }
}
