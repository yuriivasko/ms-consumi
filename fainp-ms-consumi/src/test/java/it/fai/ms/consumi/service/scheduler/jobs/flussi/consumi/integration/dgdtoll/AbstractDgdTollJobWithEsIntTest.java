package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.dgdtoll;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.fai.ms.consumi.config.ApplicationProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.repository.NotificationSetupRepository;
import it.fai.common.notification.service.NotificationMapper;
import it.fai.common.notification.service.NotificationMessageSender;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.DgdTollJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.DgdTollRecordDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.DgdTollRecordFileFormalValidator;
import it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer;
import it.fai.ms.consumi.service.navision.NavArticlesService;
import it.fai.ms.consumi.service.navision.NavSupplierCodesService;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti.TrackycardCarburantiStandardJobDaConsumoWithESIntTest;
import it.fai.ms.consumi.service.validator.CardNumberValidityService;
import it.fai.ms.consumi.service.validator.ConsumoNoBlockingValidatorService;
import it.fai.ms.consumi.service.validator.RecordFileFormalValidator;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidatorImpl;
import it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory;

@Tag("integration")
public abstract class AbstractDgdTollJobWithEsIntTest
  extends AbstractIntegrationTestSpringContext {

  protected transient final Logger log = LoggerFactory.getLogger(getClass());

  protected final String                      filename;
  protected final String                      partnerCode;
  protected       ServicePartner              servicePartner;
  protected       DgdTollJob                  job;
  protected       StanziamentiParamsValidator stanziamentiParamsValidator;
  protected       DgdTollRecordDescriptor     descriptor;
  protected RecordFileFormalValidator<DgdTollRecord> fileFormalValidator;

  @Autowired
  protected ConsumoProcessor<DgdToll> processor;

  @Autowired
  protected RecordConsumoMapper<DgdTollRecord, DgdToll> consumoMapper;

  @Autowired
  protected StanziamentoRepository stanziamentiRepository;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Autowired
  protected StanziamentiParamsService stanziamentiParamsService;

  @Autowired
  protected RecordPersistenceService recordPersistenceService;

  @Autowired
  protected ServiceProviderRepository serviceProviderRepository;

  @Autowired
  private CardNumberValidityService cardNumberValidityService;

  @Autowired
  private PlatformTransactionManager transactionManager;
  
  @Autowired
  ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService;


  public AbstractDgdTollJobWithEsIntTest(String filename, String partnerCode) throws URISyntaxException {
    this.filename = new File(TrackycardCarburantiStandardJobDaConsumoWithESIntTest.class.getResource(filename).toURI()).getAbsolutePath();
    this.partnerCode = partnerCode;
  }

  @BeforeEach
  public void setUp() throws Exception {

//    ApplicationProperties appProperties = newJobProperties();
    descriptor = new DgdTollRecordDescriptor();

    fileFormalValidator = new DgdTollRecordFileFormalValidator(cardNumberValidityService, descriptor);
    stanziamentiParamsValidator = spy(
      new StanziamentiParamsValidatorImpl(stanziamentiParamsService, mock(NavArticlesService.class), mock(NavSupplierCodesService.class), consumoNoBlockingValidatorService, mock(ApplicationProperties.class),
                                          true));
    AllStanziamentiParams stanzaParams = stanziamentiParamsService.getAll();
    assertThat(stanzaParams).isNotNull();
    List<StanziamentiParams> stanzParamList = stanzaParams.getStanziamentiParams();
    assertThat(stanzParamList).isNotEmpty();
    var oneStanziamentiParamForTest = stanzParamList.get(0);
    log.info("StanziamentiParam in use for Tests = " + oneStanziamentiParamForTest.toString());
    doReturn(ValidationOutcomeTestFactory.newValidationOutcomeWithStanzParam(oneStanziamentiParamForTest)).when(stanziamentiParamsValidator)
                                                                                                          .checkConsistency();

    NotificationService notificationService = spy(
      new NotificationService(mock(NotificationMessageSender.class), mock(NotificationSetupRepository.class),
                              mock(NotificationMapper.class), "DgdToll"));
    doAnswer(invocation -> {
      log.info("notifying code: " + invocation.getArgument(0));
      Map<String, Object> errorMapCode = invocation.getArgument(1);
      errorMapCode.entrySet().forEach(entry -> log.info("key for message:" + entry.getKey() + " " + entry.getValue()));
      return null;
    }).when(notificationService).notify(anyString(), anyMap());
    // stanziamentiToNavJmsProducer = mock(StanziamentiToNavJmsProducer.class);
    job = new DgdTollJob(transactionManager,stanziamentiParamsValidator, fileFormalValidator, notificationService, recordPersistenceService,
                         descriptor, processor, consumoMapper, stanziamentiToNavJmsPublisher);

    servicePartner = findServicePartner(partnerCode);

  }

  protected ServicePartner findServicePartner(String providerCode) {
    ServiceProvider sp = serviceProviderRepository.findByProviderCode(providerCode);
    return sp.toServicePartner();
  }

  @Test
  public void testKOStanziamentiCheckConsistency() {

    ValidationOutcome vo = new ValidationOutcome().message(new Message("001").text("this is test error!"));
    vo.setValidationOk(false);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isEmpty();
  }
}
