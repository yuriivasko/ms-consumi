package it.fai.ms.consumi.service.jms.listener;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.document.allegatifattura.AllegatiFattureDTO;
import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatiFattureRepository;
import it.fai.ms.consumi.service.consumer.CreationAttachmentInvoiceConsumer;
import it.fai.ms.consumi.service.fatturazione.AllegatiFattureService;

@Tag("unit")
class JmsListenerCreationAttachmentInvoiceRequestTest {

  private StanziamentoRepository stanziamentoRepository = mock(StanziamentoRepository.class);

  private AllegatiFattureService allegatiFattureServiceMock = mock(AllegatiFattureService.class);

  private final ViewAllegatiFattureRepository viewAllegatiFattureRepoMock = mock(ViewAllegatiFattureRepository.class);

  private NotificationService notificationServiceMock = mock(NotificationService.class);

  private CreationAttachmentInvoiceConsumer allegatiFatturaConsumer;

  private AllegatiFattureDTO attachmentInvoice;

  private List<String> codiciStanziamenti;

  private BigDecimal ten = BigDecimal.TEN;

  private HashMap<String, Object> templateParameters;

  @BeforeEach
  void setUp() throws Exception {
    attachmentInvoice = new AllegatiFattureDTO();
    attachmentInvoice.setNumeroFattura("000001111");
    attachmentInvoice.setImportoTotale(new BigDecimal(100d));
    attachmentInvoice.setDataFattura(Instant.now());

    List<RaggruppamentoArticoliDTO> raggruppamentiArticoli = new ArrayList<>();
    RaggruppamentoArticoliDTO r1 = new RaggruppamentoArticoliDTO();
    r1.setCodice("AUT_GB");
    r1.setCodiceClienteFatturazione("codice_cliente_fatturazione1");
    r1.setCodiciStanziamenti(new ArrayList<>(Arrays.asList("COD1", "COD2", "COD3")));

    RaggruppamentoArticoliDTO r2 = new RaggruppamentoArticoliDTO();
    r2.setCodice("AUT_CH");
    r1.setCodiceClienteFatturazione("codice_cliente_fatturazione2");
    r2.setCodiciStanziamenti(new ArrayList<>(Arrays.asList("COD7", "COD8", "COD9")));

    raggruppamentiArticoli.add(r1);
    raggruppamentiArticoli.add(r2);
    attachmentInvoice.setRaggruppamentiArticoli(raggruppamentiArticoli);

    codiciStanziamenti = new ArrayList<>(Arrays.asList("COD1", "COD2", "COD3", "COD7", "COD8", "COD9"));

    allegatiFatturaConsumer = new CreationAttachmentInvoiceConsumer(stanziamentoRepository, allegatiFattureServiceMock,
                                                                    viewAllegatiFattureRepoMock, notificationServiceMock);

    templateParameters = new HashMap<>();
    templateParameters.put("totaleCalcolato", ten.setScale(2, RoundingMode.HALF_UP));
    templateParameters.put("totaleDaNav", attachmentInvoice.getImportoTotale()
                                                           .setScale(2, RoundingMode.HALF_UP));
    templateParameters.put("numFattura", attachmentInvoice.getNumeroFattura());
    templateParameters.put("dataFattura", attachmentInvoice.getDataFattura());
  }

  @Test
  void testConsumeMessageReturnAnomaly() throws Exception {

    Set<String> stanziamenti = new HashSet<>(codiciStanziamenti);

    when(stanziamentoRepository.findByCodiceStanziamentoIn(Mockito.any())).thenReturn(stanziamenti);

    when(viewAllegatiFattureRepoMock.findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(codiciStanziamenti)).thenReturn(ten);
    allegatiFatturaConsumer.consume(attachmentInvoice);

    verify(viewAllegatiFattureRepoMock).findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(codiciStanziamenti);

    verify(notificationServiceMock).notify("INVNAV-001", templateParameters);
  }

  @Test
  void testConsumeMessageAndManageRaggrArticoli() throws Exception {

    Set<String> stanziamenti = new HashSet<>(codiciStanziamenti);

    when(stanziamentoRepository.findByCodiceStanziamentoIn(Mockito.any())).thenReturn(stanziamenti);

    when(viewAllegatiFattureRepoMock.findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(codiciStanziamenti)).thenReturn(new BigDecimal(100d));
    allegatiFatturaConsumer.consume(attachmentInvoice);

    verify(viewAllegatiFattureRepoMock).findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(codiciStanziamenti);
    verify(allegatiFattureServiceMock, times(2)).manageRaggruppamentoArticoli(Mockito.eq(attachmentInvoice.getNumeroFattura()),
                                                                              Mockito.eq(attachmentInvoice.getDataFattura()),
                                                                              Mockito.any(RaggruppamentoArticoliDTO.class),
                                                                              Mockito.eq(attachmentInvoice.getImportoTotale()),
                                                                              Mockito.eq(null), Mockito.any());
    verifyZeroInteractions(notificationServiceMock);
  }

}
