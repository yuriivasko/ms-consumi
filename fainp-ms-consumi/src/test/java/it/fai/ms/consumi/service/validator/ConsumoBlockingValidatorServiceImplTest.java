package it.fai.ms.consumi.service.validator;

import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.GlobalIdentifierTestBuilder;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.generici.telepass.ElviaGenerico;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.flussi.record.model.TestDummyRecord;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.validator.internal.ContractValidator;
import it.fai.ms.consumi.service.validator.internal.DeviceValidator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

@Tag("unit")
class ConsumoBlockingValidatorServiceImplTest {

  private final ContractValidator           contractValidator = mock(ContractValidator.class);
  private final DeviceValidator             deviceValidator = mock(DeviceValidator.class);
  private final StanziamentiParamsValidator stanziamentiParamsValidator = mock(StanziamentiParamsValidator.class);
  private final CustomerService             customerService = mock(CustomerService.class);

  final TrackyCardCarburantiJolly trustedCustomerSupplier    = new TrackyCardCarburantiJolly(
    new Source(Instant.now(), Instant.now(), ""),
    "",
    Optional.of(new TrackyCardGlobalIdentifier("")),
    TestDummyRecord.instance1
  );

  final Elvia                     deviceSupplierConsumo    = new Elvia(new Source(Instant.now(), Instant.now(), ""), "", GlobalIdentifierTestBuilder.newElviaGlobalIdentifier(), TestDummyRecord.instance1);
  final ElviaGenerico             notDeviceSupplierConsumo = new ElviaGenerico(new Source(Instant.now(), Instant.now(), ""), "", GlobalIdentifierTestBuilder.newElviaGlobalIdentifier(), TestDummyRecord.instance1);

  ConsumoBlockingValidatorServiceImpl consumoBlockingValidatorService = new ConsumoBlockingValidatorServiceImpl(
    contractValidator, deviceValidator, stanziamentiParamsValidator
  );

  @Test
  public void validateCustomer_TrustedCustomerSupplier_without_customerId_will_give_KO(){

    //not to be called
    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.empty());

    //not to be called
    given(customerService.findCustomerByUuid(any()))
      .willReturn(Optional.empty());

    Optional<ValidationOutcome> validationOutcome = consumoBlockingValidatorService.validateCustomer(trustedCustomerSupplier);
    assertThat(validationOutcome.get().isValidationOk())
      .isFalse();
  }

  @Test
  public void validateCustomer_TrustedCustomerSupplier_with_customerId_will_give_OK(){

    //    deviceSupplierConsumo.setDevice(device);
    trustedCustomerSupplier.setCustomer(new Customer("::id::"));

    //not to be called
    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.empty());

    //not to be called
    given(customerService.findCustomerByUuid(any()))
      .willReturn(Optional.empty());

    Optional<ValidationOutcome> validationOutcome = consumoBlockingValidatorService.validateCustomer(trustedCustomerSupplier);
    assertThat(validationOutcome.get().isValidationOk())
      .isTrue();
  }
}
