package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.performance.simpleprofiler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleProfilePrinter {
  private String name;
  private Instant t0;
  private MethodCallInfo root;
  private Instant tN;

  public SimpleProfilePrinter(String name) {
    this.name = name;
  }

  public void init(Instant t0, MethodCallInfo root) {
    this.t0 = t0;
    this.root = root;
  }

  public void end(Instant tN) {
    this.tN = tN;
  }

  public void printFlatTimeTableOnStdOut() {
    System.out.println("\n *** START STD OUT FILTERED TIMETABLE (t0 " + StringUtils.leftPad("" + t0, 30, " ") + ") FOR: " + name + " *** \n");

    StringWriter out = new StringWriter();
    PrintWriter writer = new PrintWriter(out);
    printFilteredTimeTableOnPrintStreamWithSeparator(writer, " | ");

    System.out.print(out.toString());
    System.out.println("\n ***  END  STD OUT FILTERED TIMETABLE (tN " + StringUtils.leftPad("" + tN, 30, " ") + ") FOR: " + name + " *** \n");
    System.out.println("\n ***  TOTAL TIME (tN " + (tN.toEpochMilli() - t0.toEpochMilli()) + ") FOR: " + name + " *** \n");
  }

  public void printGroupedTimeTableOnStdOut() {
    System.out.println("\n *** START STD OUT GROUPED TIMETABLE (t0 " + StringUtils.leftPad("" + t0, 30, " ") + ") FOR: " + name + " *** \n");
    StringWriter out = new StringWriter();
    PrintWriter writer = new PrintWriter(out);

    printGroupedTimeTableOnPrintStreamWithSeparator(writer, " | ");

    System.out.print(out.toString());
    System.out.println("\n ***  END  STD OUT GROUPED TIMETABLE (tN " + StringUtils.leftPad("" + tN, 30, " ") + ") FOR: " + name + " *** \n");
    System.out.println("\n ***  TOTAL TIME (tN " + (tN.toEpochMilli() - t0.toEpochMilli()) + ") FOR: " + name + " *** \n");
  }

  public void printGroupedTimeTableOnCsv(String filename) {
    System.out.println("\n *** START CSV GROUPED TIMETABLE (t0 " + StringUtils.leftPad("" + t0, 30, " ") + ") IN: " + filename + " *** \n");

    try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(Paths.get(filename), StandardOpenOption.CREATE_NEW))) {
      printGroupedTimeTableOnPrintStreamWithSeparator(writer, ";");
    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println("\n ***  END  CSV OUT GROUPED TIMETABLE (tN " + StringUtils.leftPad("" + tN, 30, " ") + ") IN: " + filename + " *** \n");
    System.out.println("\n ***  TOTAL TIME (tN " + (tN.toEpochMilli() - t0.toEpochMilli()) + ") FOR: " + name + " *** \n");
  }

  public void printFilteredTimeTableOnPrintStreamWithSeparator(PrintWriter writer, String separator) {
    printTimeTableHeader(writer, separator);

    List<MethodCallInfo> filteredMethodCallInfos = getMethodCallInfoQueue()
      .stream()
      .filter(methodCallInfo -> methodCallInfo.millisecondsFrom(t0) >= 0)
      .filter(methodCallInfo -> methodCallInfo.duration() > 1)
      .collect(Collectors.toList());

    printTimeTableBody(writer, separator, filteredMethodCallInfos);
  }

  private List<MethodCallInfo> getMethodCallInfoQueue() {
    List<MethodCallInfo> methodCallInfos = new LinkedList<>();
    visit(methodCallInfos, root);
    return methodCallInfos;
  }

  private void visit(List<MethodCallInfo> methodCallInfos, MethodCallInfo currentNode) {
    if (!currentNode.children.isEmpty()) {
      for (MethodCallInfo child : currentNode.children) {
        methodCallInfos.add(child);
        visit(methodCallInfos, child);
      }
    }
  }

  private void printTimeTableHeader(PrintWriter writer, String separator) {
    writer.println(
      StringUtils.leftPad("TIME FROM T0", 15, " ") + separator +
        StringUtils.leftPad("FROM PREVIOUS", 15, " ") + separator +
        StringUtils.leftPad("ENTERING TIME", 30, " ") + separator +
        StringUtils.leftPad("EXIT TIME", 30, " ") + separator +
        StringUtils.leftPad("DURATION", 15, " ") + separator +
        StringUtils.leftPad("%", 5, " ") + separator +
        StringUtils.leftPad("NEST", 5, " ") + separator +
        "METHOD NAME");
  }

  private void printTimeTableBody(PrintWriter writer, String separator, List<MethodCallInfo> filteredMethodCallInfos) {
    MethodCallInfo previousMethodCallInfo = null;
    for (MethodCallInfo methodCallInfo : filteredMethodCallInfos) {
      writer.println(
        StringUtils.leftPad("" + methodCallInfo.millisecondsFrom(t0), 15, " ") + separator +
          StringUtils.leftPad("" + methodCallInfo.millisecondsFrom((previousMethodCallInfo == null ? t0 : previousMethodCallInfo.enterTimestamp)), 15, " ") + separator +
          StringUtils.leftPad("" + methodCallInfo.enterTimestamp, 30, " ") + separator +
          StringUtils.leftPad("" + methodCallInfo.exitTimestamp, 30, " ") + separator +
          StringUtils.leftPad("" + methodCallInfo.duration(), 15, " ") + separator +
          StringUtils.leftPad(String.format("%02.2f", methodCallInfo.durationPercent(t0, tN)), 5, " ") + separator +
          StringUtils.leftPad("" + methodCallInfo.nestingLevel(), 5, " ") + separator +
          StringUtils.leftPad("", methodCallInfo.nestingLevel(), " ") + methodCallInfo.declaringTypeName + "." + methodCallInfo.name);
      previousMethodCallInfo = methodCallInfo;
    }
  }


  public void printGroupedTimeTableOnPrintStreamWithSeparator(PrintWriter writer, String separator) {
    printGroupTimeTableHeader(writer, separator);

    List<MethodCallInfo> methodCallInfoQueue = getMethodCallInfoQueue();
    List<MethodCallInfo> methodCallInfoQueue2 = getMethodCallInfoQueue();

    List<GroupedMethodCallInfo> groupedMethodCallInfos =
      methodCallInfoQueue
      .stream()
//        .filter(methodCallInfo -> methodCallInfo.millisecondsFrom(t0) >= 0)
//        .filter(methodCallInfo -> methodCallInfo.duration() > 1)
      .collect(Collectors.groupingBy(methodCallInfo -> methodCallInfo.callStackStringTrace()))
        .entrySet()
        .stream()
        .map(stringListEntry -> new GroupedMethodCallInfo(stringListEntry.getValue()))
      .collect(Collectors.toList())
      .stream()
        .map(groupedMethodCallInfo -> Pair.of(methodCallInfoQueue2.indexOf(groupedMethodCallInfo.firstMethodCallInfo()), groupedMethodCallInfo))
        .sorted(Comparator.comparingInt(value -> value.getLeft()))
        .map(Pair::getRight)
      .collect(Collectors.toList());

    printGroupTimeTableBody(writer, separator, groupedMethodCallInfos);
  }

  private void printGroupTimeTableHeader(PrintWriter writer, String separator) {
    writer.println(
      StringUtils.leftPad("TOTAL DURATION", 15, " ") + separator +
        StringUtils.leftPad("%", 5, " ") + separator +
        StringUtils.leftPad("COUNT", 5, " ") + separator +
        StringUtils.leftPad("AVG D", 5, " ") + separator +
        StringUtils.leftPad("NEST", 5, " ") + separator +
        "METHOD NAME");
  }

  private void printGroupTimeTableBody(PrintWriter writer, String separator, List<GroupedMethodCallInfo> groupedMethodCallInfos) {
    for (GroupedMethodCallInfo groupedMethodCallInfo : groupedMethodCallInfos) {
      writer.println(
        StringUtils.leftPad("" + groupedMethodCallInfo.totalDuration(), 15, " ") + separator +
          StringUtils.leftPad(String.format("%02.2f", groupedMethodCallInfo.totalPercentage(t0, tN)), 5, " ") + separator +
          StringUtils.leftPad("" + groupedMethodCallInfo.callCount(), 5, " ") + separator +
          StringUtils.leftPad("" + groupedMethodCallInfo.avgDuration(), 5, " ") + separator +
          StringUtils.leftPad("" + groupedMethodCallInfo.firstMethodCallInfo().nestingLevel(), 5, " ") + separator +
          StringUtils.leftPad("", groupedMethodCallInfo.firstMethodCallInfo().nestingLevel(), " ") + groupedMethodCallInfo.firstMethodCallInfo().declaringTypeName + "." + groupedMethodCallInfo.firstMethodCallInfo().name);
    }
  }

}
