package it.fai.ms.consumi.service.processor.consumi.sanbernardo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.jms.producer.CambioStatoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Tag("unit")
public class SanBernardoProcessorImplTest {

  private TessereSanBernardoProcessorImpl tessereSanBernardoProcessor;
  private final ConsumoValidatorService consumoValidatorService = mock(ConsumoValidatorService.class);
  private final StanziamentiProducer stanziamentiProducer = mock(StanziamentiProducer.class);
  private final VehicleService vehicleService = mock(VehicleService.class);
  private final CustomerService customerService = mock(CustomerService.class);
  private TessereSanBernardoConsumoDettaglioStanziamentoMapper dettaglioStanziamentoMapper = mock(TessereSanBernardoConsumoDettaglioStanziamentoMapper.class);
  private PriceCalculatedByNavService priceCalculatedByNavService = mock(PriceCalculatedByNavService.class);
  private CambioStatoDaConsumoJmsProducer changeStatoPublisher = mock(CambioStatoDaConsumoJmsProducer.class);

  @BeforeEach
  public void init() {
    tessereSanBernardoProcessor = new TessereSanBernardoProcessorImpl(
      consumoValidatorService,
      vehicleService,
      customerService,
      stanziamentiProducer,
      dettaglioStanziamentoMapper,
      priceCalculatedByNavService,
      changeStatoPublisher
    );
  }


  @Test
  void validateAndProcess_if_fail_blocking_validation_will_sikp_stanziamento_production() {
    SanBernardo consumo = mock(SanBernardo.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);

    given(consumo.getVehicle()).willReturn(Vehicle.newEmptyVehicleWithFareClass("4A"));

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isFatalError())
      .willReturn(true);

    tessereSanBernardoProcessor.validateAndProcess(consumo, bucket);
    then(stanziamentiProducer)
      .shouldHaveZeroInteractions();
    then(changeStatoPublisher).should(never()).sendFatturato(any(), any());
  }

  @Test
  void validateAndProcess_if_fail_waring_validation_will_produce_notification_and_stanziamentos() {
    SanBernardo consumo = mock(SanBernardo.class);
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);

    given(consumo.getDate()).willReturn(Instant.now());

    given(consumo.getVehicle()).willReturn(Vehicle.newEmptyVehicleWithFareClass("4A"));

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    Bucket bucket = new Bucket("");

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(false);

    given(validationOutcome.getMessages())
      .willReturn(new HashSet<>(Arrays.asList(new Message("").withText(""))));

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);

    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    Article article = new Article("::articleCode::");
    article.setCountry("::country::");
    Currency currency = Currency.getInstance("EUR");
    article.setCurrency(currency);
    article.setVatRate(22f);

    given(stanziamentiParams.getArticle())
      .willReturn(article);

    given(stanziamentiParams.getCostoRicavo())
      .willReturn(CostoRicavo.C);

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(dettaglioStanziamentoMapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamento.class));

    given(priceCalculatedByNavService.getCalculatedPriceByNav(any())).willReturn(new BigDecimal(10d));

    ProcessingResult processingResult = tessereSanBernardoProcessor.validateAndProcess(consumo, bucket);

    then(consumoValidatorService)
    .should(times(1))
    .notifyProcessingResult(any());

    then(changeStatoPublisher).should(never()).sendFatturato(any(), any());

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }

  @Test
  void validateAndProcess_if_success_validation_will_produce_stanziamentos() {
    SanBernardo consumo = mock(SanBernardo.class);
    Bucket bucket = new Bucket("");
    final Device device = mock(Device.class);

    given(consumo.getDevice())
      .willReturn(device);

    given(device.getId())
      .willReturn("::id::");

    given(consumo.getDate()).willReturn(Instant.now());

    given(consumo.getVehicle()).willReturn(Vehicle.newEmptyVehicleWithFareClass("4A"));

    given(device.getType())
      .willReturn(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);

    given(customerService.findCustomerByDeviceSeriale(any(), any()))
      .willReturn(Optional.of(mock(Customer.class)));

    final ValidationOutcome validationOutcome = mock(ValidationOutcome.class);
    given(consumoValidatorService.validate(consumo))
      .willReturn(validationOutcome);

    given(validationOutcome.isValidationOk())
      .willReturn(true);

    final StanziamentiParams stanziamentiParams = mock(StanziamentiParams.class);
    given(validationOutcome.getStanziamentiParamsOptional())
      .willReturn(Optional.of(stanziamentiParams));

    Article article = new Article("::articleCode::");
    article.setCountry("::country::");
    Currency currency = Currency.getInstance("EUR");
    article.setCurrency(currency);
    article.setVatRate(22f);

    given(stanziamentiParams.getArticle())
      .willReturn(article);

    given(stanziamentiParams.getCostoRicavo())
      .willReturn(CostoRicavo.C);

    given(stanziamentiParams.getSupplier())
      .willReturn(mock(Supplier.class));

    final List<Stanziamento> stanziamentoList = Arrays.asList(mock(Stanziamento.class));
    given(stanziamentiProducer.produce(any(), any()))
      .willReturn(stanziamentoList);

    given(dettaglioStanziamentoMapper.mapConsumoToDettaglioStanziamento(consumo))
      .willReturn(mock(DettaglioStanziamento.class));

    given(priceCalculatedByNavService.getCalculatedPriceByNav(any())).willReturn(new BigDecimal(10d));

    ProcessingResult processingResult = tessereSanBernardoProcessor.validateAndProcess(consumo, bucket);

    then(changeStatoPublisher).should(never()).sendFatturato(any(), any());

    assertThat(processingResult.getStanziamenti())
      .isEqualTo(stanziamentoList);
  }

}
