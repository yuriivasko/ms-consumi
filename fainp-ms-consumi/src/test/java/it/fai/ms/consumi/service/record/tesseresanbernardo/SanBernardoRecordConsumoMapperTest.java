package it.fai.ms.consumi.service.record.tesseresanbernardo;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoViaggiRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.SanBernardoRecordFileInfo;
import it.fai.ms.consumi.web.rest.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
public class SanBernardoRecordConsumoMapperTest {
  private SanBernardoRecordConsumoMapper mapper;
  private final Instant now = Instant.now();


  @BeforeEach
  public void init() {
    mapper = new SanBernardoRecordConsumoMapper() {
      @Override
      protected Instant getNow() {
        return now;
      }
    };
  }

  @Test
  public void mapConsumoTessere() throws Exception {
    String fileName = "YYYYMMDD_elenco_viaggi.csv";
    SanBernardoViaggiRecord record = new SanBernardoViaggiRecord(fileName, 1);
    record.setClasse("4A");
    record.setData("31/05/2018 18:17");
    record.setNumeroPacchetto("10888");
    String expectedPan = "9756934101999003952";
    record.setPanNumber(expectedPan);
    record.setPiazzale("NORD");
    record.setPista("1");
    record.setTipoTitolo(SanBernardo.SAN_BERNARDO_TESSERE);
    Map<String,String> buoniOrdiniMap = new HashMap<>();
    /*
    tuple("516111315276", "516111315276", "4643", "4643", "130015808"),
      tuple("516343001058", "516343001058", "4636", "4636", "100205"),
      tuple("513458542745","513458542745","4636","4636","100205")
     */
    String numero = "516111315276";
    String ordineN = "4636";
    String commento="130015808";
    buoniOrdiniMap.put(numero, commento);

    SanBernardo consumo = mapper.mapRecordToConsumo(record);
    assertThat(consumo).isNotNull();
    assertThat(consumo.getInvoiceType()).isEqualTo(InvoiceType.D);
    assertThat(consumo.getSource().getIngestionTime()).isEqualTo(now);
    assertThat(consumo.getSource().getRowNumber()).isEqualTo(1l);
    assertThat(consumo.getDevice().getType()).isEqualTo(TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO);
    assertThat(consumo.getDevice().getPan()).isEqualTo(expectedPan);
    assertThat(consumo.getTollPointExit().getTime()).isEqualTo(TestUtils.fromStringToInstant("2018-05-31 18:17:00"));
    assertThat(consumo.getCustomerId()).isNull();
  }

  @Test
  public void mapConsumoBuoniClienteNotFound() throws Exception {
    String fileName = "YYYYMMDD_elenco_viaggi.csv";
    SanBernardoViaggiRecord record = new SanBernardoViaggiRecord(fileName, 1);
    record.setClasse("4A");
    record.setData("31/05/2018 18:17");
    record.setNumeroPacchetto("10888");
    String expectedPan = "513458542745";
    record.setPanNumber(expectedPan);
    record.setPiazzale("SUD");
    record.setPista("2");
    record.setTipoTitolo(SanBernardo.SAN_BERNARDO_BUONI);
    Map<String, String> buoniOrdiniMap = new HashMap<>();
    /*
    tuple("516111315276", "516111315276", "4643", "4643", "130015808"),
      tuple("516343001058", "516343001058", "4636", "4636", "100205"),
      tuple("513458542745","513458542745","4636","4636","100205")
     */
    String numero = "516111315276";
    String ordineN = "4636";
    String commento="130015808";
    buoniOrdiniMap.put(numero, commento);
    RecordsFileInfo sanBernardoRecordFileInfo = new SanBernardoRecordFileInfo(fileName,now.toEpochMilli(),now,buoniOrdiniMap);

  }

  @Test
  public void mapConsumiBuoni() throws Exception {
    String fileName = "YYYYMMDD_elenco_viaggi.csv";
    SanBernardoViaggiRecord record = new SanBernardoViaggiRecord(fileName, 1);
    record.setClasse("4A");
    record.setData("31/05/2018 18:17");
    record.setNumeroPacchetto("10888");
    String expectedPan = "513458542745";
    record.setPanNumber(expectedPan);
    record.setPiazzale("SUD");
    record.setPista("2");
    record.setTipoTitolo(SanBernardo.SAN_BERNARDO_BUONI);
    Map<String, String> buoniOrdiniMap = new HashMap<>();
    /*
    tuple("516111315276", "516111315276", "4643", "4643", "130015808"),
      tuple("516343001058", "516343001058", "4636", "4636", "100205"),
      tuple("513458542745","513458542745","4636","4636","100205")
     */
    String numero = expectedPan;
    String ordineN = "4636";
    String commento="130015808";
    buoniOrdiniMap.put(numero, commento);
    SanBernardo consumo = mapper.mapRecordToConsumo(record);
    assertThat(consumo).isNotNull();
    assertThat(consumo.getInvoiceType()).isEqualTo(InvoiceType.D);
    assertThat(consumo.getSource().getIngestionTime()).isEqualTo(now);
    assertThat(consumo.getSource().getRowNumber()).isEqualTo(1l);
    assertThat(consumo.getDevice().getType()).isEqualTo(TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO);
    assertThat(consumo.getDevice().getPan()).isEqualTo(expectedPan);
    assertThat(consumo.getTollPointExit().getTime()).isEqualTo(TestUtils.fromStringToInstant("2018-05-31 18:17:00"));
    assertThat(consumo.getCustomerId()).isEqualTo(null);
  }





}
