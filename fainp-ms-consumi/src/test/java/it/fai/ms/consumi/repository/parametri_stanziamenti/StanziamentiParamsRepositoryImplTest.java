package it.fai.ms.consumi.repository.parametri_stanziamenti;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Currency;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.fai.ms.consumi.domain.ArticleTestBuilder;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.StanziamentiParamsTestBuilder;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiDetailsTypeEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsArticleEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsCostoRicavoEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntityMapper;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntityTestBuilder;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsSupplierEntity;

@ExtendWith(SpringExtension.class)
@ComponentScan(basePackages = { "it.fai.ms.consumi.domain.parametri_stanziamento", "it.fai.ms.consumi.repository.parametri_stanziamento" })
@DataJpaTest
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.common.notification.repository" })
@Tag("unit")
class StanziamentiParamsRepositoryImplTest {

  @Autowired
  private EntityManager em;

  private StanziamentiParamsRepository repository;

  private StanziamentiParamsEntityMapper entityMapper;

  @BeforeEach
  void setUp() throws Exception {
    entityMapper = new StanziamentiParamsEntityMapper();
    repository = new StanziamentiParamsRepositoryImpl(em, entityMapper);
  }

  @Test
  void when_table_contains_moreThan_oneRow_findByArticle_returns_StanziamentiParams_by_articleCode() {

    // given
    var stanziamentiParams1 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                 .withArticleCode("::articleCode-1::")
                                                                 .withSupplierCode("::supplierCode-1::")
                                                                 .withTransactionType(null)
                                                                 .withMeasurementUnit(null)
                                                                 .withFareClass(null)
                                                                 .withSource("::source-1::")
                                                                 .withTransactionDetail(null)
                                                                 .withRecordCode(null)
                                                                 .withVatRate(22.0f)
                                                                 .withGrouping("::grouping::")
                                                                 .build();
    em.persist(stanziamentiParams1);

    var article2 = new StanziamentiParamsArticleEntity("::articleCode-2::");
    article2.setDescription("::description::");
    var supplier2 = new StanziamentiParamsSupplierEntity("::supplierCode-2::");
    supplier2.setLegacyCode("::supplierCodeLegacy::");
    var stanziamentiParams2 = new StanziamentiParamsEntity(article2);
    stanziamentiParams2.setCurrency(Currency.getInstance("USD"));
    stanziamentiParams2.setSource("::source-2::");
    stanziamentiParams2.setSupplier(supplier2);
    stanziamentiParams2.setCostoRicavo(StanziamentiParamsCostoRicavoEntity.R);
    stanziamentiParams2.setCountry("::country::");
    stanziamentiParams2.setGrouping("::grouping::");
    stanziamentiParams2.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.CARBURANTI);
    em.persist(stanziamentiParams2);

    // when
    var optionalStanziamentiParams = repository.findFirstByArticleCode("::articleCode-1::");

    // then
    assertThat(optionalStanziamentiParams).hasValueSatisfying((StanziamentiParams s) -> {
      assertThat(s.getArticle()
                  .getCode()).isEqualTo("::articleCode-1::");
      assertThat(s.getArticle()
                  .getDescription()).isEqualTo("::description::");
      assertThat(s.getArticle()
                  .getCurrency()).isEqualTo(Currency.getInstance("EUR"));
      assertThat(s.getArticle()
                  .getVatRate()).isEqualTo(22.0f);
      assertThat(s.getArticle()
                  .getCountry()).isEqualTo("::country::");
      assertThat(s.getArticle()
                  .getGrouping()).isEqualTo("::grouping::");
      assertThat(s.getTransactionType()).isNull();
      assertThat(s.getArticle()
                  .getMeasurementUnit()).isNull();
      assertThat(s.getFareClass()).isNull();
      assertThat(s.getSource()).isEqualTo("::source-1::");
      assertThat(s.getTransactionDetail()).isNull();
      assertThat(s.getRecordCode()).isEqualTo("DEFAULT");
      assertThat(s.getCostoRicavo()).isEqualTo(CostoRicavo.C);
    });
  }

  @Test
  void when_table_contains_oneRow_getAll_returns_list_with_size_one() {

    // given
    var stanziamentiParams = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                .withArticleCode("::articleCode::")
                                                                .withSupplierCode("::supplierCode::")
                                                                .withTransactionType("::transactionType::")
                                                                .withMeasurementUnit("::measurementUnit::")
                                                                .withFareClass("::fareClass::")
                                                                .withSource("::source::")
                                                                .withTransactionDetail("::transactionDetail::")
                                                                .withRecordCode("::recordCode::")
                                                                .withVatRate(22.0f)
                                                                .withGrouping("::grouping::")
                                                                .build();
    em.persist(stanziamentiParams);

    // when

    var allStanziamentiParams = repository.getAll();

    // then

    assertThat(allStanziamentiParams.getStanziamentiParams()).hasSize(1);
  }

  @Test
  void when_table_contains_moreThan_oneRow_getAll_returns_list_of_stanziamentiParams() {

    // given

    var stanziamentiParams1 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                 .withArticleCode("::articleCode-1::")
                                                                 .withSupplierCode("::supplierCode::")
                                                                 .withTransactionType("::transactionType::")
                                                                 .withMeasurementUnit("::measurementUnit::")
                                                                 .withFareClass("::fareClass::")
                                                                 .withSource("::source::")
                                                                 .withTransactionDetail("::transactionDetail::")
                                                                 .withRecordCode("::recordCode::")
                                                                 .withVatRate(22.0f)
                                                                 .withGrouping("::grouping::")
                                                                 .build();
    em.persist(stanziamentiParams1);

    var stanziamentiParams2 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                 .withArticleCode("::articleCode-2::")
                                                                 .withSupplierCode("::supplierCode::")
                                                                 .withTransactionType("::transactionType::")
                                                                 .withMeasurementUnit("::measurementUnit::")
                                                                 .withFareClass("::fareClass::")
                                                                 .withSource("::source::")
                                                                 .withTransactionDetail("::transactionDetail::")
                                                                 .withRecordCode("::recordCode::")
                                                                 .withVatRate(22.0f)
                                                                 .withGrouping("::grouping::")
                                                                 .build();
    em.persist(stanziamentiParams2);

    // when

    var allStanziamentiParams = repository.getAll();

    // then

    assertThat(allStanziamentiParams.getStanziamentiParams()).hasSize(2);
  }

  @Test
  void when_table_contains_oneRow_findByArticle_returns_StanziamentiParams() {

    // given

    var stanziamentiParams = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                .withArticleCode("::articleCode::")
                                                                .withSupplierCode("::supplierCode::")
                                                                .withTransactionType("::transactionType::")
                                                                .withMeasurementUnit("::measurementUnit::")
                                                                .withFareClass("::fareClass::")
                                                                .withSource("::source::")
                                                                .withTransactionDetail("::transactionDetail::")
                                                                .withRecordCode("::recordCode::")
                                                                .withVatRate(22.0f)
                                                                .withGrouping("::grouping::")
                                                                .build();
    em.persist(stanziamentiParams);

    // when

    var optionalStanziamentiParams = repository.findFirstByArticleCode("::articleCode::");

    // then

    assertThat(optionalStanziamentiParams).hasValueSatisfying((StanziamentiParams s) -> {
      assertThat(s.getArticle()
                  .getCode()).isEqualTo("::articleCode::");
      assertThat(s.getArticle()
                  .getDescription()).isEqualTo("::description::");
      assertThat(s.getArticle()
                  .getCurrency()).isEqualTo(Currency.getInstance("EUR"));
      assertThat(s.getArticle()
                  .getVatRate()).isEqualTo(22.0f);
      assertThat(s.getArticle()
                  .getCountry()).isEqualTo("::country::");
      assertThat(s.getArticle()
                  .getGrouping()).isEqualTo("::grouping::");
      assertThat(s.getArticle()
                  .getMeasurementUnit()).isEqualTo("::measurementUnit::");
      assertThat(s.getFareClass()).isEqualTo("::fareClass::");
      assertThat(s.getSource()).isEqualTo("::source::");
      assertThat(s.getTransactionDetail()).isEqualTo("::transactionDetail::");
      assertThat(s.getRecordCode()).isEqualTo("::recordCode::");
      assertThat(s.getSupplier()
                  .getCode()).isEqualTo("::supplierCode::");
      assertThat(s.getSupplier()
                  .getCodeLegacy()).isEqualTo("::supplierCodeLegacy::");
      assertThat(s.getCostoRicavo()).isEqualTo(CostoRicavo.C);
      assertThat(s.getTransactionType()).isEqualTo("::transactionType::");
      assertThat(s.getStanziamentiDetailsType()).isEqualTo(StanziamentiDetailsType.PEDAGGI);
    });
  }

  @Test
  void when_table_isEmpty_findByArticle_returns_emptyOptional() {

    // given

    // when

    var optionalStanziamentiParams = repository.findFirstByArticleCode("::articleCode::");

    // then

    assertThat(optionalStanziamentiParams).isEmpty();
  }

  @Test
  void when_table_isEmpty_getAll_returns_emptyList() {

    // given

    // when

    var allStanziamentiParams = repository.getAll();

    // then

    assertThat(allStanziamentiParams.getStanziamentiParams()).isEmpty();
  }

  @Test
  void when_table_isEmpty_findBySourceAndRecordCode_returns_emptyList() {

    // given

    // when
    var stanziamentiParamsList = repository.findBySourceAndRecordCode("::source::", "::recordCode::");

    // then

    assertThat(stanziamentiParamsList).isEmpty();
  }

  @Test
  void when_table_contains_oneRow_findBySourceAndRecordCode_returns_StanziamentiParamsList_with_one_element() {

    // given

    var stanziamentiParamsEntity = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                      .withArticleCode("::articleCode::")
                                                                      .withSupplierCode("::supplierCode::")
                                                                      .withTransactionType("::transactionType::")
                                                                      .withMeasurementUnit("::measurementUnit::")
                                                                      .withFareClass("::fareClass::")
                                                                      .withSource("::source::")
                                                                      .withTransactionDetail("::transactionDetail::")
                                                                      .withRecordCode("::recordCode::")
                                                                      .withVatRate(22.0f)
                                                                      .withGrouping("::grouping::")
                                                                      .build();
    em.persist(stanziamentiParamsEntity);

    // when

    var stanziamentiParamsList = repository.findBySourceAndRecordCode("::source::", "::recordCode::");

    // then

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode::")
                                                                                         .withCountry("::country::")
                                                                                         .withVatRate(22.0f)
                                                                                         .withGrouping("::grouping::")
                                                                                         .withMeasurementUnit("::measurementUnit::")
                                                                                         .build())
                                                          .withTransactionType("::transactionType::")
                                                          .withFareClass("::fareClass::")
                                                          .withSource("::source::")
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .withRecordCode("::recordCode::")
                                                          .build();
    assertThat(stanziamentiParamsList).contains(new StanziamentiParams[] { stanziamentiParams });
  }

  @Test
  void when_table_contains_oneRow_whitSourceAndRecordCode_findBySourceAndRecordCode_returns_StanziamentiParamsList_withOneElement() {

    // given

    var stanziamentiParamsEntity1 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-1::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-1::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-1::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    var stanziamentiParamsEntity2 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-2::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-2::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-2::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    var stanziamentiParamsEntity3 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-3::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-3::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-3::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    em.persist(stanziamentiParamsEntity1);
    em.persist(stanziamentiParamsEntity2);
    em.persist(stanziamentiParamsEntity3);

    // when

    var stanziamentiParamsList = repository.findBySourceAndRecordCode("::source-1::", "::recordCode-1::");

    // then

    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                          .withSupplierCode("::supplierCode::")
                                                          .withArticle(ArticleTestBuilder.newInstance()
                                                                                         .withCode("::articleCode-1::")
                                                                                         .withCountry("::country::")
                                                                                         .withVatRate(22.0f)
                                                                                         .withGrouping("::grouping::")
                                                                                         .withMeasurementUnit("::measurementUnit::")
                                                                                         .build())
                                                          .withTransactionType("::transactionType::")
                                                          .withFareClass("::fareClass::")
                                                          .withSource("::source-1::")
                                                          .withTransactionDetail("::transactionDetail::")
                                                          .withRecordCode("::recordCode-1::")
                                                          .build();
    assertThat(stanziamentiParamsList).contains(new StanziamentiParams[] { stanziamentiParams });
  }

  @Test
  void when_table_contains_moreThanOneRow_whitSourceAndRecordCode_findBySourceAndRecordCode_returns_StanziamentiParamsList() {

    // given

    var stanziamentiParamsEntity1 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-1::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-1::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-1::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    var stanziamentiParamsEntity2 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-2::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-1::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-1::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    var stanziamentiParamsEntity3 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-3::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-2::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-2::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    em.persist(stanziamentiParamsEntity1);
    em.persist(stanziamentiParamsEntity2);
    em.persist(stanziamentiParamsEntity3);

    // when

    var stanziamentiParamsList = repository.findBySourceAndRecordCode("::source-1::", "::recordCode-1::");

    // then

    var stanziamentiParams1 = StanziamentiParamsTestBuilder.newInstance()
                                                           .withSupplierCode("::supplierCode::")
                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                          .withCode("::articleCode-1::")
                                                                                          .withCountry("::country::")
                                                                                          .withVatRate(22.0f)
                                                                                          .withGrouping("::grouping::")
                                                                                          .withMeasurementUnit("::measurementUnit::")
                                                                                          .build())
                                                           .withTransactionType("::transactionType::")
                                                           .withFareClass("::fareClass::")
                                                           .withSource("::source-1::")
                                                           .withTransactionDetail("::transactionDetail::")
                                                           .withRecordCode("::recordCode-1::")
                                                           .build();
    var stanziamentiParams2 = StanziamentiParamsTestBuilder.newInstance()
                                                           .withSupplierCode("::supplierCode::")
                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                          .withCode("::articleCode-2::")
                                                                                          .withCountry("::country::")
                                                                                          .withVatRate(22.0f)
                                                                                          .withGrouping("::grouping::")
                                                                                          .withMeasurementUnit("::measurementUnit::")
                                                                                          .build())
                                                           .withTransactionType("::transactionType::")
                                                           .withFareClass("::fareClass::")
                                                           .withSource("::source-1::")
                                                           .withTransactionDetail("::transactionDetail::")
                                                           .withRecordCode("::recordCode-1::")
                                                           .build();

    assertThat(stanziamentiParamsList).contains(new StanziamentiParams[] { stanziamentiParams1, stanziamentiParams2 });
  }

  @Test
  void when_findByArticleCode_RecordCode_SupplierCode_DetailsType_TransactionDetailCode() {

    // given

    var stanziamentiParamsEntity1 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-1::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-1::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-1::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .withStanziamentiDetailsType(StanziamentiDetailsTypeEntity.CARBURANTI)
                                                                       .build();
    var stanziamentiParamsEntity2 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-2::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-1::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-1::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .withStanziamentiDetailsType(StanziamentiDetailsTypeEntity.CARBURANTI)
                                                                       .build();
    var stanziamentiParamsEntity3 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-3::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-2::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-2::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .withStanziamentiDetailsType(StanziamentiDetailsTypeEntity.CARBURANTI)
                                                                       .build();
    em.persist(stanziamentiParamsEntity1);
    em.persist(stanziamentiParamsEntity2);
    em.persist(stanziamentiParamsEntity3);

    // when

    var stanziamentiParamsOpt = repository.findByArticleAndRecordCodeAndSupplierCodeAndTransactionalDetailAndTipoDettaglio("::articleCode-1::", "::recordCode-1::", "::supplierCode::", "::transactionDetail::", StanziamentiDetailsType.CARBURANTI);

    // then

    assertThat(stanziamentiParamsOpt.isPresent()).isTrue();
    assertThat(stanziamentiParamsOpt.get().getArticle().getCode()).isEqualTo("::articleCode-1::");
  }

  @Test
  public void updateStanzimentiParams_with_null_uuid(){
    var stanziamentiParamsEntity1 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-1::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-1::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-1::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    em.persist(stanziamentiParamsEntity1);
    em.flush();
    StanziamentiParamsEntity stanziamentiParamsEntity = em.find(StanziamentiParamsEntity.class, stanziamentiParamsEntity1.getId());


    var stanziamentiParams = entityMapper.toDomain(stanziamentiParamsEntity);
    stanziamentiParams.setUuid(null);

    assertThrows(IllegalArgumentException.class,
                 ()-> repository.updateStanziamentiParams(stanziamentiParams));
  }

  @Test
  public void updateStanzimentiParams_with_new_Stanziamentiparmas(){
    var stanziamentiParams = StanziamentiParamsTestBuilder.newInstance()
                                                           .withSupplierCode("::supplierCode::")
                                                           .withArticle(ArticleTestBuilder.newInstance()
                                                                                          .withCode("::articleCode-1::")
                                                                                          .withCountry("::country::")
                                                                                          .withVatRate(22.0f)
                                                                                          .withGrouping("::grouping::")
                                                                                          .withMeasurementUnit("::measurementUnit::")
                                                                                          .build())
                                                           .withTransactionType("::transactionType::")
                                                           .withFareClass("::fareClass::")
                                                           .withSource("::source-1::")
                                                           .withTransactionDetail("::transactionDetail::")
                                                           .withRecordCode("::recordCode-1::")
                                                           .build();

    assertThrows(IllegalArgumentException.class,
                 ()-> repository.updateStanziamentiParams(stanziamentiParams));
  }

  @Test
  public void updateStanzimentiParams(){
    var stanziamentiParamsEntity1 = StanziamentiParamsEntityTestBuilder.newInstance()
                                                                       .withArticleCode("::articleCode-1::")
                                                                       .withSupplierCode("::supplierCode::")
                                                                       .withTransactionType("::transactionType::")
                                                                       .withMeasurementUnit("::measurementUnit::")
                                                                       .withFareClass("::fareClass::")
                                                                       .withSource("::source-1::")
                                                                       .withTransactionDetail("::transactionDetail::")
                                                                       .withRecordCode("::recordCode-1::")
                                                                       .withVatRate(22.0f)
                                                                       .withGrouping("::grouping::")
                                                                       .build();
    em.persist(stanziamentiParamsEntity1);
    em.flush();
    StanziamentiParamsEntity stanziamentiParamsEntity = em.find(StanziamentiParamsEntity.class, stanziamentiParamsEntity1.getId());
    stanziamentiParamsEntity.setFareClass("::NEW fareClass::");

    var stanziamentiParams = entityMapper.toDomain(stanziamentiParamsEntity);
    StanziamentiParams updatedStanziamentiParams = repository.updateStanziamentiParams(stanziamentiParams);
    assertThat(updatedStanziamentiParams.getFareClass())
      .isEqualTo("::NEW fareClass::");
  }
}
