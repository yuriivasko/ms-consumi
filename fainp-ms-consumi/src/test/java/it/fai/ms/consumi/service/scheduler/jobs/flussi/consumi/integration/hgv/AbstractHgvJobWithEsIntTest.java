package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.hgv;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.repository.NotificationSetupRepository;
import it.fai.common.notification.service.NotificationMapper;
import it.fai.common.notification.service.NotificationMessageSender;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.client.CountryDTO;
import it.fai.ms.consumi.client.CountryService;
import it.fai.ms.consumi.client.CountryServiceFactory;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici.HgvJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici.HgvRecordDescriptor;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer;
import it.fai.ms.consumi.service.navision.NavArticlesService;
import it.fai.ms.consumi.service.navision.NavSupplierCodesService;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.hgv.HgvProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.hgv.HgvRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.AbstractIntegrationTestSpringContext;
import it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti.TrackycardCarburantiStandardJobDaConsumoWithESIntTest;
import it.fai.ms.consumi.service.validator.ConsumoNoBlockingValidatorService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidatorImpl;
import it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory;

@Tag("integration")
public abstract class AbstractHgvJobWithEsIntTest
  extends AbstractIntegrationTestSpringContext {

  protected transient final Logger log = LoggerFactory.getLogger(getClass());

  protected final String                           filename;
  protected final String                           partnerCode;
  protected       ServicePartner                   servicePartner;
  protected       HgvJob                           job;
  protected       StanziamentiParamsValidator      stanziamentiParamsValidator;
  protected       HgvRecordDescriptor              descriptor;
  protected       AllineamentoDaConsumoJmsProducer allineamentoDaConsumoJmsProducer;
  protected       HgvRecordConsumoGenericoMapper   consumoMapper;

  @Autowired
  protected HgvProcessor processor;


  @Autowired
  protected StanziamentoRepository stanziamentiRepository;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Autowired
  protected StanziamentiParamsService stanziamentiParamsService;

  @Autowired
  protected RecordPersistenceService recordPersistenceService;

  @Autowired
  protected ServiceProviderRepository serviceProviderRepository;

  @Autowired
  private PlatformTransactionManager transactionManager;
  
  @Autowired
  ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService;


  public AbstractHgvJobWithEsIntTest(String filename, String partnerCode) throws URISyntaxException {
    this.filename = new File(TrackycardCarburantiStandardJobDaConsumoWithESIntTest.class.getResource(filename).toURI()).getAbsolutePath();
    this.partnerCode = partnerCode;
  }

  @BeforeEach
  public void setUp() throws Exception {

    descriptor = new HgvRecordDescriptor();

    stanziamentiParamsValidator = spy(
      new StanziamentiParamsValidatorImpl(stanziamentiParamsService, mock(NavArticlesService.class), mock(NavSupplierCodesService.class),consumoNoBlockingValidatorService,mock(ApplicationProperties.class),
                                          true));
    AllStanziamentiParams stanzaParams = stanziamentiParamsService.getAll();
    assertThat(stanzaParams).isNotNull();
    List<StanziamentiParams> stanzParamList = stanzaParams.getStanziamentiParams();
    assertThat(stanzParamList).isNotEmpty();
    var oneStanziamentiParamForTest = stanzParamList.get(0);
    log.info("StanziamentiParam in use for Tests = " + oneStanziamentiParamForTest.toString());
    doReturn(ValidationOutcomeTestFactory.newValidationOutcomeWithStanzParam(oneStanziamentiParamForTest)).when(stanziamentiParamsValidator)
                                                                                                          .checkConsistency();
    // doReturn(ValidationOutcomeTestFactory.newValidationOutcomeWithStanzParam(oneStanziamentiParamForTest)).when(stanziamentiParamsValidator).existsParamsStanziamento(any());
    // when(stanziamentiParamsValidator.checkConsistency()).thenReturn(ValidationOutcomeTestFactory.newValidationOutcome());
    NotificationService notificationService = spy(
      new NotificationService(mock(NotificationMessageSender.class), mock(NotificationSetupRepository.class),
                              mock(NotificationMapper.class), "Hgv"));

    doAnswer(invocation -> {
      EnrichedNotification enrichedNotification = invocation.getArgument(0);
      String code = enrichedNotification.getCode();
      log.info("notifying code: " + code);
      Map<String, Object> errorMapCode = enrichedNotification.transformFiledsToParameters();
      errorMapCode.entrySet().stream().forEach(entry -> log.info("key for message:" + entry.getKey() + " " + entry.getValue()));
      return null;
    }).when(notificationService).notify(any());

    // stanziamentiToNavJmsProducer = mock(StanziamentiToNavJmsProducer.class);
    consumoMapper = new HgvRecordConsumoGenericoMapper(new CountryServiceFactory(null, new ApplicationProperties()){
      @Override
      public CountryService getCountryService() {
        final CountryDTO countryDTO = new CountryDTO();
        countryDTO.setCodiceIso2("IT");
        countryDTO.setSiglaAuto("I");
        return new CountryService(Arrays.asList(countryDTO));
      }
    });

    servicePartner = findServicePartner(partnerCode);
    allineamentoDaConsumoJmsProducer = mock(AllineamentoDaConsumoJmsProducer.class);

    job = new HgvJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService,
                     descriptor, processor, consumoMapper, stanziamentiToNavJmsPublisher, allineamentoDaConsumoJmsProducer);

  }

  protected ServicePartner findServicePartner(String providerCode) {
    ServiceProvider sp = serviceProviderRepository.findByProviderCode(providerCode);
    return sp.toServicePartner();
  }

  @Test
  public void testKOStanziamentiCheckConsistency() {

    ValidationOutcome vo = new ValidationOutcome().message(new Message("001").text("this is test error!"));
    vo.setValidationOk(false);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isEmpty();
  }
}
