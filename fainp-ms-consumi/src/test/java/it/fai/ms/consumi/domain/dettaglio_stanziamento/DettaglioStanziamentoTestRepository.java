package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Validated
@Transactional
public class DettaglioStanziamentoTestRepository {

    @Autowired
    private EntityManager em;


    @SuppressWarnings("unchecked")
    public List<DettaglioStanziamentoPedaggioEntity> findAllDettaglioStanziamentiPedaggio() {
        return em.createNativeQuery("select * from dettaglio_stanziamento_pedaggi dsp inner join dettaglio_stanziamento ds on ds.id=dsp.id ", DettaglioStanziamentoPedaggioEntity.class).getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<StanziamentoEntity> findAllStanziamenti(){
      List<StanziamentoEntity> stanziamentoEntities = em.createNativeQuery("select * from stanziamento", StanziamentoEntity.class).getResultList();
      stanziamentoEntities.stream().forEach( stanziamentoEntity -> {
        System.err.println("*** stanziamentoEntity: " + stanziamentoEntity.getCode());
        stanziamentoEntity.getDettaglioStanziamenti().stream().forEach( dettaglioStanziamentoEntity -> {
          System.err.println("*** dettaglioStanziamentoEntity: " + dettaglioStanziamentoEntity.getId());
        });
      });
      return stanziamentoEntities;
    }
    
    @SuppressWarnings("unchecked")
    public List<ViewStoricoDispositivoVeicoloContratto> findAllStorico(){
        return em.createNativeQuery("select * from view_storico_dispositivo_veicolo",ViewStoricoDispositivoVeicoloContratto.class).getResultList();
    }
}
