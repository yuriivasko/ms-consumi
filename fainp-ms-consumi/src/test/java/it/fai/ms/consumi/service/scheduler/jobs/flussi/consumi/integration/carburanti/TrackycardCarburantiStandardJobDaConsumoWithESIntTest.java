package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti;

import static java.time.Month.JANUARY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;

@Tag("integration")
public class TrackycardCarburantiStandardJobDaConsumoWithESIntTest
  extends AbstractTrackycardCarburantiStandardJobWithEsIntTest {

  @Test
  public void testKOStanziamentiCheckConsistency(){
    String filename = getAbsolutePath("/test-files/trackycardcarbstd/FR05GFP20180118095935.txt.20180118103023");
    ServicePartner servicePartner = findServicePartner("FR03");

    ValidationOutcome vo = new ValidationOutcome().message(new Message("001").text("this is test error!"));
    vo.setValidationOk(false);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(vo);

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(),servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isEmpty();
  }

  @Test
  @Transactional
  public void test_DettaglioStanziamentoEntity_count() {
    String filename = getAbsolutePath("/test-files/trackycardcarbstd/FR05GFP20180118095935.txt.20180118103023");
    ServicePartner servicePartner = findServicePartner("FR03");


    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    for (Stanziamento stanziamento : stanziamentoList) {
      assertThat(stanziamento.getCosto()).isGreaterThan(BigDecimal.ZERO);
      assertThat(stanziamento.getPrezzo()).isGreaterThan(BigDecimal.ZERO);
      assertThat(stanziamento.getAmountCurrencyCode()).isEqualTo("EUR");
    }

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    assertThat(dettaglioStanziamenti)
      .isNotEmpty();

    for (DettaglioStanziamentoEntity dettaglioStanziamentoEntity : dettaglioStanziamenti) {
      DettaglioStanziamentoCarburanteEntity dettaglioStanziamentoCarburante = (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamentoEntity;

      //Testing dates
      final Instant ingestionTime = dettaglioStanziamentoCarburante.getIngestionTime();
      final Instant dataAcquisizioneFlusso = dettaglioStanziamentoCarburante.getDataAcquisizioneFlusso();

      assertThat(ingestionTime)
        .isNotNull();
      assertThat(dataAcquisizioneFlusso)
        .isNotNull();
      assertThat(ingestionTime)
        .isNotEqualTo(dataAcquisizioneFlusso);

      //from file hader 20170417
      LocalDate date = LocalDate.ofInstant(dataAcquisizioneFlusso, ZoneId.systemDefault());
      assertThat(date.getYear()).isEqualTo(2018);
      assertThat(date.getMonth()).isEqualTo(JANUARY);
      assertThat(date.getDayOfMonth()).isEqualTo(18);

      //can be broken using tests on midnight..
      date = LocalDate.ofInstant(ingestionTime, ZoneId.systemDefault());
      final LocalDate now = LocalDate.now();
      assertThat(date.getYear()).isEqualTo(now.getYear());
      assertThat(date.getMonth()).isEqualTo(now.getMonth());
      assertThat(date.getDayOfMonth()).isEqualTo(now.getDayOfMonth());

    }
    // assertThat(job.isRunning()).isEqualTo(true);
  }

}
