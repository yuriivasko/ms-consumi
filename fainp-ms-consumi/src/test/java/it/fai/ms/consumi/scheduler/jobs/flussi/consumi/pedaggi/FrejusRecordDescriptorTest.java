package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.FrejusRecord;
import it.fai.ms.consumi.service.frejus.FrejusCustomerProvider;

@Tag("unit")
class FrejusRecordDescriptorTest {

  private final FrejusCustomerProvider       customerProvider             = mock(FrejusCustomerProvider.class);
  private FrejusRecordDescriptor subject;
  private String[]              data;

  @BeforeEach
  public void setUp() {
    data = new String[3];
    data[0] = "020180904112954001036                                                                                                                                 ";
    data[1] =
"1500600713010627786120180801064231+025870021205+0000000000002200000280/TRF_1808     000011010340638032193                                             ";
    data[2] = "920180904112954001036+004550116833765000531007046370-000000000000000000000000000000                                                                   ";
    subject = new FrejusRecordDescriptor(customerProvider);
  }

  @Test
  public void getStartEndPosTotalRows() {
    // {69=75, 38=44, 54=60, 23=29}
    assertThat(subject.getStartEndPosTotalRows()).containsAllEntriesOf(Map.of(23, 28, /*38, 43,*/ 54, 59/*, 69, 74*/));
  }

  @Test
  public void getHeaderBegin() {
    assertThat(subject.getHeaderBegin()).isEqualTo("0");
  }

  @Test
  public void getFooterCodeBegin() {
    assertThat(subject.getFooterCodeBegin()).isEqualTo("9");
  }

  @Test
  public void extractRecordCode_header() {
    assertThat(subject.extractRecordCode(data[0])).isEqualTo("0");
  }

  @Test
  public void extractRecordCode_record() {
    assertThat(subject.extractRecordCode(data[1])).isEqualTo("1");
  }

  @Test
  public void extractRecordCode_footer() {
    assertThat(subject.extractRecordCode(data[2])).isEqualTo("9");
  }

  @Test
  void decodeRecordCodeAndCallSetFromString_header() {
    // non ha header!!!
    FrejusRecord record = subject.decodeRecordCodeAndCallSetFromString(data[0], subject.extractRecordCode(data[0]), "LEPTCTTR.999", -1);

    assertThat(record.getRecordCode()).isEqualTo("0");
    assertThat(record.getTransactionDetail()).isEqualTo("");
    assertThat(record.getCreationDateInFile()).isEqualTo("20180904");
    assertThat(record.getCreationTimeInFile()).isEqualTo("112954");
    assertThat(record.getNumeroCliente()).isEqualTo("001036");

  }

  @Test
  void decodeRecordCodeAndCallSetFromString_record() {
    FrejusRecord record = subject.decodeRecordCodeAndCallSetFromString(data[1], subject.extractRecordCode(data[1]), "LEPTCTTR.999", -1);

    assertThat(record.getRecordCode()).isEqualTo("1");
    assertThat(record.getTransactionDetail()).isEqualTo("");
//    assertThat(record.getIsoEmettitore()).isEqualTo("500600");
//    assertThat(record.getTipoTitolo()).isEqualTo("7");
//    assertThat(record.getNumeroGruppo()).isEqualTo("13");
//    assertThat(record.getNumeroSocieta()).isEqualTo("0106");
    assertThat(record.getNumeroTessera()).isEqualTo("500600"+"7"+"13"+"0106"+"27786"+"1");
    assertThat(record.getDataTransazione()).isEqualTo("20180801064231");
    assertThat(record.getSegnoImporto()).isEqualTo("+");
    assertThat(record.getImporto()).isEqualTo("025870");
    assertThat(record.getNumeroFattura()).isEqualTo("000280/TRF_1808");
    assertThat(record.getNumeroSocietaInGruppo()).isEqualTo("00001");
    assertThat(record.getModalitaTransito()).isEqualTo("01");
    assertThat(record.getPista()).isEqualTo("0");
    assertThat(record.getCategoriaVeicolo()).isEqualTo("3");
    assertThat(record.getClassificaECO()).isEqualTo("06");
    assertThat(record.getStazioneTransito()).isEqualTo("38032193");
    assertThat(record.getPercIva()).isEqualTo("2200");
    assertThat(record.getImportoPericoloseNoVat()).isEqualTo("000000");
    assertThat(record.getSegnoImportoPericolose()).isEqualTo("+");
    assertThat(record.getSenso()).isEqualTo("1");

  }

  @Test
  void decodeRecordCodeAndCallSetFromString_footer() {
    // non ha header!!!
    FrejusRecord record = subject.decodeRecordCodeAndCallSetFromString(data[2], subject.extractRecordCode(data[2]), "LEPTCTTR.999", -1);

    assertThat(record.getRecordCode()).isEqualTo("9");
    assertThat(record.getTransactionDetail()).isEqualTo("");
    assertThat(record.getCreationDateInFile()).isEqualTo("20180904");
    assertThat(record.getCreationTimeInFile()).isEqualTo("112954");
    assertThat(record.getNumeroCliente()).isEqualTo("001036");

    assertThat(sumPositions(data[2], subject.getStartEndPosTotalRows()
                                            .entrySet())).isEqualTo(4550L);
  }

  private static long sumPositions(String footer,
                                   Set<Entry<Integer, Integer>> startStopPositionsForRecordNumToSum) throws NumberFormatException {
    AtomicLong declaredTotalRows = new AtomicLong(0);
    startStopPositionsForRecordNumToSum.stream()
                                       .forEach(couple -> {
                                         String totalRowsDeclaredInFooterAsString = footer.substring(couple.getKey(), couple.getValue());
                                         declaredTotalRows.set(declaredTotalRows.get() + Long.parseLong(totalRowsDeclaredInFooterAsString));
                                       });
    return declaredTotalRows.get();
  }
}
