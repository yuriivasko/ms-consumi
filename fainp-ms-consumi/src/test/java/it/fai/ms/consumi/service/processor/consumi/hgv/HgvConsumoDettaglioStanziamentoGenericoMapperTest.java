package it.fai.ms.consumi.service.processor.consumi.hgv;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.Asfinag;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.AsfinagGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.asfinag.AsfinagConsumoDettaglioStanziamentoPedaggioMapper;

@Tag("unit")
class HgvConsumoDettaglioStanziamentoGenericoMapperTest {

  HgvConsumoDettaglioStanziamentoGenricoMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new HgvConsumoDettaglioStanziamentoGenricoMapper();
  }

  @Test
  void mapConsumoToDettaglioStanziamento() {
    final Hgv consumo = mock(Hgv.class);

    given(consumo.getGlobalIdentifier())
      .willReturn(new AsfinagGlobalIdentifier(""));

    given(consumo.getNavSupplierCode())
      .willReturn("");

    DettaglioStanziamento dettaglioStanziamento = mapper.mapConsumoToDettaglioStanziamento(consumo);

    //FIXME dettagliare test

    assertThat(dettaglioStanziamento)
      .isNotNull();
  }
}
