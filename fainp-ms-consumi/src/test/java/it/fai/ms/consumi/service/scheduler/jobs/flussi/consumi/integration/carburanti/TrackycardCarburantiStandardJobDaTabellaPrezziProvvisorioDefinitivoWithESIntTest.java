package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.carburanti;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;

@Tag("integration")
@ActiveProfiles(profiles= {"default"})
public class TrackycardCarburantiStandardJobDaTabellaPrezziProvvisorioDefinitivoWithESIntTest
  extends AbstractTrackycardCarburantiStandardJobWithEsIntTest {

  @Test
  @Transactional
  public void test() {
    String filename = getAbsolutePath("/test-files/trackycardcarbstd/ES00GFP20170418084530.txt.20170418091505");
    ServicePartner servicePartner = findServicePartner("ES00");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList).isNotEmpty();

    boolean costoAssert = false;
    boolean ricavoAssert = false;

    List<DettaglioStanziamentoEntity> dettstanziamentoList = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoCarburante();
    assertEquals(4, dettstanziamentoList.size());
    for (DettaglioStanziamentoEntity item : dettstanziamentoList) {
      DettaglioStanziamentoCarburanteEntity itemC = (DettaglioStanziamentoCarburanteEntity) item;
      System.out.println("########### " + itemC);

      assertThat("F000542").isEqualTo(itemC.getCodiceFornitoreNav());
      assertThat("7896970000312900954").isEqualTo(itemC.getCodiceTrackycard());
      if(itemC.getCostoUnitarioCalcolatoNoiva()==null) {
        assertNotNull(itemC.getPrezzoUnitarioCalcolatoNoiva(),"getPrezzoUnitarioCalcolatoNoiva null");
        assertNotNull(itemC.getPrezzoUnitarioDiRiferimentoNoiva(),"getPrezzoUnitarioDiRiferimentoNoiva null");
        assertEquals(InvoiceType.D, itemC.getTipoConsumo());
      }
      if(itemC.getPrezzoUnitarioCalcolatoNoiva()==null) {
        assertNotNull(itemC.getCostoUnitarioCalcolatoNoiva(),"getCostoUnitarioCalcolatoNoiva null");
        assertNotNull(itemC.getPrezzoUnitarioDiRiferimentoNoiva(),"getPrezzoUnitarioDiRiferimentoNoiva not null");
        assertEquals(InvoiceType.P, itemC.getTipoConsumo());
      }

      assertEquals("ES00000035", itemC.getPuntoErogazione());

      //itemC.getDataUtilizzo(). //TODO match date 201704180412


      //FIXME assertThat(1).isEqualTo(itemC.getStanziamenti().size());

      /*switch (itemC.getStanziamenti()
                   .iterator()
                   .next()
                   .getGeneraStanziamento()) {
      case COSTO:
        costoAssert = true;
        break;
      case RICAVO:
        ricavoAssert = true;
        break;
      default:
        assertTrue(false, "case " + itemC.getStanziamenti()
                                         .iterator()
                                         .next()
                                         .getGeneraStanziamento()
                          + " not valid");
        break;
      }*/
    }
    //assertTrue(costoAssert, "Stanziamento costo not found");
    //assertTrue(ricavoAssert, "Stanziamento ricavo not found");

    costoAssert = false;
    boolean ricavoProvvisorioAssert = false;
    boolean ricavoDefinitivoAssert = false;

    System.out.println(stanziamentoList);
    assertEquals(4, stanziamentoList.size());
    for (Stanziamento stanziamento : stanziamentoList) {
      System.out.println("*********** " + stanziamento);
      assertThat(stanziamento.getAmountCurrencyCode()).isEqualTo("EUR");

      switch (stanziamento.getGeneraStanziamento()) {
      case COSTO:
        costoAssert = true;
        assertThat(stanziamento.getCosto()).isGreaterThan(BigDecimal.ZERO);
        assertEquals(InvoiceType.P, stanziamento.getInvoiceType());
        break;
      case RICAVO:
        assertEquals(InvoiceType.D, stanziamento.getInvoiceType());
        ricavoDefinitivoAssert = true;
        assertThat(stanziamento.getPrezzo()).isGreaterThan(BigDecimal.ZERO);

        break;
      default:
        assertTrue(false, "case " + stanziamento.getGeneraStanziamento() + " not valid");
        break;
      }
    }

    assertTrue(costoAssert, "Stanziamento costo not found");
    //assertTrue(ricavoProvvisorioAssert, "Stanziamento ricavo provvisorio not found");
    assertTrue(ricavoDefinitivoAssert, "Stanziamento ricavo definitivo not found");

    // assertThat(job.isRunning()).isEqualTo(true);
  }

}
