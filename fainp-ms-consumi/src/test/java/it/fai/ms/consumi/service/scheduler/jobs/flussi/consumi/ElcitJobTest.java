package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.transaction.PlatformTransactionManager;

import io.github.glytching.junit.extension.folder.TemporaryFolder;
import io.github.glytching.junit.extension.folder.TemporaryFolderExtension;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitJob;
import it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elcit.ElcitProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElcitRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Tag("unit")
@ExtendWith(TemporaryFolderExtension.class)
public class ElcitJobTest {

  private RecordConsumoMapper<ElcitRecord, Elcit> consumoMapper;
  private ConsumoProcessor<Elcit>                 consumoProcessor;
  private ElcitDescriptor                         descriptor;
  private String                                  filename = null;
  private ElcitJob                                job;
  private RecordPersistenceService                recordPersistenceService;
  private StanziamentiParamsValidator             stanziamentiParamsValidator;
  private StanziamentiToNavPublisher            stanziamentiToNavJmsProducer;
  private final PlatformTransactionManager transactionManager   = mock(PlatformTransactionManager.class);


  @BeforeEach
  public void setUp() throws Exception {
    filename = new File(ElcitJobTest.class.getResource("/test-files/elcit/DA06A284.ELCIT.UXXXXXXX.N00002")
                                          .toURI()).getAbsolutePath();
    recordPersistenceService = mock(RecordPersistenceService.class);
    stanziamentiParamsValidator = mock(StanziamentiParamsValidator.class);
    consumoProcessor = mock(ElcitProcessor.class);
    doReturn(mock(ProcessingResult.class)).when(consumoProcessor)
                                          .validateAndProcess(any(), any());
    consumoMapper = new ElcitRecordConsumoMapper();
    descriptor = new ElcitDescriptor();
    stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);

  }

  @Test
  public void testOk() {

    final ValidationOutcome vo = new ValidationOutcome();
    vo.setValidationOk(true);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(new ValidationOutcome());
    final NotificationService notificationService = mock(NotificationService.class);
    job = new ElcitJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService, descriptor,
                       consumoProcessor, consumoMapper, stanziamentiToNavJmsProducer, "FILENAME_ASC");

    assertThat(job.getSource()).isEqualTo("ELCIT");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);

    // assertThat(job.isRunning()).isEqualTo(true);
  }

  @Test
  public void test_toOrderedList_LAST_MODIFIED(TemporaryFolder temporaryFolder) throws IOException, InterruptedException {
    job = new ElcitJob(null, null, null, null, null,
                       null, null, null, "LAST_MODIFIED");

    List<String> fileList = Arrays.asList(
      "DA06A284.ELCIT.FAI.UNO",
      "DA06A284.ELCIT.FAI.DUE",
      "DA06A284.ELCIT.FAI.TRE"
      );

    String root = temporaryFolder.getRoot().getAbsolutePath();

    LinkedList<Path> paths = new LinkedList<>();
    for (String fileName : fileList) {
      paths.add(temporaryFolder.createFile(fileName).toPath());
      //some filesystem use seconds instead of millisecond
      Thread.sleep(1001);
    }

    List<Path> orederdList = job.orderFileList(Files.newDirectoryStream(temporaryFolder.getRoot().toPath()));

    List<String> collected = orederdList.stream().map(o -> o.toString()).collect(Collectors.toList());

    assertThat(collected)
      .containsExactly(
        root + "/" +"DA06A284.ELCIT.FAI.UNO",
        root + "/" +"DA06A284.ELCIT.FAI.DUE",
        root + "/" +"DA06A284.ELCIT.FAI.TRE"
      );
  }

  @Test
  public void test_toOrderedList_FILENAME_DESC(TemporaryFolder temporaryFolder) throws IOException, InterruptedException {
    job = new ElcitJob(null, null, null, null, null,
                       null, null, null, "FILENAME_DESC");

    List<String> fileList = Arrays.asList(
      "DA06A284.ELCIT.FAI.UNO",
      "DA06A284.ELCIT.FAI.DUE",
      "DA06A284.ELCIT.FAI.TRE"
    );

    String root = temporaryFolder.getRoot().getAbsolutePath();

    LinkedList<Path> paths = new LinkedList<>();
    for (String fileName : fileList) {
      paths.add(temporaryFolder.createFile(fileName).toPath());
      //some filesystem use seconds instead of millisecond
      Thread.sleep(1001);
    }

    List<Path> orederdList = job.orderFileList(Files.newDirectoryStream(temporaryFolder.getRoot().toPath()));

    List<String> collected = orederdList.stream().map(o -> o.toString()).collect(Collectors.toList());

    assertThat(collected)
      .containsExactly(
        root + "/" +"DA06A284.ELCIT.FAI.UNO",
        root + "/" +"DA06A284.ELCIT.FAI.TRE",
        root + "/" +"DA06A284.ELCIT.FAI.DUE"
      );
  }
}
