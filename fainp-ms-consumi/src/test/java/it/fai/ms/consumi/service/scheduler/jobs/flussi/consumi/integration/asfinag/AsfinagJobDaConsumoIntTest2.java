package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration.asfinag;

import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("development")
public class AsfinagJobDaConsumoIntTest2
  extends AbstractAsfinagJobWithEsIntTest {

  final static String FILENAME = "/test-files/asfinag/LEPTCTTR.951.20181210063001";
  final static String PARTNERCODE = "AT06";


  public AsfinagJobDaConsumoIntTest2() throws URISyntaxException {
    super(FILENAME, PARTNERCODE);
  }

  @Test
  @Transactional
  public void process_sample_file_producing_stanziamenti() {

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), servicePartner);

    List<Stanziamento> stanziamentoList = stanziamentiRepository.getAll();
    assertThat(stanziamentoList.size())
      .isEqualTo(14);

    List<DettaglioStanziamentoEntity> dettaglioStanziamenti = dettaglioStanziamentoRepo.getAllDettaglioStanziamentoPedaggio();
    assertThat(dettaglioStanziamenti.size())
      .isEqualTo(14);


  }
}
