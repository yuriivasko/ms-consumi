package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;


import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Vacon;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoTestRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElceuRecord;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.*;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@Tag("integration")
public class VaconOnElceuIntTest
  extends AbstractTelepassIntTest{

  @Autowired
  private PlatformTransactionManager transactionManager;
  @Autowired
  private RecordPersistenceService recordPersistenceService;
  @Autowired
  private ConsumoProcessor<Elceu> elceuProcessor;
  @Autowired
  private RecordConsumoMapper<ElceuRecord, Elceu> elceuRecordConsumoMapper;
  private StanziamentiToNavPublisher elceuConsumoProcessor;


  private ElceuJob elceuJob;
  private VaconJob vaconJob;
//  @Autowired
//  private RecordDescriptor<VaconRecord> vaconRecordDescriptor;
  @Autowired
  private ConsumoProcessor<Vacon> vaconConsumoProcessor;
  @Autowired
  private RecordConsumoMapper<VaconRecord, Vacon> vaconRecordConsumoMapper;
  @Autowired
  private NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationService;

  @Autowired
  private DettaglioStanziamentoTestRepository dettaglioStanziamentoTestRepository;

  private ElceuDescriptor elceuDescriptor = new ElceuDescriptor();

  private VaconDescriptor vaconDescriptor = new VaconDescriptor();


  @BeforeEach
  public void setUp() throws Exception {


    super.setUpMockContext("Elvia");

    elceuJob = new ElceuJob(transactionManager,
      stanziamentiParamsValidator,
      notificationService,
      recordPersistenceService,
      elceuDescriptor,
      elceuProcessor,
      elceuRecordConsumoMapper,
      stanziamentiToNavJmsPublisher
    );

    vaconJob = new VaconJob(transactionManager,stanziamentiParamsValidator,notificationService,recordPersistenceService,
      vaconDescriptor,
      vaconConsumoProcessor,
      vaconRecordConsumoMapper,
      stanziamentiToNavJmsPublisher,
      notConfirmedTemporaryAllocationService);

  }
  @Test
  @Transactional(propagation = Propagation.NOT_SUPPORTED)
  public void test_caso2_differentAmount_should_group() throws Exception {
    String filename = new File(VaconOnElceuIntTest.class
      .getResource("/test-files/vacon/caso2_different_amounts_group/DA06A284.ELCEU.FAI.N0007_per_bug_caso2conpiutransitistessoveicolo")
      .toURI()).getAbsolutePath();
    elceuJob.process(filename,System.currentTimeMillis(), new Date().toInstant(), null);
    List<StanziamentoEntity> allStanziamenti = dettaglioStanziamentoTestRepository.findAllStanziamenti();
    assertThat(allStanziamenti)
      .extracting(s->tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getStatoStanziamento(), s.getTarga(), s.getPaese(), s.getCosto(),s.getPrezzo(), s.getClasseVeicoloEuro(),s.getValuta()))
      .containsExactlyInAnyOrder(
        tuple("0_P","0064266",null,"FRQPE01",false, InvoiceType.P,"KWI39332","FR",new BigDecimal("37.42000"),new BigDecimal("37.42000"),"04","EUR")
      );
    List<DettaglioStanziamentoPedaggioEntity> allDettagliPedaggio = dettaglioStanziamentoTestRepository.findAllDettaglioStanziamentiPedaggio();
    assertThat(allDettagliPedaggio)
      .extracting(dsp->tuple(dsp.getArticlesGroup(),dsp.getAmountIncludingVat().setScale(2, RoundingMode.HALF_DOWN),dsp.getInvoiceType()))
      .containsExactlyInAnyOrder(
        tuple("AUT_FR", new BigDecimal("26.40"),InvoiceType.P),
        tuple("AUT_FR", new BigDecimal("18.50"),InvoiceType.P)
      );

    filename = new File(VaconOnElceuIntTest.class
      .getResource("/test-files/vacon/caso2_different_amounts_group/DA06A284.VACON.FAI.N00005_bug_caso2conpiutransitistessoveicolo")
      .toURI()).getAbsolutePath();
    vaconJob.process(filename,System.currentTimeMillis(), Instant.now(),null);


    allStanziamenti = dettaglioStanziamentoTestRepository.findAllStanziamenti();
    assertThat(allStanziamenti)
      .extracting(s->tuple(s.getCode(),s.getNumeroCliente(), s.getCodiceStanziamentoProvvisorio(), s.getArticleCode(), s.isConguaglio(),  s.getStatoStanziamento(), s.getTarga(), s.getPaese(), s.getCosto(),s.getPrezzo(), s.getClasseVeicoloEuro(),s.getValuta()))
      .containsExactlyInAnyOrder(
        tuple("0_P","0064266",null,  "FRQPE01", false, InvoiceType.P,"KWI39332","FR",new BigDecimal("37.42000"),new BigDecimal("37.42000"),"04","EUR"),
        tuple("1_D","0064266","0_P",  "FRQPE01", false, InvoiceType.D,"KWI39332","FR",new BigDecimal("60.42000"),new BigDecimal("60.42000"),"04","EUR"),
        tuple("2_D","0064266","0_P", "FRQPE01", true,  InvoiceType.P,"KWI39332","FR",new BigDecimal("22.00000").multiply(new BigDecimal(-1)),new BigDecimal("22.00000").multiply(new BigDecimal(-1)),"04","EUR"),
        tuple("3_D","0064266",null, "FRQPE01", true,  InvoiceType.P,"KWI39332","FR",new BigDecimal("45.00000"),new BigDecimal("45.00000"),"04","EUR")
        );
    allDettagliPedaggio = dettaglioStanziamentoTestRepository.findAllDettaglioStanziamentiPedaggio();
    assertThat(allDettagliPedaggio)
      .extracting(dsp->tuple(dsp.getArticlesGroup(),dsp.getAmountIncludingVat().setScale(2, RoundingMode.HALF_DOWN),dsp.getInvoiceType()))
      .containsExactlyInAnyOrder(
        tuple("AUT_FR", new BigDecimal("26.40"),InvoiceType.D),
        tuple("AUT_FR", new BigDecimal("26.40").multiply(new BigDecimal(-1)),InvoiceType.D),
        tuple("AUT_FR", new BigDecimal("18.50"),InvoiceType.D),
        tuple("AUT_FR", new BigDecimal("54.00"),InvoiceType.D)
      );

//    verifyZeroInteractions(notificationService);
  }
}
