package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import it.fai.common.notification.domain.NotificationSetup;
import it.fai.common.notification.repository.NotificationSetupRepository;
import it.fai.common.notification.service.NotificationMapper;
import it.fai.common.notification.service.NotificationMessageSender;
import it.fai.ms.common.jms.notification_v2.NotificationMessage;
import org.junit.jupiter.api.AfterEach;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.flussi.record.RecordRepository;
import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.service.navision.NavArticlesService;
import it.fai.ms.consumi.service.navision.NavSupplierCodesService;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.ConsumoNoBlockingValidatorService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidatorImpl;
import it.fai.ms.consumi.service.validator.ValidationOutcomeTestFactory;

@EnableFeignClients( basePackageClasses = {ElasticSearchFeignClient.class})
public class AbstractTelepassIntTest
  extends AbstractIntegrationTestSpringContext {

  private transient final Logger log = LoggerFactory.getLogger(getClass());

  @SpyBean
  private RecordRepository recordRepo;

  @Autowired
  private ElasticSearchFeignClient esClient;

  @Autowired
  protected StanziamentiParamsService stanziamentiParamsService;

  @Autowired
  protected StanziamentoRepository stanziamentiRepository;

  @Autowired
  protected StanziamentoCodeGenerator stanziamentoCodeGenerator;

  @SpyBean
  protected NotificationService notificationService;

  @Autowired
  protected PlatformTransactionManager transactionManager;

  @Autowired
  protected RecordPersistenceService recordPersistenceService;

  @Autowired
  protected DettaglioStanziamantiRepository dettaglioStanziamentoRepo;
  
  @Autowired
  private ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService;

  protected StanziamentiParamsValidator stanziamentiParamsValidator;

  private AtomicLong sequence = new AtomicLong();



  public void setUpMockContext(String defaultSource) {
    stanziamentiParamsValidator = spy(
      new StanziamentiParamsValidatorImpl(
        stanziamentiParamsService,
        mock(NavArticlesService.class),
        mock(NavSupplierCodesService.class),
        consumoNoBlockingValidatorService,
        mock(ApplicationProperties.class),
        true)
    );

    AllStanziamentiParams stanzaParams = stanziamentiParamsService.getAll();
    assertThat(stanzaParams).isNotNull();
    List<StanziamentiParams> stanzParamList = stanzaParams.getStanziamentiParams();
    assertThat(stanzParamList).isNotEmpty();

    var oneStanziamentiParamForTest = stanzParamList.get(0);
    log.info("StanziamentiParam in use for Tests = " + oneStanziamentiParamForTest.toString());
    doReturn(ValidationOutcomeTestFactory.newValidationOutcomeWithStanzParam(oneStanziamentiParamForTest))
      .when(stanziamentiParamsValidator)
      .checkConsistency();

  }

  protected String getAbsolutePath(String relativePath) {
    try {
      return new File(AbstractTelepassIntTest.class.getResource(relativePath).toURI()).getAbsolutePath();
    } catch (URISyntaxException e) {
      throw new RuntimeException("Cannot find file in given path!", e);
    }
  }

  @AfterEach
  protected void validateEsSerialize() {
    ArgumentCaptor<BaseRecord> record = ArgumentCaptor.forClass(BaseRecord.class);

    verify(recordRepo, Mockito.atLeast(0)).persist(record.capture());
//    create(Mockito.anyLong(), Mockito.anyString(), record.capture());
    record.getAllValues()
      .forEach(r -> {
        System.out.println(r.getClass().getSimpleName()+"/"+  r.getUuid());
        var esr = esClient.getRecord(r.getClass().getSimpleName(),  r.getUuid());
        assertThat(esr.getRawdata()).isEqualToComparingFieldByFieldRecursively(r);
      });
  }

  public NotificationService getNotificationServiceMocked() {
    NotificationMessageSender notificationMessageSender = mock(NotificationMessageSender.class);
    NotificationSetupRepository notificationSetupRepository = mock(NotificationSetupRepository.class);

    NotificationMapper notificationMapper = new NotificationMapper(){


      NotificationMessage mapToNotificationMessage(String source, NotificationSetup notificationSetup, Map<String, Object> parameters) {
        return new NotificationMessage(source, "", null, Collections.emptyList());
      }

      };

    NotificationService notificationService = spy(new NotificationService(notificationMessageSender,
      notificationSetupRepository,
      notificationMapper, "Elceu"));
    doAnswer(invocation -> {
      log.info("notifying code: " + invocation.getArgument(0));
      Map<String, Object> errorMapCode = invocation.getArgument(1);
      errorMapCode.entrySet()
        .stream()
        .forEach(entry -> log.info("key for message:" + entry.getKey() + " " + entry.getValue()));
      return null;
    }).when(notificationService)
      .notify(anyString(), anyMap());
    return notificationService;
  }

}
