package it.fai.ms.consumi.service.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.util.Currency;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

@Tag("unit")
class ArticleToStanziamentiParamsMapperTest {

  ArticleToStanziamentiParamsMapper articleToStanziamentiParamsMapper = new ArticleToStanziamentiParamsMapper();

  @BeforeEach
  void setUp() {
  }

  @Test
  void toStanziamentiParams() {
    final Article navArticle = mock(Article.class);
    final Article currentArticle = new Article("::old_code::");

    final StanziamentiParams currentStanziamentiParams = mock(StanziamentiParams.class);
    given(currentStanziamentiParams.getArticle())
      .willReturn(currentArticle);

    given(navArticle.getCode())
      .willReturn("::code::");
    given(navArticle.getVatRate())
      .willReturn(22f);
    given(navArticle.getCurrency())
      .willReturn(Currency.getInstance("EUR"));
    given(navArticle.getCountry())
      .willReturn("::country::");
    given(navArticle.getGrouping())
      .willReturn("::grouping::");
    given(navArticle.getDescription())
      .willReturn("::description::");
    given(navArticle.getMeasurementUnit())
      .willReturn("::mu::");

    StanziamentiParams stanziamentiParams = articleToStanziamentiParamsMapper.toStanziamentiParams(navArticle, currentStanziamentiParams);

    assertThat(stanziamentiParams.getArticle().getVatRate())
      .isEqualTo(navArticle.getVatRate());
    assertThat(stanziamentiParams.getArticle().getCurrency())
      .isEqualTo(navArticle.getCurrency());
    assertThat(stanziamentiParams.getArticle().getCountry())
      .isEqualTo(navArticle.getCountry());
    assertThat(stanziamentiParams.getArticle().getGrouping())
      .isEqualTo(navArticle.getGrouping());
    assertThat(stanziamentiParams.getArticle().getDescription())
      .isEqualTo(navArticle.getDescription());
    assertThat(stanziamentiParams.getArticle().getMeasurementUnit())
      .isEqualTo(navArticle.getMeasurementUnit());
  }
}
