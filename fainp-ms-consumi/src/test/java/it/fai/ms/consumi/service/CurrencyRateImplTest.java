package it.fai.ms.consumi.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.CurrencyRate;
import it.fai.ms.consumi.repository.CurrencyRateRepository;
import it.fai.ms.consumi.service.dto.CurrencyRateDTO;
import it.fai.ms.consumi.service.impl.CurrencyRateServiceImpl;
import it.fai.ms.consumi.service.mapper.CurrencyRateMapper;
import it.fai.ms.consumi.service.mapper.CurrencyRateMapperImpl;

@Tag("unit")
class CurrencyRateImplTest {

  private CurrencyRateServiceImpl currencyRateServiceImpl;
  private CurrencyRateRepository  mockCurrencyRateRepository = mock(CurrencyRateRepository.class);
  private CurrencyRateMapper      mockCurrencyRateMapper     = new CurrencyRateMapperImpl();

  @BeforeEach
  void setUp() throws Exception {
    currencyRateServiceImpl = new CurrencyRateServiceImpl(mockCurrencyRateRepository, mockCurrencyRateMapper);
  }

  @Test
  void testConversionUSD2USD() throws ParseException {

    var totalAmount = Money.of(new BigDecimal(100), "USD");

    var amount = new Amount();
    amount.setAmountExcludedVat(totalAmount);

    Amount convertAmount = currencyRateServiceImpl.convertAmount(amount, Currency.getInstance("USD"), newFixedInstant());

    assertThat(100.0).isEqualTo(convertAmount.getAmountExcludedVat()
                                             .getNumber()
                                             .doubleValue());
    assertThat(convertAmount.getCurrency()
                            .getCurrencyCode()).isEqualTo("USD");
  }

  @Test
  void testConversionUSD2EUR() throws ParseException {

    var totalAmount = Money.of(new BigDecimal(100), "USD");
    Instant time = newFixedInstant();
    var amount = new Amount();
    amount.setAmountExcludedVat(totalAmount);

    CurrencyRate currencyRate = new CurrencyRate();
    currencyRate.setId(-1l);
    currencyRate.setCurrency("USD");
    currencyRate.setTime(time);
    currencyRate.setRate(new BigDecimal(0.5));
    Optional<CurrencyRate> opRate = Optional.of(currencyRate);
    
    when(mockCurrencyRateRepository.findFirstByCurrencyAndTimeLessThanEqualOrderByTimeDesc("USD",time)).thenReturn(opRate);

    Amount converted = currencyRateServiceImpl.convertAmount(amount, Currency.getInstance("EUR"), newFixedInstant());

    then(mockCurrencyRateRepository).should().findFirstByCurrencyAndTimeLessThanEqualOrderByTimeDesc("USD",time);

    assertThat(200.0).isEqualTo(converted.getAmountExcludedVat()
                                             .getNumber()
                                             .doubleValue());
    assertThat(converted.getAmountExcludedVat()
                            .getCurrency()
                            .getCurrencyCode()).isEqualTo("EUR");

  }

  private static Instant newFixedInstant() throws ParseException {
    return new SimpleDateFormat("yyyy-MM-dd").parse("2018-07-04")
                                             .toInstant();
  }
}
