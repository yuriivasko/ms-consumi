package it.fai.ms.consumi.service.validator.internal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

import java.util.Optional;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.service.ContractService;

@Tag("unit")
class ContractValidatorImplTest {

  private ContractValidator contractValidator;
  private ContractService   mockContractService = mock(ContractService.class);

  @Test
  void when_validator_isEnabled_and_ContractIsFound_exists_outcomeResponse_isTrue() {

    // given
    Contract contract = new Contract("::contractUuid::");
    contract.setCompanyCode("::conpanyCode::");
    given(mockContractService.findContractByNumber("::contractUuid::")).willReturn(Optional.of(contract));

    contractValidator = new ContractValidatorImpl(mockContractService, true);

    // when

    var validationOutcome = contractValidator.findAndSetContract(new Contract("::contractUuid::"));

    // then

    then(mockContractService).should()
                             .findContractByNumber("::contractUuid::");

    assertThat(validationOutcome.isValidationOk()).isTrue();
  }

  @Test
  void when_validator_isEnabled_and_ContractIsNotFound_exists_outcomeResponse_isFalse() {

    // given

    contractValidator = new ContractValidatorImpl(mockContractService, true);

    // when

    var validationOutcome = contractValidator.findAndSetContract(new Contract("::contractUuid::"));

    // then

    then(mockContractService).should()
                             .findContractByNumber("::contractUuid::");

    assertThat(validationOutcome.isValidationOk()).isFalse();
  }

  @Test
  void when_validator_isDisabled_exists_outcomeResponse_isTrue() {

    // given

    contractValidator = new ContractValidatorImpl(mockContractService, false);

    // when

    var validationOutcome = contractValidator.findAndSetContract(new Contract("::contractUuid::"));

    // then

    then(mockContractService).should()
                             .findContractByNumber("::contractUuid::");

    assertThat(validationOutcome.isValidationOk()).isTrue();
  }

}
