package it.fai.ms.consumi.repository.flussi.record.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.repository.flussi.record.utils.RecordUtils.NotValidFileException;

@Tag("unit")
class RecordUtilsTest {

  private static final Logger log = LoggerFactory.getLogger(RecordUtilsTest.class);

  @Test
  void testCheckFileConsistencyNoFooter() {

    String filename;
    try {
      filename = new File(RecordUtilsTest.class.getResource("/recordutils/recordsWithNoFooter.txt")
                                               .toURI()).getAbsolutePath();
      Throwable exception = assertThrows(NotValidFileException.class, () -> {
        RecordUtils.checkFileConsistency(filename, Charset.forName("ISO-8859-1"), "90", 47, 54, 2);
      });
      log.info("exception message was '" + exception.getMessage() + "'");
    } catch (NullPointerException | URISyntaxException e) {
      throw new IllegalStateException(e.getMessage() + " in test??? check test file!!!");
    }
  }

  @Test
  void testCheckFileConsistencyWrongLinesDeclared() {

    String filename;
    try {
      filename = new File(RecordUtilsTest.class.getResource("/recordutils/recordsWithWrongLinesDeclared.txt")
                                               .toURI()).getAbsolutePath();
      Throwable exception = assertThrows(NotValidFileException.class, () -> {
        RecordUtils.checkFileConsistency(filename, Charset.forName("ISO-8859-1"), "90", 47, 54, 2);
      });
      log.info("exception message was '" + exception.getMessage() + "'");
    } catch (NullPointerException | URISyntaxException e) {
      throw new IllegalStateException(e.getMessage() + " in test??? check test file!!!");
    }
  }

  @Test
  void testCheckFileConsistencyWithMoreTotalInFooter() {

    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    startEndPosTotalRows.put(37, 48);
    startEndPosTotalRows.put(64, 75);
    String filename;
    try {
      filename = new File(RecordUtilsTest.class.getResource("/recordutils/recordsWithMoreTotal.txt")
                                               .toURI()).getAbsolutePath();
      assertDoesNotThrow(() -> {
        long detailRows = RecordUtils.checkFileConsistency(filename, Charset.forName("ISO-8859-1"), "TR", startEndPosTotalRows,
                                                           2);
        assertThat(detailRows).isEqualTo(6);
      });
    } catch (NullPointerException | URISyntaxException e) {
      throw new IllegalStateException(e.getMessage() + " in test??? check test file!!!");
    }
  }

  @Test
  void testCheckFileConsistencysOkWithLastEmptyLine() {
    String filename;
    try {
      filename = new File(RecordUtilsTest.class.getResource("/recordutils/recordsOkWithLastEmptyLine.txt")
                                               .toURI()).getAbsolutePath();
      assertDoesNotThrow(() -> {
        long detailRows = RecordUtils.checkFileConsistency(filename, Charset.forName("ISO-8859-1"), "90", 47, 54, 2);
        assertThat(detailRows).isEqualTo(48);
      });
    } catch (NullPointerException | URISyntaxException e) {
      throw new IllegalStateException(e.getMessage() + " in test??? check test file!!!");
    }
  }
  
  @Test
  void testCheckFileConsistencysKOWithlastTrimmable() {
    String filename;
    try {
      filename = new File(RecordUtilsTest.class.getResource("/recordutils/recordsKOWithlastTrimmable.txt")
                                               .toURI()).getAbsolutePath();
      assertThrows(NotValidFileException.class, () -> {
        long detailRows = RecordUtils.checkFileConsistency(filename, Charset.forName("ISO-8859-1"), "90", 47, 54, 2);
        assertThat(detailRows).isEqualTo(48);
      });
    } catch (NullPointerException | URISyntaxException e) {
      throw new IllegalStateException(e.getMessage() + " in test??? check test file!!!");
    }
  }
  
  @Test
  void testCheckFileConsistencysrecordsOkWithLastTrimmableFollowedByEmpty() {
    String filename;
    try {
      filename = new File(RecordUtilsTest.class.getResource("/recordutils/recordsOkWithLastTrimmableFollowedByEmpty.txt")
                                               .toURI()).getAbsolutePath();
      assertDoesNotThrow(() -> {
        long detailRows = RecordUtils.checkFileConsistency(filename, Charset.forName("ISO-8859-1"), "90", 47, 54, 2);
        assertThat(detailRows).isEqualTo(48);
      });
    } catch (NullPointerException | URISyntaxException e) {
      throw new IllegalStateException(e.getMessage() + " in test??? check test file!!!");
    }
  }

}
