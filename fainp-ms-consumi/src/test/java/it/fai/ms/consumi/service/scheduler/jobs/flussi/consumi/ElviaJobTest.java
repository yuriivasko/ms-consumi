package it.fai.ms.consumi.service.scheduler.jobs.flussi.consumi;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.generici.telepass.ElviaGenerico;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.File;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@Tag("unit")
public class ElviaJobTest {


  private final ElviaProcessor defaultProcessor = mock(ElviaProcessor.class);
  private final ElviaRecordConsumoMapper defaultRecordConsumoMapper = mock(ElviaRecordConsumoMapper.class);

  private final ElviaGenericoProcessor consumoGenericoProcessor = mock(ElviaGenericoProcessor.class);
  private final ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper = mock(ElviaRecordConsumoGenericoMapper.class);

  private ElviaDescriptor descriptor;
  private String                                  filename;
  private ElviaJob                                job;
  private RecordPersistenceService                recordPersistenceService;
  private StanziamentiParamsValidator             stanziamentiParamsValidator;
  private StanziamentiToNavPublisher            stanziamentiToNavJmsProducer;
  private final PlatformTransactionManager transactionManager                       = mock(PlatformTransactionManager.class);
  private NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationService;

  @BeforeEach
  public void setUp() throws Exception {
    filename = new File(ElviaJobTest.class.getResource("/test-files/elvia/DA06A284.ELVIA.UXXXXXXXX.N00001")
                                          .toURI()).getAbsolutePath();
    recordPersistenceService = mock(RecordPersistenceService.class);
    stanziamentiParamsValidator = mock(StanziamentiParamsValidator.class);
    doReturn(mock(ProcessingResult.class)).when(defaultProcessor).validateAndProcess(any(), any());
    descriptor = new ElviaDescriptor();
    stanziamentiToNavJmsProducer = mock(StanziamentiToNavPublisher.class);
    notConfirmedTemporaryAllocationService = mock(NotConfirmedTemporaryAllocationsService.class);
    given(defaultRecordConsumoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(Elvia.class));

    given(recordConsumoGenericoMapper.mapRecordToConsumo(any()))
      .willReturn(mock(ElviaGenerico.class));

    given(defaultProcessor.validateAndProcess(any(), any()))
      .willReturn(mock(ProcessingResult.class));

    given(consumoGenericoProcessor.validateAndProcess(any(), any()))
      .willReturn(mock(ProcessingResult.class));
  }

  @Test
  public void test() {

    final ValidationOutcome vo = new ValidationOutcome();
    vo.setValidationOk(true);
    when(stanziamentiParamsValidator.checkConsistency()).thenReturn(new ValidationOutcome());
    final NotificationService notificationService = mock(NotificationService.class);

    job = new ElviaJob(transactionManager, stanziamentiParamsValidator, notificationService, recordPersistenceService, descriptor,
                       defaultProcessor, defaultRecordConsumoMapper, consumoGenericoProcessor, recordConsumoGenericoMapper,
                       stanziamentiToNavJmsProducer,notConfirmedTemporaryAllocationService);
    assertThat(job.getSource()).isEqualTo("ELVIA");

    job.process(filename, System.currentTimeMillis(), new Date().toInstant(), null);
    verify(notConfirmedTemporaryAllocationService).processSource(eq("elcit"),any());

  }

}
