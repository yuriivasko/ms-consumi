package it.fai.ms.consumi.service.processor.consumi.dartford;

import it.fai.ms.consumi.domain.consumi.generici.dartford.Dartford;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;
import it.fai.ms.consumi.service.record.dartford.DartfordRecordConsumoGenericoMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("unit")
class DartfordConsumoDettaglioStanziamentoGenricoMapperTest {

  private DartfordRecordConsumoGenericoMapper mapper;

  @BeforeEach
  void setup() {
    mapper = new DartfordRecordConsumoGenericoMapper();
  }

  @Test
  void testMapConsumoToDettaglioStanziamento1() throws Exception {

    String fileName = "test-file";

    Dartford consumo = createConsumo(fileName, (it) -> {
      it.setCrossingDate("24/01/2018");
      it.setTime("11:20");
      it.setDirection("Southbound");
      it.setVehicle("FC231VA");
      it.setGroupName("");
      it.setBilledOn("31/01/2018");
    });

    DartfordConsumoDettaglioStanziamentoGenricoMapper subject = new DartfordConsumoDettaglioStanziamentoGenricoMapper();
    DettaglioStanziamento dettaglioStanziamento = subject.mapConsumoToDettaglioStanziamento(consumo);

    assertThat(dettaglioStanziamento).isNotNull();

  }

  @Test
  void testMapConsumoToDettaglioStanziamento2() throws Exception {
    try {
    String fileName = "test-file";
    Dartford consumo = createConsumo(fileName, (it) -> {
      it.setCrossingDate("24/04/2018");
      it.setTime("10:55");
      it.setDirection("Northbound");
      it.setVehicle("EY164FX");
      it.setGroupName("130016451 ");
      it.setBilledOn("25/04/2018");
    });

    assertThat(consumo.getCustomer()).isNotNull();
    assertThat(consumo.getCustomer().getId()).isEqualTo("130016451");

    DartfordConsumoDettaglioStanziamentoGenricoMapper subject = new DartfordConsumoDettaglioStanziamentoGenricoMapper();
    DettaglioStanziamentoGenerico dettaglioStanziamento = (DettaglioStanziamentoGenerico) subject.mapConsumoToDettaglioStanziamento(consumo);

    assertThat(dettaglioStanziamento).isNotNull();
    assertThat(dettaglioStanziamento.getGlobalIdentifier()).isNotNull();
    assertThat(dettaglioStanziamento.getSource().getType()).isEqualTo("dartford");
    assertThat(dettaglioStanziamento.getRecordCode()).isEqualTo("DEFAULT");
    assertThat(dettaglioStanziamento.getSource().getFileName()).isEqualTo(fileName);
    assertThat(dettaglioStanziamento.getCustomer()).isNull();


    assertThat(dettaglioStanziamento.getAmount().getAmountExcludedVatBigDecimal()).isEqualTo(new BigDecimal("0.00000"));
    assertThat(dettaglioStanziamento.getAmount().getVatRateBigDecimal()).isEqualTo(BigDecimal.ZERO);
    }catch (Exception e) {
      e.printStackTrace();
      throw e;
    }
  }


  private Dartford createConsumo(String fileName, Consumer<DartfordRecord> initializer) throws Exception {
    DartfordRecord record = new DartfordRecord(fileName, 1);
    initializer.accept(record);

    var consumo = mapper.mapRecordToConsumo(record);
    consumo.setNavSupplierCode("dartford");
    return consumo;
  }

}
