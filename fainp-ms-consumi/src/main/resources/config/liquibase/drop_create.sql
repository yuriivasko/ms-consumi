use tempdb;
drop database if exists faiconsumi;
create database faiconsumi;

use faiconsumi;
delete faiconsumi.dbo.dettaglio_stanziamento_generico;
delete faiconsumi.dbo.dettaglio_stanziamento_pedaggi;
delete faiconsumi.dbo.dettaglio_stanziamento_carburanti;
delete faiconsumi.dbo.dettaglio_stanziamento_treni;
delete faiconsumi.dbo.dettaglio_stanziamento_treni_altri_importi;
delete faiconsumi.dbo.dettagliostanziamento_stanziamento;
delete faiconsumi.dbo.dettaglio_stanziamento;
delete faiconsumi.dbo.stanziamento;

delete fainotification.dbo.message_notification