package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.FrejusRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(FrejusJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class FrejusJob extends AbstractJob<FrejusRecord, Frejus> {

  public final static String QUALIFIER = "FrejusJob";

  private final transient Logger log = LoggerFactory.getLogger(getClass());


  public FrejusJob(PlatformTransactionManager transactionManager,
                   StanziamentiParamsValidator stanziamentiParamsValidator,
                   NotificationService notificationService,
                   RecordPersistenceService persistenceService,
                   RecordDescriptor<FrejusRecord> recordDescriptor,
                   ConsumoProcessor<Frejus> consumoProcessor,
                   RecordConsumoMapper<FrejusRecord, Frejus> recordConsumoMapper,
                   StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("SITAF", transactionManager, stanziamentiParamsValidator,
          notificationService, persistenceService, recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
  }

  @Override
  public Format getJobQualifier() {
    return Format.SITAF;
  }
}
