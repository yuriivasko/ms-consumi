package it.fai.ms.consumi.repository.storico_dml;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.consumi.repository.storico_dml.model.DmlEmbeddedId;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoOrdiniCliente;

@Repository
public interface StoricoOrdiniClienteRepository extends JpaRepository<StoricoOrdiniCliente, DmlEmbeddedId>,InterfaceDmlRepository<StoricoOrdiniCliente> {

  List<StoricoOrdiniCliente> findAllByCodiceCliente(String codiceCliente);

}
