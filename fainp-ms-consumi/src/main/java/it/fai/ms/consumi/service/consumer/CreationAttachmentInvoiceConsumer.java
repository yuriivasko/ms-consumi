package it.fai.ms.consumi.service.consumer;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.document.allegatifattura.AllegatiFattureDTO;
import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatiFattureRepository;
import it.fai.ms.consumi.service.fatturazione.AllegatiFattureService;
import it.fai.ms.consumi.util.AnomalyException;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_UNCOMMITTED)
public class CreationAttachmentInvoiceConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AllegatiFattureService allegatiFattureService;

  private final ViewAllegatiFattureRepository viewAllegatiFattureRepo;

  private final NotificationService notificationService;

  private StanziamentoRepository stanziamentoRepository;

  public CreationAttachmentInvoiceConsumer(final StanziamentoRepository stanziamentoRepository,
                                           final AllegatiFattureService _allegatiFattureService,
                                           final ViewAllegatiFattureRepository _viewAllegatiFattureRepo,
                                           final NotificationService _notificationService) {
    this.stanziamentoRepository = stanziamentoRepository;
    allegatiFattureService = _allegatiFattureService;
    viewAllegatiFattureRepo = _viewAllegatiFattureRepo;
    notificationService = _notificationService;
  }

  @CacheEvict(value = { "findStanziamentiParams4Allegati", "tipoSupportoFix", "pointAllegati", "networkDescription" }, allEntries = true)
  public void consume(AllegatiFattureDTO attachInvoiceDTO) {

    if (log.isInfoEnabled()) {
      logAllegatiFattura(attachInvoiceDTO);
    }

    String invoiceCode = attachInvoiceDTO.getNumeroFattura();
    Map<String, RequestsCreationAttachments> mapRaggruppamentoInvoiceCode = new HashMap<>();
    List<RequestsCreationAttachments> requestsInvoice = allegatiFattureService.findByCodiceFatturaNewTransaction(invoiceCode);
    
    // Filtro quelli che hanno già un uuid documento e quindi il documento è stato già creato in precedenza
    if (requestsInvoice != null && !requestsInvoice.isEmpty()) {
      mapRaggruppamentoInvoiceCode = requestsInvoice.stream()
                                                    .collect(toMap(RequestsCreationAttachments::getCodiceRaggruppamentoArticolo, r -> r));
    }

    Instant dataInvoice = attachInvoiceDTO.getDataFattura();
    BigDecimal importoTotale = attachInvoiceDTO.getImportoTotale();
    String lang = attachInvoiceDTO.getLingua();
    List<RaggruppamentoArticoliDTO> raggruppamentiArticoli = attachInvoiceDTO.getRaggruppamentiArticoli();

    List<String> codiciStanziamenti = raggruppamentiArticoli.parallelStream()
                                                            .map(ra -> ra.getCodiciStanziamenti())
                                                            .flatMap(code -> code.stream())
                                                            .collect(toList());

    BigDecimal totalAmountNoVat = viewAllegatiFattureRepo.findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(codiciStanziamenti);
    boolean skippedAll = true;
    if (compareValues(importoTotale, totalAmountNoVat)) {
      for (RaggruppamentoArticoliDTO raggruppamentoArticoliDTO : raggruppamentiArticoli) {
        String codeRaggruppamentoArticolo = raggruppamentoArticoliDTO.getCodice();
        
        // controllo che per il codice raggr. NON sia già stato creato il documento
        if (hasNoDocument(codeRaggruppamentoArticolo, mapRaggruppamentoInvoiceCode)) {
          skippedAll = false;
          try {
            allegatiFattureService.manageRaggruppamentoArticoli(invoiceCode, dataInvoice, raggruppamentoArticoliDTO, importoTotale, lang,
                                                                mapRaggruppamentoInvoiceCode.get(codeRaggruppamentoArticolo));
          } catch (Exception e) {
            log.error("Message Exception: ", e);
            if (StringUtils.isNotBlank(e.getMessage()) && e.getMessage()
                                                           .startsWith(AllegatiFattureService.errorNomeInstestazioneTessere)) {
              String codeClientInvoice = raggruppamentoArticoliDTO.getCodiceClienteFatturazione();
              sendAnomalyToNotFoundClienteFai(codeClientInvoice, invoiceCode, dataInvoice, e);
            } else if (StringUtils.isNotBlank(e.getMessage()) && e.getMessage()
                                                                  .startsWith(AllegatiFattureService.errorCodeRaggruppamentoArticolo)) {
              String codice = raggruppamentoArticoliDTO.getCodice();
              sendAnomalyToNotFoundCodiceRaggrArticolo(codice, invoiceCode, dataInvoice, e);
            } else if (StringUtils.isNotBlank(e.getMessage()) && e.getMessage()
                                                                  .startsWith(AllegatiFattureService.errorParamStanziamenti)) {
              String codice = raggruppamentoArticoliDTO.getCodice();
              sendAnomalyToNotFoundParamStanziamenti(codice, invoiceCode, dataInvoice, e);
            } else {
              sendAnomalyToError(invoiceCode, dataInvoice, e);
              throw e;
            }
          }
        }
      }
    } else {
      log.warn("\n\n ** ANOMALY - L'importo totale della fattura {} non corrisponde. In ingresso ho  {}, da database {} ) **\n\n",
               invoiceCode, importoTotale, totalAmountNoVat);
      sendAnomalyToTotalAmount(totalAmountNoVat, importoTotale, invoiceCode, dataInvoice);

      /**
       * Verifica dei codici stanziamento in ingresso.
       */
      Set<String> stanziamentiCheck = this.checkMissingStanziamento(codiciStanziamenti);
      if (!stanziamentiCheck.isEmpty()) {
        log.error("\n\n ** I seguenti codici stanziamento non sono presenti su db **\n\n");
        stanziamentiCheck.forEach(s -> {
          log.error("-> {}", s);
        });
      }
    }

    if (skippedAll) {
      Optional<RaggruppamentoArticoliDTO> optRaggruppamentoArticolo = raggruppamentiArticoli.stream()
                                                                                            .findFirst();
      if (optRaggruppamentoArticolo.isPresent()) {
        RaggruppamentoArticoliDTO raggruppamentoArticoliDTO = optRaggruppamentoArticolo.get();
        String codeRaggrArticolo = raggruppamentoArticoliDTO.getCodice();
        Optional<RequestsCreationAttachments> requestOpt = requestsInvoice.stream()
                                                                          .filter(r -> r.getCodiceRaggruppamentoArticolo() != null
                                                                                       && r.getCodiceRaggruppamentoArticolo()
                                                                                           .equals(codeRaggrArticolo))
                                                                          .findFirst();
        if (requestOpt.isPresent()) {
          RequestsCreationAttachments requestsCreationAttachments = requestOpt.get();
          allegatiFattureService.sendMessage(requestsCreationAttachments);

        } else {
          log.warn("Not found '{}' with codice raggruppamento: {}", RequestsCreationAttachments.class.getSimpleName(), codeRaggrArticolo);
        }
      }
    }

    log.info("Message consumed");
  }

  private boolean hasNoDocument(String key, Map<String, RequestsCreationAttachments> map) {
    return !map.containsKey(key) || StringUtils.isBlank(map.get(key).getUuidDocumento());
  }

  private boolean compareValues(BigDecimal importoTotaleNav, BigDecimal importoDettagli) {
    BigDecimal importTotaleNavScaled = importoTotaleNav.setScale(2, RoundingMode.HALF_UP);
    BigDecimal importoDettagliScaled = importoDettagli.setScale(2, RoundingMode.HALF_UP);
    return importTotaleNavScaled.compareTo(importoDettagliScaled) == 0;
  }

  private void sendAnomalyToTotalAmount(BigDecimal totalAmountNoVat, BigDecimal importoTotale, String numFattura, Instant dataFattura) {
    final HashMap<String, Object> templateParameters = new HashMap<>();
    templateParameters.put("totaleCalcolato", totalAmountNoVat.setScale(2, RoundingMode.HALF_UP));
    templateParameters.put("totaleDaNav", importoTotale.setScale(2, RoundingMode.HALF_UP));
    templateParameters.put("numFattura", numFattura);
    templateParameters.put("dataFattura", dataFattura);
    notificationService.notify("INVNAV-001", templateParameters);
  }

  private void sendAnomalyToNotFoundCodiceRaggrArticolo(String codice, String numFattura, Instant dataFattura, Exception e) {
    final HashMap<String, Object> templateParameters = new HashMap<>();
    log.warn("WARNING: ", e);
    templateParameters.put("codiceRaggruppamentoArticolo", codice);
    templateParameters.put("numFattura", numFattura);
    templateParameters.put("dataFattura", dataFattura);
    notificationService.notify("INVNAV-004", templateParameters);

  }

  private void sendAnomalyToNotFoundParamStanziamenti(String codice, String numFattura, Instant dataFattura, Exception e) {
    final HashMap<String, Object> templateParameters = new HashMap<>();
    log.warn("WARNING: Not found param stanziamenti: ", e);
    templateParameters.put("codiceRaggruppamentoArticolo", codice);
    templateParameters.put("numFattura", numFattura);
    templateParameters.put("dataFattura", dataFattura);
    notificationService.notify("INVNAV-005", templateParameters);

  }

  private void sendAnomalyToNotFoundClienteFai(String codiceClienteFatturazione, String numFattura, Instant dataFattura, Exception e) {
    final HashMap<String, Object> templateParameters = new HashMap<>();
    log.warn("WARNING Exception retrieve cliente FAI: ", e);
    templateParameters.put("codiceClienteFatturazione", codiceClienteFatturazione);
    templateParameters.put("numFattura", numFattura);
    templateParameters.put("dataFattura", dataFattura);
    notificationService.notify("INVNAV-003", templateParameters);
  }

  private void sendAnomalyToError(String numFattura, Instant dataFattura, Exception e) {
    final HashMap<String, Object> templateParameters = new HashMap<>();
    String errorCode = UUID.randomUUID()
                           .toString();
    log.error("Exception: {}", errorCode, e);
    templateParameters.put("errorCode", errorCode);
    templateParameters.put("numFattura", numFattura);
    templateParameters.put("dataFattura", dataFattura);
    if(e instanceof AnomalyException) {
      templateParameters.put("errorMessage", e.getMessage());
      notificationService.notify("INVNAV-006", templateParameters);
    }else {
      notificationService.notify("INVNAV-000", templateParameters);
    }
  }

  private void logAllegatiFattura(AllegatiFattureDTO allegatiFattureDTO) {

    StringBuilder sb = new StringBuilder();
    sb.append("\n");
    sb.append(StringUtils.center(" ALLEGATI FATTURA DTO ", 60, '#'));
    sb.append("\n");
    try {
      ObjectMapper objectMapper = new ObjectMapper();
      sb.append(objectMapper.writerWithDefaultPrettyPrinter()
                            .writeValueAsString(allegatiFattureDTO));
    } catch (Exception e) {
      sb.append(allegatiFattureDTO);
    }
    sb.append("\n");
    log.info("{}", sb.toString());
  }

  private Set<String> checkMissingStanziamento(List<String> codiciStanziamenti) {

    Set<String> stanziamentiInput = new HashSet<>(codiciStanziamenti);

    Set<String> stanziamentiOutput = this.stanziamentoRepository.findByCodiceStanziamentoIn(new HashSet<>(codiciStanziamenti));
    /**
     * Sottraggo ai codici stanziamenti in input quelli che mi ritornano dalla query. Se avro' una lista vuota tutto
     * sara' OK. Se invece rimane qualcosa vuol dire che sul DB mancano dei dati
     **/
    stanziamentiInput.removeAll(stanziamentiOutput);

    return stanziamentiInput;

  }

}
