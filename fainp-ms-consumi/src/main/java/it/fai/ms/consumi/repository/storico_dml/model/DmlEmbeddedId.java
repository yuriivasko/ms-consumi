package it.fai.ms.consumi.repository.storico_dml.model;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Embeddable;

@Embeddable
public class DmlEmbeddedId implements Serializable {
  private static final long serialVersionUID = 4079308285240672942L;

  private String dmlUniqueIdentifier;
  
  private Instant dmlRevisionTimestamp;
  
    public DmlEmbeddedId() {
    }

    public DmlEmbeddedId(String dmlUniqueIdentifier, Instant dmlRevisionTimestamp) {
        this.dmlUniqueIdentifier = dmlUniqueIdentifier;
        this.dmlRevisionTimestamp = dmlRevisionTimestamp;
    }

    public String getDmlUniqueIdentifier() {
      return dmlUniqueIdentifier;
    }

    public void setDmlUniqueIdentifier(String dmlUniqueIdentifier) {
      this.dmlUniqueIdentifier = dmlUniqueIdentifier;
    }

    public Instant getDmlRevisionTimestamp() {
      return dmlRevisionTimestamp;
    }

    public void setDmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
      this.dmlRevisionTimestamp = dmlRevisionTimestamp;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((dmlRevisionTimestamp == null) ? 0 : dmlRevisionTimestamp.hashCode());
      result = prime * result + ((dmlUniqueIdentifier == null) ? 0 : dmlUniqueIdentifier.hashCode());
      return result;
    }

    @Override
    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      DmlEmbeddedId other = (DmlEmbeddedId) obj;
      if (dmlRevisionTimestamp == null) {
        if (other.dmlRevisionTimestamp != null)
          return false;
      } else if (!dmlRevisionTimestamp.equals(other.dmlRevisionTimestamp))
        return false;
      if (dmlUniqueIdentifier == null) {
        if (other.dmlUniqueIdentifier != null)
          return false;
      } else if (!dmlUniqueIdentifier.equals(other.dmlUniqueIdentifier))
        return false;
      return true;
    }

    
}