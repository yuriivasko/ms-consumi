package it.fai.ms.consumi.repository.stanziamento.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.MappedDettaglioStanziamentoEntity;

@MappedSuperclass
public abstract class MappedStanziamentoEntity {

  @Column(name = "nr_articolo")
  private String articleCode;

  @Id
  @Column(name = "codice_stanziamento", unique = true, length = 60)
  private String code;

  @Column(name = "codice_cliente_fatturazione")
  private String codiceClienteFatturazione;

  @Column(name = "codicestanziamento_provvisorio", length = 60)
  private String codiceStanziamentoProvvisorio;

  @Column(name = "conguaglio")
  private boolean conguaglio = false;

  @Column(name = "costo" , precision = 16, scale = 5)
  private BigDecimal costo;

  @Column(name = "valuta")
  private String currency;

  @Column(name = "data_erogazione_servizio")
  private LocalDate dataErogazioneServizio;

  @Column(name = "data_acquisizione_flusso")
  private Instant dataAcquisizioneFlusso;

  @Column(name = "data_fattura")
  private Instant dataFattura;

  @Column(name = "data_registrazione")
  private Instant dataRegistrazione;

  @Column(name = "data_invio_nav")
  private Instant dateNavSent;

  @Column(name = "data_accodato_nav")
  private Instant dateQueuing;

  @Column(name = "desc_1")
  private String description1 = "";

  @Column(name = "desc_2")
  private String description2 = "";

  @Column(name = "desc_3")
  private String description3 = "";

  @Column(name = "genera_stanziamento")
  private GeneraStanziamento generaStanziamento;

  @Column(name = "stato_stanziamento")
  @Enumerated(EnumType.STRING)
  private InvoiceType invoiceType;

  @Column(name = "nr_cliente")
  private String numeroCliente;

  @Column(name = "numero_fattura")
  private String numeroFattura;

  @Column(name = "nr_fornitore")
  private String numeroFornitore;

  @Column(name = "paese")
  private String paese;

  @Column(name = "prezzo", precision = 16, scale = 5)
  private BigDecimal prezzo;

  @Column(name = "provvigioni_aquisite")
  private int provvigioniAquisite = 0;

  @Column(name = "quantita")
  private BigDecimal quantity;

  @Column(name = "storno")
  private String storno;

  @Column(name = "documento_da_fornitore")
  private String supplierDocument;

  @Column(name = "targa")
  private String targa;

  @Column(name = "nazione_targa")
  private String nazioneTarga;

  @Column(name = "tipo_flusso")
  private TypeFlow tipoFlusso;

  @Column(name = "nr_")
  private int totalAllocationDetails = 1;

  @Column(name = "classe_veicolo_euro")
  private String vehicleEuroClass;

  @Column(name = "anno_stanziamento")
  private Integer year;

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final MappedStanziamentoEntity obj = getClass().cast(_obj);
      res = Objects.equals(obj.code, code);
    }
    return res;
  }

  public Integer getAnnoStanziamento() {
    return year;
  }

  public String getArticleCode() {
    return articleCode;
  }

  public String getCode() {
    return code;
  }

  public String getCodiceClienteFatturazione() {
    return codiceClienteFatturazione;
  }

  public String getCodiceStanziamentoProvvisorio() {
    return codiceStanziamentoProvvisorio;
  }

  public BigDecimal getCosto() {
    return costo;
  }

  public LocalDate getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public Instant getDataFattura() {
    return dataFattura;
  }

  public Instant getDataRegistrazione() {
    return dataRegistrazione;
  }

  public Instant getDateQueuing() {
    return dateQueuing;
  }

  public String getDescription1() {
    return description1;
  }

  public String getDescription2() {
    return description2;
  }

  public String getDescription3() {
    return description3;
  }

  public abstract <Y extends MappedDettaglioStanziamentoEntity> Set<Y> getDettaglioStanziamenti() ;

  public GeneraStanziamento getGeneraStanziamento() {
    return generaStanziamento;
  }

  public Instant getNavSentDate() {
    return dateNavSent;
  }

  public String getNumeroCliente() {
    return numeroCliente;
  }

  public String getNumeroFattura() {
    return numeroFattura;
  }

  public String getNumeroFornitore() {
    return numeroFornitore;
  }

  public String getPaese() {
    return paese;
  }

  public BigDecimal getPrezzo() {
    return prezzo;
  }

  public int getProvvigioniAquisite() {
    return provvigioniAquisite;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public InvoiceType getStatoStanziamento() {
    return invoiceType;
  }

  public String getStorno() {
    return storno;
  }

  public String getSupplierDocument() {
    return supplierDocument;
  }

  public String getTarga() {
    return targa;
  }

  public TypeFlow getTipoFlusso() {
    return tipoFlusso;
  }

  public int getTotalAllocationDetails() {
    return totalAllocationDetails;
  }

  public String getCurrency() {
    return currency;
  }

  public String getVehicleEuroClass() {
    return vehicleEuroClass;
  }

  public boolean isConguaglio() {
    return conguaglio;
  }

  public Instant getDataAcquisizioneFlusso() {
    return dataAcquisizioneFlusso;
  }

  public void setDataAcquisizioneFlusso(Instant dataAcquisizioneFlusso) {
    this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
  }

  public String getNazioneTarga() {
    return nazioneTarga;
  }

  public void setNazioneTarga(String nazioneTarga) {
    this.nazioneTarga = nazioneTarga;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("code=");
    builder.append(code);
    builder.append(", tot dettagliStanziamenti=");
    builder.append(getDettaglioStanziamenti().size());
    builder.append(",invoiceType=");
    builder.append(invoiceType);
    builder.append(", tipoFlusso=");
    builder.append(tipoFlusso);
    builder.append(", dataErogazioneServizio=");
    builder.append(dataErogazioneServizio);
    builder.append(", dataAcquisizioneFlusso=");
    builder.append(dataAcquisizioneFlusso);
    builder.append(", numeroFornitore=");
    builder.append(numeroFornitore);
    builder.append(", numeroCliente=");
    builder.append(numeroCliente);
    builder.append(", articleCode=");
    builder.append(articleCode);
    builder.append(", paese=");
    builder.append(paese);
    builder.append(", year=");
    builder.append(year);
    builder.append(", targa=");
    builder.append(targa);
    builder.append(", nazioneTarga=");
    builder.append(nazioneTarga);
    builder.append(", vehicleEuroClass=");
    builder.append(vehicleEuroClass);
    builder.append(", dataRegistrazione=");
    builder.append(dataRegistrazione);
    builder.append(", prezzo=");
    builder.append(prezzo);
    builder.append(", costo=");
    builder.append(costo);
    builder.append(", quantity=");
    builder.append(quantity);
    builder.append(", totalAllocationDetails=");
    builder.append(totalAllocationDetails);
    builder.append(", currency=");
    builder.append(currency);
    builder.append(", descriptions=");
    builder.append(description1)
           .append(",")
           .append(description2)
           .append(",")
           .append(description3);
    builder.append(", generaStanziamento=");
    builder.append(generaStanziamento);
    builder.append(", storno=");
    builder.append(storno);
    builder.append(", provvigioniAquisite=");
    builder.append(provvigioniAquisite);
    builder.append(", conguaglio=");
    builder.append(conguaglio);
    builder.append(", codiceStanziamentoProvvisorio=");
    builder.append(codiceStanziamentoProvvisorio);
    builder.append(", supplierDocument=");
    builder.append(supplierDocument);
    builder.append(", numeroFattura=");
    builder.append(numeroFattura);
    builder.append(", dataFattura=");
    builder.append(dataFattura);
    builder.append(", codiceClienteFatturazione=");
    builder.append(codiceClienteFatturazione);
    return builder.toString();
  }

}
