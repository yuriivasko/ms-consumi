package it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class TessereSanBernardoGlobalIdentifier extends GlobalIdentifier {
    public TessereSanBernardoGlobalIdentifier(GlobalIdentifierType _type) {
        super(_type);
    }

    public TessereSanBernardoGlobalIdentifier(String _id, GlobalIdentifierType _type) {
        super(_id, _type);
    }

    public TessereSanBernardoGlobalIdentifier(String _id) {
        super(_id, GlobalIdentifierType.EXTERNAL);
    }

    @Override
    public GlobalIdentifier newGlobalIdentifier() {
        throw new IllegalStateException("newGlobalIdentifier operation is supported only for global identifier type internal");
    }
}
