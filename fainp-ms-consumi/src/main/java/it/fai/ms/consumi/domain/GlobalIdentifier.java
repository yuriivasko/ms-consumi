package it.fai.ms.consumi.domain;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public abstract class GlobalIdentifier {

  public static String decodeId(final String _encodedGlobalIdentifier) {
    return _encodedGlobalIdentifier.split("#")[0];
  }

  public static GlobalIdentifierType decodeType(final String _encodedGlobalIdentifier) {
    return GlobalIdentifierType.valueOf(_encodedGlobalIdentifier.split("#")[1]);
  }

  private final String               id;
  private final GlobalIdentifierType type;
  private final Optional<UUID>     dbId;
  

  public GlobalIdentifier(final GlobalIdentifierType _type) {
    this(UUID.randomUUID()
             .toString(),
         _type,
         Optional.empty());
  }

  public GlobalIdentifier(final String _id, final GlobalIdentifierType _type) {
    this(_id,
     _type,
     Optional.empty());
  }
                          
  public GlobalIdentifier(final String _id, final GlobalIdentifierType _type, Optional<UUID>  dbId) {
    id = Objects.requireNonNull(_id, "Parameter global identifier id is mandatory");
    type = Objects.requireNonNull(_type, "Parameter global identifier type is mandatory");
    this.dbId = dbId;
  }

  public String encode() {
    return getId() + "#" + type.name();
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    try {
      res = Objects.equals(((GlobalIdentifier) _obj).getId(), getId());
    } catch (@SuppressWarnings("unused") final ClassCastException _e) {
      res = false;
    }
    return res;
  }

  public String getId() {
    return id;
  }
  
  public Optional<UUID> getDbId() {
    return dbId;
  }

  public GlobalIdentifierType getType() {
    return type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  public abstract GlobalIdentifier newGlobalIdentifier();

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append(getId())
                              .append(",")
                              .append(type)
                              .append(",")
                              .append(dbId)
                              .append("]")
                              .toString();
  }

}
