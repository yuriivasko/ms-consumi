package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

public enum StanziamentiDetailsTypeEntity {

  PEDAGGI, CARBURANTI, TRENI, GENERICO;

}
