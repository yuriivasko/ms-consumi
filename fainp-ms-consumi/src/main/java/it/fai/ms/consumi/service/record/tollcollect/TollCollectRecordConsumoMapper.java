package it.fai.ms.consumi.service.record.tollcollect;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.AmountBuilder;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollectGlobalIdentifier;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsQuery;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TollCollectRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsNotFoundException;

@Component
public class TollCollectRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<TollCollectRecord, TollCollect> {

  private static final String DEFAULT_RECORD_CODE = "DEFAULT";
  /*private final StanziamentiParamsService stanziamentiParamsService;


  public TollCollectRecordConsumoMapper(StanziamentiParamsService stanziamentiParamsService) {
    this.stanziamentiParamsService = stanziamentiParamsService;
  }*/

  @Override
  public TollCollect mapRecordToConsumo(TollCollectRecord commonRecord) throws Exception {
    String sourceType = "TOLL_COLLECT";
    Instant supplierServiceDate = new SimpleDateFormat("dd.MM.yyyy").parse(commonRecord.getSupplierServiceDate()).toInstant();
    //non c'e' la data nel record
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = supplierServiceDate;

    Source source = new Source(ingestionTime, acquisitionDate, sourceType);
    source.setFileName(commonRecord.getFileName());
    source.setRowNumber(commonRecord.getRowNumber());
    TollCollect consumo = new TollCollect(source, commonRecord.getRecordCode(),
      Optional.of(new TollCollectGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);
    consumo.setContract(Optional.empty());
    //transaction
    Transaction transaction = new Transaction();
    consumo.setTransaction(transaction);

    //device - Questo flusso va per TARGA - NON inserire nè PAN nè SERIALE nell'oggetto device
    Device device = new Device(TipoDispositivoEnum.TOLL_COLLECT);
    device.setServiceType(TipoServizioEnum.PEDAGGI_GERMANIA);
    consumo.setDevice(device);

    //amount
    //if the record contains the vat rate then the amounts are not calculated
    //otherwise the vat rate is taken from parametri_stanziamenti and the net amount is calculated
    //using the aforementioned vat rate and the gross amount
    BigDecimal vatRate = null;
    MonetaryAmount amountIncludedVat = null;
    MonetaryAmount amountExcludedVat = MonetaryUtils.strToMonetary(commonRecord.getNetAmountSC(), "EUR");
//    if(StringUtils.isNotBlank(commonRecord.getVatPercentage())) {
//      vatRate = new BigDecimal(commonRecord.getVatPercentage());
//      amountIncludedVat=MonetaryUtils.strToMonetary(commonRecord.getGrossAmountSC(), "EUR");
//    }else{
//      StanziamentiParamsQuery query = new StanziamentiParamsQuery(sourceType, DEFAULT_RECORD_CODE);
//      StanziamentiParams stanziamentiParams = stanziamentiParamsService.findByQuery(query).orElseThrow(()->new StanziamentiParamsNotFoundException("Source: "+sourceType+" and record code "+DEFAULT_RECORD_CODE+" not found"));
//      vatRate=new BigDecimal(stanziamentiParams.getArticle().getVatRate());
//    }

    Amount amount = AmountBuilder.builder()
      .amountExcludedVat(amountExcludedVat)
      .exchangeRate(null)
      .vatRate(null)
      .build();
    consumo.setAmount(amount);

    //vehicle
    Vehicle vehicle = new Vehicle();
    VehicleLicensePlate licencePlate = new VehicleLicensePlate(
      commonRecord.getRegistrationNumber(),
      commonRecord.getRegistrationCountry());
    vehicle.setLicensePlate(licencePlate);
    consumo.setVehicle(vehicle);
    //tipo consumo by default is D
    TollPoint tp = new TollPoint();

    tp.setTime(supplierServiceDate);
    consumo.setTollPointExit(tp);
    return consumo;
  }

}
