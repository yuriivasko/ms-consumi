package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.generico;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Currency;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoProcessor;
import it.fai.ms.consumi.service.processor.stanziamenti.generico.StanziamentoProcessorGenericoImpl;

@Service
public class DettaglioStanziamentoGenericoProcessorImpl
  implements DettaglioStanziamentoGenericoProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamantiRepository dettaglioStanziamantiRepository;
  private final StanziamentoProcessor           stanziamentoProcessor;

  private final CurrencyRateService currencyRateService;

  @Inject
  public DettaglioStanziamentoGenericoProcessorImpl(DettaglioStanziamantiRepository dettaglioStanziamantiRepository,
                                                    StanziamentoProcessorGenericoImpl stanziamentoProcessor,
                                                    CurrencyRateService _currencyRateService) {
    this.dettaglioStanziamantiRepository = dettaglioStanziamantiRepository;
    this.stanziamentoProcessor = stanziamentoProcessor;
    currencyRateService = _currencyRateService;
  }

  @Override
  public List<Stanziamento> produce(final DettaglioStanziamentoGenerico _dettaglioStanziamentoGenerico,
                                    final StanziamentiParams _stanziamentiParams) {

    _log.info("{} Processing start {}", _dettaglioStanziamentoGenerico, _stanziamentiParams);

    // calcolo costo, ricavo, definitivo provvisorio
    final var dettagliStanziamanti = process(_dettaglioStanziamentoGenerico, _stanziamentiParams);

    _log.debug("Creato/i {} dettagli stanziamento generico", dettagliStanziamanti.size());

    List<Stanziamento> stanziamentiAll = new ArrayList<>();
    for ( DettaglioStanziamentoGenerico dettaglio : dettagliStanziamanti) {
      stanziamentiAll.addAll(generateStanziamentoAndSaveDettaglio(dettaglio, _stanziamentiParams));
    }

    return stanziamentiAll;
  }

  private List<DettaglioStanziamentoGenerico> process(DettaglioStanziamentoGenerico _dettaglioStanziamentoGenerico,
                                                      StanziamentiParams _stanziamentiParams) {

    Currency targetCurrency = _stanziamentiParams.getArticle().getCurrency();

    Amount amount = _dettaglioStanziamentoGenerico.getAmount();
    amount = currencyRateService.convertAmount(amount, targetCurrency, _dettaglioStanziamentoGenerico.getDate());
    _dettaglioStanziamentoGenerico.setAmount(amount);

    _dettaglioStanziamentoGenerico.setInvoiceType(InvoiceType.D); // definitivo

    return Arrays.asList(_dettaglioStanziamentoGenerico);
  }

  public List<Stanziamento> generateStanziamentoAndSaveDettaglio(DettaglioStanziamentoGenerico dettaglio, final StanziamentiParams _stanziamentiParams){
    List<Stanziamento> stanziamenti = stanziamentoProcessor.processStanziamento(dettaglio, _stanziamentiParams);
    dettaglio.getStanziamenti().addAll(stanziamenti);
    dettaglioStanziamantiRepository.save(dettaglio);
    return stanziamenti;
  }




}
