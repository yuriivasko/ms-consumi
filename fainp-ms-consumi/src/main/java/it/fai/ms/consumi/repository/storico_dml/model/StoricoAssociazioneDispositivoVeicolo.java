package it.fai.ms.consumi.repository.storico_dml.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "storico_associazione_dv")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoAssociazioneDispositivoVeicolo extends DmlBaseEntity {
  private static final long serialVersionUID = 4514211889468093681L;

  @Size(max = 255)
  @Column(name = "veicolo_dml_unique_identifier", length = 255)
  private String veicoloDmlUniqueIdentifier;

  @Column(name = "data_associazione")
  private Instant dataAssociazione;

  @Transient
  public String getDispositivoDmlUniqueIdentifier() {
    return getDmlUniqueIdentifier();
  }

  public String getVeicoloDmlUniqueIdentifier() {
    return veicoloDmlUniqueIdentifier;
  }

  public void setVeicoloDmlUniqueIdentifier(String veicoloDmlUniqueIdentifier) {
    this.veicoloDmlUniqueIdentifier = veicoloDmlUniqueIdentifier;
  }

  public Instant getDataAssociazione() {
    return dataAssociazione;
  }

  public void setDataAssociazione(Instant dataAssociazione) {
    this.dataAssociazione = dataAssociazione;
  }

  @Override
  public String toString() {
    return "StoricoAssociazioneDispositivoVeicolo ["
           + ", veicoloDmlUniqueIdentifier=" + veicoloDmlUniqueIdentifier + ", dataAssocizione=" + dataAssociazione + "]";
  }
}