package it.fai.ms.consumi.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import feign.Headers;
import it.fai.ms.consumi.domain.navision.PricesByNavDTO;
import it.fai.ms.consumi.domain.navision.PricesByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.PutAllegatiByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.Stanziamento;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2018-02-02T10:20:24.106+01:00")
@FeignClient(name = "navcrm", url = "${application.restclient.url}", path = "/exage")
public interface StanziamentiApiAuthClient {

  final static String REQUEST_HEADER = "Authorization";

  
  @PostMapping("/api/Stanziamenti")
  @Headers({ "Content-Type: application/json", "Accept: application/json", })
  void stanziamentiPost(@RequestHeader(REQUEST_HEADER) String authorizationToken, List<Stanziamento> righe) throws StanziamentiPostException;

  @PutMapping("/Api/PutAllegatiByNav/{numeroFattura}")
  PutAllegatiByNavReturnDTO putAllegatiByNav(@RequestHeader(REQUEST_HEADER) String requestHeader,
                                                @PathVariable("numeroFattura") String numeroFattura, @RequestBody List<String> codiciAllegati);

  @PostMapping("/api/GetPricesByNav")
  PricesByNavReturnDTO getPricesByNavPost(@RequestHeader(REQUEST_HEADER) String requestHeader, PricesByNavDTO pricesByNavDTO);

}
