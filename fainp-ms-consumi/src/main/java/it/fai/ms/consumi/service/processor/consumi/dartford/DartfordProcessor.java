package it.fai.ms.consumi.service.processor.consumi.dartford;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.generici.dartford.Dartford;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.generico.DettaglioStanziamentoGenericoProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class DartfordProcessor
  extends ConsumoAbstractProcessor implements ConsumoProcessor<Dartford> {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoGenericoProcessor                  dettaglioStanziamentoGenericoProcessor;
  private final DartfordConsumoDettaglioStanziamentoGenricoMapper mapper;

  @Inject
  public DartfordProcessor(final ConsumoValidatorService _consumoValidatorService, final DettaglioStanziamentoGenericoProcessor _stanziamentiProducer,
                          final DartfordConsumoDettaglioStanziamentoGenricoMapper _mapper, 
                          final VehicleService _vehicleService, final CustomerService _customerService) {
    super(_consumoValidatorService, _vehicleService, _customerService);
    dettaglioStanziamentoGenericoProcessor = _stanziamentiProducer;
    mapper = _mapper;
  }

  @Override
  public ProcessingResult validateAndProcess(final Dartford consumo, final Bucket _bucket) {
    return executeProcess(consumo, _bucket);
  }

  private ProcessingResult executeProcess(final Consumo _consumo, final Bucket _bucket) {

    _log.info(">");
    _log.info("> processing start {}", _bucket.getFileName());
    _log.info("  --> {}", _consumo);

    //TODO: Recupera Contratto
    //TODO: Recupera Veicolo? verificare cosa bisogna validare e se i dati del veicolo sono necessari


    var processingResult = validateAndNotify(_consumo);

    _log.debug(" - {} processing validation completed", _consumo);

    final var consumo = processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
                      .ifPresent(stanziamentiParams -> {
                        final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
                        final var stanziamenti = dettaglioStanziamentoGenericoProcessor.produce((DettaglioStanziamentoGenerico) dettaglioStanziamento, stanziamentiParams);
                        processingResult.getStanziamenti().addAll(stanziamenti);
                      });
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }

    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", _consumo.getSource());
    _log.info("<");

    return processingResult;
  }

  private DettaglioStanziamento mapConsumoToDettaglioStanziamento(final Consumo _consumo) {
    return mapConsumoToDettaglioStanziamento(_consumo, mapper);
  }

}
