package it.fai.ms.consumi.service.record.asfinag;

import java.time.Instant;
import java.util.Optional;

import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.Asfinag;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.AsfinagGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.AsfinagRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class AsfinagRecordConsumoMapper
  extends AbstractRecordConsumoMapper implements RecordConsumoMapper<AsfinagRecord, Asfinag> {

  @Override
  public Asfinag mapRecordToConsumo(AsfinagRecord commonRecord) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyyMMdd_dateformat, HHmm_timeformat);
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, "ASFINAG");
    source.setFileName(commonRecord.getFileName());

    source.setRowNumber(commonRecord.getRowNumber());
    Asfinag toConsumo = new Asfinag(source, commonRecord.getRecordCode(),
                                Optional.of(new AsfinagGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);

    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();
    transaction.setDetailCode(commonRecord.getTransactionDetail());
    toConsumo.setTransaction(transaction);

    // CONSUMOPEDAGGIO.DEVICE
    final String pan = commonRecord.getNumeroCarta();
    Device device = new Device(TipoDispositivoEnum.GO_BOX);
    device.setPan(pan);
    device.setServiceType(TipoServizioEnum.PEDAGGI_AUSTRIA);

    toConsumo.setDevice(device);

    // CONSUMO.AMOUNT
    Amount amount = new Amount();
    amount.setVatRate(commonRecord.getVatPercRate()); // deve stare prima del setAmountExcludedVat
    amount.setAmountExcludedVat(commonRecord.getAmountExcludedVat());
    amount.setExchangeRate(null);
    toConsumo.setAmount(amount);

    transaction.setSign(commonRecord.getTransactionSign());

    //tipo definitivo settato di default nel implementazione del consumo


    // CONSUMOPEDAGGIO.TOLLPOINTEXIT
    TollPoint tollpointExit = new TollPoint();
    tollpointExit.setTime(calculateInstantByDateAndTime(commonRecord.getDataConsegna(), commonRecord.getTempoConsegna(), yyyyMMdd_dateformat, HHmm_timeformat).get());
    toConsumo.setTollPointExit(tollpointExit);

    ConsumoPedaggio toConsumoPedaggio = toConsumo;
    toConsumoPedaggio.setProcessed(commonRecord.isProcessed());

    toConsumo.setAdditionalInfo(commonRecord.getNumeroRicevuta());

    return toConsumo;
  }


}
