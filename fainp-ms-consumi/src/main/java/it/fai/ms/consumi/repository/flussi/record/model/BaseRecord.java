package it.fai.ms.consumi.repository.flussi.record.model;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
@JsonTypeInfo(use=JsonTypeInfo.Id.MINIMAL_CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public abstract class BaseRecord implements Serializable {

  @JsonIgnore
  protected Logger logger = LoggerFactory.getLogger(getClass());

  private static final long serialVersionUID = -7900905785578796700L;

  @Valid
  protected long id = -1;

  private String uuid = UUID.randomUUID().toString();

  protected BaseRecord parentRecord;

  public long getId() {
    return id;
  }

  public String getUuid() {
    return uuid;
  }

  private final static String[] ignore_fields = new String[] { "setCreatedAt", "setUpdatedAt", "setProperties", "setId", "setHistory" };

  public void setProperties(Object obj) {
    @SuppressWarnings("rawtypes")
    Class clazz = this.getClass();
    @SuppressWarnings("rawtypes")
    Class claww = obj.getClass();
    for (Method method : clazz.getMethods()) {
      try {
        if (method.getName()
                  .startsWith("set")) {
          boolean flag = false;
          for (int i = 0; i < ignore_fields.length; i++) {
            if (method.getName()
                      .equalsIgnoreCase(ignore_fields[i])) {
              flag = true;
              break;
            }
          }

          if (!flag) {
            String getMethod = "get" + method.getName()
                                             .substring(3);
            @SuppressWarnings("unchecked")
            Method m = claww.getMethod(getMethod);

            if (m != null) {
              Object getValue = m.invoke(obj);
              // logger.info(method.getName() + " " + getValue);
              if (getValue != null) {
                method.invoke(this, getValue);
              }
              // logger.info(method.getName());
            }
          }
        }
      } catch (Exception e) {
        logger.warn("Method " + method.getName() + " failed");
        e.printStackTrace();
        return;
      }
    }

  }

  public int getNestedLevel() {
    return 0;
  }

  @JsonIgnore
  public boolean toBeSkipped() {
    return false;
  }

  public BaseRecord getParentRecord() {
    return parentRecord;
  }

  public void setParentRecord(BaseRecord parentRecord) {
    this.parentRecord = parentRecord;
  }

}
