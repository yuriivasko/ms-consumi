package it.fai.ms.consumi.domain.consumi;

public interface FareClassSupplier {

  String getFareClass();

}
