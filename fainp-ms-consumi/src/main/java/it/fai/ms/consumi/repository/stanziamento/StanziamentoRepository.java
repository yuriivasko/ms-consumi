package it.fai.ms.consumi.repository.stanziamento;

import java.time.Instant;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoLogicKey;

public interface StanziamentoRepository {

  StanziamentoEntity cloneAndModify(@NotNull StanziamentoEntity allocation, InvoiceType invoiceType);

  Set<String> findByCodiceStanziamentoIn(Set<String> codes);

  Stanziamento findByCodiceStanziamento(String code);
  
  Stanziamento findByCodiceStanziamento(String code, StanziamentiDetailsType stanziamentiDetailsType);

  StanziamentoEntity findByCode(String code);

  Set<Stanziamento> findStanziamentoByLogicKey(@NotNull StanziamentoLogicKey stanziamentoLogicKey, boolean filterNotAlreadySentToNav);
  
  Set<Stanziamento> findStanziamentoByLogicKey(@NotNull StanziamentoLogicKey _stanziamentoLogicKey, boolean filterNotAlreadySentToNav, StanziamentiDetailsType stanziamentiDetailsType);

  List<Stanziamento> getAll();

//  Stanziamento create(@NotNull Stanziamento allocation);

  void updateDateNavSent(@NotNull Set<String> codes, Instant date);

  void updateDateQueuing(@NotNull Set<String> codes, Instant date);

  void updateInvoiceData(@NotNull Stanziamento _stanziamento);

  StanziamentoEntity create(@NotNull StanziamentoEntity _allocationEntity);

  StanziamentoEntity updateEntity(@NotNull StanziamentoEntity _entity);

  List<Stanziamento> find100NotQueuedToNav(Instant dataAcquisizioneFlusso);

  List<Stanziamento> find100NotQueuedToNav();


}
