package it.fai.ms.consumi.repository.dettaglio_stanziamento;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import it.fai.ms.consumi.repository.stanziamento.model.ViewStanziamentoEntity;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "view_all_dettaglio_stanziamento")
@DiscriminatorColumn(name = "tipo_dettaglio_stanziamento")
@Immutable
public class ViewDettaglioStanziamentoEntity extends MappedDettaglioStanziamentoEntity{

  @Id
  private String id;
  
  @ManyToMany
  @JoinTable(name = "view_all_dettagliostanziamento_stanziamento")
  private Set<ViewStanziamentoEntity> stanziamenti = new HashSet<>();

  @SuppressWarnings("unchecked")
  @Override
  public Set<ViewStanziamentoEntity> getStanziamenti() {
    return stanziamenti;
  }

  @SuppressWarnings("unchecked")
  @Override
  public String getId() {
    return id;
  }
}
