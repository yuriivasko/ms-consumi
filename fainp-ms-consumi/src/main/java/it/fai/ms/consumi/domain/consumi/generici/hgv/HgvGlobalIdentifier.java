package it.fai.ms.consumi.domain.consumi.generici.hgv;

import java.util.UUID;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElcitGlobalIdentifier;

public class HgvGlobalIdentifier
    extends GlobalIdentifier {

  public HgvGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.INTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    return new HgvGlobalIdentifier(UUID.randomUUID()
                                         .toString());
  }

}
