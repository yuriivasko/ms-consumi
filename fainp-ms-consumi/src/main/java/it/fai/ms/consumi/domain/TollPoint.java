package it.fai.ms.consumi.domain;

import java.time.Instant;
import java.util.Objects;

public class TollPoint {

  private GlobalGate globalGate;
  private Instant    time;

  public TollPoint() {
  }
  

  public TollPoint(GlobalGate globalGate,Instant  time ) {
    this.globalGate = globalGate;
    this.time = time;
  }


  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final TollPoint obj = getClass().cast(_obj);
      res = Objects.equals(obj.globalGate, globalGate) && Objects.equals(obj.time, time);
    }
    return res;
  }

  public GlobalGate getGlobalGate() {
    return globalGate;
  }

  public Instant getTime() {
    return time;
  }

  @Override
  public int hashCode() {
    return Objects.hash(globalGate, time);
  }

  public void setGlobalGate(final GlobalGate _globalGate) {
    globalGate = _globalGate;
  }

  public void setTime(final Instant _time) {
    time = _time;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append(globalGate)
                              .append(",")
                              .append(time)
                              .append("]")
                              .toString();
  }

}
