package it.fai.ms.consumi.repository.flussi.record.model.util;

public class CsvItem {

  private String method = "";
  private int    pos    = 0;

  public CsvItem() {
    super();
  }

  public CsvItem(String method, int pos) {
    super();
    this.method = method.trim();
    this.pos = pos;
  }

  public String getMethod() {
    return method;
  }

  public int getPos() {
    return pos;
  }

}
