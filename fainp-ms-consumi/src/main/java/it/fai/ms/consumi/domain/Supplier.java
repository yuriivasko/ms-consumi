package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.util.Objects;

public class Supplier implements Serializable{
  private static final long serialVersionUID = -7089620684457899596L;

  private final String code;
  private String       codeLegacy;
  private String       document;

  public Supplier(final String _code) {
    code = Objects.requireNonNull(_code, "Parameter code is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Supplier obj = getClass().cast(_obj);
      res = Objects.equals(obj.code, code);
    }
    return res;
  }

  public String getCode() {
    return code;
  }

  public String getCodeLegacy() {
    return codeLegacy;
  }

  public String getDocument() {
    return document;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }

  public void setCodeLegacy(final String _codeLegacy) {
    codeLegacy = _codeLegacy;
  }

  public void setDocument(final String _document) {
    document = _document;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append(code)
                              .append(codeLegacy != null ? "(" : "")
                              .append(codeLegacy != null ? codeLegacy : "")
                              .append(codeLegacy != null ? ")" : "")
                              .append(document != null ? ",document=" : "")
                              .append(document != null ? document : "")
                              .append("]")
                              .toString();
  }

}
