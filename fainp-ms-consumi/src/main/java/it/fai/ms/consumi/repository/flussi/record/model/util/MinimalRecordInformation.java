package it.fai.ms.consumi.repository.flussi.record.model.util;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Arrays;
import java.util.List;

public class MinimalRecordInformation {

  private String simpleName;
  private String uuid;
  private String rowNumber;
  private String recordCode;
  private String transactionDetail;

  public MinimalRecordInformation(String simpleName, String uuid, String rowNumber, String recordCode, String transactionDetail) {
    this.simpleName = simpleName;
    this.uuid = uuid;
    this.rowNumber = rowNumber;
    this.recordCode = recordCode;
    this.transactionDetail = transactionDetail;
  }

  public String getSimpleName() {
    return simpleName;
  }

  public String getUuid() {
    return uuid;
  }

  public String getRowNumber() {
    return rowNumber;
  }

  public Long getRowNumberAsLong() {
    return rowNumber != null && NumberUtils.isCreatable(rowNumber) ? NumberUtils.createLong(rowNumber) : -1;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public String getTransactionDetail() {
    return transactionDetail;
  }

  public List<String> toList() {
    return toList(this);
  }

  public List<String> toList(MinimalRecordInformation minimalRecordInformation) {
    String simpleName = this.getClass().getSimpleName();
    return Arrays.asList(simpleName, uuid, rowNumber+"", recordCode!=null ? recordCode : "", transactionDetail!=null ? transactionDetail :"");
  }

  public static MinimalRecordInformation fromCommonRecord(CommonRecord commonRecord){
    String simpleName = commonRecord.getClass().getSimpleName();
    return new MinimalRecordInformation(simpleName, commonRecord.getUuid(), commonRecord.getRowNumber()+"", commonRecord.getRecordCode()!=null ? commonRecord.getRecordCode() : "", commonRecord.getTransactionDetail()!=null ? commonRecord.getTransactionDetail() :"");
  }

  public static MinimalRecordInformation fromList(List<String> strings) {
    if(strings == null) {
      return new MinimalRecordInformation("n/a","n/a","n/a","n/a","n/a");
    }
    if(strings.size()<5) {
      return new MinimalRecordInformation("n/a","n/a","n/a","n/a","n/a");
    }
    return new MinimalRecordInformation(strings.get(0), strings.get(1), strings.get(2), strings.get(3), strings.get(4));
  }
}
