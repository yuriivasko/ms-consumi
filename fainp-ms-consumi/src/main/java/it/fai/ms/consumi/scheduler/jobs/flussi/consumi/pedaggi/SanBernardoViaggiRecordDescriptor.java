package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;


import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoViaggiRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici.HgvRecordDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class SanBernardoViaggiRecordDescriptor implements RecordDescriptor<SanBernardoViaggiRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(HgvRecordDescriptor.class);

  private final List<String> HEADER = Arrays.asList(
    "tipoTitolo", "panNumber", "piazzale", "pista", "data", "classe", "numeroPacchetto"
  );


  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public String getHeaderBegin() {
    return "Tipo Titolo;Pan Number;Piazzale;Pista;Data;Classe;Numero Pacchetto";
  }

  @Override
  public String getFooterCodeBegin() {
    return "Numero Viaggi";
  }

  @Override
  public String extractRecordCode(String _data) {
    return "";
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  @Override
  public SanBernardoViaggiRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String _fileName, long _rowNumber) {

    SanBernardoViaggiRecord sanBernardoViaggiRecord = new SanBernardoViaggiRecord(_fileName, _rowNumber);
    sanBernardoViaggiRecord.setFromCsvRecord(_data, HEADER);
    return sanBernardoViaggiRecord;
  }

  public void checkConfiguration() {
    log.trace("No need to check configuration...");
  }
}
