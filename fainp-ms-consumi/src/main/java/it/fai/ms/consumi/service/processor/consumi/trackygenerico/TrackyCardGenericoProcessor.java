package it.fai.ms.consumi.service.processor.consumi.trackygenerico;

import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface TrackyCardGenericoProcessor
  extends ConsumoProcessor<TrackyCardCarburantiStandard> {

}
