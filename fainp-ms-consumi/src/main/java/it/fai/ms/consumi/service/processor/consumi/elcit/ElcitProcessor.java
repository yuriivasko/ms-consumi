package it.fai.ms.consumi.service.processor.consumi.elcit;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface ElcitProcessor extends ConsumoProcessor<Elcit> {

}
