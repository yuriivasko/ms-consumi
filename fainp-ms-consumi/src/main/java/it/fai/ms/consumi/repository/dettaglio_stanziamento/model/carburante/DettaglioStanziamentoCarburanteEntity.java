package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante;

import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric;

@Entity
@Table(name = "dettaglio_stanziamento_carburanti")
@EntityListeners(value = DettaglioStanziamentoCarburanteListener.class)
@DiscriminatorValue(value = "CARBURANTI")
public class DettaglioStanziamentoCarburanteEntity extends DettaglioStanziamentoEntityGeneric {

  @Column(name = "altro_supporto")
  private String altroSupporto;

  @Column(name = "codice_fornitore_nav")
  private String codiceFornitoreNav;

  @Column(name = "codice_trackycard")
  private String codiceTrackycard;

  @Column(name = "costo_unitario_calcolato_noiva", precision = 16, scale = 5)
  private BigDecimal costoUnitarioCalcolatoNoiva;

  @Column(name = "data_ora_utilizzo")
  private Instant dataOraUtilizzo;

  @Column(name = "erogatore")
  private String erogatore;

  @Column(name = "km")
  private BigDecimal km;

  @Column(name = "unita_di_misura", nullable = true)
  private String unitaMisura;

  @Column(name = "partner_code")
  private String partnerCode;

  @Column(name = "perc_iva", precision = 19, scale = 2)
  private BigDecimal percIva;

  @Column(name = "prezzo_unitario_calcolato_noiva", precision = 16, scale = 5)
  private BigDecimal prezzoUnitarioCalcolatoNoiva;

  @Column(name = "prezzo_unitario_di_riferimento_noiva", precision = 16, scale = 5)
  private BigDecimal prezzoUnitarioDiRiferimentoNoiva;

  @Column(name = "punto_erogazione")
  private String puntoErogazione;

  @Column(name = "quantita", precision = 16, scale = 5)
  private BigDecimal quantita;

  @Column(name = "storno")
  private boolean storno = false;

  @Column(name = "tipo_consumo")
  @Enumerated(EnumType.STRING)
  private InvoiceType tipoConsumo;

  @Column(name = "tipo_sorgente")
  @Enumerated(EnumType.STRING)
  private Format tipoSorgente;

  @Column(name = "transaction_detail_code")
  private String transactionDetailCode;

  @Column(name = "veicolo_classe")
  private String veicoloClasse;

  @Column(name = "veicolo_nazione_targa")
  private String veicoloNazioneTarga;

  @Column(name = "veicolo_targa")
  private String veicoloTarga;
 
  @Column(name = "row_number")
  private long sourceRowNumber = -1l;
  
  public DettaglioStanziamentoCarburanteEntity(final String _globalIdentifier) {
    super(_globalIdentifier);
  }

  protected DettaglioStanziamentoCarburanteEntity() {
  }

  @Override
  public DettaglioStanziamentoEntity changeInvoiceTypeToD() {
    throw new UnsupportedOperationException();
  }

  public String getAltroSupporto() {
    return altroSupporto;
  }

  public String getCodiceFornitoreNav() {
    return codiceFornitoreNav;
  }

  public String getCodiceTrackycard() {
    return codiceTrackycard;
  }

  public BigDecimal getCostoUnitarioCalcolatoNoiva() {
    return costoUnitarioCalcolatoNoiva;
  }

  public Instant getDataOraUtilizzo() {
    return dataOraUtilizzo;
  }

  public String getErogatore() {
    return erogatore;
  }

  public BigDecimal getKm() {
    return km;
  }

  public String getUnitaMisura() {
    return unitaMisura;
  }

  public String getPartnerCode() {
    return partnerCode;
  }

  public BigDecimal getPercIva() {
    return percIva;
  }

  public BigDecimal getPrezzoUnitarioCalcolatoNoiva() {
    return prezzoUnitarioCalcolatoNoiva;
  }

  public BigDecimal getPrezzoUnitarioDiRiferimentoNoiva() {
    return prezzoUnitarioDiRiferimentoNoiva;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public BigDecimal getQuantita() {
    return quantita;
  }

  public InvoiceType getTipoConsumo() {
    return tipoConsumo;
  }

  public Format getTipoSorgente() {
    return tipoSorgente;
  }

  public String getTransactionDetailCode() {
    return transactionDetailCode;
  }

  public String getVeicoloClasse() {
    return veicoloClasse;
  }

  public String getVeicoloNazioneTarga() {
    return veicoloNazioneTarga;
  }

  public String getVeicoloTarga() {
    return veicoloTarga;
  }

  public boolean isStorno() {
    return storno;
  }

  @Override
  public DettaglioStanziamentoEntity negateAmount() {
    throw new UnsupportedOperationException();
  }

  public void setAltroSupporto(String altroSupporto) {
    this.altroSupporto = altroSupporto;
  }

  public void setCodiceFornitoreNav(String codiceFornitoreNav) {
    this.codiceFornitoreNav = codiceFornitoreNav;
  }

  public void setCodiceTrackycard(String codiceTrackycard) {
    this.codiceTrackycard = codiceTrackycard;
  }

  public void setCostoUnitarioCalcolatoNoiva(BigDecimal costoUnitarioCalcolatoNoiva) {
    this.costoUnitarioCalcolatoNoiva = costoUnitarioCalcolatoNoiva;
  }


  public void setDataOraUtilizzo(Instant dataUtilizzo) {
    this.dataOraUtilizzo = dataUtilizzo;
  }

  public void setErogatore(String erogatore) {
    this.erogatore = erogatore;
  }

  public void setKm(BigDecimal km) {
    this.km = km;
  }

  public void setUnitaMisura(String unitaMisura) {
    this.unitaMisura = unitaMisura;
  }

  public void setPartnerCode(String partnerCode) {
    this.partnerCode = partnerCode;
  }

  public void setPercIva(BigDecimal percIva) {
    this.percIva = percIva;
  }

  public void setPrezzoUnitarioCalcolatoNoiva(BigDecimal prezzoUnitarioCalcolatoNoiva) {
    this.prezzoUnitarioCalcolatoNoiva = prezzoUnitarioCalcolatoNoiva;
  }

  public void setPrezzoUnitarioDiRiferimentoNoiva(BigDecimal prezzoUnitarioDiRiferimentoNoiva) {
    this.prezzoUnitarioDiRiferimentoNoiva = prezzoUnitarioDiRiferimentoNoiva;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

  public void setQuantita(BigDecimal quantita) {
    this.quantita = quantita;
  }

  public void setStorno(boolean storno) {
    this.storno = storno;
  }

  public void setTipoConsumo(InvoiceType tipoConsumo) {
    this.tipoConsumo = tipoConsumo;
  }

  public void setTipoSorgente(Format tipoSorgente) {
    this.tipoSorgente = tipoSorgente;
  }

  public void setTransactionDetailCode(String transactionDetailCode) {
    this.transactionDetailCode = transactionDetailCode;
  }

  public void setVeicoloClasse(String veicoloClasse) {
    this.veicoloClasse = veicoloClasse;
  }

  public void setVeicoloNazioneTarga(String veicoloNazioneTarga) {
    this.veicoloNazioneTarga = veicoloNazioneTarga;
  }

  public void setVeicoloTarga(String veicoloTarga) {
    this.veicoloTarga = veicoloTarga;
  }
   

  public long getSourceRowNumber() {
    return sourceRowNumber;
  }

  public void setSourceRowNumber(long sourceRowNumber) {
    this.sourceRowNumber = sourceRowNumber;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("DettaglioStanziamentoCarburanteEntity [altroSupporto=");
    builder.append(altroSupporto);
    builder.append(", codiceFornitoreNav=");
    builder.append(codiceFornitoreNav);
    builder.append(", codiceTrackycard=");
    builder.append(codiceTrackycard);
    builder.append(", costoUnitarioCalcolatoNoiva=");
    builder.append(costoUnitarioCalcolatoNoiva);
    builder.append(", dataUtilizzo=");
    builder.append(dataOraUtilizzo);
    builder.append(", erogatore=");
    builder.append(erogatore);
    builder.append(", km=");
    builder.append(km);
    builder.append(", unitaMisura=");
    builder.append(unitaMisura);
    builder.append(", partnerCode=");
    builder.append(partnerCode);
    builder.append(", percIva=");
    builder.append(percIva);
    builder.append(", prezzoUnitarioCalcolatoNoiva=");
    builder.append(prezzoUnitarioCalcolatoNoiva);
    builder.append(", prezzoUnitarioDiRiferimentoNoiva=");
    builder.append(prezzoUnitarioDiRiferimentoNoiva);
    builder.append(", puntoErogazione=");
    builder.append(puntoErogazione);
    builder.append(", quantita=");
    builder.append(quantita);
    builder.append(", storno=");
    builder.append(storno);
    builder.append(", tipoConsumo=");
    builder.append(tipoConsumo);
    builder.append(", tipoSorgente=");
    builder.append(tipoSorgente);
    builder.append(", transactionDetailCode=");
    builder.append(transactionDetailCode);
    builder.append(", veicoloClasse=");
    builder.append(veicoloClasse);
    builder.append(", veicoloNazioneTarga=");
    builder.append(veicoloNazioneTarga);
    builder.append(", veicoloTarga=");
    builder.append(veicoloTarga);
    builder.append(", getGlobalIdentifier()=");
    builder.append(getGlobalIdentifier());
    builder.append(", sourceRowNumber()=");
    builder.append(sourceRowNumber);
   
    
    builder.append("]");
    return builder.toString();
  }



}
