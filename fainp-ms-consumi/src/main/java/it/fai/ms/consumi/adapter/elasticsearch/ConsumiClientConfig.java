package it.fai.ms.consumi.adapter.elasticsearch;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Feign;
import it.fai.ms.consumi.config.FeignConfiguration;

@Configuration
public class ConsumiClientConfig {

  @Bean
  public ElasticSearchFeignClient elasticSearchFeignClient(@Value("${es-consumi.ribbon.listOfServers}") String esServerAddress){
    ElasticSearchFeignClient elasticSearchFeignClient = Feign.builder()
      .contract(new SpringMvcContract())
      .encoder(FeignConfiguration.buildSpringEncoder())
      .decoder(FeignConfiguration.buildSpringDecoder())
      .target(ElasticSearchFeignClient.class, "http://" + esServerAddress); //FIXME usare il feign bilanciato
    return elasticSearchFeignClient;
  }
  

//  @Bean
//  public Decoder feignDecoder(ObjectMapper customObjectMapper) {
//    customObjectMapper.registerModule(monetaryAmountStringDeserializer());
//    customObjectMapper.registerModule(monetaryAmountStringSerializer());
//
//    HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter(customObjectMapper);
//    ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(jacksonConverter);
//    return new ResponseEntityDecoder(new SpringDecoder(objectFactory));
//  }
//
//  private SimpleModule monetaryAmountStringDeserializer(){
//    SimpleModule module = new SimpleModule();
//    module.addDeserializer(MonetaryAmount.class, new MoneyDeserializer());
//    return module;
//  }
//
//  private SimpleModule monetaryAmountStringSerializer(){
//    SimpleModule module = new SimpleModule();
//    module.addSerializer(MonetaryAmount.class, new MoneySerializer());
//    return module;
//  }
//
//  @Bean
//  @Profile("integration")
//  public ObjectMapper customObjectMapper(){
//    ObjectMapper objectMapper = new ObjectMapper();
//    objectMapper.findAndRegisterModules();
//    objectMapper.registerModule(new MoneyModule());
//    return objectMapper;
//  }


}
