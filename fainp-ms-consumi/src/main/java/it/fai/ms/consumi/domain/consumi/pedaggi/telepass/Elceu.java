package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Objects;
import java.util.Optional;

public class Elceu extends ConsumoPedaggio {

  public Elceu(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Elceu obj = getClass().cast(_obj);
      res = super.equals(_obj);
    }
    return res;
  }

  @Override
  public InvoiceType getInvoiceType() {
    return InvoiceType.P;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode());
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }

}
