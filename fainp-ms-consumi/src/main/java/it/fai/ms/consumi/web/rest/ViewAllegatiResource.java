package it.fai.ms.consumi.web.rest;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione1Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione1Id;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione1Repository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione2Repository;
import it.fai.ms.consumi.web.rest.errors.BadRequestAlertException;

@RestController
@RequestMapping(ViewAllegatiResource.BASE_PATH)
public class ViewAllegatiResource {

  static final String BASE_PATH = "/api/public";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String VIEW_ALLEGATI_FATTURE = "/viewallegatifatture";

  private final DettaglioStanziamantiRepository dettaglioRepository;

  private final ViewAllegatoPedaggioSezione1Repository viewSezione1Repository;

  private final ViewAllegatoPedaggioSezione2Repository viewSezione2Repository;

  public ViewAllegatiResource(final DettaglioStanziamantiRepository _dettaglioRepository,
                              final ViewAllegatoPedaggioSezione1Repository _viewSezione1Repository,
                              final ViewAllegatoPedaggioSezione2Repository _viewSezione2Repository) {
    dettaglioRepository = _dettaglioRepository;
    viewSezione1Repository = _viewSezione1Repository;
    viewSezione2Repository = _viewSezione2Repository;
  }

  @GetMapping(VIEW_ALLEGATI_FATTURE + "/contratti")
  public ResponseEntity<Set<String>> getContrattiByCodiciStanziamento(@RequestParam List<String> codiciStanziamento) {

    log.debug("Request to retrieve contratti by codiciStanziamento: {}", codiciStanziamento);

    Set<String> codiceContratti = null;
    try {
      List<UUID> dettagliId = dettaglioRepository.findDettagliStanziamentoIdByCodiciStanziamento(codiciStanziamento);
      List<ViewAllegatoPedaggioSezione2Entity> viewSezione2 = viewSezione2Repository.findByDettaglioStanziamentoId(dettagliId);
      codiceContratti = viewSezione2.stream()
                                    .map(v -> v.getCodiceContratto())
                                    .collect(Collectors.toSet());
    } catch (Exception e) {
      log.error("Error on call rest cliente efservice: ", e);
      throw new BadRequestAlertException("Error on retrieve contratti", ViewAllegatiResource.class.getSimpleName(), "");
    }

    return ResponseEntity.ok(codiceContratti);
  }

  @GetMapping(VIEW_ALLEGATI_FATTURE + "/dettagli/{tipoDispositivo}/{pannumber}/{targa}/{nazione}/{euroclass}")
  public ResponseEntity<Set<ViewAllegatoPedaggioSezione2Entity>> getDettagliByParams(@RequestParam(
                                                                                                   value = "tipoDispositivo",
                                                                                                   required = false) TipoDispositivoEnum deviceType,
                                                                                     @RequestParam(
                                                                                                   value = "pannumber",
                                                                                                   required = false,
                                                                                                   defaultValue = "") String panNumber,
                                                                                     @RequestParam(
                                                                                                   value = "targa",
                                                                                                   required = false,
                                                                                                   defaultValue = "") String licensePlate,
                                                                                     @RequestParam(
                                                                                                   value = "nazione",
                                                                                                   required = false,
                                                                                                   defaultValue = "") String country,
                                                                                     @RequestParam(
                                                                                                   value = "euroclass",
                                                                                                   required = false,
                                                                                                   defaultValue = "") String euroClass) {

    log.debug("Request to retrieve DettagliStanziamentoPedaggi by : {}, {}, {}, {}, {}", deviceType, panNumber, licensePlate, country,
              euroClass);

    Set<ViewAllegatoPedaggioSezione2Entity> dettagli = null;
    try {
      dettagli = viewSezione2Repository.findDettagliBy(deviceType, licensePlate, country, panNumber, euroClass);
    } catch (Exception e) {
      log.error("Error on call rest cliente efservice: ", e);
      throw new BadRequestAlertException("Error on retrieve dettagli", ViewAllegatiResource.class.getSimpleName(), "");
    }

    return ResponseEntity.ok(dettagli);
  }

  @GetMapping(VIEW_ALLEGATI_FATTURE + "/dettagli/{codiceContratto}")
  public ResponseEntity<List<ViewAllegatoPedaggioSezione2Entity>> getDettagliByContrattoAndCodiciStanziamento(@RequestParam(
                                                                                                                            value = "codiceContratto",
                                                                                                                            required = false,
                                                                                                                            defaultValue = "") String contratto,
                                                                                                              @RequestParam List<String> codiciStanziamento) {

    log.debug("Request to retrieve DettagliStanziamentoPedaggi by codiceContratto : {} and CodiciStanziamento: {}", contratto,
              codiciStanziamento);

    List<ViewAllegatoPedaggioSezione2Entity> dettagli = null;
    try {
      List<UUID> dettagliId = dettaglioRepository.findDettagliStanziamentoIdByCodiciStanziamento(codiciStanziamento);
      List<ViewAllegatoPedaggioSezione2Entity> viewSezione2 = viewSezione2Repository.findByDettaglioStanziamentoId(dettagliId);
      dettagli = viewSezione2.stream()
                             .filter(v -> v.getCodiceContratto()
                                           .equals(contratto))
                             .collect(Collectors.toList());
    } catch (Exception e) {
      log.error("Error on call rest cliente efservice: ", e);
      throw new BadRequestAlertException("Error on retrieve dettagli", ViewAllegatiResource.class.getSimpleName(), "");
    }

    return ResponseEntity.ok(dettagli);
  }

  @GetMapping(VIEW_ALLEGATI_FATTURE + "/reportmensile/{codiceContratto}/{pannumber}/{tipoDispositivo}")
  public ResponseEntity<Set<ViewAllegatoPedaggioSezione1Entity>> getTotaleMensile(@RequestParam(
                                                                                                value = "codiceContratto",
                                                                                                required = false,
                                                                                                defaultValue = "") String contratto,
                                                                                  @RequestParam(
                                                                                                value = "pannumber",
                                                                                                required = false,
                                                                                                defaultValue = "") String panNumber,
                                                                                  @RequestParam(
                                                                                                value = "tipoDispositivo",
                                                                                                required = false,
                                                                                                defaultValue = "") TipoDispositivoEnum tipoDispositivo) {

    log.debug("Request to retrieve TotaliMensili by codiceContratto : {} and panNumber: {} and tipo dispositivo: {}", contratto, panNumber,
              tipoDispositivo);
    ViewAllegatoPedaggioSezione1Id idClass = new ViewAllegatoPedaggioSezione1Id();
    idClass.setCodiceContratto(contratto);
    idClass.setPanNumber(panNumber);
    idClass.setTipoDispositivo(tipoDispositivo.name());

    Set<ViewAllegatoPedaggioSezione1Entity> reportMensili = null;
    try {
      reportMensili = viewSezione1Repository.findBySezioneIdClass(idClass);
    } catch (Exception e) {
      log.error("Error on call rest cliente efservice: ", e);
      throw new BadRequestAlertException("Error on retrieve Report Mensili", ViewAllegatiResource.class.getSimpleName(), "");
    }

    return ResponseEntity.ok(reportMensili);
  }

  @GetMapping(VIEW_ALLEGATI_FATTURE + "/reportmensile/{codiceContratto}/{pannumber}/{tipoDispositivo}/{dateFrom}/{dateTo}")
  public ResponseEntity<Set<ViewAllegatoPedaggioSezione1Entity>> getTotaleMese(@RequestParam(
                                                                                             value = "codiceContratto",
                                                                                             required = false,
                                                                                             defaultValue = "") String contratto,
                                                                               @RequestParam(
                                                                                             value = "pannumber",
                                                                                             required = false,
                                                                                             defaultValue = "") String panNumber,
                                                                               @RequestParam(
                                                                                             value = "tipoDispositivo",
                                                                                             required = false,
                                                                                             defaultValue = "") TipoDispositivoEnum tipoDispositivo,
                                                                               @RequestParam(
                                                                                             value = "dateFrom",
                                                                                             required = false,
                                                                                             defaultValue = "") LocalDate dateFrom,
                                                                               @RequestParam(
                                                                                             value = "dateTo",
                                                                                             required = false,
                                                                                             defaultValue = "") LocalDate dateTo) {

    log.debug("Request to retrieve getTotaleMese by codiceContratto : {} and panNumber: {} and tipo dispositivo: {}, Range date: {} - {}",
              contratto, panNumber, tipoDispositivo, dateFrom, dateTo);
    ViewAllegatoPedaggioSezione1Id idClass = new ViewAllegatoPedaggioSezione1Id();
    idClass.setCodiceContratto(contratto);
    idClass.setPanNumber(panNumber);
    idClass.setTipoDispositivo(tipoDispositivo.name());

    Set<ViewAllegatoPedaggioSezione1Entity> reportMensili = null;
    try {
      reportMensili = viewSezione1Repository.findBySezioneIdClassAndRangeDateNotInvoiced(idClass, dateFrom, dateTo);
    } catch (Exception e) {
      log.error("Error on call rest cliente efservice: ", e);
      throw new BadRequestAlertException("Error on retrieve Report Mese", ViewAllegatiResource.class.getSimpleName(), "");
    }

    return ResponseEntity.ok(reportMensili);
  }

}
