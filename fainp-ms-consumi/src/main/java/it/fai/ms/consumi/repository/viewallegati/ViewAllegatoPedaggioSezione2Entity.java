package it.fai.ms.consumi.repository.viewallegati;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;

@Entity
@Table(name = "allegati_fatture_pedaggi_sezione2")
@Immutable
public class ViewAllegatoPedaggioSezione2Entity {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "codice_contratto")
  private String codiceContratto;

  @Column(name = "tipo_dispositivo")
  private String tipoDispositivo;

  @Column(name = "pan_number")
  private String panNumber;

  @Column(name = "serial_number")
  private String serialNumber;

  @Column(name = "nazione_targa")
  private String nazioneVeicolo;

  @Column(name = "targa")
  private String targaVeicolo;

  @Column(name = "classificazione_euro")
  private String classificazioneEuro;

  @Column(name = "causale")
  private String causale;

  @Column(name = "timestamp_ingresso")
  private Instant timeStampIngresso;

  @Column(name = "timestamp_uscita")
  private Instant timeStampUscita;

  @Column(name = "codice_ingresso")
  private String codiceIngresso;

  @Column(name = "codice_uscita")
  private String codiceUscita;

  @Column(name = "descrizione_ingresso")
  private String descrizioneIngresso;

  @Column(name = "descrizione_uscita")
  private String descrizioneUscita;

  @Column(name = "autostrada")
  private String autostrada;

  @Column(name = "segno_transazione")
  private String segnoTransazione;

  @Column(name = "importo")
  private Double importo;

  @Column(name = "importo_con_iva")
  private Double importoIva;

  @Column(name = "classe_tariffaria")
  private String classeTariffaria;

  @Column(name = "valuta")
  private String valuta;

  @Column(name = "cambio")
  private Double cambio;

  @Column(name = "numero_transazione")
  private String numeroTransazione;

  @Column(name = "perc_iva")
  private Double percIva;
  
  @Column(name = "tipo_consumo")
  private String tipoConsumo;
  
  @Column(name = "tipo_hardware")
  private String tipoHardware;
   
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getPanNumber() {
    return panNumber;
  }

  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getNazioneVeicolo() {
    return nazioneVeicolo;
  }

  public void setNazioneVeicolo(String nazioneVeicolo) {
    this.nazioneVeicolo = nazioneVeicolo;
  }

  public String getTargaVeicolo() {
    return targaVeicolo;
  }

  public void setTargaVeicolo(String targaVeicolo) {
    this.targaVeicolo = targaVeicolo;
  }

  public String getClassificazioneEuro() {
    return classificazioneEuro;
  }

  public void setClassificazioneEuro(String classificazioneEuro) {
    this.classificazioneEuro = classificazioneEuro;
  }

  public String getCausale() {
    return causale;
  }

  public void setCausale(String causale) {
    this.causale = causale;
  }

  public Instant getTimeStampIngresso() {
    return timeStampIngresso;
  }

  public LocalDate getDataIngresso() {
    Instant timeStamp = getTimeStampIngresso();
    if (timeStamp == null) {
      return null;
    }
    return FaiConsumiDateUtil.convertToLocalDate(timeStamp);
  }

  public LocalTime getOraIngresso() {
    Instant timeStamp = getTimeStampIngresso();
    if (timeStamp == null) {
      return null;
    }
    return FaiConsumiDateUtil.convertToLocalTime(timeStamp);
  }

  public void setTimeStampIngresso(Instant timeStampIngresso) {
    this.timeStampIngresso = timeStampIngresso;
  }

  public Instant getTimeStampUscita() {
    return timeStampUscita;
  }

  public LocalDate getDataUscita() {
    Instant timeStamp = getTimeStampUscita();
    if (timeStamp == null) {
      return null;
    }
    return FaiConsumiDateUtil.convertToLocalDate(timeStamp);
  }

  public LocalTime getOraUscita() {
    Instant timeStamp = getTimeStampUscita();
    if (timeStamp == null) {
      return null;
    }
    return FaiConsumiDateUtil.convertToLocalTime(timeStamp);
  }

  public void setTimeStampUscita(Instant timeStampUscita) {
    this.timeStampUscita = timeStampUscita;
  }

  public String getCodiceIngresso() {
    return codiceIngresso;
  }

  public void setCodiceIngresso(String codiceIngresso) {
    this.codiceIngresso = codiceIngresso;
  }

  public String getCodiceUscita() {
    return codiceUscita;
  }

  public void setCodiceUscita(String codiceUscita) {
    this.codiceUscita = codiceUscita;
  }

  public String getDescrizioneIngresso() {
    return descrizioneIngresso;
  }

  public void setDescrizioneIngresso(String descrizioneIngresso) {
    this.descrizioneIngresso = descrizioneIngresso;
  }

  public String getDescrizioneUscita() {
    return descrizioneUscita;
  }

  public void setDescrizioneUscita(String descrizioneUscita) {
    this.descrizioneUscita = descrizioneUscita;
  }

  public String getAutostrada() {
    return autostrada;
  }

  public void setAutostrada(String autostrada) {
    this.autostrada = autostrada;
  }

  public String getSegnoTransazione() {
    return segnoTransazione;
  }

  public void setSegnoTransazione(String segnoTransazione) {
    this.segnoTransazione = segnoTransazione;
  }

  public Double getImporto() {
    return importo;
  }

  public void setImporto(Double importo) {
    this.importo = importo;
  }

  public Double getImportoIva() {
    return importoIva;
  }

  public void setImportoIva(Double importoIva) {
    this.importoIva = importoIva;
  }

  public String getClasseTariffaria() {
    return classeTariffaria;
  }

  public void setClasseTariffaria(String classeTariffaria) {
    this.classeTariffaria = classeTariffaria;
  }

  public String getValuta() {
    return valuta;
  }

  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

  public Double getCambio() {
    return cambio;
  }

  public void setCambio(Double cambio) {
    this.cambio = cambio;
  }

  public String getNumeroTransazione() {
    return numeroTransazione;
  }

  public void setNumeroTransazione(String numeroTransazione) {
    this.numeroTransazione = numeroTransazione;
  }

  public Double getPercIva() {
    return percIva;
  }

  public void setPercIva(Double percIva) {
    this.percIva = percIva;
  } 

  public String getTipoConsumo() {
    return tipoConsumo;
  }

  public void setTipoConsumo(String tipoConsumo) {
    this.tipoConsumo = tipoConsumo;
  } 

  public String getTipoHardware() {
    return tipoHardware;
  }

  public void setTipoHardware(String tipoHardware) {
    this.tipoHardware = tipoHardware;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ViewAllegatoPedaggioSezione2Entity [id=");
    builder.append(id);
    builder.append(", codiceContratto=");
    builder.append(codiceContratto);
    builder.append(", tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", panNumber=");
    builder.append(panNumber);
    builder.append(", serialNumber=");
    builder.append(serialNumber);
    builder.append(", nazioneVeicolo=");
    builder.append(nazioneVeicolo);
    builder.append(", targaVeicolo=");
    builder.append(targaVeicolo);
    builder.append(", classificazioneEuro=");
    builder.append(classificazioneEuro);
    builder.append(", causale=");
    builder.append(causale);
    builder.append(", timeStampIngresso=");
    builder.append(timeStampIngresso);
    builder.append(", timeStampUscita=");
    builder.append(timeStampUscita);
    builder.append(", codiceIngresso=");
    builder.append(codiceIngresso);
    builder.append(", codiceUscita=");
    builder.append(codiceUscita);
    builder.append(", descrizioneIngresso=");
    builder.append(descrizioneIngresso);
    builder.append(", descrizioneUscita=");
    builder.append(descrizioneUscita);
    builder.append(", autostrada=");
    builder.append(autostrada);
    builder.append(", segnoTransazione=");
    builder.append(segnoTransazione);
    builder.append(", importo=");
    builder.append(importo);
    builder.append(", importoIva=");
    builder.append(importoIva);
    builder.append(", classeTariffaria=");
    builder.append(classeTariffaria);
    builder.append(", valuta=");
    builder.append(valuta);
    builder.append(", cambio=");
    builder.append(cambio);
    builder.append(", numeroTransazione=");
    builder.append(numeroTransazione);
    builder.append(", percIva=");
    builder.append(percIva);
    builder.append(", tipoConsumo=");
    builder.append(tipoConsumo);
    builder.append(", tipoHardware=");
    builder.append(tipoHardware);
    builder.append("]");
    return builder.toString();
  }

}
