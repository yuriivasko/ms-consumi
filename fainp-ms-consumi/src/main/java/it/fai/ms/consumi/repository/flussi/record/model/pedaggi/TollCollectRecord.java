package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryBaseRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;
import org.apache.commons.csv.CSVFormat;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.util.UUID;

public class TollCollectRecord extends CommonRecord
  implements GlobalIdentifierBySupplier, MonetaryBaseRecord {

  private final String globalIdentifier;

  private String invoiceRecipient;
  private String recipientCity;
  private String statementLevel;
  private String EDIPartnerCode;
  private String RAGCode;
  private String invoiceNumber;
  private String invoiceDate;
  private String dueDate;
  private String positionNumber;
  private String subPositionNumber;
  private String processingDate;
  private String supplierName;
  private String supplierAddress1;
  private String supplierAddress2;
  private String supplierUIN;
  private String supplierTaxNumber;
  private String cardNumber;
  private String supplierNumber;
  private String registrationCountry;
  private String registrationNumber;
  private String serviceDescription;
  private String branch;
  private String supplierServiceDate;
  private String supplierCostCenter;
  private String supplierInvoiceNumber;
  private String supplierInvoiceDate;
  private String supplierStatementDate;
  private String saleCurrencySC;
  private String netAmountSC;
  private String vatPercentage;
  private String vatPercentageSC;
  private String grossAmountSC;
  private String currencyRate;
  private String statementCurrencyIC;
  private String netAmountIC;
  private String vatIC;

  @JsonCreator
  public TollCollectRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID().toString();
  }

  @Override
  public CSVFormat getCsvFormat() {
    return super.getCsvFormat().withDelimiter(';');
  }


  public String getInvoiceRecipient() {
    return invoiceRecipient;
  }

  public void setInvoiceRecipient(String invoiceRecipient) {
    this.invoiceRecipient = invoiceRecipient;
  }

  public String getRecipientCity() {
    return recipientCity;
  }

  public void setRecipientCity(String recipientCity) {
    this.recipientCity = recipientCity;
  }

  public String getStatementLevel() {
    return statementLevel;
  }

  public void setStatementLevel(String statementLevel) {
    this.statementLevel = statementLevel;
  }

  public String getEDIPartnerCode() {
    return EDIPartnerCode;
  }

  public void setEDIPartnerCode(String EDIPartnerCode) {
    this.EDIPartnerCode = EDIPartnerCode;
  }

  public String getRAGCode() {
    return RAGCode;
  }

  public void setRAGCode(String RAGCode) {
    this.RAGCode = RAGCode;
  }

  public String getInvoiceNumber() {
    return invoiceNumber;
  }

  public void setInvoiceNumber(String invoiceNumber) {
    this.invoiceNumber = invoiceNumber;
  }

  public String getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(String invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public String getDueDate() {
    return dueDate;
  }

  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

  public String getPositionNumber() {
    return positionNumber;
  }

  public void setPositionNumber(String positionNumber) {
    this.positionNumber = positionNumber;
  }

  public String getSubPositionNumber() {
    return subPositionNumber;
  }

  public void setSubPositionNumber(String subPositionNumber) {
    this.subPositionNumber = subPositionNumber;
  }

  public String getProcessingDate() {
    return processingDate;
  }

  public void setProcessingDate(String processingDate) {
    this.processingDate = processingDate;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getSupplierAddress1() {
    return supplierAddress1;
  }

  public void setSupplierAddress1(String supplierAddress1) {
    this.supplierAddress1 = supplierAddress1;
  }

  public String getSupplierAddress2() {
    return supplierAddress2;
  }

  public void setSupplierAddress2(String supplierAddress2) {
    this.supplierAddress2 = supplierAddress2;
  }

  public String getSupplierUIN() {
    return supplierUIN;
  }

  public void setSupplierUIN(String supplierUIN) {
    this.supplierUIN = supplierUIN;
  }

  public String getSupplierTaxNumber() {
    return supplierTaxNumber;
  }

  public void setSupplierTaxNumber(String supplierTaxNumber) {
    this.supplierTaxNumber = supplierTaxNumber;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getSupplierNumber() {
    return supplierNumber;
  }

  public void setSupplierNumber(String supplierNumber) {
    this.supplierNumber = supplierNumber;
  }

  public String getRegistrationCountry() {
    return registrationCountry;
  }

  public void setRegistrationCountry(String registrationCountry) {
    this.registrationCountry = registrationCountry;
  }

  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  public String getServiceDescription() {
    return serviceDescription;
  }

  public void setServiceDescription(String serviceDescription) {
    this.serviceDescription = serviceDescription;
  }

  public String getBranch() {
    return branch;
  }

  public void setBranch(String branch) {
    this.branch = branch;
  }

  public String getSupplierServiceDate() {
    return supplierServiceDate;
  }

  public void setSupplierServiceDate(String supplierServiceDate) {
    this.supplierServiceDate = supplierServiceDate;
  }

  public String getSupplierCostCenter() {
    return supplierCostCenter;
  }

  public void setSupplierCostCenter(String supplierCostCenter) {
    this.supplierCostCenter = supplierCostCenter;
  }

  public String getSupplierInvoiceNumber() {
    return supplierInvoiceNumber;
  }

  public void setSupplierInvoiceNumber(String supplierInvoiceNumber) {
    this.supplierInvoiceNumber = supplierInvoiceNumber;
  }

  public String getSupplierInvoiceDate() {
    return supplierInvoiceDate;
  }

  public void setSupplierInvoiceDate(String supplierInvoiceDate) {
    this.supplierInvoiceDate = supplierInvoiceDate;
  }

  public String getSupplierStatementDate() {
    return supplierStatementDate;
  }

  public void setSupplierStatementDate(String supplierStatementDate) {
    this.supplierStatementDate = supplierStatementDate;
  }

  public String getSaleCurrencySC() {
    return saleCurrencySC;
  }

  public void setSaleCurrencySC(String saleCurrencySC) {
    this.saleCurrencySC = saleCurrencySC;
  }

  public String getNetAmountSC() {
    return netAmountSC;
  }

  public void setNetAmountSC(String netAmountSC) {
    this.netAmountSC = netAmountSC;
  }

  public String getVatPercentage() {
    return vatPercentage;
  }

  public void setVatPercentage(String vatPercentage) {
    this.vatPercentage = vatPercentage;
  }

  public String getVatPercentageSC() {
    return vatPercentageSC;
  }

  public void setVatPercentageSC(String vatPercentageSC) {
    this.vatPercentageSC = vatPercentageSC;
  }

  public String getGrossAmountSC() {
    return grossAmountSC;
  }

  public void setGrossAmountSC(String grossAmountSC) {
    this.grossAmountSC = grossAmountSC;
  }

  public String getCurrencyRate() {
    return currencyRate;
  }

  public void setCurrencyRate(String currencyRate) {
    this.currencyRate = currencyRate;
  }

  public String getStatementCurrencyIC() {
    return statementCurrencyIC;
  }

  public void setStatementCurrencyIC(String statementCurrencyIC) {
    this.statementCurrencyIC = statementCurrencyIC;
  }

  public String getNetAmountIC() {
    return netAmountIC;
  }

  public void setNetAmountIC(String netAmountIC) {
    this.netAmountIC = netAmountIC;
  }

  public String getVatIC() {
    return vatIC;
  }

  public void setVatIC(String vatIC) {
    this.vatIC = vatIC;
  }


  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  @Override
  public BigDecimal getVatPercRate() {
    //TODO verify vatPercentageSC or vatPercentage
    return vatPercentage == null ? null : NumericUtils.strToBigDecimal(vatPercentage, 2,100);
  }

  @Override
  public MonetaryAmount getAmountIncludedVat() {
    if (this.grossAmountSC != null) {
      return MonetaryUtils.strToMonetary(this.grossAmountSC, -2, statementCurrencyIC);
    }
    return null;  }

  @JsonIgnore
  @Override
  public MonetaryAmount getAmountExcludedVat() {
    if (this.netAmountSC != null) {
      return MonetaryUtils.strToMonetary(this.netAmountSC, -2, statementCurrencyIC);
    }
    return null;
  }
}
