package it.fai.ms.consumi.service.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.StoricoOrdiniClienteDMLDTO;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoOrdiniCliente;

@Component
public class StoricoOrdiniClienteDMLDTOMapper {

  // TODO : campi non assegnati

    public StoricoOrdiniClienteDMLDTO toDTO(final StoricoOrdiniCliente data) {
      StoricoOrdiniClienteDMLDTO dto =  new StoricoOrdiniClienteDMLDTO();
      dto.setCodiceClienteAssegnatario(data.getClienteAssegnatario());
      // data.getCodiceCliente();
      dto.setConsumoPrevisto(data.getConsumoMensilePrevisto());
      dto.setDmlRevisionTimestamp(data.getDmlRevisionTimestamp());
      dto.setOrdineClienteDmlUniqueIdentifier(data.getOrdineClienteDmlUniqueIdentifier());
      dto.setNumero(data.getNumeroOrdine());
      dto.setRagioneSocialeAssegnatario(data.getRagioneSociale());
      // data.getRichiedente();
      dto.setStato(data.getStatoOrdine());
      dto.setTipo(data.getTipoOrdine());
      return dto;
    }

//  public OrdiniClienteDMLDTO toDTO(final StoricoOrdiniCliente data) {
//    OrdiniClienteDMLDTO dto =  new OrdiniClienteDMLDTO();
//    dto.setCodiceClienteAssegnatario(data.getClienteAssegnatario());
//    dto.setCodiceClienteFatturazione(data.getCodiceCliente());
//    dto.setConsumoPrevisto(data.getConsumoMensilePrevisto());
//    dto.setDmlRevisionTimestamp(data.getDmlRevisionTimestamp());
//    dto.setDmlUniqueIdentifier(data.getDmlUniqueIdentifier());
//    //dto.setNumero(data.getOrdineClienteDmlUniqueIdentifier());
//    dto.setNumero(data.getNumeroOrdine());
//    dto.setRagioneSocialeAssegnatario(data.getRagioneSociale());
//    dto.setRichiedente(data.getRichiedente());
//    dto.setStato(data.getStatoOrdine());
//    dto.setTipo(data.getTipoOrdine());
//    return dto;
//  }

}
