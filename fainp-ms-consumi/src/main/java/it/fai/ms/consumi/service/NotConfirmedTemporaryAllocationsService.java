package it.fai.ms.consumi.service;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.NotConfirmedAllocationsConfig;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.scheduler.jobs.StanziamentoDTOMapper;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;

import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGeneratorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer.QUEUE;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

@Service
public class NotConfirmedTemporaryAllocationsService {

    private Logger log = LoggerFactory.getLogger(getClass());

    private final DettaglioStanziamantiRepository dettaglioStanziamentiRepository;
    private final StanziamentiToNavPublisher stanziamentiToNavJmsProducer;
    private final StanziamentoRepository stanziamentoRepository;
    private final StanziamentoEntityMapper stanziamentoEntityMapper;

    private List<String> sources = Arrays.asList("vacon", "elvia");


    private final NotConfirmedAllocationsConfig notConfirmedAllocationsConfig;
    
    private final StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator;


    public NotConfirmedTemporaryAllocationsService(
            DettaglioStanziamantiRepository repository,
            StanziamentiToNavPublisher stanziamentiToNavJmsProducer,
            StanziamentoRepository stanziamentoRepository,
            StanziamentoEntityMapper stanziamentoEntityMapper,
            ApplicationProperties applicationProperties,
            StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator) {
        this.dettaglioStanziamentiRepository = repository;
        this.stanziamentiToNavJmsProducer = stanziamentiToNavJmsProducer;
        this.stanziamentoRepository = stanziamentoRepository;
        this.stanziamentoEntityMapper = stanziamentoEntityMapper;
        this.notConfirmedAllocationsConfig = applicationProperties.getNotConfirmedAllocationsConfig();
        this.stanziamentoCodeGenerator = stanziamentoCodeGenerator;
    }

    @Transactional
    public void processSource(String source, Instant from){
        log.info("Processing temporary allocation not confirmed from {}  by source {} ",from, source);
        List<Stanziamento> stanziamentiDaInviareNav = new ArrayList<>();
        //data di arrivo dell'ultimo file
        Map<String,Integer> codRaggruppamentoGiorniMap = notConfirmedAllocationsConfig.getArticleGroupExpirations().stream().filter(a->a.getSourceType().equals(source))
                .collect(toMap(NotConfirmedAllocationsConfig.ArticleGroupExpiration::getArticleGroup, NotConfirmedAllocationsConfig.ArticleGroupExpiration::getExpiration));
        if(!codRaggruppamentoGiorniMap.isEmpty()) {
            List<DettaglioStanziamentoPedaggioEntity> dettagliStanziamento = dettaglioStanziamentiRepository.findDettagliStanziamentiProvvisoriBySorgenteAndCodRaggruppamentiBeforeDate(source,
                    codRaggruppamentoGiorniMap,
                    from);
            log.info("Found {} not confirmed allocations ",dettagliStanziamento.size());
            dettagliStanziamento
                    .forEach(ds -> {
                        //clona dettaglio stanziamento
                        DettaglioStanziamentoPedaggioEntity conguaglioDettaglioStanziamento = (DettaglioStanziamentoPedaggioEntity) clone(ds);
                        //clona lo stanziamento associato, collegandolo allo stanziamento sorgente e lo imposta come conguaglio
                        StanziamentoEntity stanziamento = ds.getStanziamenti().stream().findFirst().get();
                        StanziamentoEntity conguaglioStanziamento = cloneStanziamentoAndLinkToProvvisorio(stanziamento);
                        conguaglioStanziamento.setConguaglio(true);
                        //inverte l'importo del dettaglio stanziamento e lo imposta come definitivo
                        conguaglioDettaglioStanziamento = invertAmountSignAndSetDefinitivo(conguaglioDettaglioStanziamento);
                        //aggiorna il valore del nuovo stanziamento provvisorio di conguaglio
                        if (conguaglioStanziamento.getCosto() != null) {
                            conguaglioStanziamento.setCosto(conguaglioDettaglioStanziamento.getAmountNoVat());
                        }
                        if (conguaglioStanziamento.getPrezzo() != null) {
                            conguaglioStanziamento.setPrezzo(conguaglioDettaglioStanziamento.getAmountNoVat());
                        }
                        //persiste il nuovo stanziamento (provvisorio di conguaglio)
                        conguaglioStanziamento = stanziamentoRepository.create(conguaglioStanziamento);
                        conguaglioDettaglioStanziamento.getStanziamenti().add(conguaglioStanziamento);
                        //persiste vecchio dettaglio stanziamento (da P a D)
                        ds.setInvoiceType(InvoiceType.D);
                        dettaglioStanziamentiRepository.update(ds);
                        //persiste nuovo dettaglio stanziamento D
                        dettaglioStanziamentiRepository.save(conguaglioDettaglioStanziamento);

                        stanziamentiDaInviareNav.add(stanziamentoEntityMapper.toDomain(conguaglioStanziamento));

                    });
            sendStanziamentiListPaginated(stanziamentiDaInviareNav);
        }else{
            log.warn("No config found for source {} ", source);
        }
    }

    private void sendStanziamentiListPaginated(final Collection<Stanziamento> _stanziamentiToSend) {

        final int BATCH_STANZIAMENTO_TO_NAV_SIZE = 20;

        log.info("Putting into queue {} StanziamentoMessage {} elements. Only missing DataInvioNav will be sent {} elements at time", QUEUE,
                _stanziamentiToSend.size(), BATCH_STANZIAMENTO_TO_NAV_SIZE);

        final var counter = new AtomicInteger(0);
        final var stanziamentiPaginated = _stanziamentiToSend.stream()
                .collect(groupingBy(
                        stanziamento -> counter.getAndIncrement() / BATCH_STANZIAMENTO_TO_NAV_SIZE))
                .values();

        stanziamentiPaginated.forEach(singleBath -> {

            final var stanziamentiDTO = new StanziamentoDTOMapper().mapToStanziamentoDTO(singleBath);
            stanziamentiToNavJmsProducer.publish(stanziamentiDTO);
            log.info("Sent stanziamento single page (item per page {}, tot pages {})", stanziamentiDTO.size(), stanziamentiPaginated.size());
        });

    }

    private DettaglioStanziamentoEntity clone(DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoEntity) {

        return dettaglioStanziamentiRepository.cloneAndCleanId(dettaglioStanziamentoEntity,false);
    }

    private StanziamentoEntity cloneStanziamentoAndLinkToProvvisorio(StanziamentoEntity stanziamentoEntity) {
        StanziamentoEntity newStanziamentoEntity = new StanziamentoEntity(null);
        BeanUtils.copyProperties(stanziamentoEntity, newStanziamentoEntity);
        newStanziamentoEntity.refreshCode(stanziamentoCodeGenerator);
        newStanziamentoEntity.setCodiceStanziamentoProvvisorio(stanziamentoEntity.getCode());
        return newStanziamentoEntity;
    }


    private DettaglioStanziamentoPedaggioEntity invertAmountSignAndSetDefinitivo(DettaglioStanziamentoPedaggioEntity dettaglioStanziamentoPedaggioEntity) {
        dettaglioStanziamentoPedaggioEntity.setAmountExcludedVat(dettaglioStanziamentoPedaggioEntity.getAmountNoVat().multiply(new BigDecimal(-1)));
        dettaglioStanziamentoPedaggioEntity.setAmountIncludedVat(dettaglioStanziamentoPedaggioEntity.getAmountIncludingVat().multiply(new BigDecimal(-1)));
        dettaglioStanziamentoPedaggioEntity.setInvoiceType(InvoiceType.D);
        return dettaglioStanziamentoPedaggioEntity;
    }
}
