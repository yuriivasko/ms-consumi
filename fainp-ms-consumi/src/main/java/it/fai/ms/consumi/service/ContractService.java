package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.Contract;

public interface ContractService {

  Optional<Contract> findContractByNumber(@NotNull String contractUuid);

  boolean isContractActiveInDate(String _contractNumber, Instant _date);

}
