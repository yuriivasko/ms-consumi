package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;

public class Vehicle implements Serializable{
  private static final long serialVersionUID = 802102788127909018L;

  public static Vehicle newUnsafeVehicle(final String _fareClass, String _euroClass, final String _licenseId, final String _countryId) {
    final var vehicle = new Vehicle();
    vehicle.setEuroClass(_euroClass != null ? _euroClass : "");
    vehicle.setFareClass(_fareClass != null ? _fareClass : "");
    vehicle.setLicensePlate(new VehicleLicensePlate(
        _licenseId != null ? _licenseId : "",
        _countryId != null ? _countryId : "")
    );
    return vehicle;
  }

  public static Vehicle newEmptyVehicleWithFareClass(final String _fareClass) {
    final var vehicle = new Vehicle();
    vehicle.setEuroClass("");
    vehicle.setFareClass(_fareClass);
    vehicle.setLicensePlate(new VehicleLicensePlate("", ""));
    return vehicle;
  }

  private String              euroClass = "";
  private String              fareClass = "";
  private VehicleLicensePlate licensePlate;

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Vehicle obj = getClass().cast(_obj);
      res = Objects.equals(obj.euroClass, euroClass) && Objects.equals(obj.fareClass, fareClass)
            && Objects.equals(obj.licensePlate, licensePlate);
    }
    return res;
  }

  public String getEuroClass() {
    return euroClass;
  }

  public String getFareClass() {
    return fareClass;
  }

  public VehicleLicensePlate getLicensePlate() {
    return licensePlate;
  }

  @Override
  public int hashCode() {
    return Objects.hash(euroClass, fareClass, licensePlate);
  }

  public void setEuroClass(final String _euroClass) {
    euroClass = _euroClass;
  }

  public void setFareClass(final String _fareClass) {
    fareClass = _fareClass;
  }

  public void setLicensePlate(final VehicleLicensePlate _licensePlate) {
    licensePlate = _licensePlate;
  }

  public boolean isComplete(){
    return StringUtils.isNotBlank(euroClass) && StringUtils.isNotBlank(licensePlate.getCountryId()) && StringUtils.isNotBlank(licensePlate.getLicenseId());
  }

  public boolean hasLicenceId(){
    return Optional.ofNullable(licensePlate).map(l -> l.getLicenseId()).filter(s -> StringUtils.isNotBlank(s)).isPresent();
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("euroClass=")
                              .append(euroClass)
                              .append(",fareClass=")
                              .append(fareClass)
                              .append(",")
                              .append(licensePlate)
                              .append("]")
                              .toString();
  }

}
