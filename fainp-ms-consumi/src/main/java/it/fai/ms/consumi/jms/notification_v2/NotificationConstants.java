package it.fai.ms.consumi.jms.notification_v2;

public class NotificationConstants {
  public static final Long NO_ROW = -1l;
  public static final String NO_RECORD_UUID = "RECORD_UUID_NOT_AVAILABLE";
  public static final String NO_RECORD = "RECORD_NOT_AVAILABLE";

}
