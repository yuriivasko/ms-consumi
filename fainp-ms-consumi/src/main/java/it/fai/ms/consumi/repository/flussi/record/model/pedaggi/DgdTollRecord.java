package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

import javax.money.MonetaryAmount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryVatRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;

public class DgdTollRecord //AutostradeCHRecord
  extends CommonRecord
  implements MonetaryVatRecord, GlobalIdentifierBySupplier {

  private static final long serialVersionUID = 156762402739692303L;
  public static final String DEFAULT_CURRENCY = "CHF";

  private String tollPointCodeEntry;
  private String tollPointNameEntry;
  private String tollPointCodeExit;
  private String tollPointNameExit;
  private String dateIn;
  private String timeIn;
  private String dateOut;
  private String timeOut;
  private String cardNumber;
  private String currency;
  private String sign;
  private String taxableAmount;
  private String taxAmount;
  private String taxRate;
  private String taxedAmount;
  private String documentNumber;
  private String counter;

  private String sequenceNumber;
  private String totalRows;

  private String       totalTaxableAmount;
  private String       totalTaxAmount;
  private String       totalTaxedAmount;
  private final String globalIdentifier;

  @JsonCreator
  public DgdTollRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID().toString();
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  @JsonIgnore
  @Override
  public MonetaryAmount getAmountExcludedVat() {
    String curr = ((currency == null) || currency.isEmpty() ? DEFAULT_CURRENCY : currency);
    return MonetaryUtils.strToMonetary(taxableAmount, curr);
  }

  @JsonIgnore
  @Override
  public MonetaryAmount getVatAmount() {
    String curr = ((currency == null) || currency.isEmpty() ? DEFAULT_CURRENCY : currency);
    return MonetaryUtils.strToMonetary(taxAmount, curr);
  }

  @JsonIgnore
  @Override
  public MonetaryAmount getAmountIncludedVat() {
    String curr = ((currency == null) || currency.isEmpty() ? DEFAULT_CURRENCY : currency);
    return MonetaryUtils.strToMonetary(taxableAmount, curr);
  }

  @JsonIgnore
  @Override
  public MonetaryAmount getTotalAmountVatExcluded() {
    String curr = ((currency == null) || currency.isEmpty() ? DEFAULT_CURRENCY : currency);
    return MonetaryUtils.strToMonetary(totalTaxableAmount, curr);
  }

  @JsonIgnore
  @Override
  public MonetaryAmount getTotalVatAmount() {
    String curr = ((currency == null) || currency.isEmpty() ? DEFAULT_CURRENCY : currency);
    return MonetaryUtils.strToMonetary(totalTaxAmount, curr);
  }

  @JsonIgnore
  @Override
  public MonetaryAmount getTotalAmountVatIncluded() {
    String curr = ((currency == null) || currency.isEmpty() ? DEFAULT_CURRENCY : currency);
    return MonetaryUtils.strToMonetary(totalTaxedAmount, curr);
  }

  @Override
  public BigDecimal getVatPercRate() {
    return taxRate == null ? null : NumericUtils.strToBigDecimal(taxRate, 2);
  }

  public String getTollPointCodeEntry() {
    return tollPointCodeEntry;
  }

  public void setTollPointCodeEntry(String tollPointCodeEntry) {
    this.tollPointCodeEntry = tollPointCodeEntry;
  }

  public String getTollPointNameEntry() {
    return tollPointNameEntry;
  }

  public void setTollPointNameEntry(String tollPointNameEntry) {
    this.tollPointNameEntry = tollPointNameEntry;
  }

  public String getTollPointCodeExit() {
    return tollPointCodeExit;
  }

  public void setTollPointCodeExit(String tollPointCodeExit) {
    this.tollPointCodeExit = tollPointCodeExit;
  }

  public String getTollPointNameExit() {
    return tollPointNameExit;
  }

  public void setTollPointNameExit(String tollPointNameExit) {
    this.tollPointNameExit = tollPointNameExit;
  }

  public String getDateIn() {
    return dateIn;
  }

  public void setDateIn(String dateIn) {
    this.dateIn = dateIn;
  }

  public String getTimeIn() {
    return timeIn;
  }

  public void setTimeIn(String timeIn) {
    this.timeIn = timeIn;
  }

  public String getDateOut() {
    return dateOut;
  }

  public void setDateOut(String dateOut) {
    this.dateOut = dateOut;
  }

  public String getTimeOut() {
    return timeOut;
  }

  public void setTimeOut(String timeOut) {
    this.timeOut = timeOut;
  }

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getTaxableAmount() {
    return taxableAmount;
  }

  public void setTaxableAmount(String taxableAmount) {
    this.taxableAmount = taxableAmount;
  }

  public String getTaxAmount() {
    return taxAmount;
  }

  public void setTaxAmount(String taxAmount) {
    this.taxAmount = taxAmount;
  }

  public String getTaxRate() {
    return taxRate;
  }

  public void setTaxRate(String taxRate) {
    this.taxRate = taxRate;
  }

  public String getTaxedAmount() {
    return taxedAmount;
  }

  public void setTaxedAmount(String taxedAmount) {
    this.taxedAmount = taxedAmount;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }

  public String getCounter() {
    return counter;
  }

  public void setCounter(String counter) {
    this.counter = counter;
  }

  public String getSequenceNumber() {
    return sequenceNumber;
  }

  public void setSequenceNumber(String sequenceNumber) {
    this.sequenceNumber = sequenceNumber;
  }

  public String getTotalRows() {
    return totalRows;
  }

  public void setTotalRows(String totalRows) {
    this.totalRows = totalRows;
  }

  public String getTotalTaxableAmount() {
    return totalTaxableAmount;
  }

  public void setTotalTaxableAmount(String totalTaxableAmount) {
    this.totalTaxableAmount = totalTaxableAmount;
  }

  public String getTotalTaxAmount() {
    return totalTaxAmount;
  }

  public void setTotalTaxAmount(String totalTaxAmount) {
    this.totalTaxAmount = totalTaxAmount;
  }

  public String getTotalTaxedAmount() {
    return totalTaxedAmount;
  }

  public void setTotalTaxedAmount(String totalTaxedAmount) {
    this.totalTaxedAmount = totalTaxedAmount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    DgdTollRecord that = (DgdTollRecord) o;
    return Objects.equals(tollPointCodeEntry, that.tollPointCodeEntry) && Objects.equals(tollPointNameEntry, that.tollPointNameEntry)
           && Objects.equals(tollPointCodeExit, that.tollPointCodeExit) && Objects.equals(tollPointNameExit, that.tollPointNameExit)
           && Objects.equals(dateIn, that.dateIn) && Objects.equals(timeIn, that.timeIn) && Objects.equals(dateOut, that.dateOut) && Objects
             .equals(timeOut, that.timeOut) && Objects.equals(cardNumber, that.cardNumber) && Objects.equals(currency, that.currency)
           && Objects.equals(sign, that.sign) && Objects.equals(taxableAmount, that.taxableAmount) && Objects.equals(taxAmount,
                                                                                                                     that.taxAmount)
           && Objects.equals(taxRate, that.taxRate) && Objects.equals(taxedAmount, that.taxedAmount) && Objects.equals(
      documentNumber, that.documentNumber) && Objects.equals(counter, that.counter) && Objects.equals(sequenceNumber, that.sequenceNumber)
           && Objects.equals(totalRows, that.totalRows) && Objects.equals(totalTaxableAmount, that.totalTaxableAmount) && Objects.equals(
      totalTaxAmount, that.totalTaxAmount) && Objects.equals(totalTaxedAmount, that.totalTaxedAmount) && Objects.equals(
      globalIdentifier, that.globalIdentifier);
  }

  @Override
  public int hashCode() {

    return Objects.hash(tollPointCodeEntry, tollPointNameEntry, tollPointCodeExit, tollPointNameExit, dateIn, timeIn, dateOut, timeOut,
                        cardNumber, currency, sign, taxableAmount, taxAmount, taxRate, taxedAmount, documentNumber, counter,
                        sequenceNumber, totalRows, totalTaxableAmount, totalTaxAmount, totalTaxedAmount, globalIdentifier);
  }

  @Override
  public String toString() {
    return "DgdTollRecord{" + "tollPointCodeEntry='" + tollPointCodeEntry + '\'' + ", tollPointNameEntry='" + tollPointNameEntry + '\''
           + ", tollPointCodeExit='" + tollPointCodeExit + '\'' + ", tollPointNameExit='" + tollPointNameExit + '\'' + ", dateIn='" + dateIn
           + '\'' + ", timeIn='" + timeIn + '\'' + ", dateOut='" + dateOut + '\'' + ", timeOut='" + timeOut + '\'' + ", cardNumber='"
           + cardNumber + '\'' + ", currency='" + currency + '\'' + ", sign='" + sign + '\'' + ", taxableAmount='" + taxableAmount + '\''
           + ", taxAmount='" + taxAmount + '\'' + ", taxRate='" + taxRate + '\'' + ", taxedAmount='" + taxedAmount + '\''
           + ", documentNumber='" + documentNumber + '\'' + ", counter='" + counter + '\'' + ", sequenceNumber='" + sequenceNumber + '\''
           + ", totalRows='" + totalRows + '\'' + ", totalTaxableAmount='" + totalTaxableAmount + '\'' + ", totalTaxAmount='"
           + totalTaxAmount + '\'' + ", totalTaxedAmount='" + totalTaxedAmount + '\'' + ", globalIdentifier='"
           + globalIdentifier + '\'' + "} " + super.toString();
  }
}


