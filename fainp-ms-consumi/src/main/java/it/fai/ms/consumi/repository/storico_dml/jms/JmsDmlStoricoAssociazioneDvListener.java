package it.fai.ms.consumi.repository.storico_dml.jms;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.efservice.dto.AssociazioneDvDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractMappableDmlListener;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.consumi.repository.storico_dml.StoricoAssociazioneDispositivoVeicoloRepository;
import it.fai.ms.consumi.repository.storico_dml.jms.mapper.StoricoAssociazioneDvDmlListenerMapper;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAssociazioneDispositivoVeicolo;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsDmlStoricoAssociazioneDvListener extends AbstractMappableDmlListener<AssociazioneDvDMLDTO, StoricoAssociazioneDispositivoVeicolo> implements JmsTopicListener {

  public JmsDmlStoricoAssociazioneDvListener(StoricoAssociazioneDispositivoVeicoloRepository repository,
                                             StoricoAssociazioneDvDmlListenerMapper mapper, ApplicationEventPublisher publisher) {
    super(AssociazioneDvDMLDTO.class, repository, mapper, publisher, false);
  }
  
  @Value("${application.isIntilialLoading:}")
  private String isInitialLoading;


  @Override
  public JmsTopicNames getTopicName() {
	  if ("true".equalsIgnoreCase(isInitialLoading)){
		  return JmsTopicNames.NO_LISTENER_2;
	  }
    return JmsTopicNames.DML_ASSOCIAZIONE_DV_SAVE;
  }
}
