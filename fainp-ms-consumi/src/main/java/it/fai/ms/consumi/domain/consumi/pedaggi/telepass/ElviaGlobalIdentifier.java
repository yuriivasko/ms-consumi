package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import java.util.UUID;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class ElviaGlobalIdentifier extends GlobalIdentifier {

  public ElviaGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.EXTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    return new ElviaGlobalIdentifier(UUID.randomUUID()
                                         .toString());
  }

}
