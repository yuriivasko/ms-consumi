package it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd;

import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface TrackyCardCarbStdProcessor
  extends ConsumoProcessor<TrackyCardCarburantiStandard> {

}
