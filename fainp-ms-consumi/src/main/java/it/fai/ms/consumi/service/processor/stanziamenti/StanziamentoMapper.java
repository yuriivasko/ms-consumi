package it.fai.ms.consumi.service.processor.stanziamenti;

import java.time.LocalDate;
import java.time.ZoneId;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.VehicleSupplier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.treno.DettaglioStanziamentoTreno;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

@Component
public abstract class StanziamentoMapper {

  private StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator;

  public StanziamentoMapper(StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator) {
    this.stanziamentoCodeGenerator = stanziamentoCodeGenerator;
  }

  private GeneraStanziamento mapFrom(final CostoRicavo _costoRicavo) {
    GeneraStanziamento generaStanziamento = null;
    switch (_costoRicavo) {
    case C:
      generaStanziamento = GeneraStanziamento.COSTO;
      break;
    case R:
      generaStanziamento = GeneraStanziamento.RICAVO;
      break;
    default:
      generaStanziamento = GeneraStanziamento.COSTO_RICAVO;
      break;
    }
    return generaStanziamento;
  }

  // public StanziamentoCodeGeneratorInterface getStanziamentoCodeGenerator() {
  // return stanziamentoCodeGenerator;
  // }

  public Stanziamento toStanziamento(@NotNull final DettaglioStanziamento _allocationDetail, final StanziamentiParams _params) {

    final var allocation = new Stanziamento(stanziamentoCodeGenerator.generate(_allocationDetail.getInvoiceType()));

    allocation.setYear(_allocationDetail.extractYear());
    allocation.setGeneraStanziamento(mapFrom(_params.getCostoRicavo()));
    allocation.setArticleCode(_params.getArticle()
                                     .getCode());
    allocation.setNumeroCliente(_allocationDetail.getCustomer()
                                                 .getId());
    allocation.setNumeroFornitore(_allocationDetail.getSupplier()
                                                   .getCode());
    allocation.setPaese(_params.getArticle()
                               .getCountry());
    allocation.setSupplierDocument(_allocationDetail.getSupplier()
                                                 .getDocument());


    if (_allocationDetail.getAmount() != null) {
      allocation.setCosto(_allocationDetail.getAmount()
                                           .getAmountExcludedVatBigDecimal());
      allocation.setPrezzo(_allocationDetail.getAmount()
                                            .getAmountExcludedVatBigDecimal());
    }
    allocation.setDataErogazioneServizio(LocalDate.ofInstant(_allocationDetail.getDate(), ZoneId.of("Europe/Rome")));
    allocation.setDataAcquisizioneFlusso(_allocationDetail.getSource().getAcquisitionDate());
    allocation.setInvoiceType(_allocationDetail.getInvoiceType());

    allocation.setAmountCurrencyCode(_params.getArticle()
                                            .getCurrency()
                                            .getCurrencyCode());

    if (_allocationDetail instanceof VehicleSupplier) {
      allocation.setVehicleEuroClass(((VehicleSupplier) _allocationDetail).getVehicle()
                                                                          .getFareClass());
      allocation.setVehicleLicensePlate(((VehicleSupplier) _allocationDetail).getVehicle()
                                                                             .getLicensePlate()
                                                                             .getLicenseId());
      allocation.setVehicleCountry(((VehicleSupplier) _allocationDetail).getVehicle()
                                                                        .getLicensePlate()
                                                                        .getCountryId());
    }

    // pedaggi

    if (_allocationDetail instanceof DettaglioStanziamentoPedaggio) {

      // spostato in processor
      if (!_allocationDetail.hasTheSameCurrency(_params.getArticle()
                                                       .getCurrency()
                                                       .getCurrencyCode())) {
        throw new RuntimeException(String.format("Wrong currecy found! Conversion service has not been called!! DettaglioStanziamentoPedaggio: [%s], StanziamentiParams: [%s]",
                                                 (DettaglioStanziamentoPedaggio) _allocationDetail, _params));
      }
    }

    // carburanti

    if (_allocationDetail instanceof DettaglioStanziamentoCarburante) {
      DettaglioStanziamentoCarburante dsc = (DettaglioStanziamentoCarburante) _allocationDetail;

      allocation.setCosto(dsc.getCostComputed() != null ? dsc.getCostComputed()
                                                             .getAmountExcludedVatBigDecimal()
                                                        : null);
      allocation.setPrezzo(dsc.getPriceComputed() != null ? dsc.getPriceComputed()
                                                               .getAmountExcludedVatBigDecimal()
                                                          : null);

      if (allocation.getCosto() != null && allocation.getPrezzo() == null)
        allocation.setGeneraStanziamento(GeneraStanziamento.COSTO);
      else if (allocation.getCosto() == null && allocation.getPrezzo() != null)
        allocation.setGeneraStanziamento(GeneraStanziamento.RICAVO);
      else
        allocation.setGeneraStanziamento(GeneraStanziamento.COSTO_RICAVO);

      allocation.setQuantity(dsc.getQuantity());

    } else if (_allocationDetail instanceof DettaglioStanziamentoGenerico) {
      DettaglioStanziamentoGenerico dsg = (DettaglioStanziamentoGenerico) _allocationDetail;
      if (dsg.getVehicle() != null) {
        allocation.setVehicleEuroClass(dsg.getVehicle()
                                          .getEuroClass());
        allocation.setVehicleLicensePlate((dsg.getVehicle()
                                              .getLicensePlate() != null) ? dsg.getVehicle()
                                                                               .getLicensePlate()
                                                                               .getLicenseId()
                                                                          : null);
        allocation.setVehicleCountry((dsg.getVehicle()
          .getLicensePlate() != null) ? dsg.getVehicle()
          .getLicensePlate()
          .getCountryId()
          : null);
      }
      
      allocation.setQuantity(dsg.getQuantity());

    } else if (_allocationDetail instanceof DettaglioStanziamentoTreno) {
      // TODO Inmplentare per DettaglioStanziamentoTreno
    }

    return allocation;
  }

}

