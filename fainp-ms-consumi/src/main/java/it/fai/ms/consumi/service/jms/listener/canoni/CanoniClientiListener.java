package it.fai.ms.consumi.service.jms.listener.canoni;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.fattura.canoni.EsitoCanoni;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoni;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoniDispositivi;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoniServizi;
import it.fai.ms.consumi.repository.canoni.CanoniRepository;
import it.fai.ms.consumi.service.canoni.CanoniClientService;
import it.fai.ms.consumi.service.canoni.CanoniService;
import it.fai.ms.consumi.service.canoni.DispositivoForCanoni;

@Service
@Transactional
@Profile("!no-jmslistener")
public class CanoniClientiListener implements JmsQueueListener {
  private Logger _log = LoggerFactory.getLogger(getClass());

  protected final CanoniService       canoniService;
  protected final CanoniRepository    canoniRepository;
  protected final NotificationService notificationService;
  protected final CanoniClientService client;

  public CanoniClientiListener(CanoniService canoniService, CanoniRepository canoniRepository,
                               NotificationService notificationService, CanoniClientService client) {
    this.canoniService       = canoniService;
    this.canoniRepository    = canoniRepository;
    this.notificationService = notificationService;
    this.client              = client;
  }

  @Override
  public void onMessage(Message _msg) {
    try {
      if (_msg instanceof ObjectMessage) {

        Serializable data;
        data = ((ObjectMessage) _msg).getObject();

        if (data instanceof RichiestaCanoniClientiMessage) {
          RichiestaCanoniClientiMessage msg = (RichiestaCanoniClientiMessage) data;
          _log.info("execute canoni cliente for {} ", msg);
          if(msg.getMsg() instanceof RichiestaCanoniDispositivi) {
            processDispositivi((RichiestaCanoniDispositivi) msg.getMsg(), msg.getCodiceCliente());
          }else if(msg.getMsg() instanceof RichiestaCanoniServizi) {
            processServizi((RichiestaCanoniServizi) msg.getMsg(), msg.getCodiceCliente());
          }
        } else {
          _log.error("Invalid Message Datafor canoni: {}", data.getClass());
          return;
        }
      } else {
        _log.error("Invalid Message Datafor canoni: {}", _msg);
        return;
      }
    } catch (JMSException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.CONSUMI_RICHIESTA_CANONI_CLIENTE;
  }

  private void processDispositivi(RichiestaCanoniDispositivi msg, String codiceCliente) {
    Instant dataDa = msg.getDataDa().toInstant();
    Instant dataA  = msg.getDataA().toInstant();

    _log.info("processing canoni dispositivi {} for {}", msg.getIdentificativo(), codiceCliente);
    var canoniStream = canoniService.canoniDispositivi(dataDa, dataA, msg.getIdentificativo(), codiceCliente);
    canoniStream.map(t -> toEsito(t, msg, msg.getIdentificativo().name())).forEach(this::postCanoniDispositivi);
    _log.info("END processing canoni dispositivi {} for {}", msg.getIdentificativo(), codiceCliente);
  }

  private void postCanoniDispositivi(EsitoCanoni esito) {
    _log.info("send esito{}", esito);
    try {
      client.postCanoniDispositivi(esito);
    } catch (Exception e) {
      _log.error("error calling postCanoniDispositivi ", e);
      notificationService.notify("CNN-001", toMessageMap("dispositivi", esito, e));
    }
  }

  private void processServizi(RichiestaCanoniServizi msg, String codiceCliente) {
    Instant dataDa = msg.getDataDa().toInstant();
    Instant dataA  = msg.getDataA().toInstant();

    _log.info("processing canoni servizi {} for {}", msg.getIdentificativo(), codiceCliente);
    var canoniStream = canoniService.canoniServizi(dataDa, dataA, msg.getIdentificativo(), codiceCliente);
    canoniStream.map(t -> toEsito(t, msg, msg.getIdentificativo().name())).forEach(this::postCanoniServizi);
  }

  private void postCanoniServizi(EsitoCanoni esito) {
    _log.info("send esito{}", esito);
    try {
      client.postCanoniServizi(esito);
    } catch (Exception e) {
      _log.error("error calling postCanoniDispositivi ", e);
      notificationService.notify("CNN-001", toMessageMap("servizi", esito, e));
    }
  }

  protected EsitoCanoni toEsito(DispositivoForCanoni device, RichiestaCanoni msg, String identificativo) {
    EsitoCanoni es = device.toEsitoCanoni();
    es.setDataDa(msg.getDataDa());
    es.setDataA(msg.getDataA());
    es.setIdentificativo(identificativo);
    es.setTransactionId(msg.getTransactionId());
    return es;
  }

  protected Map<String, Object> toMessageMap(String tipo, EsitoCanoni esito, Exception e) {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("tipo", tipo);
    map.put("cliente", esito.getCodiceCliente());
    map.put("identificativo", esito.getIdentificativo());
    map.put("dettagli", e.getMessage());
    return map;
  }

}