package it.fai.ms.consumi.repository.device;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo_;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto_;

@Repository
@Validated
@Transactional
@Primary
public class DeviceRepositoryImpl implements DeviceRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final EntityManager      em;
  private final DeviceEntityMapper entityMapper;

  @Inject
  public DeviceRepositoryImpl(final EntityManager _entityManager, final DeviceEntityMapper _entityMapper) {
    em           = _entityManager;
    entityMapper = _entityMapper;
  }

  @Override
  @Cacheable(value = "storicoDispositivo", key = "{#root.methodName, #root.args[0], #root.args[1], #root.args[2]}")
  public Optional<Device> findDeviceBySeriale(@NotNull final String _deviceSeriale, @NotNull final TipoDispositivoEnum _devicetype, final String _contractNumber) {

    _log.debug(" Searching {} by seriale {} type {} contract {}", Device.class.getSimpleName(), _deviceSeriale, _devicetype, _contractNumber);

    var query          = getQueryBySeriale(_deviceSeriale, _devicetype, _contractNumber);
    var optionalDevice = em.createQuery(query)
        .setFirstResult(0) // offset
        .setMaxResults(1)
      .getResultStream()
      .filter(storicoStatoDispositivo -> storicoStatoDispositivo != null)
      .map(storicoStatoDispositivo -> entityMapper.toDomain(storicoStatoDispositivo))
      .findFirst();

    // WORKAROUND per flusso ELCEU/ELCIT che non riporta il tipo dispositivo -> l'evoluzione è capire del OBU number il
    // tipo dispositivo
    if (!optionalDevice.isPresent() && _devicetype == TipoDispositivoEnum.TELEPASS_EUROPEO) {
      query          = getQueryBySeriale(_deviceSeriale, TipoDispositivoEnum.TELEPASS_EUROPEO_SAT, _contractNumber);
      optionalDevice = em.createQuery(query).setFirstResult(0).setMaxResults(1)
        .getResultStream()
        .map(storicoStatoDispositivo -> entityMapper.toDomain(storicoStatoDispositivo))
        .findFirst();
    }

    _log.debug(" Found : {}", optionalDevice);

    return optionalDevice;
  }

  /*
   * public Optional<Device> findDeviceBySeriale2(@NotNull final String _deviceSeriale, @NotNull final
   * TipoDispositivoEnum _devicetype) { _log.debug(" Searching {} by seriale {} type {}...",
   * Device.class.getSimpleName(), _deviceSeriale, _devicetype); var query =
   * getQueryBySeriale(_deviceSeriale,_devicetype); var device = query.getSingleResult(); if(device != null) { var
   * queryC = getQueryByContractId(device.getContrattoDmlUniqueIdentifier(), device.getDmlRevisionTimestamp()); var
   * contract = queryC.getSingleResult(); if(contract!=null) { return Optional.ofNullable(entityMapper.toDomain(device,
   * contract)); } }else if (_devicetype == TipoDispositivoEnum.TELEPASS_EUROPEO) { // WORKAROUND per flusso ELCEU/ELCIT
   * che non riporta il tipo dispositivo -> l'evoluzione è capire del OBU number il tipo dispositivo query =
   * getQueryBySeriale(_deviceSeriale,TipoDispositivoEnum.TELEPASS_EUROPEO_SAT); device = query.getSingleResult();
   * if(device != null) { var queryC = getQueryByContractId(device.getContrattoDmlUniqueIdentifier(),
   * device.getDmlRevisionTimestamp()); var contract = queryC.getSingleResult(); if(contract!=null) { return
   * Optional.ofNullable(entityMapper.toDomain(device, contract)); } } } return Optional.empty(); }
   */

  @Override
  public Optional<Device> findDeviceByPan(String _pan, TipoServizioEnum _servicetype, final String _contractNumber) {
    _log.debug(" Searching {} by pan {} type {}...", Device.class.getSimpleName(), _pan, _servicetype);

    var query          = getQueryByPan(_pan, _servicetype, _contractNumber);
    var optionalDevice = query.setFirstResult(0).setMaxResults(1).getResultStream()
      .filter(storicoStatoDispositivo -> storicoStatoDispositivo != null)
      .map(storicoStatoDispositivo -> entityMapper.toDomain(storicoStatoDispositivo))
      .findFirst();

    _log.debug(" Found : {}", optionalDevice);

    return optionalDevice;
  }

  @Override
  public Optional<Device> findDeviceByLicensePlate(String _licensePlate, TipoDispositivoEnum _devicetype, Instant _associationDate) {
    _log.debug(" Searching {} by licensePlate {} type {}...", Device.class.getSimpleName(), _licensePlate, _devicetype);

    var query          = getQueryByLicensePlate(_licensePlate, _devicetype, _associationDate);
    var optionalDevice = em.createQuery(query)
        .setFirstResult(0).setMaxResults(1)
      .getResultStream()
      .filter(storicoStatoDispositivo -> storicoStatoDispositivo != null)
      .map(storicoStatoDispositivo -> entityMapper.toDomain(storicoStatoDispositivo))
      .findFirst();

    // WORKAROUND per flusso ELCEU/ELCIT che non riporta il tipo dispositivo -> l'evoluzione è capire del OBU number il
    // tipo dispositivo
    if (!optionalDevice.isPresent() && _devicetype == TipoDispositivoEnum.TELEPASS_EUROPEO) {
      query          = getQueryByLicensePlate(_licensePlate, TipoDispositivoEnum.TELEPASS_EUROPEO_SAT, _associationDate);
      optionalDevice = em.createQuery(query)
          .setFirstResult(0).setMaxResults(1)
        .getResultStream()
        .map(storicoStatoDispositivo -> entityMapper.toDomain(storicoStatoDispositivo))
        .findFirst();
    }

    _log.debug(" Found : {}", optionalDevice);

    return optionalDevice;
  }

  private final static String QUERY_BY_PAN = "SELECT d from ViewStoricoDispositivoVeicoloContratto d, StoricoStatoServizio s where d.dispositivoDmlUniqueIdentifier = s.dispositivoDmlUniqueIdentifier"
                                             + " and s.tipoServizio = :tipoServizio and s.pan = :pan ORDER BY d."
                                             + ViewStoricoDispositivoVeicoloContratto_.DATA_VARIAZIONE + " DESC, d."
                                             + ViewStoricoDispositivoVeicoloContratto_.CONTRATTO_DML_REVISION_TIMESTAMP + " DESC, d."
                                             + ViewStoricoDispositivoVeicoloContratto_.DATA_ASSOCIAZIONE_VEICOLO + " DESC, d."
                                             + ViewStoricoDispositivoVeicoloContratto_.VEICOLO_DML_REVISION_TIMESTAMP + " DESC";
  
  private final static String QUERY_BY_PAN_CONTRACT = "SELECT d from ViewStoricoDispositivoVeicoloContratto d, StoricoStatoServizio s where d.dispositivoDmlUniqueIdentifier = s.dispositivoDmlUniqueIdentifier"
          + " and s.tipoServizio = :tipoServizio and s.pan = :pan and d.contrattoNumero = :contrattoNumero ORDER BY d."
          + ViewStoricoDispositivoVeicoloContratto_.DATA_VARIAZIONE + " DESC, d."
          + ViewStoricoDispositivoVeicoloContratto_.CONTRATTO_DML_REVISION_TIMESTAMP + " DESC, d."
          + ViewStoricoDispositivoVeicoloContratto_.DATA_ASSOCIAZIONE_VEICOLO + " DESC, d."
          + ViewStoricoDispositivoVeicoloContratto_.VEICOLO_DML_REVISION_TIMESTAMP + " DESC";

  private TypedQuery<ViewStoricoDispositivoVeicoloContratto> getQueryByPan(final String _pan, final TipoServizioEnum _servicetype, final String _contractNumber) {
    var query = _contractNumber== null ? 
    	em.createQuery(QUERY_BY_PAN, ViewStoricoDispositivoVeicoloContratto.class)
	      .setParameter("tipoServizio", _servicetype.name())
	      .setParameter("pan", _pan) :
	    em.createQuery(QUERY_BY_PAN_CONTRACT, ViewStoricoDispositivoVeicoloContratto.class)
		  .setParameter("tipoServizio", _servicetype.name())
		  .setParameter("pan", _pan)
	      .setParameter("contrattoNumero", _contractNumber);
    return query;
  }

  private CriteriaQuery<ViewStoricoDispositivoVeicoloContratto> getQueryByLicensePlate(final String _licensePlate,
                                                                                       final TipoDispositivoEnum _devicetype,
                                                                                       final Instant _associationDate) {
    var query = em.getCriteriaBuilder()
      .createQuery(ViewStoricoDispositivoVeicoloContratto.class);
    var root  = query.from(ViewStoricoDispositivoVeicoloContratto.class);

    final var predicates = List.of(em.getCriteriaBuilder()
      .equal(root.get(ViewStoricoDispositivoVeicoloContratto_.VEICOLO_TARGA), _licensePlate),
                                   em.getCriteriaBuilder()
                                     .equal(root.get(ViewStoricoDispositivoVeicoloContratto_.TIPO_DISPOSITIVO), _devicetype),
                                   em.getCriteriaBuilder()
                                     .lessThanOrEqualTo(root.get(ViewStoricoDispositivoVeicoloContratto_.DATA_ASSOCIAZIONE_VEICOLO),
                                                        _associationDate))
      .toArray(new Predicate[2]);

    return createQuery(em.getCriteriaBuilder(), root, query, predicates);
  }

  private CriteriaQuery<ViewStoricoDispositivoVeicoloContratto> getQueryBySeriale(final String _deviceSeriale,
                                                                                  final TipoDispositivoEnum _devicetype, 
                                                                                  final String _contractNumber) {
    var query = em.getCriteriaBuilder()
      .createQuery(ViewStoricoDispositivoVeicoloContratto.class);
    var root  = query.from(ViewStoricoDispositivoVeicoloContratto.class);
    
    Predicate[] predicates = new Predicate[_contractNumber==null ? 2 : 3];
    predicates[0] = em.getCriteriaBuilder().equal(root.get(ViewStoricoDispositivoVeicoloContratto_.SERIALE_DISPOSITIVO), _deviceSeriale);
    predicates[1] = em.getCriteriaBuilder().equal(root.get(ViewStoricoDispositivoVeicoloContratto_.TIPO_DISPOSITIVO), _devicetype);
    if(_contractNumber!=null) {
    	predicates[2] = em.getCriteriaBuilder().equal(root.get(ViewStoricoDispositivoVeicoloContratto_.CONTRATTO_NUMERO), _contractNumber);
    }

    return createQuery(em.getCriteriaBuilder(), root, query, predicates);
  }

  private CriteriaQuery<ViewStoricoDispositivoVeicoloContratto> createQuery(CriteriaBuilder cb,
                                                                            Root<ViewStoricoDispositivoVeicoloContratto> root,
                                                                            CriteriaQuery<ViewStoricoDispositivoVeicoloContratto> query,
                                                                            Predicate[] predicates) {
    Order[] ord = new Order[4];
    ord[0] = cb.desc(root.get(ViewStoricoDispositivoVeicoloContratto_.DATA_VARIAZIONE));
    ord[1] = cb.desc(root.get(ViewStoricoDispositivoVeicoloContratto_.CONTRATTO_DML_REVISION_TIMESTAMP));
    ord[2] = cb.desc(root.get(ViewStoricoDispositivoVeicoloContratto_.DATA_ASSOCIAZIONE_VEICOLO));
    ord[3] = cb.desc(root.get(ViewStoricoDispositivoVeicoloContratto_.VEICOLO_DML_REVISION_TIMESTAMP));

    return query.where(predicates)
      .orderBy(ord);
  }

  @Override
  public Optional<StoricoDispositivo> findDeviceAtDate(String deviceID, Instant dataRiferimentoInizio) {
    _log.debug(" Searching {} by id {} date {}...", Device.class.getSimpleName(), deviceID, dataRiferimentoInizio);


    var cb = em.getCriteriaBuilder();

    CriteriaQuery<StoricoDispositivo> query = cb.createQuery(StoricoDispositivo.class);
    Root<StoricoDispositivo>          root  = query.from(StoricoDispositivo.class);

    query.where(cb.and(cb.equal(root.get(StoricoDispositivo_.DML_UNIQUE_IDENTIFIER), deviceID),
                       cb.lessThanOrEqualTo(root.get(StoricoDispositivo_.dmlRevisionTimestamp), dataRiferimentoInizio)));
    query.orderBy(cb.desc(root.get(StoricoDispositivo_.dmlRevisionTimestamp)));
    return em.createQuery(query).setFirstResult(0).setMaxResults(1).getResultStream().findFirst();


  }

  // @Override
  // public Optional<StoricoStatoServizio> findStoricoStatoServizioByDevice(String _dispositivoDmlUniqueIdentifier,
  // TipoServizioEnum _servicetype,
  // Instant _date) {
  // _log.debug(" Searching {} by dispositivoDmlUniqueIdentifier {} type {}...", Device.class.getSimpleName(),
  // _dispositivoDmlUniqueIdentifier, _servicetype);
  //
  //
  // var query = getQueryServiceByDevice(_dispositivoDmlUniqueIdentifier, _servicetype, _date);
  // var optionalStoricoStatoServizio = query
  // .getResultStream()
  // .filter(storicoStatoServizio -> storicoStatoServizio != null)
  // .findFirst();
  //
  // _log.debug(" Found : {}", optionalStoricoStatoServizio);
  // return optionalStoricoStatoServizio;
  // }

  // private final static String QUERY_SERVICE_BY_DEVICE =
  // "SELECT s from ViewStoricoDispositivoVeicoloContratto d, StoricoStatoServizio s "
  // + "where d.dispositivoDmlUniqueIdentifier = s.dispositivoDmlUniqueIdentifier"
  // +" and s.tipoServizio = :tipoServizio "
  // + "and d.dispositivoDmlUniqueIdentifier = :dispositivoDmlUniqueIdentifier "
  // +" and d."+ViewStoricoDispositivoVeicoloContratto_.DATA_VARIAZIONE+" <= :date "
  // + "ORDER BY d."+ViewStoricoDispositivoVeicoloContratto_.DATA_VARIAZIONE+", "
  // + "d."+ViewStoricoDispositivoVeicoloContratto_.CONTRATTO_DML_REVISION_TIMESTAMP+", "
  // + "d."+ViewStoricoDispositivoVeicoloContratto_.VEICOLO_DML_REVISION_TIMESTAMP;
  //
  // private TypedQuery<StoricoStatoServizio> getQueryServiceByDevice(final String _dispositivoDmlUniqueIdentifier,
  // final TipoServizioEnum _servicetype,
  // Instant _date) {
  // var query = em.createQuery(QUERY_SERVICE_BY_DEVICE, StoricoStatoServizio.class)
  // .setParameter("dispositivoDmlUniqueIdentifier", _dispositivoDmlUniqueIdentifier)
  // .setParameter("date", _date)
  // .setParameter("tipoServizio", _servicetype.name());
  // return query;
  // }
}
