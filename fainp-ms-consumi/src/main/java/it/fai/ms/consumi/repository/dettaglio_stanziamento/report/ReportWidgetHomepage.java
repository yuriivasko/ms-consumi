package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ReportWidgetHomepage {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public ReportWidgetHomepage() {
  }

  public List<String> getTotalToInvoice(final EntityManager em, String codiceCliente, Set<String> article) {
    log.info("TotalToll -> CodiceCliente: {}, ArticleCodes: {}", codiceCliente, article);
    String articleCodes = null;
    if (article != null && !article.isEmpty()) {
      articleCodes = String.join(", ", article.stream()
                                              .map(code -> "'" + code + "'")
                                              .collect(toList()));
    }

    List<WidgetDaFatturareDTO> totaliDaFatturare = new ArrayList<>();
    totaliDaFatturare.addAll(getTotaleDaFatturareDefinitivi(em, codiceCliente, articleCodes));
    totaliDaFatturare.addAll(getTotaleDaFatturareProvvisori(em, codiceCliente, articleCodes));

    Map<String, List<WidgetDaFatturareDTO>> mapByUnitaDiMisura = totaliDaFatturare.stream()
                                                                                  .collect(groupingBy(WidgetDaFatturareDTO::getUnitaDiMisura));
    List<String> totalToInvoice = mapByUnitaDiMisura.keySet()
                                                    .stream()
                                                    .map(k -> {
                                                      List<WidgetDaFatturareDTO> valueByUnitaDiMisura = mapByUnitaDiMisura.get(k);
                                                      Integer sum = valueByUnitaDiMisura.stream()
                                                                                        .filter(dto -> dto.getAmount() != null)
                                                                                        .map(dto -> dto.getAmount())
                                                                                        .collect(Collectors.summingInt(BigDecimal::intValue));
                                                      return sum + StringUtils.SPACE + k;
                                                    })
                                                    .collect(toList());
    if(totalToInvoice == null || totalToInvoice.isEmpty()) {
      log.debug("Empty or null totalToInvoice...");
      totalToInvoice = new ArrayList<>();
      totalToInvoice.add("0");
    }
    log.info("Total Toll to invoice: {} for client: {}", totalToInvoice, codiceCliente);    
    return totalToInvoice;
  }

  private List<WidgetDaFatturareDTO> getTotaleDaFatturareDefinitivi(final EntityManager em, final String codiceCliente,
                                                                    final String articleCodes) {
    // @formatter:off
    String sqlQuery = "SELECT SUM(prezzo * quantita) AS amount, valuta AS unitaDiMisura " 
                       + " FROM faiconsumi.dbo.stanziamento "
                       + " WHERE data_fattura IS NULL " 
                       + " AND nr_cliente =  '" + codiceCliente + "' " 
                       + " AND stato_stanziamento = 'D' "
                       + ((articleCodes != null) ? " AND nr_articolo IN (" + articleCodes + ") " : " ") 
                       + " GROUP BY valuta;";
    // @formatter:on

    List<WidgetDaFatturareDTO> results = getResultTotalToInvoice(em, sqlQuery);
    log.info("Found 'DaFatturare Definitivi': {}", results);
    return results;

  }

  private List<WidgetDaFatturareDTO> getTotaleDaFatturareProvvisori(final EntityManager em, final String codiceCliente,
                                                                    final String articleCodes) {
    // @formatter:off
    String sqlQuery = "SELECT SUM(s.prezzo * s.quantita) AS amount, s.valuta AS unitaDiMisura" 
                      + " FROM faiconsumi.dbo.stanziamento s "
                      + " LEFT OUTER JOIN faiconsumi.dbo.stanziamento s2 "
                      + " ON s.codice_stanziamento = s2.codicestanziamento_provvisorio AND s2.stato_stanziamento = 'D' " 
                      + " WHERE "
                      + " s.data_fattura IS NULL " 
                      + " AND s.nr_cliente =  '" + codiceCliente + "' " 
                      + " AND s.stato_stanziamento = 'P' "
                      + " AND s2.codice_stanziamento IS NULL " 
                      + ((articleCodes != null) ? " AND s.nr_articolo IN (" + articleCodes + ") " : " ") 
                      + " GROUP BY s.valuta;";
    // @formatter:on

    List<WidgetDaFatturareDTO> results = getResultTotalToInvoice(em, sqlQuery);
    log.info("Found 'DaFatturare Provvisori': {}", results);
    return results;
  }

  public List<WidgetDaFatturareDTO> getResultTotalToInvoice(final EntityManager em, final String sqlQuery) {
    Query query = em.createNativeQuery(sqlQuery, Tuple.class);
    @SuppressWarnings("unchecked")
    List<Tuple> tuples = (List<Tuple>) query.getResultList();
    List<WidgetDaFatturareDTO> results = new ArrayList<>();
    if (!tuples.isEmpty()) {
      List<WidgetDaFatturareDTO> definitiviDaFatturare = tuples.stream()
                                                               .map(t -> {
                                                                 BigDecimal amount = (BigDecimal) t.get("amount");
                                                                 if(amount == null) {
                                                                   amount = BigDecimal.ZERO;
                                                                 }
                                                                 
                                                                 String unitaDiMisura = (String) t.get("unitaDiMisura");
                                                                 return new WidgetDaFatturareDTO(amount, unitaDiMisura);
                                                               })
                                                               .collect(toList());
      results.addAll(definitiviDaFatturare);
    }

    return results;
  }

}
