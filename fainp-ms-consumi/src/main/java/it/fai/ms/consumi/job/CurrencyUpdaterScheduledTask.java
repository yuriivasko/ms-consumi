package it.fai.ms.consumi.job;


import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.service.impl.CurrencyRateServiceImpl;

@Component
public class CurrencyUpdaterScheduledTask {

  private final Logger log = LoggerFactory.getLogger(CurrencyUpdaterScheduledTask.class);

  @Autowired
  private CurrencyRateServiceImpl currencyRateServiceImpl;

  public CurrencyUpdaterScheduledTask(){
    log.info("Created CurrencyUpdaterScheduledTask");
  }

  @Value("${currency.inputPath}") String _inputPath;
  @Value("${currency.outputPath}") String _outputPath;
  //@Scheduled(cron = "0 0 1 * * *")  // every day at 1:00 AM
  @Scheduled(cron = "${currency.scheduling}") // every hour every day
  public void currencyUpdater() {
    log.info("Updating currency values");
    try {
      log.info("Try to reach [{}]", _inputPath);
      if(_inputPath== null || _inputPath.equals("no-path")) {
        return;
      }
      URL website = new URL(_inputPath);
      ReadableByteChannel rbc = Channels.newChannel(website.openStream());
      FileOutputStream fos = new FileOutputStream(_outputPath);
      fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
      fos.close();
      log.info("saved currency file in [{}]", _outputPath);

      currencyRateServiceImpl.readCurrencyRateFile(_outputPath);
    } catch (IOException e) {
      log.error("Error retrieving currencies: [{}]", e);
    }
  }
}
