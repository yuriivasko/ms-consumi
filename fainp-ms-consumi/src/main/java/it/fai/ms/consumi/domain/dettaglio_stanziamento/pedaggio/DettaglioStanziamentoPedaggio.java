package it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio;

import java.math.BigDecimal;

import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleConsumer;
import it.fai.ms.consumi.domain.VehicleSupplier;
import it.fai.ms.consumi.domain.consumi.DeviceConsumer;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;

public class DettaglioStanziamentoPedaggio extends DettaglioStanziamento
  implements VehicleSupplier, VehicleConsumer, DeviceSupplier, DeviceConsumer {

  private Device     device;
  private TollPoint  tollPointEntry;
  private TollPoint  tollPointExit;
  private Vehicle    vehicle;
  private String     networkCode;
  private String     routeName;
  private BigDecimal imponibileAdr;

  public DettaglioStanziamentoPedaggio(final GlobalIdentifier _globalIdentifier) {
    super(_globalIdentifier);
  }

  @Override
  public Device getDevice() {
    return device;
  }

  @Override
  public void setDevice(final Device _device) {
    device = _device;
  }

  public TollPoint getTollPointEntry() {
    return tollPointEntry;
  }

  public void setTollPointEntry(final TollPoint _tollPointEntry) {
    tollPointEntry = _tollPointEntry;
  }

  public TollPoint getTollPointExit() {
    return tollPointExit;
  }

  public void setTollPointExit(final TollPoint _tollPointExit) {
    tollPointExit = _tollPointExit;
  }

  @Override
  public Vehicle getVehicle() {
    return vehicle;
  }

  @Override
  public void setVehicle(final Vehicle _vehicle) {
    vehicle = _vehicle;
  }

  public String getNetworkCode() {
    return networkCode;
  }

  public void setNetworkCode(final String _networkCode) {
    networkCode = _networkCode;
  }

  public BigDecimal getImponibileAdr() {
    return imponibileAdr;
  }

  public void setImponibileAdr(final BigDecimal _imponibileAdr) {
    imponibileAdr = _imponibileAdr;
  }

  public String getRouteName() {
    return routeName;
  }

  public void setRouteName(final String _routeName) {
    routeName = _routeName;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      res = super.equals(_obj);
    }
    return res;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public String toString() {
    return new StringBuilder().append(super.toString())
                              .append(",")
                              .append(device)
                              .append(",")
                              .append(vehicle)
                              .append(",")
                              .append(tollPointEntry != null ? tollPointEntry : "")
                              .append(tollPointExit != null ? tollPointExit : "")
                              .append(",")
                              .append(networkCode)
                              .append(",")
                              .append(routeName)
                              .append(",")
                              .append(imponibileAdr)
                              .toString();
  }

}
