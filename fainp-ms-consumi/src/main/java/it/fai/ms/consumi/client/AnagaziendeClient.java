package it.fai.ms.consumi.client;

import static it.fai.ms.consumi.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import it.fai.ms.consumi.dto.ArticoloDTO;
import it.fai.ms.consumi.dto.FornitoreDTO;

@FeignClient(name = "faianagaziende")
public interface AnagaziendeClient {

  String BASE_PATH        = "/api";

  @GetMapping(BASE_PATH + "/articoli")
  List<ArticoloDTO> getAllArticoli(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken);

  @GetMapping(BASE_PATH + "/fornitori")
  List<FornitoreDTO> getAllFornitori(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken);

}
