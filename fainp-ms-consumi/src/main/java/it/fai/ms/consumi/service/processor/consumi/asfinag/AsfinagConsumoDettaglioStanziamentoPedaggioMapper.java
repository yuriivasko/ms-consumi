package it.fai.ms.consumi.service.processor.consumi.asfinag;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.Asfinag;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class AsfinagConsumoDettaglioStanziamentoPedaggioMapper
  extends ConsumoDettaglioStanziamentoMapper {
  
  public AsfinagConsumoDettaglioStanziamentoPedaggioMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamentoPedaggio dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof Asfinag) {
      var asfinag = (Asfinag) _consumo;
      dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggio) toDettaglioStanziamentoPedaggio(asfinag, asfinag.getGlobalIdentifier());
      dettaglioStanziamentoPedaggio.setRouteName(asfinag.getRouteName());

      dettaglioStanziamentoPedaggio.getSupplier().setDocument(asfinag.getAdditionalInfo());
    }
    return dettaglioStanziamentoPedaggio;
  }

}
