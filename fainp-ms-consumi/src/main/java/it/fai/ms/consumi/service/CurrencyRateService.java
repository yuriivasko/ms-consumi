package it.fai.ms.consumi.service;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;
import java.util.List;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.service.dto.CurrencyRateDTO;

/**
 * Service Interface for managing CurrencyRate.
 */
public interface CurrencyRateService {

    /**
     * Save a currencyRate.
     *
     * @param currencyRateDTO the entity to save
     * @return the persisted entity
     */
    CurrencyRateDTO save(CurrencyRateDTO currencyRateDTO);

    /**
     * Get all the currencyRates.
     *
     * @return the list of entities
     */
    List<CurrencyRateDTO> findAll();

    /**
     * Get the "id" currencyRate.
     *
     * @param id the id of the entity
     * @return the entity
     */
    CurrencyRateDTO findOne(Long id);

    /**
     * Delete the "id" currencyRate.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
    
    BigDecimal convertToEuroRate(BigDecimal rate, BigDecimal amount);
    
    void readCurrencyRateFile(String fname);
    
    Amount convertAmount(Amount amount, Currency targetCurrency, Instant time);

}
