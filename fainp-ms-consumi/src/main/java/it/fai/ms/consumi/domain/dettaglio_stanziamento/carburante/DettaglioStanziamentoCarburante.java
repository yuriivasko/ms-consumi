package it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante;

import java.math.BigDecimal;
import java.time.Instant;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleConsumer;
import it.fai.ms.consumi.domain.VehicleSupplier;
import it.fai.ms.consumi.domain.consumi.DeviceConsumer;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;

public class DettaglioStanziamentoCarburante extends DettaglioStanziamento implements VehicleSupplier, VehicleConsumer, DeviceSupplier, DeviceConsumer {

  private Device      device;
  private Vehicle     vehicle;
  private FuelStation fuelStation;
  private String      navSupplierCode;
  private Instant     entryDateTime;
  private String      otherSupport;
  private Integer     counter;
  private BigDecimal  quantity;
  private BigDecimal  kilometers;
  private String      measurementUnit;
  private String      ticket;
  private String      productCode;
  private Amount      priceReference;
  private Amount      priceComputed;
  private Amount      costComputed;
  private Boolean     storno;

  public String getCodiceTrackycard() {
    if(getDevice()!=null) {
      return getDevice().getSeriale()!=null ? getDevice().getSeriale() : getDevice()
                                                                 .getPan();
    }
    return null;
  }


  public DettaglioStanziamentoCarburante(final GlobalIdentifier _globalIdentifier) {
    super(_globalIdentifier);
  }

  @Override
  public Amount getAmount() {
    return priceComputed != null ? priceComputed : costComputed;
  }

  @Override
  public void setAmount(Amount amount) {
    this.priceComputed = amount;
  }

  @Override
  public Device getDevice() {
    return device;
  }

  @Override
  public void setDevice(Device _device) {
    device = _device;
  }

  @Override
  public Vehicle getVehicle() {
    return vehicle;
  }

  @Override
  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public FuelStation getFuelStation() {
    return fuelStation;
  }

  public void setFuelStation(FuelStation fuelStation) {
    this.fuelStation = fuelStation;
  }

  public String getNavSupplierCode() {
    return navSupplierCode;
  }

  public void setNavSupplierCode(String navSupplierCode) {
    this.navSupplierCode = navSupplierCode;
  }

  public Instant getEntryDateTime() {
    return entryDateTime;
  }

  public void setEntryDateTime(Instant entryDateTime) {
    this.entryDateTime = entryDateTime;
  }

  public String getOtherSupport() {
    return otherSupport;
  }

  public void setOtherSupport(String otherSupport) {
    this.otherSupport = otherSupport;
  }

  public Integer getCounter() {
    return counter;
  }

  public void setCounter(Integer counter) {
    this.counter = counter;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getKilometers() {
    return kilometers;
  }

  public void setKilometers(BigDecimal kilometers) {
    this.kilometers = kilometers;
  }

  public void setMesaurementUnit(String mesaurementUnit) {
    this.measurementUnit = mesaurementUnit;
  }

  public String getMesaurementUnit() {
    return measurementUnit;
  }

  public String getTicket() {
    return ticket;
  }

  public void setTicket(String ticket) {
    this.ticket = ticket;
  }

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public Amount getPriceReference() {
    return priceReference;
  }

  public void setPriceReference(Amount priceExposed) {
    this.priceReference = priceExposed;
  }


  public Amount getPriceComputed() {
    return priceComputed;
  }

  public void setPriceComputed(Amount priceComputed) {
    this.priceComputed = priceComputed;
  }

  public Amount getCostComputed() {
    return costComputed;
  }

  public void setCostComputed(Amount costComputed) {
    this.costComputed = costComputed;
  }

  public Boolean isStorno() {
    return storno;
  }

  public void setStorno(Boolean storno) {
    this.storno = storno;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = (prime * result) + ((costComputed == null) ? 0 : costComputed.hashCode());
    result = (prime * result) + ((counter == null) ? 0 : counter.hashCode());
    result = (prime * result) + ((device == null) ? 0 : device.hashCode());
    result = (prime * result) + ((entryDateTime == null) ? 0 : entryDateTime.hashCode());
    result = (prime * result) + ((fuelStation == null) ? 0 : fuelStation.hashCode());
    result = (prime * result) + ((kilometers == null) ? 0 : kilometers.hashCode());
    result = (prime * result) + ((measurementUnit == null) ? 0 : measurementUnit.hashCode());
    result = (prime * result) + ((quantity == null) ? 0 : quantity.hashCode());
    result = (prime * result) + ((navSupplierCode == null) ? 0 : navSupplierCode.hashCode());
    result = (prime * result) + ((otherSupport == null) ? 0 : otherSupport.hashCode());
    result = (prime * result) + ((priceComputed == null) ? 0 : priceComputed.hashCode());
    result = (prime * result) + ((priceReference == null) ? 0 : priceReference.hashCode());
    result = (prime * result) + ((productCode == null) ? 0 : productCode.hashCode());
    result = (prime * result) + ((ticket == null) ? 0 : ticket.hashCode());
    result = (prime * result) + ((vehicle == null) ? 0 : vehicle.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    DettaglioStanziamentoCarburante other = (DettaglioStanziamentoCarburante) obj;
    if (costComputed == null) {
      if (other.costComputed != null) {
        return false;
      }
    } else if (!costComputed.equals(other.costComputed)) {
      return false;
    }
    if (counter == null) {
      if (other.counter != null) {
        return false;
      }
    } else if (!counter.equals(other.counter)) {
      return false;
    }
    if (device == null) {
      if (other.device != null) {
        return false;
      }
    } else if (!device.equals(other.device)) {
      return false;
    }
    if (entryDateTime == null) {
      if (other.entryDateTime != null) {
        return false;
      }
    } else if (!entryDateTime.equals(other.entryDateTime)) {
      return false;
    }
    if (fuelStation == null) {
      if (other.fuelStation != null) {
        return false;
      }
    } else if (!fuelStation.equals(other.fuelStation)) {
      return false;
    }
    if (kilometers == null) {
      if (other.kilometers != null) {
        return false;
      }
    } else if (!kilometers.equals(other.kilometers)) {
      return false;
    }
    if (measurementUnit == null) {
      if (other.measurementUnit != null) {
        return false;
      }
    } else if (!measurementUnit.equals(other.measurementUnit)) {
      return false;
    }
    if (quantity == null) {
      if (other.quantity != null) {
        return false;
      }
    } else if (!quantity.equals(other.quantity)) {
      return false;
    }
    if (navSupplierCode == null) {
      if (other.navSupplierCode != null) {
        return false;
      }
    } else if (!navSupplierCode.equals(other.navSupplierCode)) {
      return false;
    }
    if (otherSupport == null) {
      if (other.otherSupport != null) {
        return false;
      }
    } else if (!otherSupport.equals(other.otherSupport)) {
      return false;
    }
    if (priceComputed == null) {
      if (other.priceComputed != null) {
        return false;
      }
    } else if (!priceComputed.equals(other.priceComputed)) {
      return false;
    }
    if (priceReference == null) {
      if (other.priceReference != null) {
        return false;
      }
    } else if (!priceReference.equals(other.priceReference)) {
      return false;
    }
    if (productCode == null) {
      if (other.productCode != null) {
        return false;
      }
    } else if (!productCode.equals(other.productCode)) {
      return false;
    }
    if (ticket == null) {
      if (other.ticket != null) {
        return false;
      }
    } else if (!ticket.equals(other.ticket)) {
      return false;
    }
    if (vehicle == null) {
      if (other.vehicle != null) {
        return false;
      }
    } else if (!vehicle.equals(other.vehicle)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("DettaglioStanziamentoCarburante [");
    if (device != null) {
      builder.append("device=");
      builder.append(device);
      builder.append(", ");
    }
    if (vehicle != null) {
      builder.append("vehicle=");
      builder.append(vehicle);
      builder.append(", ");
    }
    if (fuelStation != null) {
      builder.append("fuelStation=");
      builder.append(fuelStation);
      builder.append(", ");
    }
    if (navSupplierCode != null) {
      builder.append("navSupplierCode=");
      builder.append(navSupplierCode);
      builder.append(", ");
    }
    if (entryDateTime != null) {
      builder.append("entryDateTime=");
      builder.append(entryDateTime);
      builder.append(", ");
    }
    if (otherSupport != null) {
      builder.append("otherSupport=");
      builder.append(otherSupport);
      builder.append(", ");
    }
    if (counter != null) {
      builder.append("counter=");
      builder.append(counter);
      builder.append(", ");
    }
    if (quantity != null) {
      builder.append("quantity=");
      builder.append(quantity);
      builder.append(", ");
    }
    if (kilometers != null) {
      builder.append("kilometers=");
      builder.append(kilometers);
      builder.append(", ");
    }
    if (measurementUnit != null) {
      builder.append("measurementUnit=");
      builder.append(measurementUnit);
      builder.append(", ");
    }
    if (ticket != null) {
      builder.append("ticket=");
      builder.append(ticket);
      builder.append(", ");
    }
    if (productCode != null) {
      builder.append("productCode=");
      builder.append(productCode);
      builder.append(", ");
    }
    if (priceReference != null) {
      builder.append("priceReference=");
      builder.append(priceReference);
      builder.append(", ");
    }
    if (priceComputed != null) {
      builder.append("priceComputed=");
      builder.append(priceComputed);
      builder.append(", ");
    }
    if (costComputed != null) {
      builder.append("costComputed=");
      builder.append(costComputed);
      builder.append(", ");
    }
    if (storno == null) {
      builder.append("storno=");
      builder.append(storno);
      builder.append(", ");
    }
    builder.append("]");
    builder.append(super.toString());
    return builder.toString();
  }
}
