package it.fai.ms.consumi.domain;
public enum AsyncJobStatusEnum {
    PENDING, ERROR, SUCCESS;
}