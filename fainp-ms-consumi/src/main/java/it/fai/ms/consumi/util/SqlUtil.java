package it.fai.ms.consumi.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SqlUtil {

	private static final Logger log = LoggerFactory.getLogger(SqlUtil.class);

	public static final int MAX_SIZE_PARAMETER = 2090;
	
	public static <E> List<List<E>> splitToSubList(Set<E> attributes,int SIZE) {
		return splitToSubList(new ArrayList(attributes), SIZE);
	}

	public static <E> List<List<E>> splitToSubList(List<E> attributes,int SIZE) {
		int size = attributes.size();
		int chunkSize = size / SIZE;

		List<List<E>> rs = new ArrayList<>();
		for (int i = 0; i <= chunkSize; i++) {
			int start = i * SIZE;
			int end = (start + SIZE) > size ? size : start + SIZE;
			log.debug("Generate SubList from {} to {}", start, end);
			List<E> subList = attributes.subList(start, end);
			if (subList != null && !subList.isEmpty() && subList.size() > 0) {
				rs.add(subList);
			}
		}
		return rs;
	}
}
