package it.fai.ms.consumi.service.processor.stanziamenti.carburanti;

import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGeneratorInterface;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoMapper;

@Component
public class StanziamentoCarburanteMapper extends StanziamentoMapper {

  public StanziamentoCarburanteMapper(StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator) {
    super (stanziamentoCodeGenerator);
  }

}
