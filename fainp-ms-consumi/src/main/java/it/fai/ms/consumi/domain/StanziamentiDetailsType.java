package it.fai.ms.consumi.domain;

public enum StanziamentiDetailsType {

  PEDAGGI, CARBURANTI, TRENI, GENERICO;

}
