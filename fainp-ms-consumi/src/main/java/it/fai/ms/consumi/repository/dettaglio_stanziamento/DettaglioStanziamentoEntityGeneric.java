package it.fai.ms.consumi.repository.dettaglio_stanziamento;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class DettaglioStanziamentoEntityGeneric extends DettaglioStanziamentoEntity {

  @NotEmpty
  @Column(name = "currency_code", nullable = false)
  private String currencyCode = "EUR";

  @NotEmpty
  @Column(name = "codice_cliente_nav", nullable = false)
  private String customerId;

  @Column(name = "exchange_rate", precision = 16, scale = 5)
  private BigDecimal exchangeRate;

  @Column(name = "file_name")
  private String fileName;

  @NotNull
  @Column(name = "ingestion_time", nullable = false)
  private Instant ingestionTime = Instant.now();

  @Column(name = "data_acquisizione_flusso")
  private Instant dataAcquisizioneFlusso;

  @Column(name = "record_code")
  private String recordCode;

  @NotNull
  @Column(name = "sign_of_transaction", nullable = false)
  private String transactionSign = "+";

  @Column(name = "contract_code")
  private String contractCode;

  public DettaglioStanziamentoEntityGeneric(@NotNull String _globalIdentifier) {
    super(_globalIdentifier);
  }
  
  public DettaglioStanziamentoEntityGeneric(@NotNull String _globalIdentifier, Optional<UUID> id) {
    super(_globalIdentifier, id);
  }

  protected DettaglioStanziamentoEntityGeneric() {
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCustomerId() {
    return customerId;
  }

  public BigDecimal getExchangeRate() {
    return exchangeRate;
  }

  public String getFileName() {
    return fileName;
  }

  public Instant getIngestionTime() {
    return ingestionTime;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public String getTransactionSign() {
    return transactionSign;
  }

  public void setCurrencyCode(final String _currencyCode) {
    currencyCode = Optional.ofNullable(_currencyCode)
                           .orElse("EUR");
  }

  public void setCustomerId(final String _customerId) {
    customerId = _customerId;
  }

  public void setExchangeRate(final BigDecimal _exchangeRate) {
    exchangeRate = _exchangeRate;
  }

  public void setFileName(final String _fileName) {
    fileName = _fileName;
  }

  public void setIngestionTime(final Instant _ingestionTime) {
    ingestionTime = _ingestionTime;
  }

  public void setRecordCode(final String _recordCode) {
    recordCode = _recordCode;
  }

  public void setTransactionSign(final String _transactionSign) {
    transactionSign = _transactionSign;
  }

  public String getContractCode() {
    return contractCode;
  }

  public void setContractCode(final String _contractCode) {
    contractCode = _contractCode;
  }

  public Instant getDataAcquisizioneFlusso() {
    return dataAcquisizioneFlusso;
  }

  public void setDataAcquisizioneFlusso(Instant dataAcquisizioneFlusso) {
    this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
  }



}
