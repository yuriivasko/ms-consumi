package it.fai.ms.consumi.repository.storico_dml.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "storico_veicolo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoVeicolo extends DmlBaseEntity {
  private static final long serialVersionUID = -7735880640869378621L;

  @Size(max = 255)
  @Column(name = "classe_euro", length = 255)
  private String classeEuro;

  @Column(name = "targa")
  private String targa;
  
  @Column(name = "nazione", length=3)
  private String nazione;

  public String getClasseEuro() {
    return classeEuro;
  }

  public void setClasseEuro(String classeEuro) {
    this.classeEuro = classeEuro;
  }

  public Instant getDataVariazione() {
    return getDmlRevisionTimestamp();
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getNazione() {
    return nazione;
  }

  public void setNazione(String nazione) {
    this.nazione = nazione;
  }

  @Override
  public String toString() {
    return "StoricoVeicolo [classeEuro=" + classeEuro + ", targa="
           + targa + ", nazione=" + nazione + ", toString()=" + super.toString() + "]";
  }
}
