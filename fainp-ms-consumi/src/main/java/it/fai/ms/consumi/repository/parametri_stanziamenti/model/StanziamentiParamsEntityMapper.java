package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

@Component
@Validated
public class StanziamentiParamsEntityMapper {

  public StanziamentiParamsEntityMapper() {
  }

  public StanziamentiParams toDomain(@NotNull final StanziamentiParamsEntity _entity) {
    final StanziamentiParams domain = new StanziamentiParams(new Supplier(_entity.getSupplier()
                                                                                 .getCode()));
    domain.setUuid(_entity.getId());

    domain.getSupplier()
          .setCodeLegacy(_entity.getSupplier()
                                .getLegacyCode());

    domain.setArticle(new Article(_entity.getArticle()
                                         .getCode()));
    domain.getArticle()
          .setDescription(_entity.getArticle()
                                 .getDescription());
    domain.getArticle()
          .setCountry(_entity.getCountry());
    domain.getArticle()
          .setCurrency(_entity.getCurrency());
    domain.getArticle()
          .setGrouping(_entity.getGrouping());
    domain.getArticle()
          .setMeasurementUnit(_entity.getMeasurementUnit());
    domain.getArticle()
          .setVatRate(_entity.getVatRate());

    domain.setFareClass(_entity.getFareClass());
    domain.setSource(_entity.getSource());
    domain.setTransactionDetail(_entity.getTransactionDetail());
    domain.setRecordCode(_entity.getRecordCode());

    switch (_entity.getCostoRicavo()) {
    case C:
      domain.setCostoRicavo(CostoRicavo.C);
      break;
    case R:
      domain.setCostoRicavo(CostoRicavo.R);
      break;
    case CR:
      domain.setCostoRicavo(CostoRicavo.CR);
      break;
    default:
      throw new IllegalStateException("CostoRicavo enum not mapped");
    }

    domain.setTransactionType(_entity.getTransactionType());

    switch (_entity.getStanziamentiDetailsType()) {
    case PEDAGGI:
      domain.setStanzaimentiDetailsType(StanziamentiDetailsType.PEDAGGI);
      break;
    case CARBURANTI:
      domain.setStanzaimentiDetailsType(StanziamentiDetailsType.CARBURANTI);
      break;
    case TRENI:
      domain.setStanzaimentiDetailsType(StanziamentiDetailsType.TRENI);
      break;
    case GENERICO:
      domain.setStanzaimentiDetailsType(StanziamentiDetailsType.GENERICO);
      break;
    default:
      throw new IllegalStateException("StanzimentiDetailsType enum not mapped");
    }
    return domain;
  }

  public StanziamentiParamsEntity toEntity(final StanziamentiParams _domain) {

    Article article = _domain.getArticle();
    StanziamentiParamsArticleEntity articleEntity = new StanziamentiParamsArticleEntity(article.getCode());
    articleEntity.setDescription(article.getDescription());

    StanziamentiParamsEntity entity = new StanziamentiParamsEntity(articleEntity);
    entity.setCountry(article.getCountry());
    entity.setCurrency(article.getCurrency());
    entity.setGrouping(article.getGrouping());
    entity.setMeasurementUnit(article.getMeasurementUnit());
    entity.setVatRate(article.getVatRate());

    entity.setId(_domain.getUuid());

    CostoRicavo costoRicavo = _domain.getCostoRicavo();
    switch (costoRicavo) {
    case C:
      entity.setCostoRicavo(StanziamentiParamsCostoRicavoEntity.C);
      break;
    case R:
      entity.setCostoRicavo(StanziamentiParamsCostoRicavoEntity.R);
      break;
    case CR:
      entity.setCostoRicavo(StanziamentiParamsCostoRicavoEntity.CR);
      break;
    default:
      throw new IllegalStateException("CostoRicavo enum not mapped");
    }



    entity.setFareClass(_domain.getFareClass());
    entity.setRecordCode(_domain.getRecordCode());
    entity.setSource(_domain.getSource());

    StanziamentiDetailsType stanzimentiDetailsType = _domain.getStanziamentiDetailsType();
    switch (stanzimentiDetailsType) {
    case PEDAGGI:
      entity.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.PEDAGGI);
      break;
    case CARBURANTI:
      entity.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.CARBURANTI);
      break;
    case TRENI:
      entity.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.TRENI);
      break;
    case GENERICO:
      entity.setStanziamentiDetailsType(StanziamentiDetailsTypeEntity.GENERICO);
      break;
    default:
      throw new IllegalStateException("StanziamentiDetailsTypeEntity enum not mapped");
    }

    Supplier supplier = _domain.getSupplier();
    StanziamentiParamsSupplierEntity supplierEntity = new StanziamentiParamsSupplierEntity(supplier.getCode());
    supplierEntity.setLegacyCode(supplier.getCodeLegacy());

    entity.setSupplier(supplierEntity);

    entity.setTransactionDetail(_domain.getTransactionDetail());
    entity.setTransactionType(_domain.getTransactionType());

    return entity;
  }

}
