package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi;

import java.math.BigDecimal;
import java.time.Instant;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.Importo;

public class PedaggiDaFatturareDto {

  private Instant dataUscita;             // val dd-MM-yyyy
  private String  codiceCaselloIngresso;  // DIREZ. USC. |DIREZ. ENTR.|MARENE|CASTEL.STURA|PARCHEGGIO AR
  private String  codiceCaselloUscita;
  private Importo importoLordoSoggettoIva;// Importo lordo soggetto I.V.A. (imponibile + imposta) 2,5|0,7
  private String  serialeDispositivo;

  public PedaggiDaFatturareDto(Instant dataUscita, String codiceCaselloIngresso, String codiceCaselloUscita,
                               BigDecimal importoLordoSoggettoIva, String currency, String serialeDispositivo) {
    super();
    this.dataUscita = dataUscita;
    this.codiceCaselloIngresso = codiceCaselloIngresso;
    this.codiceCaselloUscita = codiceCaselloUscita;
    this.importoLordoSoggettoIva = new Importo(importoLordoSoggettoIva, currency);
    this.serialeDispositivo = serialeDispositivo;
  }

  public Instant getDataUscita() {
    return dataUscita;
  }

  public void setDataUscita(Instant dataUscita) {
    this.dataUscita = dataUscita;
  }

  public String getCodiceCaselloIngresso() {
    return codiceCaselloIngresso;
  }

  public void setCodiceCaselloIngresso(String codiceCaselloIngresso) {
    this.codiceCaselloIngresso = codiceCaselloIngresso;
  }

  public String getCodiceCaselloUscita() {
    return codiceCaselloUscita;
  }

  public void setCodiceCaselloUscita(String codiceCaselloUscita) {
    this.codiceCaselloUscita = codiceCaselloUscita;
  }
  
  public Importo getImportoLordoSoggettoIva() {
    return importoLordoSoggettoIva;
  }

  public void setImportoLordoSoggettoIva(Importo importoLordoSoggettoIva) {
    this.importoLordoSoggettoIva = importoLordoSoggettoIva;
  }

  public String getSerialeDispositivo() {
    return serialeDispositivo;
  }

  public void setSerialeDispositivo(String serialeDispositivo) {
    this.serialeDispositivo = serialeDispositivo;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codiceCaselloIngresso == null) ? 0 : codiceCaselloIngresso.hashCode());
    result = prime * result + ((codiceCaselloUscita == null) ? 0 : codiceCaselloUscita.hashCode());
    result = prime * result + ((dataUscita == null) ? 0 : dataUscita.hashCode());
    result = prime * result + ((importoLordoSoggettoIva == null) ? 0 : importoLordoSoggettoIva.hashCode());
    result = prime * result + ((serialeDispositivo == null) ? 0 : serialeDispositivo.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PedaggiDaFatturareDto other = (PedaggiDaFatturareDto) obj;
    if (codiceCaselloIngresso == null) {
      if (other.codiceCaselloIngresso != null)
        return false;
    } else if (!codiceCaselloIngresso.equals(other.codiceCaselloIngresso))
      return false;
    if (codiceCaselloUscita == null) {
      if (other.codiceCaselloUscita != null)
        return false;
    } else if (!codiceCaselloUscita.equals(other.codiceCaselloUscita))
      return false;
    if (dataUscita == null) {
      if (other.dataUscita != null)
        return false;
    } else if (!dataUscita.equals(other.dataUscita))
      return false;
    if (importoLordoSoggettoIva == null) {
      if (other.importoLordoSoggettoIva != null)
        return false;
    } else if (!importoLordoSoggettoIva.equals(other.importoLordoSoggettoIva))
      return false;
    if (serialeDispositivo == null) {
      if (other.serialeDispositivo != null)
        return false;
    } else if (!serialeDispositivo.equals(other.serialeDispositivo))
      return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("PedaggiDaFatturareDto [dataUscita=");
    builder.append(dataUscita);
    builder.append(", codiceCaselloIngresso=");
    builder.append(codiceCaselloIngresso);
    builder.append(", codiceCaselloUscita=");
    builder.append(codiceCaselloUscita);
    builder.append(", importoLordoSoggettoIva=");
    builder.append(importoLordoSoggettoIva);
    builder.append(", serialeDispositivo=");
    builder.append(serialeDispositivo);
    builder.append("]");
    return builder.toString();
  }

}
