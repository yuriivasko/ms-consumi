package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.Asfinag;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.AsfinagRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(AsfinagJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class AsfinagJob
  extends AbstractJob<AsfinagRecord, Asfinag> {

  public final static String QUALIFIER = "AsfinagJob";

  public AsfinagJob(PlatformTransactionManager transactionManager,
                      StanziamentiParamsValidator stanziamentiParamsValidator,
                      NotificationService notificationService,
                      RecordPersistenceService persistenceService,
                      RecordDescriptor<AsfinagRecord> recordDescriptor,
                      ConsumoProcessor<Asfinag> consumoProcessor,
                      RecordConsumoMapper<AsfinagRecord, Asfinag> recordConsumoMapper,
                      StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("ASFINAG",transactionManager, stanziamentiParamsValidator, notificationService,
          persistenceService, recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
  }

  @Override
  public Format getJobQualifier() {
    return Format.ASFINAG;
  }

}
