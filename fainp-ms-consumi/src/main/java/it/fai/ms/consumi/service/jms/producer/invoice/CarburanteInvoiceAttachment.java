package it.fai.ms.consumi.service.jms.producer.invoice;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.common.jms.fattura.CarburantiAllegatoFattureMessage;
import it.fai.ms.common.jms.fattura.CarburantiBodyDTO;
import it.fai.ms.common.jms.fattura.CarburantiHeaderDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2Entity;
import it.fai.ms.consumi.service.ViewAllegatiCarburantiService;

@Component
public class CarburanteInvoiceAttachment implements IGenerateInvoiceAttachment {

  private final DettaglioStanziamentiRepositoryImpl dettagliRepository;

  private final ViewAllegatiCarburantiService viewAllegatiCarburantiService;

  public CarburanteInvoiceAttachment(final DettaglioStanziamentiRepositoryImpl _dettagliRepository,
                                     ViewAllegatiCarburantiService _viewAllegatiCarburantiService) {
    dettagliRepository = _dettagliRepository;
    viewAllegatiCarburantiService = _viewAllegatiCarburantiService;
  }

  @Override
  public Object generateDtoToAttachmentInvoice(DettaglioStanziamentoType tipoDettaglio,
                                               RequestsCreationAttachments requestCreationAttachments,
                                               final RaggruppamentoArticoliDTO raggruppamentoArticoliDTO, final TemplateAllegati template) {
    long t1 = System.currentTimeMillis();
    if (!getDettaglioStanziamentoType().equals(tipoDettaglio)) {
      return null;
    }

    //Template actually not used, but Marina explain that it is possible to use this field to future implementation.
    log.info("Start generate message to create Allegati Fattura {} ...", tipoDettaglio);
    CarburantiAllegatoFattureMessage message = new CarburantiAllegatoFattureMessage(getNomeDocumento(requestCreationAttachments, raggruppamentoArticoliDTO));
    message.setCodiceRaggruppamentoArticolo(requestCreationAttachments.getCodiceRaggruppamentoArticolo());
    message.setHeader(createHeaderMessage(requestCreationAttachments, raggruppamentoArticoliDTO));

    final List<String> codiciStanziamenti = raggruppamentoArticoliDTO.getCodiciStanziamenti();
    List<UUID> dettagliStanziamentoId = dettagliRepository.findDettagliStanziamentoIdByCodiciStanziamento(codiciStanziamenti);
    List<ViewAllegatoCarburantiSezione2Entity> dataCarburantiAllegati = viewAllegatiCarburantiService.getDettagliStanziamento(dettagliStanziamentoId);
    List<CarburantiBodyDTO> body = viewAllegatiCarburantiService.createBodyMessageAndSection2(dataCarburantiAllegati,
                                                                                              requestCreationAttachments);
    message.setBody(body);
    message.setLang(requestCreationAttachments.getLang()
                                              .toLowerCase());
    log.info("Finished generate message to create Allegati Fattura {} in {} ms", tipoDettaglio, System.currentTimeMillis() - t1);
    printToJsonFormat(message);
    return message;
  }

  @Override
  public DettaglioStanziamentoType getDettaglioStanziamentoType() {
    return DettaglioStanziamentoType.CARBURANTE;
  }

  private CarburantiHeaderDTO createHeaderMessage(RequestsCreationAttachments requestCreationAttachments,
                                                  RaggruppamentoArticoliDTO raggruppamentoArticoliDTO) {

    CarburantiHeaderDTO header = new CarburantiHeaderDTO();
    header.setNumeroFattura(requestCreationAttachments.getCodiceFattura());

    LocalDate localDateFattura = LocalDate.ofInstant(requestCreationAttachments.getDataFattura(), ZoneOffset.UTC);
    header.setDataFattura(localDateFattura);

    String codiceClienteFatturazione = raggruppamentoArticoliDTO.getCodiceClienteFatturazione();
    String ragioneSocialeClienteFatturazione = raggruppamentoArticoliDTO.getRagioneSocialeClienteFatturazione();
    String clienteHeader = StringUtils.isNoneBlank(ragioneSocialeClienteFatturazione) ? codiceClienteFatturazione + " - "
                                                                                        + ragioneSocialeClienteFatturazione
                                                                                      : codiceClienteFatturazione;
    header.setCodiceCliente(codiceClienteFatturazione);
    header.setCliente(clienteHeader);
    header.setRagioneSociale(ragioneSocialeClienteFatturazione);
    // header.setNomeIntestazioneTessere(requestCreationAttachments.getNomeIntestazioneTessere());

    header.setDettaglio(requestCreationAttachments.getDescrRaggruppamentoArticolo());
    return header;
  }

}
