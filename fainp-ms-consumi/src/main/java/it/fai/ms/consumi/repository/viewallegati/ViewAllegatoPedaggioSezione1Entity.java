package it.fai.ms.consumi.repository.viewallegati;

import java.time.LocalDate;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "allegati_fatture_pedaggi_sezione1")
@IdClass(ViewAllegatoPedaggioSezione1Id.class)
@Immutable
public class ViewAllegatoPedaggioSezione1Entity {

  @Id
  @Column(name = "codice_contratto")
  private String codiceContratto;

  @Id
  @Column(name = "device_type")
  private String tipoDispositivo;

  @Id
  @Column(name = "pan_number")
  private String panNumber;

  @Column(name = "serial_number")
  private String serialNumber;

  @Column(name = "veicolo_nazione_targa")
  private String nazioneVeicolo;

  @Column(name = "veicolo_targa")
  private String targaVeicolo;

  @Column(name = "classe_euro")
  private String classificazioneEuro;

  @Column(name = "data_erogazione_servizio")
  private LocalDate dataErogazioneServizio;

  @Column(name = "amount")
  private Double amount;

  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getPanNumber() {
    return panNumber;
  }

  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getNazioneVeicolo() {
    return nazioneVeicolo;
  }

  public void setNazioneVeicolo(String nazioneVeicolo) {
    this.nazioneVeicolo = nazioneVeicolo;
  }

  public String getTargaVeicolo() {
    return targaVeicolo;
  }

  public void setTargaVeicolo(String targaVeicolo) {
    this.targaVeicolo = targaVeicolo;
  }

  public String getClassificazioneEuro() {
    return classificazioneEuro;
  }

  public void setClassificazioneEuro(String classificazioneEuro) {
    this.classificazioneEuro = classificazioneEuro;
  }

  public LocalDate getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public void setDataErogazioneServizio(LocalDate dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ViewAllegatoPedaggioSezione1Entity [");
    sb.append("codiceContratto= ");
    sb.append(codiceContratto);
    sb.append(", tipoDispositivo= ");
    sb.append(tipoDispositivo);
    sb.append(", panNumber= ");
    sb.append(panNumber);
    sb.append(", serialNumber= ");
    sb.append(serialNumber);
    sb.append(", nazioneVeicolo= ");
    sb.append(nazioneVeicolo);
    sb.append(", targaVeicolo= ");
    sb.append(targaVeicolo);
    sb.append(", classificazioneEuro= ");
    sb.append(classificazioneEuro);
    sb.append(", dataErogazioneServizio= ");
    sb.append(dataErogazioneServizio);
    sb.append(", amount= ");
    sb.append(amount);
    sb.append("]");
    return sb.toString();
  }

}
