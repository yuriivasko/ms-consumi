package it.fai.ms.consumi.domain;

import java.time.Instant;

public class ViewAllegatiFatture {

  private String  deviceType;
  private String  panNumber;
  private String  countryLicensePlate;
  private String  licensePlate;
  private Instant dataErogazioneServizio;
  private Double  totale;
  
  public String getDeviceType() {
    return deviceType;
  }
  public void setDeviceType(String deviceType) {
    this.deviceType = deviceType;
  }
  public String getPanNumber() {
    return panNumber;
  }
  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }
  public String getCountryLicensePlate() {
    return countryLicensePlate;
  }
  public void setCountryLicensePlate(String countryLicensePlate) {
    this.countryLicensePlate = countryLicensePlate;
  }
  public String getLicensePlate() {
    return licensePlate;
  }
  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }
  public Instant getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }
  public void setDataErogazioneServizio(Instant dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }
  public Double getTotale() {
    return totale;
  }
  public void setTotale(Double totale) {
    this.totale = totale;
  }

}
