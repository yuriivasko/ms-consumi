package it.fai.ms.consumi.jms.notification_v2;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import it.fai.ms.common.jms.notification_v2.NotificationMessage;
import it.fai.ms.common.jms.notification_v2.NotificationTarget;
import it.fai.ms.common.jms.notification_v2.NotificationType;
import it.fai.ms.consumi.service.util.FaiConsumiFilesUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConsumoNotReprocessableNotification
  extends EnrichedNotification<NotificationMessage> {

  private String job_name;
  private String fileName;
  private Long fileLine;
  private String original_message;


  public ConsumoNotReprocessableNotification(String jobName, String fileName, Long fileLine, String notificationCode, String message) {
    super(notificationCode, null);
    this.job_name = jobName;
    this.fileName = FaiConsumiFilesUtil.getFilename(fileName);
    this.fileLine = fileLine;
    this.original_message = message;
  }

  public ConsumoNotReprocessableNotification(String jobName, String fileName, String notificationCode, String message) {
    this(jobName, fileName, NotificationConstants.NO_ROW, notificationCode, message);
  }

  public String enrichNotificationText(String text){
    return "Tracciato: " + job_name + ", " +  fileName + ", riga: " +  fileLine + ", recordCode: " +  "'n/a'" + ", transactionDetail: " +  "'n/a'" + " - " + text;
  }

  @Override
  public NotificationMessage get(NotificationType type, List<NotificationTarget> targets){
    return new NotificationMessage(source, code, NotificationType.CONSUMO_ERROR_NOT_REPROCESSABLE, targets);
  }
}
