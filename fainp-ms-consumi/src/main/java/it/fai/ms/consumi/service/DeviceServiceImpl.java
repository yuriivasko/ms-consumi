package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.NullHandling;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.repository.device.DeviceRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoStatoServizioRepository;
import it.fai.ms.consumi.repository.storico_dml.ViewStoricoDispositivoVeicoloContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio_;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;

@Service
@Validated
public class DeviceServiceImpl implements DeviceService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceRepository                                 deviceRepository;
  private final ViewStoricoDispositivoVeicoloContrattoRepository storicoStatoDispositivoRepository;
  private final StoricoStatoServizioRepository                   storicoStatoServizioRepository;

  @Inject
  public DeviceServiceImpl(DeviceRepository deviceRepository,
                           ViewStoricoDispositivoVeicoloContrattoRepository storicoStatoDispositivoRepository,
                           StoricoStatoServizioRepository storicoStatoServizioRepository) {

    this.deviceRepository = deviceRepository;
    this.storicoStatoDispositivoRepository = storicoStatoDispositivoRepository;
    this.storicoStatoServizioRepository = storicoStatoServizioRepository;
  }

  @Override
  public List<StoricoStatoServizio> findStatiServizioInterval(String deviceDmlUniqueIdentifier, String tipoServizio, Instant dataInizio,
                                                               Instant dataFine) {
                    List<StoricoStatoServizio> activeInInterval = storicoStatoServizioRepository
                      .findAll(StoricoStatoServizioRepository.inInterval(deviceDmlUniqueIdentifier, tipoServizio, dataInizio,
                                                                         dataFine),
                               Sort.by(new Sort.Order(Direction.ASC, StoricoStatoServizio_.DATA_VARIAZIONE, NullHandling.NULLS_FIRST),
                                       new Sort.Order(Direction.ASC, StoricoStatoServizio_.DATA_FINE_VARIAZIONE, NullHandling.NULLS_LAST)));
                    return activeInInterval;
                  }

  @Override
  public Optional<Device> findDeviceBySeriale(@NotNull final String _deviceSeriale, @NotNull final TipoDispositivoEnum _deviceType, final String _contractNumber) {

    _log.debug("Searching device for serialCode {} and type {} ...", _deviceSeriale, _deviceType);

    final var optionalDevice = deviceRepository.findDeviceBySeriale(_deviceSeriale, _deviceType, _contractNumber);

    optionalDevice.ifPresentOrElse(device -> _log.debug("... found {}", device), () -> {
      var deviceTypeString = _deviceType != null ? "'" + _deviceType.toString() + "'" : "null";
      var deviceSerialeString = _deviceSeriale != null ? "'" + _deviceSeriale + "'"  : "null";
      var contractNumberString = _contractNumber != null ? "'" + _contractNumber + "'"  : "null";
      _log.info("... device {} serialCode {} contract {} not found", deviceTypeString, deviceSerialeString, contractNumberString);
     });

    return optionalDevice;
  }

  @Override
  public Optional<Device> findDeviceByLicensePlate(String _licensePlate, TipoDispositivoEnum _deviceType, Instant _associationDate) {

    _log.debug("Searching device for licencePlate {}, type {} and associationDate {}...", _licensePlate, _deviceType, _associationDate);

    final var optionalDevice = deviceRepository.findDeviceByLicensePlate(_licensePlate, _deviceType, _associationDate);

    optionalDevice.ifPresentOrElse(device -> _log.debug("... found {}", device),
                                   () -> _log.info("... device {} with plate {} not found in date {}", _deviceType, _licensePlate,
                                                   _associationDate));

    return optionalDevice;
  }

  @Override
  public Optional<Device> findDeviceByPan(String _pan, TipoServizioEnum _serviceType, String _contractNumber) {

    _log.debug("Searching device for pan {} and type {} ...", _pan, _serviceType);

    final var optionalDevice = deviceRepository.findDeviceByPan(_pan, _serviceType, _contractNumber);

    optionalDevice.ifPresentOrElse(device -> _log.debug("... found {}", device),
                                   () -> _log.info("... device {} pan {} contract {} not found", _serviceType, _pan, _contractNumber));

    return optionalDevice;
  }

  @Override
  public Optional<ViewStoricoDispositivoVeicoloContratto> findStoricoDispositivo(@NotNull final String _deviceSeriale,
                                                                                 @NotNull final TipoDispositivoEnum _deviceType,
                                                                                 @NotNull final Instant _date) {

    return storicoStatoDispositivoRepository.findFirstBySerialeDispositivoAndTipoDispositivoAndDataVariazioneBeforeOrderByDataVariazioneDesc(
      _deviceSeriale, _deviceType, _date);
  }

  @Override
  public Optional<StoricoStatoServizio> findStoricoStatoServizio(@NotNull final String _deviceUuid,
                                                                 @NotNull final TipoServizioEnum _serviceType,
                                                                 @NotNull final Instant _date) {

    return storicoStatoServizioRepository.findFirstByDispositivoDmlUniqueIdentifierAndTipoServizioAndDataVariazioneBeforeOrderByDataVariazioneDesc(
      _deviceUuid, _serviceType.name(), _date);
  }
}
