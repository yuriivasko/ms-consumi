package it.fai.ms.consumi.domain.validation;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

public class ValidationOutcome {

  private boolean                      fatalError;
  private final Set<Message>           messages                   = new HashSet<>();
  private Optional<StanziamentiParams> optionalStanziamentiParams = Optional.empty();
  private boolean                      validationOk               = true;

  public ValidationOutcome() {
  }

  public Message addMessage(String code){
    final Message message = new Message(code);
    addMessage(message);
    return message;
  }

  public void addMessage(final Message _message) {
    if (_message != null) {
      getMessages().add(_message);
      if (validationOk) {
        validationOk = false;
      }
    }
  }

  public void addMessages(final Set<Message> _messages) {
    if (_messages != null && !_messages.isEmpty()) {
      getMessages().addAll(_messages);
      if (validationOk) {
        validationOk = false;
      }
    }
  }

  public void addValidationOutcomeMessages(Optional<ValidationOutcome> validationOutcome) {
    if(validationOutcome!=null) {
      validationOutcome.ifPresent(v -> this.addMessages(v.getMessages()));
    }
  }

  public void addValidationOutcomeMessages(ValidationOutcome validationOutcome) {
    if(validationOutcome!=null) {
      this.addMessages(validationOutcome.getMessages());
    }
  }

  public Set<Message> getMessages() {
    return messages;
  }

  public Optional<StanziamentiParams> getStanziamentiParamsOptional() {
    return optionalStanziamentiParams;
  }

  public boolean isFatalError() {
    return fatalError;
  }

  public boolean isValidationOk() {
    return validationOk;
  }

  public ValidationOutcome message(final Message _message) {
    addMessage(_message);
    return this;
  }

  public void setFatalError(final boolean _fatalError) {
    fatalError = _fatalError;
  }

  public void setStanziamentiParams(final StanziamentiParams _stanziamentiParams) {
    optionalStanziamentiParams = Optional.ofNullable(_stanziamentiParams);
  }

  public void setValidationOk(final boolean _validation) {
    validationOk = _validation;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("validationOk=")
                              .append(validationOk)
                              .append(",fatalError=")
                              .append(fatalError)
                              .append(",")
                              .append(optionalStanziamentiParams)
                              .append(",")
                              .append(messages)
                              .append("]")
                              .toString();
  }
}
