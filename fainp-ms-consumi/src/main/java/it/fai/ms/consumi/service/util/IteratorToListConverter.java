package it.fai.ms.consumi.service.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IteratorToListConverter<T> {

  protected final transient Logger _log = LoggerFactory.getLogger(getClass());

  private Iterator<T>       iterator;
  private Supplier<List<T>> listSupplier;
  private Comparator<T>     comparator;

  public IteratorToListConverter(Iterator<T> iterator) {
    this(iterator, Comparator.comparing(Object::toString), LinkedList::new);
  }

  public IteratorToListConverter(Iterator<T> iterator, Comparator<T> comparator) {
    this(iterator, comparator, LinkedList::new);
  }

  public IteratorToListConverter(Iterator<T> iterator, Supplier<List<T>> listSupplier) {
    this(iterator, Comparator.comparing(Object::toString), listSupplier);
  }

  public IteratorToListConverter(Iterator<T> iterator, Comparator<T> comparator, Supplier<List<T>> listSupplier) {
    this.iterator = Optional.ofNullable(iterator).orElseThrow(() -> new IllegalArgumentException("iterator must not be null!"));
    this.listSupplier = Optional.ofNullable(listSupplier).orElseThrow(() -> new IllegalArgumentException("listSupplier must not be null!"));
    this.comparator = Optional.ofNullable(comparator).orElseThrow(() -> new IllegalArgumentException("comparator must not be null!"));
  }

  public List<T> toOrderedList() {
    List<T> orderedList = listSupplier.get();
    iterator.forEachRemaining(orderedList::add);
    orderedList.sort(comparator);
    return orderedList;
  }
}
