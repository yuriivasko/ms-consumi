package it.fai.ms.consumi.service.validator.internal;

import java.time.Instant;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.consumi.DateSupplier;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;

@Service
public class CustomerValidatorImpl implements CustomerValidator {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final CustomerService customerService;

  @Inject
  public CustomerValidatorImpl(final CustomerService _customerService,
                               @Value("#{new Boolean(${validator.customer:false})}") final boolean _validatorEnabled) {
    customerService = _customerService;
    _log.info("Validator enabled 'validator.customer' : {}", _validatorEnabled);
  }

  @Override
  public ValidationOutcome customerActiveInDate(final String _companyCode, final Instant _date) {
    ValidationOutcome rs = new ValidationOutcome();

    Instant dateOffset = _date.plus(DateSupplier.VALIDATOR_DATE_OFFSET);
    _log.debug("Check if customer {} was active in date {}", _companyCode, dateOffset);

    boolean isActive = customerService.isCustomerActiveInDate(_companyCode, dateOffset);
    if(!isActive) {
      rs.addMessage(new Message("w001").withText("Customer "+_companyCode+" is not active in date "+FaiConsumiDateUtil.formatInstant(_date, DateSupplier.VALIDATOR_DATE_OFFSET)));
    }

    return rs;
  }
}
