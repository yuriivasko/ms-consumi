package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.input.ReversedLinesFileReader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.RecordFileFormalValidator;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(DgdTollJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class DgdTollJob
  extends AbstractJob<DgdTollRecord, DgdToll> {

  public final static String QUALIFIER = "DgdTollJob";

  private final transient Logger log = LoggerFactory.getLogger(getClass());

  private final RecordFileFormalValidator<DgdTollRecord> formalFileValidator;

  public DgdTollJob(PlatformTransactionManager transactionManager,
                    StanziamentiParamsValidator stanziamentiParamsValidator,
                    RecordFileFormalValidator<DgdTollRecord> formalFileValidator,
                    NotificationService notificationService,
                    RecordPersistenceService persistenceService,
                    RecordDescriptor<DgdTollRecord> recordDescriptor,
                    ConsumoProcessor<DgdToll> consumoProcessor,
                    RecordConsumoMapper<DgdTollRecord, DgdToll> recordConsumoMapper,
                    StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("DGD", transactionManager, stanziamentiParamsValidator, notificationService,
          persistenceService, recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
    this.formalFileValidator = formalFileValidator;
  }

  @Override
  protected void postFileElaboration(String filename,
                                     Instant ingestionTime,
                                     ServicePartner servicePartner,
                                     Map<String, Stanziamento> stanziamentiDaSpedireANAV,
                                     List<String> errorList,
                                     RecordsFileInfo recordsFileInfo) throws IOException {

    super.postFileElaboration(filename, ingestionTime, servicePartner, stanziamentiDaSpedireANAV, errorList, recordsFileInfo);
    log.debug("Creating log file");

    String footer;
    try (ReversedLinesFileReader in = new ReversedLinesFileReader(new File(filename), Charset.forName(ISO_8859_1))) {
      footer = in.readLine();
    }
    DgdTollRecord footerRecord = recordDescriptor.decodeRecordCodeAndCallSetFromString(footer, DgdTollRecordDescriptor.FOOTER_CODE_BEGIN, filename, -1);

    Path path = createLogFilePathByFileName(filename);
    try (BufferedWriter writer = Files.newBufferedWriter(path, Charset.forName(ISO_8859_1))) {
      String line = footerRecord.getTotalRows() + "000000" + footerRecord.getSign()+footerRecord.getTotalTaxedAmount();
      writer.write(line, 0, line.length());
    } catch (IOException x) {
      System.err.format("IOException: %s%n", x);
    }
  }

  private Path createLogFilePathByFileName(String filename) {
    String logFileName = convertToLogFilename(filename);
    return Paths.get(logFileName);
  }

  private String convertToLogFilename(String filename) {
    return StringUtils.replace(filename, "TXT", "LOG");
  }

  @Override
  public RecordsFileInfo validateFile(String _filename, long _startTime, Instant _ingestionTime) {
    RecordsFileInfo recordsFileInfo = super.validateFile(_filename, _startTime, _ingestionTime);
    recordsFileInfo.setHeader(new DgdTollRecord(_filename, _startTime));
    recordsFileInfo.getHeader().setCreationDateInFile(extractDateFromFilename(_filename));
    recordsFileInfo.getHeader().setCreationTimeInFile("000000");

    if (recordsFileInfo.fileIsValid()) {
      return formalFileValidator.validateFile(recordsFileInfo);
    }
    return recordsFileInfo;
  }

  private String extractDateFromFilename(String filename) {
    String extractedDate = StringUtils.substringAfter(filename, "FAI_RE");
    extractedDate = StringUtils.left(extractedDate, 6);
    return "20" + extractedDate;
  }

  @Override
  public Format getJobQualifier() {
    return Format.DGD;
  }

}
