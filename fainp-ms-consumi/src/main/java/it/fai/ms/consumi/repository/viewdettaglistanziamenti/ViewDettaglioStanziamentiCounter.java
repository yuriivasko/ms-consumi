package it.fai.ms.consumi.repository.viewdettaglistanziamenti;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "view_dettagliostanziamenti_stanziamenti_counter")
public class ViewDettaglioStanziamentiCounter {

  @Id
  @Column(name = "counter")
  private long counter;

  @Column(name = "dispositivo")
  private String dispositivo;

  @Column(name = "importo")
  private String importo;

  @Column(name = "segno_importo")
  private String segnoImporto;

  @Column(name = "data_ora_consumo")
  private String dataOraConsumo;

  @Column(name = "quantita")
  private String quantita;

  @Column(name = "tipo_consumo")
  private String tipoConsumo;

  @Column(name = "tipo")
  private String tipo;

  @Column(name = "codice_stanziamento")
  private String codiceStanziamento;

  public long getCounter() {
    return counter;
  }

  public void setCounter(long counter) {
    this.counter = counter;
  }

  public String getDispositivo() {
    return dispositivo;
  }

  public void setDispositivo(String dispositivo) {
    this.dispositivo = dispositivo;
  }

  public String getImporto() {
    return importo;
  }

  public void setImporto(String importo) {
    this.importo = importo;
  }

  public String getSegnoImporto() {
    return segnoImporto;
  }

  public void setSegnoImporto(String segnoImporto) {
    this.segnoImporto = segnoImporto;
  }

  public String getDataOraConsumo() {
    return dataOraConsumo;
  }

  public void setDataOraConsumo(String dataOraConsumo) {
    this.dataOraConsumo = dataOraConsumo;
  }

  public String getQuantita() {
    return quantita;
  }

  public void setQuantita(String quantita) {
    this.quantita = quantita;
  }

  public String getTipoConsumo() {
    return tipoConsumo;
  }

  public void setTipoConsumo(String tipoConsumo) {
    this.tipoConsumo = tipoConsumo;
  }

  public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  public String getCodiceStanziamento() {
    return codiceStanziamento;
  }

  public void setCodiceStanziamento(String codiceStanzimento) {
    this.codiceStanziamento = codiceStanzimento;
  }

  @Override
  public String toString() {
    return "VwCollecSecSearch [counter=" + counter + ", dispositivo=" + dispositivo + ", importo=" + importo + ", segnoImporto="
           + segnoImporto + ", dataOraConsumo=" + dataOraConsumo + ", quantita=" + quantita + ", tipoConsumo="
           + tipoConsumo + ", tipo=" + tipo + ", codiceStanzimento=" + codiceStanziamento + "]";
  }

}