package it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti.DettaglioStanziamentoCarburantiProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class TrackyCardCarbStdProcessorImpl
  extends ConsumoAbstractProcessor
  implements TrackyCardCarbStdProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoCarburantiProcessor     dettaglioStanziamentoProcessor;
  private final TrackyCardCarbStdDettaglioStanziamentoMapper mapper;

  @Inject
  public TrackyCardCarbStdProcessorImpl(final ConsumoValidatorService _consumoValidatorService,
                                        final DettaglioStanziamentoCarburantiProcessor _dettaglioStanziamentoProcessor,
                                        final TrackyCardCarbStdDettaglioStanziamentoMapper _mapper,
                                        final VehicleService _vehicleService,
                                        final CustomerService _customerService) {

    super(_consumoValidatorService,  _vehicleService, _customerService);
    dettaglioStanziamentoProcessor = _dettaglioStanziamentoProcessor;
    mapper = _mapper;
  }

  @Override
  public ProcessingResult validateAndProcess(final TrackyCardCarburantiStandard _trackyCardCarburantiStandard, final Bucket _bucket) {
    return executeProcess(_trackyCardCarburantiStandard, _bucket);
  }

  private ProcessingResult executeProcess(final Consumo _consumo, final Bucket _bucket) {

    _log.info(">");
    _log.info("> processing start {}", _bucket.getFileName());
    _log.info("  --> {}", _consumo);

    var processingResult = validateAndNotify(_consumo);

    _log.debug(" - {} processing validation completed", _consumo);

    final var consumo = processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
                      .ifPresent(stanziamentiParams -> {
                        final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
                        final var stanziamenti = dettaglioStanziamentoProcessor.produce(dettaglioStanziamento, stanziamentiParams);
                        processingResult.getStanziamenti()
                                        .addAll(stanziamenti);
                      });
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }

    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", _consumo.getSource());
    _log.info("<");

    return processingResult;
  }

  private DettaglioStanziamentoCarburante mapConsumoToDettaglioStanziamento(final Consumo _consumo) {
    var dettaglioStanziamento = (DettaglioStanziamentoCarburante) mapper.mapConsumoToDettaglioStanziamento(      _consumo);
    addVehicle(_consumo, dettaglioStanziamento);
    addCustomer(_consumo, dettaglioStanziamento);
    _log.info("Mapped {}", dettaglioStanziamento);
    return dettaglioStanziamento;
  }
}
