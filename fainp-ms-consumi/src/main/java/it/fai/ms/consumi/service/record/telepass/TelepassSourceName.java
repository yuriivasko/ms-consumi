package it.fai.ms.consumi.service.record.telepass;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public interface TelepassSourceName {
  
  static final Logger _log = LoggerFactory.getLogger(TelepassSourceName.class);


  public static final String ELCEU = "elceu";
  public static final String ELCIT = "elcit";
  public static final String ELVIA = "elvia";
  public static final String VACON = "vacon";
  
  public static final List<String> TELEPASS_SOURCES = Arrays.asList(ELCEU,ELCIT,ELVIA,VACON);
  
  public static boolean isTelepassSource(String _sourceName) {
    boolean res = false;
    if (_sourceName != null) {
      res = TELEPASS_SOURCES.contains(_sourceName.toLowerCase());
    }else {
      _log.error("isTelepassSource: SourceName is null");
    }
    return res;
  }
}
