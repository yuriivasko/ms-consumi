package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import java.util.Date;
import java.util.Map;

import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiJollyRecord;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiJollyDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecordBody;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;


@Service
public class TrackyCardPedaggiStandardRecordDescriptor implements RecordDescriptor<TrackyCardPedaggiStandardRecord> {

  private static final Logger log = LoggerFactory.getLogger(TrackyCardPedaggiStandardRecordDescriptor.class);


  public final static String HEADER_CODE_BEGIN = "H0";
  public final static FileDescriptor header = new FileDescriptor();

  static {
    int start = 0;
    header.addItem("recordCode", 1, start += 2);
    header.skipItem("recordNumber", 3, start += 8);
    header.skipItem("codicePartner", 11, start += 10);
    header.skipItem("data", 21, start += 8);
    header.skipItem("ora", 29, start += 6);
    header.skipItem("riservatoLibero", 35, start += 266);
  }


  static final Map<Integer, Integer> startEndPosTotalRows;
  public final static String BODY_CODE_BEGIN = "D0";
  public final static FileDescriptor recordBody = new FileDescriptor();

  static {
    int start = 0;
    recordBody.addItem("recordCode", 1, start += 2);
    recordBody.addItem("recordNumber", 3, start += 8);
    recordBody.addItem("tipoMovimento", 11, start += 5);
    recordBody.addItem("codiceAutostrada", 16, start += 10);
    recordBody.addItem("descrizioneAutostrada", 26, start += 30);
    recordBody.addItem("codiceISOIIN", 56, start += 6);
    recordBody.addItem("codiceGruppo", 62, start += 2);
    recordBody.addItem("codiceCliente", 64, start += 6);
    recordBody.addItem("numeroTessera", 70, start += 4);
    recordBody.addItem("cifradiControllo", 74, start += 1);
    recordBody.addItem("dataIngresso", 75, start += 8);
    recordBody.addItem("oraIngresso", 83, start += 6);
    recordBody.addItem("dataUscita", 89, start += 8);
    recordBody.addItem("oraUscita", 97, start += 6);
    recordBody.addItem("caselloIngresso", 103, start += 30);
    recordBody.addItem("caselloUscita", 133, start += 30);
    recordBody.addItem("imponibile", 163, start += 7);
    recordBody.addItem("imposta", 170, start += 7);
    recordBody.addItem("imponibileImposta", 177, start += 7);
    recordBody.addItem("percentualeIVA", 184, start += 4);
    recordBody.addItem("segno", 188, start += 1);
    recordBody.addItem("valuta", 189, start += 3);
    recordBody.addItem("classeVeicolo", 192, start += 2);
    recordBody.skipItem("riservatoLibero", 194, start += 107);
  }


  public final static String FOOTER_CODE_BEGIN = "T0";
  public final static FileDescriptor footer = new FileDescriptor();

  static {
    int start = 0;
    footer.addItem("recordCode", 1, start += 2);
    footer.addItem("recordNumber", 3, start += 8);
    footer.skipItem("codicePartner", 11, start += 10);
    footer.skipItem("data", 21, start += 8);
    footer.skipItem("ora", 29, start += 6);
    footer.skipItem("riservatoLibero", 35, start += 266);
    startEndPosTotalRows = RecordDescriptor.getStartEndForFields(footer, "recordNumber");
  }


  @Override
  public String getHeaderBegin() {
    return HEADER_CODE_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public TrackyCardPedaggiStandardRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String _fileName, long _rowNumber) {
    TrackyCardPedaggiStandardRecord record = null;
    switch (_recordCode) {
      case HEADER_CODE_BEGIN:
        record = new TrackyCardPedaggiStandardRecord(_fileName, _rowNumber);
        record.setFromString(header.getItems(), _data);
        break;

      case BODY_CODE_BEGIN:
        record = new TrackyCardPedaggiStandardRecordBody(_fileName, _rowNumber);
        record.setFromString(recordBody.getItems(), _data);
        break;

      case FOOTER_CODE_BEGIN:
        record = new TrackyCardPedaggiStandardRecord(_fileName, _rowNumber);
        record.setFromString(footer.getItems(), _data);
        break;

      default:
        log.warn("RecordCode {} not supported", _recordCode);
        break;
    }
    return record;
  }


  protected Date getNow() {
    return new Date();
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(TrackyCardPedaggiStandardRecordDescriptor.class, TrackyCardPedaggiStandardRecord.class).checkConfiguration();
  }
}
