package it.fai.ms.consumi.repository.device;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;

@Component
@Validated
public class DeviceEntityMapper {

  public DeviceEntityMapper() {
  }

  public Device toDomain(@NotNull final ViewStoricoDispositivoVeicoloContratto _storicoStatoDispositivo) {
    final var device = new Device(_storicoStatoDispositivo.getSerialeDispositivo(),_storicoStatoDispositivo.getTipoDispositivo());
    device.setContractNumber(_storicoStatoDispositivo.getContrattoNumero());
    device.setVehicleUUID(_storicoStatoDispositivo.getVeicoloDmlUniqueIdentifier());
    device.setDeviceUUID(_storicoStatoDispositivo.getDispositivoDmlUniqueIdentifier());
    return device;
  }

}
