package it.fai.ms.consumi.service.record;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

public interface RecordConsumoMapper<T extends CommonRecord, K extends Consumo> {

  K mapRecordToConsumo(T commonRecord) throws Exception;

}
