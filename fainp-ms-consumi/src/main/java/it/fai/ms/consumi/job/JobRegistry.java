package it.fai.ms.consumi.job;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class JobRegistry {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final Set<AbstractJob<? extends CommonRecord, ? extends Consumo>> jobs;

  public JobRegistry(Set<AbstractJob<? extends CommonRecord, ? extends Consumo>> jobs) {
    this.jobs = jobs;
    log.info("Jobs : {}", jobs);
  }

  public Optional<AbstractJob<? extends CommonRecord, ? extends Consumo>> findJobByServiceProviderFormat(final Format _format) {
    return jobs.stream()
      .filter(job -> job.getJobQualifier()
        .equals(_format))
      .findAny();
  }
}
