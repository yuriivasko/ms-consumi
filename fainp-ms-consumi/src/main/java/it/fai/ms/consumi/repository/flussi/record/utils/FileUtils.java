package it.fai.ms.consumi.repository.flussi.record.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtils {

  private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

  private FileUtils() {
    super();
  }

  public static boolean Exist(String value) {
    return (value.length() > 0) ? Files.exists(Paths.get(value)) : false;
  }

  public static boolean Delete(String src) {
    /*
     * WARNING: file owner is the same of process owner
     */
    try {
      if (src.length() > 0) {
        Path path = Paths.get(src);
        if (Files.exists(path)) {
          logger.info("Deleting file " + src + " ...");
          Files.delete(path);
          return true;
        }
      }
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
    }
    return false;
  }

  public static boolean Copy(String src, String dest) {
    try {
      logger.info("Copying file " + src + " to " + dest + " ...");
      File f1 = new File(src);
      File f2 = new File(dest);
      com.google.common.io.Files.copy(f1, f2);
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
      return false;
    }
    return true;
  }

  public static boolean Move(String src, String dest) {
    try {
      logger.info("Moving file " + src + " to " + dest + " ...");
      File f1 = new File(src);
      File f2 = new File(dest);
      com.google.common.io.Files.move(f1, f2);
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
      return false;
    }
    return true;
  }

  public static boolean Move(String src, String dest, String newName) {
    try {
      File afile = new File(src);
      logger.info("Moving file " + src + " to " + dest + " ...");
      if (!afile.renameTo(new File(dest + "/" + newName))) {
        logger.info("File is failed to move!");
      }
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
      return false;
    }
    return true;
  }

  public static boolean Rename(String oldName, String newName) {
    try {
      logger.info("Renaming file " + oldName + " to " + newName + " ...");
      // Path p1 = Paths.get(oldName);
      // Files.move(p1, p1.resolveSibling(newName));
      File file = new File(oldName);
      file.renameTo(new File(newName));
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
      return false;
    }
    return true;
  }

  public static boolean WriteText(String filename, String content) {
    return write_file(filename, content, false);
  }

  public static boolean AppendText(String filename, String content) {
    return write_file(filename, content, true);
  }

  public static String TempDir() {
    return System.getProperty("java.io.tmpdir");
  }

  private static boolean write_file(String filename, String content, boolean append) {
    try {
      File file = new File(filename);

      // if file doesnt exists, then create it
      if (!file.exists()) {
        file.createNewFile();
      }

      FileWriter fw = new FileWriter(file.getAbsoluteFile(), append);
      BufferedWriter bw = new BufferedWriter(fw);
      bw.write(content);
      bw.close();
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
      return false;
    }

    return true;
  }

  public static String Read(String filename) {
    StringBuilder sb = new StringBuilder();
    try {
      BufferedReader br = new BufferedReader(new FileReader(filename));
      String line = br.readLine();
      while (line != null) {
        sb.append(line);
        sb.append("\n");
        line = br.readLine();
      }
      br.close();
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
    }
    return sb.toString();
  }

  public static boolean MakeDir(String directory) {
    try {
      if (directory != null && directory.length() > 0) {
        File file = new File(directory);
        return file.exists() ? file.isDirectory() : file.mkdirs();
      } else {
        logger.warn("Failed to create directory : is empty");
      }
    } catch (Exception e) {
      logger.error("{}", e.getMessage());
    }
    return false;
  }

  public static void unZipToFolder(String zipfilename, String outputdir) throws IOException {
    File zipfile = new File(zipfilename);
    if (zipfile.exists()) {
      outputdir = outputdir + File.separator;
      org.apache.commons.io.FileUtils.forceMkdir(new File(outputdir));
      try (ZipFile zf = new ZipFile(zipfile, "UTF-8");) {
        Enumeration<?> zipArchiveEntrys = zf.getEntries();
        while (zipArchiveEntrys.hasMoreElements()) {
          ZipArchiveEntry zipArchiveEntry = (ZipArchiveEntry) zipArchiveEntrys.nextElement();
          if (zipArchiveEntry.isDirectory()) {
            org.apache.commons.io.FileUtils.forceMkdir(new File(outputdir + zipArchiveEntry.getName() + File.separator));
          } else {
            IOUtils.copy(zf.getInputStream(zipArchiveEntry),
                         org.apache.commons.io.FileUtils.openOutputStream(new File(outputdir + zipArchiveEntry.getName())));
          }
        }
      }
    } else {
      throw new IOException("Zipfile not existing: " + zipfilename);
    }
  }

}
