package it.fai.ms.consumi.service.notifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.validation.ValidationOutcome;

@Service
public class ProcessingErrorNotifierImpl implements ProcessingErrorNotifier {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  public ProcessingErrorNotifierImpl() {
  }

  @Override
  public void notify(final ValidationOutcome _validationOutcome) {

  }

}
