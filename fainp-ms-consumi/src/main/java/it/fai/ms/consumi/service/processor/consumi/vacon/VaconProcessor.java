package it.fai.ms.consumi.service.processor.consumi.vacon;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Vacon;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface VaconProcessor extends ConsumoProcessor<Vacon> {

}
