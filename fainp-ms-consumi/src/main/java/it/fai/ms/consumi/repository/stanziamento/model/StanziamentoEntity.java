package it.fai.ms.consumi.repository.stanziamento.model;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.TypeFlow;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGeneratorInterface;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "stanziamento")
public class StanziamentoEntity {

  @Column(name = "nr_articolo")
  private String articleCode;

  @Id
  @Column(name = "codice_stanziamento", unique = true, length = 60)
  private String code;

  @Column(name = "codice_cliente_fatturazione")
  private String codiceClienteFatturazione;

  @Column(name = "codicestanziamento_provvisorio", length = 60)
  private String codiceStanziamentoProvvisorio;

  @Column(name = "conguaglio")
  private boolean conguaglio = false;

  @Column(name = "costo" , precision = 16, scale = 5)
  private BigDecimal costo;

  @Column(name = "valuta")
  private String currency;

  @Column(name = "data_erogazione_servizio")
  private LocalDate dataErogazioneServizio;

  @Column(name = "data_acquisizione_flusso")
  private Instant dataAcquisizioneFlusso;

  @Column(name = "data_fattura")
  private Instant dataFattura;

  @Column(name = "data_registrazione")
  private Instant dataRegistrazione;

  @Column(name = "data_invio_nav")
  private Instant dateNavSent;

  @Column(name = "data_accodato_nav")
  private Instant dateQueuing;

  @Column(name = "desc_1")
  private String description1 = "";

  @Column(name = "desc_2")
  private String description2 = "";

  @Column(name = "desc_3")
  private String description3 = "";

  @ManyToMany(mappedBy = "stanziamenti", cascade = CascadeType.ALL)
  private final Set<DettaglioStanziamentoEntity> dettaglioStanziamenti = new HashSet<>();

  @Column(name = "genera_stanziamento")
  private GeneraStanziamento generaStanziamento;

  @Column(name = "stato_stanziamento")
  @Enumerated(EnumType.STRING)
  private InvoiceType invoiceType;

  @Column(name = "nr_cliente")
  private String numeroCliente;

  @Column(name = "numero_fattura")
  private String numeroFattura;

  @Column(name = "nr_fornitore")
  private String numeroFornitore;

  @Column(name = "paese")
  private String paese;

  @Column(name = "prezzo", precision = 16, scale = 5)
  private BigDecimal prezzo;

  @Column(name = "provvigioni_aquisite")
  private int provvigioniAquisite = 0;

  @Column(name = "quantita")
  private BigDecimal quantity;

  @Column(name = "storno")
  private String storno;

  @Column(name = "documento_da_fornitore")
  private String supplierDocument;

  @Column(name = "targa")
  private String targa;

  @Column(name = "nazione_targa")
  private String nazioneTarga;

  @Column(name = "tipo_flusso")
  private TypeFlow tipoFlusso;

  @Column(name = "nr_")
  private int totalAllocationDetails = 1;

  @Column(name = "classe_veicolo_euro")
  private String vehicleEuroClass;

  @Column(name = "anno_stanziamento")
  private Integer year;

  public StanziamentoEntity(final String _code) {
    code = _code;
  }

  protected StanziamentoEntity() {
  }

  public Integer getAnnoStanziamento() {
    return year;
  }

  public String getArticleCode() {
    return articleCode;
  }

  @Deprecated
  public String getClasseVeicoloEuro() {
    return vehicleEuroClass;
  }

  public String getCode() {
    return code;
  }

  public String getCodiceClienteFatturazione() {
    return codiceClienteFatturazione;
  }

  public String getCodiceStanziamentoProvvisorio() {
    return codiceStanziamentoProvvisorio;
  }

  public BigDecimal getCosto() {
    return costo;
  }

  public LocalDate getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public Instant getDataFattura() {
    return dataFattura;
  }

  public Instant getDataRegistrazione() {
    return dataRegistrazione;
  }

  public Instant getDateQueuing() {
    return dateQueuing;
  }

  public String getDescription1() {
    return description1;
  }

  public String getDescription2() {
    return description2;
  }

  public String getDescription3() {
    return description3;
  }

  public Set<DettaglioStanziamentoEntity> getDettaglioStanziamenti() {
    return dettaglioStanziamenti;
  }

  public GeneraStanziamento getGeneraStanziamento() {
    return generaStanziamento;
  }

  public Instant getNavSentDate() {
    return dateNavSent;
  }

  public String getNumeroCliente() {
    return numeroCliente;
  }

  public String getNumeroFattura() {
    return numeroFattura;
  }

  public String getNumeroFornitore() {
    return numeroFornitore;
  }

  public String getPaese() {
    return paese;
  }

  public BigDecimal getPrezzo() {
    return prezzo;
  }

  public int getProvvigioniAquisite() {
    return provvigioniAquisite;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public InvoiceType getStatoStanziamento() {
    return invoiceType;
  }

  public String getStorno() {
    return storno;
  }

  public String getSupplierDocument() {
    return supplierDocument;
  }

  public String getTarga() {
    return targa;
  }

  public TypeFlow getTipoFlusso() {
    return tipoFlusso;
  }

  public int getTotalAllocationDetails() {
    return totalAllocationDetails;
  }

  @Deprecated
  public String getValuta() {
    return currency;
  }

  public String getCurrency() {
    return currency;
  }

  public String getVehicleEuroClass() {
    return vehicleEuroClass;
  }

  public StanziamentoEntity incrementCounter() {
    setTotalAllocationDetails(getTotalAllocationDetails() + 1);
    return this;
  }

  public boolean isConguaglio() {
    return conguaglio;
  }

  public boolean notAlreadySent() {
    return dateQueuing == null;
  }

  public void refreshCode(StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator) {
    code = stanziamentoCodeGenerator.generate(invoiceType);
  }

  public void setAnnoStanziamento(final Integer _year) {
    year = _year;
  }

  public void setCodiceClienteFatturazione(String codiceClienteFatturazione) {
    this.codiceClienteFatturazione = codiceClienteFatturazione;
  }

  public void setCodiceStanziamentoProvvisorio(String codiceStanziamentoProvvisorio) {
    this.codiceStanziamentoProvvisorio = codiceStanziamentoProvvisorio;
  }

  public void setConguaglio(boolean conguaglio) {
    this.conguaglio = conguaglio;
  }

  public void setCosto(BigDecimal costo) {
    this.costo = costo;
  }

  public void setCurrency(final String _currency) {
    currency = _currency;
  }

  public void setDataErogazioneServizio(LocalDate dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }

  public void setDataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
  }

  public void setDataRegistrazione(Instant dataRegistrazione) {
    this.dataRegistrazione = dataRegistrazione;
  }

  public void setDateNavSent(final Instant _dateNavSent) {
    dateNavSent = _dateNavSent;
  }

  public void setDateQueuing(final Instant _dateQueuing) {
    dateQueuing = _dateQueuing;
  }

  public void setDescription1(final String _description1) {
    this.description1 = _description1;
  }

  public void setDescription2(final String _description2) {
    this.description2 = _description2;
  }

  public void setDescription3(final String _description3) {
    this.description3 = _description3;
  }

  public void setDocumentoDaFornitore(final String _supplierDocument) {
    supplierDocument = _supplierDocument;
  }

  public void setGeneraStanziamento(GeneraStanziamento generaStanziamento) {
    this.generaStanziamento = generaStanziamento;
  }

  public void setNumeroArticolo(final String _articleCode) {
    articleCode = _articleCode;
  }

  public void setArticleCode(final String _articleCode) {
    setNumeroArticolo(_articleCode);
  }

  public void setNumeroCliente(final String _numeroCliente) {
    numeroCliente = _numeroCliente;
  }

  public void setNumeroFattura(String numeroFattura) {
    this.numeroFattura = numeroFattura;
  }

  public void setNumeroFornitore(String numeroFornitore) {
    this.numeroFornitore = numeroFornitore;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public void setPrezzo(BigDecimal prezzo) {
    this.prezzo = prezzo;
  }

  public void setProvvigioniAquisite(int provvigioniAquisite) {
    this.provvigioniAquisite = provvigioniAquisite;
  }

  public void setQuantity(final BigDecimal _quantity) {
    if (_quantity != null) {
      quantity = _quantity.setScale(5, RoundingMode.HALF_UP);
    } else {
      quantity = BigDecimal.ZERO.setScale(5, RoundingMode.HALF_UP);
    }
  }

  public void setStatoStanziamento(final InvoiceType statoStanziamento) {
    this.invoiceType = statoStanziamento;
  }

  public void setStorno(String storno) {
    this.storno = storno;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public void setTipoFlusso(TypeFlow _typeFlow) {
    tipoFlusso = _typeFlow;
  }

  public void setTotalAllocationDetails(final int _totalAllocationDetails) {
    totalAllocationDetails = _totalAllocationDetails;
  }

  public void setVehicleEuroClass(final String _vehicleEuroClass) {
    vehicleEuroClass = _vehicleEuroClass;
  }

  public Instant getDataAcquisizioneFlusso() {
    return dataAcquisizioneFlusso;
  }

  public void setDataAcquisizioneFlusso(Instant dataAcquisizioneFlusso) {
    this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
  }

  public String getNazioneTarga() {
    return nazioneTarga;
  }

  public void setNazioneTarga(String nazioneTarga) {
    this.nazioneTarga = nazioneTarga;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final StanziamentoEntity obj = getClass().cast(_obj);
      res = Objects.equals(obj.code, code);
    }
    return res;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("StanziamentoEntity [");
    builder.append("code=");
    builder.append(code);
    builder.append(",invoiceType=");
    builder.append(invoiceType);
    builder.append(", tipoFlusso=");
    builder.append(tipoFlusso);
    builder.append(", dataErogazioneServizio=");
    builder.append(dataErogazioneServizio);
    builder.append(", dataAcquisizioneFlusso=");
    builder.append(dataAcquisizioneFlusso);
    builder.append(", numeroFornitore=");
    builder.append(numeroFornitore);
    builder.append(", numeroCliente=");
    builder.append(numeroCliente);
    builder.append(", articleCode=");
    builder.append(articleCode);
    builder.append(", paese=");
    builder.append(paese);
    builder.append(", year=");
    builder.append(year);
    builder.append(", targa=");
    builder.append(targa);
    builder.append(", nazioneTarga=");
    builder.append(nazioneTarga);
    builder.append(", vehicleEuroClass=");
    builder.append(vehicleEuroClass);
    builder.append(", dataRegistrazione=");
    builder.append(dataRegistrazione);
    builder.append(", prezzo=");
    builder.append(prezzo);
    builder.append(", costo=");
    builder.append(costo);
    builder.append(", quantity=");
    builder.append(quantity);
    builder.append(", totalAllocationDetails=");
    builder.append(totalAllocationDetails);
    builder.append(", currency=");
    builder.append(currency);
    builder.append(", descriptions=");
    builder.append(description1)
           .append(",")
           .append(description2)
           .append(",")
           .append(description3);
    builder.append(", generaStanziamento=");
    builder.append(generaStanziamento);
    builder.append(", storno=");
    builder.append(storno);
    builder.append(", provvigioniAquisite=");
    builder.append(provvigioniAquisite);
    builder.append(", conguaglio=");
    builder.append(conguaglio);
    builder.append(", codiceStanziamentoProvvisorio=");
    builder.append(codiceStanziamentoProvvisorio);
    builder.append(", supplierDocument=");
    builder.append(supplierDocument);
    builder.append(", numeroFattura=");
    builder.append(numeroFattura);
    builder.append(", dataFattura=");
    builder.append(dataFattura);
    builder.append(", codiceClienteFatturazione=");
    builder.append(codiceClienteFatturazione);
    builder.append("]");
    return builder.toString();
  }

}
