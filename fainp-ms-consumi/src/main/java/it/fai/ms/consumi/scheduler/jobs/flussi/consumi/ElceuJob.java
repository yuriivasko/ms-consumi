package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import it.fai.ms.consumi.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElceuRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

import java.util.concurrent.atomic.AtomicLong;

/*
 * Pedaggi Europeo Telepass Provvisorio (vacon sono i definitivi)
 */
@Service(ElceuJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class ElceuJob extends AbstractJob<ElceuRecord, Elceu> {

  public final static String QUALIFIER = "elceuJob";

  @Autowired
  private CacheService cacheService;

  public ElceuJob(final PlatformTransactionManager transactionManager,
                  final StanziamentiParamsValidator stanziamentiParamsValidator,
                  final NotificationService notificationService,
                  final RecordPersistenceService persistenceService,
                  final RecordDescriptor<ElceuRecord> recordDescriptor,
                  final ConsumoProcessor<Elceu> consumoProcessor,
                  final RecordConsumoMapper<ElceuRecord, Elceu> recordConsumoMapper,
                  final StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("ELCEU", transactionManager,  stanziamentiParamsValidator, notificationService, persistenceService,
          recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
  }

  @Override
  public Format getJobQualifier() {
    return Format.ELCEU;
  }

  @Override
  protected boolean isRecordToBeSkipped(String line, long detailIndex, ElceuRecord record) {
    if (super.isRecordToBeSkipped(line, detailIndex, record)){
      return true;
    }

    if (ElceuDescriptor.BEGIN_15.equals(record.getRecordCode())){
      Cache germanyCache = cacheService.getCache("ElceuJob_Germany_Cache");
      String previousStoredRecordKey = germanyCache.get(record.getFileName() + "§" + record.getTripId(), String.class);
      if (previousStoredRecordKey == null){
        germanyCache.put(record.getFileName() + "§" + record.getTripId(), record.getFileName() + "§" + record.getTripId());
      } else {
        return true;
      }
    }
    return false;
  }
}
