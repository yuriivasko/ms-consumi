package it.fai.ms.consumi.service.navision;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.adapter.nav.auth.NavAuthorizationTokenService;
import it.fai.ms.consumi.client.StanziamentiApiService;
import it.fai.ms.consumi.client.StanziamentiPostException;
import it.fai.ms.consumi.domain.navision.Stanziamento;

@Service
@Transactional
public class StanziamentiToNavService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final StanziamentiApiService       stanziamentiApi;
  private final NavAuthorizationTokenService navAuthorizationTokenService;

  @Inject
  public StanziamentiToNavService(final StanziamentiApiService _stanziamentiApi,
                                  final NavAuthorizationTokenService _navAuthorizationTokenService) {
    stanziamentiApi = _stanziamentiApi;
    navAuthorizationTokenService = _navAuthorizationTokenService;
  }
  
  @PostConstruct
  public void post() {
     log.debug("StanziamentiToNavService postconstruct={}",stanziamentiApi);
  }

  public void notifyStanziamentoToNav(final List<Stanziamento> _stanziamentiNav) throws StanziamentiPostException {
    if (!_stanziamentiNav.isEmpty()) {
      log.info("Sending {} stanziamenti to nav", _stanziamentiNav.size());
      log.trace("Sending to Nav : {}", _stanziamentiNav);
      stanziamentiApi.stanziamentiPost(getAuthorizationToken(), _stanziamentiNav);
      log.info("Sent stanziamenti");
    }
  }

  private String getAuthorizationToken() {
    return navAuthorizationTokenService.getAuthorizationToken();
  }

}
