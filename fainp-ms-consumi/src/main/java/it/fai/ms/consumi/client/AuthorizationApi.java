package it.fai.ms.consumi.client;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import feign.Headers;

public interface AuthorizationApi {

  public final static String REQUEST_HEADER = "Authorization";

  @PostMapping(value = "/login", params = { "grant_type", "username", "password" }, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  @Headers("Content-Type: application/x-www-form-urlencoded")
  public LoginResponse login(@RequestBody String body);

  String getAuthorizationToken(String grant_type, String username, String password);

  public class LoginResponse {

    private String access_token;
    private String token_type;
    private String expires_in;

    public String getAccess_token() {
      return access_token;
    }

    public void setAccess_token(String access_token) {
      this.access_token = access_token;
    }

    public String getToken_type() {
      return token_type;
    }

    public void setToken_type(String token_type) {
      this.token_type = token_type;
    }

    public String getExpires_in() {
      return expires_in;
    }

    public void setExpires_in(String expires_in) {
      this.expires_in = expires_in;
    }
  }

}
