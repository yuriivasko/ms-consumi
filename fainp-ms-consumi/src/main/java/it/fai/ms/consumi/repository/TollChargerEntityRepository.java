package it.fai.ms.consumi.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.consumi.repository.tollcharger.model.TollChargerEntity;

@Repository
public interface TollChargerEntityRepository extends JpaRepository<TollChargerEntity, Long> {

  Optional<TollChargerEntity> findOneByCode(String code);
  
  TollChargerEntity findFirstByOrderByIdDesc();
  
}
