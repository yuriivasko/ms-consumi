package it.fai.ms.consumi.domain.fatturazione;

import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;

public class AllegatiConfiguration {

  private String codiceRaggruppamentoArticolo;

  private String descrizione;
  
  private String transCodeAllegatoFix;

  private boolean generaAllegato;

  private DettaglioStanziamentoType tipoDettaglioStanziamento;
  
  private TemplateAllegati template;

  public String getCodiceRaggruppamentoArticolo() {
    return codiceRaggruppamentoArticolo;
  }

  public void setCodiceRaggruppamentoArticolo(String codiceRaggruppamentoArticolo) {
    this.codiceRaggruppamentoArticolo = codiceRaggruppamentoArticolo;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public String getTransCodeAllegatoFix() {
    return transCodeAllegatoFix;
  }

  public void setTransCodeAllegatoFix(String transCodeAllegatoFix) {
    this.transCodeAllegatoFix = transCodeAllegatoFix;
  }

  public boolean isGeneraAllegato() {
    return generaAllegato;
  }

  public void setGeneraAllegato(boolean generaAllegato) {
    this.generaAllegato = generaAllegato;
  }

  public DettaglioStanziamentoType getTipoDettaglioStanziamento() {
    return tipoDettaglioStanziamento;
  }

  public void setTipoDettaglioStanziamento(DettaglioStanziamentoType dettaglioStanziamento) {
    this.tipoDettaglioStanziamento = dettaglioStanziamento;
  }

  public TemplateAllegati getTemplate() {
    return template;
  }

  public void setTemplate(TemplateAllegati template) {
    this.template = template;
  }
  
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AllegatiConfiguration [codiceRaggruppamentoArticolo=");
    builder.append(codiceRaggruppamentoArticolo);
    builder.append(", descrizione=");
    builder.append(descrizione);
    builder.append(", transCodeAllegatoFix=");
    builder.append(transCodeAllegatoFix);
    builder.append(", generaAllegato=");
    builder.append(generaAllegato);
    builder.append(", tipoDettaglioStanziamento=");
    builder.append(tipoDettaglioStanziamento);
    builder.append(", template=");
    builder.append(template);
    builder.append("]");
    return builder.toString();
  }

}
