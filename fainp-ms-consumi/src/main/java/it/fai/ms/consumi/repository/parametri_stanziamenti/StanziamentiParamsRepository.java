package it.fai.ms.consumi.repository.parametri_stanziamenti;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsArticleDTO;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiDetailsTypeEntity;

public interface StanziamentiParamsRepository {

  Optional<StanziamentiParams> findFirstByArticleCode(@NotNull String articleCode);
  
  Optional<List<StanziamentiParams>> findByArticleCode(@NotNull String articleCode);
  
  Optional<List<StanziamentiParams>> findByTransactionDetail(@NotNull String transactionDetail);

  List<StanziamentiParams> findBySourceAndRecordCode(@NotNull String source, @NotNull String recordCode);

  AllStanziamentiParams getAll();

  List<StanziamentiParamsArticleDTO> findAllStanziamentiParamsArticles();

  Optional<StanziamentiParams> findByArticleAndRecordCodeAndSupplierCodeAndTransactionalDetailAndTipoDettaglio(String numeroArticolo,
                                                                                                               String recordCode,
                                                                                                               String code,
                                                                                                               String detailCode,
                                                                                                               StanziamentiDetailsType carburanti);

  Optional<StanziamentiParams> findBySourceAndLegacyCode(@NotNull String _source, @NotNull String _legacyCode);

  Optional<String> findDescriptionByRaggruppamentoArticleCode(String articleCode);

  List<StanziamentiParams> findByTipoDettaglio(StanziamentiDetailsType _tipo);

  StanziamentiParams updateStanziamentiParams(StanziamentiParams stanziamentiParams);

  Optional<StanziamentiDetailsType> findByTracciatoAndTransactionDetail(@NotNull String tracciato, @NotNull String transactionDetail);

  Optional<StanziamentiParams> findByTransactionDetailAndCodiceFornitoreAndRecordCodeAndTipoDettaglio(@NotNull String transactionDetail,
                                                                                                      @NotNull String codiceFornitore,
                                                                                                      @NotNull String recordCode,
                                                                                                      @NotNull StanziamentiDetailsTypeEntity detailType);

  Optional<StanziamentiParams> findFirstByCodiceRaggruppamentoArticolo(@NotNull String raggruppamentoArticolo);

}
