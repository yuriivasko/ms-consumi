package it.fai.ms.consumi.jms.notification_v2;

import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import it.fai.ms.common.jms.notification_v2.NotificationMessage;
import it.fai.ms.common.jms.notification_v2.NotificationTarget;
import it.fai.ms.common.jms.notification_v2.NotificationType;
import it.fai.ms.consumi.service.util.FaiConsumiFilesUtil;

import java.util.List;

public class ConsumoReprocessableNotification
  extends EnrichedNotification<NotificationMessage> {

  private String job_name;
  private String fileName;
  private long fileLine;
  private String recordType;
  private String recordUUID;
  private String recordCode;
  private String transactionDetail;
  private String original_message;
  private String original_record;

  public ConsumoReprocessableNotification(String jobName, String fileName, long fileLine, String recordType, String recordUUID, String recordCode, String transactionDetail, String originalRecord, String notificationCode, String message) {
    super(notificationCode, null);
    this.job_name = jobName;
    this.fileName = FaiConsumiFilesUtil.getFilename(fileName);
    this.fileLine = fileLine;
    this.recordType = recordType;
    this.recordUUID = recordUUID;
    this.recordCode = recordCode;
    this.transactionDetail = transactionDetail;
    this.original_message = message;
    this.original_record = originalRecord;
  }

  public String enrichNotificationText(String text){
    return "Tracciato: " + job_name + ", " +  fileName + ", riga: " +  fileLine + ", recordCode: " +  recordCode + ", transactionDetail: " + transactionDetail + " - " + text;
  }

  @Override
  public NotificationMessage get(NotificationType type, List<NotificationTarget> targets){
    return new NotificationMessage(source, code, NotificationType.CONSUMO_ERROR_REPROCESSABLE, targets);
  }

}
