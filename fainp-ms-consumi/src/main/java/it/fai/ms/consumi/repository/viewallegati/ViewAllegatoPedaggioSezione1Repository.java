package it.fai.ms.consumi.repository.viewallegati;

import java.time.LocalDate;
import java.util.Set;

public interface ViewAllegatoPedaggioSezione1Repository {

  Set<ViewAllegatoPedaggioSezione1Entity> findBySezioneIdClass(ViewAllegatoPedaggioSezione1Id keyIdClass);

  Set<ViewAllegatoPedaggioSezione1Entity> findBySezioneIdClassAndRangeDateNotInvoiced(ViewAllegatoPedaggioSezione1Id keyIdClass, LocalDate dateFrom,
                                                                           LocalDate dateTo);

}
