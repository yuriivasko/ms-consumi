package it.fai.ms.consumi.service.record.telepass;

import java.time.Instant;
import java.util.Optional;

import javax.money.MonetaryAmount;

import it.fai.ms.consumi.domain.*;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Vacon;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.VaconGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryRecord;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.VaconDescriptor;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class VaconRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<VaconRecord, Vacon> {

  private static GlobalIdentifier newGlobalIDentifierByRecordCode(final VaconRecord _commonRecord) {
    return _commonRecord.getRecordCode()
                        .equals("20") ? new VaconGlobalIdentifier(_commonRecord.getGlobalIdentifier(), GlobalIdentifierType.INTERNAL)
                                      : new VaconGlobalIdentifier(_commonRecord.getGlobalIdentifier(), GlobalIdentifierType.EXTERNAL);
  }

  @Override
  public Vacon mapRecordToConsumo(VaconRecord commonRecord) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, TelepassSourceName.VACON);
    source.setFileName(commonRecord.getFileName());

    source.setRowNumber(commonRecord.getRowNumber());
    // TODO gestire record 20
    Vacon toConsumo = new Vacon(source, commonRecord.getRecordCode(), Optional.of(newGlobalIDentifierByRecordCode(commonRecord)), commonRecord);
    Consumo toConsumoBase = toConsumo;
    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();
    // TODO è giusto questo riferimento della transazione id;
    transaction.setDetailCode(commonRecord.getTransactionDetail());
    toConsumoBase.setTransaction(transaction);
    toConsumoBase.setSupplierInvoiceDocument(commonRecord.getDocumentNumber());

    // CONTRACT -> assente su questo flusso
    if (commonRecord.getTelepassCustomerID() != null && !commonRecord.getTelepassCustomerID()
      .trim()
      .isEmpty()) {
      Contract contract = new Contract(commonRecord.getTelepassCustomerID());
      toConsumoBase.setContract(Optional.of(contract));
    }

    // CONSUMO.AMOUNT
    MonetaryRecord monetaryRecord = commonRecord;
    Amount amount = new Amount();
    amount.setExchangeRate(null);
    amount.setVatRate(monetaryRecord.getVatRateBigDecimal());
    MonetaryAmount amountExcludedVat = (monetaryRecord.getAmount_novat() != null) ? monetaryRecord.getAmount_novat() : monetaryRecord.getAmount_novat_excluded_discount();
    if (amountExcludedVat!=null && !amountExcludedVat.isZero()) {
      amount.setAmountExcludedVat(amountExcludedVat);
    }else {
      amount.setAmountIncludedVat(monetaryRecord.getAmount_vat());
    }
    toConsumoBase.setAmount(amount);
    // CONSUMO altri campi
    ConsumoPedaggio toConsumoPedaggio = toConsumo;
    toConsumoPedaggio.setProcessed(commonRecord.isProcessed());

    // CONSUMOPEDAGGIO.DEVICE
    VaconRecord vaconRecord = commonRecord;

    if (vaconRecord.getTelepassPanNumber() != null && !vaconRecord.getTelepassPanNumber()
      .trim()
      .isEmpty()){

      //L'Obu non è obbligatorio e non e' detto che sia il nostro "obunumber". la coppia PAN+TIPOSERVIZIO invece e' sempre univoca
      Device device = new Device(TipoDispositivoEnum.TELEPASS_EUROPEO);
      device.setPan(vaconRecord.getTelepassPanNumber());
      device.setServiceType(vaconRecord.getService_type());
      toConsumoPedaggio.setDevice(device);
    }

    // CONSUMOPEDAGGIO.TOLLPOINTENTRY
    TollPoint tollpointEntry = new TollPoint();
    GlobalGate globalGateEntry = new GlobalGate(vaconRecord.getEntryGateCode());
    // globalGateEntry.setDescription(); //TODO STCON

    if (vaconRecord.getEntryTimestamp() != null && !vaconRecord.getEntryTimestamp()
                                                               .isEmpty()) {
      tollpointEntry.setTime(parseTimestamp(vaconRecord.getEntryTimestamp()));
    } else if (vaconRecord.getEntryDate() != null && !vaconRecord.getEntryDate()
                                                                 .isEmpty()) {
      tollpointEntry.setTime(calculateInstantByDateAndTime(vaconRecord.getEntryDate(), vaconRecord.getEntryTime(), yyyyMMdd_dateformat,
                                                           HHmmss_timeformat).get());
    }

    tollpointEntry.setGlobalGate(globalGateEntry);
    toConsumoPedaggio.setTollPointEntry(tollpointEntry);

    // CONSUMOPEDAGGIO.TOLLPOINTEXIT
    TollPoint tollpointExit = new TollPoint();
    GlobalGate globalGateExit = new GlobalGate(vaconRecord.getExitGateCode());
    // globalGateExit.setDescription(); //TODO STCON

    if (vaconRecord.getExitTimestamp() != null && !vaconRecord.getExitTimestamp()
                                                               .isEmpty()) {
      tollpointExit.setTime(parseTimestamp(vaconRecord.getExitTimestamp()));
    } else if (vaconRecord.getExitDate() != null && !vaconRecord.getExitDate()
                                                                 .isEmpty()) {
      tollpointExit.setTime(calculateInstantByDateAndTime(vaconRecord.getExitDate(), vaconRecord.getExitTime(), yyyyMMdd_dateformat,
                                                          HHmmss_timeformat).get());
    }

    tollpointExit.setGlobalGate(globalGateExit);
    toConsumoPedaggio.setTollPointExit(tollpointExit);

    //
    // //CONSUMOPEDAGGIO.VEHICLE
    Vehicle vehicle = new Vehicle();
    vehicle.setFareClass(vaconRecord.getPricingClass());
    toConsumoPedaggio.setVehicle(vehicle);
    //
    // //CONSUMOPEDAGGIO. altri campi
    // toConsumoPedaggio.setTotalAmount(new BigDecimal(commonRecord.getTotalAmount()));
    toConsumoPedaggio.setSplitNumber(vaconRecord.getSplitNumber());
    toConsumoPedaggio.setCompensationNumber(vaconRecord.getCompensationNumber());
    toConsumoPedaggio.setTspExitCountryCode(vaconRecord.getTspExitCountryCode());
    toConsumoPedaggio.setTspExitNumber(vaconRecord.getTspExitNumber());
    toConsumoPedaggio.setTspRespCountryCode(vaconRecord.getTspRespCountryCode());
    toConsumoPedaggio.setTspRespNumber(vaconRecord.getTspRespNumber());
    toConsumoPedaggio.setTollCharger(vaconRecord.getTollCharger());
    toConsumoPedaggio.setTollGate(vaconRecord.getTollGate());
    toConsumoPedaggio.setLaneId(vaconRecord.getLaneId());
    toConsumoPedaggio.setNetworkCode(vaconRecord.getEuropeanNetworkCode());
    if(vaconRecord.getRecordCode().equals(VaconDescriptor.BEGIN_20)) {
        toConsumoPedaggio.getTransaction()
        .setSign(vaconRecord.getSignOfAmountWithoutVAT());
    }else {
    	toConsumoPedaggio.getTransaction()
    				  .setSign(vaconRecord.getSignOfTransaction());
    }
    toConsumoPedaggio.setAdditionalInfo(vaconRecord.getAdditionalInfo());
    toConsumoPedaggio.setRegion(vaconRecord.getRegion());
    toConsumoPedaggio.setRoute(vaconRecord.getRoute());
    toConsumoPedaggio.setRoadType(vaconRecord.getRoadType());
    // toConsumoPedaggio.setImponibileAdr(); --> non applicabile // Non è da settare;

    return toConsumo;
  }

}
