/*
 * Copyright 2016-2018 the original author or authors from the JHipster project.
 *
 * This file is part of the JHipster project, see https://www.jhipster.tech/
 * for more information.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.fai.ms.consumi.config;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.Servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.util.StopWatch;
import org.springframework.web.servlet.DispatcherServlet;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.config.apidoc.customizer.JHipsterSwaggerCustomizer;
import io.github.jhipster.config.apidoc.customizer.SwaggerCustomizer;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Springfox Swagger configuration.
 * <p>
 * Warning! When having a lot of REST endpoints, Springfox can become a performance issue.
 * In that case, you can use the "no-swagger" Spring profile, so that this bean is ignored.
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({
    ApiInfo.class,
    BeanValidatorPluginsConfiguration.class,
    Servlet.class,
    DispatcherServlet.class
})
@AutoConfigureAfter(JHipsterProperties.class)
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerTrxAutoConfiguration {

    public static final String STARTING_MESSAGE = "Starting Swagger Transactions";
    public static final String STARTED_MESSAGE = "Started Swagger Transactions in {} ms";
    public static final String MANAGEMENT_GROUP_NAME = "managementTrx";
    public static final String MANAGEMENT_DESCRIPTION = "Management endpoints documentation Transactions";

    private final Logger log = LoggerFactory.getLogger(SwaggerTrxAutoConfiguration.class);

    private final JHipsterProperties.Swagger properties;

    public SwaggerTrxAutoConfiguration(JHipsterProperties jHipsterProperties) {
        this.properties = new JHipsterProperties.Swagger();
        this.properties.setContactEmail("roberto.griffa@digitalmill.it");
        this.properties.setContactName("Roberto Griffa");
        this.properties.setContactUrl("http://www.faiservice.it");
        this.properties.setDefaultIncludePattern("/api/trx/.*");
        this.properties.setDescription("");
        this.properties.setHost("");
        this.properties.setLicense("");
        this.properties.setLicenseUrl("");
        this.properties.setProtocols(jHipsterProperties.getSwagger().getProtocols());
        this.properties.setTermsOfServiceUrl(jHipsterProperties.getSwagger().getTermsOfServiceUrl());
        this.properties.setTitle("Api Transactions");
        this.properties.setVersion("0.0.1");
    }

    /**
     * Springfox configuration for the API Swagger docs.
     *
     * @param swaggerCustomizers Swagger customizers
     * @param alternateTypeRules alternate type rules
     * @return the Swagger Springfox configuration
     */
    @Bean
    @ConditionalOnMissingBean(name = "swaggerSpringfoxApiDocketTrx")
    public Docket swaggerSpringfoxApiDocketTrx(
        ObjectProvider<AlternateTypeRule[]> alternateTypeRules) {
      
      List<SwaggerCustomizer> swaggerCustomizers = Arrays.asList( new JHipsterSwaggerCustomizer(properties));
        log.info(STARTING_MESSAGE);
        StopWatch watch = new StopWatch();
        watch.start();

        Docket docket = createDocket();

        // Apply all SwaggerCustomizers orderly.
        swaggerCustomizers.forEach(customizer -> customizer.customize(docket));

        // Add all AlternateTypeRules if available in spring bean factory.
        // Also you can add your rules in a customizer bean above.
        Optional.ofNullable(alternateTypeRules.getIfAvailable()).ifPresent(docket::alternateTypeRules);

        watch.stop();
        log.info(STARTED_MESSAGE, watch.getTotalTimeMillis());
        return docket;
    }

    protected Docket createDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName(MANAGEMENT_GROUP_NAME);
    }

}
