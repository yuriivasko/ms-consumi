package it.fai.ms.consumi.repository.canoni;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.common.jms.efservice.message_v2.device.StatoDispositivoServizioUpdateMessage.StatoDS;
import it.fai.ms.consumi.service.canoni.DispositivoForCanoni;

@Service

public class CanoniRepository {
  private Logger                    log = LoggerFactory.getLogger(getClass());
  private static final List<String> DEVICE_GREEN_STATUS = StatoDispositivo.getGreenState()
    .stream()
    .map(s -> s.name())
    .collect(Collectors.toList());

  private static final List<String> GREEN_STATUS = Arrays.asList(StatoDS.ATTIVO.name());

  private final EntityManager em;

  public CanoniRepository(EntityManager em) {
    this.em = em;
  }
  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.SUPPORTS)
  public Stream<String> elencoClienti(Instant dataDa, Instant dataA, TipoDispositivoEnum tipoDispositivo) {
    String sqlString =
    // @formatter:off
        "select  " +
        "         distinct  c.codice_azienda " +
        "         from view_storico_dispositivo d " +
        "            left join view_storico_contratto c " +
        "                on d.contratto_dml_unique_identifier = c.dml_unique_identifier " +
        "                and c.data_variazione <= :dataA " +
        "                and (c.data_fine is null or c.data_fine >= :dataDa )" +
        "            left join storico_dispositivo d2 on  " +
        "                d2.dml_unique_identifier = d.dml_unique_identifier  " +
        "                and d.stato in :stati " +
        "         where  " +
        "             d.tipo_dispositivo = :tipoDispositivo " +
        "             and d.data_modifica_stato <= :dataA " +
        "             and (d.data_fine is null or d.data_fine >= :dataDa) " +
        "             and d.stato in :stati " ;
    // @formatter:on

    Query query = em.createNativeQuery(sqlString);
   query.setParameter("tipoDispositivo", tipoDispositivo.name());
   query.setParameter("dataA", dataA);
   query.setParameter("dataDa", dataDa);
   query.setParameter("stati", DEVICE_GREEN_STATUS);
    @SuppressWarnings("unchecked")
    Stream<String> resultStream = query.getResultStream();
    return resultStream;
  }
  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.SUPPORTS)
  public Stream<DispositivoForCanoni> dispositiviAttivi(Instant dataDa, Instant dataA, TipoDispositivoEnum tipoDispositivo,
                                                        String codiceCliente) {
    String sqlString =
    // @formatter:off
        "select " +
        "            storico.dml_unique_identifier, " +
        "            storico.codice_azienda, " +
        "            storico.contratto_dml_unique_identifier,  " +
        "            storico.data_prima_attivazione, " +
        "            sum(DATEDIFF(hh,storico.data_inizio,storico.data_fine)) as durata " +
        "from ( " +
        "        select  " +
        "            d.dml_unique_identifier, " +
        "            case WHEN d.data_fine < :dataA then d.data_fine else :dataA end as data_fine, " +
        "            case WHEN d.data_modifica_stato > :dataDa then d.data_modifica_stato else :dataDa end as data_inizio, " +
        "            c.codice_azienda, " +
        "            d.contratto_dml_unique_identifier as contratto_dml_unique_identifier, " +
        "            min (d2.data_modifica_stato) as data_prima_attivazione " +
        "         from view_storico_dispositivo d " +
        "            left join view_storico_contratto c " +
        "                on d.contratto_dml_unique_identifier = c.dml_unique_identifier " +
        "                and c.data_variazione <= :dataA " +
        "                and (c.data_fine is null or c.data_fine >= :dataA) " +
        "            left join storico_dispositivo d2 on  " +
        "                d2.dml_unique_identifier = d.dml_unique_identifier  " +
        "                and d.stato in :stati " +
        "         where  " +
        "             d.tipo_dispositivo = :tipoDispositivo " +
        "             and d.data_modifica_stato <= :dataA " +
        "             and (d.data_fine is null or d.data_fine >= :dataDa) " +
        "             and d.stato in :stati " +
        "             and c.codice_azienda = :codiceCliente " +
        "        group by  " +
        "            d.dml_unique_identifier, " +
        "            d.data_fine, " +
        "            d.data_modifica_stato, " +
        "            c.codice_azienda, " +
        "            d.contratto_dml_unique_identifier " +
        ") storico group by  " +
        "            storico.dml_unique_identifier, " +
        "            storico.codice_azienda, " +
        "            storico.contratto_dml_unique_identifier, " +
        "            storico.data_prima_attivazione " +
        "having " +
        "    sum(DATEDIFF(hh, storico.data_inizio,storico.data_fine)) > 24 "
;
 // @formatter:on
    Query dispositiviAttivi = em.createNativeQuery(sqlString);
    dispositiviAttivi.setParameter("tipoDispositivo", tipoDispositivo.name());
    dispositiviAttivi.setParameter("dataA", dataA);
    dispositiviAttivi.setParameter("dataDa", dataDa);
    dispositiviAttivi.setParameter("stati", DEVICE_GREEN_STATUS);
    dispositiviAttivi.setParameter("codiceCliente", codiceCliente);

    @SuppressWarnings("unchecked")
    Stream<Object[]>             resultStream2 = dispositiviAttivi.getResultStream();
    Stream<DispositivoForCanoni> resultStream  = resultStream2.map(a -> {
                                                 // storico.seriale_dispositivo, " +
                                                 // storico.codice_azienda, " +
                                                 // storico.contratto_numero, " +
                                                 // storico.data_prima_attivazione, " +
                                                 // sum(DATEDIFF(hh,storico.data_fine,storico.data_inizio)) as durata "
                                                 // +

                                                 String       idDispositivo        = (String) a[0];
                                                 String       codiceAzienda        = (String) a[1];
                                                 final String numeroContratto      = (String) a[2];
                                                 final var    dataPrimaAttivazione = (Timestamp) a[3];
                                                 log.trace("{},{},duration: {}",codiceAzienda,idDispositivo,a[4]);
                                                 return new DispositivoForCanoni(idDispositivo, numeroContratto, codiceAzienda, dataPrimaAttivazione);
                                               });

    return resultStream;
  }
  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.REQUIRES_NEW)
  public LocalDate getDataPrimaAttivazioneDispositivo(String idDispositivo) {
    String sqlString =
    // @formatter:off
      " select " +
      "    min(d.data_modifica_stato) " +
      " from " +
      "    storico_dispositivo d " +
      " where " +
      "    d.dml_unique_identifier = :id " +
      "    and d.stato in :greenStatus ";
    // @formatter:on
    Query query = em.createNativeQuery(sqlString);
    query.setParameter("id", idDispositivo);
    query.setParameter("greenStatus", DEVICE_GREEN_STATUS);
    Timestamp dataAttivazione = (Timestamp) query.getSingleResult();

    return dataAttivazione.toLocalDateTime()
      .toLocalDate();
  }
  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.SUPPORTS)
  public Stream<String> elencoClienti(Instant dataDa, Instant dataA, TipoServizioEnum tipoServizio) {
    String sqlString =
    // @formatter:off
        "    select " +
        "        c.codice_azienda " +
        "    from " +
        "        view_storico_stato_servizio s " +
        "    left join view_storico_dispositivo d on " +
        "        s.dispositivo_dml_unique_identifier = d.dml_unique_identifier " +
        "        and d.data_modifica_stato <= :dataA " +
        "        and (d.data_fine is null " +
        "        or d.data_fine >= :dataA) " +
        "    left join view_storico_contratto c on " +
        "        d.contratto_dml_unique_identifier = c.dml_unique_identifier " +
        "        and c.data_variazione <= :dataA " +
        "        and (c.data_fine is null or c.data_fine >= :dataA) " +
        "    WHERE " +
        "        s.data_variazione <= :dataA " +
        "        and s.tipo_servizio = :tipoServizio " +
        "        and (s.data_fine is null OR s.data_fine >= :dataDa) " +
        "        and s.stato in :stati  " ;
    // @formatter:on

    Query query = em.createNativeQuery(sqlString);
   query.setParameter("tipoServizio", tipoServizio.name());
   query.setParameter("dataA", dataA);
   query.setParameter("dataDa", dataDa);
   query.setParameter("stati", DEVICE_GREEN_STATUS);
    @SuppressWarnings("unchecked")
    Stream<String> resultStream = query.getResultStream();
    return resultStream;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.SUPPORTS)
  public Stream<DispositivoForCanoni> serviziAttivi(Instant dataDa, Instant dataA, TipoServizioEnum tipoServizio, String codiceCliente) {
    String sqlString =
    // @formatter:off
        "select  " +
        "    storico.dispositivo_dml_unique_identifier,  " +
        "    storico.codice_azienda,  " +
        "    storico.contratto_dml_unique_identifier,   " +
        "    storico.data_prima_attivazione,  " +
        "    sum(DATEDIFF(hh,storico.data_inizio,storico.data_fine)) as durata    " +
        "from " +
        "( " +
        "select " +
        "    ser.dispositivo_dml_unique_identifier, " +
        "    case WHEN ser.data_fine < :dataA then ser.data_fine else :dataA " +
        "    end as data_fine, " +
        "    case WHEN ser.data_variazione > :dataDa then ser.data_variazione else :dataDa " +
        "    end as data_inizio, " +
        "    ser.codice_azienda, " +
        "    ser.contratto_dml_unique_identifier, " +
        "    min(s1.data_variazione) as data_prima_attivazione " +
        "from " +
        "    ( " +
        "    select " +
        "        distinct s.*, " +
        "        d.contratto_dml_unique_identifier, " +
        "        c.codice_azienda " +
        "    from " +
        "        view_storico_stato_servizio s " +
        "    left join view_storico_dispositivo d on " +
        "        s.dispositivo_dml_unique_identifier = d.dml_unique_identifier " +
        "        and d.data_modifica_stato <= :dataA " +
        "        and (d.data_fine is null " +
        "        or d.data_fine >= :dataA) " +
        "    left join view_storico_contratto c on " +
        "        d.contratto_dml_unique_identifier = c.dml_unique_identifier " +
        "        and c.data_variazione <= :dataA " +
        "        and (c.data_fine is null or c.data_fine >= :dataA) " +
        "    WHERE " +
        "        s.data_variazione <= :dataA " +
        "        and s.tipo_servizio = :tipoServizio " +
        "        and s.data_fine >= :dataDa " +
        "        and s.stato in :stati  " +
        "        and c.codice_azienda = :codiceCliente  " +
        "        ) ser " +
        "left join storico_stato_servizio s1 on " +
        "    s1.dispositivo_dml_unique_identifier = ser.dispositivo_dml_unique_identifier " +
        "    and s1.tipo_servizio = ser.tipo_servizio " +
        "    and s1.stato in :stati " +
        "group by " +
        "    ser.dispositivo_dml_unique_identifier, " +
        "    ser.data_fine, " +
        "    ser.data_variazione, " +
        "    ser.codice_azienda, " +
        "    ser.contratto_dml_unique_identifier " +
        ") storico group by  " +
        "    storico.dispositivo_dml_unique_identifier,  " +
        "    storico.codice_azienda,  " +
        "    storico.contratto_dml_unique_identifier,   " +
        "    storico.data_prima_attivazione " +
        "having sum(DATEDIFF(hh, storico.data_inizio,storico.data_fine)) > 24 "
        ;//+ " order by d.dml_unique_identifier,d.data_modifica_stato";
 // @formatter:on
    Query dispositiviAttivi = em.createNativeQuery(sqlString);
    dispositiviAttivi.setParameter("tipoServizio", tipoServizio.name());
    dispositiviAttivi.setParameter("dataA", dataA);
    dispositiviAttivi.setParameter("dataDa", dataDa);
    dispositiviAttivi.setParameter("codiceCliente", codiceCliente);
    dispositiviAttivi.setParameter("stati", GREEN_STATUS);

    @SuppressWarnings("unchecked")
    Stream<Object[]>             resultStream2 = dispositiviAttivi.getResultStream();
    Stream<DispositivoForCanoni> resultStream  = resultStream2.map(a -> {

                                                 String       idDispositivo        = (String) a[0];
                                                 String       codiceAzienda        = (String) a[1];
                                                 final String numeroContratto      = (String) a[2];
                                                 final var    dataPrimaAttivazione = (Timestamp) a[3];
                                                 log.trace("{},{},duration: {}", codiceAzienda, idDispositivo, a[4]);
                                                 return new DispositivoForCanoni(idDispositivo, numeroContratto, codiceAzienda,
                                                                                 dataPrimaAttivazione);
                                               });
    return resultStream;
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.REQUIRES_NEW)
  public LocalDate getDataPrimaAttivazioneServizio(String idDispositivo) {
    String sqlString =
    // @formatter:off
          " select " +
          "    min(d.data_variazione) " +
          " from " +
          "    storico_stato_servizio d " +
          " where " +
          "    d.dispositivo_dml_unique_identifier = :id " +
          "    and d.stato in :greenStatus ";
        // @formatter:on
    Query query = em.createNativeQuery(sqlString);
    query.setParameter("id", idDispositivo);
    query.setParameter("greenStatus", GREEN_STATUS);
    Timestamp dataAttivazione = (Timestamp) query.getSingleResult();

    return dataAttivazione.toLocalDateTime()
      .toLocalDate();

  }

}
