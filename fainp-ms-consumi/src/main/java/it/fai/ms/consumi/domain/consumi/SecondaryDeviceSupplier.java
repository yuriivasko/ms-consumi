package it.fai.ms.consumi.domain.consumi;

import java.util.Optional;

import it.fai.ms.consumi.domain.Device;

public interface SecondaryDeviceSupplier
  extends DeviceSupplier{

  Optional<Device> getSecondaryDevice();

}
