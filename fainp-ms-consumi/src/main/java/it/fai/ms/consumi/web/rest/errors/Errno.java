package it.fai.ms.consumi.web.rest.errors;

/**
 * https://exagespa.atlassian.net/wiki/spaces/FAIMYS/pages/2555909/Convenzioni+sul+codice
 * https://exagespa.atlassian.net/browse/FAINP-541
 */
public enum Errno {
  //@formatter:off
  UNKNOWN(9000, "unknown", "unknown"),
  RECORD_NOT_RETRIEVABLE(4221, "ERROR! Record not retrievable!", "Cannot find record to reprocess with given type and uuid!"),
  RECORD_NOT_REPROCESSED(4221, "ERROR! Record not reprocessed!", "Reprocessing record with given type and uuid return errors: notification will be sent."),
  ;
  //@formatter:on

  private final Integer code;
  private final String  internalMessage;
  private final String  description;

  Errno(Integer code, String internalMessage, String description) {
    this.code = code;
    this.internalMessage = internalMessage;
    this.description = description;
  }

  public Integer code() {
    return code;
  }

  public String internalMessage() {
    return internalMessage;
  }

  public String description() {
    return description;
  }

}
