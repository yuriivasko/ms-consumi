package it.fai.ms.consumi.service.validator.internal;

import java.time.Instant;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.consumi.DateSupplier;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.ContractService;
import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;

@Service
public class ContractValidatorImpl implements ContractValidator {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final ContractService contractService;
  private final boolean         validatorEnabled;

  @Inject
  public ContractValidatorImpl(final ContractService _contractService,
                               @Value("#{new Boolean(${validator.contract:false})}") final boolean _validatorEnabled) {
    contractService = _contractService;
    validatorEnabled = _validatorEnabled;
    _log.debug("Validator enabled 'validator.contract' : {}", _validatorEnabled);
  }

  @Override
  public ValidationOutcome findAndSetContract(final Contract _contract) {

    var validationOutcome = new ValidationOutcome();

    final var optionalContract = contractService.findContractByNumber(_contract.getId());
    if (!optionalContract.isPresent()) {
      validationOutcome.addMessage(new Message("b003").withText("Contract " + _contract.getId() + "not found"));
    } else {
      _contract.setCompanyCode(optionalContract.get().getCompanyCode());
      _contract.setState(optionalContract.get().getState());
      if(_contract.getCompanyCode()==null || _contract.getCompanyCode().isEmpty()) {
        validationOutcome.addMessage(new Message("b003").withText("Contract " + _contract.getId() + " has a null or empty company code"));
      }
    }

    _log.debug("Contract exists validator result : {}", validationOutcome);
    if (!validatorEnabled) {
      validationOutcome = new ValidationOutcome();
      _log.debug("Contract exists validator disabled, response overrided : {}", validationOutcome);
    }

    return validationOutcome;
  }

  @Override
  public ValidationOutcome contractActiveInDate(String _contractNumber, Instant _date) {
    ValidationOutcome rs = new ValidationOutcome();

    Instant dateOffset = _date.plus(DateSupplier.VALIDATOR_DATE_OFFSET);

    _log.trace("Check if customer {} was active in date {}", _contractNumber, dateOffset);

    boolean isActive = contractService.isContractActiveInDate(_contractNumber, dateOffset);
    if(!isActive) {
      rs.addMessage(new Message("w002").withText("Contract "+_contractNumber+" is not active in date "+FaiConsumiDateUtil.formatInstant(_date, DateSupplier.VALIDATOR_DATE_OFFSET)));
    }

    return rs;
  }

}
