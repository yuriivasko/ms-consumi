package it.fai.ms.consumi.service.parametri_stanziamenti;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsQuery;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

public interface StanziamentiParamsService {

  Optional<StanziamentiParams> findByQuery(StanziamentiParamsQuery query);

  AllStanziamentiParams getAll();

  Optional<StanziamentiParams> findByDettaglioStanziamentoCarburante(DettaglioStanziamentoCarburanteEntity dettaglio,
                                                                     StanziamentoEntity stanziamento);

  Optional<String> findDescriptionArticleByRaggruppamentoArticleCode(String raggruppamentoArticleCode);

  void updateStanziamentiParams(StanziamentiParams stanziamentiParams);

  Optional<StanziamentiParams> findByTrx(String routeType, // OIL, NO_OIL //non usato attualmente
                                         StanziamentiDetailsType type, // CARBURANTI, GENERICO
                                         @NotNull String articleCode, // GASOLIO, BIODIESEL
                                         @NotNull String vendorCode, // IT063
                                         String unit, // LT
                                         @NotNull String currency, // EUR
                                         BigDecimal vatPercentage, // 22
                                         @NotNull String productCode);

  Optional<StanziamentiDetailsType> findConsumoTypeByTracciatoAndTransactionDetail(Format format, String typeMovement);

  Set<String> findArticleCodesByTipoDettaglioStanziamento(StanziamentiDetailsType type);
}
