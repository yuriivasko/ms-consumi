package it.fai.ms.consumi.domain.consumi;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.enumeration.PriceSource;

public class ServicePartner {

  private Format       format;
  private final String id;
  private String       partnerCode;
  private String       partnerName;
  private PriceSource  priceSource;

  @JsonCreator
  public ServicePartner(@JsonProperty("id") final String _id) {
    id = Objects.requireNonNull(_id, "Parameter id is mandatory");
  }




  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final ServicePartner obj = getClass().cast(_obj);
      res = Objects.equals(obj.id, id);
    }
    return res;
  }

  public Format getFormat() {
    return format;
  }

  public String getId() {
    return id;
  }

  public String getPartnerCode() {
    return partnerCode;
  }

  public String getPartnerName() {
    return partnerName;
  }

  public PriceSource getPriceSoruce() {
    return priceSource;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, partnerCode, partnerName, priceSource, format);
  }

  public void setFormat(final Format _format) {
    format = _format;
  }

  public void setPartnerCode(final String _partnerCode) {
    partnerCode = _partnerCode;
  }

  public void setPartnerName(final String _partnerName) {
    partnerName = _partnerName;
  }

  public void setPriceSoruce(PriceSource priceSoruce) {
    this.priceSource = priceSoruce;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("id=")
                              .append(id)
                              .append(",partnerCode=")
                              .append(partnerCode)
                              .append(",partnerName=")
                              .append(partnerName)
                              .append(",priceSource=")
                              .append(priceSource)
                              .append(",format=")
                              .append(format)
                              .append("]")
                              .toString();
  }

}
