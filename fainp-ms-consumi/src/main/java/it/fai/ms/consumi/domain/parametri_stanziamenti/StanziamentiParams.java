package it.fai.ms.consumi.domain.parametri_stanziamenti;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.Supplier;

public class StanziamentiParams implements Serializable{
  private static final long serialVersionUID = -6658926684142194619L;

  private Long                    uuid;
  private Article                 article;
  private CostoRicavo             costoRicavo;
  private String                  fareClass;
  private String                  recordCode;
  private String                  source;
  private StanziamentiDetailsType stanziamentiDetailsType;
  private final Supplier          supplier;
  private String                  transactionDetail;
  private String                  transactionType;

  public StanziamentiParams(final Supplier _supplier) {
    supplier = Objects.requireNonNull(_supplier, "Supplier parameter is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final StanziamentiParams obj = getClass().cast(_obj);
      res = Objects.equals(obj.source, source) && Objects.equals(obj.recordCode, recordCode)
            && Objects.equals(obj.transactionDetail, transactionDetail) && Objects.equals(obj.article, article)
            && Objects.equals(obj.supplier, supplier) && Objects.equals(obj.fareClass, fareClass);
    }
    return res;
  }

  public Article getArticle() {
    return article;
  }

  public CostoRicavo getCostoRicavo() {
    return costoRicavo;
  }

  public String getFareClass() {
    return fareClass;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public String getSource() {
    return source;
  }

  public StanziamentiDetailsType getStanziamentiDetailsType() {
    return stanziamentiDetailsType;
  }

  public Supplier getSupplier() {
    return supplier;
  }

  public String getTransactionDetail() {
    return transactionDetail;
  }

  public String getTransactionType() {
    return transactionType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(source, recordCode, transactionDetail, article, supplier, fareClass);
  }

  public boolean matchVatRate(BigDecimal _vatRate) {
    boolean match = article != null && article.getVatRate() != null;
    match = match && _vatRate != null && _vatRate.floatValue() == article.getVatRate();
    return match;
  }

  public boolean matchTransactionDetail(final String _transactionDetail) {
    if (StringUtils.isBlank(_transactionDetail) && StringUtils.isBlank(transactionDetail))
      return true;
    return StringUtils.isNotBlank(_transactionDetail) && _transactionDetail.equalsIgnoreCase(transactionDetail);
  }

  public boolean match(final String _transactionDetail, final BigDecimal _vatRate) {
    return matchTransactionDetail(_transactionDetail) && matchVatRate(_vatRate);
  }

  public boolean match(final String _transactionDetail, final BigDecimal _vatRate, final String _fareClass) {
    return match(_transactionDetail, _vatRate) && matchFareClass(_fareClass);
  }

  public boolean matchFareClass(final String _fareClass) {
    return _fareClass != null && _fareClass.equalsIgnoreCase(fareClass);
  }

  public boolean matchCodiceFornitoreLegacy(final String _codiceFornitoreLegacy) {
    return this.supplier != null && _codiceFornitoreLegacy.equals(this.supplier.getCodeLegacy());
  }

  public void setArticle(final Article _article) {
    article = _article;
  }

  public void setCostoRicavo(CostoRicavo _costoRicavo) {
    costoRicavo = _costoRicavo;
  }

  public void setFareClass(final String _fareClass) {
    fareClass = _fareClass;
  }

  public void setRecordCode(final String _recordCode) {
    recordCode = _recordCode;
  }

  public void setSource(final String _source) {
    source = _source;
  }

  public void setStanzaimentiDetailsType(final StanziamentiDetailsType _stanzimentiDetailsType) {
    stanziamentiDetailsType = _stanzimentiDetailsType;
  }

  public void setTransactionDetail(final String _transactionDetail) {
    transactionDetail = _transactionDetail;
  }

  public void setTransactionType(final String _transactionType) {
    transactionType = _transactionType;
  }

  public boolean isCosto() {
    return this.costoRicavo != null && (this.costoRicavo == CostoRicavo.C || this.costoRicavo == CostoRicavo.CR);
  }

  public boolean isRicavo() {
    return this.costoRicavo != null && (this.costoRicavo == CostoRicavo.R || this.costoRicavo == CostoRicavo.CR);
  }

  public void setUuid(Long uuid) {
    this.uuid = uuid;
  }

  public Long getUuid() {
    return uuid;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append(article)
                              .append(",")
                              .append(supplier)
                              .append(",fareClass=")
                              .append(fareClass)
                              .append(",source=")
                              .append(source)
                              .append(",transactionDetail=")
                              .append(transactionDetail)
                              .append(",transactionType=")
                              .append(transactionType)
                              .append(",recordCode=")
                              .append(recordCode)
                              .append("]")
                              .toString();
  }

}
