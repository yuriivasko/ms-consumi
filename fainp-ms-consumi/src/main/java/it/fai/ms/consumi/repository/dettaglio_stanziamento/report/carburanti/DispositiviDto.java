package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti;

import java.math.BigDecimal;

import it.fai.common.enumeration.TipoDispositivoEnum;

public class DispositiviDto {


	private String codiceClienteNav;
	private String licensePlate;
	private String licensePlateCountry;
	private TipoDispositivoEnum deviceType;
	private String obu;
	private BigDecimal total;
	private Long id;

	public DispositiviDto() {
	}

	public DispositiviDto(String codiceClienteNav, String licensePlate,
			String licensePlateCountry, /*TipoDispositivoEnum deviceType,*/ String obu) {
		super();
		this.codiceClienteNav = codiceClienteNav;
		this.licensePlate = licensePlate;
		this.licensePlateCountry = licensePlateCountry;
//		this.deviceType = deviceType;
		this.obu = obu;
	}

	@Override
	public String toString() {
		return "MasterDto [codiceClienteNav=" + codiceClienteNav  + ", licensePlate=" + licensePlate + ", licensePlateCountry="
				+ licensePlateCountry + ", deviceType=" + deviceType + ", obu=" + obu + "]";
	}

	public String getCodiceClienteNav() {
		return codiceClienteNav;
	}
	public void setCodiceClienteNav(String codiceClienteNav) {
		this.codiceClienteNav = codiceClienteNav;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getLicensePlateCountry() {
		return licensePlateCountry;
	}
	public void setLicensePlateCountry(String licensePlateCountry) {
		this.licensePlateCountry = licensePlateCountry;
	}
	public TipoDispositivoEnum getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(TipoDispositivoEnum deviceType) {
		this.deviceType = deviceType;
	}
	public String getObu() {
		return obu;
	}
	public void setObu(String obu) {
		this.obu = obu;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codiceClienteNav == null) ? 0 : codiceClienteNav.hashCode());
		result = prime * result + ((deviceType == null) ? 0 : deviceType.hashCode());
		result = prime * result + ((licensePlate == null) ? 0 : licensePlate.hashCode());
		result = prime * result + ((licensePlateCountry == null) ? 0 : licensePlateCountry.hashCode());
		result = prime * result + ((obu == null) ? 0 : obu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DispositiviDto other = (DispositiviDto) obj;
		if (codiceClienteNav == null) {
			if (other.codiceClienteNav != null)
				return false;
		} else if (!codiceClienteNav.equals(other.codiceClienteNav))
			return false;
		if (deviceType == null) {
			if (other.deviceType != null)
				return false;
		} else if (!deviceType.equals(other.deviceType))
			return false;
		if (licensePlate == null) {
			if (other.licensePlate != null)
				return false;
		} else if (!licensePlate.equals(other.licensePlate))
			return false;
		if (licensePlateCountry == null) {
			if (other.licensePlateCountry != null)
				return false;
		} else if (!licensePlateCountry.equals(other.licensePlateCountry))
			return false;
		if (obu == null) {
			if (other.obu != null)
				return false;
		} else if (!obu.equals(other.obu))
			return false;
		return true;
	}

  public BigDecimal getTotal() {
    return total;
  }

  public void setTotal(BigDecimal total) {
    this.total = total;
  }


}
