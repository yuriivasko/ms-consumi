package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class ElceuGlobalIdentifier extends GlobalIdentifier {

  public ElceuGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.EXTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    throw new IllegalStateException("newGlobalIdentifier operation is supported only for global identifier type internal");
  }

}
