package it.fai.ms.consumi.domain.parametri_stanziamenti;

import java.util.Objects;

public class StanziamentiParamsArticleDTO {

  private String code;
  private String description = "";

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    StanziamentiParamsArticleDTO that = (StanziamentiParamsArticleDTO) o;
    return Objects.equals(code, that.code) &&
      Objects.equals(description, that.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, description);
  }

  @Override
  public String toString() {
    return "StanziamentiParamsArticleDTO{" +
      "code='" + code + '\'' +
      ", description='" + description + '\'' +
      '}';
  }
}
