package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio;

import java.math.RoundingMode;
import java.util.Optional;

import javax.inject.Inject;
import javax.money.Monetary;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;

@Component
@Validated
public class DettaglioStanziamentoPedaggioEntityMapper {

  @Inject
  public DettaglioStanziamentoPedaggioEntityMapper() {
  }

  public DettaglioStanziamentoPedaggio toDomain(@NotNull final DettaglioStanziamentoPedaggioEntity _entity) {
    final var entity = _entity;
    final var pedaggio = new DettaglioStanziamentoPedaggio(new GlobalIdentifier(GlobalIdentifier.decodeId(_entity.getGlobalIdentifier()),
                                                                                GlobalIdentifier.decodeType(_entity.getGlobalIdentifier()),
                                                                                Optional.ofNullable(entity.getId())) {
      @Override
      public GlobalIdentifier newGlobalIdentifier() {
        return newGlobalIdentifier();
      }
    });

    pedaggio.setRecordCode(_entity.getRecordCode());

    pedaggio.setAmount(new Amount());
    pedaggio.getAmount()
            .setAmountExcludedVat(Monetary.getDefaultAmountFactory()
                                          .setCurrency(entity.getCurrencyCode())
                                          .setNumber(entity.getAmountNoVat()
                                                           .setScale(5, RoundingMode.HALF_UP))
                                          .create());
    pedaggio.getAmount()
            .setAmountIncludedVat(Monetary.getDefaultAmountFactory()
                                          .setCurrency(entity.getCurrencyCode())
                                          .setNumber(entity.getAmountIncludingVat()
                                                           .setScale(5, RoundingMode.HALF_UP))
                                          .create());
    pedaggio.getAmount()
            .setExchangeRate(entity.getExchangeRate());
    pedaggio.getAmount()
            .setVatRate(entity.getAmountVatRate());

    pedaggio.setTollPointEntry(new TollPoint());
    if (entity.getEntryGlobalGateIdentifier()!=null) {
       pedaggio.getTollPointEntry().setGlobalGate(new GlobalGate(entity.getEntryGlobalGateIdentifier()));
       pedaggio.getTollPointEntry().getGlobalGate().setDescription((entity.getEntryGlobalGateIdentifierDescription()));
    }
    pedaggio.getTollPointEntry().setTime(entity.getEntryTimestamp());

    pedaggio.setTollPointExit(new TollPoint());
    if (entity.getExitGlobalGateIdentifier()!=null) {
      pedaggio.getTollPointExit().setGlobalGate(new GlobalGate(entity.getExitGlobalGateIdentifier()));
      pedaggio.getTollPointExit().getGlobalGate().setDescription((entity.getExitGlobalGateIdentifierDescription()));
   }
   pedaggio.getTollPointExit().setTime(entity.getExitTimestamp());


    pedaggio.setDate(entity.getExitTimestamp()!=null ? entity.getExitTimestamp() : entity.getDataAcquisizioneFlusso());

    pedaggio.setInvoiceType(entity.getInvoiceType());

    pedaggio.setRegion(entity.getRegion());

    pedaggio.setSource(new Source(_entity.getIngestionTime(), _entity.getDataAcquisizioneFlusso(), entity.getSourceType()));
    pedaggio.getSource()
            .setRowNumber(entity.getSourceRowNumber());

    pedaggio.setTransaction(new Transaction());
    pedaggio.getTransaction()
            .setDetailCode(entity.getTransactionDetailCode());
    pedaggio.getTransaction()
            .setSign(entity.getTransactionSign());

    pedaggio.setNetworkCode(entity.getNetwordCode());

    pedaggio.setRouteName(entity.getTratta());

    pedaggio.setArticlesGroup(entity.getArticlesGroup());

    pedaggio.setCustomer(new Customer(entity.getCustomerId()));

    pedaggio.setContract(Contract.newUnsafeContract(entity.getContractCode(), entity.getCustomerId(), null));

    if (entity.getDeviceType()!=null) {
      if(entity.getDeviceObu()!=null)
        pedaggio.setDevice(new Device(entity.getDeviceObu(),entity.getDeviceType()));
      else
        pedaggio.setDevice(new Device(entity.getDeviceType()));
      pedaggio.getDevice()
           .setPan(entity.getDevicePan());
    }
    pedaggio.setVehicle(new Vehicle());
    pedaggio.getVehicle()
            .setEuroClass(entity.getVehicleEuroClass());
    pedaggio.getVehicle()
            .setFareClass(entity.getVehicleFareClass());
    if(entity.getVehicleLicenseLicenseId()!=null && entity.getVehicleLicenseCountryId()!=null) {
      pedaggio.getVehicle()
            .setLicensePlate(new VehicleLicensePlate(entity.getVehicleLicenseLicenseId(), entity.getVehicleLicenseCountryId()));
    }

    pedaggio.setImponibileAdr(entity.getAdrTaxableAmount());

    pedaggio.setSupplier(new Supplier(entity.getSupplierCode()));
    pedaggio.getSupplier()
            .setCodeLegacy(entity.getSupplierCodeLegacy());
    pedaggio.getSupplier()
            .setDocument(entity.getSupplierDocument());

    return pedaggio;
  }

  public DettaglioStanziamentoPedaggioEntity toEntity(@NotNull final DettaglioStanziamentoPedaggio _domain) {

    var entity = new DettaglioStanziamentoPedaggioEntity(_domain.getGlobalIdentifier()
      .encode(),
      Optional.ofNullable(_domain.getGlobalIdentifier().getDbId()).orElse(_domain.getGlobalIdentifier().getDbId()));

    return toEntity(_domain, entity);
  }

  public DettaglioStanziamentoPedaggioEntity toEntity(@NotNull final DettaglioStanziamentoPedaggio _domain, @NotNull DettaglioStanziamentoPedaggioEntity entity) {

    entity.setAmountIncludedVat(_domain.getAmount()
                                       .getAmountIncludedVatBigDecimal());
    entity.setAmountExcludedVat(_domain.getAmount()
                                       .getAmountExcludedVatBigDecimal());
    entity.setExchangeRate(_domain.getAmount()
                                  .getExchangeRate());

    entity.setCustomerId(_domain.getCustomer() != null ? _domain.getCustomer()
                                                                .getId()
                                                       : "");

    if (_domain.getSupplier() != null) {
      entity.setCodiceFornitoreNav(_domain.getSupplier()
                                          .getCode());
      entity.setSupplierCodeLegacy(_domain.getSupplier()
                                          .getCodeLegacy());
      entity.setSupplierDocument(_domain.getSupplier()
                                        .getDocument());
    }

    entity.setCurrencyCode(_domain.getAmount()
                                  .getCurrency()
                                  .getCurrencyCode());

    if (_domain.getTollPointEntry() != null) {
      if (_domain.getTollPointEntry()
                 .getGlobalGate() != null) {
        entity.setEntryGlobalGateIdentifier(_domain.getTollPointEntry()
                                                   .getGlobalGate()
                                                   .getId());
        entity.setEntryGlobalGateIdentifierDescription(_domain.getTollPointEntry()
                                                              .getGlobalGate()
                                                              .getDescription());
      }
      entity.setEntryTimestamp(_domain.getTollPointEntry()
                                      .getTime());
    }

    if (_domain.getTollPointExit() != null) {
      if (_domain.getTollPointExit()
                 .getGlobalGate() != null) {
        entity.setExitGlobalGateIdentifier(_domain.getTollPointExit()
                                                  .getGlobalGate()
                                                  .getId());
        entity.setExitGlobalGateIdentifierDescription(_domain.getTollPointExit()
                                                             .getGlobalGate()
                                                             .getDescription());
      }
      entity.setExitTimestamp(_domain.getTollPointExit()
                                     .getTime());
    }

    entity.setFileName(_domain.getSource()
                              .getFileName());
    entity.setAdrTaxableAmount(_domain.getImponibileAdr());

    entity.setIngestionTime(_domain.getSource().getIngestionTime());
    entity.setDataAcquisizioneFlusso(_domain.getSource().getAcquisitionDate());

    entity.setInvoiceType(_domain.getInvoiceType());

    if (_domain.getVehicle() != null) {
      entity.setVehicleEuroClass(_domain.getVehicle()
                                        .getEuroClass());
      entity.setVehicleFareClass(_domain.getVehicle()
                                        .getFareClass());
      if (_domain.getVehicle()
                 .getLicensePlate() != null) {
        entity.setVehicleLicenseLicenseId(_domain.getVehicle()
                                                 .getLicensePlate()
                                                 .getLicenseId());
        entity.setVehicleLicenseCountryId(_domain.getVehicle()
                                                 .getLicensePlate()
                                                 .getCountryId());
      }
    }

    if (_domain.getDevice() != null) {
      entity.setDeviceObu(_domain.getDevice()
                                 .getId());
      entity.setDevicePan(_domain.getDevice()
                                 .getPan());
    }

    entity.setAmountVatRate(_domain.getAmount()
                                   .getVatRateBigDecimal());

    entity.setRaggruppamentoArticoliNav(_domain.getArticlesGroup());
    entity.setRecordCode(_domain.getRecordCode());
    entity.setContractCode(_domain.getContract()!=null ? _domain.getContract().getId():null );
    entity.setSourceRowNumber(_domain.getSource()
                                     .getRowNumber());
    entity.setSourceType(_domain.getSource()
                                .getType());

    if (_domain.getTransaction() != null) {
      entity.setTransactionDetailCode(_domain.getTransaction()
                                             .getDetailCode());
      entity.setTransactionSign(_domain.getTransaction()
                                       .getSign());
    }
    entity.setNetwordCode(_domain.getNetworkCode());
    entity.setTratta(_domain.getRouteName());

    entity.setRegion(_domain.getRegion());

    entity.setDeviceType(_domain.getDevice() != null ? _domain.getDevice()
                                                              .getType() : null);

    return entity;
  }

}
