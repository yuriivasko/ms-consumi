package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici;

import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.generici.dartford.Dartford;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.dartford.DartfordProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.dartford.DartfordRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(DartfordJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class DartfordJob extends AbstractJob<DartfordRecord, Dartford> {

  public final static String QUALIFIER = "DartfordJob";

  @SuppressWarnings("unused")
  private final transient Logger log = LoggerFactory.getLogger(getClass());


  public DartfordJob(final PlatformTransactionManager transactionManager,
                     final StanziamentiParamsValidator stanziamentiParamsValidator,
                     final NotificationService notificationService,
                     final RecordPersistenceService persistenceService,
                     final DartfordRecordDescriptor recordDescriptor,
                     final DartfordProcessor consumoGenericoProcessor,
                     final DartfordRecordConsumoGenericoMapper recordConsumoGenericoMapper,
                     final StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("dartford", transactionManager, stanziamentiParamsValidator, notificationService, persistenceService,
          recordDescriptor, consumoGenericoProcessor, recordConsumoGenericoMapper, stanziamentiToNavJmsProducer);
  }

  @Override
  protected RecordsFileInfo validateFile(String _filename, long _startTime, Instant _ingestionTime) {
    //no generic flat file validation: is CSV!
    var fileInfo = new RecordsFileInfo(_filename, _startTime, _ingestionTime);
    DartfordRecord header = new DartfordRecord(_filename, 0);
    fileInfo.setHeader(header);
    return fileInfo;
  }


  @Override
  public Format getJobQualifier() {
    return Format.DARTFORD;
  }
}
