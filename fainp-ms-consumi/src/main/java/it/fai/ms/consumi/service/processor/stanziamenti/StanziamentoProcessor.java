package it.fai.ms.consumi.service.processor.stanziamenti;

import java.util.List;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

public interface StanziamentoProcessor {

  List<Stanziamento> processClonedDStanziamentoForCase2aWithDiffAmount(@NotNull DettaglioStanziamento allocationDetailDCloned,
                                                                       @NotNull StanziamentiParams allocationParams);

  List<Stanziamento> processCreatedStanziamentoForCase2aWithDiffAmount(@NotNull DettaglioStanziamento allocationDetailCreated,
                                                                       @NotNull StanziamentiParams allocationParams);

  List<Stanziamento> processStanziamento(@NotNull DettaglioStanziamento allocationDetail, @NotNull StanziamentiParams stanziamentiParams);

  List<Stanziamento> processStanziamentoForCase2aWithSameAmount(@NotNull DettaglioStanziamento persistedAllocationDetail,
                                                                @NotNull StanziamentiParams stanziamentiParams);

}
