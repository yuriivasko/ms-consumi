package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.carburante.TokheimConsumo;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.tokheim.TokheimProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.carburanti.TokheimRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(TokheimJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class TokheimJob extends AbstractJob<TokheimRecord, TokheimConsumo> {

  public final static String QUALIFIER = "tokheim";

  @SuppressWarnings("unused")
  private final transient Logger log = LoggerFactory.getLogger(getClass());


  public TokheimJob(final PlatformTransactionManager transactionManager,
                     final StanziamentiParamsValidator stanziamentiParamsValidator,
                     final NotificationService notificationService,
                     final RecordPersistenceService persistenceService,
                     final RecordDescriptor<TokheimRecord> recordDescriptor,
                     final TokheimProcessor consumoGenericoProcessor,
                     final TokheimRecordConsumoMapper recordConsumoGenericoMapper,
                     final StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("tokheim", transactionManager, stanziamentiParamsValidator, notificationService, persistenceService,
          recordDescriptor, consumoGenericoProcessor, recordConsumoGenericoMapper, stanziamentiToNavJmsProducer);
  }


  @Override
  public Format getJobQualifier() {
    return Format.TOKHEIM;
  }
}
