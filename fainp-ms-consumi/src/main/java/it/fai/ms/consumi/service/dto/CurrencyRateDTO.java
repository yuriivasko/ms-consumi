package it.fai.ms.consumi.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the CurrencyRate entity.
 */
public class CurrencyRateDTO implements Serializable {

  private Long id;

  private Instant time;

  private String currency;

  private BigDecimal rate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Instant getTime() {
    return time;
  }

  public void setTime(Instant time) {
    this.time = time;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CurrencyRateDTO currencyRateDTO = (CurrencyRateDTO) o;
    if (currencyRateDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), currencyRateDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "CurrencyRateDTO{" + "id=" + getId() + ", time='" + getTime() + "'" + ", currency='" + getCurrency() + "'" + ", rate="
           + getRate() + "}";
  }
}
