package it.fai.ms.consumi.domain.fatturazione.enumeration;

public enum DettaglioStanziamentoType {

  PEDAGGIO("PEDAGGIO"), CARBURANTE("CARBURANTE"), GENERICO("GENERICO"), TRENO("TRENI");

  private String tableName;

  DettaglioStanziamentoType(final String _tableName) {
    tableName = _tableName;
  }

  public String getTableName() {
    return tableName;
  }

}
