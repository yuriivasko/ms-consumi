package it.fai.ms.consumi.service.jms.model;

import static java.util.stream.Collectors.toSet;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class StanziamentoMessage implements Serializable {

  private static final long serialVersionUID = -8690510227656704332L;

  public static Set<String> extractCodes(final List<StanziamentoMessage> _stanziamentoMessages) {
    return Optional.ofNullable(_stanziamentoMessages)
                   .orElse(Collections.emptyList())
                   .stream()
                   .map(stanziamentoMessage -> stanziamentoMessage.getCode())
                   .collect(toSet());
  }

  public static Instant now() {
    return Instant.now();
  }

  private String         code;
  private String         codicestanziamentoProvvisorio;
  private boolean        conguaglio;
  private Double         costo;
  private String         country;
  private LocalDate      dataErogazioneServizio;
  private Instant        dataAcquisizioneFlusso;
  private final String[] descriptions = new String[] { "", "", "" };
  private String         documentoDaFornitore;
  private int            generaStanziamento;
  private int            nr;
  private String         nrArticolo;
  private String         nrCliente;
  private String         nrFornitore;
  private Double         prezzo;
  private Double         quantity;
  private int            statoStanziamento;
  private int            tipoFlusso;
  private String         valuta;
  private String         vehicleEuroClass;
  private String         vehicleLicenseId;
  private String         nazioneTarga;
  private Integer        year;

  public StanziamentoMessage(final String _code) {
    code = Objects.requireNonNull(_code, "Code parameter is mandatory");
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    StanziamentoMessage other = (StanziamentoMessage) obj;
    if (year == null) {
      if (other.year != null) {
        return false;
      }
    } else if (!year.equals(other.year)) {
      return false;
    }
    if (vehicleEuroClass == null) {
      if (other.vehicleEuroClass != null) {
        return false;
      }
    } else if (!vehicleEuroClass.equals(other.vehicleEuroClass)) {
      return false;
    }
    if (code == null) {
      if (other.code != null) {
        return false;
      }
    } else if (!code.equals(other.code)) {
      return false;
    }
    if (codicestanziamentoProvvisorio == null) {
      if (other.codicestanziamentoProvvisorio != null) {
        return false;
      }
    } else if (!codicestanziamentoProvvisorio.equals(other.codicestanziamentoProvvisorio)) {
      return false;
    }
    if (conguaglio != other.conguaglio) {
      return false;
    }
    if (costo == null) {
      if (other.costo != null) {
        return false;
      }
    } else if (!costo.equals(other.costo)) {
      return false;
    }
    if (dataErogazioneServizio == null) {
      if (other.dataErogazioneServizio != null) {
        return false;
      }
    } else if (!dataErogazioneServizio.equals(other.dataErogazioneServizio)) {
      return false;
    }
    if (dataAcquisizioneFlusso == null) {
      if (other.dataAcquisizioneFlusso != null) {
        return false;
      }
    } else if (!dataAcquisizioneFlusso.equals(other.dataAcquisizioneFlusso)) {
      return false;
    }
    if (descriptions == null) {
      if (other.descriptions != null) {
        return false;
      }
    } else if (!descriptions.equals(other.descriptions)) {
      return false;
    }
    if (documentoDaFornitore == null) {
      if (other.documentoDaFornitore != null) {
        return false;
      }
    } else if (!documentoDaFornitore.equals(other.documentoDaFornitore)) {
      return false;
    }
    if (generaStanziamento != other.generaStanziamento) {
      return false;
    }
    if (nr != other.nr) {
      return false;
    }
    if (nrArticolo == null) {
      if (other.nrArticolo != null) {
        return false;
      }
    } else if (!nrArticolo.equals(other.nrArticolo)) {
      return false;
    }
    if (nrCliente == null) {
      if (other.nrCliente != null) {
        return false;
      }
    } else if (!nrCliente.equals(other.nrCliente)) {
      return false;
    }
    if (nrFornitore == null) {
      if (other.nrFornitore != null) {
        return false;
      }
    } else if (!nrFornitore.equals(other.nrFornitore)) {
      return false;
    }
    if (country == null) {
      if (other.country != null) {
        return false;
      }
    } else if (!country.equals(other.country)) {
      return false;
    }
    if (prezzo == null) {
      if (other.prezzo != null) {
        return false;
      }
    } else if (!prezzo.equals(other.prezzo)) {
      return false;
    }
    if (quantity == null) {
      if (other.quantity != null) {
        return false;
      }
    } else if (!quantity.equals(other.quantity)) {
      return false;
    }
    if (statoStanziamento != other.statoStanziamento) {
      return false;
    }
    if (vehicleLicenseId == null) {
      if (other.vehicleLicenseId != null) {
        return false;
      }
    } else if (!vehicleLicenseId.equals(other.vehicleLicenseId)) {
      return false;
    }
    if (tipoFlusso != other.tipoFlusso) {
      return false;
    }
    if (valuta == null) {
      if (other.valuta != null) {
        return false;
      }
    } else if (!valuta.equals(other.valuta)) {
      return false;
    }
    if (nazioneTarga == null) {
      if (other.nazioneTarga != null) {
        return false;
      }
    } else if (!nazioneTarga.equals(other.nazioneTarga)) {
      return false;
    }
    return true;
  }

  public Integer getAnnoStanziamento() {
    return year;
  }

  public String getClasseEuroVeicolo() {
    return vehicleEuroClass;
  }

  public String getCode() {
    return code;
  }

  public String getCodicestanziamentoProvvisorio() {
    return codicestanziamentoProvvisorio;
  }

  public Double getCosto() {
    return costo;
  }

  public LocalDate getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public String[] getDescriptions() {
    return descriptions;
  }

  public String getDocumentoDaFornitore() {
    return documentoDaFornitore;
  }

  public int getGeneraStanziamento() {
    return generaStanziamento;
  }

  public int getNr() {
    return nr;
  }

  public String getNrArticolo() {
    return nrArticolo;
  }

  public String getNrCliente() {
    return nrCliente;
  }

  public String getNrFornitore() {
    return nrFornitore;
  }

  public String getPaese() {
    return country;
  }

  public Double getPrezzo() {
    return prezzo;
  }

  public Double getQuantita() {
    return quantity;
  }

  public int getStatoStanziamento() {
    return statoStanziamento;
  }

  public String getTarga() {
    return vehicleLicenseId;
  }

  public int getTipoFlusso() {
    return tipoFlusso;
  }

  public String getValuta() {
    return valuta;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }

  public boolean isConguaglio() {
    return conguaglio;
  }

  public void setAnnoStanziamento(Integer anno_Stanziamento) {
    this.year = anno_Stanziamento;
  }

  public void setClasseEuroVeicolo(String classeEuroVeicolo) {
    this.vehicleEuroClass = classeEuroVeicolo;
  }

  public void setCodicestanziamentoProvvisorio(String codicestanziamentoProvvisorio) {
    this.codicestanziamentoProvvisorio = codicestanziamentoProvvisorio;
  }

  public void setConguaglio(boolean conguaglio) {
    this.conguaglio = conguaglio;
  }

  public void setCosto(Double costo) {
    this.costo = costo;
  }

  public void setDataErogazioneServizio(LocalDate data_erogazione_servizio) {
    this.dataErogazioneServizio = data_erogazione_servizio;
  }

  public void setDocumentoDaFornitore(String documentoDaFornitore) {
    this.documentoDaFornitore = documentoDaFornitore;
  }

  public void setGeneraStanziamento(int genera_stanziamento) {
    this.generaStanziamento = genera_stanziamento;
  }

  public void setNr(int nr) {
    this.nr = nr;
  }

  public void setNrArticolo(String nr_articolo) {
    this.nrArticolo = nr_articolo;
  }

  public void setNrCliente(String nr_cliente) {
    this.nrCliente = nr_cliente;
  }

  public void setNrFornitore(String nr_fornitore) {
    this.nrFornitore = nr_fornitore;
  }

  public void setPaese(String paese) {
    this.country = paese;
  }

  public void setPrezzo(Double prezzo) {
    this.prezzo = prezzo;
  }

  public void setQuantita(double quantita) {
    this.quantity = quantita;
  }

  public void setStatoStanziamento(int stato_Stanziamento) {
    this.statoStanziamento = stato_Stanziamento;
  }

  public void setTarga(String targa) {
    this.vehicleLicenseId = targa;
  }

  public void setTipoFlusso(int tipo_flusso) {
    this.tipoFlusso = tipo_flusso;
  }

  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

  public Instant getDataAcquisizioneFlusso() {
    return dataAcquisizioneFlusso;
  }

  public void setDataAcquisizioneFlusso(Instant dataAcquisizioneFlusso) {
    this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
  }

  public String getNazioneTarga() {
    return nazioneTarga;
  }

  public void setNazioneTarga(String nazioneTarga) {
    this.nazioneTarga = nazioneTarga;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("StanziamentoMessage [");
    builder.append("code=");
    builder.append(code);
    builder.append(", statoStanziamento=");
    builder.append(statoStanziamento);
    builder.append(", tipoFlusso=");
    builder.append(tipoFlusso);
    builder.append(", dataErogazioneServizio=");
    builder.append(dataErogazioneServizio);
    builder.append(", dataAcquisizioneFlusso=");
    builder.append(dataAcquisizioneFlusso);
    builder.append(", nrFornitore=");
    builder.append(nrFornitore);
    builder.append(", nrCliente=");
    builder.append(nrCliente);
    builder.append(", nrArticolo=");
    builder.append(nrArticolo);
    builder.append(", country=");
    builder.append(country);
    builder.append(", year=");
    builder.append(year);
    builder.append(", vehicleLicenseId=");
    builder.append(vehicleLicenseId);
    builder.append(", nazioneTarga=");
    builder.append(nazioneTarga);
    builder.append(", vehicleEuroClass=");
    builder.append(vehicleEuroClass);
    builder.append(", prezzo=");
    builder.append(prezzo);
    builder.append(", costo=");
    builder.append(costo);
    builder.append(", quantity=");
    builder.append(quantity);
    builder.append(", nr=");
    builder.append(nr);
    builder.append(", valuta=");
    builder.append(valuta);
    builder.append(", descriptions=");
    builder.append(Arrays.toString(descriptions));
    builder.append(", generaStanziamento=");
    builder.append(generaStanziamento);
    builder.append(", conguaglio=");
    builder.append(conguaglio);
    builder.append(", codicestanziamentoProvvisorio=");
    builder.append(codicestanziamentoProvvisorio);
    builder.append(", documentoDaFornitore=");
    builder.append(documentoDaFornitore);
    builder.append("]");
    return builder.toString();
  }

}
