package it.fai.ms.consumi.service.async;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.AsyncJobResponse;
import it.fai.ms.consumi.domain.AsyncJobStatusEnum;
import it.fai.ms.consumi.repository.AsyncJobResponseRepository;

@Service
@Transactional
public class ReportAsyncService {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  AsyncJobResponseRepository jobRepo;

  public ReportAsyncService(AsyncJobResponseRepository jobRepo) {
    this.jobRepo = jobRepo;
  }

  public AsyncJobResponse executeAsync(Supplier<String> action) {

    AsyncJobResponse jobResponse = new AsyncJobResponse();
    jobResponse.setStatus(AsyncJobStatusEnum.PENDING);
    AsyncJobResponse response = jobRepo.save(jobResponse);
    long jobKey = response.getId();
    CompletableFuture.supplyAsync(() -> {
      response.setResultData(action.get());
      response.setStatus(AsyncJobStatusEnum.SUCCESS);
      jobRepo.save(response);
      return response;
    })
                     .exceptionally(e -> {
                       log.error("Exception in Report Resource method processAsyncReport: {}", e);
                       response.setId(jobKey);
                       response.setStatus(AsyncJobStatusEnum.ERROR);
                       response.setResultData(truncateString(e));
                       jobRepo.save(response);
                       return response;
                     });
    return jobRepo.findById(jobKey)
                  .get();

  }

  private String truncateString(Throwable e) {

    String string = "" + e.getMessage();
    if (string.length() > 254)
      string = string.substring(0, 254);
    return string;
  }

  public AsyncJobResponse getAsyncJobById(Long jobKey) {
    return jobRepo.findById(jobKey)
                  .get();
  }

  public Long generateKeyJob() {
    AsyncJobResponse jobResponse = new AsyncJobResponse();
    jobResponse.setStatus(AsyncJobStatusEnum.PENDING);
    AsyncJobResponse response = jobRepo.save(jobResponse);
    return response.getId();
  }

  public void saveResponse(Long keyJob, String fileName) {
    Optional<AsyncJobResponse> optResponse = jobRepo.findById(keyJob);
    if (optResponse.isPresent()) {
      AsyncJobResponse asyncJobResponse = optResponse.get();
      asyncJobResponse.setStatus(AsyncJobStatusEnum.SUCCESS);
      if (StringUtils.isBlank(fileName)) {
        log.error("Not found file from document microservice");
        asyncJobResponse.setStatus(AsyncJobStatusEnum.ERROR);
      }
      asyncJobResponse.setResultData(fileName);
      jobRepo.save(asyncJobResponse);
    } else {
      String error = "Not found asyncJobResponse by ID: " + keyJob;
      log.error(error);
      throw new RuntimeException(error);
    }
  }

}
