package it.fai.ms.consumi.repository.dettaglio_stanziamento;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "dettaglio_stanziamento")
@DiscriminatorColumn(name = "tipo_dettaglio_stanziamento")
public abstract class DettaglioStanziamentoEntity {

  @Column(name = "global_identifier", nullable=false)
  private String globalIdentifier;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  @Type(type = "uuid-char")
  private UUID id;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "dettagliostanziamento_stanziamento")
  private Set<StanziamentoEntity> stanziamenti = new HashSet<>();

  public DettaglioStanziamentoEntity(@NotNull final String _globalIdentifier) {
    globalIdentifier = _globalIdentifier;
  }
  
  public DettaglioStanziamentoEntity(@NotNull final String _globalIdentifier, Optional<UUID> id) {
    this( _globalIdentifier);
    if (id.isPresent()) {
      this.id = id.get();
    }
  }

  protected DettaglioStanziamentoEntity() {
  }

  public abstract DettaglioStanziamentoEntity changeInvoiceTypeToD();

  public abstract DettaglioStanziamentoEntity negateAmount();

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final DettaglioStanziamentoEntity obj = getClass().cast(_obj);
      res = Objects.equals(obj.id, id) && Objects.equals(obj.globalIdentifier, globalIdentifier);
    }
    return res;
  }

  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public Set<StanziamentoEntity> getStanziamenti() {
    return stanziamenti;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, globalIdentifier);
  }

  protected DettaglioStanziamentoEntity clearId() {
    id = null;
    return this;
  }

  public UUID getId() {
    return id;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("id=")
                              .append(id)
                              .append(",")
                              .append(stanziamenti)
                              .append("]")
                              .toString();
  }

}
