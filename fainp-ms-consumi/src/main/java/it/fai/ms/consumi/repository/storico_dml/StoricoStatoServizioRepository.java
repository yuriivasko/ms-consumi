package it.fai.ms.consumi.repository.storico_dml;

import java.time.Instant;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio_;

@Repository
public interface StoricoStatoServizioRepository extends JpaRepository<StoricoStatoServizio, String>, JpaSpecificationExecutor<StoricoStatoServizio> {

  Optional<StoricoStatoServizio> findFirstByDispositivoDmlUniqueIdentifierAndTipoServizioAndDataVariazioneBeforeOrderByDataVariazioneDesc(String deviceUuid,
                                                                                                                                          String serviceType,
                                                                                                                                          Instant date);

  public static Specification<StoricoStatoServizio> inInterval(String dispositivoDmlUniqueId, String tipoServizio, Instant dataInizio,
                                                               Instant dataFine) {
    return (root, query, cb) -> {

      Predicate predicate = cb.and(cb.equal(root.get(StoricoStatoServizio_.dispositivoDmlUniqueIdentifier), dispositivoDmlUniqueId),
                                   cb.equal(root.get(StoricoStatoServizio_.tipoServizio), tipoServizio),
                                   cb.or(cb.isNull(root.get(StoricoStatoServizio_.dataFineVariazione)),
                                         cb.greaterThan(root.get(StoricoStatoServizio_.dataFineVariazione), dataInizio)));
      if (dataFine != null) {
        predicate = cb.and(predicate, cb.or(cb.isNull(root.get(StoricoStatoServizio_.dataVariazione)),
                                            cb.lessThan(root.get(StoricoStatoServizio_.dataVariazione), dataFine)));
      }
      return predicate;
    };
  }
}
