package it.fai.ms.consumi.dto;

public class AsyncJobResponseDataDto {
	private String downloadUri;
	
	
	public AsyncJobResponseDataDto(String resultData) {
		downloadUri = resultData;
	}

	public String getDownloadUri() {
		return downloadUri;
	}

	public void setDownloadUri(String downloadUri) {
		this.downloadUri = downloadUri;
	}
}
