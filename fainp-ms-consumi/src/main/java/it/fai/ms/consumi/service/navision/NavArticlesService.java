package it.fai.ms.consumi.service.navision;

import java.util.List;

import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;

public interface NavArticlesService {

  List<Article> getAllArticlesAndUpdateStanziamentiParams(AllStanziamentiParams stanziamentiParams);

}
