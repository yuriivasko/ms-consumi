package it.fai.ms.consumi.domain.consumi.generici.dartford;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.generici.ConsumoGenerico;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

public class Dartford
    extends ConsumoGenerico implements VehicleAssociatedWithDeviceSupplier{

  private Customer customer;
  private Device device;

  public Dartford(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public InvoiceType getInvoiceType() {
    return InvoiceType.D;
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    return new GlobalIdentifier(GlobalIdentifierType.INTERNAL) {
      @Override
      public String getId() {
        return "::globalIdentifierTBD::";
      }

      @Override
      public GlobalIdentifier newGlobalIdentifier() {
        throw new UnsupportedOperationException("not implemented");
      }
    };
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Dartford obj = getClass().cast(_obj);
      res = super.equals(obj);
    }
    return res;
  }


  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode());
  }

  @Override
  public Instant getDate() {
    return this.getEndDate();
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  @Override
  public Device getDevice() {
    return device;
  }

  public void setDevice(Device device) {
    this.device = device;
  }
}
