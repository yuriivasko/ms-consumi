package it.fai.ms.consumi.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.Optional;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;

public class Amount {

  private static CurrencyUnit defaultCurrencyUnit() {
    return Monetary.getCurrency("EUR");
  }

  private MonetaryAmount amountExcludedVat;
  private MonetaryAmount amountIncludedVat;
  private BigDecimal     exchangeRate;
  private Optional<BigDecimal> vatRate;

  public Amount() {
  }

  public Amount clone() {
    Amount a = new Amount();
    a.setVatRate(this.getVatRate());
    a.setAmountExcludedVat(this.getAmountExcludedVat());
    a.setExchangeRate(this.getExchangeRate());
    return a;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Amount obj = getClass().cast(_obj);
      res = Objects.equals(obj.amountExcludedVat, amountExcludedVat) && Objects.equals(obj.amountIncludedVat, amountIncludedVat)
            && Objects.equals(obj.exchangeRate, exchangeRate) && Objects.equals(obj.vatRate, vatRate);
    }
    return res;
  }

  public MonetaryAmount getAmountExcludedVat() {
    return amountExcludedVat;
  }

  public BigDecimal getAmountExcludedVatBigDecimal() {
    BigDecimal bigDecimal = null;
    if (getAmountExcludedVat() != null && getAmountExcludedVat().getNumber() != null) {
      bigDecimal = getAmountExcludedVat().getNumber()
                                         .numberValueExact(BigDecimal.class)
                                         .setScale(5, RoundingMode.HALF_UP);
    }
    return bigDecimal;
  }

  public MonetaryAmount getAmountIncludedVat() {
    return amountIncludedVat;
  }

  public BigDecimal getAmountIncludedVatBigDecimal() {
    BigDecimal bigDecimal = null;
    if (getAmountIncludedVat() != null && getAmountIncludedVat().getNumber() != null) {
      bigDecimal = getAmountIncludedVat().getNumber()
                                         .numberValueExact(BigDecimal.class)
                                         .setScale(5, RoundingMode.HALF_UP);
    }
    return bigDecimal;
  }

  public CurrencyUnit getCurrency() {
    if (amountExcludedVat == null && amountIncludedVat == null) {
      throw new IllegalStateException("Both amounts, included and excluded vat, are null");
    }
    if (hasExcludedAmount() && hasIncludedAmount()) {
      if (!amountExcludedVat.getCurrency()
                            .equals(amountIncludedVat.getCurrency())) {
        throw new IllegalStateException("Amounts currency, included and excluded vat, are different");
      }
    }
    final var currencyUnit = hasExcludedAmount() ? amountExcludedVat.getCurrency() : amountIncludedVat.getCurrency();
    return currencyUnit != null ? currencyUnit : defaultCurrencyUnit();
  }

  public BigDecimal getExchangeRate() {
    return exchangeRate;
  }


  public BigDecimal getVatRateBigDecimal() {
    return _getVatRateBigDecimal();
  }
  public Optional<BigDecimal> getVatRate(){
    return vatRate;
  }
  public boolean hasCurrency(final CurrencyUnit _currency) {

    Objects.requireNonNull(_currency, "Currency is mandatory");

    if (amountExcludedVat == null && amountIncludedVat == null) {
      throw new IllegalStateException("Both amounts, included and excluded vat, are null");
    }

    final var currency = hasExcludedAmount() ? amountExcludedVat.getCurrency() : amountIncludedVat.getCurrency();

    return currency.equals(_currency);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amountExcludedVat, amountIncludedVat, exchangeRate, vatRate);
  }

  public boolean isPositiveOrZero(){
    Boolean excludedIsPositive = null;
    Boolean includedIsPositive = null;
    if (hasExcludedAmount()) {
      excludedIsPositive = amountExcludedVat.isPositiveOrZero();
    }
    if (hasIncludedAmount()) {
      includedIsPositive = amountIncludedVat.isPositiveOrZero();
    }
    if (excludedIsPositive != null && includedIsPositive != null && !excludedIsPositive.equals(includedIsPositive)){
      throw new IllegalStateException("Cannot evaluate isPositiveOrZero! amountExcludedVat and amountIncludedVat have different signs!!!");
    }
    return Optional.ofNullable(excludedIsPositive)
                   .orElse(
                     Optional.ofNullable(includedIsPositive)
                             .orElseThrow(() -> new IllegalStateException("Cannot evaluate isPositiveOrZero! amountExcludedVat and amountIncludedVat are both null!!!"))
                   );
  }

  public void negate() {
    if (hasExcludedAmount()) {
      amountExcludedVat = amountExcludedVat.negate();
    }
    if (hasIncludedAmount()) {
      amountIncludedVat = amountIncludedVat.negate();
    }
  }

  public boolean hasIncludedAmount() {
    return amountIncludedVat != null;
  }

  public boolean hasExcludedAmount() {
    return amountExcludedVat != null;
  }

  public void resetAmounts() {
    amountExcludedVat = null;
    amountIncludedVat = null;
  }

  private BigDecimal _getVatRateBigDecimal(){
    if(vatRate==null || !vatRate.isPresent()){
      return BigDecimal.ZERO;
    }else{
      return vatRate.get();
    }
  }

  public void setAmountExcludedVat(MonetaryAmount _amountExcludedVat) {
    if (hasNotZeroIncludedAmount()) {
      if (_amountExcludedVat == null || _amountExcludedVat.isZero()) {
        // ignoro l'ingresso e ricalcolo dall'included
        amountExcludedVat = MonetaryUtils.amountNoVatFromAmountWithVat(amountIncludedVat, getVatRateBigDecimal());
        amountExcludedVat = Money.of(amountExcludedVat.getNumber()
                                                      .numberValue(BigDecimal.class)
                                                      .setScale(5, RoundingMode.HALF_UP),
                                     amountExcludedVat.getCurrency());
      } else {
        // TODO dovrei controllare che l'excluded in ingresso e l'included già presente siano coerenti.. per ora
        // sovrascrivo
        if (_amountExcludedVat.getNumber().getScale() != 5){
          _amountExcludedVat = Money.of(amountExcludedVat.getNumber()
                            .numberValue(BigDecimal.class)
                            .setScale(5, RoundingMode.HALF_UP),
                            _amountExcludedVat.getCurrency());
        }
        amountExcludedVat = _amountExcludedVat;
      }
    } else {
      // assegno e sovrascrivo
      if (_amountExcludedVat != null) {
        amountExcludedVat = Money.of(_amountExcludedVat.getNumber()
                                                       .numberValue(BigDecimal.class)
                                                       .setScale(5, RoundingMode.HALF_UP),
                                     _amountExcludedVat.getCurrency());
        amountIncludedVat = MonetaryUtils.amountWithVatFromAmountNoVat(amountExcludedVat, getVatRateBigDecimal());
        amountIncludedVat = Money.of(amountIncludedVat.getNumber()
                                                      .numberValue(BigDecimal.class)
                                                      .setScale(5, RoundingMode.HALF_UP),
                                     amountIncludedVat.getCurrency());
      } else {
        amountExcludedVat = null;
      }
    }
  }

  public boolean hasNotZeroIncludedAmount() {
    return hasIncludedAmount() && !amountIncludedVat.isZero();
  }

  public void setAmountIncludedVat(MonetaryAmount _amountIncludedVat) {
    if (hasNotZeroExcludedAmount()) {
      if (_amountIncludedVat == null || _amountIncludedVat.isZero()) {
        // ignoro l'ingresso e ricalcolo dall'excluded
        amountIncludedVat = MonetaryUtils.amountWithVatFromAmountNoVat(amountExcludedVat, getVatRateBigDecimal());
      } else {
        // TODO dovrei controllare che l'included in ingresso e l'excluded già presente siano coerenti..per ora
        // sovrascrivo
        if (_amountIncludedVat.getNumber().getScale() != 5){
          _amountIncludedVat = Money.of(_amountIncludedVat.getNumber()
                            .numberValue(BigDecimal.class)
                            .setScale(5, RoundingMode.HALF_UP),
                            _amountIncludedVat.getCurrency());
        }
        amountIncludedVat = _amountIncludedVat;
      }
    } else {
      // assegno e sovrascrivo
      if (_amountIncludedVat != null) {
        amountIncludedVat = Money.of(_amountIncludedVat.getNumber()
                                                       .numberValue(BigDecimal.class)
                                                       .setScale(5, RoundingMode.HALF_UP),
                                     _amountIncludedVat.getCurrency());
        amountExcludedVat = MonetaryUtils.amountNoVatFromAmountWithVat(amountIncludedVat, getVatRateBigDecimal());
        amountExcludedVat = Money.of(amountExcludedVat.getNumber()
                                                      .numberValue(BigDecimal.class)
                                                      .setScale(5, RoundingMode.HALF_UP),
                                     amountExcludedVat.getCurrency());
      } else {
        amountIncludedVat = null;
      }
    }
  }

  public boolean hasNotZeroExcludedAmount() {
    return hasExcludedAmount() && !amountExcludedVat.isZero();
  }

  public void setExchangeRate(final BigDecimal _exchangeRate) {
    if (exchangeRate != null) {
      exchangeRate = _exchangeRate.setScale(5, RoundingMode.HALF_UP);
    } else {
      exchangeRate = null;
    }
  }

  public void setVatRate(final Optional<BigDecimal> vatRate){
      this.vatRate = vatRate;
  }
  public void setVatRate(final BigDecimal _vatRate) {
      vatRate = _vatRate!=null?Optional.of(_vatRate.setScale(2, RoundingMode.HALF_UP)):Optional.empty();
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("amountIncVat=")
                              .append(amountIncludedVat)
                              .append(",amountExcVat=")
                              .append(amountExcludedVat)
                              .append(",vatRate=")
                              .append(vatRate)
                              .append(",excRate=")
                              .append(exchangeRate)
                              .append("]")
                              .toString();
  }

  public BigDecimal total() {
    return amountIncludedVat.getNumber()
                            .numberValue(BigDecimal.class);
  }

}
