package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric;

@Entity
@Table(name = "dettaglio_stanziamento_treni")
@DiscriminatorValue(value = "TRENI")
public class DettaglioStanziamentoTrenoEntity extends DettaglioStanziamentoEntityGeneric {

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<AltriImportiTreniEntity> altriImportiTreni = new HashSet<>();

  public DettaglioStanziamentoTrenoEntity(final String _globalIdentifier) {
    super(_globalIdentifier);
  }

  protected DettaglioStanziamentoTrenoEntity() {
  }

  @Override
  public DettaglioStanziamentoEntity changeInvoiceTypeToD() {
    throw new UnsupportedOperationException();
  }

  public Set<AltriImportiTreniEntity> getAltriImportiTreni() {
    return altriImportiTreni;
  }

  @Override
  public DettaglioStanziamentoEntity negateAmount() {
    throw new UnsupportedOperationException();
  }

  public void setAltriImportiTreni(Set<AltriImportiTreniEntity> altriImportiTreni) {
    this.altriImportiTreni = altriImportiTreni;
  }

}
