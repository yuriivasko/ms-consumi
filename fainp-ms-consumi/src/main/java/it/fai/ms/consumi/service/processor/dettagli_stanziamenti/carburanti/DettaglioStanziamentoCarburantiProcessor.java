package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti;

import java.util.List;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

public interface DettaglioStanziamentoCarburantiProcessor {

  List<Stanziamento> produce(DettaglioStanziamentoCarburante dettaglioStanziamentoCarburante, StanziamentiParams stanziamentiParams);

}
