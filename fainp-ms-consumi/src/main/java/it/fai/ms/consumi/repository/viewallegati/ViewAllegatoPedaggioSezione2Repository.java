package it.fai.ms.consumi.repository.viewallegati;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import it.fai.common.enumeration.TipoDispositivoEnum;

public interface ViewAllegatoPedaggioSezione2Repository {

  Set<ViewAllegatoPedaggioSezione2Entity> findDettagliBy(TipoDispositivoEnum deviceType, String licensePlate, String country,
                                                         String panNumber, String euroClass);

  List<ViewAllegatoPedaggioSezione2Entity> findByDettaglioStanziamentoId(List<UUID> dettagliStanziamentoId);

}
