package it.fai.ms.consumi.repository.flussi.record.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class TimeUtils {

  public static final String DATE_TIME_FORMAT     = "yyyyMMddHHmmss";
  public static final String DATE_FORMAT          = "yyyyMMdd";
  public static final String DATE_FORMAT_SMALL    = "yyyy-MM-dd";
  public static final String DATE_FORMAT_SMALL1   = "yyMMdd";
  public static final String DATE_FORMAT_IT       = "dd/MM/yyyy";
  public static final String DATE_FORMAT_IT_SMALL = "dd/MM/yy";
  public static final String TIME_FORMAT          = "HHmmss";
  public static final String TIME_FORMAT_SMALL    = "HH:mm";
  public static final String TIME_FORMAT_SMALL1   = "HHmm";

  public final static String JSON_TIMESTAMP = "yyyy-MM-dd HH:mm:ss";

  private static Calendar calendar = Calendar.getInstance();

  public TimeUtils() {
    super();
  }

  public static Date addDays(Date date, int amount) {
    return DateUtils.addDays(date, amount);
  }

  public static Date addMinutes(Date date, int amount) {
    return DateUtils.addMinutes(date, amount);
  }

  public static Date addSeconds(Date date, int amount) {
    return DateUtils.addSeconds(date, amount);
  }

  public static Long currentTimeMillis() {
    return System.currentTimeMillis();
  }

  public static Date fromString(String source, String pattern) throws ParseException {
    if (source != null && source.length() > 0) {
      SimpleDateFormat sdf = new SimpleDateFormat(pattern);
      return sdf.parse(source);
    }
    return null;
  }

  public static String toString(Date date, String format) {
    SimpleDateFormat formatter = new SimpleDateFormat(format);
    return formatter.format(date);
  }

  public static String toString(String format) {
    return toString(new Date(), format);
  }

  public static Integer getYear(Date value) {
    if (value != null) {
      calendar.setTime(value);
      return calendar.get(Calendar.YEAR);
    } else {
      return 0;
    }
  }

  public static Date Now() {
    return new Date();
  }

  public static Double SecondsDiffTime(Date start, Date end) {
    return ((end.getTime() - start.getTime()) / 1000) * 1.0;
  }

}
