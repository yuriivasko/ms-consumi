package it.fai.ms.consumi.repository.viewallegati;

import java.time.Instant;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "allegati_fatture_carburanti_sezione2")
@Immutable
public class ViewAllegatoCarburantiSezione2Entity {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "codice_contratto")
  private String codiceContratto;

  @Column(name = "tipo_dispositivo")
  private String tipoDispositivo;

  @Column(name = "serial_number")
  private String serialNumber;

  @Column(name = "targa")
  private String targaVeicolo;

  @Column(name = "nazione_targa")
  private String nazioneVeicolo;

  @Column(name = "classificazione_euro")
  private String classificazioneEuro;

  @Column(name = "data_ora_utilizzo")
  private Instant dataOraUtilizzo;

  @Column(name = "unita_misura")
  private String unitaMisura;

  @Column(name = "transaction_detail_code")
  private String transactionDetailCode;

  @Column(name = "sign_of_transaction")
  private String signTransaction;

  @Column(name = "quantita")
  private Double quantita;

  @Column(name = "importo")
  private Double importo;

  @Column(name = "valuta")
  private String valuta;

  @Column(name = "cambio")
  private Double cambio;

  @Column(name = "km")
  private Double km;

  @Column(name = "erogatore")
  private String erogatore;

  @Column(name = "punto_erogazione")
  private String puntoErogazione;

  @Column(name = "perc_iva")
  private Double percIva;

  @Column(name = "codice_cliente_nav")
  private String codiceClienteNav;

  @Column(name = "codice_fornitore")
  private String codiceFornitore;

  @Column(name = "record_code")
  private String recordCode;
  
  @Column(name = "tipo_consumo")
  private String tipoConsumo;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getNazioneVeicolo() {
    return nazioneVeicolo;
  }

  public void setNazioneVeicolo(String nazioneVeicolo) {
    this.nazioneVeicolo = nazioneVeicolo;
  }

  public String getTargaVeicolo() {
    return targaVeicolo;
  }

  public void setTargaVeicolo(String targaVeicolo) {
    this.targaVeicolo = targaVeicolo;
  }

  public String getClassificazioneEuro() {
    return classificazioneEuro;
  }

  public void setClassificazioneEuro(String classificazioneEuro) {
    this.classificazioneEuro = classificazioneEuro;
  }

  public Instant getDataOraUtilizzo() {
    return dataOraUtilizzo;
  }

  public void setDataOraUtilizzo(Instant dataOraUtilizzo) {
    this.dataOraUtilizzo = dataOraUtilizzo;
  }

  public String getUnitaMisura() {
    return unitaMisura;
  }

  public void setUnitaMisura(String unitaMisura) {
    this.unitaMisura = unitaMisura;
  }

  public String getTransactionDetailCode() {
    return transactionDetailCode;
  }

  public void setTransactionDetailCode(String transactionDetailCode) {
    this.transactionDetailCode = transactionDetailCode;
  }

  public String getSignTransaction() {
    return signTransaction;
  }

  public void setSignTransaction(String signTransaction) {
    this.signTransaction = signTransaction;
  }

  public Double getQuantita() {
    return quantita;
  }

  public void setQuantita(Double quantita) {
    this.quantita = quantita;
  }

  public Double getImporto() {
    return importo;
  }

  public void setImporto(Double importo) {
    this.importo = importo;
  }

  public String getValuta() {
    return valuta;
  }

  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

  public Double getCambio() {
    return cambio;
  }

  public void setCambio(Double cambio) {
    this.cambio = cambio;
  }

  public Double getKm() {
    return km;
  }

  public void setKm(Double km) {
    this.km = km;
  }

  public String getErogatore() {
    return erogatore;
  }

  public void setErogatore(String erogatore) {
    this.erogatore = erogatore;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

  public Double getPercIva() {
    return percIva;
  }

  public void setPercIva(Double percIva) {
    this.percIva = percIva;
  }

  public String getCodiceClienteNav() {
    return codiceClienteNav;
  }

  public void setCodiceClienteNav(String codiceClienteNav) {
    this.codiceClienteNav = codiceClienteNav;
  }

  public String getCodiceFornitore() {
    return codiceFornitore;
  }

  public void setCodiceFornitore(String codiceFornitore) {
    this.codiceFornitore = codiceFornitore;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }
  
  
  
  public String getTipoConsumo() {
    return tipoConsumo;
  }

  public void setTipoConsumo(String tipoConsumo) {
    this.tipoConsumo = tipoConsumo;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ViewAllegatoCarburantiSezione2Entity [id=");
    builder.append(id);
    builder.append(", codiceContratto=");
    builder.append(codiceContratto);
    builder.append(", tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", serialNumber=");
    builder.append(serialNumber);
    builder.append(", targaVeicolo=");
    builder.append(targaVeicolo);
    builder.append(", nazioneVeicolo=");
    builder.append(nazioneVeicolo);
    builder.append(", classificazioneEuro=");
    builder.append(classificazioneEuro);
    builder.append(", dataOraUtilizzo=");
    builder.append(dataOraUtilizzo);
    builder.append(", unitaMisura=");
    builder.append(unitaMisura);
    builder.append(", transactionDetailCode=");
    builder.append(transactionDetailCode);
    builder.append(", signTransaction=");
    builder.append(signTransaction);
    builder.append(", quantita=");
    builder.append(quantita);
    builder.append(", importo=");
    builder.append(importo);
    builder.append(", valuta=");
    builder.append(valuta);
    builder.append(", cambio=");
    builder.append(cambio);
    builder.append(", km=");
    builder.append(km);
    builder.append(", erogatore=");
    builder.append(erogatore);
    builder.append(", puntoErogazione=");
    builder.append(puntoErogazione);
    builder.append(", percIva=");
    builder.append(percIva);
    builder.append(", codiceClienteNav=");
    builder.append(codiceClienteNav);
    builder.append(", codiceFornitore=");
    builder.append(codiceFornitore);
    builder.append(", recordCode=");
    builder.append(recordCode);
    builder.append(", tipoConsumo=");
    builder.append(tipoConsumo);
    builder.append("]");
    return builder.toString();
  }

}
