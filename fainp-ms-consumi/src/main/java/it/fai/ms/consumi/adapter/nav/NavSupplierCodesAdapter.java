package it.fai.ms.consumi.adapter.nav;

import java.util.Set;

public interface NavSupplierCodesAdapter {

  Set<String> getAllCodes();

}
