package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.util.ComparatorFactoryService;
import it.fai.ms.consumi.service.util.IteratorToListConverter;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

/*
 * Pedaggi Italia Telepass Provvisorio (elvia sono i definitivi)
 */
@Service(ElcitJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class ElcitJob extends AbstractJob<ElcitRecord, Elcit> {

  public final static String QUALIFIER = "elcitJob";

  private final String fileOrdering;

  public ElcitJob(final PlatformTransactionManager transactionManager,
                  final StanziamentiParamsValidator stanziamentiParamsValidator,
                  final NotificationService notificationService,
                  final RecordPersistenceService persistenceService,
                  final RecordDescriptor<ElcitRecord> recordDescriptor,
                  final ConsumoProcessor<Elcit> consumoProcessor,
                  final RecordConsumoMapper<ElcitRecord, Elcit> recordConsumoMapper,
                  final StanziamentiToNavPublisher stanziamentiToNavJmsProducer,
                  final @Value("${application.fileOrdering.elcit}") String fileOrdering) {

    super("ELCIT", transactionManager, stanziamentiParamsValidator, notificationService, persistenceService,
          recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
    this.fileOrdering = fileOrdering;
  }

  @Override
  public Format getJobQualifier() {
    return Format.ELCIT;
  }

  @Override
  public List<Path> orderFileList(DirectoryStream<Path> directoryStream) {
    final Comparator<Path> comparator = ComparatorFactoryService.getPathComparatorByLabel(fileOrdering);
    return new IteratorToListConverter<>(directoryStream.iterator(), comparator).toOrderedList();
  }


}
