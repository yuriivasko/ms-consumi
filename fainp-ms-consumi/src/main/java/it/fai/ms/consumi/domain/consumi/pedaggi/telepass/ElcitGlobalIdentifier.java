package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import java.util.UUID;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class ElcitGlobalIdentifier extends GlobalIdentifier {

  public ElcitGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.EXTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    return new ElcitGlobalIdentifier(UUID.randomUUID()
                                         .toString());
  }

}
