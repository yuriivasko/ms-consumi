package it.fai.ms.consumi.repository.viewallegati;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface ViewAllegatiFattureRepository {

  Set<ViewAllegatiFattureEntity> findByCodiceStanziamentoIn(List<String> codiciStanziamenti);

  Map<String, ViewAllegatiFattureEntity> findByCodiceStanziamentoInAndRangeDate(List<String> codiciStanziamenti, Instant dateFrom, Instant dateTo);

  BigDecimal findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(List<String> codiciStanziamento);

}
