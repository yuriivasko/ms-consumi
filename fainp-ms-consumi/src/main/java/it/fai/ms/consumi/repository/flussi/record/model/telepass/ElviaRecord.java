package it.fai.ms.consumi.repository.flussi.record.model.telepass;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.TimeUtils;
import org.apache.commons.lang.StringUtils;

import javax.money.MonetaryAmount;
import java.math.RoundingMode;
import java.text.ParseException;

public class ElviaRecord
extends CommonRecord
implements GlobalIdentifierBySupplier {

  private final static long serialVersionUID = 8429849328430432831L;

  private String partnerCode             = "";
  private String subContractCode         = "";
  private String typeMovement            = "";
  private String dateMovement            = "";
  private String hourMovement            = "";
  private String originalAmount          = "";
  private String amountNotSubjectedToVat = "";
  private String discount                = "";
  private String typeSupport             = "";
  private String supportDescription      = "";
  private String supportCode             = "";
  private String movementDescription     = "";
  private String tollClass               = "";
  private String dateOfDebit             = "";
  private String signOfTheAmount         = "";
  private String networkCode             = "";
  private String entryGateCode           = "";
  private String exitGateCode            = "";
  private String dateOfInvoice           = "";
  private String vatDescription          = "";
  private String vatRate                 = "";
  private String routeID                 = "";
  private String vatCode                 = "";

    private MonetaryAmount total_amount_value          = null;
    private MonetaryAmount discount_value              = null;
    private MonetaryAmount amount_not_subjected_to_vat = null;
  private String rawSupportCode;

  @JsonCreator
  public ElviaRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }

  public String getRouteID() {
    return routeID;
  }

  public void setRouteID(String routeID) {
    this.routeID = routeID;
  }

  public MonetaryAmount getTotal_amount_value() {
    return total_amount_value;
  }

  public String getVatCode() {
    return vatCode;
  }

  public void setVatCode(String vatCode) {
    this.vatCode = vatCode;
  }

  public String getVatRate() {
    return vatRate;
  }

  public void setVatRate(String vatRate) {
    this.vatRate = vatRate;
    this.vatRateBigDecimal = NumericUtils.strToBigDecimal(vatRate, 100).divide(NumericUtils.ONE_HUNDRED,2,RoundingMode.HALF_UP);
  }

  public String getPartnerCode() {
    return partnerCode;
  }

  public void setPartnerCode(String partnerCode) {
    this.partnerCode = partnerCode;
  }

  public String getSubContractCode() {
    return subContractCode;
  }

  public void setSubContractCode(String subContractCode) {
    this.subContractCode = subContractCode;
  }

  public String getTypeMovement() {
    return typeMovement;
  }

  public void setTypeMovement(String typeMovement) {
    this.typeMovement = typeMovement;
  }

  public String getDateMovement() {
    return dateMovement;
  }

  public void setDateMovement(String dateMovement) {
    this.dateMovement = dateMovement;
    try {
      this.rtdtm = TimeUtils.fromString(dateMovement, TimeUtils.DATE_FORMAT_SMALL);
      this.anno = TimeUtils.getYear(this.rtdtm);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getHourMovement() {
    return hourMovement;
  }

  public void setHourMovement(String hourMovement) {
    this.hourMovement = hourMovement;
    try {
      this.rttime = TimeUtils.fromString(hourMovement, "HH.mm.ss");
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getOriginalAmount() {
    return originalAmount;
  }

  public void setOriginalAmount(String originalAmount) {
    this.originalAmount = originalAmount;
    this.total_amount_value = MonetaryUtils.strToMonetary(originalAmount, -2, "EUR");
  }

  public String getAmountNotSubjectedToVat() {
    return amountNotSubjectedToVat;
  }

  public MonetaryAmount getAmount_not_subjected_to_vat() {
    return amount_not_subjected_to_vat;
  }

  public void setAmountNotSubjectedToVat(String amountNotSubjectedToVat) {
    this.amountNotSubjectedToVat = amountNotSubjectedToVat;
    this.amount_not_subjected_to_vat = MonetaryUtils.strToMonetary(amountNotSubjectedToVat, -2, "EUR");
  }

  public String getDiscount() {
    return discount;
  }

  public void setDiscount(String discount) {
    this.discount = discount;
    this.discount_value = MonetaryUtils.strToMonetary(discount, -2, "EUR");
  }

  public String getTypeSupport() {
    return typeSupport;
  }

  public void setTypeSupport(String typeSupport) {
    this.typeSupport = typeSupport;
  }

  public String getSupportDescription() {
    return supportDescription;
  }

  public void setSupportDescription(String supportDescription) {
    this.supportDescription = supportDescription;
  }

  public String getRawSupportCode() {
    return rawSupportCode;
  }

  public String getSupportCode() {
    return supportCode;
  }

  public void setSupportCode(String supportCode) {
    this.rawSupportCode = supportCode;
    if (supportCode!=null && !supportCode.trim().isEmpty()) {
      this.supportCode = StringUtils.leftPad(supportCode.trim().replaceAll("^0+", ""),11,"0");
    }else
      this.supportCode = null;
  }

  public String getMovementDescription() {
    return movementDescription;
  }

  public void setMovementDescription(String movementDescription) {
    this.movementDescription = movementDescription;
  }

  public String getTollClass() {
    return tollClass;
  }

  public void setTollClass(String tollClass) {
    this.tollClass = tollClass;
  }

  public String getDateOfDebit() {
    return dateOfDebit;
  }

  public void setDateOfDebit(String dateOfDebit) {
    this.dateOfDebit = dateOfDebit;
  }

  public String getSignOfTheAmount() {
    return signOfTheAmount;
  }

  public void setSignOfTheAmount(String signOfTheAmount) {
    this.signOfTheAmount = signOfTheAmount;
  }

  public String getNetworkCode() {
    return networkCode;
  }

  public void setNetworkCode(String networkCode) {
    this.networkCode = networkCode;
  }

  public String getEntryGateCode() {
    return entryGateCode;
  }

  public void setEntryGateCode(String entryGateCode) {
    this.entryGateCode = entryGateCode;
  }

  public String getExitGateCode() {
    return exitGateCode;
  }

  public void setExitGateCode(String exitGateCode) {
    this.exitGateCode = exitGateCode;
  }

  public String getDateOfInvoice() {
    return dateOfInvoice;
  }

  public void setDateOfInvoice(String dateOfInvoice) {
    this.dateOfInvoice = dateOfInvoice;
  }

  public String getVatDescription() {
    return vatDescription;
  }

  public void setVatDescription(String vatDescription) {
    this.vatDescription = vatDescription;
  }

  public MonetaryAmount getDiscount_value() {
    return discount_value;
  }

  public void setDiscount_value(MonetaryAmount discount_value) {
    this.discount_value = discount_value;
  }

  @Override
  public String getGlobalIdentifier() {
    return String.join("§", getSubContractCode(), getSupportCode(), getTypeSupport(), getDateMovement(), getHourMovement(),getTypeMovement(),
                       getNetworkCode(), getExitGateCode(), getSignOfTheAmount(), getVatRate()).replaceAll("null", "");
  }

  public String getGlobalIdentifierAsEse() {
    return String.join("§", getSubContractCode(), getSupportCode(), getTypeSupport(), getDateMovement(), getHourMovement(),getTypeMovement(),
                       getNetworkCode(), getExitGateCode(), getSignOfTheAmount(), getVatRate() != null ? "0000" : "").replaceAll("null", "");
  }

}
