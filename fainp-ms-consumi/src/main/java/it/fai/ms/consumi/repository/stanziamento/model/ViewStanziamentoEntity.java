package it.fai.ms.consumi.repository.stanziamento.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.ViewDettaglioStanziamentoEntity;

@Entity
@Table(name = "view_all_stanziamento")
@Immutable
public class ViewStanziamentoEntity extends MappedStanziamentoEntity{

  @ManyToMany(mappedBy = "stanziamenti")
  private final Set<ViewDettaglioStanziamentoEntity> dettaglioStanziamenti = new HashSet<>();

  @SuppressWarnings("unchecked")
  @Override
  public Set<ViewDettaglioStanziamentoEntity> getDettaglioStanziamenti() {
    return dettaglioStanziamenti;
  }
}
