package it.fai.ms.consumi.repository.storico_dml;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.consumi.repository.storico_dml.model.DmlEmbeddedId;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

@Repository
public interface StoricoVeicoloRepository extends JpaRepository<StoricoVeicolo, DmlEmbeddedId>,InterfaceDmlRepository<StoricoVeicolo>  {

  @Query("select distinct v from StoricoVeicolo v, StoricoAssociazioneDispositivoVeicolo a, StoricoDispositivo d, StoricoContratto c " 
      + " where v.dmlUniqueIdentifier = a.veicoloDmlUniqueIdentifier"
      + " and a.dmlUniqueIdentifier = d.dmlUniqueIdentifier"
      + " and d.contrattoDmlUniqueIdentifier = c.contrattoNumero"
      + " and c.codiceAzienda = :code")
  List<StoricoVeicolo> findAllByCustomerCode(@Param("code") String code);

}
