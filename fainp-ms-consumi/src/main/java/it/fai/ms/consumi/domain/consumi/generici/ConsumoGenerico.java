package it.fai.ms.consumi.domain.consumi.generici;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

public abstract class ConsumoGenerico
  extends Consumo {

  public ConsumoGenerico(Source _source, String _recordCode, Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  private Boolean    processed = Boolean.FALSE;
  private String     partnerCode;
  private String     deviceCode;
  private Instant    startDate;
  private Instant    endDate;
  private BigDecimal quantity;
  private Vehicle    vehicle;
  private String     licencePlateOnRecord;
  private String     route;
  private String     additionalInformation;
  private String     tipoDispositivo;
  private String     puntoErogazione;
  private String     km;

  public Boolean getProcessed() {
    return processed;
  }

  @Override
  public String getPartnerCode() {
    return partnerCode;
  }

  public String getDeviceCode() {
    return deviceCode;
  }

  public Instant getStartDate() {
    return startDate;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public Vehicle getVehicle() {
    return vehicle;
  }

  public String getRoute() {
    return route;
  }

  public String getAdditionalInformation() {
    return additionalInformation;
  }

  public void setProcessed(Boolean processed) {
    this.processed = processed;
  }

  public void setPartnerCode(String partnerCode) {
    this.partnerCode = partnerCode;
  }

  public void setDeviceCode(String deviceCode) {
    this.deviceCode = deviceCode;
  }

  public void setStartDate(Instant startDate) {
    this.startDate = startDate;
  }

  public Instant getEndDate() {
    return endDate;
  }

  public void setEndDate(Instant endDate) {
    this.endDate = endDate;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public String getLicencePlateOnRecord() {
    return licencePlateOnRecord;
  }

  public void setLicencePlateOnRecord(String licencePlateOnRecord) {
    this.licencePlateOnRecord = licencePlateOnRecord;
  }

  public void setRoute(String route) {
    this.route = route;
  }

  public void setAdditionalInformation(String additionalInformation) {
    this.additionalInformation = additionalInformation;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    ConsumoGenerico that = (ConsumoGenerico) o;
    return Objects.equals(processed, that.processed) && Objects.equals(partnerCode, that.partnerCode) && Objects.equals(deviceCode,
                                                                                                                        that.deviceCode)
           && Objects.equals(startDate, that.startDate) && Objects.equals(endDate, that.endDate) && Objects.equals(quantity, that.quantity)
           && Objects.equals(vehicle, that.vehicle) && Objects.equals(licencePlateOnRecord, that.licencePlateOnRecord) && Objects.equals(
        route, that.route) && Objects.equals(additionalInformation, that.additionalInformation)
          && Objects.equals(km,that.km) && Objects.equals(puntoErogazione,that.puntoErogazione) && Objects.equals(tipoDispositivo,that.tipoDispositivo);
  }

  @Override
  public int hashCode() {

    return Objects.hash(super.hashCode(), processed, partnerCode, deviceCode, startDate, endDate, quantity, vehicle, licencePlateOnRecord,
                        route, additionalInformation,km,puntoErogazione,tipoDispositivo);
  }

  @Override
  public String toString() {
    return "ConsumoGenerico{" +
      "processed=" + processed +
      ", partnerCode='" + partnerCode + '\'' +
      ", deviceCode='" + deviceCode + '\'' +
      ", startDate=" + startDate +
      ", endDate=" + endDate +
      ", quantity=" + quantity +
      ", vehicle=" + vehicle +
      ", licencePlateOnRecord='" + licencePlateOnRecord + '\'' +
      ", route='" + route + '\'' +
      ", additionalInformation='" + additionalInformation + '\'' +
      ", tipoDispositivo='" + tipoDispositivo + '\'' +
      ", puntoErogazione='" + puntoErogazione + '\'' +
      ", km='" + km + '\'' +
      ", "+super.toString()+
      '}';
  }
}
