package it.fai.ms.consumi.service.validator;

import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

public interface RecordFileFormalValidator<T extends CommonRecord> {

  RecordsFileInfo validateFile(RecordsFileInfo recordsFileInfo);

}
