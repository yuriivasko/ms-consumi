package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.money.Monetary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.petrolpump.PriceCalculatedByPriceTableService;

@Service
public class DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl {

  private final transient Logger log = LoggerFactory.getLogger(getClass());

  private final PriceCalculatedByPriceTableService priceTableService;
  private final CurrencyRateService                currencyRateService;
  private final PriceCalculatedByNavService        priceCalculatedByNavClient;

  @Inject
  public DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl(PriceCalculatedByPriceTableService _priceTableService,
                                                                    CurrencyRateService _currencyRateService,
                                                                    PriceCalculatedByNavService _priceCalculatedByNavClient) {
    currencyRateService = _currencyRateService;
    priceTableService = _priceTableService;
    priceCalculatedByNavClient = _priceCalculatedByNavClient;
  }

  public List<DettaglioStanziamentoCarburante> process(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                                       StanziamentiParams _stanziamentiParams) {

    GlobalIdentifier globalIdentifier = _dettaglioStanziamentoCarburante.getGlobalIdentifier();
    List<PriceTableDTO> priceTableList = priceTableService.getPriceTableList(_dettaglioStanziamentoCarburante.getSupplier().getCode(),
                                                                             _stanziamentiParams.getArticle()
                                                                                                .getCode(),
                                                                             _dettaglioStanziamentoCarburante.getEntryDateTime());
    Optional<PriceTableDTO> costPrice = priceTableList.stream()
                                                      .filter(p -> p.getCostRevenueFlag() == CostRevenue.COST)
                                                      .findFirst();
    Optional<PriceTableDTO> revenuePrice = priceTableList.stream()
                                                         .filter(p -> p.getCostRevenueFlag() == CostRevenue.REVENUE)
                                                         .findFirst();

    if(_dettaglioStanziamentoCarburante.getPriceReference()==null) {
      _dettaglioStanziamentoCarburante.setPriceReference(new Amount());
    }

    // Creo 2 stanziamenti
    DettaglioStanziamentoCarburante ricavoDettaglioStanziamento = null;
    DettaglioStanziamentoCarburante costoDettaglioStanziamento = null;

    if (_stanziamentiParams.getCostoRicavo() == CostoRicavo.CR) {
      ricavoDettaglioStanziamento = new DettaglioStanziamentoCarburante(globalIdentifier);
      BeanUtils.copyProperties(_dettaglioStanziamentoCarburante, ricavoDettaglioStanziamento);
      ricavoDettaglioStanziamento.setPriceReference(_dettaglioStanziamentoCarburante.getPriceReference().clone());

      costoDettaglioStanziamento = _dettaglioStanziamentoCarburante;
      // FIXME create globalIdentifier per costo_dettaglioStanziamentoCarburante e ricavo
    } else if (_stanziamentiParams.getCostoRicavo() == CostoRicavo.C) {
      costoDettaglioStanziamento = _dettaglioStanziamentoCarburante;
    } else if (_stanziamentiParams.getCostoRicavo() == CostoRicavo.R) {
      ricavoDettaglioStanziamento = _dettaglioStanziamentoCarburante;
    }

    return process(ricavoDettaglioStanziamento, costoDettaglioStanziamento, _stanziamentiParams, costPrice, revenuePrice);
  }

  private List<DettaglioStanziamentoCarburante> process(DettaglioStanziamentoCarburante ricavoDettaglioStanziamento,
                                                        DettaglioStanziamentoCarburante costoDettaglioStanziamento,
                                                        StanziamentiParams _stanziamentiParams, Optional<PriceTableDTO> costPrice,
                                                        Optional<PriceTableDTO> revenuePrice) {

    log.info("Chiamo priceCalculatedByNavClient per calcolo prezzo / costo se necessario...");

    // Costo
    if (costoDettaglioStanziamento != null) {
      log.info("costoDettaglioStanziamento != null, chiamo priceCalculatedByNavClient.setCostCalculatedAmountByNav");
      setInvoiceType(costoDettaglioStanziamento, costPrice);
      if (!costPrice.isPresent())
        log.warn("La priceTable non dovrebbe sempre essere mai null nel caso di DA_TABELLA_PREZZI");

      costoDettaglioStanziamento.setPriceComputed(null);

      // setto il referenceAmount e il computedAmount per il costo
      setReferenceAmount(_stanziamentiParams, costPrice, costoDettaglioStanziamento.getPriceReference(),
                         costoDettaglioStanziamento.getEntryDateTime());
      priceCalculatedByNavClient.setCostCalculatedAmountByNav(costoDettaglioStanziamento, _stanziamentiParams);
    }

    // Ricavo
    if (ricavoDettaglioStanziamento != null) {
      log.info("ricavoDettaglioStanziamento != null, chiamo priceCalculatedByNavClient.setPriceCalculatedAmountByNav");
      setInvoiceType(ricavoDettaglioStanziamento, revenuePrice);
      ricavoDettaglioStanziamento.setCostComputed(null);

      // setto il referenceAmount e il computedAmount per il ricavo
      setReferenceAmount(_stanziamentiParams, revenuePrice, ricavoDettaglioStanziamento.getPriceReference(),
                         ricavoDettaglioStanziamento.getEntryDateTime());
      priceCalculatedByNavClient.setPriceCalculatedAmountByNav(ricavoDettaglioStanziamento, _stanziamentiParams);
    }

    if (costoDettaglioStanziamento != null && ricavoDettaglioStanziamento != null) {
      return Arrays.asList(costoDettaglioStanziamento, ricavoDettaglioStanziamento);
    } else if (costoDettaglioStanziamento != null) {
      return Arrays.asList(costoDettaglioStanziamento);
    } else if (ricavoDettaglioStanziamento != null) {
      return Arrays.asList(ricavoDettaglioStanziamento);
    } else {
      return java.util.Collections.emptyList();
    }
  }

  private void setInvoiceType(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante, Optional<PriceTableDTO> priceTableDTO) {
    if (isProvvisorio(priceTableDTO)) {
      _dettaglioStanziamentoCarburante.setInvoiceType(InvoiceType.P); // provvisorio
    } else {
      _dettaglioStanziamentoCarburante.setInvoiceType(InvoiceType.D); // definitivo
    }
  }

  private boolean isProvvisorio(Optional<PriceTableDTO> priceTableDTO) {
    return priceTableDTO.isPresent() && priceTableDTO.get()
                                                     .getValidUntil() == null;
  }

  private void setReferenceAmount(StanziamentiParams _stanziamentiParams, Optional<PriceTableDTO> priceTableDTO, Amount amountReference,
                                  Instant entryDateTime) {
    if (priceTableDTO.isPresent()) {
      setAmountFields(amountReference, _stanziamentiParams, priceTableDTO.get()
                                                                         .getReferencePrice());
    } else {
      setConvertedAmountFields(amountReference, _stanziamentiParams, entryDateTime);
    }
  }

  private void setConvertedAmountFields(Amount amount, StanziamentiParams _stanziamentiParams, Instant entryDateTime) {
    Currency targetCurrency = _stanziamentiParams.getArticle()
                                                 .getCurrency();

    Amount to = currencyRateService.convertAmount(amount, targetCurrency, entryDateTime);
    amount.resetAmounts();
    amount.setVatRate(BigDecimal.valueOf(_stanziamentiParams.getArticle()
                                                            .getVatRate()));
    amount.setAmountExcludedVat(to.getAmountExcludedVat());
  }

  private void setAmountFields(Amount amount, StanziamentiParams _stanziamentiParams, BigDecimal amountExcludedVat) {
    amount.resetAmounts();
    amount.setVatRate(BigDecimal.valueOf(_stanziamentiParams.getArticle()
                                                            .getVatRate()));
    amount.setAmountExcludedVat(Monetary.getDefaultAmountFactory()
                                        .setCurrency(_stanziamentiParams.getArticle()
                                                                        .getCurrency()
                                                                        .getCurrencyCode())
                                        .setNumber(amountExcludedVat)
                                        .create());
  }

}
