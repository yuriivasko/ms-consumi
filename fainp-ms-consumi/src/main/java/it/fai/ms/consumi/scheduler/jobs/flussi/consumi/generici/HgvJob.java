package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.efservice.message.consumi.AllineamentoDaConsumoMessage;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.flussi.record.model.generici.HgvRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.hgv.HgvProcessor;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.hgv.HgvRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 * Pedaggi Italia Telepass Definitivo (Elvia sono i definitivi)
 */
@Service(HgvJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class HgvJob
  extends AbstractJob<HgvRecord, Hgv> {

  public final static String QUALIFIER = "hgvJob";

  private final transient Logger                           log = LoggerFactory.getLogger(getClass());
  private final           AllineamentoDaConsumoJmsProducer allineamentiDaConsumoProducer;

  public HgvJob(final PlatformTransactionManager transactionManager,
                final StanziamentiParamsValidator stanziamentiParamsValidator,
                final NotificationService notificationService,
                final RecordPersistenceService persistenceService,
                final HgvRecordDescriptor recordDescriptor,
                final HgvProcessor consumoGenericoProcessor,
                final HgvRecordConsumoGenericoMapper recordConsumoGenericoMapper,
                final StanziamentiToNavPublisher stanziamentiToNavJmsProducer,
                final AllineamentoDaConsumoJmsProducer allineamentiDaConsumoProducer) {

    super("HGV", transactionManager, stanziamentiParamsValidator, notificationService,
          persistenceService, recordDescriptor, consumoGenericoProcessor, recordConsumoGenericoMapper, stanziamentiToNavJmsProducer);
    this.allineamentiDaConsumoProducer = allineamentiDaConsumoProducer;
  }

  @Override
  public Format getJobQualifier() {
    return Format.HGV;
  }

  @Override
  protected RecordsFileInfo validateFile(String _filename, long _startTime, Instant _ingestionTime) {
    //no generic flat file validation: is CSV!
    final RecordsFileInfo recordsFileInfo = new RecordsFileInfo(_filename, _startTime, _ingestionTime);
    recordsFileInfo.setHeader(new HgvRecord(_filename, 0));
    return recordsFileInfo;
  }

//  @Override
//  protected void postFileElaboration(String filename,
//                                     Instant ingestionTime,
//                                     ServicePartner servicePartner,
//                                     Map<String, Stanziamento> stanziamentiDaSpedireANAV,
//                                     ArrayList<String> errorList,
//                                     RecordsFileInfo recordsFileInfo) throws IOException {
//
//    List<AllineamentoDaConsumoMessage> allineamentiDaConsumo = stanziamentiDaSpedireANAV
//      .values().stream()
//      .flatMap(s -> s.getDettaglioStanziamenti().stream())
//      .filter(ds -> ds instanceof DettaglioStanziamentoGenerico)
//      .map(ds -> {
//        DettaglioStanziamentoGenerico dettaglioStanziamentoGenerico = (DettaglioStanziamentoGenerico) ds;
//        return new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder().deviceType(TipoDispositivoEnum.HGV)
//                                                                              .licensePlateNumber(dettaglioStanziamentoGenerico.getVehicle()
//                                                                                                                               .getLicensePlate()
//                                                                                                                               .getLicenseId())
//                                                                              .licensePlateNation(dettaglioStanziamentoGenerico.getVehicle()
//                                                                                                                               .getLicensePlate()
//                                                                                                                               .getCountryId())
//                                                                              .contractCode(dettaglioStanziamentoGenerico.getContract().getCompanyCode())
//                                                                               .customerCode(dettaglioStanziamentoGenerico.getCustomer().getId())
//                                                                              .startDate(FaiConsumiDateUtil.formatInstant(dettaglioStanziamentoGenerico.getStartDate()))
//                                                                              .endDate(FaiConsumiDateUtil.formatInstant(dettaglioStanziamentoGenerico.getDate()))
//                                                                              .build();
//      })
//      .collect(Collectors.toMinimalInformationList());
//    for (AllineamentoDaConsumoMessage allineamentoDaConsumo : allineamentiDaConsumo) {
//      allineamentiDaConsumoProducer.sendMessage(allineamentoDaConsumo);
//    }
//    super.postFileElaboration(filename, ingestionTime, servicePartner, stanziamentiDaSpedireANAV, errorList, recordsFileInfo);
//  }

  @Override
  protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
    super.afterStanziamentiSent(sourceType, stanziamentiDaSpedireANAV);
    List<AllineamentoDaConsumoMessage> allineamentiDaConsumo = stanziamentiDaSpedireANAV
      .values().stream()
      .flatMap(s -> s.getDettaglioStanziamenti().stream())
      .filter(ds -> ds instanceof DettaglioStanziamentoGenerico)
      .map(ds -> {
        DettaglioStanziamentoGenerico dettaglioStanziamentoGenerico = (DettaglioStanziamentoGenerico) ds;
        return new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder().deviceType(TipoDispositivoEnum.HGV)
          .licensePlateNumber(dettaglioStanziamentoGenerico.getVehicle()
            .getLicensePlate()
            .getLicenseId())
          .licensePlateNation(dettaglioStanziamentoGenerico.getVehicle()
            .getLicensePlate()
            .getCountryId())
          .contractCode(dettaglioStanziamentoGenerico.getContract().getCompanyCode())
          .customerCode(dettaglioStanziamentoGenerico.getCustomer().getId())
          .startDate(FaiConsumiDateUtil.formatInstant(dettaglioStanziamentoGenerico.getStartDate()))
          .endDate(FaiConsumiDateUtil.formatInstant(dettaglioStanziamentoGenerico.getDate()))
          .build();
      })
      .collect(Collectors.toList());
    for (AllineamentoDaConsumoMessage allineamentoDaConsumo : allineamentiDaConsumo) {
      allineamentiDaConsumoProducer.sendMessage(allineamentoDaConsumo);

    }
  }
}
