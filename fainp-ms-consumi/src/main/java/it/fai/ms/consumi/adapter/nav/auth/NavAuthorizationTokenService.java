package it.fai.ms.consumi.adapter.nav.auth;

public interface NavAuthorizationTokenService {

  String getAuthorizationToken();

}
