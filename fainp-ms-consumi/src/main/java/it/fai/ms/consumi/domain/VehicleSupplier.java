package it.fai.ms.consumi.domain;

public interface VehicleSupplier {

  Vehicle getVehicle();

}
