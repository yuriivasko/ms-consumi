//package it.fai.ms.consumi.web.rest.async;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Service;
//
//import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
//import it.fai.ms.consumi.domain.async.AsyncJobResponse;
//
//@Service
//public class AsyncJobService {
//
//  private final Logger log = LoggerFactory.getLogger(getClass());
//
//  private final ElasticSearchFeignClient repository;
//
//  public AsyncJobService(ElasticSearchFeignClient repository) {
//    this.repository = repository;
//  }
//
//  public void save(final AsyncJobResponse job) {
//    log.info("Persisting job.. {}", job);
//    repository.createAsychJob(job);
//  }
//
//  public AsyncJobResponse findOneById(String jobKey) {
//    return repository.findOne(jobKey);
//  }
//
//
//  public String generateKeyAndPersist() {
//    AsyncJobResponse job = new AsyncJobResponse();
//    job.setState(AsyncJobStatusEnum.PENDING);
//    repository.save(job);
//    String keyJob = job.getId();
//    log.debug("Generated key {} with PENDING status", keyJob);
//    return keyJob;
//  }
//}
