package it.fai.ms.consumi.repository.dettaglio_stanziamento.model;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.treno.DettaglioStanziamentoTreno;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntityMapper;

@Component
@Validated
public class DettaglioStanziamentoEntityMapper {

  private final DettaglioStanziamentoCarburanteEntityMapper carburanteEntityMapper;
  private final DettaglioStanziamentoGenericoEntityMapper   genericoEntityMapper;
  private final DettaglioStanziamentoPedaggioEntityMapper   pedaggioEntityMapper;
  private final DettaglioStanziamentoTrenoEntityMapper      trenoEntityMapper;

  @Inject
  public DettaglioStanziamentoEntityMapper(final DettaglioStanziamentoCarburanteEntityMapper _carburanteEntityMapper,
                                           final DettaglioStanziamentoGenericoEntityMapper _genericoEntityMapper,
                                           final DettaglioStanziamentoPedaggioEntityMapper _pedaggioEntityMapper,
                                           final DettaglioStanziamentoTrenoEntityMapper _trenoEntityMapper) {
    carburanteEntityMapper = _carburanteEntityMapper;
    genericoEntityMapper = _genericoEntityMapper;
    pedaggioEntityMapper = _pedaggioEntityMapper;
    trenoEntityMapper = _trenoEntityMapper;
  }

  public DettaglioStanziamento toDomain(final DettaglioStanziamentoEntity _entity) {

    DettaglioStanziamento allocationDetail = null;

    if (_entity instanceof DettaglioStanziamentoPedaggioEntity) {
      allocationDetail = pedaggioEntityMapper.toDomain((DettaglioStanziamentoPedaggioEntity) _entity);
    }

    if (_entity instanceof DettaglioStanziamentoCarburanteEntity) {
      allocationDetail = carburanteEntityMapper.toDomain((DettaglioStanziamentoCarburanteEntity) _entity);
    }

    if (_entity instanceof DettaglioStanziamentoTrenoEntity) {
      allocationDetail = trenoEntityMapper.toDomain((DettaglioStanziamentoTrenoEntity) _entity);
    }

    if (_entity instanceof DettaglioStanziamentoGenericoEntity) {
      allocationDetail = genericoEntityMapper.toDomain((DettaglioStanziamentoGenericoEntity) _entity);
    }

    return allocationDetail;
  }

  public List<DettaglioStanziamento> toDomain(@NotNull final Set<DettaglioStanziamentoEntity> _entity) {
    return _entity.stream()
                  .map(dettaglioStanziamentoEntity -> toDomain(dettaglioStanziamentoEntity))
                  .collect(toList());
  }

  public DettaglioStanziamentoEntity toEntity(@NotNull final DettaglioStanziamento _domain, DettaglioStanziamentoEntity _entity) {

    DettaglioStanziamentoEntity entity = null;

    if (_domain instanceof DettaglioStanziamentoPedaggio) {
      var domain = (DettaglioStanziamentoPedaggio) _domain;
      entity = pedaggioEntityMapper.toEntity(domain, (DettaglioStanziamentoPedaggioEntity) _entity);
    }

    if (_domain instanceof DettaglioStanziamentoCarburante) {
//      var domain = (DettaglioStanziamentoCarburante) _domain;
//      entity = carburanteEntityMapper.toEntity(domain);
      throw new UnsupportedOperationException("Mapper from entity is not supported for DettaglioStanziamentoCarburante");
    }

    if (_domain instanceof DettaglioStanziamentoTreno) {
//      var domain = (DettaglioStanziamentoTreno) _domain;
//      entity = trenoEntityMapper.toEntity(domain);
      throw new UnsupportedOperationException("Mapper from entity is not supported for DettaglioStanziamentoTreno");
    }

    if (_domain instanceof DettaglioStanziamentoGenerico) {
//      var domain = (DettaglioStanziamentoGenerico) _domain;
//      entity = genericoEntityMapper.toEntity(domain);
      throw new UnsupportedOperationException("Mapper from entity is not supported for DettaglioStanziamentoGenerico");
    }

    return entity;
  }

  public DettaglioStanziamentoEntity toEntity(@NotNull final DettaglioStanziamento _domain) {

    DettaglioStanziamentoEntity entity = null;

    if (_domain instanceof DettaglioStanziamentoPedaggio) {
      var domain = (DettaglioStanziamentoPedaggio) _domain;
      entity = pedaggioEntityMapper.toEntity(domain);
    }

    if (_domain instanceof DettaglioStanziamentoCarburante) {
      var domain = (DettaglioStanziamentoCarburante) _domain;
      entity = carburanteEntityMapper.toEntity(domain);
    }

    if (_domain instanceof DettaglioStanziamentoTreno) {
      var domain = (DettaglioStanziamentoTreno) _domain;
      entity = trenoEntityMapper.toEntity(domain);
    }

    if (_domain instanceof DettaglioStanziamentoGenerico) {
      var domain = (DettaglioStanziamentoGenerico) _domain;
      entity = genericoEntityMapper.toEntity(domain);
    }

    return entity;
  }

  public List<DettaglioStanziamentoEntity> toEntity(@NotNull final Set<DettaglioStanziamento> _domain) {
    return _domain.stream()
                  .map(dettaglioStanziamento -> toEntity(dettaglioStanziamento))
                  .collect(toList());
  }

}
