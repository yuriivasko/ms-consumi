package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TrackyCardPedaggiStandardRecordBody  extends TrackyCardPedaggiStandardRecord {
  
  private static final long serialVersionUID = 1L;

  private String recordCode;
  private String tipoMovimento;
  private String codiceAutostrada;
  private String descrizioneAutostrada;
  private String codiceISOIIN;
  private String codiceGruppo;
  private String codiceCliente;
  private String numeroTessera;
  private String cifradiControllo;
  private String dataIngresso;
  private String oraIngresso;
  private String dataUscita;
  private String oraUscita;
  private String caselloIngresso;
  private String caselloUscita;
  private String imponibile;
  private String imposta;
  private String imponibileImposta;
  private String percentualeIVA;
  private String segno;
  private String valuta;
  private String classeVeicolo;
  private String riservatoLibero;

  @JsonCreator
  public TrackyCardPedaggiStandardRecordBody(@JsonProperty("fileName") String fileName, @JsonProperty("rowNumber") long rowNumber, @JsonProperty("globalIdentifier") String globalIdentifier) {
    super(fileName, rowNumber, globalIdentifier);
  }

  public String getRecordCode() {
    return recordCode;
  }
  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }
 
 
  public String getTipoMovimento() {
    return tipoMovimento;
  }
  public void setTipoMovimento(String tipoMovimento) {
    this.tipoMovimento = tipoMovimento;
  }
  public String getCodiceAutostrada() {
    return codiceAutostrada;
  }
  public void setCodiceAutostrada(String codiceAutostrada) {
    this.codiceAutostrada = codiceAutostrada;
  }
  public String getDescrizioneAutostrada() {
    return descrizioneAutostrada;
  }
  public void setDescrizioneAutostrada(String descrizioneAutostrada) {
    this.descrizioneAutostrada = descrizioneAutostrada;
  }
  public String getCodiceISOIIN() {
    return codiceISOIIN;
  }
  public void setCodiceISOIIN(String codiceISOIIN) {
    this.codiceISOIIN = codiceISOIIN;
  }
  public String getCodiceGruppo() {
    return codiceGruppo;
  }
  public void setCodiceGruppo(String codiceGruppo) {
    this.codiceGruppo = codiceGruppo;
  }
  public String getCodiceCliente() {
    return codiceCliente;
  }
  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }
  public String getNumeroTessera() {
    return numeroTessera;
  }
  public void setNumeroTessera(String numeroTessera) {
    this.numeroTessera = numeroTessera;
  }
  public String getCifradiControllo() {
    return cifradiControllo;
  }
  public void setCifradiControllo(String cifradiControllo) {
    this.cifradiControllo = cifradiControllo;
  }
  public String getDataIngresso() {
    return dataIngresso;
  }
  public void setDataIngresso(String dataIngresso) {
    this.dataIngresso = dataIngresso;
  }
  public String getOraIngresso() {
    return oraIngresso;
  }
  public void setOraIngresso(String oraIngresso) {
    this.oraIngresso = oraIngresso;
  }
  public String getDataUscita() {
    return dataUscita;
  }
  public void setDataUscita(String dataUscita) {
    this.dataUscita = dataUscita;
  }
  public String getOraUscita() {
    return oraUscita;
  }
  public void setOraUscita(String oraUscita) {
    this.oraUscita = oraUscita;
  }
  public String getCaselloIngresso() {
    return caselloIngresso;
  }
  public void setCaselloIngresso(String caselloIngresso) {
    this.caselloIngresso = caselloIngresso;
  }
  public String getCaselloUscita() {
    return caselloUscita;
  }
  public void setCaselloUscita(String caselloUscita) {
    this.caselloUscita = caselloUscita;
  }
  public String getImponibile() {
    return imponibile;
  }
  public void setImponibile(String imponibile) {
    this.imponibile = imponibile;
  }
  public String getImposta() {
    return imposta;
  }
  public void setImposta(String imposta) {
    this.imposta = imposta;
  }
  public String getImponibileImposta() {
    return imponibileImposta;
  }
  public void setImponibileImposta(String imponibileImposta) {
    this.imponibileImposta = imponibileImposta;
  }
  public String getPercentualeIVA() {
    return percentualeIVA;
  }
  public void setPercentualeIVA(String percentualeIVA) {
    this.percentualeIVA = percentualeIVA;
  }
  public String getSegno() {
    return segno;
  }
  public void setSegno(String segno) {
    this.segno = segno;
  }
  public String getValuta() {
    return valuta;
  }
  public void setValuta(String valuta) {
    this.valuta = valuta;
  }
  public String getClasseVeicolo() {
    return classeVeicolo;
  }
  public void setClasseVeicolo(String classeVeicolo) {
    this.classeVeicolo = classeVeicolo;
  }
  public String getRiservatoLibero() {
    return riservatoLibero;
  }
  public void setRiservatoLibero(String riservatoLibero) {
    this.riservatoLibero = riservatoLibero;
  }
  public static long getSerialversionuid() {
    return serialVersionUID;
  }
  
  public TrackyCardPedaggiStandardRecordBody(String fileName, long rowNumber) {
    super(fileName, rowNumber);
  }
}
