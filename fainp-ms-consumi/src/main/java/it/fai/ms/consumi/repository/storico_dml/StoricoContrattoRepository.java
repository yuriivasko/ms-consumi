package it.fai.ms.consumi.repository.storico_dml;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.consumi.repository.storico_dml.model.DmlEmbeddedId;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@Repository
public interface StoricoContrattoRepository extends JpaRepository<StoricoContratto, DmlEmbeddedId>,InterfaceDmlRepository<StoricoContratto> {

  Optional<StoricoContratto> findFirstByContrattoNumeroAndDataVariazioneBeforeOrderByDataVariazioneDesc(String _contractNumber,
                                                                                                        Instant _date);

  List<StoricoContratto> findAllByCodiceAzienda(String codiceAzienda);
  
  
  @Cacheable(value = "storicoContratto", key = "{#root.methodName, #root.args[0]}")
  Optional<StoricoContratto> findFirstByContrattoNumeroOrderByDataVariazioneDesc(String _contractNumber);
  
}
