package it.fai.ms.consumi.client;

import org.springframework.stereotype.Service;

import it.fai.ms.consumi.config.ApplicationProperties;

@Service
public class CountryServiceFactory {

  public CountryServiceFactory(GeoClient client, ApplicationProperties applicationProperties) {
    super();
    this.client = client;
    this.jwt = applicationProperties.getAuthorizationHeader();
  }

  private GeoClient client;
  private String jwt;

  public CountryService getCountryService() {
    return new CountryService(client.getAllCountriesSorted(jwt));
  }
}
