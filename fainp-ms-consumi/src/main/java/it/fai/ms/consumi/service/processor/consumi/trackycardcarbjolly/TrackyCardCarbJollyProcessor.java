package it.fai.ms.consumi.service.processor.consumi.trackycardcarbjolly;

import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface TrackyCardCarbJollyProcessor extends ConsumoProcessor<TrackyCardCarburantiJolly> {

}
