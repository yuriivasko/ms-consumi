package it.fai.ms.consumi.service.record;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.repository.flussi.record.RecordRepository;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

@Service
public class RecordPersistenceServiceImpl implements RecordPersistenceService {

  private RecordRepository<CommonRecord> recordRepository;
  private final transient Logger         _log = LoggerFactory.getLogger(getClass());

  public RecordPersistenceServiceImpl(final RecordRepository<CommonRecord> recordRepository) {
    this.recordRepository = recordRepository;
  }

  @Override
  @Async
  public void persist(CommonRecord record) {
    if (_log.isDebugEnabled()) {
      _log.debug("persisting record " + record);
    }
    recordRepository.persist(record);

  }

}
