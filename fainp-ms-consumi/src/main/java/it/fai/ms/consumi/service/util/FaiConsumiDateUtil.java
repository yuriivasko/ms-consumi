package it.fai.ms.consumi.service.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.Optional;

public class FaiConsumiDateUtil {

  public static final String   DEFAULT_PATTERN = "yyyy-MM-dd HH:mm:ss";
  public static final Duration DEFAULT_OFFSET  = Duration.ZERO;
  public static final ZoneId   DEFAULT_ZONEID  = ZoneId.systemDefault();

  public static String formatInstant(Instant instant) {
    return formatInstant(null, instant, null, null);
  }

  public static String formatInstant(Instant instant, ZoneId zoneId) {
    return formatInstant(null, instant, null, zoneId);
  }

  public static String formatInstant(Instant instant, Duration offset) {
    return formatInstant(null, instant, offset, null);
  }

  public static String formatInstant(String _pattern, Instant _instant) {
    return formatInstant(_pattern, _instant, null, null);
  }

  public static LocalDate convertToLocalDate(Instant _instant) {
    return LocalDate.ofInstant(_instant, DEFAULT_ZONEID);
  }

  public static LocalTime convertToLocalTime(Instant _instant) {
    return LocalTime.ofInstant(_instant, DEFAULT_ZONEID);
  }

  private static String formatInstant(String _pattern, Instant _instant, Duration _offset, ZoneId _zoneId) {
    Instant instant = Objects.requireNonNull(_instant, "Parameter instant cannot be null!");

    String pattern = Optional.ofNullable(_pattern)
                             .orElse(DEFAULT_PATTERN);
    Duration offset = Optional.ofNullable(_offset)
                              .orElse(DEFAULT_OFFSET);
    ZoneId zoneId = Optional.ofNullable(_zoneId)
                            .orElse(DEFAULT_ZONEID);

    StringBuilder stringBuilder = new StringBuilder();

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zoneId);

    stringBuilder.append(dateTimeFormatter.format(localDateTime));
    if (offset != DEFAULT_OFFSET) {
      stringBuilder.append("(offset: ");
      stringBuilder.append(offset.toHours());
      stringBuilder.append("h)");
    }

    if (zoneId != DEFAULT_ZONEID) {
      stringBuilder.append(zoneId.toString());
    }

    return stringBuilder.toString();
  }

  public static Optional<Instant> calculateInstantByDate(final String entryDate, final String datePattern) throws ParseException {
    return calculateInstantByDateAndTime(entryDate, null, datePattern, null);
  }

  public static Optional<Instant> calculateInstantByDateAndTime(final String entryDate, final String entryTime, final String datePattern,
                                                                final String timePattern) throws ParseException {
    if (entryDate != null && !entryDate.isEmpty()) {
      if (entryTime != null && !entryTime.isEmpty() && (timePattern == null || timePattern.isEmpty()))
        throw new IllegalStateException("entryTime specified required not-null timepattern");
      String completePattern = datePattern
                               + (entryTime != null && !entryTime.isEmpty() && timePattern != null && !timePattern.isEmpty() ? timePattern
                                                                                                                             : "");
      String completeDateAndTime = entryDate + (entryTime != null ? entryTime : "");
      var dateFormat = new SimpleDateFormat(completePattern);
      var date = dateFormat.parse(completeDateAndTime);
      if (date.after(maxDate())) {
        return Optional.of(maxDate().toInstant());
      } else {
        return Optional.of(date.toInstant());
      }
    } else {
      return Optional.ofNullable(null);
    }
  }

  public static Date maxDate() {
    final var calendar = GregorianCalendar.getInstance();
    calendar.set(9999, 11, 30, 23, 59);
    return calendar.getTime();
  }
}
