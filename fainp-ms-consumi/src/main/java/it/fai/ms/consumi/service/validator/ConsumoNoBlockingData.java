package it.fai.ms.consumi.service.validator;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import it.fai.ms.consumi.domain.Device;

public class ConsumoNoBlockingData implements Serializable{
  private static final long serialVersionUID = 1065761196326318002L;
  
  private String jobName;
  private String companyCode;
  private boolean hasDevice;
  private Device device;
  private boolean isTrustedStatoDispositivo;
  
  private Instant consumoDate;
  private String contractCode;
  private Float amountExcludedVat;
  private String  groupArticlesNav;
  
  private boolean isTrustedVechicleSupplier;
  private boolean isCompleteVehicle;
  private boolean isVehicleNull;
  private List<String> recordData;
  
  public String getCompanyCode() {
    return companyCode;
  }
  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }
  public boolean isHasDevice() {
    return hasDevice;
  }
  public void setHasDevice(boolean hasDevice) {
    this.hasDevice = hasDevice;
  }
  public Device getDevice() {
    return device;
  }
  public void setDevice(Device device) {
    this.device = device;
  }
  public boolean isTrustedStatoDispositivo() {
    return isTrustedStatoDispositivo;
  }
  public void setTrustedStatoDispositivo(boolean isTrustedStatoDispositivo) {
    this.isTrustedStatoDispositivo = isTrustedStatoDispositivo;
  }
  public Instant getConsumoDate() {
    return consumoDate;
  }
  public void setConsumoDate(Instant consumoDate) {
    this.consumoDate = consumoDate;
  }
  public String getContractCode() {
    return contractCode;
  }
  public void setContractCode(String contractCode) {
    this.contractCode = contractCode;
  }
  public Float getAmountExcludedVat() {
    return amountExcludedVat;
  }
  public void setAmountExcludedVat(Float amountExcludedVat) {
    this.amountExcludedVat = amountExcludedVat;
  }
  public String getGroupArticlesNav() {
    return groupArticlesNav;
  }
  public void setGroupArticlesNav(String groupArticlesNav) {
    this.groupArticlesNav = groupArticlesNav;
  }
  public boolean isTrustedVechicleSupplier() {
    return isTrustedVechicleSupplier;
  }
  public void setTrustedVechicleSupplier(boolean isTrustedVechicleSupplier) {
    this.isTrustedVechicleSupplier = isTrustedVechicleSupplier;
  }
  public boolean isCompleteVehicle() {
    return isCompleteVehicle;
  }
  public void setCompleteVehicle(boolean isCompleteVehicle) {
    this.isCompleteVehicle = isCompleteVehicle;
  }
  public boolean isVehicleNull() {
    return isVehicleNull;
  }
  public void setVehicleNull(boolean isVehicleNull) {
    this.isVehicleNull = isVehicleNull;
  }
  public String getJobName() {
    return jobName;
  }
  public void setJobName(String jobName) {
    this.jobName = jobName;
  }
  public List<String> getRecordData() {
    return recordData;
  }
  public void setRecordData(List<String> recordData) {
    this.recordData = recordData;
  }
}
