package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoBuoniRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoOrdiniRecord;

import java.time.Instant;
import java.util.Map;
import java.util.Objects;

public class SanBernardoRecordFileInfo extends RecordsFileInfo {

  private Map<String, String> buoniOrdiniSanBernardoMap;

  public SanBernardoRecordFileInfo(String _filename, long _startTime, Instant _ingestionTime, Map<String, String> buoniOrdiniSanBernardoMap) {
    super(_filename, _startTime, _ingestionTime);
    this.buoniOrdiniSanBernardoMap = buoniOrdiniSanBernardoMap;
  }

  public Map<String, String> getBuoniOrdiniSanBernardoMap() {
    return buoniOrdiniSanBernardoMap;
  }
}
