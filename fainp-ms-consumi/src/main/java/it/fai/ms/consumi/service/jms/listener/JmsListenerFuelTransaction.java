package it.fai.ms.consumi.service.jms.listener;

  import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
  import it.fai.ms.common.jms.JmsQueueListener;
  import it.fai.ms.common.jms.JmsQueueNames;
  import it.fai.ms.common.jms.dto.consumi.fuel.FuelTransactionDTO;
  import it.fai.ms.common.jms.dto.consumi.fuel.FuelTransactionRecord;
  import it.fai.ms.consumi.domain.ServiceProvider;
  import it.fai.ms.consumi.domain.consumi.ServicePartner;
  import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
  import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsQuery;
  import it.fai.ms.consumi.repository.ServiceProviderRepository;
  import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
  import it.fai.ms.consumi.service.consumer.TrackyCardCarburantiStandardRecordConsumer;
  import it.fai.ms.consumi.service.jms.mapper.FuelTransactionDTOMapper;
  import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
  import it.fai.ms.consumi.service.record.RecordPersistenceService;
  import org.slf4j.Logger;
  import org.slf4j.LoggerFactory;
  import org.springframework.context.annotation.Profile;
  import org.springframework.stereotype.Service;
  import org.springframework.transaction.annotation.Transactional;

  import java.io.File;
  import java.time.Instant;
  import java.util.Optional;

@Service
@Transactional
@Deprecated /* messaggi da WS dresser / q8 che non dovrebbero più arrivare */
@Profile("!no-jmslistener")
public class JmsListenerFuelTransaction
  extends JmsObjectMessageListenerTemplate<FuelTransactionDTO>
  implements JmsQueueListener {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final StanziamentiParamsService                  stanziamentiParamsService;
  private final ServiceProviderRepository                  spService;
  private final FuelTransactionDTOMapper                   fuelTransactionDTOMapper;
  private final RecordPersistenceService                   persistenceService;
  private final TrackyCardCarburantiStandardRecordConsumer trackyCardCarburantiStandardRecordConsumer;

  public JmsListenerFuelTransaction(StanziamentiParamsService stanziamentiParamsService,
                                    ServiceProviderRepository spService,
                                    FuelTransactionDTOMapper fuelTransactionDTOMapper,
                                    RecordPersistenceService persistenceService,
                                    TrackyCardCarburantiStandardRecordConsumer trackyCardCarburantiStandardRecordConsumer) {
    this.stanziamentiParamsService = stanziamentiParamsService;
    this.spService = spService;
    this.fuelTransactionDTOMapper = fuelTransactionDTOMapper;
    this.persistenceService = persistenceService;
    this.trackyCardCarburantiStandardRecordConsumer = trackyCardCarburantiStandardRecordConsumer;
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.FUEL_TRANSACTIONS;
  }

  @Override
  protected void consumeMessage(FuelTransactionDTO fuelTransactionDTO) {
    log.debug("Consuming fuelTransactionDTO:=[{}]", fuelTransactionDTO);

    //scodamento messaggio e creazione record
    final Instant ingestionTime = Instant.now();
    final FuelTransactionRecord fuelTransactionRecord = fuelTransactionDTO.getRecord();

    log.debug("Retrieving StanziamentiParams...");
    final StanziamentiParams stanziamentiParams = getStanziamentiParams(fuelTransactionRecord);

    log.debug("Retrieving ServicePartner...");
    final ServicePartner servicePartner = getServicePartnerByMessage(stanziamentiParams);

    log.debug("Composing filename...");
    final String filename = getFileNameByServicePartner(servicePartner);

    log.debug("Mapping fuelTransactionDTO to TrackyCardCarburantiStandardRecord", fuelTransactionDTO);
    TrackyCardCarburantiStandardRecord record = fuelTransactionDTOMapper.toTrackyCardCarburantiStandardRecord(ingestionTime,
                                                                                                              stanziamentiParams,
                                                                                                              servicePartner,
                                                                                                              filename,
                                                                                                              fuelTransactionRecord);

    record.setFileName(new File(filename).getName());
    record.setIngestion_time(ingestionTime);

    log.debug("Persisting record");
    persistenceService.persist(record);

    //trasformazione record in consumo
    log.debug("Send record to trackyCardCarburantiStandardRecordConsumer...");
    trackyCardCarburantiStandardRecordConsumer.consume(stanziamentiParams, servicePartner, record);
  }

  private StanziamentiParams getStanziamentiParams(FuelTransactionRecord fuelTransactionRecord) {

    final StanziamentiParamsQuery queryByFuelTransactionRecord = getStanziamentiParamsQueryByFuelTransactionRecord(fuelTransactionRecord);

    Optional<StanziamentiParams> stanziamentiParamsOpt = stanziamentiParamsService.findByQuery(queryByFuelTransactionRecord);
    return stanziamentiParamsOpt.orElseThrow(
      () -> new RuntimeException(String.format("Cannot find StanziamentiParams for fuelTransactionDTO:=[%s]", fuelTransactionRecord))
    );
  }

  private StanziamentiParamsQuery getStanziamentiParamsQueryByFuelTransactionRecord(FuelTransactionRecord fuelTransactionRecord) {
    final StanziamentiParamsQuery query = new StanziamentiParamsQuery(fuelTransactionRecord.getSource(),
                                                                      fuelTransactionRecord.getRecordCode());
    query.setCodiceFornitoreLegacy(fuelTransactionRecord.getProviderCodeLegacy());
    query.setTransactionDetail(fuelTransactionRecord.getTransactionDetail());
    return query;
  }

  private String getFileNameByServicePartner(ServicePartner servicePartner) {
    return servicePartner.getFormat().name();
  }

  private ServicePartner getServicePartnerByMessage(StanziamentiParams stanziamentiParams) {
    Optional<ServiceProvider> serviceProvider = Optional.ofNullable(
      spService.findByProviderCode(stanziamentiParams.getSupplier().getCodeLegacy()));
    return serviceProvider.map((sp) -> {
      ServicePartner servicePartner = new ServicePartner(sp.getId() + "");
      servicePartner.setFormat(sp.getFormat());
      servicePartner.setPartnerCode(sp.getProviderCode());
      servicePartner.setPartnerName(sp.getProviderName());
      servicePartner.setPriceSoruce(sp.getPriceSource());
      return servicePartner;
    }).orElseThrow(
      () -> new RuntimeException(String.format("Cannot find ServicePartner for StanziamentiParams:=[%s]", stanziamentiParams))
    );
  }

}

