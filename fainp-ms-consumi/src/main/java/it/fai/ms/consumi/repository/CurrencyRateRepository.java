package it.fai.ms.consumi.repository;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.consumi.domain.CurrencyRate;
import it.fai.ms.consumi.service.dto.CurrencyRateDTO;

/**
 * Spring Data JPA repository for the CurrencyRate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, Long> {

  @Query(value="SELECT c FROM CurrencyRate c WHERE currency = :currency AND c.time = :time ORDER BY c.time DESC")
  List<CurrencyRate> findByTimeAndCurrency(@Param("time") Instant time, @Param("currency") String currency);

  @Modifying(clearAutomatically = true)
  @Query("UPDATE CurrencyRate cr SET cr.rate = :rate WHERE cr.currency = :currency AND cr.time = :time")
  int setNewRateForCurrencyRate(@Param("rate") BigDecimal rate, @Param("currency") String currency, @Param("time") Instant time);
  
  
  Optional<CurrencyRate> findFirstByCurrencyAndTimeLessThanEqualOrderByTimeDesc(@Param("currency") String currency, @Param("time") Instant time);
}
