package it.fai.ms.consumi.service.jms.producer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.actico.DettaglioCarburantiDTO;
import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.service.dto.TransactionFuelDetailDTO;

@Service
@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
public class DettaglioCarburanteTransactionJmsProducer {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  private final DettaglioStanziamantiRepository dettaglioStaziamentoRepository;

  private final static JmsQueueNames queue = JmsQueueNames.ACTICO_TRANSACTION;

  public DettaglioCarburanteTransactionJmsProducer(final JmsProperties _jmsProperties,
                                                   final DettaglioStanziamantiRepository _dettaglioStaziamentoRepository) {
    jmsProperties = _jmsProperties;
    dettaglioStaziamentoRepository = _dettaglioStaziamentoRepository;
  }

  public DettaglioCarburantiDTO sendDetailFuelMessage(TransactionFuelDetailDTO entity) {
    log.debug("Send detail fuel: {}", entity);

    DettaglioCarburantiDTO detailFuelMessage = null;
    if (isCostProceeds(entity)) {
      log.info("Dettaglio Stanziamento Carburante is of type: COSTO RICAVO");
      detailFuelMessage = mapToDettaglioCarburantiDTO(entity, entity.getPrice(), entity.getCost());
    } else {
      log.info("Dettaglio Stanziamento Carburante is of type: ONLY COST or ONLY PROCEEDS");
      detailFuelMessage = getDettaglioCarburantiDTO(entity);
    }

    log.trace("DTO: {}", detailFuelMessage);
    JmsQueueSenderUtil.publish(jmsProperties, queue, detailFuelMessage);
    log.info("DettaglioCarburanteDTO {} was sent in queue {}", detailFuelMessage, queue);
    return detailFuelMessage;
  }

  private boolean isCostProceeds(TransactionFuelDetailDTO entity) {
    return isCost(entity) && isProceeds(entity);
  }
  
  private boolean isCost(TransactionFuelDetailDTO entity) {
    return entity.getCost() != null;
  }
  
  private boolean isProceeds(TransactionFuelDetailDTO entity) {
    return entity.getPrice() != null;
  }

  private DettaglioCarburantiDTO getDettaglioCarburantiDTO(TransactionFuelDetailDTO entity) {
    DettaglioCarburantiDTO dettaglioCarburantiDTO = null;

    BigDecimal price = entity.getPrice();
    BigDecimal cost = entity.getCost();
    
    CostRevenue costProceedstoFind = isCost(entity) ? CostRevenue.REVENUE:CostRevenue.COST;

    String globalIdentifier = entity.getGlobalIdentifier();
    UUID uuid = entity.getDetailId();
    Optional<DettaglioStanziamentoEntity> optDetailsAllocation = dettaglioStaziamentoRepository.findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(globalIdentifier,
                                                                                                                                        StanziamentiDetailsType.CARBURANTI,
                                                                                                                                        uuid, costProceedstoFind);
    log.debug("Found Optional: {}", optDetailsAllocation);
    if (optDetailsAllocation.isPresent()) {
      DettaglioStanziamentoEntity detailAllocation = optDetailsAllocation.get();
      if (detailAllocation instanceof DettaglioStanziamentoCarburanteEntity) {
        DettaglioStanziamentoCarburanteEntity fuelDetail = (DettaglioStanziamentoCarburanteEntity) detailAllocation;
        log.debug("Dettaglio Carburante: {}", fuelDetail);
        if (isDetailOfCost(entity)) {
          price = fuelDetail.getPrezzoUnitarioCalcolatoNoiva();
          log.info("new Price: {}", price);
        } else {
          cost = fuelDetail.getCostoUnitarioCalcolatoNoiva();
          log.info("new Cost: {}", cost);
        }
      }
    } else {
      log.debug("Not found Dettaglio Stanziamento with global identifier: {} and with not id {}", globalIdentifier, uuid.toString());
    }

    dettaglioCarburantiDTO = mapToDettaglioCarburantiDTO(entity, price, cost);
    return dettaglioCarburantiDTO;
  }

  private boolean isDetailOfCost(TransactionFuelDetailDTO entity) {
    BigDecimal costo = entity.getCost();
    BigDecimal prezzo = entity.getPrice();
    if (costo != null && prezzo == null) {
      return true;
    }
    return false;
  }

  private DettaglioCarburantiDTO mapToDettaglioCarburantiDTO(TransactionFuelDetailDTO entity, BigDecimal price, BigDecimal cost) {
    DettaglioCarburantiDTO dto = new DettaglioCarburantiDTO();
    dto.setCode(entity.getGlobalIdentifier()
                      .toString());
    if (entity.getTipoSorgente()
              .equals(Format.API)) {
      String globalIdentifier = entity.getGlobalIdentifier();
      String code = "";
      if (StringUtils.isNotBlank(globalIdentifier) && globalIdentifier.contains("#")) {
        code = StringUtils.substring(globalIdentifier, 0, globalIdentifier.indexOf("#"));
      } else {
        code = globalIdentifier;
      }
      log.info("Overwrite code to actico-code: {}", code);
      dto.setCode(code);
    }

    dto.setCardCode(entity.getCodiceTrackyCard());
    dto.setDt(OffsetDateTime.ofInstant(entity.getDataOraUtilizzo(), ZoneOffset.UTC));
    BigDecimal percIva = entity.getPercIva();
    BigDecimal quantita = entity.getQuantity();
    dto.setQty(quantita);
    dto.setBuyerAmt(getAmount(price, quantita, percIva));
    dto.setVendorAmt(getAmount(cost, quantita, percIva));
    dto.setIsActive(1);
    dto.setOutletCode(entity.getPuntoErogazione());
    dto.setProductCode(TipoServizioEnum.CARBURANTI.name());
    log.info("Dettaglio Carburanti DTO to ACTICO: {}", dto);
    return dto;
  }

  public static BigDecimal getAmount(BigDecimal unity, BigDecimal quantity, BigDecimal percIva) {
    BigDecimal amount = BigDecimal.ZERO;
    if (unity != null && percIva != null) {
      BigDecimal vatRate = percIva.divide(new BigDecimal(100));
      BigDecimal vat = unity.multiply(vatRate);
      BigDecimal unityIncludingIva = unity.add(vat);
      amount = unityIncludingIva.multiply(quantity);
    }
    return amount.setScale(2, RoundingMode.HALF_UP);

  }

}
