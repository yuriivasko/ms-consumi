package it.fai.ms.consumi.repository.fatturazione.model;

import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "richieste_creazione_allegati",uniqueConstraints={@UniqueConstraint(columnNames = {"codice_fattura", "codice_raggruppamento_articolo"})})
public class RequestsCreationAttachmentsEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "codice_fattura")
  private String codiceFattura;

  @Column(name = "data_fattura")
  private Instant dataFattura;

  @Column(name = "codice_raggruppamento_articolo")
  private String codiceRaggruppamentoArticolo;

  @Column(name = "descrizione_raggruppamento")
  private String descrRaggruppamentoArticolo;

  @Column(name = "nome_intestazione_tessere")
  private String nomeIntestazioneTessere;

  @Column(name = "uuid_documento")
  private String uuidDocumento;

  @Column(name = "importo_totale")
  private BigDecimal importoTotale;

  @Column(name = "data")
  private Instant data;

  @Column(name = "lang")
  private String lang;

  @Column(name = "tipo_servizio")
  private String tipoServizio;

  @Column(name = "nazione_fatturazione")
  private String nazioneFatturazione;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceFattura() {
    return codiceFattura;
  }

  public RequestsCreationAttachmentsEntity codiceFattura(String codiceFattura) {
    this.codiceFattura = codiceFattura;
    return this;
  }

  public void setCodiceFattura(String codiceFattura) {
    this.codiceFattura = codiceFattura;
  }

  public Instant getDataFattura() {
    return dataFattura;
  }

  public RequestsCreationAttachmentsEntity dataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
    return this;
  }

  public void setDataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
  }

  public String getCodiceRaggruppamentoArticolo() {
    return codiceRaggruppamentoArticolo;
  }

  public RequestsCreationAttachmentsEntity codiceRaggruppamentoArticolo(String codiceRaggruppamentoArticolo) {
    this.codiceRaggruppamentoArticolo = codiceRaggruppamentoArticolo;
    return this;
  }

  public void setCodiceRaggruppamentoArticolo(String codiceRaggruppamentoArticolo) {
    this.codiceRaggruppamentoArticolo = codiceRaggruppamentoArticolo;
  }

  public String getDescrRaggruppamentoArticolo() {
    return descrRaggruppamentoArticolo;
  }

  public RequestsCreationAttachmentsEntity descrRaggruppamentoArticolo(String descrRaggruppamentoArticolo) {
    this.descrRaggruppamentoArticolo = descrRaggruppamentoArticolo;
    return this;
  }

  public void setDescrRaggruppamentoArticolo(String descrRaggruppamentoArticolo) {
    this.descrRaggruppamentoArticolo = descrRaggruppamentoArticolo;
  }

  public String getNomeIntestazioneTessere() {
    return nomeIntestazioneTessere;
  }

  public RequestsCreationAttachmentsEntity nomeIntestazioneTessere(String nomeIntestazioneTessere) {
    this.nomeIntestazioneTessere = nomeIntestazioneTessere;
    return this;
  }

  public void setNomeIntestazioneTessere(String nomeIntestazioneTessere) {
    this.nomeIntestazioneTessere = nomeIntestazioneTessere;
  }

  public String getUuidDocumento() {
    return uuidDocumento;
  }

  public RequestsCreationAttachmentsEntity uuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
    return this;
  }

  public void setUuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
  }

  public BigDecimal getImportoTotale() {
    return importoTotale;
  }

  public RequestsCreationAttachmentsEntity importoTotale(BigDecimal importoTotale) {
    this.importoTotale = importoTotale;
    return this;
  }

  public void setImportoTotale(BigDecimal importoTotale) {
    this.importoTotale = importoTotale;
  }

  public Instant getData() {
    return data;
  }

  public RequestsCreationAttachmentsEntity data(Instant data) {
    this.data = data;
    return this;
  }

  public void setData(Instant data) {
    this.data = data;
  }

  public String getLang() {
    return lang;
  }

  public RequestsCreationAttachmentsEntity lang(String lang) {
    this.lang = lang;
    return this;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public String getTipoServizio() {
    return tipoServizio;
  }

  public RequestsCreationAttachmentsEntity tipoServizio(String tipoServizio) {
    this.tipoServizio = tipoServizio;
    return this;
  }

  public void setTipoServizio(String tipoServizio) {
    this.tipoServizio = tipoServizio;
  }

  public String getNazioneFatturazione() {
    return nazioneFatturazione;
  }
  
  public RequestsCreationAttachmentsEntity nazioneFatturazione(String nazioneFatturazione) {
    this.nazioneFatturazione = nazioneFatturazione;
    return this;
  }

  public void setNazioneFatturazione(String nazioneFatturazione) {
    this.nazioneFatturazione = nazioneFatturazione;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("RequestsCreationAttachmentsEntity [");
    sb.append("codiceFattura= ");
    sb.append(codiceFattura);
    sb.append(", dataFattura= ");
    sb.append(dataFattura);
    sb.append(", codiceRaggruppamentoArticolo= ");
    sb.append(codiceRaggruppamentoArticolo);
    sb.append(", descrizioneRaggruppamentoArticolo= ");
    sb.append(descrRaggruppamentoArticolo);
    sb.append(", nomeIntestazioneTessere= ");
    sb.append(nomeIntestazioneTessere);
    sb.append(", uuidDocumento= ");
    sb.append(uuidDocumento);
    sb.append(", importoTotale= ");
    sb.append(importoTotale);
    sb.append(", data= ");
    sb.append(data);
    sb.append(", lang= ");
    sb.append(lang);
    sb.append(", tipoServizio= ");
    sb.append(tipoServizio);
    sb.append(", nazioneFatturazione= ");
    sb.append(nazioneFatturazione);
    sb.append("]");
    return sb.toString();
  }

}
