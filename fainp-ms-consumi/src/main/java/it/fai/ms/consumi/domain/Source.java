package it.fai.ms.consumi.domain;

import java.time.Instant;
import java.util.Objects;

import it.fai.ms.consumi.service.util.FaiConsumiFilesUtil;

public class Source {

  private       String  fileName;
  private       Long    rowNumber = -1l;
  private final Instant ingestionTime;
  private final Instant acquisitionDate;
  private final String  type;

  public Source(Instant _ingestionTime, Instant _acquisitionDate, final String _type) {
    this.type = Objects.requireNonNull(_type, "Parameter type is mandatory");
    this.ingestionTime = Objects.requireNonNull(_ingestionTime, "Parameter ingestionTime is mandatory");
    this.acquisitionDate = Objects.requireNonNull(_acquisitionDate, "Parameter acquisitionDate is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Source obj = getClass().cast(_obj);
      res = Objects.equals(obj.type, type)
            && Objects.equals(obj.acquisitionDate, acquisitionDate)
            && Objects.equals(obj.ingestionTime, ingestionTime)
            && Objects.equals(obj.fileName, fileName)
            && Objects.equals(obj.rowNumber, rowNumber);
    }
    return res;
  }

  public String getFileName() {
    return fileName;
  }

  public Long getRowNumber() {
    return rowNumber;
  }

  public String getType() {
    return type;
  }

  public Instant getIngestionTime() {
    return ingestionTime;
  }

  public Instant getAcquisitionDate() {
    return acquisitionDate;
  }

  @Override
  public int hashCode() {
    return Objects.hash(type,
                        acquisitionDate,
                        ingestionTime,
                        fileName);
  }

  public void setFileName(final String _fileName) {
    fileName = FaiConsumiFilesUtil.getFilename(_fileName);
  }

  public void setRowNumber(final Long _rowNumber) {
    rowNumber = _rowNumber;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("type=")
                              .append(type)
                              .append(",fileName=")
                              .append(fileName)
                              .append(",rowNumber=")
                              .append(rowNumber)
                              .append(",acquisitionDate=")
                              .append(acquisitionDate)
                              .append(",ingestionTime=")
                              .append(ingestionTime)
                              .append("]")
                              .toString();
  }

}
