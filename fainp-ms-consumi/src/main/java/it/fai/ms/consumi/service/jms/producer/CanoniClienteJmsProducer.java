package it.fai.ms.consumi.service.jms.producer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsDestination;
import it.fai.ms.common.jms.JmsObjectMessageSenderTemplate;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.consumi.service.jms.listener.canoni.RichiestaCanoniClientiMessage;

@Service
@Transactional
public class CanoniClienteJmsProducer extends JmsObjectMessageSenderTemplate<RichiestaCanoniClientiMessage> {

  public CanoniClienteJmsProducer(JmsProperties jmsProperties) {
    super(jmsProperties);
  }

  @Override
  public JmsDestination getJmsDestination() {
    return JmsQueueNames.CONSUMI_RICHIESTA_CANONI_CLIENTE;
  }

  public void sendRichiestaCanoniCliente(RichiestaCanoniClientiMessage msg) {
    sendMessage(msg);
  }
}
