package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante;

import javax.inject.Inject;
import javax.persistence.PostPersist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteJmsProducer;
import it.fai.ms.consumi.util.BeanUtil;

public class DettaglioStanziamentoCarburanteListener {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Inject
  DettaglioCarburanteJmsProducer jmsProducer;

  @PostPersist
  // @PostUpdate
  void onPostSave(DettaglioStanziamentoCarburanteEntity entity) {
    if (entity == null) {
      log.error("OnPostSave entity {} is null", DettaglioStanziamentoCarburanteEntity.class.getSimpleName());
    } else {
      if (jmsProducer == null) {
        jmsProducer = BeanUtil.getBean(DettaglioCarburanteJmsProducer.class);
      }
      log.info("EventListener on dettaglio stanziamento carburante to send at ACTICO: {}", entity.getId());
      log.debug("Code send to ACTICO: {}", entity.getGlobalIdentifier());
      jmsProducer.sendDetailFuelMessage(entity);
    }
  }

}
