package it.fai.ms.consumi.service.processor.consumi.elcit;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class ElcitConsumoDettaglioStanziamentoPedaggioMapper extends ConsumoDettaglioStanziamentoMapper {

  public ElcitConsumoDettaglioStanziamentoPedaggioMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamento dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof Elcit) {
      Elcit elcitConsumo = (Elcit) _consumo;
      dettaglioStanziamentoPedaggio = toDettaglioStanziamentoPedaggio(elcitConsumo, elcitConsumo.getGlobalIdentifier());
    }
    return dettaglioStanziamentoPedaggio;
  }

}
