package it.fai.ms.consumi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import io.github.jhipster.config.JHipsterProperties;

import java.util.List;
import java.util.Map;

/**
 * Properties specific to Faiconsumi.
 * <p>
 * Properties are configured in the application.yml file. See {@link JHipsterProperties} for a
 * good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = true)
public class ApplicationProperties {

  private String codiceClienteFai;
  private String authorizationHeader;
  private RestClientProperties             restclient;
  private NotConfirmedAllocationsConfig notConfirmedAllocationsConfig;
  private FileOrdering fileOrdering;
  private Map<String, List<String>> optionalVatConfiguration;

  private FrejusJobProperties frejusJobProperties;
  
  private String hostManagementHazelcast = null;

  public String getHostManagementHazelcast() {
  return hostManagementHazelcast;
  }

  public void setHostManagementHazelcast(String hostManagementHazelcast) {
  this.hostManagementHazelcast = hostManagementHazelcast;
  }

  public String getCodiceClienteFai() {
    return codiceClienteFai;
  }

  public void setCodiceClienteFai(String codiceClienteFai) {
    this.codiceClienteFai = codiceClienteFai;
  }

  public String getAuthorizationHeader() {
    return authorizationHeader;
  }

  public void setAuthorizationHeader(String authorizationHeader) {
    this.authorizationHeader = authorizationHeader;
  }

  public RestClientProperties getRestclient() {
    return restclient;
  }

  public void setRestclient(RestClientProperties restclient) {
    this.restclient = restclient;
  }

  public FrejusJobProperties getFrejusJobProperties() {
    return frejusJobProperties;
  }

  public void setFrejusJobProperties(FrejusJobProperties frejusJobProperties) {
    this.frejusJobProperties = frejusJobProperties;
  }

  public NotConfirmedAllocationsConfig getNotConfirmedAllocationsConfig() {
    return notConfirmedAllocationsConfig;
  }

  public void setNotConfirmedAllocationsConfig(NotConfirmedAllocationsConfig notConfirmedAllocationsConfig) {
    this.notConfirmedAllocationsConfig = notConfirmedAllocationsConfig;
  }

  public FileOrdering getFileOrdering() {
    return fileOrdering;
  }

  public void setFileOrdering(FileOrdering fileOrdering) {
    this.fileOrdering = fileOrdering;
  }

  public Map<String, List<String>> getOptionalVatConfiguration() {
    return optionalVatConfiguration;
  }

  public void setOptionalVatConfiguration(Map<String, List<String>> optionalVatConfiguration) {
    this.optionalVatConfiguration = optionalVatConfiguration;
  }
}
