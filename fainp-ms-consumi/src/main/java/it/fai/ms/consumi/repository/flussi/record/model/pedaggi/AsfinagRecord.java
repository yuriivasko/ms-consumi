package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import java.math.BigDecimal;
import java.util.UUID;

import javax.money.MonetaryAmount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryBaseRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;

public class AsfinagRecord // Go box - Pedaggi Austria
                           extends CommonRecord
  implements GlobalIdentifierBySupplier, MonetaryBaseRecord {

  private static final long serialVersionUID = -3408799781806245911L;

  public static final String DEFAULT_CURRENCY = "EUR";

  private String       numeroCorsa;
  private String       dataCreazioneFile;
  private String       numeroBollaConsegna;
  private String       numeroCarta;
  private String       dataConsegna;
  private String       tempoConsegna;
  private String       tipoTransazione;
  private String       codiceProdotto;
  private String       km;
  private String       driverId;
  private String       quantitaProdotto;
  private String       importoIvaIncl;
  private String       importoIvaEscl;
  private String       numeroRicevuta;
  private String       campoInformazione;
  private String       aliquotaIva;
  private String       importoIva;
  private final String globalIdentifier;

  @JsonCreator
  public AsfinagRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID()
                           .toString();
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }


  @Override
  public BigDecimal getVatPercRate() {
    return aliquotaIva == null ? null : NumericUtils.strToBigDecimal(aliquotaIva, 2,100);
  }

  //FIXME mmm...
  @Override
  public MonetaryAmount getAmountIncludedVat() {
    if (this.importoIvaIncl != null) {
      return MonetaryUtils.strToMonetary(this.importoIvaIncl, -2, DEFAULT_CURRENCY);
    }
    return null;  }

  @JsonIgnore
  @Override
  public MonetaryAmount getAmountExcludedVat() {
    if (this.importoIvaEscl != null) {
      return MonetaryUtils.strToMonetary(this.importoIvaEscl, -2, DEFAULT_CURRENCY);
    }
    return null;
  }

  public String getNumeroCorsa() {
    return numeroCorsa;
  }

  public void setNumeroCorsa(String numeroCorsa) {
    this.numeroCorsa = numeroCorsa;
  }

  public String getDataCreazioneFile() {
    return dataCreazioneFile;
  }

  public void setDataCreazioneFile(String dataCreazioneFile) {
    this.dataCreazioneFile = dataCreazioneFile;
  }

  public String getNumeroBollaConsegna() {
    return numeroBollaConsegna;
  }

  public void setNumeroBollaConsegna(String numeroBollaConsegna) {
    this.numeroBollaConsegna = numeroBollaConsegna;
  }

  public String getNumeroCarta() {
    return numeroCarta;
  }

  public void setNumeroCarta(String numeroCarta) {
    this.numeroCarta = numeroCarta;
  }

  public String getDataConsegna() {
    return dataConsegna;
  }

  public void setDataConsegna(String dataConsegna) {
    this.dataConsegna = dataConsegna;
  }

  public String getTempoConsegna() {
    return tempoConsegna;
  }

  public void setTempoConsegna(String tempoConsegna) {
    this.tempoConsegna = tempoConsegna;
  }

  public String getTipoTransazione() {
    return tipoTransazione;
  }

  public void setTipoTransazione(String tipoTransazione) {
    this.tipoTransazione = tipoTransazione;
  }

  public String getCodiceProdotto() {
    return codiceProdotto;
  }

  public void setCodiceProdotto(String codiceProdotto) {
    this.codiceProdotto = codiceProdotto;
  }

  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }

  public String getDriverId() {
    return driverId;
  }

  public void setDriverId(String driverId) {
    this.driverId = driverId;
  }

  public String getQuantitaProdotto() {
    return quantitaProdotto;
  }

  public void setQuantitaProdotto(String quantitaProdotto) {
    this.quantitaProdotto = quantitaProdotto;
  }

  public String getImportoIvaIncl() {
    return importoIvaIncl;
  }

  public void setImportoIvaIncl(String importoIvaIncl) {
    this.importoIvaIncl = importoIvaIncl;
  }

  public String getImportoIvaEscl() {
    return importoIvaEscl;
  }

  public void setImportoIvaEscl(String importoIvaEscl) {
    this.importoIvaEscl = importoIvaEscl;
  }

  public String getNumeroRicevuta() {
    return numeroRicevuta;
  }

  public void setNumeroRicevuta(String numeroRicevuta) {
    this.numeroRicevuta = numeroRicevuta;
  }

  public String getCampoInformazione() {
    return campoInformazione;
  }

  public void setCampoInformazione(String campoInformazione) {
    this.campoInformazione = campoInformazione;
  }

  public String getAliquotaIva() {
    return aliquotaIva;
  }

  public void setAliquotaIva(String aliquotaIva) {
    this.aliquotaIva = aliquotaIva;
  }

  public String getImportoIva() {
    return importoIva;
  }

  public void setImportoIva(String importoIva) {
    this.importoIva = importoIva;
  }

  public static String getDefaultCurrency() {
    return DEFAULT_CURRENCY;
  }

  public String getTransactionSign() {
    if (this.getTipoTransazione() != null) {
      if (this.getTipoTransazione()
              .equals("00")) {
        return "+";
      } else if (this.getTipoTransazione()
                     .equals("03")) {
        return "-";
      }
    }
    return null;
  }

}
