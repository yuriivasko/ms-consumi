package it.fai.ms.consumi.client;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.dto.ArticoloDTO;
import it.fai.ms.consumi.dto.FornitoreDTO;

public class AnagaziendeServiceImpl
  implements AnagaziendeService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final ApplicationProperties applicationProperties;
  private final AnagaziendeClient     anagaziendeClient;

  public AnagaziendeServiceImpl(ApplicationProperties applicationProperties, AnagaziendeClient anagaziendeClient) {
    this.applicationProperties = applicationProperties;
    this.anagaziendeClient = anagaziendeClient;
  }

  @Override
  public List<ArticoloDTO> getAllArticoli() {
    return anagaziendeClient.getAllArticoli(applicationProperties.getAuthorizationHeader());
  }

  @Override
  public List<FornitoreDTO> getAllFornitori() {
    return anagaziendeClient.getAllFornitori(applicationProperties.getAuthorizationHeader());
  }
}
