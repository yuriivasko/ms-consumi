package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti;

import it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd.TrackyCardCarbStdProcessor;
import org.springframework.context.annotation.Primary;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(TrackyCardCarburantiStandardJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class TrackyCardCarburantiStandardJob extends AbstractJob<TrackyCardCarburantiStandardRecord, TrackyCardCarburantiStandard> {

  public final static String QUALIFIER = "TrackyCardCarburantiStandardJob";

  public TrackyCardCarburantiStandardJob(PlatformTransactionManager transactionManager,
                                         StanziamentiParamsValidator stanziamentiParamsValidator,
                                         NotificationService notificationService,
                                         RecordPersistenceService persistenceService,
                                         RecordDescriptor<TrackyCardCarburantiStandardRecord> recordDescriptor,
                                         TrackyCardCarbStdProcessor consumoProcessor,
                                         RecordConsumoMapper<TrackyCardCarburantiStandardRecord,
                                         TrackyCardCarburantiStandard> recordConsumoMapper,
                                         StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("TRACKYCARD_STANDARD",transactionManager, stanziamentiParamsValidator, notificationService,
          persistenceService, recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
  }

  @Override
  public Format getJobQualifier() {
    return Format.FAISERVICE;
  }

}
