package it.fai.ms.consumi.client.efservice;

import static it.fai.ms.consumi.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import javax.validation.constraints.NotNull;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

import it.fai.ms.consumi.client.efservice.dto.ClienteFaiDTO;

@FeignClient(name = "faiefservice")
public interface EfserviceServiceClient {

  final String BASE_PATH                = "/api/public";
  final String CLIENTE_FAI              = BASE_PATH + "/clientefai";
  final String CLIENTE_FAI_FATTURAZIONE = CLIENTE_FAI + "/fatturazione";
  final String DISPOSITIVO              = BASE_PATH + "/dispositivo";

  @GetMapping(CLIENTE_FAI + "/{codiceCliente}")
  public ClienteFaiDTO getClienteFaiByCodice(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                             @PathVariable("codiceCliente") String codicecliente);

  @GetMapping(CLIENTE_FAI_FATTURAZIONE + "/{codiceClienteFatturazione}")
  public ClienteFaiDTO getClienteFaiByCodiceFatturazione(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                                         @PathVariable("codiceClienteFatturazione") String codiceclienteFatturazione);

  @GetMapping(DISPOSITIVO + "/tiposupporto/{tipoDispositivo}")
  public String getTipoSupportoByTipoDispositivo(@RequestHeader(AUTHORIZATION_HEADER) String authorizationHeader,
                                                 @PathVariable("tipoDispositivo") @NotNull String tipoDispositivo);

}
