package it.fai.ms.consumi.repository.flussi.record.model.carburanti;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TokheimRecord0302 extends TokheimRecord {

  private static final long serialVersionUID = -8929993262096856077L;

  String NrErogatore;
  String codiceArticoloFornitore;
  String segno;
  String quantita;
  String segnoUnitario;
  String prezzoUnitario;
  String segnoImponibile;
  String imponibile;
  String percentualeIva;
  String segnoImposta;
  String imposta;
//  String filler1;

  @JsonCreator
  public TokheimRecord0302(@JsonProperty("fileName") String fileName, @JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }
  @Override
  public int getNestedLevel() {
    return 2;
  }
  @Override
  public TokheimRecord0301 getParentRecord() {
    return (TokheimRecord0301) super.getParentRecord();
  }
  public TokheimRecord0201 get0201() {
    return getParentRecord().getParentRecord();
  }

  public TokheimRecord0301 get0301() {
    return getParentRecord();
  }

  public String getNrErogatore() {
    return NrErogatore;
  }
  public void setNrErogatore(String nrErogatore) {
    NrErogatore = nrErogatore;
  }
  public String getCodiceArticoloFornitore() {
    return codiceArticoloFornitore;
  }
  public void setCodiceArticoloFornitore(String codiceArticoloFornitore) {
    this.codiceArticoloFornitore = codiceArticoloFornitore;
  }
  public String getSegnoUnitario() {
    return segnoUnitario;
  }
  public void setSegnoUnitario(String segnoUnitario) {
    this.segnoUnitario = segnoUnitario;
  }
  public String getPrezzoUnitario() {
    return prezzoUnitario;
  }
  public void setPrezzoUnitario(String prezzoUnitario) {
    this.prezzoUnitario = prezzoUnitario;
  }
  public String getSegnoImponibile() {
    return segnoImponibile;
  }
  public void setSegnoImponibile(String segnoImponibile) {
    this.segnoImponibile = segnoImponibile;
  }
  public String getImponibile() {
    return imponibile;
  }
  public void setImponibile(String imponibile) {
    this.imponibile = imponibile;
  }
  public String getPercentualeIva() {
    return percentualeIva;
  }
  public void setPercentualeIva(String percentualeIva) {
    this.percentualeIva = percentualeIva;
  }
  public String getSegnoImposta() {
    return segnoImposta;
  }
  public void setSegnoImposta(String segnoImposta) {
    this.segnoImposta = segnoImposta;
  }
  public String getImposta() {
    return imposta;
  }
  public void setImposta(String imposta) {
    this.imposta = imposta;
  }
  public String getSegno() {
    return segno;
  }
  public void setSegno(String segno) {
    this.segno = segno;
  }
  public String getQuantita() {
    return quantita;
  }
  public void setQuantita(String quantita) {
    this.quantita = quantita;
  }

}
