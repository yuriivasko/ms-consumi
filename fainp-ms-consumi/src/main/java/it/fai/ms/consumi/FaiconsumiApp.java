package it.fai.ms.consumi;

import java.net.InetAddress;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import io.github.jhipster.config.JHipsterConstants;
import it.fai.common.notification.config.NotificationsConfiguration;
import it.fai.ms.common.jms.JmsConfiguration;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.DefaultProfileUtil;

@SpringBootApplication
@EnableConfigurationProperties({ ApplicationProperties.class })
@EnableDiscoveryClient
@EnableCircuitBreaker
@EnableScheduling
@Import({ JmsConfiguration.class, NotificationsConfiguration.class })
public class FaiconsumiApp {

  private static final Logger log = LoggerFactory.getLogger(FaiconsumiApp.class);

  private final Environment env;

  public FaiconsumiApp(final Environment _env) {
    env = _env;
  }

  /**
   * Initializes faiconsumi.
   * <p>
   * Spring profiles can be configured with a program arguments --spring.profiles.active=your-active-profile
   * <p>
   * You can find more information on how profiles work with JHipster on
   * <a href="https://www.jhipster.tech/profiles/">https://www.jhipster.tech/profiles/</a>.
   */
  @PostConstruct
  public void initApplication() {
    Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
    if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)
        && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_PRODUCTION)) {
      log.error("You have misconfigured your application! It should not run "
                + "with both the 'dev' and 'prod' profiles at the same time.");
    }
    if (activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_DEVELOPMENT)
        && activeProfiles.contains(JHipsterConstants.SPRING_PROFILE_CLOUD)) {
      log.error("You have misconfigured your application! It should not "
                + "run with both the 'dev' and 'cloud' profiles at the same time.");
    }
  }

  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(FaiconsumiApp.class);
    DefaultProfileUtil.addDefaultProfile(app);
    Environment env = app.run(args)
                         .getEnvironment();
    String protocol = "http";
    if (env.getProperty("server.ssl.key-store") != null) {
      protocol = "https";
    }
    String hostAddress = "localhost";
    try {
      hostAddress = InetAddress.getLocalHost()
                               .getHostAddress();
    } catch (@SuppressWarnings("unused") Exception e) {
      log.warn("The host name could not be determined, using `localhost` as fallback");
    }
    log.info("\n----------------------------------------------------------\n\t" + "Application '{}' is running! Access URLs:\n\t"
             + "Local: \t\t{}://localhost:{}\n\t" + "External: \t{}://{}:{}\n\t"
             + "Profile(s): \t{}\n----------------------------------------------------------", env.getProperty("spring.application.name"),
             protocol, env.getProperty("server.port"), protocol, hostAddress, env.getProperty("server.port"), env.getActiveProfiles());

    String configServerStatus = env.getProperty("configserver.status");
    log.info("\n----------------------------------------------------------\n\t"
             + "Config Server: \t{}\n----------------------------------------------------------",
             configServerStatus == null ? "Not found or not setup for this application" : configServerStatus);
  }
}
