package it.fai.ms.consumi.client;

import java.util.List;

import it.fai.ms.consumi.domain.navision.PricesByNavDTO;
import it.fai.ms.consumi.domain.navision.PricesByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.PutAllegatiByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.Stanziamento;

public class StanziamentiApiServiceImpl implements StanziamentiApiService{

  private final StanziamentiApiAuthClient stanziamentiApiAuthClient;

  public StanziamentiApiServiceImpl(StanziamentiApiAuthClient stanziamentiApiAuthClient) {
    this.stanziamentiApiAuthClient = stanziamentiApiAuthClient;
  }

  public void stanziamentiPost(String authorizationToken, List<Stanziamento> righe) throws StanziamentiPostException {
    stanziamentiApiAuthClient.stanziamentiPost(authorizationToken, righe);
  }

  public PutAllegatiByNavReturnDTO putAllegatiByNav(String requestHeader, String numeroFattura, List<String> codiciAllegati) {
    return stanziamentiApiAuthClient.putAllegatiByNav(requestHeader, numeroFattura, codiciAllegati);
  }

  public PricesByNavReturnDTO getPricesByNavPost(String requestHeader, PricesByNavDTO pricesByNavDTO) {
    return stanziamentiApiAuthClient.getPricesByNavPost(requestHeader, pricesByNavDTO);
  }

}
