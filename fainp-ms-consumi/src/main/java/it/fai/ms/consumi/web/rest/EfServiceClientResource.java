package it.fai.ms.consumi.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.consumi.client.efservice.EfserviceRestClient;
import it.fai.ms.consumi.client.efservice.dto.ClienteFaiDTO;
import it.fai.ms.consumi.web.rest.errors.BadRequestAlertException;

@RestController
@RequestMapping(EfServiceClientResource.BASE_PATH)
public class EfServiceClientResource {

  static final String BASE_PATH = "/api/public";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String CLIENTE_FAI = "/clientefaifatturazione";

  private final EfserviceRestClient client;

  public EfServiceClientResource(final EfserviceRestClient _client) {
    client = _client;
  }

  @GetMapping(CLIENTE_FAI + "/{codiceClienteFatturazione}")
  public ResponseEntity<ClienteFaiDTO> getClienteFaiByCodiceClienteFatturazione(@PathVariable("codiceClienteFatturazione") String codiceClienteFatturazione) {

    log.debug("Request to retrieve cliente fai by codice cliente fatturazione: {}", codiceClienteFatturazione);

    ClienteFaiDTO clienteFai = null;
    try {
      clienteFai = client.findByCodiceClienteFatturazione(codiceClienteFatturazione);
    } catch (Exception e) {
      log.error("Error on call rest cliente efservice: ", e);
      throw new BadRequestAlertException("Error on call rest cliente efservice", EfserviceRestClient.class.getSimpleName(), "");
    }

    return ResponseEntity.ok(clienteFai);
  }

}
