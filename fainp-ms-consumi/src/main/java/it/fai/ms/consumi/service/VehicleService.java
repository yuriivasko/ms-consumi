package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Vehicle;

public interface VehicleService {

  Optional<Vehicle> newVehicleByVehicleUUID_WithFareClass(@NotNull Device device, @NotNull Instant instant,
                                                      @NotNull final String fareClass);

}
