package it.fai.ms.consumi.client;


import static it.fai.ms.consumi.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;


/**
 * REST controller for managing Country.
 */
@FeignClient(name = "faigeo")
public interface GeoClient {

	@GetMapping("/api/countries-sorted")
	public List<CountryDTO> getAllCountriesSorted(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken);
}
