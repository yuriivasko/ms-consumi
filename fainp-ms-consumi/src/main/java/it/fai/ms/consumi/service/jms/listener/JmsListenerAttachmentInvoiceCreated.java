package it.fai.ms.consumi.service.jms.listener;

import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.consumi.invoice.AttachmentInvoiceCreatedDTO;
import it.fai.ms.consumi.service.consumer.AttachmentInvoiceCreatedConsumer;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsListenerAttachmentInvoiceCreated implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final AttachmentInvoiceCreatedConsumer attachmentInvoiceService;

  @Autowired
  public JmsListenerAttachmentInvoiceCreated(final AttachmentInvoiceCreatedConsumer _attachmentInvoiceService) {
    attachmentInvoiceService = _attachmentInvoiceService;
  }

  public void onMessage(Message _message) {
    AttachmentInvoiceCreatedDTO invoiceAttachment = null;
    try {
      try {
        invoiceAttachment = (AttachmentInvoiceCreatedDTO) ((ObjectMessage) _message).getObject();
      } catch (@SuppressWarnings("unused") final ClassCastException _e) {
        log.warn("JMS ObjectMessage isn't {}", AttachmentInvoiceCreatedDTO.class.getSimpleName());
      }
      log.info("Received JMS message {}", invoiceAttachment);
      attachmentInvoiceService.consume(invoiceAttachment);
    } catch (final Exception _e) {
      log.error("Error consuming message {}", _message, _e);
      throw new RuntimeException(_e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.GENERA_ALLEGATO_FATTURE_RESPONSE;
  }
}
