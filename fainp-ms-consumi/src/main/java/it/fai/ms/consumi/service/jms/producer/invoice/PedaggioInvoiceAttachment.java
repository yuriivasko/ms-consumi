package it.fai.ms.consumi.service.jms.producer.invoice;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.common.jms.fattura.PedaggioAllegatoFatturaMessage;
import it.fai.ms.common.jms.fattura.PedaggioBodyDTO;
import it.fai.ms.common.jms.fattura.PedaggioHeaderDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentiRepositoryImpl;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione2Entity;
import it.fai.ms.consumi.service.ViewAllegatiPedaggiService;

@Component
public class PedaggioInvoiceAttachment implements IGenerateInvoiceAttachment {

  private final DettaglioStanziamentiRepositoryImpl dettagliRepository;

  private final ViewAllegatiPedaggiService viewAllegatiPedaggiService;

  public PedaggioInvoiceAttachment(final DettaglioStanziamentiRepositoryImpl _dettagliRepository,
                                   ViewAllegatiPedaggiService _viewAllegatiPedaggiService) {
    dettagliRepository = _dettagliRepository;
    viewAllegatiPedaggiService = _viewAllegatiPedaggiService;
  }

  @Override
  public Object generateDtoToAttachmentInvoice(DettaglioStanziamentoType tipoDettaglio,
                                               final RequestsCreationAttachments requestCreationAttachments,
                                               final RaggruppamentoArticoliDTO raggruppamentoArticoliDTO, final TemplateAllegati template) {
    long t1 = System.currentTimeMillis();
    if (!getDettaglioStanziamentoType().equals(tipoDettaglio)) {
      return null;
    }

    // Template actually not used, but Marina explain that it is possible to use this field to future implementation.
    log.info("Start generate message to create Allegati Fattura {} ...", tipoDettaglio);
    PedaggioAllegatoFatturaMessage message = new PedaggioAllegatoFatturaMessage(getNomeDocumento(requestCreationAttachments,
                                                                                                 raggruppamentoArticoliDTO));
    message.setCodiceRaggruppamentoArticolo(requestCreationAttachments.getCodiceRaggruppamentoArticolo());
    message.setLang(requestCreationAttachments.getLang()
                                              .toLowerCase());

    message.setHeader(createHeaderMessage(requestCreationAttachments, raggruppamentoArticoliDTO));

    final List<String> codiciStanziamenti = raggruppamentoArticoliDTO.getCodiciStanziamenti();
    List<UUID> dettagliStanziamentoId = dettagliRepository.findDettagliStanziamentoIdByCodiciStanziamento(codiciStanziamenti);
    List<ViewAllegatoPedaggioSezione2Entity> dataAllegatiPedaggi = viewAllegatiPedaggiService.getDettagliStanziamento(dettagliStanziamentoId);
    List<PedaggioBodyDTO> bodyWithoutSection1 = viewAllegatiPedaggiService.createBodyMessageAndSection2(dataAllegatiPedaggi,
                                                                                                        requestCreationAttachments);

    List<PedaggioBodyDTO> body = viewAllegatiPedaggiService.addSection1OnBody(dataAllegatiPedaggi,
                                                                              requestCreationAttachments.getDataFattura(),
                                                                              bodyWithoutSection1);

    message.setBody(body);
    log.info("Finished generate message to create Allegati Fattura {} in {} ms", tipoDettaglio, System.currentTimeMillis() - t1);
    printToJsonFormat(message);
    return message;
  }

  @Override
  public DettaglioStanziamentoType getDettaglioStanziamentoType() {
    return DettaglioStanziamentoType.PEDAGGIO;
  }

  private PedaggioHeaderDTO createHeaderMessage(RequestsCreationAttachments requestCreationAttachments,
                                                RaggruppamentoArticoliDTO raggruppamentoArticoliDTO) {

    PedaggioHeaderDTO header = new PedaggioHeaderDTO();
    header.setNumeroFattura(requestCreationAttachments.getCodiceFattura());

    LocalDate localDateFattura = LocalDate.ofInstant(requestCreationAttachments.getDataFattura(), ZoneOffset.UTC);
    header.setDataFattura(localDateFattura);

    String codiceClienteFatturazione = raggruppamentoArticoliDTO.getCodiceClienteFatturazione();
    String ragioneSocialeClienteFatturazione = raggruppamentoArticoliDTO.getRagioneSocialeClienteFatturazione();
    String clienteHeader = StringUtils.isNoneBlank(ragioneSocialeClienteFatturazione) ? codiceClienteFatturazione + " - "
                                                                                        + ragioneSocialeClienteFatturazione
                                                                                      : codiceClienteFatturazione;
    header.setCodiceCliente(codiceClienteFatturazione);
    header.setCliente(clienteHeader);
    header.setRagioneSociale(ragioneSocialeClienteFatturazione);
    header.setNomeIntestazioneTessere(requestCreationAttachments.getNomeIntestazioneTessere());
    header.setDettaglio(requestCreationAttachments.getDescrRaggruppamentoArticolo());
    return header;
  }

}
