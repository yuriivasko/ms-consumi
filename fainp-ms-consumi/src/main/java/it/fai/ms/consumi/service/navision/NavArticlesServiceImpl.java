package it.fai.ms.consumi.service.navision;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.adapter.nav.NavArticlesAdapter;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.mapper.ArticleToStanziamentiParamsMapper;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;

@Service
public class NavArticlesServiceImpl implements NavArticlesService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final NavArticlesAdapter                navArticleAdapter;
  private final ArticleToStanziamentiParamsMapper stanziamentiParamsMapper;
  private final StanziamentiParamsService         stanziamentiParamsService;
  private final boolean validatorConsistencyEnabled;

  @Inject
  public NavArticlesServiceImpl(final NavArticlesAdapter _navArticleAdapter,
                                final StanziamentiParamsService _stanziamentiParamsService,
                                final ArticleToStanziamentiParamsMapper _stanziamentiParamsMapper,
                                final @Value("#{new Boolean(${validator.stanziamentiParams.consistency:false})}") boolean _validatorConsistencyEnabled) {
    navArticleAdapter = _navArticleAdapter;
    stanziamentiParamsMapper = _stanziamentiParamsMapper;
    stanziamentiParamsService = _stanziamentiParamsService;
    validatorConsistencyEnabled = _validatorConsistencyEnabled;
  }

  @Override
  public List<Article> getAllArticlesAndUpdateStanziamentiParams(AllStanziamentiParams allStanziamentiParams) {
    List<Article> articles = new LinkedList<>();

    _log.info("Retrieve all articles");

    articles = navArticleAdapter.getAllArticles();
    _log.info("retrieved {} articles", articles.size());
    _log.trace("All articles : {}", articles);


    _log.info("Updating StanziamentiParams based on nav articles...");
    Map<String, List<StanziamentiParams>> stanziamentiParamsGropupedByArticles = allStanziamentiParams.getStanziamentiParamsGropupedByArticleCodes();
    articles.stream()
            .forEach(article -> {
              Optional.ofNullable(stanziamentiParamsGropupedByArticles.get(article.getCode())).ifPresentOrElse(
                stanziamentiParamsList -> {
                  _log.debug("Mapping article to StanziamentiParams...");
                  stanziamentiParamsList.stream()
                    .map(currentStanziamentiParams -> {
                      _log.debug("Current StanziamentiParams [{}]", currentStanziamentiParams);
                      return stanziamentiParamsMapper.toStanziamentiParams(article, currentStanziamentiParams);
                    })
                    .forEach(updatedStanziamentiParams -> {
                      _log.debug("Saving updated StanziamentiParams [{}]", updatedStanziamentiParams);
                      if (validatorConsistencyEnabled) {
                        stanziamentiParamsService.updateStanziamentiParams(updatedStanziamentiParams);
                      } else {
                        _log.debug("validatorConsistencyEnabled is [{}], skipping update", validatorConsistencyEnabled);
                      }
                    });
                },
                () -> _log.debug("No StanziamentiParams found for this article [{}]. Skipping it.", article.getCode()));
            });
    return articles;
  }

}
