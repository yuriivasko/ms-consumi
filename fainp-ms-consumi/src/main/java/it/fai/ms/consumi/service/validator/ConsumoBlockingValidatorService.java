package it.fai.ms.consumi.service.validator;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.consumi.Consumo;

public interface ConsumoBlockingValidatorService {

  ValidationOutcome validate(@NotNull Consumo consumo);

}
