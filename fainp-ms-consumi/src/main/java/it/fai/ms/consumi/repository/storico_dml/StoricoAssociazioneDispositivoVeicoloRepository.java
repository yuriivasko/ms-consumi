package it.fai.ms.consumi.repository.storico_dml;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.consumi.repository.storico_dml.model.DmlEmbeddedId;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAssociazioneDispositivoVeicolo;

@Repository
public interface StoricoAssociazioneDispositivoVeicoloRepository extends JpaRepository<StoricoAssociazioneDispositivoVeicolo, DmlEmbeddedId>, InterfaceDmlRepository<StoricoAssociazioneDispositivoVeicolo> {

}
