package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class DartfordRecordDescriptor implements RecordDescriptor<DartfordRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(DartfordRecordDescriptor.class);

  private final List<String> HEADER = Arrays.asList("crossingDate", "time", "direction", "vehicle", "groupName", "billedOn");

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public String getHeaderBegin() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean isHeaderLine(String line) {
    return line.contains("Crossing Date");
  }
  @Override
  public boolean isFooterLine(String line) {
    return false;
  }

  @Override
  public String getFooterCodeBegin() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  @Override
  public String extractRecordCode(String _data) {
    return "DEFAULT";
  }

  @Override
  public DartfordRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {
    DartfordRecord recordObject = new DartfordRecord(fileName, rowNumber);

    if (rowNumber == 0) {
      recordObject.setRecordCode("DEFAULT");
      recordObject.setTransactionDetail("");
      return recordObject;
    }

    recordObject.setTransactionDetail("");
    recordObject.setFromCsvRecord(_data, HEADER);
    recordObject.setRecordCode("DEFAULT");

    return recordObject;

  }

  protected Date getNow() {
    return new Date();
  }

  public void checkConfiguration(){
    log.trace("No need to check configuration...");
  }
}
