package it.fai.ms.consumi.service.validator.internal;

import java.time.Instant;

import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.validator.TimeValidator;

public interface CustomerValidator extends TimeValidator {

  ValidationOutcome customerActiveInDate(String customerUuid, Instant instant);

}
