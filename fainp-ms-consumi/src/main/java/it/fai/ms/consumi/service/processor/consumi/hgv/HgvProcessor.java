package it.fai.ms.consumi.service.processor.consumi.hgv;

import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface HgvProcessor
  extends ConsumoProcessor<Hgv> {

}
