package it.fai.ms.consumi.service.processor;

import it.fai.ms.consumi.service.validator.ConsumoNoBlockingData;

public class SingleRecordProcessingResult {

  private ProcessingResult processingResult;
  private final ConsumoNoBlockingData consumoNoBlockingData;
  private Exception exception;

  public SingleRecordProcessingResult(final ProcessingResult processingResult) {
    this.processingResult = processingResult;
    consumoNoBlockingData = null;
  }

  public SingleRecordProcessingResult(final Exception exception) {
    this.exception = exception;
    consumoNoBlockingData = null;
  }

  public SingleRecordProcessingResult(ProcessingResult processingResult, ConsumoNoBlockingData consumoNoBlockingData) {
    this.processingResult = processingResult;
    this.consumoNoBlockingData = consumoNoBlockingData; 
  }

  public boolean isOK(){
    return (processingResult != null && processingResult.getValidationOutcome().isValidationOk() && !processingResult.getValidationOutcome().isFatalError())
      && exception == null;

  }
  
  public ConsumoNoBlockingData getConsumoNoBlockingData() {
    return consumoNoBlockingData;
  }

}
