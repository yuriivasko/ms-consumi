package it.fai.ms.consumi.domain.consumi;

import java.time.Duration;
import java.time.Instant;

public interface DateSupplier {
  
  // Tutte le validazioni non bloccanti alla data del consumo deve essere aggiunta 24h
  static final Duration VALIDATOR_DATE_OFFSET = Duration.ofHours(24);

  Instant getDate();
  
  default Instant getDateWithOffsetForValidator() {
    return getDate()!=null ? getDate().plus(VALIDATOR_DATE_OFFSET) : null;
  }

}
