package it.fai.ms.consumi.repository.flussi.record.model.util;

public class StringItem {

  private String method = "";
  private int    start  = 0;
  private int    end    = 0;

  public StringItem() {
    super();
  }

  public StringItem(final String _methodName, final int _start, final int _end) {
    super();
    method = _methodName.trim();
    start = _start;
    end = _end;
  }

  public String getMethod() {
    return method;
  }

  public int getStart() {
    return start;
  }

  public int getEnd() {
    return end;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("StringItem [method=");
    builder.append(method);
    builder.append(", start=");
    builder.append(start);
    builder.append(", end=");
    builder.append(end);
    builder.append("]");
    return builder.toString();
  }

}
