package it.fai.ms.consumi.client;

import it.fai.ms.common.jms.dto.vehicle.CentroDiCostroSearchDTO;

public interface VehicleClientService {

  String getCentroDiCosto(CentroDiCostroSearchDTO dto);

}
