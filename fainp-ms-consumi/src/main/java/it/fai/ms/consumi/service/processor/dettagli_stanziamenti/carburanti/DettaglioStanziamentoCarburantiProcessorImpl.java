package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoProcessor;
import it.fai.ms.consumi.service.processor.stanziamenti.carburanti.StanziamentoProcessorCarburanteImpl;

@Service
public class DettaglioStanziamentoCarburantiProcessorImpl implements DettaglioStanziamentoCarburantiProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoCaburantiDaConsumoProcessorImpl       daCosumoProcessor;
  private final DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl daTabellaPrezziProcessor;
  private final DettaglioStanziamantiRepository                            dettaglioStanziamantiRepository;
  private final StanziamentoProcessor                                      stanziamentoProcessor;

  @Inject
  public DettaglioStanziamentoCarburantiProcessorImpl(final StanziamentoProcessorCarburanteImpl _stanziamentoProcessor,
                                                      final DettaglioStanziamantiRepository _dettagliStanziamantiRepository,
                                                      final DettaglioStanziamentoCaburantiDaTabellaPrezziProcessorImpl _daTabellaPrezziProcessor,
                                                      final DettaglioStanziamentoCaburantiDaConsumoProcessorImpl _daCosumoProcessor) {
    stanziamentoProcessor = _stanziamentoProcessor;
    daTabellaPrezziProcessor = _daTabellaPrezziProcessor;
    daCosumoProcessor = _daCosumoProcessor;
    dettaglioStanziamantiRepository = _dettagliStanziamantiRepository;
  }

  @Override
  public List<Stanziamento> produce(final DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                    final StanziamentiParams _stanziamentiParams) {

    _log.info("{} Processing start {}", _dettaglioStanziamentoCarburante, _stanziamentiParams);

    //settaggio unita' di misura
    _dettaglioStanziamentoCarburante.setMesaurementUnit(_stanziamentiParams.getArticle().getMeasurementUnit());

    // calcolo costo, ricavo, definitivo provvisorio
    final var dettagliStanziamanti = getDettagliCostoRivavo(_dettaglioStanziamentoCarburante, _stanziamentiParams);

    _log.debug("Creato/i {} dettagli stanziamento carburante", dettagliStanziamanti.size());

    List<Stanziamento> stanziamentiAll = new ArrayList<>();
    for ( DettaglioStanziamentoCarburante dettaglio : dettagliStanziamanti) {
      stanziamentiAll.addAll(generateStanziamentoAndSaveDettaglio(dettaglio, _stanziamentiParams));
    }

    return stanziamentiAll;
  }

  public List<Stanziamento> generateStanziamentoAndSaveDettaglio(DettaglioStanziamentoCarburante dettaglio, final StanziamentiParams _stanziamentiParams){
    List<Stanziamento> stanziamenti = stanziamentoProcessor.processStanziamento(dettaglio, _stanziamentiParams);
    dettaglio.getStanziamenti().addAll(stanziamenti);
    dettaglioStanziamantiRepository.save(dettaglio);
    return stanziamenti;
  }


  // TODO move into a separated service
  private List<DettaglioStanziamentoCarburante> getDettagliCostoRivavo(final DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                                                       final StanziamentiParams stanziamentiParams) {

    switch (_dettaglioStanziamentoCarburante.getServicePartner()
                                            .getPriceSoruce()) {
    case by_invoice:
      return daCosumoProcessor.process(_dettaglioStanziamentoCarburante, stanziamentiParams);
    case by_prices_table:
      return daTabellaPrezziProcessor.process(_dettaglioStanziamentoCarburante, stanziamentiParams);
    default:
      throw new IllegalArgumentException("value " + _dettaglioStanziamentoCarburante.getServicePartner()
                                                                                    .getPriceSoruce()
                                         + " not permitted");
    }
  }

}
