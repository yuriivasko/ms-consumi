package it.fai.ms.consumi.dto;

import java.math.BigDecimal;


public class ArticoloDTO {

  private String     key;
  private String     no;
  private String     description;
  private String     description2;
  private String     type;
  private BigDecimal inventory;
  private Boolean    createdFromNonstockItem;
  private Boolean    substitutesExist;
  private Boolean    stockkeepingUnitExists;
  private Boolean    assemblyBOM;
  private String     productionBOMNo;
  private String     routingNo;
  private String     baseUnitOfMeasure;
  private String     shelfNo;
  private String     costingMethod;
  private Boolean    costIsAdjusted;
  private BigDecimal standardCost;
  private BigDecimal unitCost;
  private BigDecimal lastDirectCost;
  private String     priceProfitCalculation;
  private BigDecimal profitPercent;
  private BigDecimal unitPrice;
  private String     inventoryPostingGroup;
  private String     genProdPostingGroup;
  private String     vatProdPostingGroup;
  private String     itemDiscGroup;
  private String     vendorNo;
  private String     vendorItemNo;
  private String     tariffNo;
  private String     searchDescription;
  private BigDecimal overheadRate;
  private BigDecimal indirectCostPercent;
  private String     itemCategoryCode;
  private String     productGroupCode;
  private Boolean    blocked;
  private String     lastDateModified;
  private String     salesUnitOfMeasure;
  private String     replenishmentSystem;
  private String     purchUnitOfMeasure;
  private String     leadTimeCalculation;
  private String     manufacturingPolicy;
  private String     flushingMethod;
  private String     assemblyPolicy;
  private String     itemTrackingCode;
  private BigDecimal costKg;
  private BigDecimal unitCostFixed;
  private BigDecimal netChange;
  private String     vendorName;
  private Boolean    workInProgress;
  private String     commissionGroupCode;
  private Boolean    comment;
  private String     globalDimension2Code;
  private String     commonItemNo;
  private String     customerNo;
  private String     defaultDeferralTemplateCode;
  private Boolean    flgArtCosto;
  private Boolean    consumable;
  private Boolean    freePass;
  private String     defaultLocation;
  private String     defaultBin;
  private String     raggruppamentoArticolo;
  private String     countryRegionOfOriginCode;
  private String     valuta;
  private BigDecimal percentualeIva;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescription2() {
    return description2;
  }

  public void setDescription2(String description2) {
    this.description2 = description2;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public BigDecimal getInventory() {
    return inventory;
  }

  public void setInventory(BigDecimal inventory) {
    this.inventory = inventory;
  }

  public Boolean getCreatedFromNonstockItem() {
    return createdFromNonstockItem;
  }

  public void setCreatedFromNonstockItem(Boolean createdFromNonstockItem) {
    this.createdFromNonstockItem = createdFromNonstockItem;
  }

  public Boolean getSubstitutesExist() {
    return substitutesExist;
  }

  public void setSubstitutesExist(Boolean substitutesExist) {
    this.substitutesExist = substitutesExist;
  }

  public Boolean getStockkeepingUnitExists() {
    return stockkeepingUnitExists;
  }

  public void setStockkeepingUnitExists(Boolean stockkeepingUnitExists) {
    this.stockkeepingUnitExists = stockkeepingUnitExists;
  }

  public Boolean getAssemblyBOM() {
    return assemblyBOM;
  }

  public void setAssemblyBOM(Boolean assemblyBOM) {
    this.assemblyBOM = assemblyBOM;
  }

  public String getProductionBOMNo() {
    return productionBOMNo;
  }

  public void setProductionBOMNo(String productionBOMNo) {
    this.productionBOMNo = productionBOMNo;
  }

  public String getRoutingNo() {
    return routingNo;
  }

  public void setRoutingNo(String routingNo) {
    this.routingNo = routingNo;
  }

  public String getBaseUnitOfMeasure() {
    return baseUnitOfMeasure;
  }

  public void setBaseUnitOfMeasure(String baseUnitOfMeasure) {
    this.baseUnitOfMeasure = baseUnitOfMeasure;
  }

  public String getShelfNo() {
    return shelfNo;
  }

  public void setShelfNo(String shelfNo) {
    this.shelfNo = shelfNo;
  }

  public String getCostingMethod() {
    return costingMethod;
  }

  public void setCostingMethod(String costingMethod) {
    this.costingMethod = costingMethod;
  }

  public Boolean getCostIsAdjusted() {
    return costIsAdjusted;
  }

  public void setCostIsAdjusted(Boolean costIsAdjusted) {
    this.costIsAdjusted = costIsAdjusted;
  }

  public BigDecimal getStandardCost() {
    return standardCost;
  }

  public void setStandardCost(BigDecimal standardCost) {
    this.standardCost = standardCost;
  }

  public BigDecimal getUnitCost() {
    return unitCost;
  }

  public void setUnitCost(BigDecimal unitCost) {
    this.unitCost = unitCost;
  }

  public BigDecimal getLastDirectCost() {
    return lastDirectCost;
  }

  public void setLastDirectCost(BigDecimal lastDirectCost) {
    this.lastDirectCost = lastDirectCost;
  }

  public String getPriceProfitCalculation() {
    return priceProfitCalculation;
  }

  public void setPriceProfitCalculation(String priceProfitCalculation) {
    this.priceProfitCalculation = priceProfitCalculation;
  }

  public BigDecimal getProfitPercent() {
    return profitPercent;
  }

  public void setProfitPercent(BigDecimal profitPercent) {
    this.profitPercent = profitPercent;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public String getInventoryPostingGroup() {
    return inventoryPostingGroup;
  }

  public void setInventoryPostingGroup(String inventoryPostingGroup) {
    this.inventoryPostingGroup = inventoryPostingGroup;
  }

  public String getGenProdPostingGroup() {
    return genProdPostingGroup;
  }

  public void setGenProdPostingGroup(String genProdPostingGroup) {
    this.genProdPostingGroup = genProdPostingGroup;
  }

  public String getVatProdPostingGroup() {
    return vatProdPostingGroup;
  }

  public void setVatProdPostingGroup(String vatProdPostingGroup) {
    this.vatProdPostingGroup = vatProdPostingGroup;
  }

  public String getItemDiscGroup() {
    return itemDiscGroup;
  }

  public void setItemDiscGroup(String itemDiscGroup) {
    this.itemDiscGroup = itemDiscGroup;
  }

  public String getVendorNo() {
    return vendorNo;
  }

  public void setVendorNo(String vendorNo) {
    this.vendorNo = vendorNo;
  }

  public String getVendorItemNo() {
    return vendorItemNo;
  }

  public void setVendorItemNo(String vendorItemNo) {
    this.vendorItemNo = vendorItemNo;
  }

  public String getTariffNo() {
    return tariffNo;
  }

  public void setTariffNo(String tariffNo) {
    this.tariffNo = tariffNo;
  }

  public String getSearchDescription() {
    return searchDescription;
  }

  public void setSearchDescription(String searchDescription) {
    this.searchDescription = searchDescription;
  }

  public BigDecimal getOverheadRate() {
    return overheadRate;
  }

  public void setOverheadRate(BigDecimal overheadRate) {
    this.overheadRate = overheadRate;
  }

  public BigDecimal getIndirectCostPercent() {
    return indirectCostPercent;
  }

  public void setIndirectCostPercent(BigDecimal indirectCostPercent) {
    this.indirectCostPercent = indirectCostPercent;
  }

  public String getItemCategoryCode() {
    return itemCategoryCode;
  }

  public void setItemCategoryCode(String itemCategoryCode) {
    this.itemCategoryCode = itemCategoryCode;
  }

  public String getProductGroupCode() {
    return productGroupCode;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }

  public Boolean getBlocked() {
    return blocked;
  }

  public void setBlocked(Boolean blocked) {
    this.blocked = blocked;
  }

  public String getLastDateModified() {
    return lastDateModified;
  }

  public void setLastDateModified(String lastDateModified) {
    this.lastDateModified = lastDateModified;
  }

  public String getSalesUnitOfMeasure() {
    return salesUnitOfMeasure;
  }

  public void setSalesUnitOfMeasure(String salesUnitOfMeasure) {
    this.salesUnitOfMeasure = salesUnitOfMeasure;
  }

  public String getReplenishmentSystem() {
    return replenishmentSystem;
  }

  public void setReplenishmentSystem(String replenishmentSystem) {
    this.replenishmentSystem = replenishmentSystem;
  }

  public String getPurchUnitOfMeasure() {
    return purchUnitOfMeasure;
  }

  public void setPurchUnitOfMeasure(String purchUnitOfMeasure) {
    this.purchUnitOfMeasure = purchUnitOfMeasure;
  }

  public String getLeadTimeCalculation() {
    return leadTimeCalculation;
  }

  public void setLeadTimeCalculation(String leadTimeCalculation) {
    this.leadTimeCalculation = leadTimeCalculation;
  }

  public String getManufacturingPolicy() {
    return manufacturingPolicy;
  }

  public void setManufacturingPolicy(String manufacturingPolicy) {
    this.manufacturingPolicy = manufacturingPolicy;
  }

  public String getFlushingMethod() {
    return flushingMethod;
  }

  public void setFlushingMethod(String flushingMethod) {
    this.flushingMethod = flushingMethod;
  }

  public String getAssemblyPolicy() {
    return assemblyPolicy;
  }

  public void setAssemblyPolicy(String assemblyPolicy) {
    this.assemblyPolicy = assemblyPolicy;
  }

  public String getItemTrackingCode() {
    return itemTrackingCode;
  }

  public void setItemTrackingCode(String itemTrackingCode) {
    this.itemTrackingCode = itemTrackingCode;
  }

  public BigDecimal getCostKg() {
    return costKg;
  }

  public void setCostKg(BigDecimal costKg) {
    this.costKg = costKg;
  }

  public BigDecimal getUnitCostFixed() {
    return unitCostFixed;
  }

  public void setUnitCostFixed(BigDecimal unitCostFixed) {
    this.unitCostFixed = unitCostFixed;
  }

  public BigDecimal getNetChange() {
    return netChange;
  }

  public void setNetChange(BigDecimal netChange) {
    this.netChange = netChange;
  }

  public String getVendorName() {
    return vendorName;
  }

  public void setVendorName(String vendorName) {
    this.vendorName = vendorName;
  }

  public Boolean getWorkInProgress() {
    return workInProgress;
  }

  public void setWorkInProgress(Boolean workInProgress) {
    this.workInProgress = workInProgress;
  }

  public String getCommissionGroupCode() {
    return commissionGroupCode;
  }

  public void setCommissionGroupCode(String commissionGroupCode) {
    this.commissionGroupCode = commissionGroupCode;
  }

  public Boolean getComment() {
    return comment;
  }

  public void setComment(Boolean comment) {
    this.comment = comment;
  }

  public String getGlobalDimension2Code() {
    return globalDimension2Code;
  }

  public void setGlobalDimension2Code(String globalDimension2Code) {
    this.globalDimension2Code = globalDimension2Code;
  }

  public String getCommonItemNo() {
    return commonItemNo;
  }

  public void setCommonItemNo(String commonItemNo) {
    this.commonItemNo = commonItemNo;
  }

  public String getCustomerNo() {
    return customerNo;
  }

  public void setCustomerNo(String customerNo) {
    this.customerNo = customerNo;
  }

  public String getDefaultDeferralTemplateCode() {
    return defaultDeferralTemplateCode;
  }

  public void setDefaultDeferralTemplateCode(String defaultDeferralTemplateCode) {
    this.defaultDeferralTemplateCode = defaultDeferralTemplateCode;
  }

  public Boolean getFlgArtCosto() {
    return flgArtCosto;
  }

  public void setFlgArtCosto(Boolean flgArtCosto) {
    this.flgArtCosto = flgArtCosto;
  }

  public Boolean getConsumable() {
    return consumable;
  }

  public void setConsumable(Boolean consumable) {
    this.consumable = consumable;
  }

  public Boolean getFreePass() {
    return freePass;
  }

  public void setFreePass(Boolean freePass) {
    this.freePass = freePass;
  }

  public String getDefaultLocation() {
    return defaultLocation;
  }

  public void setDefaultLocation(String defaultLocation) {
    this.defaultLocation = defaultLocation;
  }

  public String getDefaultBin() {
    return defaultBin;
  }

  public void setDefaultBin(String defaultBin) {
    this.defaultBin = defaultBin;
  }

  public String getRaggruppamentoArticolo() {
    return raggruppamentoArticolo;
  }

  public void setRaggruppamentoArticolo(String raggruppamentoArticolo) {
    this.raggruppamentoArticolo = raggruppamentoArticolo;
  }

  public String getCountryRegionOfOriginCode() {
    return countryRegionOfOriginCode;
  }

  public void setCountryRegionOfOriginCode(String countryRegionOfOriginCode) {
    this.countryRegionOfOriginCode = countryRegionOfOriginCode;
  }

  public String getValuta() {
    return valuta;
  }

  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

  public BigDecimal getPercentualeIva() {
    return percentualeIva;
  }

  public void setPercentualeIva(BigDecimal percentualeIva) {
    this.percentualeIva = percentualeIva;
  }
}
