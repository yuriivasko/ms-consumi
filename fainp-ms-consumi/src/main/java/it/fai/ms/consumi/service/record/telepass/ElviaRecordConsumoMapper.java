package it.fai.ms.consumi.service.record.telepass;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.ESE_TOT_STATUS;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class ElviaRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<ElviaRecord, Elvia> {

  /*
   * (non-Javadoc)
   * @see it.fai.ms.consumi.service.record.telepass.RecordConsumoMapper#mapRecordToConsumo(it.fai.ms.consumi.repository.
   * flussi.record.model.CommonRecord)
   */
  @Override
  public Elvia mapRecordToConsumo(ElviaRecord commonRecord) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyy_MM_dd_dateformat, null);
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, TelepassSourceName.ELVIA);
    source.setFileName(commonRecord.getFileName());

    source.setRowNumber(commonRecord.getRowNumber());
    // TODO costruire gid e settare type internal
    Elvia toConsumo = new Elvia(source, commonRecord.getRecordCode(), Optional.of(new ElviaGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);

    Consumo toConsumoBase = toConsumo;
    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();
    transaction.setDetailCode(commonRecord.getTypeMovement());
    toConsumoBase.setTransaction(transaction);
    toConsumoBase.setSupplierInvoiceDocument(null); //non presente in questo flusso
    if (commonRecord.getSubContractCode() != null && !commonRecord.getSubContractCode()
                                                                  .trim()
                                                                  .isEmpty()) {
      Contract contract = new Contract(commonRecord.getSubContractCode());
      toConsumoBase.setContract(Optional.of(contract));
    }
    // CONSUMO.AMOUNT
    Amount amount = new Amount();
    MonetaryAmount amountIncluededVat = null;
    amount.setExchangeRate(null); // Valore di default --> non viene usato
    MonetaryAmount totalAmount = null;
    amount.setVatRate(commonRecord.getVatRateBigDecimal());
    MonetaryAmount amountNotSubjectedToVat = commonRecord.getAmount_not_subjected_to_vat();

    //    totalAmount = commonRecord.getTotal_amount_value();
    //    modificato uso del discount: e' la cifra pagata (IVA inclusa) al netto degli sconti
    totalAmount = commonRecord.getDiscount_value();

    amountIncluededVat = totalAmount.subtract(amountNotSubjectedToVat);
    amount.setAmountIncludedVat(amountIncluededVat); //questo gia' calcola excludedvat
    ESE_TOT_STATUS ese_tot_evaluated = null;
    if (!amountNotSubjectedToVat.isZero()) {
      ese_tot_evaluated = ESE_TOT_STATUS.ESE;
      Amount eseAmount = new Amount();
      eseAmount.setVatRate(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP));
      eseAmount.setAmountExcludedVat(commonRecord.getAmount_not_subjected_to_vat());
      toConsumo.setEseAmount(eseAmount);
      toConsumo.setEseGlobalIdentifier(commonRecord.getGlobalIdentifierAsEse());
    } else {
      ese_tot_evaluated = ESE_TOT_STATUS.TOT;
    }
    // commonRecord.getDiscount_value()
    toConsumoBase.setAmount(amount);

    // CONSUMO altri campi
    ConsumoPedaggio toConsumoPedaggio = toConsumo;
    toConsumoPedaggio.setProcessed(commonRecord.isProcessed());
    toConsumoPedaggio.setEseTotStatus(ese_tot_evaluated);

    // CONSUMOPEDAGGIO.DEVICE
    // //CONSUMOPEDAGGIO.VEHICLE
    ElviaRecord elviaRecord = commonRecord;

    Vehicle vehicle = new Vehicle();
    vehicle.setFareClass(elviaRecord.getTollClass());

    Device device  = null;
    final String elviaRecordTypeSupport = elviaRecord.getTypeSupport();
    final Optional<TipoDispositivoEnum> tipoDispositivoByTypeOfSupport = TelepassDeviceType.getTipoDispositivoByTypeOfSupport(elviaRecordTypeSupport);

    if (isLicencePlateTypeOfMovement(elviaRecord)){
      //when movemenet is isLicencePlateTypeOfMovement the support code is license plate id
      final String licenseId = elviaRecord.getRawSupportCode();
      final VehicleLicensePlate licensePlate = new VehicleLicensePlate(licenseId, VehicleLicensePlate.NO_COUNTRY);
      vehicle.setLicensePlate(licensePlate);
      vehicle.setFareClass(elviaRecord.getTollClass());
      //      if (tipoDispositivoByTypeOfSupport.isPresent()) {
      //        device = new Device(tipoDispositivoByTypeOfSupport.get());
      //      }
    } else {

      String supportCode = elviaRecord.getSupportCode();
      
      if (tipoDispositivoByTypeOfSupport.isPresent()) {
        switch(tipoDispositivoByTypeOfSupport.get()) {
          case TELEPASS_EUROPEO:
          case TELEPASS_EUROPEO_SAT:
            device = new Device(tipoDispositivoByTypeOfSupport.get());
            device.setPan(supportCode);
            break;
          case VIACARD:
            //trim a 9 caratteri
            supportCode = StringUtils.right(supportCode, 9);
            device = new Device(supportCode, tipoDispositivoByTypeOfSupport.get());
            break;
          default:
            device = new Device(supportCode, tipoDispositivoByTypeOfSupport.get());
        }
        device.setServiceType(TipoServizioEnum.PEDAGGI_ITALIA); // E' l'unico servizio che arriva su questo flusso per i telepass europei e europei sat
      }
    }
    toConsumoPedaggio.setVehicle(vehicle);
    toConsumoPedaggio.setDevice(device);

    //
    // //CONSUMOPEDAGGIO.TOLLPOINTENTRY
    TollPoint tollpointEntry = new TollPoint();
    GlobalGate globalGateEntry = new GlobalGate(elviaRecord.getEntryGateCode());
    tollpointEntry.setGlobalGate(globalGateEntry);
    if (StringUtils.isNotBlank(elviaRecord.getMovementDescription())) {
      globalGateEntry.setDescription(StringUtils.left(elviaRecord.getMovementDescription(), 13).trim());
    }
    toConsumoPedaggio.setTollPointEntry(tollpointEntry);
    //
    // //CONSUMOPEDAGGIO.TOLLPOINTEXIT
    TollPoint tollpointExit = new TollPoint();
    GlobalGate globalGateExit = new GlobalGate(elviaRecord.getExitGateCode());
    tollpointExit.setGlobalGate(globalGateExit);
    if (StringUtils.isNotBlank(elviaRecord.getMovementDescription())) {
      globalGateExit.setDescription(StringUtils.mid(elviaRecord.getMovementDescription(), 13, 13).trim());
    }
    if (elviaRecord.getDateMovement() != null && elviaRecord.getHourMovement() != null) {
      tollpointExit.setTime(calculateInstantByDateAndTime(elviaRecord.getDateMovement().replace("-", ""), elviaRecord.getHourMovement().replace(".", ""), yyyyMMdd_dateformat,
                                                          HHmmss_timeformat).get());
    }
    toConsumoPedaggio.setTollPointExit(tollpointExit);

    // //CONSUMOPEDAGGIO. altri campi
    // //toConsumoPedaggio.setTotalAmount(totalAmount);
    toConsumoPedaggio.setTollGate(elviaRecord.getExitGateCode());
    toConsumoPedaggio.setNetworkCode(elviaRecord.getNetworkCode());
    toConsumoPedaggio.setRoute(elviaRecord.getMovementDescription());
    toConsumoPedaggio.setExitTransitDateTime(elviaRecord.getDateMovement() + elviaRecord.getHourMovement());
    toConsumoPedaggio.getTransaction()
                     .setSign(elviaRecord.getSignOfTheAmount());
    // //toConsumoPedaggio.setImponibileAdr(imponibileAdr); --> non applicabile

    return toConsumo;
  }

  private boolean isLicencePlateTypeOfMovement(ElviaRecord elviaRecord) {
    return TelepassDeviceType.PLATE.equals(elviaRecord.getTypeSupport());
  }


}
