package it.fai.ms.consumi.service.processor.consumi.trx;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.generici.trackycard.TrackyCardGenerico;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.processor.ProcessingResult;

public interface TrxNoOilProcessor {

  ProcessingResult validateAndProcess(StanziamentiParams stanziamentiParams, TrackyCardGenerico trackyCardGenerico, Bucket bucket);
}
