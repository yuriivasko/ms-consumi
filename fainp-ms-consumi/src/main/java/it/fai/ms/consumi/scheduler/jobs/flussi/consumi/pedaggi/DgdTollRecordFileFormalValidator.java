package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import static it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.DgdTollRecordDescriptor.BEGIN_N;
import static it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.DgdTollRecordDescriptor.FOOTER_CODE_BEGIN;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.money.MonetaryAmount;

import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.RecordUtils;
import it.fai.ms.consumi.service.validator.CardNumberValidityService;
import it.fai.ms.consumi.service.validator.RecordFileFormalValidator;

@Service
public class DgdTollRecordFileFormalValidator
  implements RecordFileFormalValidator<DgdTollRecord> {

  private final CardNumberValidityService cardNumberValidityService;
  private final DgdTollRecordDescriptor   recordDescriptor;

  public DgdTollRecordFileFormalValidator(CardNumberValidityService cardNumberValidityService, DgdTollRecordDescriptor recordDescriptor) {
    this.cardNumberValidityService = cardNumberValidityService;
    this.recordDescriptor = recordDescriptor;
  }

  public RecordsFileInfo validateFile(RecordsFileInfo recordsFileInfo) {
      try {
        AtomicLong index = new AtomicLong(1);
        final String filename = recordsFileInfo.getFilename();
        LinkedList<DgdTollRecord> rows = Files.lines(Paths.get(new File(filename).getPath()),  Charset.forName(AbstractJob.ISO_8859_1))
                                                    .map(line -> validateRecordFormally(filename, line, index.incrementAndGet()))
                                                    .collect(Collectors.toCollection(() -> new LinkedList<>()));

        //05 - il numero di sequenza non è progressivo - generare anomalia, NON acquisire flusso
        //IGNORIAMO IL CONTROLLO

        Set<DgdTollRecord> recordSet = new HashSet<>(rows);
        //08 - Doppio record nello schedario	controllo solo all'interno del file stesso. generare anomalia, NON acquisire flusso
        if (rows.size() > recordSet.size()){
          throw new RecordUtils.NotValidFileException(String.format("Duplicated row found in file!"));
        }

        //12 - Più di un trailer - generare anomalia, NON acquisire flusso
        List<DgdTollRecord> footers = rows.stream()
                                                .filter(recordPair -> FOOTER_CODE_BEGIN.equals(recordPair.getRecordCode()))
                                                .collect(Collectors.toList());
        if (footers.size() != 1){
          throw new RecordUtils.NotValidFileException(String.format("No footer or more than one footer found!!"));
        }

        MonetaryAmount totalRowsAmount = rows.stream()
                                             .limit(rows.size() - 1)
                                             .map(dgdTollRecord -> dgdTollRecord.getAmountIncludedVat())
                                             .reduce(MonetaryUtils.strToMonetary("0", "CHF"), MonetaryAmount::add);

        //07 - il totale complessivo dello schedario è errato - generare anomalia, NON acquisire flusso
        if (!totalRowsAmount.equals(footers.get(0).getTotalAmountVatIncluded())){
          throw new RecordUtils.NotValidFileException(String.format("Total rows amount != total footer amount!"));
        }
      } catch (Exception e) {
        recordsFileInfo.addMessage("FCJOB-500", e.getMessage());
      }
      return recordsFileInfo;
  }

  private DgdTollRecord validateRecordFormally(String fileName, String line, long index) {
    DgdTollRecord dgdTollRecord = recordDescriptor.decodeRecordCodeAndCallSetFromString(line, StringUtils.left(line, 1), fileName, index);
    if (BEGIN_N.equals(dgdTollRecord.getRecordCode())) {
      //il parser non ha ricevuto una riga di dettaglio!!
      if (StringUtils.isBlank(dgdTollRecord.getRecordCode())){
        throw new RecordUtils.NotValidFileException(String.format("Parsed a record not starting with N/S/T! line:=[%s]",  line));
      }

      //01  -data o ora non valida - non controllare/ignorare

      //02 - numero della carta errato (cifra di controllo) - non controllare/ignorare (incluso nel controllo successivo)
      //03 - la carta non è di FAI- generare anomalia.NON acquisire flusso
      if (!"789697".equals(StringUtils.left(dgdTollRecord.getCardNumber(), 6))){
        throw new RecordUtils.NotValidFileException(String.format("Card number not FAI! line:=[%s]", line));
      }

      if (!cardNumberValidityService.hasValidCheckingDigit(dgdTollRecord.getCardNumber())){
        throw new RecordUtils.NotValidFileException(String.format("Checking digit is wrong in card number! line:=[%s]", line));
      }

      //04 - manca il dato trailer (record del totale) - generare anomalia, NON acquisire flusso
      //fatto nella superclasse

      //06 - la quantità dei dati è errata (Trailer dello schedario) - generare anomalia, NON acquisire flusso
      //fatto nella superclasse

      //09 - Netto + IVA ineguale lordo - generare anomalia, NON acquisire flusso
      final MonetaryAmount taxableAmount = dgdTollRecord.getAmountExcludedVat();
      final MonetaryAmount vatAmount = dgdTollRecord.getVatAmount();
      final MonetaryAmount vatIncludedAmount = dgdTollRecord.getAmountIncludedVat();

      if (!taxableAmount.add(vatAmount).equals(vatIncludedAmount)){
        throw new RecordUtils.NotValidFileException(String.format("Taxable amount + vat amount != total amount! line:=[%s]", line));
      }

      //10 - valuta diversa CHF - generare anomalia, NON acquisire flusso
      if (!"CHF".equals(dgdTollRecord.getCurrency())){
        throw new RecordUtils.NotValidFileException(String.format("Currency not CHF! line:=[%s]", line));
      }
    }
    return dgdTollRecord;
  }
}
