package it.fai.ms.consumi.repository.flussi.record;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;
import java.io.IOException;

public class MoneyDeserializer
  extends JsonDeserializer<MonetaryAmount> {

  public MoneyDeserializer() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public MonetaryAmount deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    JsonNode node = jp.getCodec().readTree(jp);
    String valueAsString = node.get("value").asText();
//    var valueAsString = jsonParser.getValueAsString("value");
    if(valueAsString == null)
      return null;
    return Money.parse((CharSequence) valueAsString);
  }

}
