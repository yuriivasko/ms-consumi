package it.fai.ms.consumi.dto;

import it.fai.ms.consumi.domain.AsyncJobResponse;
import it.fai.ms.consumi.domain.AsyncJobStatusEnum;

public class AsyncJobResponseDto {
	private Long id;

	private AsyncJobStatusEnum state;

	private AsyncJobResponseDataDto data;
	
	public AsyncJobResponseDto(AsyncJobResponse response) {
		id = response.getId();
		state = response.getStatus();
		data = new AsyncJobResponseDataDto(response.getResultData());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AsyncJobStatusEnum getState() {
		return state;
	}

	public void setState(AsyncJobStatusEnum state) {
		this.state = state;
	}

	public AsyncJobResponseDataDto getData() {
		return data;
	}

	public void setData(AsyncJobResponseDataDto data) {
		this.data = data;
	}
	
	
}
