package it.fai.ms.consumi.client;

import it.fai.ms.consumi.domain.navision.StanziamentoError;

public class StanziamentiPostException extends Exception {
  private static final long serialVersionUID = -3125442924721030179L;
 
  public StanziamentiPostException(StanziamentoError stanziamentoError, String errorMessage) {
    this.stanziamentoError = stanziamentoError;
    this.errorMessage = errorMessage;
  }
 
  public StanziamentoError getStanziamentoError() {
    return stanziamentoError;
  }

  private final StanziamentoError stanziamentoError;
  private final String errorMessage;

  @Override
  public String getMessage() {
    return stanziamentoError.toString()+ " \n "+errorMessage;
  }
}
