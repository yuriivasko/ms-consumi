package it.fai.ms.consumi.service.record;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.jms.notification_v2.ConsumoReprocessableNotification;
import it.fai.ms.consumi.job.JobRegistry;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.service.processor.SingleRecordProcessingResult;

@Service
public class RecordConsumoReprocessorImpl
  implements RecordConsumoReprocessor {

  private static final Logger log = LoggerFactory.getLogger(RecordConsumoReprocessorImpl.class);

  private final JobRegistry jobRegistry;
  private final NotificationService notificationService;

  public RecordConsumoReprocessorImpl(final JobRegistry jobRegistry, final NotificationService notificationService) {
    this.jobRegistry = jobRegistry;
    this.notificationService = notificationService;
  }

  @Override
  public <T extends CommonRecord, K extends Consumo> SingleRecordProcessingResult processSingleRecord(T foundRecord){
    log.debug("Called processSingleRecord with:=[{}]", foundRecord);
    ServicePartner servicePartner = foundRecord.getServicePartner();

    AbstractJob<T, K> abstractJob = retrieveJob(servicePartner);
    Map<String, Stanziamento> stanziamentiDaSpedireANAV = new HashMap<>();
    SingleRecordProcessingResult singleProcessingResult;
    try {

      log.info("Clearing all caches");
      abstractJob.allCacheEviction();

      Bucket bucket = new Bucket("");

      singleProcessingResult = abstractJob.processSingleRecord(servicePartner, bucket, stanziamentiDaSpedireANAV, foundRecord);
      abstractJob.sendComputedStanziamenti(stanziamentiDaSpedireANAV);
      if(singleProcessingResult.getConsumoNoBlockingData()!=null)
        abstractJob.checkNoBlockingValidation(Arrays.asList(singleProcessingResult.getConsumoNoBlockingData()), foundRecord.getFileName() ,  foundRecord.getIngestion_time().toString(), servicePartner.toString());

      abstractJob.checkNoBlockingValidationOnStanziamento(stanziamentiDaSpedireANAV);
    } catch (Exception e) {
      log.error("Unhandled excepion (won't be rethrown) when processing single record : {}", e);
//      sendNotification(abstractJob.getSource(), "FCJOB-500", e.getMessage());
      String source = abstractJob.getSource();
//      notificationService.notify(new ConsumoNotReprocessableNotification(
//        source,
//        foundRecord.getFileName(),
//        "FCJOB-500",
//        e.getMessage()
//      ));
      notificationService.notify(new ConsumoReprocessableNotification(
        source,
        foundRecord.getFileName(),
        foundRecord.getRowNumber(),
        foundRecord.getClass().getSimpleName(),
        foundRecord.getUuid(),
        foundRecord.getRecordCode(),
        foundRecord.getTransactionDetail(),
        foundRecord.toNotificationMessage(),
        "FCJOB-200",
        e.getMessage()
      ));
      singleProcessingResult = new SingleRecordProcessingResult(e);
    }
    return singleProcessingResult;
  }

  private <T extends CommonRecord, K extends Consumo> AbstractJob<T, K> retrieveJob(ServicePartner servicePartner) {
    Optional<AbstractJob<? extends CommonRecord, ? extends Consumo>> a = jobRegistry.findJobByServiceProviderFormat(servicePartner.getFormat());
    if (a.isPresent()){
      return (AbstractJob<T, K>) a.get();
    }
    throw new IllegalStateException("Cannot find Job for current record!");
  }

//  private void sendNotification(String jobName, String messageCode, String messageText) {
//    var errorMessage = new HashMap<String, Object>();
//    errorMessage.put("job_name", jobName);
//    errorMessage.put("original_message", messageText);
//    notificationService.notify(messageCode, errorMessage);
//  }

}
