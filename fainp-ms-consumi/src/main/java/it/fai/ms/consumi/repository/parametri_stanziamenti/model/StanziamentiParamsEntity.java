package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Currency;
import java.util.Optional;

import static javax.persistence.EnumType.STRING;

@Entity
@Table(name = "parametri_stanziamenti")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class StanziamentiParamsEntity {

  @Embedded
  private StanziamentiParamsArticleEntity article;

  @Enumerated(STRING)
  @Column(name = "COSTO_RICAVO", nullable = false)
  private StanziamentiParamsCostoRicavoEntity costoRicavo;

  @Column(name = "PAESE", nullable = false)
  @NotBlank
  private String country;

  @Column(name = "VALUTA", nullable = false)
  @NotNull
  private Currency currency;

  @Column(name = "CLASSE_TARIFFARIA")
  private String fareClass;

  @Column(name = "RAGGRUPPAMENTO", nullable = false)
  @NotBlank
  private String grouping;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "UNITA_DI_MISURA", nullable = true)
  private String measurementUnit;

  @Column(name = "RECORD_CODE", nullable = false)
  @NotBlank
  private String recordCode = "DEFAULT";

  @Column(name = "TRACCIATO", nullable = false)
  @NotBlank
  private String source;

  @Enumerated(STRING)
  @Column(name = "TIPO_DETTAGLI_STANZIAMENTO", nullable = false)
  private StanziamentiDetailsTypeEntity stanziamentiDetailsType;

  @Embedded
  private StanziamentiParamsSupplierEntity supplier;

  @Column(name = "TRANSACTION_DETAIL", nullable = true)
  private String transactionDetail;

  @Column(name = "TIPO_TRANSAZIONE")
  private String transactionType;

  @Column(name = "IVA", nullable = false)
  private float vatRate = 0.0f;

  public StanziamentiParamsEntity() {
  }

  public StanziamentiParamsArticleEntity getArticle() {
    return article;
  }

  public StanziamentiParamsEntity(final StanziamentiParamsArticleEntity _article) {
    article = _article;
  }

  public StanziamentiParamsCostoRicavoEntity getCostoRicavo() {
    return costoRicavo;
  }

  public String getCountry() {
    return country;
  }

  public Currency getCurrency() {
    return currency;
  }

  public String getFareClass() {
    return fareClass;
  }

  public String getGrouping() {
    return grouping;
  }

  public String getMeasurementUnit() {
    return measurementUnit;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public String getSource() {
    return source;
  }

  public StanziamentiDetailsTypeEntity getStanziamentiDetailsType() {
    return stanziamentiDetailsType;
  }

  public StanziamentiParamsSupplierEntity getSupplier() {
    return supplier;
  }

  public String getTransactionDetail() {
    return transactionDetail;
  }

  public String getTransactionType() {
    return transactionType;
  }

  public float getVatRate() {
    return vatRate;
  }

  public void setCostoRicavo(final StanziamentiParamsCostoRicavoEntity _costoRicavo) {
    costoRicavo = _costoRicavo;
  }

  public void setCountry(String _country) {
    country = _country;
  }

  public void setCurrency(final Currency _currency) {
    currency = _currency;
  }

  public void setFareClass(final String _fareClass) {
    fareClass = _fareClass;
  }

  public void setGrouping(final String _grouping) {
    grouping = _grouping;
  }

  public void setMeasurementUnit(final String _measurementUnit) {
    measurementUnit = _measurementUnit;
  }

  public void setRecordCode(final String _recordCode) {
    recordCode = Optional.ofNullable(_recordCode)
                         .orElse("DEFAULT");
  }

  public void setSource(final String _source) {
    source = _source;
  }

  public void setStanziamentiDetailsType(StanziamentiDetailsTypeEntity _stanziamentiDetailsType) {
    stanziamentiDetailsType = _stanziamentiDetailsType;
  }

  public void setSupplier(final StanziamentiParamsSupplierEntity _supplier) {
    supplier = _supplier;
  }

  public void setTransactionDetail(final String _transactionDetail) {
    transactionDetail = _transactionDetail;
  }

  public void setTransactionType(final String _transactionType) {
    transactionType = _transactionType;
  }

  public void setVatRate(final float _vatRate) {
    vatRate = _vatRate;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("id=")
                              .append(id)
                              .append(",")
                              .append(article)
                              .append(",")
                              .append(supplier)
                              .append(",")
                              .append(currency)
                              .append(",fareClass=")
                              .append(fareClass)
                              .append(",source=")
                              .append(source)
                              .append(",transactionDetail=")
                              .append(transactionDetail)
                              .append(",transactionType=")
                              .append(transactionType)
                              .append(",recordCode=")
                              .append(recordCode)
                              .append(",costoRicavo=")
                              .append(costoRicavo)
                              .append(",vatRate=")
                              .append(vatRate)
                              .append(",grouping=")
                              .append(grouping)
                              .append(",measurementUnit=")
                              .append(measurementUnit)
                              .append(",stanziamentiDetailsType=")
                              .append(stanziamentiDetailsType)
                              .append("]")
                              .toString();
  }

}
