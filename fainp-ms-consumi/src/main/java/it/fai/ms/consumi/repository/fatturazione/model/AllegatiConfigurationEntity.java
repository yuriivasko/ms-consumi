package it.fai.ms.consumi.repository.fatturazione.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;

@Entity
@Table(name = "allegati_configuration")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AllegatiConfigurationEntity {

  @Id
  @Column(name = "codice_raggruppamento_articolo")
  private String codiceRaggruppamentoArticolo;

  @Column(name = "descrizione")
  private String descrizione;

  @Column(name = "trans_code_allegato_fix")
  private String transCodeAllegatoFix;

  @Column(name = "genera_allegato")
  private boolean generaAllegato;

  @Enumerated(EnumType.STRING)
  @Column(name = "tabella_dettaglio_stanziamento")
  private DettaglioStanziamentoType tipoDettaglioStanziamento;
  
  @Enumerated(EnumType.STRING)
  @Column(name = "template")
  private TemplateAllegati template;

  public String getCodiceRaggruppamentoArticolo() {
    return codiceRaggruppamentoArticolo;
  }

  public void setCodiceRaggruppamentoArticolo(String codiceRaggruppamentoArticolo) {
    this.codiceRaggruppamentoArticolo = codiceRaggruppamentoArticolo;
  }

  public String getDescrizione() {
    return descrizione;
  }

  public void setDescrizione(String descrizione) {
    this.descrizione = descrizione;
  }

  public String getTransCodeAllegatoFix() {
    return transCodeAllegatoFix;
  }

  public void setTransCodeAllegatoFix(String tipoServizio) {
    this.transCodeAllegatoFix = tipoServizio;
  }

  public boolean isGeneraAllegato() {
    return generaAllegato;
  }

  public void setGeneraAllegato(boolean generaAllegato) {
    this.generaAllegato = generaAllegato;
  }

  public DettaglioStanziamentoType getTipoDettaglioStanziamento() {
    return tipoDettaglioStanziamento;
  }

  public void setTipoDettaglioStanziamento(DettaglioStanziamentoType dettaglioStanziamento) {
    this.tipoDettaglioStanziamento = dettaglioStanziamento;
  }

  public TemplateAllegati getTemplate() {
    return template;
  }

  public void setTemplate(TemplateAllegati template) {
    this.template = template;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("AllegatiConfigurationEntity [codiceRaggruppamentoArticolo=");
    builder.append(codiceRaggruppamentoArticolo);
    builder.append(", descrizione=");
    builder.append(descrizione);
    builder.append(", transCodeAllegatoFix=");
    builder.append(transCodeAllegatoFix);
    builder.append(", generaAllegato=");
    builder.append(generaAllegato);
    builder.append(", tipoDettaglioStanziamento=");
    builder.append(tipoDettaglioStanziamento);
    builder.append(", template=");
    builder.append(template);
    builder.append("]");
    return builder.toString();
  }

}
