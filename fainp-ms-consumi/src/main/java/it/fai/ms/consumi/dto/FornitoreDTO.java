package it.fai.ms.consumi.dto;

public class FornitoreDTO {

  protected String key;
  protected String no;
  protected String name;
  protected String name2;
  protected String classificazioneFornitori;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getNo() {
    return no;
  }

  public void setNo(String no) {
    this.no = no;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName2() {
    return name2;
  }

  public void setName2(String name2) {
    this.name2 = name2;
  }

  public String getClassificazioneFornitori() {
    return classificazioneFornitori;
  }

  public void setClassificazioneFornitori(String classificazioneFornitori) {
    this.classificazioneFornitori = classificazioneFornitori;
  }
}
