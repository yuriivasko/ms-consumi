package it.fai.ms.consumi.client.efservice.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for the ClienteFai entity.
 */
public class ClienteFaiDTO implements Serializable {

  private Long id;

  @NotNull
  private String codiceCliente;

  private String email;

  private StatoAzienda stato;

  private Long numeroAccount;

  private String ragioneSociale;

  private String codiceFiscale;

  private String partitaIva;

  private String codiceAgente;

  private String raggruppamentoImpresa;

  private String descrizioneRaggruppamentoImpresa;

  private Boolean consorzioPadre;

  @NotNull
  private String via;

  @NotNull
  private String citta;

  private String cap;

  private String provincia;

  @Size(max = 2)
  private String paese;

  private String emailLegaleRappresentante;

  private String nomeLegaleRappresentante;

  private String cognomeLegaleRappresentante;

  private String contattoTelefonicoLegaleRappresentante;

  private String codiceClienteFatturazione;

  @NotNull
  private Instant dmlRevisionTimestamp;

  private String nomeIntestazioneTessere;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public StatoAzienda getStato() {
    return stato;
  }

  public void setStato(StatoAzienda stato) {
    this.stato = stato;
  }

  public Long getNumeroAccount() {
    return numeroAccount;
  }

  public void setNumeroAccount(Long numeroAccount) {
    this.numeroAccount = numeroAccount;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getCodiceFiscale() {
    return codiceFiscale;
  }

  public void setCodiceFiscale(String codiceFiscale) {
    this.codiceFiscale = codiceFiscale;
  }

  public String getPartitaIva() {
    return partitaIva;
  }

  public void setPartitaIva(String partitaIva) {
    this.partitaIva = partitaIva;
  }

  public String getCodiceAgente() {
    return codiceAgente;
  }

  public void setCodiceAgente(String codiceAgente) {
    this.codiceAgente = codiceAgente;
  }

  public String getRaggruppamentoImpresa() {
    return raggruppamentoImpresa;
  }

  public void setRaggruppamentoImpresa(String raggruppamentoImpresa) {
    this.raggruppamentoImpresa = raggruppamentoImpresa;
  }

  public String getDescrizioneRaggruppamentoImpresa() {
    return descrizioneRaggruppamentoImpresa;
  }

  public void setDescrizioneRaggruppamentoImpresa(String descrizioneRaggruppamentoImpresa) {
    this.descrizioneRaggruppamentoImpresa = descrizioneRaggruppamentoImpresa;
  }

  public Boolean isConsorzioPadre() {
    return consorzioPadre;
  }

  public void setConsorzioPadre(Boolean consorzioPadre) {
    this.consorzioPadre = consorzioPadre;
  }

  public String getVia() {
    return via;
  }

  public void setVia(String via) {
    this.via = via;
  }

  public String getCitta() {
    return citta;
  }

  public void setCitta(String citta) {
    this.citta = citta;
  }

  public String getCap() {
    return cap;
  }

  public void setCap(String cap) {
    this.cap = cap;
  }

  public String getProvincia() {
    return provincia;
  }

  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  public String getPaese() {
    return paese;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public String getEmailLegaleRappresentante() {
    return emailLegaleRappresentante;
  }

  public void setEmailLegaleRappresentante(String emailLegaleRappresentante) {
    this.emailLegaleRappresentante = emailLegaleRappresentante;
  }

  public String getNomeLegaleRappresentante() {
    return nomeLegaleRappresentante;
  }

  public void setNomeLegaleRappresentante(String nomeLegaleRappresentante) {
    this.nomeLegaleRappresentante = nomeLegaleRappresentante;
  }

  public String getCognomeLegaleRappresentante() {
    return cognomeLegaleRappresentante;
  }

  public void setCognomeLegaleRappresentante(String cognomeLegaleRappresentante) {
    this.cognomeLegaleRappresentante = cognomeLegaleRappresentante;
  }

  public String getContattoTelefonicoLegaleRappresentante() {
    return contattoTelefonicoLegaleRappresentante;
  }

  public void setContattoTelefonicoLegaleRappresentante(String contattoTelefonicoLegaleRappresentante) {
    this.contattoTelefonicoLegaleRappresentante = contattoTelefonicoLegaleRappresentante;
  }

  public String getCodiceClienteFatturazione() {
    return codiceClienteFatturazione;
  }

  public void setCodiceClienteFatturazione(String codiceClienteFatturazione) {
    this.codiceClienteFatturazione = codiceClienteFatturazione;
  }

  public Instant getDmlRevisionTimestamp() {
    return dmlRevisionTimestamp;
  }

  public void setDmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
  }

  public String getNomeIntestazioneTessere() {
    return nomeIntestazioneTessere;
  }

  public void setNomeIntestazioneTessere(String nomeInstestazioneTessere) {
    this.nomeIntestazioneTessere = nomeInstestazioneTessere;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    ClienteFaiDTO clienteFaiDTO = (ClienteFaiDTO) o;
    if (clienteFaiDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), clienteFaiDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "ClienteFaiDTO{" + "id=" + getId() + ", codiceCliente='" + getCodiceCliente() + "'" + ", email='" + getEmail() + "'"
           + ", stato='" + getStato() + "'" + ", numeroAccount=" + getNumeroAccount() + ", ragioneSociale='" + getRagioneSociale() + "'"
           + ", codiceFiscale='" + getCodiceFiscale() + "'" + ", partitaIva='" + getPartitaIva() + "'" + ", codiceAgente='"
           + getCodiceAgente() + "'" + ", raggruppamentoImpresa='" + getRaggruppamentoImpresa() + "'"
           + ", descrizioneRaggruppamentoImpresa='" + getDescrizioneRaggruppamentoImpresa() + "'" + ", consorzioPadre='"
           + isConsorzioPadre() + "'" + ", via='" + getVia() + "'" + ", citta='" + getCitta() + "'" + ", cap='" + getCap() + "'"
           + ", provincia='" + getProvincia() + "'" + ", paese='" + getPaese() + "'" + ", emailLegaleRappresentante='"
           + getEmailLegaleRappresentante() + "'" + ", nomeLegaleRappresentante='" + getNomeLegaleRappresentante() + "'"
           + ", cognomeLegaleRappresentante='" + getCognomeLegaleRappresentante() + "'" + ", contattoTelefonicoLegaleRappresentante='"
           + getContattoTelefonicoLegaleRappresentante() + "'" + ", codiceClienteFatturazione='" + getCodiceClienteFatturazione() + "'"
           + ", dmlRevisionTimestamp='" + getDmlRevisionTimestamp() + "'" + ", nomeIntestazioneTessere='" + getNomeIntestazioneTessere()
           + "'" + "}";
  }
}
