package it.fai.ms.consumi.repository.flussi.record.model.carburanti;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public  class TokheimRecord0201 extends TokheimRecord{

  private static final long serialVersionUID = 1L;

  private String fileNumber            ;
//  String filler1               ;
  private String NrProgressivo         ;
//  String filler2               ;
  private String nomePoint             ;
  private String cittaPoint            ;
  private String capPoint              ;
  private String indirizzoPoint        ;
  private String codiceEsternoPoint    ;
//  String empty                 ;

  @JsonCreator
  public TokheimRecord0201(@JsonProperty("fileName") String fileName, @JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }

  @Override
  public boolean toBeSkipped() {
    return true;
  }

  @Override
  public int getNestedLevel() {
    return 0;
  }

  public String getFileNumber() {
    return fileNumber;
  }

  public void setFileNumber(String fileNumber) {
    this.fileNumber = fileNumber;
  }

  public String getNrProgressivo() {
    return NrProgressivo;
  }

  public void setNrProgressivo(String nrProgressivo) {
    NrProgressivo = nrProgressivo;
  }

  public String getNomePoint() {
    return nomePoint;
  }

  public void setNomePoint(String nomePoint) {
    this.nomePoint = nomePoint;
  }

  public String getCittaPoint() {
    return cittaPoint;
  }

  public void setCittaPoint(String cittaPoint) {
    this.cittaPoint = cittaPoint;
  }

  public String getCapPoint() {
    return capPoint;
  }

  public void setCapPoint(String capPoint) {
    this.capPoint = capPoint;
  }

  public String getIndirizzoPoint() {
    return indirizzoPoint;
  }

  public void setIndirizzoPoint(String indirizzoPoint) {
    this.indirizzoPoint = indirizzoPoint;
  }

  public String getCodiceEsternoPoint() {
    return codiceEsternoPoint;
  }

  public void setCodiceEsternoPoint(String codiceEsternoPoint) {
    this.codiceEsternoPoint = codiceEsternoPoint;
  }


}
