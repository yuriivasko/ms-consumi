//package it.fai.ms.consumi.domain.validation;
//
//import java.util.LinkedList;
//import java.util.function.Supplier;
//
//public class MessageTextAppender {
//
//  private Message            message;
//  private LinkedList<String> stringList;
//
//  public MessageTextAppender(Message message) {
//    this.message = message;
//    this.stringList = new LinkedList<>();
//  }
//
//  public Message done(){
//    return message.withText(String.join(" ", stringList));
//  }
//
//  public MessageTextAppender appendIf(Supplier<Boolean> condition, String string) {
//    if (condition.get()) {
//      this.append(string);
//    }
//    return this;
//  }
//
//  public MessageTextAppender append(String string) {
//    stringList.add(string);
//    return this;
//  }
//
//}
