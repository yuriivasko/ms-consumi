package it.fai.ms.consumi.config;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class NotConfirmedAllocationsConfig {
    private Integer defaultExpiration;

    private List<ArticleGroupExpiration> articleGroupExpirations = new ArrayList<>();


    public List<ArticleGroupExpiration> getArticleGroupExpirations() {
        return articleGroupExpirations;
    }

    public void setArticleGroupExpirations(List<ArticleGroupExpiration> articleGroupExpirations) {
        this.articleGroupExpirations = articleGroupExpirations;
    }

    public Integer getDefaultExpiration() {
        return defaultExpiration;
    }

    public void setDefaultExpiration(Integer defaultExpiration) {
        this.defaultExpiration = defaultExpiration;
    }

    public static class ArticleGroupExpiration {

        private String articleGroup;
        private Integer expiration;
        private String sourceType;

        public ArticleGroupExpiration() {
        }

        public ArticleGroupExpiration(String articleGroup, Integer expiration, String sourceType) {
            this.articleGroup = articleGroup;
            this.expiration = expiration;
            this.sourceType = sourceType;
        }

        public String getSourceType() {
            return sourceType;
        }

        public void setSourceType(String sourceType) {
            this.sourceType = sourceType;
        }

        public String getArticleGroup() {
            return articleGroup;
        }

        public void setArticleGroup(String articleGroup) {
            this.articleGroup = articleGroup;
        }

        public Integer getExpiration() {
            return expiration;
        }

        public void setExpiration(Integer expiration) {
            this.expiration = expiration;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ArticleGroupExpiration that = (ArticleGroupExpiration) o;
            return Objects.equals(articleGroup, that.articleGroup) &&
                    Objects.equals(expiration, that.expiration) &&
                    Objects.equals(sourceType, that.sourceType);
        }

        @Override
        public int hashCode() {
            return Objects.hash(articleGroup, expiration, sourceType);
        }

        @Override
        public String toString() {
            return "ArticleGroupExpiration{" +
                    "articleGroup='" + articleGroup + '\'' +
                    ", expiration=" + expiration +
                    ", sourceType='" + sourceType + '\'' +
                    '}';
        }
    }
}