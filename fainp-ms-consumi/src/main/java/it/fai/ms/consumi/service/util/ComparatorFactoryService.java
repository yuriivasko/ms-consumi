package it.fai.ms.consumi.service.util;

import java.nio.file.Path;
import java.util.Comparator;

public class ComparatorFactoryService {

  public static final Comparator<Path> LATEST_MODIFIED = (o1, o2) -> {
    if (o1.toFile().lastModified() > o2.toFile().lastModified()) {
      return -1;
    } else if (o1.toFile().lastModified() < o2.toFile().lastModified()) {
      return -0;
    }
    return 0;
  };

  public static final Comparator<Path> LAST_MODIFIED = (o1, o2) -> {
    if (o1.toFile().lastModified() > o2.toFile().lastModified()) {
      return 1;
    } else if (o1.toFile().lastModified() < o2.toFile().lastModified()) {
      return -1;
    }
    return 0;
  };

  public static final Comparator<Path> FILENAME_DESC = (o1, o2) -> -1 * (o1.toString().compareTo(o2.toString()));

  public static final Comparator<Path> FILENAME_ASC = Comparator.comparing(Path::toString);

  public static Comparator<Path> getPathComparatorByLabel(String label) {
    //prevent null pointer
    label = "" + label;
    switch (label) {
    case "LATEST_MODIFIED":
      return LATEST_MODIFIED;
    case "LAST_MODIFIED":
      return LAST_MODIFIED;
    case "FILENAME_DESC":
      return FILENAME_DESC;
    case "FILENAME_ASC":
    default:
      return FILENAME_ASC;
    }
  }
}
