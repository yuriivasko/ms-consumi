package it.fai.ms.consumi.service.processor.dettagli_stanziamenti;

import java.util.Optional;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

public interface DettaglioStanziamentoProvvisorioProcessor {

  Optional<DettaglioStanziamento> process(DettaglioStanziamento allocationDetail, StanziamentiParams allocationParams);

}
