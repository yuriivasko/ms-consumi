package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico;

import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric;

@Entity
@Table(name = "dettaglio_stanziamento_generico")
@DiscriminatorValue(value = "GENERICO")
public class DettaglioStanziamentoGenericoEntity
  extends DettaglioStanziamentoEntityGeneric {

  /* IN COMUNE CON PEDAGGI */
  @NotNull
  @Column(name = "tipo_sorgente", nullable = false)
  private String sourceType;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "tipo_consumo")
  @Enumerated(EnumType.STRING)
  private InvoiceType invoiceType;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "transaction_detail_code")
  private String transactionDetailCode;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "codice_fornitore_nav")
  private String supplierCode;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "partner_code")
  private String partnerCode = "";

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "raggruppamento_articoli_nav")
  private String articlesGroup;

  @Column(name = "data_inzio")
  private Instant startDate;

  @Column(name = "data_fine")
  private Instant endDate;

  @Column(name = "quantita", precision = 16, scale = 5)
  private BigDecimal quantity;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "amount_novat", precision = 16, scale = 5)
  private BigDecimal amountExcludedVat;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "perc_iva", precision = 16, scale = 5)
  private BigDecimal amountVatRate;

  @Column(name = "numero_dispositivo")
  private String deviceCode;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "tratta")
  private String tratta;

  @Column(name = "targa_su_flusso")
  private String licensePlateOnRecord;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "veicolo_classe_euro")
  private String vehicleEuroClass;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "veicolo_targa")
  private String vehicleLicenseId;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "veicolo_nazione_targa")
  private String vehicleLicenseCountryId;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "veicolo_classe_tariffaria")
  private String vehicleFareClass;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "descrizione_aggiuntiva")
  private String additionalInformation;

  /* IN COMUNE CON PEDAGGI */
  @Column(name = "documento_da_fornitore")
  private String supplierDocument;
  @Column(name = "tipo_dispositivo")
  private String tipoDispositivo;

  @Column(name = "punto_erogazione")
  private String puntoErogazione;
  
  @Column(name = "row_number")
  private long sourceRowNumber = -1l;

  @Column(name = "km")
  private String km;


  public DettaglioStanziamentoGenericoEntity(final String _globalIdentifier) {
    super(_globalIdentifier);
  }

  protected DettaglioStanziamentoGenericoEntity() {
  }

  @Override
  public DettaglioStanziamentoEntity changeInvoiceTypeToD() {
    throw new UnsupportedOperationException();
  }

  @Override
  public DettaglioStanziamentoEntity negateAmount() {
    throw new UnsupportedOperationException();
  }

  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public InvoiceType getInvoiceType() {
    return invoiceType;
  }

  public void setInvoiceType(InvoiceType invoiceType) {
    this.invoiceType = invoiceType;
  }

  public String getTransactionDetailCode() {
    return transactionDetailCode;
  }

  public void setTransactionDetailCode(String transactionDetailCode) {
    this.transactionDetailCode = transactionDetailCode;
  }

  public String getSupplierCode() {
    return supplierCode;
  }

  public void setSupplierCode(String supplierCode) {
    this.supplierCode = supplierCode;
  }

  public String getPartnerCode() {
    return partnerCode;
  }

  public void setPartnerCode(String partnerCode) {
    this.partnerCode = partnerCode;
  }

  public String getArticlesGroup() {
    return articlesGroup;
  }

  public void setArticlesGroup(String articlesGroup) {
    this.articlesGroup = articlesGroup;
  }

  public Instant getStartDate() {
    return startDate;
  }

  public void setStartDate(Instant startDate) {
    this.startDate = startDate;
  }

  public Instant getEndDate() {
    return endDate;
  }

  public void setEndDate(Instant endDate) {
    this.endDate = endDate;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getAmountExcludedVat() {
    return amountExcludedVat;
  }

  public void setAmountExcludedVat(BigDecimal amountExcludedVat) {
    this.amountExcludedVat = amountExcludedVat;
  }

  public BigDecimal getAmountVatRate() {
    return amountVatRate;
  }

  public void setAmountVatRate(BigDecimal amountVatRate) {
    this.amountVatRate = amountVatRate;
  }

  public String getDeviceCode() {
    return deviceCode;
  }

  public void setDeviceCode(String deviceCode) {
    this.deviceCode = deviceCode;
  }

  public String getTratta() {
    return tratta;
  }

  public void setTratta(String tratta) {
    this.tratta = tratta;
  }

  public String getLicensePlateOnRecord() {
    return licensePlateOnRecord;
  }

  public void setLicensePlateOnRecord(String licensePlateOnRecord) {
    this.licensePlateOnRecord = licensePlateOnRecord;
  }

  public String getVehicleEuroClass() {
    return vehicleEuroClass;
  }

  public void setVehicleEuroClass(String vehicleEuroClass) {
    this.vehicleEuroClass = vehicleEuroClass;
  }

  public String getVehicleLicenseId() {
    return vehicleLicenseId;
  }

  public void setVehicleLicenseId(String vehicleLicenseId) {
    this.vehicleLicenseId = vehicleLicenseId;
  }

  public String getVehicleLicenseCountryId() {
    return vehicleLicenseCountryId;
  }

  public void setVehicleLicenseCountryId(String vehicleLicenseCountryId) {
    this.vehicleLicenseCountryId = vehicleLicenseCountryId;
  }

  public String getVehicleFareClass() {
    return vehicleFareClass;
  }

  public void setVehicleFareClass(String vehicleFareClass) {
    this.vehicleFareClass = vehicleFareClass;
  }

  public String getAdditionalInformation() {
    return additionalInformation;
  }

  public void setAdditionalInformation(String additionalInformation) {
    this.additionalInformation = additionalInformation;
  }

  public String getSupplierDocument() {
    return supplierDocument;
  }

  public void setSupplierDocument(String supplierDocument) {
    this.supplierDocument = supplierDocument;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }
   

  public long getSourceRowNumber() {
    return sourceRowNumber;
  }

  public void setSourceRowNumber(long sourceRowNumber) {
    this.sourceRowNumber = sourceRowNumber;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("DettaglioStanziamentoGenericoEntity [sourceType=");
    builder.append(sourceType);
    builder.append(", invoiceType=");
    builder.append(invoiceType);
    builder.append(", transactionDetailCode=");
    builder.append(transactionDetailCode);
    builder.append(", supplierCode=");
    builder.append(supplierCode);
    builder.append(", partnerCode=");
    builder.append(partnerCode);
    builder.append(", articlesGroup=");
    builder.append(articlesGroup);
    builder.append(", startDate=");
    builder.append(startDate);
    builder.append(", endDate=");
    builder.append(endDate);
    builder.append(", quantity=");
    builder.append(quantity);
    builder.append(", amountExcludedVat=");
    builder.append(amountExcludedVat);
    builder.append(", amountVatRate=");
    builder.append(amountVatRate);
    builder.append(", deviceCode=");
    builder.append(deviceCode);
    builder.append(", tratta=");
    builder.append(tratta);
    builder.append(", licensePlateOnRecord=");
    builder.append(licensePlateOnRecord);
    builder.append(", vehicleEuroClass=");
    builder.append(vehicleEuroClass);
    builder.append(", vehicleLicenseId=");
    builder.append(vehicleLicenseId);
    builder.append(", vehicleLicenseCountryId=");
    builder.append(vehicleLicenseCountryId);
    builder.append(", vehicleFareClass=");
    builder.append(vehicleFareClass);
    builder.append(", additionalInformation=");
    builder.append(additionalInformation);
    builder.append(", supplierDocument=");
    builder.append(supplierDocument);
    builder.append(", tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", puntoErogazione=");
    builder.append(puntoErogazione);
    builder.append(", km=");
    builder.append(km);
    builder.append(", getGlobalIdentifier()=");
    builder.append(getGlobalIdentifier());
    builder.append(", sourceRowNumber=");
    builder.append(sourceRowNumber);
    builder.append("]");
    return builder.toString();
  }


}
