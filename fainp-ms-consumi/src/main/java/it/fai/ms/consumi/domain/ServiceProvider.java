package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.enumeration.PriceSource;

@Entity
@Table(name = "service_provider")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ServiceProvider implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @NotNull
  @Column(name = "provider_name", nullable = false)
  private String providerName;

  @NotNull
  @Size(min = 1, max = 20)
  @Column(name = "provider_code", length = 20, nullable = false, unique = true)
  private String providerCode;

  @NotNull
  @Column(name = "service_name", nullable = false)
  private String serviceName;

  @NotNull
  @Size(min = 1, max = 20)
  @Column(name = "service_code", length = 20, nullable = false)
  private String serviceCode;

  @Column(name = "file_path")
  private String filePath;

  @Enumerated(EnumType.STRING)
  @Column(name = "format")
  private Format format;

  @Enumerated(EnumType.STRING)
  @Column(name = "price_source")
  private PriceSource priceSource;

  @Column(name = "cron_expression")
  private String cronExpression;

  @Column(name = "file_match_regex")
  private String fileMatchRegex;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getProviderName() {
    return providerName;
  }

  public ServiceProvider providerName(String providerName) {
    this.providerName = providerName;
    return this;
  }

  public void setProviderName(String providerName) {
    this.providerName = providerName;
  }

  public String getProviderCode() {
    return providerCode;
  }

  public ServiceProvider providerCode(String providerCode) {
    this.providerCode = providerCode;
    return this;
  }

  public void setProviderCode(String providerCode) {
    this.providerCode = providerCode;
  }

  public String getServiceName() {
    return serviceName;
  }

  public ServiceProvider serviceName(String serviceName) {
    this.serviceName = serviceName;
    return this;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  public String getServiceCode() {
    return serviceCode;
  }

  public ServiceProvider serviceCode(String serviceCode) {
    this.serviceCode = serviceCode;
    return this;
  }

  public void setServiceCode(String serviceCode) {
    this.serviceCode = serviceCode;
  }

  public String getFilePath() {
    return filePath;
  }

  public ServiceProvider filePath(String filePath) {
    this.filePath = filePath;
    return this;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public Format getFormat() {
    return format;
  }

  public ServiceProvider format(Format format) {
    this.format = format;
    return this;
  }

  public void setFormat(Format format) {
    this.format = format;
  }

  public PriceSource getPriceSource() {
    return priceSource;
  }

  public ServiceProvider priceSource(PriceSource priceSource) {
    this.priceSource = priceSource;
    return this;
  }

  public void setPriceSource(PriceSource priceSource) {
    this.priceSource = priceSource;
  }

  public String getCronExpression() {
    return cronExpression;
  }

  public ServiceProvider cronExpression(String cronExpression) {
    this.cronExpression = cronExpression;
    return this;
  }

  public void setCronExpression(String cronExpression) {
    this.cronExpression = cronExpression;
  }

  public String getFileMatchRegex() {
    return fileMatchRegex;
  }

  public void setFileMatchRegex(String fileMatchRegex) {
    this.fileMatchRegex = fileMatchRegex;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ServiceProvider serviceProvider = (ServiceProvider) o;
    if (serviceProvider.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), serviceProvider.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "ServiceProvider [id=" + id + ", providerName=" + providerName + ", providerCode=" + providerCode + ", serviceName="
           + serviceName + ", serviceCode=" + serviceCode + ", filePath=" + filePath + ", format=" + format + ", priceSource=" + priceSource
           + ", cronExpression=" + cronExpression + ", fileMatchRegex=" + fileMatchRegex + "]";
  }

  public ServicePartner toServicePartner() {
    ServicePartner servicePartner = new ServicePartner(getId() + "");
    servicePartner.setFormat(getFormat());
    servicePartner.setPartnerCode(getProviderCode());
    servicePartner.setPartnerName(getProviderName());
    servicePartner.setPriceSoruce(getPriceSource());

    return servicePartner;
  }
}
