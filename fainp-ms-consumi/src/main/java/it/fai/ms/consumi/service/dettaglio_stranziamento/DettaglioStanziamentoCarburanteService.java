package it.fai.ms.consumi.service.dettaglio_stranziamento;

import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;

public interface DettaglioStanziamentoCarburanteService {

  void save(DettaglioStanziamentoCarburante entity);

  Set<DettaglioStanziamentoCarburante> findByCodiceStanziamento(String codiceStanziamento);

  List<DettaglioStanziamentoCarburanteEntity> findToUpdatePriceTableOpen(PriceTableFilterDTO priceTableFilter);

  List<DettaglioStanziamentoCarburanteEntity> findToUpdatePriceTableClosed(PriceTableFilterDTO priceTableFilter);

  StanziamentoEntity cloneStanziamentoCarburanteFromProvvisorio(StanziamentoEntity s);

  StanziamentoEntity cloneStanziamentoCarburanteFromProvvisorio(StanziamentoEntity s, InvoiceType state);

  void saveStanziamento(@NotNull StanziamentoEntity stanziamento);

  void saveEntity(DettaglioStanziamentoCarburanteEntity entity);

  DettaglioStanziamentoCarburante mapToDomain(DettaglioStanziamentoCarburanteEntity dettaglioCloned);

  DettaglioStanziamentoCarburanteEntity mapToEntity(DettaglioStanziamentoCarburante dettaglioClonedDomain);

  DettaglioStanziamentoCarburanteEntity cloneDettaglioStanziamento(DettaglioStanziamentoCarburanteEntity dsEntity);

  DettaglioStanziamentoCarburanteEntity update(DettaglioStanziamentoCarburanteEntity entity);

  void updateStanziamento(StanziamentoEntity stanziamento);

}
