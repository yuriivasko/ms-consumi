package it.fai.ms.consumi.repository.parametri_stanziamenti;

import static it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntity_.RECORD_CODE;
import static it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntity_.SOURCE;
import static it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntity_.SUPPLIER;
import static it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsSupplierEntity_.LEGACY_CODE;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsArticleDTO;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiDetailsTypeEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsArticleEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsArticleEntity_;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntityMapper;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsEntity_;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiParamsSupplierEntity_;

@Repository
@Validated
@Transactional
public class StanziamentiParamsRepositoryImpl implements StanziamentiParamsRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final EntityManager                  em;
  private final StanziamentiParamsEntityMapper entityMapper;

  @Inject
  public StanziamentiParamsRepositoryImpl(final EntityManager _entityManager, final StanziamentiParamsEntityMapper _entityMapper) {
    em = _entityManager;
    entityMapper = _entityMapper;
  }

  @Override
  @Cacheable(value = "stanziamentiParams", key = "{#root.methodName, #root.args[0], #root.args[1]}")
  public List<StanziamentiParams> findBySourceAndRecordCode(@NotNull final String _source, @NotNull final String _recordCode) {
    List<StanziamentiParams> stanziamentiParamsList = null;

    _log.info(" Searching {} by source : {} - recordcode : {} ...", StanziamentiParams.class.getSimpleName(), _source, _recordCode);

    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);

      final var predicates = List.of(criteriaBuilder.equal(criteriaBuilder.lower(root.get(SOURCE)), StringUtils.lowerCase(_source)),
                                     criteriaBuilder.equal(root.get(RECORD_CODE), _recordCode))
                                 .toArray(new Predicate[0]);
      query.where(predicates);

      stanziamentiParamsList = em.createQuery(query)
                                 .getResultStream()
                                 .peek(entity -> _log.trace(" Found {}", entity))
                                 .map(entity -> entityMapper.toDomain(entity))
                                 .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      stanziamentiParamsList = Collections.emptyList();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return stanziamentiParamsList;
  }

  @Override
  public Optional<StanziamentiParams> findFirstByArticleCode(@NotNull final String _articleCode) {

    Optional<StanziamentiParams> optionalStanziamentiParams = null;
    try {
      TypedQuery<StanziamentiParamsEntity> query = getQueryToSearchByArticleCode(_articleCode);
      optionalStanziamentiParams = query.getResultStream()
                                        .peek(entity -> _log.info(" Found {}", entity))
                                        .findFirst()
                                        .map(entity -> entityMapper.toDomain(entity));

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      optionalStanziamentiParams = Optional.empty();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return optionalStanziamentiParams;
  }

  @Override
  public Optional<List<StanziamentiParams>> findByArticleCode(@NotNull final String _articleCode) {

    Optional<List<StanziamentiParams>> optionalStanziamentiParams = null;
    try {

      TypedQuery<StanziamentiParamsEntity> query = getQueryToSearchByArticleCode(_articleCode);
      List<StanziamentiParams> listStanziamentiParam = query.getResultStream()
                                                            .peek(entity -> _log.info(" Found {}", entity))
                                                            .map(entity -> entityMapper.toDomain(entity))
                                                            .collect(toList());
      optionalStanziamentiParams = Optional.ofNullable(listStanziamentiParam);

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      optionalStanziamentiParams = Optional.empty();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return optionalStanziamentiParams;
  }

  private TypedQuery<StanziamentiParamsEntity> getQueryToSearchByArticleCode(@NotNull final String _articleCode) {
    _log.info(" Searching {} by article code : {} ...", StanziamentiParams.class.getSimpleName(), _articleCode);

    final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
    final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
    final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
    query.select(root);

    final Join<StanziamentiParamsEntity, StanziamentiParamsArticleEntity> join = root.join(StanziamentiParamsEntity_.ARTICLE);

    final var predicates = List.of(criteriaBuilder.equal(join.get(StanziamentiParamsArticleEntity_.CODE), _articleCode))
                               .toArray(new Predicate[0]);
    query.where(predicates);

    TypedQuery<StanziamentiParamsEntity> createQuery = em.createQuery(query);

    return createQuery;
  }

  @Override
  public AllStanziamentiParams getAll() {

    AllStanziamentiParams allStanziamentiParams = new AllStanziamentiParams();

    try {

      final CriteriaQuery<StanziamentiParamsEntity> query = em.getCriteriaBuilder()
                                                              .createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);

      final var stanziamentiParams = em.createQuery(query)
                                       .getResultStream()
                                       .peek(entity -> _log.trace(" Found {}", entity))
                                       .map(entity -> entityMapper.toDomain(entity))
                                       .collect(toList());

      allStanziamentiParams = new AllStanziamentiParams(stanziamentiParams);

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      _log.warn(" No {} found", StanziamentiParams.class.getSimpleName());
    }

    return allStanziamentiParams;
  }

  @Override
  public List<StanziamentiParamsArticleDTO> findAllStanziamentiParamsArticles() {
    try {

      final CriteriaQuery<Object[]> query = em.getCriteriaBuilder()
                                              .createQuery(Object[].class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.multiselect(root.get(StanziamentiParamsEntity_.ARTICLE)
                            .get(StanziamentiParamsArticleEntity_.CODE),
                        root.get(StanziamentiParamsEntity_.ARTICLE)
                            .get(StanziamentiParamsArticleEntity_.DESCRIPTION))
           .groupBy(root.get(StanziamentiParamsEntity_.ARTICLE)
                        .get(StanziamentiParamsArticleEntity_.CODE),
                    root.get(StanziamentiParamsEntity_.ARTICLE)
                        .get(StanziamentiParamsArticleEntity_.DESCRIPTION));

      final var articles = em.createQuery(query)
                             .getResultStream()
                             .peek(entity -> _log.trace(" Found {}", entity))
                             .map(result -> {
                               StanziamentiParamsArticleDTO a = new StanziamentiParamsArticleDTO();
                               a.setCode((String) result[0]);
                               a.setDescription((String) result[1]);
                               return a;
                             })
                             .collect(toList());

      return articles;

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      _log.warn(" No {} found", StanziamentiParams.class.getSimpleName());
    }

    return Collections.emptyList();
  }

  @Override
  public Optional<StanziamentiParams> findByArticleAndRecordCodeAndSupplierCodeAndTransactionalDetailAndTipoDettaglio(String numeroArticolo,
                                                                                                                      String recordCode,
                                                                                                                      String supplierCode,
                                                                                                                      String detailCode,
                                                                                                                      StanziamentiDetailsType detailsType) {
    _log.info(" Searching {} by : numeroArticolo: {}, recordCode: {}, supplierCode: {}, detailCode: {} and detailsType: {} ",
              StanziamentiParams.class.getSimpleName(), numeroArticolo, recordCode, supplierCode, detailCode, detailsType);

    Optional<StanziamentiParams> optionalStanziamentiParams = null;
    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);

      var predicates = new ArrayList<Predicate>();
      final Join<StanziamentiParamsEntity, StanziamentiParamsArticleEntity> join = root.join(StanziamentiParamsEntity_.ARTICLE);

      Predicate p = criteriaBuilder.equal(join.get(StanziamentiParamsArticleEntity_.CODE), numeroArticolo);
      predicates.add(p);

      p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.RECORD_CODE), recordCode);
      predicates.add(p);

      p = criteriaBuilder.equal(root.join(StanziamentiParamsEntity_.SUPPLIER)
                                    .get(StanziamentiParamsSupplierEntity_.CODE),
                                supplierCode);
      predicates.add(p);

      p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.TRANSACTION_DETAIL), detailCode);
      predicates.add(p);

      p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.STANZIAMENTI_DETAILS_TYPE),
                                StanziamentiDetailsTypeEntity.valueOf(detailsType.name()));
      predicates.add(p);

      query.where(predicates.toArray(new Predicate[] {}));

      optionalStanziamentiParams = em.createQuery(query)
                                     .getResultStream()
                                     .peek(entity -> _log.info(" Found {}", entity))
                                     .findFirst()
                                     .map(entity -> entityMapper.toDomain(entity));

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      optionalStanziamentiParams = Optional.empty();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return optionalStanziamentiParams;
  }

  @Override
  public Optional<StanziamentiParams> findBySourceAndLegacyCode(@NotNull final String _source, @NotNull final String _legacyCode) {
    Optional<StanziamentiParams> optionalStanziamentiParams = null;

    _log.info(" Searching {} by source : {} - legacyCode : {} ...", StanziamentiParams.class.getSimpleName(), _source, _legacyCode);

    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);

      final var predicates = List.of(criteriaBuilder.equal(criteriaBuilder.lower(root.get(SOURCE)), StringUtils.lowerCase(_source)),
                                     criteriaBuilder.equal(root.get(SUPPLIER)
                                                               .get(LEGACY_CODE),
                                                           _legacyCode))
                                 .toArray(new Predicate[0]);
      query.where(predicates);

      optionalStanziamentiParams = em.createQuery(query)
                                     .getResultStream()
                                     .peek(entity -> _log.info(" Found {}", entity))
                                     .findFirst()
                                     .map(entity -> entityMapper.toDomain(entity));

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      optionalStanziamentiParams = Optional.empty();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return optionalStanziamentiParams;
  }

  @Override
  public List<StanziamentiParams> findByTipoDettaglio(StanziamentiDetailsType _tipo) {
    List<StanziamentiParams> stanziamentiParamsList = null;

    _log.info(" Searching {} by detail type : {} ...", StanziamentiParams.class.getSimpleName(), _tipo);

    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);

      var predicates = new ArrayList<Predicate>();
      Predicate p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.STANZIAMENTI_DETAILS_TYPE),
                                          StanziamentiDetailsTypeEntity.valueOf(_tipo.name()));
      predicates.add(p);

      query.where(predicates.toArray(new Predicate[] {}));

      stanziamentiParamsList = em.createQuery(query)
                                 .getResultStream()
                                 .peek(entity -> _log.trace(" Found {}", entity))
                                 .map(entity -> entityMapper.toDomain(entity))
                                 .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      stanziamentiParamsList = Collections.emptyList();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return stanziamentiParamsList;
  }

  @Override
  public Optional<String> findDescriptionByRaggruppamentoArticleCode(String raggruppamentoArticleCode) {

    _log.info(" Searching {} by Raggruppamento Code : {} ...", StanziamentiParams.class.getSimpleName(), raggruppamentoArticleCode);

    Optional<StanziamentiParamsEntity> optionalStanziamentiParams = null;
    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);

      final var predicates = List.of(criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.GROUPING), raggruppamentoArticleCode))
                                 .toArray(new Predicate[0]);

      query.select(root);
      query.where(predicates);

      optionalStanziamentiParams = em.createQuery(query)
                                     .getResultStream()
                                     .findFirst();

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      optionalStanziamentiParams = Optional.empty();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return (optionalStanziamentiParams.isPresent()) ? Optional.of(optionalStanziamentiParams.get()
                                                                                            .getArticle()
                                                                                            .getDescription())
                                                    : Optional.empty();
  }

  @Override
  public StanziamentiParams updateStanziamentiParams(StanziamentiParams stanziamentiParams) {
    if (stanziamentiParams.getUuid() == null) {
      throw new IllegalArgumentException("Cannot save new StanziamentiParams or StanziamentiParams without uuid!");
    }
    StanziamentiParamsEntity stanziamentiParamsEntity = entityMapper.toEntity(stanziamentiParams);
    StanziamentiParamsEntity mergedStanziamentiParams = em.merge(stanziamentiParamsEntity);
    return entityMapper.toDomain(mergedStanziamentiParams);
  }

  @Override
  public Optional<List<StanziamentiParams>> findByTransactionDetail(@NotNull String transactionDetail) {
    Optional<List<StanziamentiParams>> optionalStanziamentiParams = null;
    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);
      final var predicates = List.of(criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.TRANSACTION_DETAIL), transactionDetail))
                                 .toArray(new Predicate[0]);
      query.where(predicates);

      TypedQuery<StanziamentiParamsEntity> createQuery = em.createQuery(query);
      List<StanziamentiParamsEntity> resultList = createQuery.getResultList();
      List<StanziamentiParams> listStanziamentiParam = resultList.stream()
                                                                 .peek(entity -> _log.info(" Found {}", entity))
                                                                 .map(entity -> entityMapper.toDomain(entity))
                                                                 .collect(toList());
      if (listStanziamentiParam != null && listStanziamentiParam.isEmpty()) {
        listStanziamentiParam = null;
      }
      optionalStanziamentiParams = Optional.ofNullable(listStanziamentiParam);

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      optionalStanziamentiParams = Optional.empty();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return optionalStanziamentiParams;
  }

  @Override
  public Optional<StanziamentiDetailsType> findByTracciatoAndTransactionDetail(@NotNull String tracciato, @NotNull String transactionDetail) {
    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);
      final var predicates = List.of(
        criteriaBuilder.equal(root.get(SOURCE), tracciato),
        criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.TRANSACTION_DETAIL), transactionDetail))
      .toArray(new Predicate[0]);
      query.where(predicates);

      TypedQuery<StanziamentiParamsEntity> createQuery = em.createQuery(query);
      List<StanziamentiParamsEntity> resultList = createQuery.getResultList();
      return resultList.stream()
        .peek(entity -> _log.info(" Found {}", entity))
        .map(entity -> entityMapper.toDomain(entity))
        .map(domain -> domain.getStanziamentiDetailsType())
        .findFirst();

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      _log.info("No {} found for tracciato:=[{}] and transactionDetail:=[{}]", StanziamentiDetailsType.class.getSimpleName(), tracciato, transactionDetail);
    }
    return Optional.empty();
  }

  @Override
  @Cacheable(value = "findStanziamentiParams4Allegati", key = "{#root.methodName, #root.args[0], #root.args[1], #root.args[2], #root.args[3]}")
  public Optional<StanziamentiParams> findByTransactionDetailAndCodiceFornitoreAndRecordCodeAndTipoDettaglio(@NotNull String transactionDetail,
                                                                                                             @NotNull String codiceFornitore,
                                                                                                             @NotNull String recordCode,
                                                                                                             @NotNull StanziamentiDetailsTypeEntity detailType) {
    Optional<StanziamentiParams> optionalStanziamentiParam = null;
    try {

      final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
      final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
      query.select(root);

      List<Predicate> predicates = new ArrayList<>();
      Predicate p = null;

      p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.TRANSACTION_DETAIL), transactionDetail);
      predicates.add(p);
      p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.SUPPLIER)
                                    .get(StanziamentiParamsSupplierEntity_.CODE),
                                codiceFornitore);
      predicates.add(p);
      p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.RECORD_CODE), recordCode);
      predicates.add(p);
      p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.STANZIAMENTI_DETAILS_TYPE), detailType);
      predicates.add(p);

      query.where(predicates.toArray(new Predicate[predicates.size()]));

      TypedQuery<StanziamentiParamsEntity> createQuery = em.createQuery(query);

      createQuery.setMaxResults(1);

      Optional<StanziamentiParamsEntity> optParamStanziamenti = createQuery.getResultStream()
                                                                           .findFirst();
      if (optParamStanziamenti.isPresent()) {
        optionalStanziamentiParam = Optional.ofNullable(entityMapper.toDomain(optParamStanziamenti.get()));
      } else {
        optionalStanziamentiParam = Optional.empty();
      }
    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      optionalStanziamentiParam = Optional.empty();
      _log.warn("No {} found", StanziamentiParams.class.getSimpleName());
    }

    return optionalStanziamentiParam;
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true, propagation = Propagation.NOT_SUPPORTED)
  public Optional<StanziamentiParams> findFirstByCodiceRaggruppamentoArticolo(final String raggruppamentoArticolo) {
    final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
    final CriteriaQuery<StanziamentiParamsEntity> query = criteriaBuilder.createQuery(StanziamentiParamsEntity.class);
    final Root<StanziamentiParamsEntity> root = query.from(StanziamentiParamsEntity.class);
    query.select(root);

    List<Predicate> predicates = new ArrayList<>();
    Predicate p = null;

    p = criteriaBuilder.equal(root.get(StanziamentiParamsEntity_.GROUPING), raggruppamentoArticolo);
    predicates.add(p);

    query.where(predicates.toArray(new Predicate[predicates.size()]));

    TypedQuery<StanziamentiParamsEntity> createQuery = em.createQuery(query);

    createQuery.setMaxResults(1);
    
    Optional<StanziamentiParamsEntity> optParamStanziamenti = createQuery.getResultStream()
        .findFirst();
    
    if (optParamStanziamenti.isPresent()) {
      return Optional.ofNullable(entityMapper.toDomain(optParamStanziamenti.get()));
    } 

    return Optional.empty();
  }
  
}
