package it.fai.ms.consumi.repository.storico_dml.model;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "storico_stato_servizio")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoStatoServizio implements Serializable {

  private static final long serialVersionUID = 7283415906582638103L;

  @Id
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  private String id;

  @Size(max = 255)
  @Column(name = "dispositivo_dml_unique_identifier", length = 255)
  private String dispositivoDmlUniqueIdentifier;
  
  @Size(max = 30)
  @Column(name = "stato", length = 30)
  private String stato;

  @Column(name = "data_variazione")
  private Instant dataVariazione;

  @Column(name = "data_fine_variazione")
  private Instant dataFineVariazione;

  @Size(max = 255)
  @Column(name = "pan", length = 255)
  private String pan;

  @Size(max = 50)
  @Column(name = "tipo_servizio", length = 50)
  private String tipoServizio;

  /** getter e setter **/
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDispositivoDmlUniqueIdentifier() {
    return dispositivoDmlUniqueIdentifier;
  }

  public void setDispositivoDmlUniqueIdentifier(String dispositivoDmlUniqueIdentifier) {
    this.dispositivoDmlUniqueIdentifier = dispositivoDmlUniqueIdentifier;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public Instant getDataVariazione() {
    return dataVariazione;
  }

  public void setDataVariazione(Instant dataVariazione) {
    this.dataVariazione = dataVariazione;
  }

  public Instant getDataFineVariazione() {
    return dataFineVariazione;
  }

  public void setDataFineVariazione(Instant dataFineVariazione) {
    this.dataFineVariazione = dataFineVariazione;
  }

  public String getPan() {
    return pan;
  }

  public void setPan(String pan) {
    this.pan = pan;
  }

  public String getTipoServizio() {
    return tipoServizio;
  }

  public void setTipoServizio(String tipoServizio) {
    this.tipoServizio = tipoServizio;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dataFineVariazione == null) ? 0 : dataFineVariazione.hashCode());
    result = prime * result + ((dataVariazione == null) ? 0 : dataVariazione.hashCode());
    result = prime * result + ((dispositivoDmlUniqueIdentifier == null) ? 0 : dispositivoDmlUniqueIdentifier.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((pan == null) ? 0 : pan.hashCode());
    result = prime * result + ((stato == null) ? 0 : stato.hashCode());
    result = prime * result + ((tipoServizio == null) ? 0 : tipoServizio.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    StoricoStatoServizio other = (StoricoStatoServizio) obj;
    if (dataFineVariazione == null) {
      if (other.dataFineVariazione != null)
        return false;
    } else if (!dataFineVariazione.equals(other.dataFineVariazione))
      return false;
    if (dataVariazione == null) {
      if (other.dataVariazione != null)
        return false;
    } else if (!dataVariazione.equals(other.dataVariazione))
      return false;
    if (dispositivoDmlUniqueIdentifier == null) {
      if (other.dispositivoDmlUniqueIdentifier != null)
        return false;
    } else if (!dispositivoDmlUniqueIdentifier.equals(other.dispositivoDmlUniqueIdentifier))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (pan == null) {
      if (other.pan != null)
        return false;
    } else if (!pan.equals(other.pan))
      return false;
    if (stato == null) {
      if (other.stato != null)
        return false;
    } else if (!stato.equals(other.stato))
      return false;
    if (tipoServizio == null) {
      if (other.tipoServizio != null)
        return false;
    } else if (!tipoServizio.equals(other.tipoServizio))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "StoricoStatoServizio [id=" + id + ", dispositivoDmlUniqueIdentifier=" + dispositivoDmlUniqueIdentifier + ", stato=" + stato
           + ", dataVariazione=" + dataVariazione + ", dataFineVariazione=" + dataFineVariazione + ", pan=" + pan + ", tipoServizio="
           + tipoServizio + "]";
  }
}
