package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import it.fai.common.enumeration.TipoDispositivoEnum;

import java.util.Objects;

public class DispositiviDto {
  private String codiceClienteNav;
  private String raggruppamentoArticoliNav;
  private String licensePlate;
  private String licensePlateCountry;
  private TipoDispositivoEnum deviceType;
  private String obu;
  private Long id;

  public DispositiviDto(){
    super();
  }


  //pedaggi
  public DispositiviDto(String codiceClienteNav, String raggruppamentoArticoliNav, String licensePlate,
                        String licensePlateCountry, TipoDispositivoEnum deviceType, String obu) {
    super();
    this.codiceClienteNav = codiceClienteNav;
    this.raggruppamentoArticoliNav = raggruppamentoArticoliNav;
    this.licensePlate = licensePlate;
    this.licensePlateCountry = licensePlateCountry;
    this.deviceType = deviceType;
    this.obu = obu;
  }
  //carburanti
  public DispositiviDto(String codiceClienteNav, String licensePlate,
                        String licensePlateCountry, String obu) {
    super();
    this.codiceClienteNav = codiceClienteNav;
    this.licensePlate = licensePlate;
    this.licensePlateCountry = licensePlateCountry;
    this.obu = obu;
  }

  public String getCodiceClienteNav() {
    return codiceClienteNav;
  }

  public void setCodiceClienteNav(String codiceClienteNav) {
    this.codiceClienteNav = codiceClienteNav;
  }

  public String getRaggruppamentoArticoliNav() {
    return raggruppamentoArticoliNav;
  }

  public void setRaggruppamentoArticoliNav(String raggruppamentoArticoliNav) {
    this.raggruppamentoArticoliNav = raggruppamentoArticoliNav;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  public String getLicensePlateCountry() {
    return licensePlateCountry;
  }

  public void setLicensePlateCountry(String licensePlateCountry) {
    this.licensePlateCountry = licensePlateCountry;
  }

  public TipoDispositivoEnum getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(TipoDispositivoEnum deviceType) {
    this.deviceType = deviceType;
  }

  public String getObu() {
    return obu;
  }

  public void setObu(String obu) {
    this.obu = obu;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }



  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DispositiviDto that = (DispositiviDto) o;
    return Objects.equals(codiceClienteNav, that.codiceClienteNav) &&
      Objects.equals(raggruppamentoArticoliNav, that.raggruppamentoArticoliNav) &&
      Objects.equals(licensePlate, that.licensePlate) &&
      Objects.equals(licensePlateCountry, that.licensePlateCountry) &&
      deviceType == that.deviceType &&
      Objects.equals(obu, that.obu) &&
      Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codiceClienteNav, raggruppamentoArticoliNav, licensePlate, licensePlateCountry, deviceType, obu, id);
  }

  @Override
  public String toString() {
    return "DispositiviDto{" +
      "codiceClienteNav='" + codiceClienteNav + '\'' +
      ", raggruppamentoArticoliNav='" + raggruppamentoArticoliNav + '\'' +
      ", licensePlate='" + licensePlate + '\'' +
      ", licensePlateCountry='" + licensePlateCountry + '\'' +
      ", deviceType=" + deviceType +
      ", obu='" + obu + '\'' +
      ", id=" + id +
      '}';
  }
}
