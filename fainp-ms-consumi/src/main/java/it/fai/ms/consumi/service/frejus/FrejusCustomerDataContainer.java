package it.fai.ms.consumi.service.frejus;

import java.util.List;

public interface FrejusCustomerDataContainer {
  public void setCustomerTrc(List<FrejusCustomerData> data);
  public List<FrejusCustomerData> getCustomerTrc();
}
