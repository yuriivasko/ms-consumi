package it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class TollCollectGlobalIdentifier extends GlobalIdentifier {

  public TollCollectGlobalIdentifier(GlobalIdentifierType _type) {
    super(_type);
  }

  public TollCollectGlobalIdentifier(String _id, GlobalIdentifierType _type) {
    super(_id, _type);
  }
  public TollCollectGlobalIdentifier(String _id) {
    super(_id, GlobalIdentifierType.EXTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    throw new IllegalStateException("newGlobalIdentifier operation is supported only for global identifier type internal");
  }
}
