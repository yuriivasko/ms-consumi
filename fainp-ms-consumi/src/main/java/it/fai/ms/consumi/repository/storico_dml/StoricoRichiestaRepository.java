package it.fai.ms.consumi.repository.storico_dml;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.consumi.repository.storico_dml.model.DmlEmbeddedId;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoRichiesta;

@Repository
public interface StoricoRichiestaRepository extends JpaRepository<StoricoRichiesta, DmlEmbeddedId>,InterfaceDmlRepository<StoricoRichiesta> {

  @Query("select distinct r from StoricoRichiesta r, StoricoOrdiniCliente o " 
      + " where r.ordineClienteDmlUniqueIdentifier = o.dmlUniqueIdentifier" 
      + " and o.codiceCliente = :code")
  List<StoricoRichiesta> findAllByCodiceCliente(@Param("code") String code);

}
