package it.fai.ms.consumi.service.processor.consumi.elvia;

import it.fai.ms.consumi.domain.consumi.generici.telepass.ElviaGenerico;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface ElviaGenericoProcessor
  extends ConsumoProcessor<ElviaGenerico> {

}
