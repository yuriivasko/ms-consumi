package it.fai.ms.consumi.repository.device;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Repository
@Validated
@Transactional
@Primary
public class TipoSupportoRepositoryImpl implements TipoSupportoRepository {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final EntityManager em;

  public TipoSupportoRepositoryImpl(final EntityManager _entityManager) {
    em = _entityManager;
  }

  @Override
  @Cacheable(value = "tipoSupportoFix", key = "{#root.methodName, #root.args[0]}")
  public TipoSupporto findByTipoDispositivo(String tipoDispositivo) {
    TipoSupporto tipoSupporto = null;
    var query = em.getCriteriaBuilder()
                  .createQuery(TipoSupporto.class);
    var root = query.from(TipoSupporto.class);

    Predicate predicate = em.getCriteriaBuilder()
                            .equal(root.get(TipoSupporto_.TIPO_DISPOSITIVO), tipoDispositivo);
    CriteriaQuery<TipoSupporto> cq = query.where(predicate);
    
    TypedQuery<TipoSupporto> createQuery = em.createQuery(cq);
    List<TipoSupporto> resultList = createQuery
                                      .getResultList();
    if (resultList != null && !resultList.isEmpty()) {
      Optional<TipoSupporto> optTipoSupporto = resultList.stream()
                                                         .findFirst();
      if (optTipoSupporto.isPresent()) {
        tipoSupporto = optTipoSupporto.get();
      }
    }
    log.info("Found TipoSupporto: {} by TipoDispositivo: {}", tipoSupporto, tipoDispositivo);
    return tipoSupporto;
  }

}
