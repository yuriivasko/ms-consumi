package it.fai.ms.consumi.domain;

public enum GlobalIdentifierType {

  INTERNAL, EXTERNAL;

}
