package it.fai.ms.consumi.service.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.service.petrolpump.consumer.PriceTableConsumer;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsListenerVariazionePriceTable implements JmsTopicListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final PriceTableConsumer priceTableConsumer;

  public JmsListenerVariazionePriceTable(final PriceTableConsumer _priceTableConsumer) {
    priceTableConsumer = _priceTableConsumer;
  }

  @Override
  public void onMessage(Message message) {
    log.debug("Received jms message {}", message);
    PriceTableDTO priceTableDTO = null;
    try {
      try {
        priceTableDTO = (PriceTableDTO) ((ObjectMessage) message).getObject();
      } catch (JMSException e) {
        log.error("Cast exception message: {}", message, e);
        throw e;
      }
      priceTableConsumer.consumeMessage(priceTableDTO);
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.VARIAZIONE_PRICE_TABLE;
  }
}
