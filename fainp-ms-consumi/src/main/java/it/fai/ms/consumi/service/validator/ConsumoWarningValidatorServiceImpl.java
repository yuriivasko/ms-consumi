package it.fai.ms.consumi.service.validator;

import java.time.Duration;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.consumi.DateSupplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.TrustedCustomerSupplier;
import it.fai.ms.consumi.domain.TrustedStatoDispositivo;
import it.fai.ms.consumi.domain.TrustedVechicleSupplier;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.validator.internal.ContractValidator;
import it.fai.ms.consumi.service.validator.internal.CustomerValidator;
import it.fai.ms.consumi.service.validator.internal.DeviceValidator;

@Service
@Validated
@Primary
@Deprecated
public class ConsumoWarningValidatorServiceImpl implements ConsumoWarningValidatorService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());
  private final ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService;

//  private final CustomerValidator customerValidator;
//  private final DeviceValidator   deviceValidator;
//  private final ContractValidator contractValidator;

  @Inject
  public ConsumoWarningValidatorServiceImpl(final ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService) {

    this.consumoNoBlockingValidatorService = consumoNoBlockingValidatorService;
  }

  @Override
  public ValidationOutcome validate(@NotNull Consumo consumo) {
    return consumoNoBlockingValidatorService.validate(consumo);
  }


//  private boolean consumoHasDevice(final Consumo _consumo) {
//    return _consumo instanceof DeviceSupplier && ((DeviceSupplier) _consumo).getDevice() != null;
//  }
//
//
//  @Override
//  public ValidationOutcome validate(@NotNull final Consumo _consumo) {
//    final var validationOutcome = new ValidationOutcome();
//
//    var _validationOutcome = customerWasActiveInDate(_consumo);
//    validationOutcome.addValidationOutcomeMessages(_validationOutcome);
//
//    boolean isDeviceNotVirtual = true;
//    if (_consumo instanceof DeviceSupplier) {
//      Device device = ((DeviceSupplier) _consumo).getDevice();
//      if (device != null) {
//        isDeviceNotVirtual = !device.isVirtuale();
//      }
//    }
//
//    if (isDeviceNotVirtual) {
//      _validationOutcome = contractWasActiveInDate(_consumo);
//      validationOutcome.addValidationOutcomeMessages(_validationOutcome);
//
//      _validationOutcome = deviceWasActiveAndAssociatedWithContractInDate(_consumo);
//      validationOutcome.addValidationOutcomeMessages(_validationOutcome);
//
//      _validationOutcome = serviceWasActiveInDate(_consumo);
//      validationOutcome.addValidationOutcomeMessages(_validationOutcome);
//
//      if (_consumo instanceof TrustedVechicleSupplier) {
//        final Vehicle vehicle = ((TrustedVechicleSupplier) _consumo).getVehicle();
//        if (vehicle == null || !vehicle.isComplete()) {
//          validationOutcome.addMessage(new Message("w006").withText("Vehicle not present :" + _consumo));
//        }
//      }
//    } else {
//      _log.info("Skip validation because device set in consumo is virtual");
//    }
//
//    _log.info("Waring validations result : {}", validationOutcome);
//
//    return validationOutcome;
//  }
//
//  // quando il codice cliente non era attivo alla data di consumo → anomalia (non bloccante)
//  private ValidationOutcome customerWasActiveInDate(final Consumo _consumo) {
//    String companyCode = null;
//    if (_consumo instanceof TrustedCustomerSupplier) {
//      companyCode = ((TrustedCustomerSupplier) _consumo).getCustomer()
//                                                        .getId();
//    } else {
//      companyCode = _consumo.getContract()
//                            .get()
//                            .getCompanyCode();
//    }
//    return customerValidator.customerActiveInDate(companyCode, _consumo.getDate());
//  }
//
//  private ValidationOutcome contractWasActiveInDate(final Consumo _consumo) {
//    if (_consumo.getContract() != null && _consumo.getContract()
//                                                  .isPresent()) {
//      return contractValidator.contractActiveInDate(_consumo.getContract()
//                                                            .get()
//                                                            .getId(),
//                                                    _consumo.getDate());
//    }
//    return null;
//  }
//
//  // - se l'abbinamento contratto cliente e dispositivo non era attivo alla data del consumo→ anomalia (non bloccante)
//  // - se il dispositivo era in stato disattivo alla data di transito + 24h → anomalia (non bloccante)
//  private ValidationOutcome deviceWasActiveAndAssociatedWithContractInDate(final Consumo _consumo) {
//    ValidationOutcome validationOutcome = null;
//    if (consumoHasDevice(_consumo)) {
//
//      if (_consumo.getContract() == null || !_consumo.getContract()
//                                                     .isPresent()) {
//        validationOutcome = new ValidationOutcome();
//        validationOutcome.addMessage(new Message("w004").withText("The consumo own device information but does not contain contract information"));
//        return validationOutcome;
//      }
//
//      final String deviceCode = ((DeviceSupplier) _consumo).getDevice()
//                                                           .getId();
//      final TipoDispositivoEnum tipoDispositivo = ((DeviceSupplier) _consumo).getDevice()
//                                                                             .getType();
//
//      var validationOutcomeOptional = _consumo.getContract()
//                                              .map(contract -> {
//                                                if (_consumo instanceof TrustedStatoDispositivo) {
//                                                  return deviceValidator.deviceWasAssociatedWithContractInDate(deviceCode, tipoDispositivo,
//                                                                                                               contract.getId(),
//                                                                                                               _consumo.getDate(), DateSupplier.VALIDATOR_DATE_OFFSET);
//                                                } else {
//                                                  return deviceValidator.deviceWasActiveAndAssociatedWithContractInDate(deviceCode,
//                                                                                                                        tipoDispositivo,
//                                                                                                                        contract.getId(),
//                                                                                                                        _consumo.getDate(),
//                                                    DateSupplier.VALIDATOR_DATE_OFFSET);
//                                                }
//                                              });
//      validationOutcome = validationOutcomeOptional.orElse(new ValidationOutcome());
//      if (validationOutcome != null) {
//        // controllo se devo inviare la notifica di tipo DispositiviBloccati che fatturano
//        if (validationOutcome.getMessages()
//                             .stream()
//                             .anyMatch(m -> m.getCode()
//                                             .equals("w003"))) {
//          // FIXME la data inviata è in UTC.. Corretto?
//          deviceValidator.sendDispositiviBloccatiFattureMessage(_consumo, deviceCode, tipoDispositivo);
//        }
//      }
//    }
//
//    return validationOutcome;
//  }
//
//  private ValidationOutcome serviceWasActiveInDate(final Consumo _consumo) {
//    if (_consumo instanceof DeviceSupplier && ((DeviceSupplier) _consumo).getDevice() != null) {
//      Device device = ((DeviceSupplier) _consumo).getDevice();
//      return deviceValidator.serviceWasActiveInDate(device, _consumo.getDate(), DateSupplier.VALIDATOR_DATE_OFFSET);
//    }
//    return null;
//  }
//
//  private static void throwsExceptionForConsumoWithoutContract(final Consumo _consumo) {
//    throw new IllegalStateException(_consumo.getClass()
//                                            .getSimpleName()
//                                    + " hasn't a contract");
//  }
}
