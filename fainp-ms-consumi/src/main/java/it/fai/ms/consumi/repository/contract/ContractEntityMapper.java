package it.fai.ms.consumi.repository.contract;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@Component
@Validated
public class ContractEntityMapper {

  public ContractEntityMapper() {
  }

  public Contract toDomain(@NotNull final StoricoContratto _storicoStatoContratto) {
    final var contract = new Contract(_storicoStatoContratto.getContrattoNumero());
    contract.setCompanyCode(_storicoStatoContratto.getCodiceAzienda());
    contract.setState(_storicoStatoContratto.getStatoContratto());
    return contract;
  }

}
