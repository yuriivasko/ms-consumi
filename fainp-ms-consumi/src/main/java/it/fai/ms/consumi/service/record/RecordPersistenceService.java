package it.fai.ms.consumi.service.record;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

public interface RecordPersistenceService {

  void persist(CommonRecord record);

}
