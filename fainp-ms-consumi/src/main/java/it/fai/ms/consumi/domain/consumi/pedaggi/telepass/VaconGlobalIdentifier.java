package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import java.util.UUID;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class VaconGlobalIdentifier extends GlobalIdentifier {

  public VaconGlobalIdentifier(final String _id, final GlobalIdentifierType _type) {
    super(_id, _type);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    GlobalIdentifier globalIDentifier = null;
    if (getType().equals(GlobalIdentifierType.INTERNAL)) {
      globalIDentifier = new VaconGlobalIdentifier(UUID.randomUUID()
                                                       .toString(),
                                                   GlobalIdentifierType.INTERNAL);
    } else {
      throw new IllegalStateException("newGlobalIdentifier operation is supported only for global identifier type internal");
    }
    return globalIDentifier;
  }

}
