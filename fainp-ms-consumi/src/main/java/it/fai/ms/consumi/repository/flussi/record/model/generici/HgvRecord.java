package it.fai.ms.consumi.repository.flussi.record.model.generici;

import java.util.UUID;

import javax.money.MonetaryAmount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;

public class HgvRecord
  extends CommonRecord
  implements GlobalIdentifierBySupplier {

  private static final long serialVersionUID = 5131682949601192520L;

  private String numeroImmatricolazioneVeicolo;
  private String paese;
  private String fascia;
  private String categoriaEuro;
  private String dataInizio;
  private String dataFine;
  private String importo;
  private String stato;
  private String dataAcquisto;
  private String acquistatoDa;

    private MonetaryAmount total_amount_value = null;

  private final String globalIdentifier;

  @JsonCreator
  public HgvRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID().toString();
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public String getNumeroImmatricolazioneVeicolo() {
    return numeroImmatricolazioneVeicolo;
  }

  public void setNumeroImmatricolazioneVeicolo(String numeroImmatricolazioneVeicolo) {
    this.numeroImmatricolazioneVeicolo = numeroImmatricolazioneVeicolo;
  }

  public String getPaese() {
    return paese;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public String getFascia() {
    return fascia;
  }

  public void setFascia(String fascia) {
    this.fascia = fascia;
  }

  public String getCategoriaEuro() {
    return categoriaEuro;
  }

  public void setCategoriaEuro(String categoriaEuro) {
    this.categoriaEuro = categoriaEuro;
  }

  public String getDataInizio() {
    return dataInizio;
  }

  public void setDataInizio(String dataInizio) {
    this.dataInizio = dataInizio;
  }

  public String getDataFine() {
    return dataFine;
  }

  public void setDataFine(String dataFine) {
    this.dataFine = dataFine;
  }

  public String getImporto() {
    return importo;
  }

  public void setImporto(String importo) {
    this.importo = importo;
    this.total_amount_value = MonetaryUtils.strToMonetary(importo, 1, "GBP");
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public String getDataAcquisto() {
    return dataAcquisto;
  }

  public void setDataAcquisto(String dataAcquisto) {
    this.dataAcquisto = dataAcquisto;
  }

  public String getAcquistatoDa() {
    return acquistatoDa;
  }

  public void setAcquistatoDa(String acquistatoDa) {
    this.acquistatoDa = acquistatoDa;
  }

  public MonetaryAmount getTotal_amount_value() {
    return total_amount_value;
  }

}
