package it.fai.ms.consumi.web.rest.util;

import org.springframework.http.HttpHeaders;

public class HeaderCustom {

  public static HeaderCustom build() {
    return new HeaderCustom();
  }

  private HttpHeaders headers = new HttpHeaders();

  public HeaderCustom() {
  }

  public HeaderCustom add(String headerName, Integer headerValue) {
    return this.add(headerName, headerValue.toString());
  }

  public HeaderCustom add(String headerName, Long headerValue) {
    return this.add(headerName, headerValue.toString());
  }

  public HeaderCustom add(String headerName, String headerValue) {
    headers.add(headerName, headerValue);
    return this;
  }

  public HttpHeaders headers() {
    return headers;
  }

  public HeaderCustom id(Long id) {
    return this.id(id != null ? id.toString() : "");
  }

  public HeaderCustom id(String id) {
    headers.add("X-id", id);
    return this;
  }

  public HeaderCustom rows(Integer rows) {
    return this.rows(rows != null ? rows.toString() : "");
  }

  public HeaderCustom rows(Long rows) {
    return this.rows(rows != null ? rows.toString() : "");
  }

  public HeaderCustom rows(String rows) {
    headers.add("X-rows", rows);
    return this;
  }

  public HeaderCustom uri(String uri) {
    headers.add("X-uri", uri);
    return this;
  }

}
