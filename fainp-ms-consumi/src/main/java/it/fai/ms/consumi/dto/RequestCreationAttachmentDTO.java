package it.fai.ms.consumi.dto;

import java.io.Serializable;

public class RequestCreationAttachmentDTO implements Serializable {

  private static final long serialVersionUID = 1269342075244138156L;

  private Long   id;
  private String codiceFattura;
  private String codiceRaggruppamentoArticoli;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public RequestCreationAttachmentDTO id(Long id) {
    this.id = id;
    return this;
  }

  public String getCodiceFattura() {
    return codiceFattura;
  }

  public void setCodiceFattura(String codiceFattura) {
    this.codiceFattura = codiceFattura;
  }

  public RequestCreationAttachmentDTO codiceFattura(String codiceFattura) {
    this.codiceFattura = codiceFattura;
    return this;
  }

  public String getCodiceRaggruppamentoArticoli() {
    return codiceRaggruppamentoArticoli;
  }

  public RequestCreationAttachmentDTO codiceRaggruppamentoArticoli(String codiceRaggruppamentoArticoli) {
    this.codiceRaggruppamentoArticoli = codiceRaggruppamentoArticoli;
    return this;
  }

  public void setCodiceRaggruppamentoArticoli(String codiceRaggruppamentoArticoli) {
    this.codiceRaggruppamentoArticoli = codiceRaggruppamentoArticoli;
  }

  @Override
  public String toString() {
    return "RequestCreationAttachmentDTO [id=" + id + ", codiceFattura=" + codiceFattura + ", codiceRaggruppamentoArticoli="
           + codiceRaggruppamentoArticoli + "]";
  }

}
