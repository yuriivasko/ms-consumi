package it.fai.ms.consumi.repository.viewallegati;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface ViewAllegatoGenericoSezione2Repository {


  List<ViewAllegatoGenericoSezione2Entity> findByDettaglioStanziamentoId(List<UUID> dettagliStanziamentoId);


}
