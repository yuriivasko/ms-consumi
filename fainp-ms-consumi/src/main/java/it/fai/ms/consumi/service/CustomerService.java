package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Customer;

public interface CustomerService {

  Optional<Customer> findCustomerByUuid(@NotNull String customerUuid);

  Optional<Customer> findCustomerByContrattoNumero(@NotNull String _contrattoNumero);

  Optional<Customer> findCustomerByDeviceSeriale(@NotNull String _deviceSeriale, @NotNull TipoDispositivoEnum _deviceType);

  boolean isCustomerActiveInDate(String _companyCode, Instant _date);

}
