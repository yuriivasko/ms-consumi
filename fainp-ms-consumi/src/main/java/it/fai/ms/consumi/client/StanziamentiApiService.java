package it.fai.ms.consumi.client;

import java.util.List;

import it.fai.ms.consumi.domain.navision.PricesByNavDTO;
import it.fai.ms.consumi.domain.navision.PricesByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.PutAllegatiByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.Stanziamento;

public interface StanziamentiApiService {

  void stanziamentiPost(String authorizationToken, List<Stanziamento> righe) throws StanziamentiPostException;

  PutAllegatiByNavReturnDTO putAllegatiByNav(String requestHeader, String numeroFattura, List<String> codiciAllegati);

  PricesByNavReturnDTO getPricesByNavPost(String requestHeader, PricesByNavDTO pricesByNavDTO);
}
