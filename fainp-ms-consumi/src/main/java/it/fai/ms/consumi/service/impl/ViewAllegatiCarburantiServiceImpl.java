package it.fai.ms.consumi.service.impl;

import static java.util.stream.Collectors.toList;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.fattura.CarburantiBodyDTO;
import it.fai.ms.common.jms.fattura.CarburantiSezione2;
import it.fai.ms.common.jms.fattura.GenericoConsumo;
import it.fai.ms.common.jms.fattura.HeaderDispositivo;
import it.fai.ms.common.jms.fattura.ReportDispositiviCarburantiDTO;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.device.TipoSupporto;
import it.fai.ms.consumi.repository.device.TipoSupportoRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiDetailsTypeEntity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2Repository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;
import it.fai.ms.consumi.service.ViewAllegatiCarburantiService;
import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;

@Service
@Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
public class ViewAllegatiCarburantiServiceImpl implements ViewAllegatiCarburantiService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final static String UNDERSCORE = "_";

  private final static String MINUS_SIGN  = "-";
  private final static double ZERO_DOUBLE = 0d;

  private final StanziamentiParamsRepository             stanziamentiParamsRepository;
  private final ViewAllegatoCarburantiSezione2Repository sezione2Repo;
  private final PetrolPumpService                        petrolPumpService;
  
  private final TipoSupportoRepository tipoSupportoRepo;

  public ViewAllegatiCarburantiServiceImpl(final ViewAllegatoCarburantiSezione2Repository _sezione2Repo,
                                           final StanziamentiParamsRepository _stanziamentiParamsRepository,
                                           final PetrolPumpService _petrolPumpService, final TipoSupportoRepository _tipoSupportoRepo) {
    sezione2Repo = _sezione2Repo;
    stanziamentiParamsRepository = _stanziamentiParamsRepository;
    petrolPumpService = _petrolPumpService;
    tipoSupportoRepo = _tipoSupportoRepo;
  }

  @Override
  public List<ViewAllegatoCarburantiSezione2Entity> getDettagliStanziamento(List<UUID> dettagliStanziamentoId) {
    log.debug("Search dettagli stanziamento to {}", dettagliStanziamentoId);
    List<ViewAllegatoCarburantiSezione2Entity> dataAllegatiCarburante = sezione2Repo.findByDettaglioStanziamentoId(dettagliStanziamentoId);
    log.debug("Found {} elements", dataAllegatiCarburante != null ? dataAllegatiCarburante.size() : 0);
    return dataAllegatiCarburante;
  }

  public List<CarburantiBodyDTO> createBodyMessageAndSection2(List<ViewAllegatoCarburantiSezione2Entity> viewAllegatiCarburante,
                                                              final RequestsCreationAttachments requestCreationAttachments) {
    final long tStart = System.currentTimeMillis();
    String nomeIntestazioneTessere = requestCreationAttachments.getNomeIntestazioneTessere();
    String nomeClienteTessere = StringUtils.isNotBlank(nomeIntestazioneTessere) ? nomeIntestazioneTessere : "";

    final String tipoServizio = requestCreationAttachments.getTipoServizio();
    final String nazioneFatturazione = requestCreationAttachments.getNazioneFatturazione();

    LinkedHashMap<String, List<ViewAllegatoCarburantiSezione2Entity>> mapContract = createMapByContratto(viewAllegatiCarburante);
    Set<String> codiciContratto = mapContract.keySet();
    log.info("Find {} codici contratto", ((codiciContratto != null) ? codiciContratto.size() : null));

    List<CarburantiBodyDTO> bodyWithSection2 = new ArrayList<>();
    bodyWithSection2 = codiciContratto.stream()
                                      .map(contratto -> {
                                        CarburantiBodyDTO allegatoBody = new CarburantiBodyDTO();

                                        allegatoBody.setContratto(contratto);
                                        allegatoBody.setNomeIntestazioneTessere(nomeClienteTessere);

                                        List<ReportDispositiviCarburantiDTO> reportDispositiviDTO = null;
                                        List<ViewAllegatoCarburantiSezione2Entity> dettagliAllegato = mapContract.get(contratto);
                                        if (dettagliAllegato != null && !dettagliAllegato.isEmpty()) {
                                          log.info("Size Dettagli: {}", dettagliAllegato.size());
                                          log.debug("Dettagli: {}", dettagliAllegato);

                                          Map<String, List<ViewAllegatoCarburantiSezione2Entity>> mapDispositivi = createMapByDispositivo(dettagliAllegato);
                                          Set<String> keyDispositivi = mapDispositivi.keySet();
                                          reportDispositiviDTO = keyDispositivi.stream()
                                                                               .map(keyDevice -> {
                                                                                 log.debug("Manage device key: {}", keyDevice);
                                                                                 ReportDispositiviCarburantiDTO reportDispositivi = new ReportDispositiviCarburantiDTO();
                                                                                 List<ViewAllegatoCarburantiSezione2Entity> list = mapDispositivi.get(keyDevice);
                                                                                 HeaderDispositivo headerDevice = createHeaderDispositivo(list.get(0),
                                                                                                                                          tipoServizio,
                                                                                                                                          nazioneFatturazione);
                                                                                 reportDispositivi.setHeaderDispositivo(headerDevice);
                                                                                 List<CarburantiSezione2> consumi = list.stream()
                                                                                                                        .map(d -> {
                                                                                                                          log.debug("mapToCarburantiSezione2 from Dettaglio: {}",
                                                                                                                                    d);
                                                                                                                          CarburantiSezione2 sezione2 = mapToSezione2(d);
                                                                                                                          return sezione2;
                                                                                                                        })
                                                                                                                        .collect(toList());
                                                                                 reportDispositivi.setConsumi(consumi);
                                                                                 return reportDispositivi;
                                                                               })
                                                                               .collect(toList());
                                        } else {
                                          log.warn("Not found dettagli to contratto: {} and dettagli stanziamento: {}", contratto,
                                                   dettagliAllegato);
                                        }
                                        allegatoBody.setReportDispositiviCarburantiDTO(reportDispositiviDTO);
                                        return (allegatoBody);
                                      })
                                      .collect(toList());

    log.info("Created body with section 2 in {} ms", System.currentTimeMillis() - tStart);
    return bodyWithSection2;
  }

  private LinkedHashMap<String, List<ViewAllegatoCarburantiSezione2Entity>> createMapByContratto(final List<ViewAllegatoCarburantiSezione2Entity> viewAllegatiCarburante) {
    LinkedHashMap<String, List<ViewAllegatoCarburantiSezione2Entity>> map = new LinkedHashMap<>();
    map = viewAllegatiCarburante.stream()
                                .collect(Collectors.groupingBy(ViewAllegatoCarburantiSezione2Entity::getCodiceContratto, LinkedHashMap::new,
                                                               Collectors.toList()));
    return map;
  }

  private Map<String, List<ViewAllegatoCarburantiSezione2Entity>> createMapByDispositivo(List<ViewAllegatoCarburantiSezione2Entity> dettagli) {
    LinkedHashMap<String, List<ViewAllegatoCarburantiSezione2Entity>> map = new LinkedHashMap<>();
    map = dettagli.stream()
                  .collect(Collectors.groupingBy(d -> composeKey(d), LinkedHashMap::new, Collectors.toList()));
    return map;
  }

  private String composeKey(ViewAllegatoCarburantiSezione2Entity d) {
    String codiceContratto = d.getCodiceContratto();
    String tipoDispositivo = d.getTipoDispositivo();
    String serialNumber = d.getSerialNumber();
    String targaVeicolo = d.getTargaVeicolo();
    String nazioneVeicolo = d.getNazioneVeicolo();
    String classificazioneEuro = d.getClassificazioneEuro();
    return String.join(UNDERSCORE, codiceContratto, tipoDispositivo, serialNumber, targaVeicolo, nazioneVeicolo, classificazioneEuro);
  }

  private HeaderDispositivo createHeaderDispositivo(final ViewAllegatoCarburantiSezione2Entity entity, final String tipoServizio,
                                                    final String nazioneFatturazione) {
    HeaderDispositivo headerDispositivo = new HeaderDispositivo();
    String tipoDispositivo = entity.getTipoDispositivo();
    headerDispositivo.setTipoDispositivo(tipoDispositivo);
    headerDispositivo.setPanNumber(entity.getSerialNumber());
    headerDispositivo.setTargaVeicolo(entity.getTargaVeicolo());
    headerDispositivo.setNazioneVeicolo(entity.getNazioneVeicolo());
    headerDispositivo.setClassificazioneEuro(entity.getClassificazioneEuro());

    if (StringUtils.isNotBlank(tipoDispositivo)) {
      headerDispositivo.setTipoSupportoFix(getTipoSupportoByNomeTipoDispositivo(tipoDispositivo));
    } else {
      log.warn("Not found TipoDispositivo on consumo carburante, so not set TipoSupportoFix...");
    }

    headerDispositivo.setTipoServizio(tipoServizio);
    headerDispositivo.setNazioneFatturazione(nazioneFatturazione);
    log.debug("Header Dispositivo: {}", headerDispositivo);
    return headerDispositivo;
  }
  
  private String getTipoSupportoByNomeTipoDispositivo(String tipoDispositivo) {
    String tipoSupporto = "";
    log.debug("Get TipoSupporto by TipoDispositivo: {}", tipoDispositivo);
    if (StringUtils.isNotBlank(tipoDispositivo)) {
      TipoSupporto deviceType = tipoSupportoRepo.findByTipoDispositivo(tipoDispositivo);
      if (deviceType != null) {
        tipoSupporto = deviceType.getTipoSupportoFix();
      }
    }
    log.debug("Found tipoSupporto: {}", tipoSupporto);
    return tipoSupporto;
  }

  private CarburantiSezione2 mapToSezione2(final ViewAllegatoCarburantiSezione2Entity dettaglio) {
    CarburantiSezione2 sezione2 = new CarburantiSezione2();
    Instant dataOraUtilizzo = dettaglio.getDataOraUtilizzo();
    LocalDate dataUtilizzo = FaiConsumiDateUtil.convertToLocalDate(dataOraUtilizzo);
    sezione2.setData(dataUtilizzo);
    LocalTime oraUtilizzo = FaiConsumiDateUtil.convertToLocalTime(dataOraUtilizzo);
    sezione2.setOra(oraUtilizzo);

    sezione2.setTransactionDetailCode(dettaglio.getTransactionDetailCode());
    setProdottoInformation(dettaglio, sezione2);

    sezione2.setErogatore(dettaglio.getErogatore());
    setCodiceDescrizioneCittaPuntoErogazione(dettaglio.getPuntoErogazione(), sezione2);

    Double quantita = dettaglio.getQuantita() != null ? dettaglio.getQuantita() : ZERO_DOUBLE;
    sezione2.setQuantita(quantita);

    Double prezzoUnitario = dettaglio.getImporto() != null ? dettaglio.getImporto() : ZERO_DOUBLE;
    String signTransaction = dettaglio.getSignTransaction();
    if (prezzoUnitario != ZERO_DOUBLE && StringUtils.isNotBlank(signTransaction) && MINUS_SIGN.equals(signTransaction.trim())) {
      prezzoUnitario = Double.parseDouble(signTransaction.trim() + prezzoUnitario);
      log.debug("unit price with sign: {}", prezzoUnitario);
    }
    sezione2.setPrezzoUnitario(prezzoUnitario);
    Double imponibile = (dettaglio.getQuantita() != null ? dettaglio.getQuantita() : ZERO_DOUBLE) * (dettaglio.getImporto());
    sezione2.setImponibile(imponibile);

    sezione2.setPercIva(dettaglio.getPercIva());
    sezione2.setValuta(dettaglio.getValuta());
    sezione2.setCambio(dettaglio.getCambio());
    sezione2.setUnitaMisura(dettaglio.getUnitaMisura());
    sezione2.setKm(dettaglio.getKm());
    sezione2.setTipoConsumo(dettaglio.getTipoConsumo());
    sezione2.setCentroDiCosto(null);
    log.debug("Sezione 2 generated: {}", sezione2);
    return sezione2;
  }

  private void setCodiceDescrizioneCittaPuntoErogazione(String puntoErogazione, CarburantiSezione2 sezione2) {
	PointDTO point = null;
	try {
		point = findPoint(puntoErogazione);
	}catch (Throwable e) {
	  String messageError = String.format("Not found point: %s", puntoErogazione);
	  log.error(messageError);
		throw new RuntimeException(messageError, e);
	}
    log.debug("Found point: {}", point);
    sezione2.setPuntoErogazione(puntoErogazione);
    if (point != null) {
      String description = point.getName() != null ? point.getName() : "";
      sezione2.setDescrizionePuntoErogazione(description);
      String city = point != null ? point.getCity() : "";
      sezione2.setCittaPuntoErogazione(city);
    } else {
      log.error("Not found point on micrservice petrolpumo by punto erogazione: {}", puntoErogazione);
      throw new IllegalArgumentException("Not found point on petrolpump by PuntoErogazione: " + puntoErogazione);
    }
  }
  
  private PointDTO findPoint(String puntoErogazione) {
    PointDTO point = petrolPumpService.getPoint(puntoErogazione);
    return point;
  }

  private void setProdottoInformation(ViewAllegatoCarburantiSezione2Entity d, CarburantiSezione2 sezione2) {
    String prodotto = d.getTransactionDetailCode();
    String codiceFornitore = d.getCodiceFornitore();
    String recordCode = d.getRecordCode();
    StanziamentiDetailsTypeEntity detailType = StanziamentiDetailsTypeEntity.CARBURANTI;
    Optional<StanziamentiParams> optParamStanziamenti = findProductByParamStanziamenti(prodotto, codiceFornitore, recordCode, detailType);
    if (optParamStanziamenti.isPresent()) {
      StanziamentiParams stanziamentiParams = optParamStanziamenti.get();
      sezione2.setCodiceProdotto(stanziamentiParams.getArticle()
                                                   .getCode());
      sezione2.setDescrizioneProdotto(stanziamentiParams.getArticle()
                                                        .getDescription());
    } else {
      StringBuilder sb = new StringBuilder("Not found param stanziamenti by:\n");
      sb.append("TransactionDetailCode: ")
        .append(prodotto)
        .append("\n");
      sb.append("Codice Fornitore: ")
        .append(codiceFornitore)
        .append("\n");
      sb.append("Record Code: ")
        .append(recordCode)
        .append("\n");
      sb.append("Tipo Dettaglio: ")
        .append(detailType)
        .append("\n");
      log.error(sb.toString());
      throw new RuntimeException(sb.toString());
    }
  }

  private Optional<StanziamentiParams> findProductByParamStanziamenti(String transactionDetail, String codiceFornitore, String recordCode,
                                                                      StanziamentiDetailsTypeEntity detailType) {
    return stanziamentiParamsRepository.findByTransactionDetailAndCodiceFornitoreAndRecordCodeAndTipoDettaglio(transactionDetail,
                                                                                                               codiceFornitore, recordCode,
                                                                                                               detailType);
  }

}
