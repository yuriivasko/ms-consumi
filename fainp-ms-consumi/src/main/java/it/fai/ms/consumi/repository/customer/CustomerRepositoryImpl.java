package it.fai.ms.consumi.repository.customer;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.repository.storico_dml.StoricoContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto_;

@Repository
@Validated
@Transactional
public class CustomerRepositoryImpl implements CustomerRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final EntityManager        em;
  private final CustomerEntityMapper entityMapper;
  private final StoricoContrattoRepository repo;

  @Inject
  public CustomerRepositoryImpl(final EntityManager _entityManager, final CustomerEntityMapper _entityMapper,StoricoContrattoRepository storicoContrattoRepository) {
    em = _entityManager;
    entityMapper = _entityMapper;
    this.repo = storicoContrattoRepository;
  }

  @Override
  public Optional<Customer> findCustomerByContrattoNumero(@NotNull final String _contrattoNumero) {

    _log.debug(" Searching {} by id {} ...", Customer.class.getSimpleName(), _contrattoNumero);

    var optionalCustomer = repo.findFirstByContrattoNumeroOrderByDataVariazioneDesc(_contrattoNumero)
                             .map(storicoStatoContratto -> entityMapper.toDomain(storicoStatoContratto));

    _log.debug(" Found : {}", optionalCustomer);

    return optionalCustomer;
  }

  @Override
  @Cacheable(value = "storicoContrattiCustomer", key = "{#root.methodName, #root.args[0]}")
  public Optional<Customer> findCustomerByUuid(@NotNull final String _customerId) {

    _log.debug(" Searching {} by id {} ...", Customer.class.getSimpleName(), _customerId);

    var query = em.getCriteriaBuilder()
                  .createQuery(StoricoContratto.class);
    var root = query.from(StoricoContratto.class);

    final var predicates = List.of(em.getCriteriaBuilder()
                                     .equal(root.get(StoricoContratto_.CODICE_AZIENDA), _customerId))
                               .toArray(new Predicate[0]);

    query.where(predicates);
    query.orderBy(em.getCriteriaBuilder()
                    .desc(root.get(StoricoContratto_.DATA_VARIAZIONE)));

    var optionalCustomer = em.createQuery(query)
        .setFirstResult(0) // offset
        .setMaxResults(1)
                             .getResultStream()
                             .map(storicoStatoContratto -> entityMapper.toDomain(storicoStatoContratto))
                             .findFirst();

    _log.debug(" Found : {}", optionalCustomer);

    return optionalCustomer;
  }

}
