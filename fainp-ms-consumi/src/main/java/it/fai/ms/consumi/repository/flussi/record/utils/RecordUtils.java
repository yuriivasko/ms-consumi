package it.fai.ms.consumi.repository.flussi.record.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.repository.flussi.file.utils.EmptyLineUnawareReversedLinesFileReader;

public class RecordUtils {

  private static final Logger log = LoggerFactory.getLogger(RecordUtils.class);

  private RecordUtils() {
  }

  public static long checkFileConsistency(String filename, Charset fileCharset, String footerStartsWith, int startPositionRecordNum,
                                          int endPositionRecordNum, int linesToSubtractToMatchDeclaration) throws NotValidFileException {
    Map<Integer, Integer> startEndMap = new HashMap<>();
    startEndMap.put(startPositionRecordNum, endPositionRecordNum);
    return checkFileConsistency(filename, fileCharset, footerStartsWith, startEndMap, linesToSubtractToMatchDeclaration);

  }

  private static long sumPositions(String footer,
                                   Set<Entry<Integer, Integer>> startStopPositionsForRecordNumToSum) throws NumberFormatException {
    AtomicLong declaredTotalRows = new AtomicLong(0);
    startStopPositionsForRecordNumToSum.stream()
                                       .forEach(couple -> {
                                         String totalRowsDeclaredInFooterAsString = footer.substring(couple.getKey(), couple.getValue());
                                         declaredTotalRows.set(declaredTotalRows.get() + Long.parseLong(totalRowsDeclaredInFooterAsString));
                                       });
    return declaredTotalRows.get();
  }

  public static long checkFileConsistency(String filename, Charset fileCharset, String footerStartsWith,
                                          Map<Integer, Integer> startStopPositionsForRecordNumToSum,
                                          int linesToSubtractToMatchDeclaration) throws NotValidFileException {
    log.debug("checkFileConsistency: filename={}; footerStartsWith={}; startStopPositionsForRecordNumToSum={}, linesToSubtractToMatchDeclaration={}",
              filename, footerStartsWith, startStopPositionsForRecordNumToSum, linesToSubtractToMatchDeclaration);

    long countOfLines = 0;
    var firstEmpyLinesCounter = new AtomicLong(0);
    long totalLinesWithoutTopEmptyLines = 0;
    final Set<Entry<Integer, Integer>> startStopPositions = startStopPositionsForRecordNumToSum.entrySet();
    if (footerStartsWith == null || (startStopPositions.stream()
                                                                        .anyMatch(couple -> couple.getValue() <= couple.getKey()))) {
      throw new IllegalStateException("footerStartsWith cannot be null and endPositionRecordNum must be greater than startPositionRecordNum");
    }
    final StringBuilder numeberBetweemStringBuffMessage = new StringBuilder();
    startStopPositions.stream()
                                       .forEach(couple -> numeberBetweemStringBuffMessage.append(couple.getKey() + ":" + couple.getValue()
                                                                                                 + " "));
    try {
      Path path = Paths.get(new File(filename).getPath());
      countOfLines = Files.lines(path,fileCharset).count();

      Files.lines(Paths.get(new File(filename).getPath()),fileCharset)
           .map(line -> line.trim())
           .allMatch(line -> {
             if (line.isEmpty()) {
               firstEmpyLinesCounter.getAndIncrement();
               return true;
             }
             return false;
           });
      totalLinesWithoutTopEmptyLines = countOfLines - firstEmpyLinesCounter.get();
    } catch (IOException e) {
      throw new NotValidFileException("problems during reading file", e);
    }
    if (countOfLines < 1) {
      throw new NotValidFileException("File must me at leat one line!!");
    }

    log.info("File {} has {} lines", filename, countOfLines);
    try (EmptyLineUnawareReversedLinesFileReader in = new EmptyLineUnawareReversedLinesFileReader(new File(filename), fileCharset)) {
      long bottonEmptyLines = 0;
      while (true) {
        String line = in.readLine();
        if (line != null) {
          line = line.trim();
          if (line.isEmpty()) {
            bottonEmptyLines++;
            continue;
          } else if (line.startsWith(footerStartsWith)) {
            long declaredTotalRows = 0;
            final String footer = line;
            if (startStopPositions.stream()
                                                   .allMatch(couple -> couple.getValue() <= footer.length())) {
              try {
                declaredTotalRows = sumPositions(footer, startStopPositions);
              } catch (NumberFormatException nfe) {
                throw new NotValidFileException("Footer declares a non parsable number between "
                                                + numeberBetweemStringBuffMessage.toString());
              }
            } else {
              throw new NotValidFileException("Footer should have total record number specified between "
                                              + numeberBetweemStringBuffMessage.toString());
            }

            if (declaredTotalRows < 1) {
              throw new NotValidFileException("Footer declares less than a record in file?? Please check footer between "
                                              + numeberBetweemStringBuffMessage.toString());
            }
            long totalDetailRowsInFile = totalLinesWithoutTopEmptyLines - bottonEmptyLines - linesToSubtractToMatchDeclaration;
            if (totalDetailRowsInFile < 0) {
              throw new IllegalStateException(String.format("Programming error %s - %s - %s < 0 !!! ", totalLinesWithoutTopEmptyLines,
                                                            bottonEmptyLines, linesToSubtractToMatchDeclaration));
            }
            if (totalDetailRowsInFile != declaredTotalRows) {
              throw new NotValidFileException(String.format("Footer declares %s detail rows while we counted %s !!", declaredTotalRows,
                                                            totalDetailRowsInFile));
            }
            // file is fine.
            return totalDetailRowsInFile;
          } else {
            throw new NotValidFileException("Footer missing: last not empty row does not start with " + footerStartsWith);
          }
        } else {
          throw new NotValidFileException("File reached top with no line??");
        }
      } // elihw
    } catch (IOException ioe) {
      throw new NotValidFileException("problems during reading file in reverse " + ioe.getMessage(), ioe);
    }

  }

  public static class NotValidFileException extends RuntimeException {

    private static final long serialVersionUID = -2421192479153085563L;

    public NotValidFileException(String message) {
      super(message);
    }

    public NotValidFileException() {
      super();
    }

    public NotValidFileException(String message, Throwable cause) {
      super(message, cause);
    }

    public NotValidFileException(Throwable cause) {
      super(cause);
    }

  }

}
