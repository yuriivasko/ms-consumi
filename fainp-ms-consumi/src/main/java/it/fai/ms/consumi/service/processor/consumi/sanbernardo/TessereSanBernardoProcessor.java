package it.fai.ms.consumi.service.processor.consumi.sanbernardo;

import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface TessereSanBernardoProcessor extends ConsumoProcessor<SanBernardo>{
}
