package it.fai.ms.consumi.service.processor.consumi.tokheim;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.consumi.carburante.TokheimConsumo;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Service
public class TokehimConsumoDettaglioStanziamentoMapper extends ConsumoDettaglioStanziamentoMapper<TokheimConsumo, DettaglioStanziamentoCarburante> {

  @Override
  public DettaglioStanziamentoCarburante mapConsumoToDettaglioStanziamento(@NotNull TokheimConsumo consumo) {
    return toDettaglioStanziamentoCarburanti(consumo, consumo.getGlobalIdentifier());
  }

}
