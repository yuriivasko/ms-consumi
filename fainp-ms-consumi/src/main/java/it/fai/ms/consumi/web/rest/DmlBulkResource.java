package it.fai.ms.consumi.web.rest;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoOrdiniCliente;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoRichiesta;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;
import it.fai.ms.consumi.service.DmlBulkService;
import it.fai.ms.consumi.service.jms.producer.StoricoJmsProducer;
import it.fai.ms.consumi.web.rest.util.HeaderCustom;

@RestController
@RequestMapping(DmlBulkResource.BASE_PATH)
public class DmlBulkResource extends AbstractResource {

  public static final String DISPOSITIVI = "/public/dmlbulk/dispositivi";
  public static final String DISPOSITIVI_AZIENDA = DISPOSITIVI + "/azienda";

  public static final String ORDINI = "/public/dmlbulk/ordini";
  public static final String ORDINI_AZIENDA = ORDINI + "/azienda";

  public static final String CONTRATTI = "/public/dmlbulk/contratti";
  public static final String CONTRATTI_AZIENDA = CONTRATTI + "/azienda";

  public static final String RICHIESTE = "/public/dmlbulk/richieste";
  public static final String RICHIESTE_AZIENDA = RICHIESTE + "/azienda";

  public static final String CLASSE_EURO = "/public/dmlbulk/classe-euro";
  public static final String CLASSE_EURO_AZIENDA = CLASSE_EURO + "/azienda";

  private final static int PAGE_SIZE = 100;

  private final DmlBulkService dmlBulkService;
  private final StoricoJmsProducer storicoJmsProducer;

  public DmlBulkResource(StoricoJmsProducer _storicoJmsProducer, DmlBulkService _dmlBulkService) {
    storicoJmsProducer = _storicoJmsProducer;
    dmlBulkService = _dmlBulkService;
  }

  @PostMapping(DISPOSITIVI)
  @Async
  public ResponseEntity<Void> sendAllDevices() throws Exception {
    log.info("REST request to send bulk dml message for Devices");

    Pageable pageable = PageRequest.of(0, PAGE_SIZE);
    while (pageable != null) {
      Page<StoricoDispositivo> page = dmlBulkService.findAllDevices(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      page.stream()
      .forEach(r -> {
        storicoJmsProducer.send(r);
      });
    }

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + DISPOSITIVI)
                 .headers())
        .body(null);
  }

  @PostMapping(DISPOSITIVI_AZIENDA + "/{codice}")
  @Async
  public ResponseEntity<Void> sendAllDevicesByCustomerCode(@PathVariable(name = "codice", required = true) String codice) throws Exception {
    log.info("REST request to send bulk dml message for Devices by customer code : {}", codice);

    Optional.ofNullable(dmlBulkService.findAllDevicesByCustomerCode(codice))
    .map(Collection::stream)
    .orElseGet(Stream::empty)
    .forEach(row -> {
      storicoJmsProducer.send(row);
    });

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + DISPOSITIVI_AZIENDA + "/" + codice)
                 .headers())
        .body(null);
  }

  @PostMapping(ORDINI)
  @Async
  public ResponseEntity<Void> sendAllCustomerOrder() throws Exception {
    log.info("REST request to send bulk dml message for Customer Order");

    Pageable pageable = PageRequest.of(0, PAGE_SIZE);
    while (pageable != null) {
      Page<StoricoOrdiniCliente> page = dmlBulkService.findAllCustomerOrder(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      page.stream()
      .forEach(r -> {
        storicoJmsProducer.send(r);
      });
    }

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + ORDINI)
                 .headers())
        .body(null);
  }

  @PostMapping(ORDINI_AZIENDA + "/{codice}")
  @Async
  public ResponseEntity<Void> sendAllCustomerOrderByCustomerCode(@PathVariable(name = "codice", required = true) String codice) throws Exception {
    log.info("REST request to send bulk dml message for Customer Order by customer code : {}", codice);

    Optional.ofNullable(dmlBulkService.findAllCustomerOrderByCustomerCode(codice))
    .map(Collection::stream)
    .orElseGet(Stream::empty)
    .forEach(row -> {
      storicoJmsProducer.send(row);
    });

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + ORDINI_AZIENDA + "/" + codice)
                 .headers())
        .body(null);
  }

  @PostMapping(RICHIESTE)
  @Async
  public ResponseEntity<Void> sendAllRequest() throws Exception {
    log.info("REST request to send bulk dml message for Requests");

    Pageable pageable = PageRequest.of(0, PAGE_SIZE);
    while (pageable != null) {
      Page<StoricoRichiesta> page = dmlBulkService.findAllRequest(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      page.stream()
      .forEach(r -> {
        storicoJmsProducer.send(r);
      });
    }

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + RICHIESTE)
                 .headers())
        .body(null);
  }

  @PostMapping(RICHIESTE_AZIENDA + "/{codice}")
  @Async
  public ResponseEntity<Void> sendAllRequestByCustomerCode(@PathVariable(name = "codice", required = true) String codice) throws Exception {
    log.info("REST request to send bulk dml message for Requests by customer code : {}", codice);

    Optional.ofNullable(dmlBulkService.findAllRequestByCustomerCode(codice))
    .map(Collection::stream)
    .orElseGet(Stream::empty)
    .forEach(row -> {
      storicoJmsProducer.send(row);
    });

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + RICHIESTE_AZIENDA + "/" + codice)
                 .headers())
        .body(null);
  }

  @PostMapping(CONTRATTI)
  @Async
  public ResponseEntity<Void> sendAllContractStatus() throws Exception {
    log.info("REST request to send bulk dml message for Contract Status");

    Pageable pageable = PageRequest.of(0, PAGE_SIZE);
    while (pageable != null) {
      Page<StoricoContratto> page = dmlBulkService.findAllContractStatus(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      page.stream().
      filter(r -> r!=null)
      .forEach(r -> {
        storicoJmsProducer.send(r);
      });
    }

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + CONTRATTI)
                 .headers())
        .body(null);
  }

  @PostMapping(CONTRATTI_AZIENDA + "/{codice}")
  @Async
  public ResponseEntity<Void> sendAllContractStatusByCustomerCode(@PathVariable(name = "codice", required = true) String codice) throws Exception {
    log.info("REST request to send bulk dml message for Requests by customer code : {}", codice);

    Optional.ofNullable(dmlBulkService.findAllContractStatusByCustomerCode(codice))
    .map(Collection::stream)
    .orElseGet(Stream::empty)
    .forEach(row -> {
      storicoJmsProducer.send(row);
    });

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + CONTRATTI_AZIENDA + "/" + codice)
                 .headers())
        .body(null);
  }

  @PostMapping(CLASSE_EURO)
  @Async
  public ResponseEntity<Void> sendAllEuroClass() throws Exception {
    log.info("REST request to send bulk dml message for Euro Classes");

    Pageable pageable = PageRequest.of(0, PAGE_SIZE);
    while (pageable != null) {
      Page<StoricoVeicolo> page = dmlBulkService.findAllVehicle(pageable);
      pageable = page.hasNext() ? page.nextPageable() : null;
      page.stream()
      .forEach(r -> {
        storicoJmsProducer.send(r);
      });
    }

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + CLASSE_EURO)
                 .headers())
        .body(null);
  }

  @PostMapping(CLASSE_EURO_AZIENDA)
  @Async
  public ResponseEntity<Void> sendAllEuroClassByCustomerCode(@PathVariable(name = "codice", required = true) String codice) throws Exception {
    log.info("REST request to send bulk dml message for Euro Classes by customer code : {}", codice);

    Optional.ofNullable(dmlBulkService.findAllEuroClassByCustomerCode(codice))
    .map(Collection::stream)
    .orElseGet(Stream::empty)
    .forEach(row -> {
      storicoJmsProducer.send(row);
    });

    return ResponseEntity.ok()
        .headers(HeaderCustom.build()
                 .uri(BASE_PATH + CLASSE_EURO_AZIENDA + "/" + codice)
                 .headers())
        .body(null);
  }

}
