package it.fai.ms.consumi.service.processor.consumi.trx;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiOil;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.processor.ProcessingResult;

public interface TrxOilProcessor {

  ProcessingResult validateAndProcess(StanziamentiParams stanziamentiParams,
                                      TrackyCardCarburantiOil trackyCardCarburantiOil,
                                      Bucket bucket);
}
