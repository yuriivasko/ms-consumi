package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.AsfinagRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class AsfinagRecordDescriptor //Go box - Pedaggi Austria
  implements RecordDescriptor<AsfinagRecord> {

  private static final transient Logger         log          = LoggerFactory.getLogger(AsfinagRecordDescriptor.class);

  public final static String HEADER_BEGIN      = "10";
  public final static String BEGIN_20          = "20";
  public final static String FOOTER_CODE_BEGIN = "90";

  // Descrittore INTESTAZIONI B.1.2 intestazioni
  public final static FileDescriptor intestazioni = new FileDescriptor();

  public static final String         counter      = "conteggioRecord";

  static {
    intestazioni.addItem("recordCode", 1, 2);
    intestazioni.addItem("transactionDetail", 3, 4);
    intestazioni.skipItem("idMittente", 5, 10);
    intestazioni.skipItem("idRicevente", 11, 16);
    intestazioni.addItem("creationDateInFile", 17, 24); // dataCreazioneFile
    intestazioni.addItem("creationTimeInFile", 25, 28); // tempoCreazioneFile
    intestazioni.addItem("numeroCorsa", 29, 32);
    intestazioni.skipItem("codiceValuta", 33, 35);
    intestazioni.skipItem("descrizione", 36, 60);
    intestazioni.skipItem("paeseIsoCode", 61, 63);

    //intestazioni.addItem("filler", 64, 183);
  }

  // Descrittore RECORD B.1.3 Corpo
  public final static FileDescriptor descriptor02 = new FileDescriptor();

  static {
    descriptor02.addItem("recordCode", 1, 2);
    descriptor02.addItem("transactionDetail", 3, 4);

    descriptor02.skipItem("filler_1", 5, 6);

    descriptor02.skipItem("idSito", 7, 21);
    descriptor02.addItem("numeroCorsa", 22, 25);

    descriptor02.addItem("dataCreazioneFile", 26, 33);

    descriptor02.addItem("numeroBollaConsegna", 35, 42);

    descriptor02.addItem("numeroCarta", 43, 64);
    descriptor02.addItem("dataConsegna", 65, 72);
    descriptor02.addItem("tempoConsegna", 73, 76);
    descriptor02.addItem("tipoTransazione", 77, 78);
    descriptor02.addItem("codiceProdotto", 79, 82);

    descriptor02.addItem("km", 83, 89);
    descriptor02.addItem("driverId", 90, 93);
    descriptor02.addItem("quantitaProdotto", 94, 99);

    descriptor02.addItem("importoIvaIncl", 100, 109);
    descriptor02.addItem("importoIvaEscl", 110, 119);
    descriptor02.addItem("numeroRicevuta", 120, 139);
    descriptor02.addItem("campoInformazione", 140, 164);
    descriptor02.addItem("aliquotaIva", 165, 168);
    descriptor02.addItem("importoIva", 169, 178);
    //descriptor02.addItem("filler", 179, 183);
  }

  // Descrittore FOOTER - B.1.4 Trailer
  public final static FileDescriptor descriptor03 = new FileDescriptor();

  static {
    descriptor03.addItem("recordCode", 1, 2);
    descriptor03.addItem("transactionDetail", 3, 4);
    descriptor03.addItem(counter, 5, 13);
    descriptor03.skipItem("checksumLotto", 14, 28);
    descriptor03.skipItem("checksumImporto", 29, 43);
//    descriptor03.addItem("filler", 44, 183);
  }

  @Override
  public String extractRecordCode(String _data){
    return StringUtils.left(_data, 2);
  }

  @Override
  public AsfinagRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {

    final AsfinagRecord entity = new AsfinagRecord(fileName, rowNumber);
    switch (_recordCode) {
    case HEADER_BEGIN:
      entity.setFromString(AsfinagRecordDescriptor.intestazioni.getItems(), _data);
      break;
    case BEGIN_20:
      entity.setFromString(AsfinagRecordDescriptor.descriptor02.getItems(), _data);
      break;
    case FOOTER_CODE_BEGIN:
      entity.setFromString(AsfinagRecordDescriptor.descriptor03.getItems(), _data);
      break;
    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    Optional<StringItem> stringItem = AsfinagRecordDescriptor.descriptor03.getItem(AsfinagRecordDescriptor.counter);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in AsfinagRecordDescriptor!");
    }
    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return AsfinagRecordDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return AsfinagRecordDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(AsfinagRecordDescriptor.class, AsfinagRecord.class).checkConfiguration();
  }
}
