package it.fai.ms.consumi.repository.storico_dml.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.mappable.DmlListenerMapper;
import it.fai.ms.common.dml.vehicle.dto.VehicleDMLDTO;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

@Component
public class StoricoVeicoloDmlListenerMapper
  implements DmlListenerMapper<VehicleDMLDTO, StoricoVeicolo> {

  @Override
  public StoricoVeicolo toORD(VehicleDMLDTO dml, StoricoVeicolo ord) {

    if(ord == null){
      ord = new StoricoVeicolo();
    }

    ord.setClasseEuro(dml.getEuro());
    ord.setDmlRevisionTimestamp(dml.getDmlRevisionTimestamp());
    ord.setNazione(dml.getNazione());
    ord.setTarga(dml.getTarga());
    ord.setDmlUniqueIdentifier(dml.getDmlUniqueIdentifier());

    return ord;
  }
}
