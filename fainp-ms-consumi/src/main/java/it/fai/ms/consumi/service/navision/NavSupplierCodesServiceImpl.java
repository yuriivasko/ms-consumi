package it.fai.ms.consumi.service.navision;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.adapter.nav.NavSupplierCodesAdapter;

@Service
public class NavSupplierCodesServiceImpl implements NavSupplierCodesService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final NavSupplierCodesAdapter navSupplierCodesAdapter;

  @Inject
  public NavSupplierCodesServiceImpl(final NavSupplierCodesAdapter _navSupplierCodesAdapter) {
    navSupplierCodesAdapter = _navSupplierCodesAdapter;
  }

  @Override
  public Set<String> getAllCodes() {
    Set<String> supplierCodes = new HashSet<>();

    _log.info("Retrieve all supplier codes");

    supplierCodes = navSupplierCodesAdapter.getAllCodes();

    _log.info("retrieve {} suppliers code", supplierCodes.size());
    _log.trace("All supplier codes : {}", supplierCodes);

    return supplierCodes;
  }

}
