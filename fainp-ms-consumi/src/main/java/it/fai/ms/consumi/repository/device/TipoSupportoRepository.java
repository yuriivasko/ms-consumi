package it.fai.ms.consumi.repository.device;

public interface TipoSupportoRepository {

  TipoSupporto findByTipoDispositivo(final String tipoDispositivo);

}
