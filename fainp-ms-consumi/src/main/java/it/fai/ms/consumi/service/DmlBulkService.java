package it.fai.ms.consumi.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoOrdiniCliente;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoRichiesta;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

public interface DmlBulkService {

  final Logger _log = LoggerFactory.getLogger(DmlBulkService.class);

  Page<StoricoDispositivo> findAllDevices(Pageable pageable);

  List<StoricoDispositivo> findAllDevicesByCustomerCode(String code);

  Page<StoricoOrdiniCliente> findAllCustomerOrder(Pageable pageable);
  
  List<StoricoOrdiniCliente> findAllCustomerOrderByCustomerCode(String code);

  Page<StoricoRichiesta> findAllRequest(Pageable pageable);

  List<StoricoRichiesta> findAllRequestByCustomerCode(String code);

  Page<StoricoContratto> findAllContractStatus(Pageable pageable);

  List<StoricoContratto> findAllContractStatusByCustomerCode(String code);

  Page<StoricoVeicolo> findAllVehicle(Pageable pageable);

  List<StoricoVeicolo> findAllEuroClassByCustomerCode(String code);

}
