package it.fai.ms.consumi.config;

import java.util.List;
import java.util.Objects;

public class OptionalVatConfiguration {

  private String source;
  private List<String> legacyCodes;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public List<String> getLegacyCodes() {
    return legacyCodes;
  }

  public void setLegacyCodes(List<String> legacyCodes) {
    this.legacyCodes = legacyCodes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OptionalVatConfiguration that = (OptionalVatConfiguration) o;
    return Objects.equals(source, that.source) &&
      Objects.equals(legacyCodes, that.legacyCodes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(source, legacyCodes);
  }

  @Override
  public String toString() {
    return "OptionalVatConfiguration{" +
      "source='" + source + '\'' +
      ", legacyCodes=" + legacyCodes +
      '}';
  }
}
