package it.fai.ms.consumi.service.impl;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.fattura.FooterContractSection;
import it.fai.ms.common.jms.fattura.FooterDeviceSection;
import it.fai.ms.common.jms.fattura.FooterTotalContract;
import it.fai.ms.common.jms.fattura.FooterTotalProduct;
import it.fai.ms.common.jms.fattura.GenericoConsumo;
import it.fai.ms.common.jms.fattura.GenericoDTO;
import it.fai.ms.common.jms.fattura.GenericoWithDevice;
import it.fai.ms.common.jms.fattura.HeaderContractSection;
import it.fai.ms.common.jms.fattura.HeaderDeviceSection;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.device.TipoSupporto;
import it.fai.ms.consumi.repository.device.TipoSupportoRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.parametri_stanziamenti.model.StanziamentiDetailsTypeEntity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Repository;
import it.fai.ms.consumi.service.ViewAllegatiGenericoService;
import it.fai.ms.consumi.util.AnomalyException;

@Service
@Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
public class ViewAllegatiGenericoServiceImpl implements ViewAllegatiGenericoService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final static String MINUS_SIGN  = "-";
  private final static double ZERO_DOUBLE = 0d;

  private final StanziamentiParamsRepository           stanziamentiParamsRepository;
  private final ViewAllegatoGenericoSezione2Repository sezione2Repo;
  private final PetrolPumpService                      petrolPumpService;

  private final TipoSupportoRepository tipoSupportoRepo;

  public ViewAllegatiGenericoServiceImpl(final ViewAllegatoGenericoSezione2Repository sezione2Repo,
                                         final StanziamentiParamsRepository stanziamentiParamsRepository,
                                         final PetrolPumpService petrolPumpService, final TipoSupportoRepository tipoSupportoRepo) {
    this.sezione2Repo = sezione2Repo;
    this.stanziamentiParamsRepository = stanziamentiParamsRepository;
    this.petrolPumpService = petrolPumpService;
    this.tipoSupportoRepo = tipoSupportoRepo;
  }

  @Override
  public List<ViewAllegatoGenericoSezione2Entity> getDettagliStanziamento(List<UUID> dettagliStanziamentoId) {
    if (log.isDebugEnabled())
      log.debug("Search dettagli stanziamento to {}", dettagliStanziamentoId);
    List<ViewAllegatoGenericoSezione2Entity> dataAllegatiPedaggio = sezione2Repo.findByDettaglioStanziamentoId(dettagliStanziamentoId);
    if (log.isDebugEnabled())
      log.debug("Found {} elements", dataAllegatiPedaggio != null ? dataAllegatiPedaggio.size() : 0);
    return dataAllegatiPedaggio;
  }

  public List<GenericoDTO> createBodyMessage(List<ViewAllegatoGenericoSezione2Entity> dataAllegati,
                                             final RequestsCreationAttachments requestCreationAttachments,
                                             final TemplateAllegati template) {
    log.info("Start generation DTO to allegati by Dettagli stanziamento Generici...");
    Long tStart = System.currentTimeMillis();

    final String nomeIntestazioneTessere = requestCreationAttachments.getNomeIntestazioneTessere();
    final String nomeClienteTessere = StringUtils.isNotBlank(nomeIntestazioneTessere) ? nomeIntestazioneTessere : "";
    final String tipoServizio = requestCreationAttachments.getTipoServizio();
    final String raggruppamentoArticolo = requestCreationAttachments.getCodiceRaggruppamentoArticolo();

    Map<String, List<ViewAllegatoGenericoSezione2Entity>> mapContrattoAllegati = new LinkedHashMap<>();
    mapContrattoAllegati = dataAllegati.stream()
                                       .collect(Collectors.groupingBy(ViewAllegatoGenericoSezione2Entity::getCodiceContratto,
                                                                      LinkedHashMap::new, Collectors.toList()));

    List<GenericoDTO> genericiDTO = new ArrayList<>();
    mapContrattoAllegati.entrySet()
                        .forEach(e -> {
                          long tContract = System.currentTimeMillis();
                          String codiceContratto = e.getKey();
                          log.debug("Codice Contratto to manage: {}", codiceContratto);
                          List<ViewAllegatoGenericoSezione2Entity> viewAllegati = e.getValue();
                          if (viewAllegati == null || viewAllegati.isEmpty()) {
                            String messageException = String.format("Not found dettagli stanziamento generici for contratto: %s",
                                                                    codiceContratto);
                            throw new RuntimeException(messageException);
                          }

                          GenericoDTO genericoDto = new GenericoDTO(false).tipoServizio(tipoServizio);
                          HeaderContractSection headerContract = new HeaderContractSection().codiceContrattoCliente(codiceContratto)
                                                                                            .nomeSuTesseraCliente(nomeClienteTessere);
                          genericoDto.setHeaderContractSection(headerContract);
                          manageDataByTemplate(viewAllegati, template, genericoDto);

                          String nazioneFatturazione = findNazioneFatturazione(raggruppamentoArticolo);
                          genericoDto.setNazioneFatturazione(nazioneFatturazione);

                          if (log.isDebugEnabled())
                            log.debug("GenericoDTO: {}", genericoDto);
                          genericiDTO.add(genericoDto);

                          if (log.isDebugEnabled())
                            log.debug("Finished manage contract {} in {} ms", codiceContratto, System.currentTimeMillis() - tContract);
                        });
    log.info("Created data DTO to Generico in {} ms", System.currentTimeMillis() - tStart);
    return genericiDTO;

  }

  private void manageDataByTemplate(List<ViewAllegatoGenericoSezione2Entity> viewAllegati, TemplateAllegati template,
                                    GenericoDTO genericoDto) {
    long tManageData = System.currentTimeMillis();
    switch (template) {
    case CON_DISPOSITIVO:
      genericoDto.setWithDevice(true);
      manageDataWithDevice(viewAllegati, genericoDto);
      break;

    case NO_DISPOSITIVO:
      manageDataWithNoDevice(viewAllegati, genericoDto);
      break;
    default:
      String messageError = String.format("This template allegati [%s] is not implemented", template);
      log.error(messageError);
      throw new RuntimeException(messageError);
    }

    generateAndSetFooter(genericoDto);
    if (log.isDebugEnabled())
      log.debug("Manage data in {} ms", System.currentTimeMillis() - tManageData);
  }

  private void manageDataWithDevice(List<ViewAllegatoGenericoSezione2Entity> viewAllegati, GenericoDTO genericoDto) {
    long tm = System.currentTimeMillis();
    Map<HeaderDeviceSection, List<GenericoConsumo>> map = viewAllegati.stream()
                                                                      .collect(groupingBy(v -> createHeaderDevice(v),
                                                                                          Collectors.mapping(v -> mapToGenerico(v),
                                                                                                             toList())));

    generateAndSetGenericoWithDevice(map, genericoDto);
    if (log.isDebugEnabled())
      log.debug("Finished managed WITH DEVICE in {} ms", System.currentTimeMillis() - tm);
  }

  private HeaderDeviceSection createHeaderDevice(ViewAllegatoGenericoSezione2Entity v) {
    HeaderDeviceSection headerDevice = new HeaderDeviceSection();
    String panNumber = v.getPanNumber();
    headerDevice.tipoDispositivo(v.getTipoDispositivo())
                .panNumber((panNumber != null) ? panNumber : "")
                .serialNumber(v.getSerialNumber())
                .nazioneTarga(v.getNazioneVeicolo())
                .targa(v.getTargaVeicolo())
                .classeEuro(v.getClassificazioneEuro());
    if (log.isDebugEnabled())
      log.debug("Header Device: {}", headerDevice);
    return headerDevice;
  }

  private void generateAndSetGenericoWithDevice(Map<HeaderDeviceSection, List<GenericoConsumo>> mapWithDevice, GenericoDTO genericoDto) {
    long tgenerate = System.currentTimeMillis();
    final List<GenericoWithDevice> bodyContractSection = getBodyContractSection(genericoDto);

    List<HeaderDeviceSection> keyList = mapWithDevice.keySet()
                                                     .stream()
                                                     .sorted(Comparator.comparing(HeaderDeviceSection::getTipoDispositivo)
                                                                       .thenComparing(HeaderDeviceSection::getPanNumber)
                                                                       .thenComparing(HeaderDeviceSection::getSerialNumber))
                                                     .collect(toList());
    keyList.forEach(k -> {
      GenericoWithDevice genericoDevice = new GenericoWithDevice();
      genericoDevice.setHeaderDeviceSection(k);
      List<GenericoConsumo> consumiOrderedBySerialNumber = mapWithDevice.get(k)
                                                                        .stream()
                                                                        .sorted((c1, c2) -> c1.getSerialNumber()
                                                                                              .compareTo(c2.getSerialNumber()))
                                                                        .collect(toList());
      genericoDevice.setConsumoDevice(mapWithDevice.get(k));
      genericoDevice.setFooterDeviceSection(mapToFooterDevice(consumiOrderedBySerialNumber));
      bodyContractSection.add(genericoDevice);
    });

    genericoDto.setBodyContractSection(bodyContractSection);
    if (log.isDebugEnabled())
      log.debug("Finished generate and set generico in {} ms", System.currentTimeMillis() - tgenerate);
  }

  private void manageDataWithNoDevice(List<ViewAllegatoGenericoSezione2Entity> viewAllegati, GenericoDTO genericoDto) {
    long tm = System.currentTimeMillis();
    HeaderContractSection headerContract = genericoDto.getHeaderContractSection();
    LinkedHashMap<HeaderContractSection, List<GenericoConsumo>> map = new LinkedHashMap<>();
    viewAllegati.forEach(v -> {
      List<GenericoConsumo> genericoNoDevice = new ArrayList<>();
      if (map.containsKey(headerContract)) {
        genericoNoDevice = map.get(headerContract);
      }
      genericoNoDevice.add(mapToGenerico(v));
      map.put(headerContract, genericoNoDevice);
    });

    generateAndSetBodyNoDevice(map, genericoDto);
    if (log.isDebugEnabled())
      log.debug("Finished manage data NO DEVICE in {} ms", System.currentTimeMillis() - tm);
  }

  private void generateAndSetBodyNoDevice(LinkedHashMap<HeaderContractSection, List<GenericoConsumo>> map, GenericoDTO genericoDto) {
    long tgenerate = System.currentTimeMillis();
    final List<GenericoWithDevice> bodyContractSection = getBodyContractSection(genericoDto);
    List<HeaderContractSection> keyList = map.keySet()
                                             .stream()
                                             .sorted(Comparator.comparing(HeaderContractSection::getCodiceContrattoCliente)
                                                               .thenComparing(HeaderContractSection::getNomeSuTesseraCliente))
                                             .collect(toList());
    keyList.forEach(k -> {
      GenericoWithDevice genericoDevice = new GenericoWithDevice();
      List<GenericoConsumo> consumiOrderedByDescrProdotto = map.get(k)
                                                               .stream()
                                                               .sorted((c1, c2) -> c1.getDescrizioneProdotto()
                                                                                     .compareTo(c2.getDescrizioneProdotto()))
                                                               .collect(toList());
      genericoDevice.setConsumoDevice(consumiOrderedByDescrProdotto);
      bodyContractSection.add(genericoDevice);
    });
    genericoDto.setBodyContractSection(bodyContractSection);
    if (log.isDebugEnabled())
      log.debug("Finished generate and set generico in {} ms", System.currentTimeMillis() - tgenerate);
  }

  // Il body non deve avere la targa e la nazione del veicolo
  private GenericoConsumo mapToGenerico(ViewAllegatoGenericoSezione2Entity v) {
    long tMap = System.currentTimeMillis();
    GenericoConsumo generico = new GenericoConsumo();
    final String panNumberFromDettaglio = v.getPanNumber();
    String panNumber = (StringUtils.isNotBlank(panNumberFromDettaglio)) ? panNumberFromDettaglio : "";
    generico.serialNumber(v.getSerialNumber())
            .descrizioneAggiuntiva(v.getDescrizioneAggiuntiva())
            .dataInizio(v.getDataInizio())
            .dataFine(v.getDataFine())
            .quantita(v.getQuantita())
            .prezzoUnitario(v.getImporto())
            .imponibile(v.getImponibile())
            .valuta(v.getValuta())
            .cambio(v.getCambio())
            .percIva(v.getPercIva())
            .panNumber(panNumber)
            .causale(v.getCodiceProdotto())
            .numeroAutorizzazione(v.getDocumentoDaFornitore())
            .tipoConsumo(v.getTipoConsumo());
            

    setPrezzoAndImponibile(v, generico);
    setProdottoInformation(v, generico);
    setCodiceDescrizioneCittaPuntoErogazione(v.getPuntoErogazione(), generico);
    setTipoSupportoFix(v, generico);

    generico.setCentroDiCosto(null);

    if (log.isDebugEnabled())
      log.debug("Finished map in {} ms", System.currentTimeMillis() - tMap);
    return generico;
  }

  private void setPrezzoAndImponibile(ViewAllegatoGenericoSezione2Entity v, GenericoConsumo generico) {
    String signTransaction = v.getSignTransaction();
    Double importo = v.getImporto();
    generico.setPrezzoUnitario(calculateValueWithSign(importo, signTransaction));
    Double imponibile = v.getImponibile();
    generico.setImponibile(calculateValueWithSign(imponibile, signTransaction));
  }

  private Double calculateValueWithSign(Double value, String signTransaction) {
    Double newValue = value;
    if(value < 0) {
      log.info("Value is already with sign... {}", newValue);
      return newValue;
    }
    
    if (value != ZERO_DOUBLE && StringUtils.isNotBlank(signTransaction) && MINUS_SIGN.equals(signTransaction.trim())) {
      newValue = Double.parseDouble(signTransaction.trim() + value);
      log.debug("New value with sign: {}", value);
    }
    return newValue;
  }

  private void generateAndSetFooter(GenericoDTO genericoDto) {
    long tFooter = System.currentTimeMillis();
    List<GenericoWithDevice> bodyContractSection = genericoDto.getBodyContractSection();
    long tFooterProduct = System.currentTimeMillis();
    Map<String, List<GenericoConsumo>> mapProductSorted = bodyContractSection.stream()
                                                                             .map(s -> s.getConsumoDevice())
                                                                             .flatMap(d -> d.stream())
                                                                             .collect(groupingBy(GenericoConsumo::getGroupByProdottoUnitaDiMisura,
                                                                                                 toList()));
    List<FooterTotalProduct> footerTotalsProduct = new ArrayList<>();
    generateFooterProduct(mapProductSorted, footerTotalsProduct);

    FooterContractSection footerContract = new FooterContractSection();

    List<FooterTotalProduct> listFooterOrdered = footerTotalsProduct.stream()
                                                                    .sorted(Comparator.comparing(FooterTotalProduct::getCodiceProdotto))
                                                                    .sorted(Comparator.comparing(FooterTotalProduct::getUnitaDiMisura))
                                                                    .collect(toList());
    footerContract.setFooterTotalProduct(listFooterOrdered);
    log.info("Finished footer Total Product in {} ms", System.currentTimeMillis() - tFooterProduct);

    List<FooterTotalContract> footerTotalsContract = new ArrayList<>();

    long tFooterContract = System.currentTimeMillis();
    Map<String, List<FooterTotalProduct>> mapUnitaDiMisuraConsumi = listFooterOrdered.stream()
                                                                                     .collect(groupingBy(FooterTotalProduct::getUnitaDiMisura,
                                                                                                         toList()));
    generateFooterContract(mapUnitaDiMisuraConsumi, footerTotalsContract);
    footerContract.setFooterTotalContract(footerTotalsContract.stream()
                                                              .sorted(Comparator.comparing(FooterTotalContract::getUnitaDiMisura))
                                                              .collect(toList()));
    if (log.isDebugEnabled())
      log.debug("Finished footer Total Contract in {} ms", System.currentTimeMillis() - tFooterContract);

    genericoDto.setFooterContractSection(footerContract);
    if (log.isDebugEnabled())
      log.debug("Finished generate Footer in {} ms", System.currentTimeMillis() - tFooter);
  }

  private void generateFooterContract(Map<String, List<FooterTotalProduct>> mapUnitaDiMisuraConsumi,
                                      List<FooterTotalContract> footerTotalsContract) {
    long tFooterContract = System.currentTimeMillis();
    mapUnitaDiMisuraConsumi.entrySet()
                           .forEach(u -> {
                             String unitaDiMisura = u.getKey();
                             List<FooterTotalProduct> consumi = u.getValue();

                             FooterTotalContract footerTotalContract = new FooterTotalContract();
                             footerTotalContract.setUnitaDiMisura(unitaDiMisura);

                             consumi.forEach(c -> {
//                               Double imponibile = (c.getQuantita() > 0) ? c.getQuantita() * c.getPrezzoUnitarioNoIva() : 0d;
                               footerTotalContract.setQuantita(footerTotalContract.getQuantita() + c.getQuantita());
                               footerTotalContract.setImponibile(footerTotalContract.getImponibile() + c.getImponibile());
                             });
                             footerTotalsContract.add(footerTotalContract);
                           });
    if (log.isDebugEnabled())
      log.debug("Finished generate FooterContract in {} ms", System.currentTimeMillis() - tFooterContract);
  }

  private void generateFooterProduct(Map<String, List<GenericoConsumo>> mapProductSorted, List<FooterTotalProduct> footerTotalsProduct) {
    long tFooterProduct = System.currentTimeMillis();
    mapProductSorted.entrySet()
                    .forEach(p -> {
                      String codiceProdottoUnitaDiMisura = p.getKey();
                      List<GenericoConsumo> consumi = p.getValue();
                      if (consumi == null || consumi.isEmpty()) {
                        String messageError = String.format("Not found consumi by prodotto-unita di misura: %s",
                                                            codiceProdottoUnitaDiMisura);
                        log.error(messageError);
                        throw new RuntimeException(messageError);
                      }

                      GenericoConsumo genericoConsumo = consumi.get(0);
                      String productCode = genericoConsumo.getCodiceProdotto();
                      String descrizioneProdotto = genericoConsumo.getDescrizioneProdotto();
                      String unitaDiMisura = genericoConsumo.getUnitaDiMisura();

                      FooterTotalProduct footerProduct = new FooterTotalProduct();
                      footerProduct.setCodiceProdotto(productCode);
                      footerProduct.setDescrizioneProdotto(descrizioneProdotto);
                      footerProduct.setUnitaDiMisura(unitaDiMisura);

                      consumi.forEach(c -> {
                        footerProduct.setQuantita(footerProduct.getQuantita() + c.getQuantita());
                        //la somma dei prezzi la utilizziamo per calcolare la media dopo
                        footerProduct.setPrezzoUnitarioNoIva(footerProduct.getPrezzoUnitarioNoIva() + c.getPrezzoUnitario());
                        footerProduct.setImponibile(footerProduct.getImponibile() + c.getImponibile());
                      });

                      //calcoliamo la media dei pezzi
                      if (footerProduct.getPrezzoUnitarioNoIva() != null && footerProduct.getQuantita() != null && !Double.valueOf("0").equals(footerProduct.getQuantita())) {
                        footerProduct.setPrezzoUnitarioNoIva(footerProduct.getPrezzoUnitarioNoIva() / footerProduct.getQuantita());
                      } else {
                        footerProduct.setPrezzoUnitarioNoIva(null);
                      }
                      footerTotalsProduct.add(footerProduct);
                    });
    if (log.isDebugEnabled())
      log.debug("Finished generate FooterProduct in {} ms", System.currentTimeMillis() - tFooterProduct);
  }

  private List<GenericoWithDevice> getBodyContractSection(GenericoDTO genericoDto) {
    List<GenericoWithDevice> bodyContractSection = new ArrayList<>();
    if (genericoDto.getBodyContractSection() != null && !genericoDto.getBodyContractSection()
                                                                    .isEmpty()) {
      log.debug("Prepopulate bodyContractSection...");
      bodyContractSection.addAll(genericoDto.getBodyContractSection());
    }
    return bodyContractSection;
  }

  private FooterDeviceSection mapToFooterDevice(List<GenericoConsumo> consumi) {
    FooterDeviceSection footer = new FooterDeviceSection();
    consumi.forEach(c -> {
      String panNumber = c.getPanNumber();
      footer.setPanNumber(panNumber);
      footer.setUnitaDiMisura(c.getUnitaDiMisura());

      Double imponibileFooter = footer.getImponibile();
      Double imponibile = imponibileFooter != null ? imponibileFooter : 0d;
      footer.setImponibile(imponibile + c.getImponibile());

      Double quantitaFooter = footer.getQuantita();
      Double quantita = quantitaFooter != null ? quantitaFooter : 0d;
      footer.setQuantita(quantita + c.getQuantita());
    });
    return footer;
  }

  private void setCodiceDescrizioneCittaPuntoErogazione(String puntoErogazione, GenericoConsumo generico) {
    PointDTO point = null;
    try {
      point = findPoint(puntoErogazione);
    } catch (Throwable e) {
      String messageError = String.format("Not found point: %s", puntoErogazione);
      log.error(messageError);
      throw new AnomalyException(messageError, e);
    }
    if (log.isDebugEnabled())
      log.debug("Found point: {}", point);
    generico.setCodicePuntoErogazione(puntoErogazione);
    if (point != null) {
      String description = point.getName() != null ? point.getName() : "";
      generico.setDescrizionePuntoErogazione(description);
      String city = point != null ? point.getCity() : "";
      generico.setCittaPuntoErogazione(city);
    } else {
      log.error("Not found point on micrservice petrolpumo by punto erogazione: {}", puntoErogazione);
      throw new IllegalArgumentException("Not found point on petrolpump by PuntoErogazione: " + puntoErogazione);
    }
  }

  private void setTipoSupportoFix(ViewAllegatoGenericoSezione2Entity v, GenericoConsumo generico) {
    if (StringUtils.isNotBlank(v.getTipoDispositivo())) {
      generico.setTipoSupportoFix(getTipoSupportoByNomeTipoDispositivo(v.getTipoDispositivo()));
    } else {
      log.warn("Not found TipoDispositivo on consumo generico, so not set TipoSupportoFix...");
    }
  }

  private String getTipoSupportoByNomeTipoDispositivo(String tipoDispositivo) {
    String tipoSupporto = "";
    log.debug("Get TipoSupporto by TipoDispositivo: {}", tipoDispositivo);
    if (StringUtils.isNotBlank(tipoDispositivo)) {
      TipoSupporto deviceType = tipoSupportoRepo.findByTipoDispositivo(tipoDispositivo);
      if (deviceType != null) {
        tipoSupporto = deviceType.getTipoSupportoFix();
      }
    }
    log.debug("Found tipoSupporto: {}", tipoSupporto);
    return tipoSupporto;
  }

  private PointDTO findPoint(String puntoErogazione) {
    PointDTO point = petrolPumpService.getPoint(puntoErogazione);
    return point;
  }

  private void setProdottoInformation(ViewAllegatoGenericoSezione2Entity v, GenericoConsumo generico) {
    String prodotto = v.getCodiceProdotto();
    String codiceFornitore = v.getCodiceFornitoreNav();
    String recordCode = v.getRecordCode();
    StanziamentiDetailsTypeEntity detailType = StanziamentiDetailsTypeEntity.GENERICO;
    Optional<StanziamentiParams> optParamStanziamenti = findProductByParamStanziamenti(prodotto, codiceFornitore, recordCode, detailType);
    if (optParamStanziamenti.isPresent()) {
      StanziamentiParams stanziamentiParams = optParamStanziamenti.get();
      Article article = stanziamentiParams.getArticle();
      if (log.isDebugEnabled())
        log.debug("Article param stanziamenti found: {}", article);

      generico.setCodiceProdotto(article.getCode());
      generico.setDescrizioneProdotto(article.getDescription());
      String measurementUnit = article.getMeasurementUnit();
      generico.setUnitaDiMisura(StringUtils.isNotBlank(measurementUnit) ? measurementUnit : "");
    } else {
      StringBuilder sb = new StringBuilder("Not found param stanziamenti by:\n");
      sb.append("TransactionDetailCode: ")
        .append(prodotto)
        .append("\n");
      sb.append("Codice Fornitore: ")
        .append(codiceFornitore)
        .append("\n");
      sb.append("Record Code: ")
        .append(recordCode)
        .append("\n");
      sb.append("Tipo Dettaglio: ")
        .append(detailType)
        .append("\n");
      log.error(sb.toString());
      throw new RuntimeException(sb.toString());
    }
  }

  private String findNazioneFatturazione(String raggruppamentoArticolo) {
    String country = "";

    Optional<StanziamentiParams> paramStanziamentiOpt = stanziamentiParamsRepository.findFirstByCodiceRaggruppamentoArticolo(raggruppamentoArticolo);
    if (paramStanziamentiOpt.isPresent()) {
      StanziamentiParams stanziamentiParams = paramStanziamentiOpt.get();
      country = stanziamentiParams.getArticle()
                                  .getCountry();
      log.debug("Found nazione Fatturazione: {}", country);
    } else {
      log.warn("Not found param stanziamenti by RaggruppamentoArticolo: {}", raggruppamentoArticolo);
    }

    return country;
  }

  private Optional<StanziamentiParams> findProductByParamStanziamenti(String transactionDetail, String codiceFornitore, String recordCode,
                                                                      StanziamentiDetailsTypeEntity detailType) {
    long tParamStanziamenti = System.currentTimeMillis();
    Optional<StanziamentiParams> optParamStanziamenti = stanziamentiParamsRepository.findByTransactionDetailAndCodiceFornitoreAndRecordCodeAndTipoDettaglio(transactionDetail,
                                                                                                                                                            codiceFornitore,
                                                                                                                                                            recordCode,
                                                                                                                                                            detailType);
    if (log.isDebugEnabled())
      log.debug("Finished search param stanziamenti in {} ms", System.currentTimeMillis() - tParamStanziamenti);
    return optParamStanziamenti;
  }

}
