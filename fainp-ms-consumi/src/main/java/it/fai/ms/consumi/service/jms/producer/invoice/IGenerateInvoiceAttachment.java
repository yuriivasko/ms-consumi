package it.fai.ms.consumi.service.jms.producer.invoice;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.common.jms.fattura.AbstractAllegatoFatturaMessage;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;

public interface IGenerateInvoiceAttachment {

  final Logger log = LoggerFactory.getLogger(IGenerateInvoiceAttachment.class);

  final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

  default Object generateDtoToAttachmentInvoice(DettaglioStanziamentoType tipoDettaglio,
                                                RequestsCreationAttachments requestCreationAttachments,
                                                RaggruppamentoArticoliDTO raggruppamentoArticoliDTO, TemplateAllegati template) {
    return null;
  }

  default DettaglioStanziamentoType getDettaglioStanziamentoType() {
    return null;
  }

  default String getNomeDocumento(final RequestsCreationAttachments requestCreationAttachments,
                                  final RaggruppamentoArticoliDTO raggruppamentoArticoliDTO) {
    String numeroFattura = requestCreationAttachments.getCodiceFattura();
    String dataFatturaFormatted = formatter.format(LocalDateTime.ofInstant(requestCreationAttachments.getDataFattura(), ZoneOffset.UTC));
    String codiceRaggruppamentoArticoli = requestCreationAttachments.getCodiceRaggruppamentoArticolo();
    String nomeDocumento = String.join("-", numeroFattura, dataFatturaFormatted, codiceRaggruppamentoArticoli);
    return nomeDocumento;
  }

  default void printToJsonFormat(AbstractAllegatoFatturaMessage message) {
    if (log.isDebugEnabled()) {
      StringBuilder sb = new StringBuilder("Start Message generated:\n");
      try {
        sb.append(new ObjectMapper().writerWithDefaultPrettyPrinter()
                                    .writeValueAsString(message));
      } catch (Exception e) {
        sb.append(message);
      }
      sb.append("\nEnd message generated");
      log.debug(sb.toString());
    }
  }

}
