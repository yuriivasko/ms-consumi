package it.fai.ms.consumi.domain.consumi.generici.dartford;

import java.util.UUID;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElcitGlobalIdentifier;

public class DartfordGlobalIdentifier
    extends GlobalIdentifier {

  public DartfordGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.INTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    return new DartfordGlobalIdentifier(UUID.randomUUID()
                                         .toString());
  }

}
