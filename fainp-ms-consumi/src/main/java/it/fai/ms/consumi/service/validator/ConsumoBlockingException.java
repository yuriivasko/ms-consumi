package it.fai.ms.consumi.service.validator;

public class ConsumoBlockingException extends RuntimeException{
  private static final long serialVersionUID = -3297795513354081882L;

  public ConsumoBlockingException(String message) {
    super(message);
  }
}
