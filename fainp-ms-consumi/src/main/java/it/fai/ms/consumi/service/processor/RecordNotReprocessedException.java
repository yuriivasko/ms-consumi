package it.fai.ms.consumi.service.processor;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

public class RecordNotReprocessedException extends RuntimeException {

  public static final String RECORD_NOT_REPROCESSED = "Record [$message] cannot be reprocessed!";

  public RecordNotReprocessedException(CommonRecord record) {
    super(composeMessage(RECORD_NOT_REPROCESSED, record));
  }

  private static String composeMessage(String template, CommonRecord commonRecord) {
    return template.replace("$message", commonRecord.toNotificationMessage());
  }
}
