package it.fai.ms.consumi.service.jms.listener.canoni;

import java.io.Serializable;

import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoni;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoniDispositivi;

public class RichiestaCanoniClientiMessage implements Serializable{

  private static final long serialVersionUID = -7472293478142776330L;
  private RichiestaCanoni msg;
  private String codiceCliente;

  public RichiestaCanoniClientiMessage(RichiestaCanoni msg, String codiceCliente) {
    this.msg = msg;
    this.codiceCliente = codiceCliente;
  }

  public RichiestaCanoni getMsg() {
    return msg;
  }

  public void setMsg(RichiestaCanoniDispositivi msg) {
    this.msg = msg;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

}
