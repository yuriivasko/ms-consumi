package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.AsfinagRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.AsfinagRecordDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.TrackyCardPedaggiStandardRecordDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElceuRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class ElceuDescriptor implements RecordDescriptor<ElceuRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(ElceuDescriptor.class);

  public final static String HEADER_BEGIN      = "01";
  public final static String BEGIN_07          = "07";
  public final static String BEGIN_09          = "09";
  public final static String BEGIN_11          = "11";
  public final static String BEGIN_12          = "12";
  public final static String BEGIN_13          = "13";
  public final static String BEGIN_14          = "14";
  public final static String BEGIN_15          = "15";
  public final static String BEGIN_17          = "17";
  public final static String FOOTER_CODE_BEGIN = "90";

  // Header
  private final static FileDescriptor descriptor01 = new FileDescriptor();

  static {
    descriptor01.addItem("recordCode", 1, 2);
    descriptor01.addItem("partnerCountryCode", 3, 5);
    descriptor01.skipItem("transmitter", 6, 10);
    descriptor01.skipItem("transmitterCountryCode", 11, 13);
    descriptor01.skipItem("receiver", 14, 18);
    descriptor01.addItem("creationDateInFile", 19, 26);
    descriptor01.addItem("creationTimeInFile", 27, 32);
    descriptor01.skipItem("fileNumber", 33, 36);
    descriptor01.skipItem("dataType", 37, 46);
    descriptor01.skipItem("fileReleaseNumber", 47, 52);
  }

  // TIS-PL Details
  static final FileDescriptor descriptor07 = new FileDescriptor();

  static {
    descriptor07.addItem("recordCode", 1, 2);
    descriptor07.addItem("partnerCountryCode", 3, 5);
    descriptor07.addItem("partnerNumber", 6, 10);
    descriptor07.addItem("documentNumber", 11, 29); // v2
    descriptor07.addItem("documentId", 30, 44); // v2
    descriptor07.addItem("invoiceType", 45, 45); // v2
    descriptor07.addItem("invoiceId", 46, 54); // v2
    descriptor07.addItem("tollType", 55, 55); // v2
    descriptor07.addItem("tspRespCountryCode", 56, 58);
    descriptor07.addItem("tspRespNumber", 59, 63);
    descriptor07.addItem("tspExitCountryCode", 64, 66);
    descriptor07.addItem("tspExitNumber", 67, 71);
    descriptor07.addItem("transactionId", 72, 100);
    descriptor07.addItem("splitNumber", 101, 101);
    descriptor07.addItem("compensationNumber", 102, 102);
    descriptor07.addItem("signOfTransaction", 103, 103);
    descriptor07.addItem("telepassPanNumber", 104, 122);
    descriptor07.addItem("discountSchemeNumber", 123, 134);
    descriptor07.addItem("entryTimestamp", 135, 148);
    descriptor07.addItem("entryGateCode", 149, 157);
    descriptor07.addItem("entryAcquisitionType", 158, 159);
    descriptor07.addItem("exitTimestamp", 160, 173);
    descriptor07.addItem("exitGateCode", 174, 182);
    descriptor07.addItem("exitAcquisitionType", 183, 184);
    descriptor07.addItem("pricingClass", 185, 186);
    descriptor07.addItem("km", 187, 191);
    descriptor07.addItem("priceType", 192, 196);
    descriptor07.addItem("priceCode", 197, 201);
    descriptor07.addItem("currencyCode", 202, 204);
    descriptor07.addItem("amountWithoutVATOfBase", 205, 215);
    descriptor07.addItem("amountWithoutVATExcludedForDiscount", 216, 226); // v2
    descriptor07.addItem("VATRate", 227, 230);
    descriptor07.addItem("amountIncludingVAT", 231, 241);
    descriptor07.addItem("observationCode", 242, 243); // v2
    descriptor07.addItem("externalOBUIdentifier", 244, 263);
    descriptor07.addItem("typeOfRecordingPoint", 264, 265);
    descriptor07.addItem("notConfirmedTransaction", 266, 266); // v2
    descriptor07.addItem("transactionDetail", 267, 268);
  }

  // Via-T Detail
  // Via-T PT
  // Tunnel Belgium
  static final FileDescriptor descriptor09_11_12 = new FileDescriptor();

  static {
    descriptor09_11_12.addItem("recordCode", 1, 2);
    descriptor09_11_12.addItem("partnerCountryCode", 3, 5);
    descriptor09_11_12.addItem("partnerNumber", 6, 10);
    descriptor09_11_12.addItem("documentNumber", 11, 29); // v2
    descriptor09_11_12.addItem("documentId", 30, 44); // v2
    descriptor09_11_12.addItem("invoiceType", 45, 45); // v2
    descriptor09_11_12.addItem("invoiceId", 46, 54); // v2
    descriptor09_11_12.addItem("tollType", 55, 55); // v2
    descriptor09_11_12.addItem("tspRespCountryCode", 56, 58);
    descriptor09_11_12.addItem("tspRespNumber", 59, 63);
    descriptor09_11_12.addItem("tspExitCountryCode", 64, 66);
    descriptor09_11_12.addItem("tspExitNumber", 67, 71);
    descriptor09_11_12.addItem("transactionId", 72, 100);
    descriptor09_11_12.addItem("signOfTransaction", 101, 101);
    descriptor09_11_12.addItem("telepassPanNumber", 102, 120);
    descriptor09_11_12.addItem("entryTimestamp", 121, 134);
    descriptor09_11_12.addItem("entryGateCode", 135, 143);
    descriptor09_11_12.addItem("exitTimestamp", 144, 157);
    descriptor09_11_12.addItem("exitGateCode", 158, 166);
    descriptor09_11_12.addItem("km", 167, 171);
    descriptor09_11_12.addItem("currencyCode", 172, 174);
    descriptor09_11_12.addItem("amountWithoutVATOfBase", 175, 185);
    descriptor09_11_12.addItem("amountWithoutVATExcludedForDiscount", 186, 196); // v2
    descriptor09_11_12.addItem("VATRate", 197, 200);
    descriptor09_11_12.addItem("amountIncludingVAT", 201, 211);
    descriptor09_11_12.addItem("observationCode", 212, 213); // v2
    descriptor09_11_12.addItem("externalOBUIdentifier", 214, 233);
    descriptor09_11_12.addItem("transactionDetail", 234, 235);
  }

  // Austria
  static final FileDescriptor descriptor13 = new FileDescriptor();

  static {
    descriptor13.addItem("recordCode", 1, 2);
    descriptor13.addItem("partnerCountryCode", 3, 5);
    descriptor13.addItem("partnerNumber", 6, 10);
    descriptor13.addItem("documentNumber", 11, 29); // v2
    descriptor13.addItem("documentId", 30, 44); // v2
    descriptor13.addItem("invoiceType", 45, 45); // v2
    descriptor13.addItem("invoiceId", 46, 54); // v2
    descriptor13.addItem("tollType", 55, 55); // v2
    descriptor13.addItem("tollCharger", 56, 63); // v2
    descriptor13.addItem("tollGate", 64, 67); // v2
    descriptor13.addItem("laneId", 68, 71); // v2
    descriptor13.addItem("networkCode", 72, 72); // v2
    descriptor13.addItem("pan", 73, 85); // v2
    descriptor13.addItem("exitTransitDateTime", 86, 99); // v2
    // descriptor13.addItem("blank", 100, 100); //v2
    descriptor13.addItem("transitType", 101, 102); // v2
    descriptor13.addItem("sign", 103, 103); // v2
    descriptor13.addItem("additionalInfo", 104, 119); // v2
    descriptor13.addItem("signOfTransaction", 120, 120);
    descriptor13.addItem("telepassPanNumber", 121, 139);
    descriptor13.addItem("entryTimestamp", 140, 153);
    descriptor13.addItem("entryGateCode", 154, 162);
    descriptor13.addItem("exitTimestamp", 163, 176);
    descriptor13.addItem("exitGateCode", 177, 185);
    descriptor13.addItem("externalCostAir", 186, 196); // v2
    descriptor13.addItem("externalCostNoise", 197, 207); // v2
    descriptor13.addItem("km", 208, 212);
    descriptor13.addItem("currencyCode", 213, 215);
    descriptor13.addItem("amountWithoutVATOfBase", 216, 226);
    descriptor13.addItem("amountWithoutVATExcludedForDiscount", 227, 237); // v2
    descriptor13.addItem("VATRate", 238, 241);
    descriptor13.addItem("amountIncludingVAT", 242, 252);
    descriptor13.addItem("observationCode", 253, 254); // v2
    descriptor13.addItem("externalOBUIdentifier", 255, 274);
    descriptor13.addItem("transactionAggregationNumber", 275, 290); // v2
    descriptor13.addItem("transactionDetail", 291, 292);
  }

  // Poland
  static final FileDescriptor descriptor14 = new FileDescriptor();

  static {
    descriptor14.addItem("recordCode", 1, 2);
    descriptor14.addItem("partnerCountryCode", 3, 5);
    descriptor14.addItem("partnerNumber", 6, 10);
    descriptor14.addItem("documentNumber", 11, 29); // v2
    descriptor14.addItem("documentId", 30, 44); // v2
    descriptor14.addItem("invoiceType", 45, 45); // v2
    descriptor14.addItem("invoiceId", 46, 54); // v2
    descriptor14.addItem("tollType", 55, 55); // v2
    descriptor14.addItem("tollCharger", 56, 63); // v2
    descriptor14.addItem("tollGate", 64, 67); // v2
    descriptor14.addItem("laneId", 68, 71); // v2
    descriptor14.addItem("networkCode", 72, 72); // v2
    descriptor14.addItem("pan", 73, 85); // v2
    descriptor14.addItem("exitTransitDateTime", 86, 99); // v2
    // descriptor14.addItem("blank", 100, 100); //v2
    descriptor14.addItem("transitType", 101, 102); // v2
    descriptor14.addItem("sign", 103, 103); // v2
    descriptor14.addItem("additionalInfo", 104, 119); // v2
    descriptor14.addItem("signOfTransaction", 120, 120);
    descriptor14.addItem("telepassPanNumber", 121, 139);
    descriptor14.addItem("entryTimestamp", 140, 153);
    descriptor14.addItem("entryGateCode", 154, 162);
    descriptor14.addItem("exitTimestamp", 163, 176);
    descriptor14.addItem("exitGateCode", 177, 185);
    descriptor14.addItem("km", 186, 190);
    descriptor14.addItem("currencyCode", 191, 193);
    descriptor14.addItem("amountWithoutVAT", 194, 204);
    descriptor14.addItem("VATRate", 205, 208);
    descriptor14.addItem("amountIncludingVAT", 209, 219);
    descriptor14.addItem("observationCode", 220, 221); // v2
    descriptor14.addItem("externalOBUIdentifier", 222, 241);
    descriptor14.addItem("transactionAggregationNumber", 242, 257); // v2
    descriptor14.addItem("transactionDetail", 258, 259);
  }

  // EETS Germany
  private static FileDescriptor descriptor15 = new FileDescriptor();

  static {
    descriptor15.addItem("recordCode", 1, 2);
    descriptor15.addItem("partnerCountryCode", 3, 5);
    descriptor15.addItem("partnerNumber", 6, 10);
    descriptor15.addItem("documentNumber", 11, 29);
    descriptor15.addItem("documentId", 30, 44);
    descriptor15.addItem("tripId", 45, 60);
    descriptor15.addItem("obu", 61, 80);
    descriptor15.addItem("entryTime", 81, 86);
    descriptor15.addItem("entryDate", 87, 94);
    descriptor15.addItem("entryGateCode", 95, 103);
    descriptor15.addItem("exitGateCode", 104, 112);
    descriptor15.addItem("signOfTransaction", 113, 113);
    descriptor15.addItem("telepassPanNumber", 114, 132);
    descriptor15.addItem("exitDate", 133, 140);
    descriptor15.addItem("exitTime", 141, 146);
    descriptor15.addItem("currencyCode", 147, 149);
    descriptor15.addItem("plateNumber", 150, 169);
    descriptor15.addItem("countryPlateNumber", 170, 172);
    descriptor15.addItem("vehicleClass", 173, 174);
    descriptor15.addItem("euroClass", 175, 176);
    descriptor15.addItem("totalAmount", 177, 187);
    descriptor15.addItem("claimId", 188, 203);
    descriptor15.addItem("crossingDate", 204, 211);
    descriptor15.addItem("crossingTime", 212, 217);
    descriptor15.addItem("sectionAmount", 218, 228);
    descriptor15.addItem("transactionDetail", 229, 230);
    descriptor15.addItem("sectionGateId", 231, 239);

    //TODO? invoice date: documento TP dice 240 - 247  - YYYYMMDD
    descriptor15.addItem("invoiceDate", 240, 247); // v2ì3
    //TODO? external cost: documento TP dice 248 - 258  - Mandatory only for Trip (9 INT 2 DEC)
    descriptor15.addItem("externalCost", 248, 258);
    /*
    TODO? trafficClassification: documento TP dice 259 - 273 -

    Mandatoryonly for section
    VVVVVOOZAGSSBBB iscomposed by:
    VVVVV= Tarifversion
    OO= Ortsklasse
    Z=Zeitklasse
    A=Achsanzahl
    G=Gewichtsklasse
    SS= EURO- Schastoffklasse
    BBb= Straßenbetreiber
    */
    descriptor15.addItem("trafficClassification", 262, 276);
    descriptor15.addItem("weightCategoryDescription", 277, 306);


  }

  // EETS Belgium
  static final FileDescriptor descriptor17 = new FileDescriptor();

  static {
    descriptor17.addItem("recordCode", 1, 2);
    descriptor17.addItem("partnerCountryCode", 3, 5);
    descriptor17.addItem("partnerNumber", 6, 10);
    descriptor17.addItem("documentNumber", 11, 29); // v2
    descriptor17.addItem("documentId", 30, 44); // v2
    descriptor17.addItem("invoiceType", 45, 45); // v2
    descriptor17.addItem("obu", 46, 65); // v2
    descriptor17.addItem("region", 66, 71); // v2
    descriptor17.addItem("roadType", 72, 75); // v2
    descriptor17.addItem("route", 76, 100); // v2
    descriptor17.addItem("entryTime", 101, 106); // v2
    descriptor17.addItem("entryDate", 107, 114);
    descriptor17.addItem("signOfTransaction", 115, 115);
    descriptor17.addItem("telepassPanNumber", 116, 134);
    descriptor17.addItem("exitDate", 135, 142); // v2
    descriptor17.addItem("exitTime", 143, 148); // v2
    descriptor17.addItem("km", 149, 158);
    descriptor17.addItem("currencyCode", 159, 161);
    descriptor17.addItem("plateNumber", 162, 181); // v2
    descriptor17.addItem("countryPlateNumber", 182, 184); // v2
    descriptor17.addItem("vehicleClass", 185, 186); // v2
    descriptor17.addItem("euroClass", 187, 188); // v2
    descriptor17.addItem("amountWithoutVAT", 189, 199);
    descriptor17.addItem("VATRate", 200, 203);
    descriptor17.addItem("amountIncludingVAT", 204, 214);
    descriptor17.addItem("transactionDetail", 215, 216);
  }

  // Trailer
  static final FileDescriptor descriptor90          = new FileDescriptor();
  static final String         numberOfDetailRecords = "numberOfDetailRecords";
  static {
    descriptor90.addItem("recordCode", 1, 2);
    descriptor90.addItem("transmitterCode", 3, 10);
    descriptor90.addItem("receiver", 11, 18);
    descriptor90.addItem("creationDateInFile", 19, 26);
    descriptor90.addItem("creationTimeInFile", 27, 32);
    descriptor90.addItem("fileNumber", 33, 36);
    descriptor90.addItem("dataType", 37, 46);
    descriptor90.addItem(numberOfDetailRecords, 47, 54);
  }

  /*
   * (non-Javadoc)
   * @see
   * it.fai.ms.consumi.scheduler.jobs.flussi.consumi.RecordDescriptor#decodeRecordCodeAndCallSetFromString(java.lang.
   * String, java.lang.String, it.fai.ms.consumi.repository.flussi.record.model.CommonRecord)
   */
  @Override
  public ElceuRecord decodeRecordCodeAndCallSetFromString(final String _data, final String _recordCode, String fileName, long rowNumber) {
    final ElceuRecord entity = new ElceuRecord(fileName,rowNumber);
    switch (_recordCode) {

    case HEADER_BEGIN:
      entity.setFromString(ElceuDescriptor.descriptor01.getItems(), _data);
      break;

    case BEGIN_07:
      entity.setFromString(ElceuDescriptor.descriptor07.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_FRANCIA);
      break;

    case BEGIN_09:
      entity.setFromString(ElceuDescriptor.descriptor09_11_12.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_SPAGNA);
      break;
    case BEGIN_11:
      entity.setFromString(ElceuDescriptor.descriptor09_11_12.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_PORTOGALLO);
      break;
    case BEGIN_12:
      entity.setFromString(ElceuDescriptor.descriptor09_11_12.getItems(), _data);
      entity.setService_type(TipoServizioEnum.TUNNEL_LIEFKENSHOEK);
      break;
    case BEGIN_13:
      entity.setFromString(ElceuDescriptor.descriptor13.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_AUSTRIA);
      break;

    case BEGIN_14:
      entity.setFromString(ElceuDescriptor.descriptor14.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_POLONIA);
      break;

    case BEGIN_15:
      entity.setFromString(ElceuDescriptor.descriptor15.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_GERMANIA);
      entity.setVATRate("0");
      entity.setAmountWithoutVAT(entity.getTotalAmount());
      break;

    case BEGIN_17:
      entity.setFromString(ElceuDescriptor.descriptor17.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_BELGIO);
      //FAINP-3034 - ELCEU-VACON SOLO Belgio, importi da parsare 6 cifre
      //imposto nuovamente i valori degli amount considerando
      String amountIncludingVatStr = entity.getAmountIncludingVAT();
      entity.setAmountIncludingVAT(amountIncludingVatStr,-6,5);
      String amountExcludedVatStr = entity.getAmountWithoutVAT();
      entity.setAmountWithoutVAT(amountExcludedVatStr,-6,5);

      break;

    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    Optional<StringItem> stringItem = ElceuDescriptor.descriptor90.getItem(ElceuDescriptor.numberOfDetailRecords);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in ElcitDescriptor!");
    }
    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return ElceuDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return ElceuDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 2;
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(ElceuDescriptor.class, ElceuRecord.class).checkConfiguration();
  }
}
