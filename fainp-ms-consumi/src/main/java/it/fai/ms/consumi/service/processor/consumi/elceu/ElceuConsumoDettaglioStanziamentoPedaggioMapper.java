package it.fai.ms.consumi.service.processor.consumi.elceu;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.repository.CaselliEntityRepository;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class ElceuConsumoDettaglioStanziamentoPedaggioMapper extends ConsumoDettaglioStanziamentoMapper {

  
  private CaselliEntityRepository caselliRepository = null;


  public ElceuConsumoDettaglioStanziamentoPedaggioMapper(CaselliEntityRepository caselliRepository) {
    this.caselliRepository = caselliRepository;
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamentoPedaggio dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof Elceu) {
      var elceuConsumo = (Elceu) _consumo;
      updateDescriptionTollPointGateByCaselli(elceuConsumo.getTollPointEntry(),caselliRepository);
      updateDescriptionTollPointGateByCaselli(elceuConsumo.getTollPointExit(),caselliRepository);
      dettaglioStanziamentoPedaggio = toDettaglioStanziamentoPedaggio(elceuConsumo, elceuConsumo.getGlobalIdentifier());
    }
    return dettaglioStanziamentoPedaggio;
  }

}
