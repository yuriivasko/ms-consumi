package it.fai.ms.consumi.repository.storico_dml.jms;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractMappableDmlListener;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.consumi.repository.storico_dml.StoricoAziendaRepository;
import it.fai.ms.consumi.repository.storico_dml.jms.mapper.StoricoAziendaDmlListenerMapper;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAzienda;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsDmlStoricoAziendaListener
  extends AbstractMappableDmlListener<AziendaDMLDTO, StoricoAzienda>   implements JmsTopicListener{

  public JmsDmlStoricoAziendaListener(StoricoAziendaRepository repository,
                                   StoricoAziendaDmlListenerMapper aziendaDmlListenerMapper, ApplicationEventPublisher publisher) {
    super(AziendaDMLDTO.class, repository, aziendaDmlListenerMapper,publisher,false);
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.DML_AZIENDE_SAVE;
  }

}
