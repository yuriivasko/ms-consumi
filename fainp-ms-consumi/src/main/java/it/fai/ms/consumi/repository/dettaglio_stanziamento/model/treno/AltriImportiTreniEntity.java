package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dettaglio_stanziamento_treni_altri_importi")
public class AltriImportiTreniEntity {

  @Id
  @Column(name = "codice_dettaglio_consumo")
  private String codiceDettaglioConsumo;

  @Column(name = "descrizione_altro_importo")
  private String descrizioneAltroImporto;

  @Column(name = "imponibile")
  private Double imponibile;

  public AltriImportiTreniEntity() {
  }

  public String getCodiceDettaglioConsumo() {
    return codiceDettaglioConsumo;
  }

  public void setCodiceDettaglioConsumo(String codiceDettaglioConsumo) {
    this.codiceDettaglioConsumo = codiceDettaglioConsumo;
  }

  public String getDescrizioneAltroImporto() {
    return descrizioneAltroImporto;
  }

  public void setDescrizioneAltroImporto(String descrizioneAltroImporto) {
    this.descrizioneAltroImporto = descrizioneAltroImporto;
  }

  public Double getImponibile() {
    return imponibile;
  }

  public void setImponibile(Double imponibile) {
    this.imponibile = imponibile;
  }

  @Override
  public String toString() {
    return "AltriImportiTreniEntity [codiceDettaglioConsumo=" + codiceDettaglioConsumo + ", descrizioneAltroImporto="
           + descrizioneAltroImporto + ", imponibile=" + imponibile + "]";
  }

}
