package it.fai.ms.consumi.service.record.dgdtoll;

import java.time.Instant;
import java.util.Optional;

import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdTollGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryVatRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class DgdTollRecordConsumoMapper
  extends AbstractRecordConsumoMapper
  implements RecordConsumoMapper<DgdTollRecord, DgdToll> {

  @Override
  public DgdToll mapRecordToConsumo(DgdTollRecord commonRecord) throws Exception {

    //yyyyMMdd_dateformat, HHmmss_timeformat
    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, "DGD");
    source.setFileName(commonRecord.getFileName());
    source.setRowNumber(commonRecord.getRowNumber());
    DgdToll toConsumo = new DgdToll(source, "DEFAULT",
                                    Optional.of(new DgdTollGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);
    Consumo toConsumoBase = toConsumo;
    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();
    transaction.setDetailCode(commonRecord.getTransactionDetail());
    toConsumoBase.setTransaction(transaction);

    // CONTRACT -> non necessario: recuperato tramite dispositivo
    toConsumoBase.setContract(Optional.empty());

    // CONSUMO.AMOUNT
    MonetaryVatRecord monetaryRecord = commonRecord;
    Amount amount = new Amount();
    if (monetaryRecord.getVatPercRate() != null) {
      amount.setVatRate(monetaryRecord.getVatPercRate());
    }
    amount.setAmountExcludedVat(monetaryRecord.getAmountExcludedVat());
    amount.setExchangeRate(null);
    toConsumoBase.setAmount(amount);

    ConsumoPedaggio toConsumoPedaggio = toConsumo;
    toConsumoPedaggio.setProcessed(commonRecord.isProcessed());

    // CONSUMOPEDAGGIO.DEVICE
    DgdTollRecord dgdTollRecord = commonRecord;
    final String pan = dgdTollRecord.getCardNumber();
    Device device = new Device(pan,TipoDispositivoEnum.TRACKYCARD);
    device.setServiceType(TipoServizioEnum.PEDAGGI_SVIZZERA);
    toConsumoPedaggio.setDevice(device);

    // CONSUMOPEDAGGIO.TOLLPOINTENTRY
    TollPoint tollpointEntry = new TollPoint();
    GlobalGate globalGateEntry = new GlobalGate(dgdTollRecord.getTollPointCodeEntry());
    globalGateEntry.setDescription(dgdTollRecord.getTollPointNameEntry());
    tollpointEntry.setGlobalGate(globalGateEntry);
    tollpointEntry.setTime(
      calculateInstantByDateAndTime(dgdTollRecord.getDateIn(), dgdTollRecord.getTimeIn(), yyyyMMdd_dateformat, HHmmss_timeformat).get());
    toConsumoPedaggio.setTollPointEntry(tollpointEntry);

    // CONSUMOPEDAGGIO.TOLLPOINTEXIT
    TollPoint tollpointExit = new TollPoint();
    GlobalGate globalGateExit = new GlobalGate(dgdTollRecord.getTollPointCodeExit());
    globalGateExit.setDescription(dgdTollRecord.getTollPointNameExit());
    tollpointExit.setGlobalGate(globalGateExit);
    tollpointExit.setTime(
      calculateInstantByDateAndTime(dgdTollRecord.getDateOut(), dgdTollRecord.getTimeOut(), yyyyMMdd_dateformat, HHmmss_timeformat).get());
    toConsumoPedaggio.setTollPointExit(tollpointExit);

    // CONSUMOPEDAGGIO.VEHICLE
//    var vehicle = new Vehicle();
//    toConsumoPedaggio.setVehicle(vehicle);

    // CONSUMOPEDAGGIO. altri campi
    toConsumoPedaggio.setAdditionalInfo(commonRecord.getDocumentNumber());
    toConsumoPedaggio.setExitTransitDateTime(commonRecord.getDateOut() + commonRecord.getTimeOut());

    // toConsumoPedaggio.setTotalAmount(new BigDecimal(commonRecord.getTotalAmount())); // Calcolato
    toConsumoPedaggio.getTransaction().setSign(dgdTollRecord.getSign());
    return toConsumo;
  }


}
