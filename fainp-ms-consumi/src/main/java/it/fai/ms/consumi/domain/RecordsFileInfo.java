package it.fai.ms.consumi.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.time.Instant;
import java.util.Set;

public class RecordsFileInfo {

  private final String filename;
  private final long startTime;
  private final Instant ingestionTime;

  private final ValidationOutcome validationOutcome;

  private long detailRecordsToRead;

  private CommonRecord header;


  @JsonCreator
  public RecordsFileInfo(@JsonProperty("filename") String _filename, @JsonProperty("startTime") long _startTime, @JsonProperty("ingestionTime") Instant _ingestionTime) {
    this.filename = _filename;
    this.startTime = _startTime;
    this.ingestionTime = _ingestionTime;
    this.validationOutcome = new ValidationOutcome();
  }

  public void setDetailRecordsToRead(long detailRecordsToRead) {
    this.detailRecordsToRead = detailRecordsToRead;
  }

  public long getDetailRecordsToRead() {
    return this.detailRecordsToRead;
  }

  public boolean fileIsValid() {
    return validationOutcome.isValidationOk();
  }

  public Set<Message> getValidationMessages() {
    return validationOutcome.getMessages();
  }

  public void addMessage(String code, String text) {
    validationOutcome.addMessage(new Message(code).withText(text));
  }

  public String getFilename() {
    return filename;
  }

  public CommonRecord getHeader() {
    return header;
  }

  public void setHeader(CommonRecord header) {
    this.header = header;
  }

  @Override
  public String toString() {
    return "RecordsFileInfo{" + "filename='" + filename + '\'' + ", startTime=" + startTime + ", ingestionTime=" + ingestionTime
      + ", header=" + header + ", validationOutcome=" + validationOutcome + ", detailRecordsToRead="
      + detailRecordsToRead + '}';
  }
}
