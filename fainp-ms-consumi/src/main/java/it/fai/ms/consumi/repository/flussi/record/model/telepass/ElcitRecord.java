package it.fai.ms.consumi.repository.flussi.record.model.telepass;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

import javax.money.MonetaryAmount;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.TimeUtils;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitDescriptor;


public class ElcitRecord extends CommonRecord implements MonetaryRecord, GlobalIdentifierBySupplier {

  private final static long serialVersionUID = 8429849328430432831L;

  private final static String NOT_SETTED = "not_setted";

  private String partnerCountryCode   = "";
  private String deviceCode           = null; // presente anche in record code: 30
  private String deviceType           = ""; // presente anche in record code: 30
  private String date                 = ""; // presente anche in record code: 30
  private String time                 = ""; // presente anche in record code: 30
  private String netCode              = ""; // presente anche in record code: 30
  private String entryGate            = ""; // presente anche in record code: 30
  private String exitGate             = ""; // presente anche in record code: 30
  private String description          = "";
  private String pricingClass         = "";
  private String km                   = "";
  private String currencyCode         = "";
  private String transactionType      = "";
  private String totalAmount          = "";
  private String signOfTheTransaction = "";
  private String licensePlate         = "";
  private String routeID              = "";
  private String invoiceDate          = "";
  private String sequenceTollDiscount = "";
  private String subContractCode      = "";

  private MonetaryAmount amount_vat = null;

  /**
   * record code : 30
   */
  private String vatCode      = "";
  private String vatRate      = NOT_SETTED;
  private String lawReference = "";
  private String amount       = "";
  private String rawDeviceCode;


  @JsonCreator
  public ElcitRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }

  @Override
  public int getNestedLevel() {
    if (getRecordCode().equals(ElcitDescriptor.BEGIN_20))
      return 0;
    else
      return 1;
  }

  @Override
  public boolean toBeSkipped() {
    return getNestedLevel()!=1 ;
  }

  public String getPartnerCountryCode() {
    return partnerCountryCode;
  }

  public void setPartnerCountryCode(String partnerCountryCode) {
    this.partnerCountryCode = partnerCountryCode;
  }

  public String getRawDeviceCode() {
    return rawDeviceCode;
  }

  public String getDeviceCode() {
    return deviceCode;
  }

  public void setDeviceCode(String deviceCode) {
    this.rawDeviceCode = deviceCode;
    if (deviceCode!=null && !deviceCode.trim().isEmpty()) {
      this.deviceCode = StringUtils.leftPad(deviceCode.trim().replaceAll("^0+", ""),11,"0");
    } else {
      this.deviceCode = null;
    }
  }

  public String getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(String deviceType) {
    this.deviceType = deviceType;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;

    try {
      this.rtdtm = TimeUtils.fromString(date, TimeUtils.DATE_FORMAT_SMALL);
      this.anno = TimeUtils.getYear(this.rtdtm);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;

    try {
      this.rttime = TimeUtils.fromString(time, "HH.mm.ss");
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getNetCode() {
    return netCode;
  }

  public void setNetCode(String netCode) {
    this.netCode = netCode;
  }

  public String getEntryGate() {
    return entryGate;
  }

  public void setEntryGate(String entryGate) {
    this.entryGate = entryGate;
  }

  public String getExitGate() {
    return exitGate;
  }

  public void setExitGate(String exitGate) {
    this.exitGate = exitGate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPricingClass() {
    return pricingClass;
  }

  public void setPricingClass(String pricingClass) {
    this.pricingClass = pricingClass;
  }

  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getTransactionType() {
    return transactionType;
  }

  public void setTransactionType(String transactionType) {
    this.transactionType = transactionType;
  }

  public String getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(String totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getSignOfTheTransaction() {
    return signOfTheTransaction;
  }

  public void setSignOfTheTransaction(String signOfTheTransaction) {
    this.signOfTheTransaction = signOfTheTransaction;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  public String getRouteID() {
    return routeID;
  }

  public void setRouteID(String routeID) {
    this.routeID = routeID;
  }

  public String getVatCode() {
    return vatCode;
  }

  public void setVatCode(String vatCode) {
    this.vatCode = vatCode;
  }

  public String getVatRate() {
    return vatRate;
  }

  /**
   * Il valore che perviene in vatRate è sempre 3 cifre del tipo "022", mentre in Elvia è 4 cifre del tipo "2200".
   * Ad ogni modo vatRateBigDecimal se valorizzato è già converito in "22.00"
   * @return
   */
  public String getVatRateElviaStyle() {
    if (this.vatRateBigDecimal!=null) {
      BigDecimal bd = this.vatRateBigDecimal;
      if (bd.toBigInteger().compareTo(new java.math.BigInteger("100"))>-1){
        throw new IllegalStateException("vatRate > 100%V not convertible to Elvia format!!!");
      }else{
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        return new DecimalFormat("00.00", otherSymbols).format(bd).replace(".","");
      }
    }
    return "";
  }

  public void setVatRate(String vatRate) {
    this.vatRate = vatRate;
    if(!NOT_SETTED.equals(vatRate))
      this.vatRateBigDecimal = NumericUtils.strToBigDecimal(vatRate, 100);
  }

  public String getLawReference() {
    return lawReference;
  }

  public void setLawReference(String lawReference) {
    this.lawReference = lawReference;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
    if (!StringUtils.isBlank(amount)) {
      if (StringUtils.isBlank(currencyCode))
        this.amount_vat = MonetaryUtils.strToMonetary(amount, -2,  MonetaryUtils.DEFAULT_CURRENCY_CODE );
      else
        this.amount_vat = MonetaryUtils.strToMonetaryIsoNumeric(amount, -2, currencyCode);
    }
  }

  public String getSubContractCode() {
    return subContractCode;
  }

  public void setSubContractCode(String subContractCode) {
    this.subContractCode = subContractCode;
  }

  public String getInvoiceDate() {
    return invoiceDate;
  }

  public void setInvoiceDate(String invoiceDate) {
    this.invoiceDate = invoiceDate;
  }

  public String getSequenceTollDiscount() {
    return sequenceTollDiscount;
  }

  public void setSequenceTollDiscount(String sequenceTollDiscount) {
    this.sequenceTollDiscount = sequenceTollDiscount;
  }

  @Override
  public String getGlobalIdentifier() {
    String optVatRate = getVatRateElviaStyle();
    BaseRecord pRecord = getParentRecord();
    if(pRecord == null|| ! (pRecord instanceof ElcitRecord)) return UUID.randomUUID().toString();
    ElcitRecord parent = (ElcitRecord) pRecord;
    if (Optional.ofNullable(parent.getTransactionType()).filter(deviceType -> "AC".equalsIgnoreCase(deviceType)).isPresent()) {
      return String.join("§", parent.getSubContractCode(), StringUtils.leftPad(StringUtils.trim(parent.getLicensePlate()).replaceAll("^0+", ""),11,"0"), "TG", parent.getDate(), parent.getTime(),
        parent.getTransactionType(), parent.getNetCode(), parent.getExitGate(), parent.getSignOfTheTransaction(), optVatRate)
        .replaceAll("null", "");
    } else {
      return String.join("§", parent.getSubContractCode(), parent.getDeviceCode(), parent.getDeviceType(), parent.getDate(), parent.getTime(),
        parent.getTransactionType(), parent.getNetCode(), parent.getExitGate(), parent.getSignOfTheTransaction(), optVatRate)
        .replaceAll("null", "");
    }
  }

  @Override
  public MonetaryAmount getAmount_vat() {
    return amount_vat;
  }

  @Override
  public MonetaryAmount getAmount_novat() {
    return null;
  }

  @Override
  public MonetaryAmount getAmount_novat_base_discount() {
    return null;
  }

  @Override
  public MonetaryAmount getAmount_novat_excluded_discount() {
    return null;
  }

}
