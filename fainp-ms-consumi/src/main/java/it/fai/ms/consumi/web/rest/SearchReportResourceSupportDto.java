package it.fai.ms.consumi.web.rest;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.Importo;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.CarburantiDetailDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.CarburantiDetailDto.Quantita;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.DispositiviDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.PedaggiDaFatturareDto;

class SearchReportrResourceSupportDto {
	public List<DispositiviDto> dispositivi;
	public Interval dateFilter;

	public SearchReportrResourceSupportDto() {

	}

	public SearchReportrResourceSupportDto(List<DispositiviDto> dispositivi, Interval dateFilter) {
		this.dispositivi = dispositivi;
		this.dateFilter = dateFilter;
	}
}


class CarburantiDaFatturareResponseDto{

  public final List<CarburantiDetailDto> detail;
  public final Map<String, Quantita> totals;

  public CarburantiDaFatturareResponseDto(List<CarburantiDetailDto> detail) {
    this.detail = detail;
    this.totals = calculateTotals(detail);
  }

   Map<String, Quantita> calculateTotals(List<CarburantiDetailDto> detail) {
    HashMap<String, Quantita> totals = new HashMap<>();
    for (CarburantiDetailDto carburantiDetailDto : detail) {
      if(carburantiDetailDto.getDescrizioneArticolo()==null)
    	  carburantiDetailDto.setDescrizioneArticolo("N.D.");
      BigDecimal total;
      Quantita quantita = carburantiDetailDto.getQuantita();
      if (totals.containsKey(carburantiDetailDto.getDescrizioneArticolo())) {
        total = totals.get(carburantiDetailDto.getDescrizioneArticolo()).valore.add(quantita.valore);
      }else {
        total = quantita.valore;
      }
      totals.put(carburantiDetailDto.getDescrizioneArticolo(), new CarburantiDetailDto.Quantita(total,quantita.unita));
    }
    return totals;
  }

}

class PedaggiDafatturareDetailResponse{

	public  List<PedaggiDaFatturareDto> details;
	public Importo totale;

	public PedaggiDafatturareDetailResponse(List<PedaggiDaFatturareDto> detailsPedaggiNonFatturati) {
		this.details = detailsPedaggiNonFatturati;
		this.totale = calculateTotal(detailsPedaggiNonFatturati);
	}
	private Importo calculateTotal(List<PedaggiDaFatturareDto> nonFatturati) {
		BigDecimal totale = nonFatturati.stream().map(p->p.getImportoLordoSoggettoIva().getValore()).reduce(BigDecimal.ZERO, BigDecimal::add);
		var valuta = nonFatturati.stream().findFirst().map(p->p.getImportoLordoSoggettoIva().getValuta()).orElse("");
		Importo total = new Importo(totale, valuta);
		return total;
	}
}