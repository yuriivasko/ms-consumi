package it.fai.ms.consumi.repository.caselli.model;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.repository.flussi.record.model.stcon.StconRecord;

@Component
@Validated
public class CaselliEntityMapper {
  
  public CaselliEntityMapper() {
    
  }

  public CaselliEntity toEntity(final StconRecord record) {
    CaselliEntity e = new CaselliEntity();
    e.setDescription(record.getDescription());
    e.setDistance(record.getDistance());
    e.setEndDate(record.getEndDate());
    e.setGateIdentifier(record.getGateIdentifier());
    e.setGateNumber(record.getGateNumber());
    e.setGlobalGateIdentifier(record.getGlobalGateIdentifier());
    e.setLaneCode(record.getLaneCode());
    e.setMainTypeOfGate(record.getMainTypeOfGate());
    e.setMotorways(record.getMotorways());
    e.setSubTypeOfGate(record.getSubTypeOfGate());
    e.setNetwork(null);
    return e;
  }

//  public CaselliEntity toEntity(StconRecord record, String network) {
//    CaselliEntity e = new CaselliEntity();
//    e.setDescription(record.getDescription());
//    e.setDistance(record.getDistance());
//    e.setEndDate(record.getEndDate());
//    e.setGateIdentifier(record.getGateIdentifier());
//    e.setGateNumber(record.getGateNumber());
//    e.setGlobalGateIdentifier(record.getGlobalGateIdentifier());
//    e.setLaneCode(record.getLaneCode());
//    e.setMainTypeOfGate(record.getMainTypeOfGate());
//    e.setMotorways(record.getMotorways());
//    e.setSubTypeOfGate(record.getSubTypeOfGate());
//    e.setNetwork(network);
//    return e;
//  }

  public CaselliEntity toEntity(StconRecord record, String network, String endDate) {
    CaselliEntity e = new CaselliEntity();
    e.setDescription(record.getDescription());
    e.setDistance(record.getDistance());
    e.setEndDate(endDate);
    e.setGateIdentifier(record.getGateIdentifier());
    e.setGateNumber(record.getGateNumber());
    e.setGlobalGateIdentifier(record.getGlobalGateIdentifier());
    e.setLaneCode(record.getLaneCode());
    e.setMainTypeOfGate(record.getMainTypeOfGate());
    e.setMotorways(record.getMotorways());
    e.setSubTypeOfGate(record.getSubTypeOfGate());
    e.setNetwork(network);
    return e;
  }
  
}
