package it.fai.ms.consumi.repository.viewallegati;

import static java.util.stream.Collectors.toSet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ViewAllegatoPedaggioSezione1RepositoryImpl implements ViewAllegatoPedaggioSezione1Repository {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final EntityManager em;

  public ViewAllegatoPedaggioSezione1RepositoryImpl(final EntityManager _em) {
    em = _em;
  }

  @Override
  public Set<ViewAllegatoPedaggioSezione1Entity> findBySezioneIdClass(ViewAllegatoPedaggioSezione1Id keyIdClass) {
    final var cb = em.getCriteriaBuilder();
    CriteriaQuery<ViewAllegatoPedaggioSezione1Entity> cq = cb.createQuery(ViewAllegatoPedaggioSezione1Entity.class);
    Root<ViewAllegatoPedaggioSezione1Entity> root = cq.from(ViewAllegatoPedaggioSezione1Entity.class);

    List<Predicate> predicates = new ArrayList<>();
    addPredicateOfKeyIdClass(cb, root, predicates, keyIdClass);

    cq.select(root);
    cq.where(predicates.toArray(new Predicate[] {}));

    TypedQuery<ViewAllegatoPedaggioSezione1Entity> query = em.createQuery(cq);
    logQuery(query);

    List<ViewAllegatoPedaggioSezione1Entity> resultList = query.getResultList();
    Set<ViewAllegatoPedaggioSezione1Entity> resultSet = resultList.stream()
                                                                  .collect(toSet());
    return resultSet;
  }

  @Override
  public Set<ViewAllegatoPedaggioSezione1Entity> findBySezioneIdClassAndRangeDateNotInvoiced(ViewAllegatoPedaggioSezione1Id keyIdClass,
                                                                                             LocalDate dateFrom, LocalDate dateTo) {
    final var cb = em.getCriteriaBuilder();
    CriteriaQuery<ViewAllegatoPedaggioSezione1Entity> cq = cb.createQuery(ViewAllegatoPedaggioSezione1Entity.class);
    Root<ViewAllegatoPedaggioSezione1Entity> root = cq.from(ViewAllegatoPedaggioSezione1Entity.class);

    List<Predicate> predicates = new ArrayList<>();
    addPredicateOfKeyIdClass(cb, root, predicates, keyIdClass);
    if (dateFrom != null) {
      predicates.add(cb.greaterThanOrEqualTo(root.get(ViewAllegatoPedaggioSezione1Entity_.DATA_EROGAZIONE_SERVIZIO), dateFrom));
    }
    predicates.add(cb.lessThanOrEqualTo(root.get(ViewAllegatoPedaggioSezione1Entity_.DATA_EROGAZIONE_SERVIZIO), dateTo));

    cq.select(root);
    cq.where(predicates.toArray(new Predicate[] {}));

    TypedQuery<ViewAllegatoPedaggioSezione1Entity> query = em.createQuery(cq);
    logQuery(query);
    List<ViewAllegatoPedaggioSezione1Entity> resultList = query.getResultList();
    Set<ViewAllegatoPedaggioSezione1Entity> resultSet = resultList.stream()
                                                                  .collect(toSet());
    return resultSet;
  }

  private void addPredicateOfKeyIdClass(CriteriaBuilder cb, Root<ViewAllegatoPedaggioSezione1Entity> root, List<Predicate> predicates,
                                        ViewAllegatoPedaggioSezione1Id keyIdClass) {
    predicates.add(cb.equal(root.get(ViewAllegatoPedaggioSezione1Entity_.CODICE_CONTRATTO), keyIdClass.getCodiceContratto()));
    predicates.add(cb.equal(root.get(ViewAllegatoPedaggioSezione1Entity_.TIPO_DISPOSITIVO), keyIdClass.getTipoDispositivo()));
    predicates.add(cb.equal(root.get(ViewAllegatoPedaggioSezione1Entity_.PAN_NUMBER), keyIdClass.getPanNumber()));
    // predicates.add(cb.equal(root.get(ViewAllegatoPedaggioSezione1Entity_.SERIAL_NUMBER),
    // keyIdClass.getSerialNumber())); // Per ora non viene utilizzato
  }

  private void logQuery(TypedQuery<?> query) {
    log.debug("Query string: {}", query.unwrap(Query.class)
                                       .getQueryString());
  }

}
