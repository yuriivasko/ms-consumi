package it.fai.ms.consumi.service.canoni;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Objects;

import it.fai.ms.common.jms.fattura.canoni.EsitoCanoni;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

public class DispositivoForCanoni {

  private String idDispositivo;
  private String idContratto;
  private String codiceCliente;

  private String    targa;
  private String    targaNazione;
  private LocalDate dataPrimaAttivazione;
  private int       quantita;

  private DispositivoForCanoni(String targa, String targaNazione, String codiceCliente, LocalDate dataAttivazione, int qunatita) {
    this.targa        = targa;
    this.targaNazione = targaNazione;
    this.setCodiceCliente(codiceCliente);
    this.setDataPrimaAttivazione(dataAttivazione);
    quantita = qunatita;
  }

  public DispositivoForCanoni(String idDispositivo, String idContratto,String codiceCliente, Timestamp dataPrimaAttivazione) {
    quantita = 1;
    this.codiceCliente = codiceCliente;
    this.idDispositivo = idDispositivo;
    this.idContratto   = idContratto;
    this.dataPrimaAttivazione = dataPrimaAttivazione.toLocalDateTime().toLocalDate();
  }



  public String getDeviceID() {
    return idDispositivo;
  }

  public static DispositivoForCanoni copy(DispositivoForCanoni d) {
    return new DispositivoForCanoni(d.targa, d.targaNazione, d.getCodiceCliente(), d.dataPrimaAttivazione, 1);
  }

  public DispositivoForCanoni combine(DispositivoForCanoni d2) {
    quantita += d2.quantita;
    return this;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public void setVeicle(StoricoVeicolo v) {
    targa        = v.getTarga();
    targaNazione = v.getNazione();
  }

  public void setDataPrimaAttivazione(LocalDate dataAttivazione) {
    this.dataPrimaAttivazione = dataAttivazione;
  }

  public static DispositivoForCanoni createFromVehicleDataPrimaAttivazione(DispositivoForCanoni d) {
    return new DispositivoForCanoni(d.targa, d.targaNazione, d.getCodiceCliente(), d.dataPrimaAttivazione, 1);
  }

  public EsitoCanoni toEsitoCanoni() {
    return new EsitoCanoni().codiceCliente(getCodiceCliente())
      .dataAttivazione(dataPrimaAttivazione)
      .quantita(quantita)
      .targa(targa)
      .targaNazione(targaNazione);
  }

  public String getIdContratto() {
    return idContratto;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCodiceCliente(), dataPrimaAttivazione, idDispositivo, idContratto, quantita, targa, targaNazione);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DispositivoForCanoni other = (DispositivoForCanoni) obj;
    return Objects.equals(getCodiceCliente(), other.getCodiceCliente()) && Objects.equals(dataPrimaAttivazione, other.dataPrimaAttivazione)
           && Objects.equals(idDispositivo, other.idDispositivo) && Objects.equals(idContratto, other.idContratto)
           && quantita == other.quantita && Objects.equals(targa, other.targa) && Objects.equals(targaNazione, other.targaNazione);
  }

  @Override
  public String toString() {
    return "DispositivoForCanoni [idDispositivo=" + idDispositivo + ", idContratto=" + idContratto + ", codiceCliente=" + getCodiceCliente()
           + ", targa=" + targa + ", targaNazione=" + targaNazione + ", dataPrimaAttivazione=" + dataPrimaAttivazione + ", quantita="
           + quantita + "]";
  }

  public void setIdContratto(String dmlUniqueIdentifier) {
    this.idContratto = dmlUniqueIdentifier;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

}
