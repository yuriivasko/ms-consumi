package it.fai.ms.consumi.web.rest;

import static it.fai.ms.consumi.web.rest.SendStanziamentiToNavResource.StanziamentoSentDTO.OUTCOME.ERROR;
import static it.fai.ms.consumi.web.rest.SendStanziamentiToNavResource.StanziamentoSentDTO.OUTCOME.SENT;

import java.io.Serializable;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoEntityRepository;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.scheduler.jobs.StanziamentoDTOMapper;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;

@RestController
@RequestMapping(SendStanziamentiToNavResource.BASE_PATH)
@Transactional(propagation = Propagation.NEVER)
public class SendStanziamentiToNavResource {

	static final String BASE_PATH = "/api/public";

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final String SEND_STANZIAMENTI_TO_NAV = "/sendallstanziamentitonav";

	private final StanziamentoRepository stanziamentoRepository;

	private final StanziamentiToNavPublisher stanziamentiToNavJmsProducer;

	private final StanziamentoEntityMapper stanziamentoEntityMapper;
	private final StanziamentoEntityRepository stanziamentoEntityRepository;

	public SendStanziamentiToNavResource(StanziamentoRepository stanziamentoRepository,
			StanziamentiToNavPublisher stanziamentiToNavJmsProducer, StanziamentoEntityMapper stanziamentoEntityMapper,
			StanziamentoEntityRepository stanziamentoEntityRepository) {
		this.stanziamentoRepository = stanziamentoRepository;
		this.stanziamentiToNavJmsProducer = stanziamentiToNavJmsProducer;
		this.stanziamentoEntityMapper = stanziamentoEntityMapper;
		this.stanziamentoEntityRepository = stanziamentoEntityRepository;
	}

	public static class StanziamentoSentDTO implements Serializable {

		private static final long serialVersionUID = 979745319644229562L;

		public enum OUTCOME {
			SENT, ERROR
		}

		private OUTCOME outcome = SENT;
		private StanziamentoMessage stanziamentoMessage;
		private Exception exception;

		public StanziamentoSentDTO(StanziamentoMessage stanziamentoMessage, Exception exception) {
			this.stanziamentoMessage = stanziamentoMessage;
			this.exception = exception;
			if (exception != null) {
				outcome = ERROR;
			}
		}

		public StanziamentoSentDTO(StanziamentoMessage stanziamentoMessage) {
			this(stanziamentoMessage, null);
		}

		public OUTCOME getOutcome() {
			return outcome;
		}

		public StanziamentoMessage getStanziamentoMessage() {
			return stanziamentoMessage;
		}

		public Exception getException() {
			return exception;
		}

		@Override
		public String toString() {
			return "StanziamentoSentDTO{" + "outcome=" + outcome + ", stanziamentoMessage=" + stanziamentoMessage
					+ ", exception=" + exception + '}';
		}
	}

	// @GetMapping(SEND_STANZIAMENTI_TO_NAV)
	public ResponseEntity<List<StanziamentoSentDTO>> sendAllStanziamentiToNav() {

		log.info("Request to send all waiting stanaziamenti to NAV");

		List<Stanziamento> sanziamentiToSend = stanziamentoRepository.find100NotQueuedToNav();
		while (!sanziamentiToSend.isEmpty()) {
			log.info("Found [{}] stanziamenti to send", sanziamentiToSend.size());
			sendStanziamentiList(sanziamentiToSend);
			sanziamentiToSend = stanziamentoRepository.find100NotQueuedToNav();
		}

		List<StanziamentoSentDTO> sanziamentiSent = Collections.emptyList();
		log.info("Sent result: {}", sanziamentiSent);
		return ResponseEntity.ok(sanziamentiSent);
	}

	// @GetMapping(SEND_STANZIAMENTI_TO_NAV + "/filtered")
	public ResponseEntity<List<StanziamentoSentDTO>> sendAllStanziamentiToNavByDataAcquisizioneFlusso(
			@RequestParam(value = "dataAcquisizioneFlusso", required = true) Instant dataAcquisizioneFlusso) {

		log.info("Request to send all waiting stanaziamenti to NAV");

//    Instant dataAcquisizioneFlussoInstant = FaiConsumiDateUtil.parseInstant("YYYY-MM-dd HH:mm:ss", dataAcquisizioneFlusso, null);
//    log.info("Parsed dataAcquisizioneFlussoInstant:=[{}]", dataAcquisizioneFlusso);

		List<Stanziamento> stanziamentiToSend = stanziamentoRepository.find100NotQueuedToNav(dataAcquisizioneFlusso);
		while (!stanziamentiToSend.isEmpty()) {
			log.info("Found [{}] stanziamenti to send", stanziamentiToSend.size());
			sendStanziamentiList(stanziamentiToSend);
			stanziamentiToSend = stanziamentoRepository.find100NotQueuedToNav();

		}

		List<StanziamentoSentDTO> sanziamentiSent = Collections.emptyList();

		log.info("Sent result: {}", sanziamentiSent);
		return ResponseEntity.ok(sanziamentiSent);
	}

	@PostMapping(SEND_STANZIAMENTI_TO_NAV)
  @Timed
  @Async
  public void sendAllStanziamenti(@RequestParam(required = true, name = "dataAcquisizioneFlusso", defaultValue = "2119-04-04T00:00:00.000Z") OffsetDateTime dateTime,
                                @RequestParam(required = true, name = "pageStart", defaultValue="0") int pageStart,
                                @RequestParam(required = true, name = "pageTo", defaultValue="0") int pageTo,
                                @RequestParam(required = true, name = "pageSize", defaultValue="1000") int pageSize,
                                @RequestParam(required = true, name = "DateQueuingIsNull", defaultValue="true") boolean dateQueuingIsNull,
                                @RequestParam(required = true, name = "test", defaultValue="true") boolean test) {
	  
	  Instant dataAcquisizioneFlusso = dateTime.toInstant();
			  
    log.info("REST request to send bulk Stanziamenti: dataAcquisizioneFlusso {}, pageStart {}, pageTo={}, pageSize={}",dataAcquisizioneFlusso,pageStart,pageTo,pageSize); 

    Set<String> codes = new HashSet<>();
    
    int pageNumber = 0;
      Pageable pageable = PageRequest.of(pageStart, pageSize);
      while (pageable != null) {
          pageNumber++;

        // se pageTo è zero o è maggiore della paggina attuale -> exit
        if(pageTo>0 && pageNumber>=pageTo) {
          log.info("Stanziamenti Bulk finish for pageTo limit: currentPage {} pageStart {}, pageTo={}, pageSize={}",pageNumber,pageStart,pageTo,pageSize); 
          return;
        }
        
        log.info("Elaborazione pagina {} : pageStart {}, pageTo={}, pageSize={}", pageNumber,pageStart,pageTo,pageSize);
        Page<StanziamentoEntity> page = dateQueuingIsNull ? stanziamentoEntityRepository.findByDataAcquisizioneFlussoAndDateQueuingIsNullAndDateNavSentIsNull(dataAcquisizioneFlusso,pageable) :
        	stanziamentoEntityRepository.findByDataAcquisizioneFlussoAndDateNavSentIsNull(dataAcquisizioneFlusso,pageable) ;
        log.info("Found {} dispositivi", pageable.getPageSize());
        
        if(!test) {
        	codes = sendStanziamentiEntList(page,codes);
        	if(codes.isEmpty()) {
        		pageable = null;
        	}
        }else {
        	for (StanziamentoEntity stanziamentoEntity : page) {
            	log.info(stanziamentoEntity.toString());
			}
            pageable = page.hasNext() ? page.nextPageable() : null;
        }
      }
      
    log.info("Stanziamenti Bulk finish: pageStart {}, pageTo={}, pageSize={}",pageStart,pageTo,pageSize); 
  }

	@GetMapping(SEND_STANZIAMENTI_TO_NAV + "/codice/{codice}")
	public ResponseEntity<List<StanziamentoSentDTO>> sendStanziamentoToNavByCodice(
			@PathVariable("codice") String codice) {

		log.info("Request to send stanaziamento {} to NAV", codice);

		Stanziamento stanziamentiToSend = stanziamentoRepository.findByCodiceStanziamento(codice);
		if (stanziamentiToSend != null) {
			log.info("Found [{}] stanziamenti to send", stanziamentiToSend);

			sendStanziamentiList(Arrays.asList(stanziamentiToSend));

		}

		List<StanziamentoSentDTO> sanziamentiSent = Collections.emptyList();

		log.info("Sent result: {}", sanziamentiSent);
		return ResponseEntity.ok(sanziamentiSent);
	}

	private Set<String> sendStanziamentiEntList(final Page<StanziamentoEntity> page, Set<String> previusCodes) {
		
		Set<String> codes = new HashSet<String>();

		List<StanziamentoMessage> stanziamentoMessage = page.stream()
				.peek(s -> codes.add(s.getCode()))
				.map(stanziamento -> new StanziamentoDTOMapper()
						.mapToStanziamentoDTO(stanziamentoEntityMapper.toDomainWithoutDetttagli(stanziamento)))
				.collect(Collectors.toList());

		if (!stanziamentoMessage.isEmpty()) {
			if(previusCodes.removeAll(codes)) {
				log.warn("trovato almeno uno stanzimento già inviato: possibile loop");
				return new HashSet<>();
			}
			
			log.info("sending stanziamentoMessage to NAV: ", stanziamentoMessage);
			stanziamentiToNavJmsProducer.publish(stanziamentoMessage);
		}
		return codes;
	}

	private void sendStanziamentiList(final Collection<Stanziamento> _stanziamentiToSend) {

		List<StanziamentoMessage> stanziamentoMessage = _stanziamentiToSend.stream()
				.map(stanziamento -> new StanziamentoDTOMapper().mapToStanziamentoDTO(stanziamento))
				.collect(Collectors.toList());

		log.info("sending stanziamentoMessage to NAV: ", stanziamentoMessage);
		stanziamentiToNavJmsProducer.publish(stanziamentoMessage);

	}

}
