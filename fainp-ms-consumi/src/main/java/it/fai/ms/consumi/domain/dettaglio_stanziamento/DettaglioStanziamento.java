package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import static java.util.Objects.requireNonNull;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

public abstract class DettaglioStanziamento {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private Amount                  amount;
  private String                  articlesGroup;
  private Customer                customer;
  private Contract                contract;
  private Instant                 date;
  private GlobalIdentifier        globalIdentifier;
  private InvoiceType             invoiceType;
  private String                  recordCode;
  private String                  region;
  private ServicePartner          servicePartner;
  private Source                  source;
  private final Set<Stanziamento> stanziamenti = new HashSet<>();
  private Supplier                supplier;
  private Transaction             transaction;

  public DettaglioStanziamento(final GlobalIdentifier _globalIdentifier) {
    globalIdentifier = requireNonNull(_globalIdentifier, "Parameter global identifier is mandatory");
  }

  public DettaglioStanziamento(final GlobalIdentifier _globalIdentifier, final InvoiceType _invoiceType) {
    globalIdentifier = requireNonNull(_globalIdentifier, "Parameter global identifier is mandatory");
    invoiceType = requireNonNull(_invoiceType, "Parameter invoice type is mandatory");
  }

  public void changeInvoiceTypeToD() {
    invoiceType = InvoiceType.D;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final DettaglioStanziamento obj = getClass().cast(_obj);
      res = Objects.equals(obj.amount, amount) && Objects.equals(obj.date, date) && Objects.equals(obj.globalIdentifier, globalIdentifier)
            && Objects.equals(obj.invoiceType, invoiceType) && Objects.equals(obj.recordCode, recordCode)
            && Objects.equals(obj.region, region) && Objects.equals(obj.source, source) && Objects.equals(obj.transaction, transaction)
            && Objects.equals(obj.servicePartner, servicePartner) && Objects.equals(obj.contract, contract);
    }
    return res;
  }

  public int extractYear() {
    return LocalDateTime.ofInstant(date, ZoneOffset.UTC)
                        .getYear();
  }

  public DettaglioStanziamento generateNewGlobalIdentifier() {
    globalIdentifier = globalIdentifier.newGlobalIdentifier();
    return this;
  }

  public Amount getAmount() {
    return amount;
  }

  public String getArticlesGroup() {
    return articlesGroup;
  }

  public Customer getCustomer() {
    return customer;
  }

  public Instant getDate() {
    return date;
  }

  public GlobalIdentifier getGlobalIdentifier() {
    return globalIdentifier;
  }

  public InvoiceType getInvoiceType() {
    return invoiceType;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public String getRegion() {
    return region;
  }

  public ServicePartner getServicePartner() {
    return servicePartner;
  }

  public Source getSource() {
    return source;
  }

  public Set<Stanziamento> getStanziamenti() {
    return stanziamenti;
  }

  public Supplier getSupplier() {
    return supplier;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public boolean hasCustomer() {
    return customer != null && customer.getId() != null && !customer.getId()
                                                                    .trim()
                                                                    .isEmpty();
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, date, globalIdentifier, invoiceType, recordCode, region, source, transaction, servicePartner);
  }

  public boolean hasTheSameAmount(final DettaglioStanziamento _dettaglioStanziamentoDefinitivo) {
    boolean res = false;
    res = _dettaglioStanziamentoDefinitivo.getAmount()
                                          .equals(amount);
    _log.info("Amount {} the same between {} and {}", res ? "IS" : "IA NOT", _dettaglioStanziamentoDefinitivo, this);
    return res;
  }

  public boolean hasTheSameCurrency(final String _currency) {
    boolean res = true;
    if (_currency != null && !_currency.trim()
                                       .isEmpty()) {
      res = amount.getCurrency()
                  .getCurrencyCode()
                  .equalsIgnoreCase(_currency);
    }
    return res;
  }

  public void setAmount(final Amount _amount) {
    amount = _amount;
  }

  public void setArticlesGroup(final String _articlesGroup) {
    articlesGroup = _articlesGroup;
  }

  public void setCustomer(final Customer _customer) {
    customer = _customer;
  }

  public void setDate(final Instant _date) {
    date = _date;
  }

  public void setInvoiceType(final InvoiceType _invoiceType) {
    invoiceType = _invoiceType;
  }

  public void setRecordCode(final String _recordCode) {
    recordCode = _recordCode;
  }

  public void setRegion(final String _region) {
    region = _region;
  }

  public void setServicePartner(final ServicePartner _servicePartner) {
    servicePartner = _servicePartner;
  }

  public void setSource(final Source _source) {
    source = _source;
  }

  public void setSupplier(final Supplier _supplier) {
    supplier = _supplier;
  }

  public void setTransaction(final Transaction _transaction) {
    transaction = _transaction;
  }

  public Contract getContract() {
    return contract;
  }

  public void setContract(Contract contract) {
    this.contract = contract;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("invoiceType=")
                              .append(invoiceType)
                              .append(",")
                              .append("contract=")
                              .append(contract)
                              .append(",")
                              .append(amount)
                              .append(",")
                              .append(customer)
                              .append(",")
                              .append("date=")
                              .append(date)
                              .append(",")
                              .append(globalIdentifier)
                              .append(",")
                              .append(invoiceType)
                              .append(",articlesGroup=")
                              .append(articlesGroup)
                              .append(",recordCode=")
                              .append(recordCode)
                              .append(",region=")
                              .append(region)
                              .append(",")
                              .append(servicePartner)
                              .append(",")
                              .append(source)
                              .append(",")
                              .append(supplier)
                              .append(",")
                              .append(transaction)
                              .append(",")
                              .append(stanziamenti.isEmpty() ? "" : stanziamenti)
                              .append("]")
                              .toString();
  }

}
