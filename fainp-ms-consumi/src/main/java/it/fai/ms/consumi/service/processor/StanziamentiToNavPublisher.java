package it.fai.ms.consumi.service.processor;

import java.util.List;

import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;

public interface StanziamentiToNavPublisher {

  void publish(List<StanziamentoMessage> _stanziamentoMessages);

}