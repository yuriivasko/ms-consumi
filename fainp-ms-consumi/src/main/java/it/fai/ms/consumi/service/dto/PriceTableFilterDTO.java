package it.fai.ms.consumi.service.dto;

import java.io.Serializable;
import java.time.Instant;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.domain.InvoiceType;

public class PriceTableFilterDTO implements Serializable {

  private static final long serialVersionUID = 3325167880006960434L;

  private String      articleCode;
  private String      supplierCode;
  private InvoiceType invoiceType;
  private CostRevenue costRevenueFlag;
  private Instant     dateFrom;
  private Instant     dateTo;

  public PriceTableFilterDTO() {
  }

  public PriceTableFilterDTO(PriceTableDTO _priceTable) {
    articleCode = _priceTable.getCodiceArticolo();
    supplierCode = _priceTable.getSupplierCode();
    invoiceType = InvoiceType.P;
    costRevenueFlag = _priceTable.getCostRevenueFlag();
    dateFrom = _priceTable.getValidFrom();
    dateTo = (_priceTable.getValidUntil() != null) ? _priceTable.getValidUntil() : null;
  }

  public String getArticleCode() {
    return articleCode;
  }

  public void setArticleCode(String articleCode) {
    this.articleCode = articleCode;
  }

  public Instant getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(Instant dateFrom) {
    this.dateFrom = dateFrom;
  }

  public Instant getDateTo() {
    return dateTo;
  }

  public void setDateTo(Instant dateTo) {
    this.dateTo = dateTo;
  }

  public CostRevenue getCostRevenueFlag() {
    return costRevenueFlag;
  }

  public void setCostRevenueFlag(CostRevenue costRevenueFlag) {
    this.costRevenueFlag = costRevenueFlag;
  }

  public InvoiceType getInvoiceType() {
    return invoiceType;
  }

  public void setInvoiceType(InvoiceType invoiceType) {
    this.invoiceType = invoiceType;
  }

  public String getSupplierCode() {
    return supplierCode;
  }

  public void setSupplierCode(String supplierCode) {
    this.supplierCode = supplierCode;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((articleCode == null) ? 0 : articleCode.hashCode());
    result = prime * result + ((costRevenueFlag == null) ? 0 : costRevenueFlag.hashCode());
    result = prime * result + ((dateFrom == null) ? 0 : dateFrom.hashCode());
    result = prime * result + ((dateTo == null) ? 0 : dateTo.hashCode());
    result = prime * result + ((invoiceType == null) ? 0 : invoiceType.hashCode());
    result = prime * result + ((supplierCode == null) ? 0 : supplierCode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null)
      return false;
    if (obj == this)
      return true;

    PriceTableFilterDTO other = (PriceTableFilterDTO) obj;
    if (!other.getArticleCode()
              .equals(this.articleCode))
      return false;
    if (!other.getSupplierCode()
              .equals(this.supplierCode))
      return false;
    if (other.getInvoiceType() != this.invoiceType)
      return false;
    if (other.getCostRevenueFlag() != this.costRevenueFlag)
      return false;
    if (this.dateFrom != null && (other.getDateFrom()
                                       .isAfter(this.dateFrom)
                                  || other.getDateFrom()
                                          .isBefore(this.dateFrom)))
      return false;
    if (this.dateTo != null && (other.getDateTo()
                                     .isAfter(this.dateTo)
                                || other.getDateTo()
                                        .isBefore(this.dateTo)))
      return false;

    return true;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("\nPriceTableFilterDTO [");
    sb.append("\t ArticoleCode: ");
    sb.append(articleCode);
    sb.append("\t SupplierCode: ");
    sb.append(supplierCode);
    sb.append("\t InvoiceType: ");
    sb.append(invoiceType);
    sb.append("\t CostRevenueFlag: ");
    sb.append(costRevenueFlag);
    sb.append("\t DateFrom: ");
    sb.append(dateFrom);
    sb.append("\t DateTo: ");
    sb.append(dateTo);
    sb.append("]");
    return sb.toString();
  }

}
