package it.fai.ms.consumi.repository.flussi.record.model.generici;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;

public class DartfordRecord extends CommonRecord implements GlobalIdentifierBySupplier {

  private static final long serialVersionUID = -2196939081059590849L;

  private String crossingDate = "";
  private String time         = "";
  private String direction    = "";
  private String vehicle      = "";
  private String groupName    = "";
  private String billedOn     = "";

  private String globalIdentifier;

  @JsonCreator
  public DartfordRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID()
                           .toString();
  }

  public String getCrossingDate() {
    return crossingDate;
  }

  public void setCrossingDate(String crossingDate) {
    this.crossingDate = crossingDate;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public String getVehicle() {
    return vehicle;
  }

  public void setVehicle(String vehicle) {
    this.vehicle = vehicle;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getBilledOn() {
    return billedOn;
  }

  public void setBilledOn(String billedOn) {
    this.billedOn = billedOn;
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getDefaultCurrency() {
    return "EUR";
  }

}
