package it.fai.ms.consumi.repository.storico_dml.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.ContrattoDMLDTO;
import it.fai.ms.common.dml.mappable.DmlListenerMapper;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@Component
public class StoricoContrattoDmlListenerMapper implements DmlListenerMapper<ContrattoDMLDTO, StoricoContratto> {

  @Override
  public StoricoContratto toORD(ContrattoDMLDTO dml, StoricoContratto to) {

    if (to == null) {
      to = new StoricoContratto();
    }

    to.setCodiceAzienda(dml.getCodiceAzienda());
    to.setContrattoNumero(dml.getCodContrattoCliente());
    to.setDmlRevisionTimestamp(dml.getDmlRevisionTimestamp());
    to.setDmlUniqueIdentifier(dml.getDmlUniqueIdentifier());
    to.setDataVariazione(dml.getDataUltimaVariazione());
    to.setStatoContratto(dml.getStato());
    to.setPrimario(dml.isPrimario());

    return to;
  }
}
