package it.fai.ms.consumi.service.processor.consumi.tollcollect;

import java.util.Optional;

import javax.inject.Inject;
import javax.money.MonetaryAmount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class TollCollectProcessorImpl extends ConsumoAbstractProcessor implements TollCollectProcessor{

  private final transient Logger log = LoggerFactory.getLogger(getClass());


  private StanziamentiProducer stanziamentiProducer;
  private TollCollectConsumoDettaglioStanziamentoPedaggioMapper mapper;

  @Inject
  public TollCollectProcessorImpl(ConsumoValidatorService consumoValidatorService,
                                  VehicleService vehicleService,
                                  CustomerService customerService,
                                  StanziamentiProducer stanziamentiProducer,
                                  TollCollectConsumoDettaglioStanziamentoPedaggioMapper mapper) {
    super(consumoValidatorService,  vehicleService, customerService);
    this.stanziamentiProducer = stanziamentiProducer;
    this.mapper = mapper;
  }

  @Override
  public ProcessingResult validateAndProcess(TollCollect tollCollect, Bucket bucket) {
    log.info(">");
    log.info("{} processing start {}", bucket, tollCollect);
    log.info("  --> {}", tollCollect);

    var processingResult = validateAndNotify(tollCollect);

    log.debug(" - {} processing validation completed", tollCollect);

    final var consumo = processingResult.getConsumo();


    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
        .ifPresent(stanziamentiParams -> {
          Amount consumoAmount = consumo.getAmount();
          final MonetaryAmount amountIncludedVat = consumoAmount.getAmountIncludedVat();
          setVatRateFromStanziamentiParams(consumo, Optional.empty(), Optional.of(amountIncludedVat), stanziamentiParams);
          final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
          final var stanziamenti = stanziamentiProducer.produce(dettaglioStanziamento, stanziamentiParams);
          processingResult.getStanziamenti().addAll(stanziamenti);
        });
    } else {
      log.warn(" - {} can't be processed", consumo);
    }

    log.info("  --> {}", processingResult);
    log.info("< processing completed {}", tollCollect.getSource());
    log.info("<");

    return processingResult;
  }

  private DettaglioStanziamento mapConsumoToDettaglioStanziamento(final Consumo _consumo) {
    return mapConsumoToDettaglioStanziamento(_consumo, mapper);
  }
}
