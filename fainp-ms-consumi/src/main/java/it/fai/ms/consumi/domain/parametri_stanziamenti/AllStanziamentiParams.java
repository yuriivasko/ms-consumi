package it.fai.ms.consumi.domain.parametri_stanziamenti;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.domain.Article;

public class AllStanziamentiParams {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final List<StanziamentiParams> stanziamentiParams = new LinkedList<>();

  public AllStanziamentiParams() {
  }

  public AllStanziamentiParams(final List<StanziamentiParams> _stanziamentiParams) {
    stanziamentiParams.addAll(_stanziamentiParams);
  }

  public List<Article> getAllArticles() {
    return stanziamentiParams.stream()
                             .map(stanziamentiParam -> stanziamentiParam.getArticle())
                             .collect(toList());
  }

  public Set<String> getAllProviderCodes() {
    return stanziamentiParams.stream()
                             .map(stanziamentiParam -> stanziamentiParam.getSupplier()
                                                                        .getCode())
                             .collect(toSet());
  }

  public List<StanziamentiParams> getStanziamentiParams() {
    return stanziamentiParams;
  }

  public boolean isSubsetOfArticles(final List<Article> _articles) {

    final Set<String> articleCodes = _articles.stream()
                                              .map(Article::getCode)
                                              .collect(toSet());
    final Set<String> allArticleCodes = getAllArticleCodes();

    _log.info("Compare articles ...");
    _log.debug(" set 1 from nav - {} ", articleCodes);
    _log.debug(" set 2 from db  - {} ", allArticleCodes);


    final boolean articleCodesAreSuperset = articleCodes.containsAll(allArticleCodes);

    _log.info("... articleCodes are a superset  : {} ", articleCodesAreSuperset);

    if (!articleCodesAreSuperset){
      Set<String> articleNotPreset = new HashSet<>(allArticleCodes);
      articleNotPreset.removeAll(articleCodes);
      _log.warn("codes not present in nav - {}", articleNotPreset);
    }


    return articleCodesAreSuperset;
  }

  public Set<String> getAllArticleCodes() {
    return getAllArticles().stream()
                           .map(Article::getCode)
                           .collect(toSet());
  }

  public boolean isSubsetOfProviderCodes(final Set<String> _providerCodes) {

    _log.info("Compare provider codes ...");
    final Set<String> allProviderCodes = getAllProviderCodes();
    _log.debug(" set from db  - {} ", allProviderCodes);
    _log.debug(" set from nav - {} ", _providerCodes);

    final boolean providerCodesAreSuperset = _providerCodes.containsAll(allProviderCodes);

    _log.info("... providerCodes are a superset : {} ", providerCodesAreSuperset);

    if (!providerCodesAreSuperset){
      Set<String> providersNotPreset = new HashSet<>(allProviderCodes);
      providersNotPreset.removeAll(_providerCodes);
      _log.warn("codes not present in nav - {}", providersNotPreset);
    }

    return providerCodesAreSuperset;
  }

  public boolean isEmpty() {
    return getStanziamentiParams().isEmpty();
  }

  public Map<String, List<StanziamentiParams>> getStanziamentiParamsGropupedByArticleCodes() {
    return stanziamentiParams.stream().collect(Collectors.groupingBy(stanziamentiParams -> stanziamentiParams.getArticle().getCode()));
  }
}
