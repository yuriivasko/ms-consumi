package it.fai.ms.consumi.service.trx;

import static java.util.stream.Collectors.toList;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.api.trx.model.TrxBasicReq;
import it.fai.ms.consumi.api.trx.model.TrxNoOilReq;
import it.fai.ms.consumi.api.trx.model.TrxOilReq;
import it.fai.ms.consumi.api.trx.model.mapper.TrxReqConsumoMapper;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiOil;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.generici.trackycard.TrackyCardGenerico;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.jms.notification_v2.ConsumoNotReprocessableNotification;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.scheduler.jobs.StanziamentoDTOMapper;
import it.fai.ms.consumi.service.StoricoInformationService;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.trx.TrxNoOilProcessor;
import it.fai.ms.consumi.service.processor.consumi.trx.TrxOilProcessor;
import javassist.NotFoundException;

@Service
@Transactional
public class TrxJob {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final static String oil = "OIL";

  private final static String noOil = "NO_OIL";

  private final static int SERVICE_PARTNER_ERROR                   = 101;
  private final static int STANZIAMENTI_PARAM_ERROR                = 102;
  private final static int VALIDATE_AND_PROCESS_ERROR              = 103;
  private final static int VALIDATE_QUANTITY_AMOUNT_NEGATIVE_ERROR = 104;

  private final static String MSG_SERVICE_PARTNER                   = "Non è stato trovato il servizio := [%s] per il fornitore := [ %s ]";
  private final static String MSG_STANZIAMENTI_PARAM                = "Non è stata trovata la configurazione della ParamStanziamenti con i seguenti parametri: ";
  private final static String MSG_VALIDATE_AND_PROCESS_CONSUMO      = "Si è verificato un errore durante la validazione e la generazione del dettaglio stanziamento";
  private final static String MSG_VALIDATE_QUANTITY_AMOUNT_NEGATIVE = "I campi quantity e amount sono <= a 0";

  private final StanziamentiParamsService  stanziamentiParamsService;
  private final ServiceProviderRepository  spService;
  private final TrxReqConsumoMapper        trxReqConsumoMapper;
  private final TrxOilProcessor            trxOilProcessor;
  private final TrxNoOilProcessor          trxNoOildProcessor;
  private final NotificationService        notificationService;
  private final StanziamentiToNavPublisher stanziamentiToNavPublisher;
  private final StoricoInformationService  storicoInformationService;

  private final DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  @Value("${application.actico.isStornoEnabled:}")
  private String isStornoEnabled;

  public TrxJob(StanziamentiParamsService stanziamentiParamsService, ServiceProviderRepository spService,
                TrxReqConsumoMapper trxReqConsumoMapper, TrxOilProcessor trxOilProcessor, TrxNoOilProcessor trxNoOildProcessor,
                NotificationService notificationService, StanziamentiToNavPublisher stanziamentiToNavPublisher,
                StoricoInformationService storicoInformationService, DettaglioStanziamantiRepository dettaglioStanziamentoRepo) {

    this.stanziamentiParamsService = stanziamentiParamsService;
    this.spService = spService;
    this.trxReqConsumoMapper = trxReqConsumoMapper;
    this.trxOilProcessor = trxOilProcessor;
    this.trxNoOildProcessor = trxNoOildProcessor;
    this.notificationService = notificationService;
    this.stanziamentiToNavPublisher = stanziamentiToNavPublisher;
    this.storicoInformationService = storicoInformationService;
    this.dettaglioStanziamentoRepo = dettaglioStanziamentoRepo;
  }

  public ValidationOutcome consumeMessage(TrxOilReq trxOilReq) {
    log.debug("Consuming trxOilReq:=[{}]", trxOilReq);
    ValidationOutcome validationOutcome = new ValidationOutcome();
    if (!validationQuantityAndAmount(trxOilReq)) {
      setErrorQuantityAmountOnValidationOutcome(validationOutcome);
      return validationOutcome;
    }

    @NotNull
    String requestCode = trxOilReq.getRequestCode();

    StanziamentiDetailsType carburanti = StanziamentiDetailsType.CARBURANTI;
    try {
      if (!existsTransaction(requestCode, carburanti)) {

        @NotNull
        String vendorCode = trxOilReq.getVendorCode();
        log.debug("Retrieving ServicePartner by Vendor code: {}", vendorCode);
        final ServicePartner servicePartner = getServicePartnerByVendorCode(vendorCode, trxOilReq.getProductCode());
        log.debug("Service Partner found: {}", servicePartner);

        final StanziamentiParams stanziamentiParams = getStanziamentiParams(trxOilReq, trxOilReq.getUnit(), oil, carburanti);
        log.debug("Params Stanziamenti found: {}", stanziamentiParams);

        TrackyCardCarburantiOil trackyCardCarburantiOil = trxReqConsumoMapper.toCarburante(trxOilReq);

        populateDeviceContractAndVehicleIsNotVirtualDevice(trxOilReq, trackyCardCarburantiOil);

        trackyCardCarburantiOil.setServicePartner(servicePartner);

        final Bucket bucket = new Bucket(UUID.randomUUID()
                                             .toString());
        ProcessingResult result = validateAndProcess(stanziamentiParams, servicePartner, trackyCardCarburantiOil, bucket);
        validationOutcome = result.dettaglioStanziamentoCanBeProcessed() ? new ValidationOutcome() : result.getValidationOutcome();

        invioStanziamentiToNavIfValidationOk(result.getStanziamenti(), validationOutcome);
      } else {
        log.info("Transaction: [{}] already exists on dettaglio stanziamento {}", requestCode, carburanti);
      }
    } catch (Exception e) {
      addExceptionOnValidation(validationOutcome, e);
    }
    return validationOutcome;
  }

  public ValidationOutcome consumeMessage(TrxNoOilReq trxNoOilReq) {
    log.debug("Consuming trxNoOilReq:=[{}]", trxNoOilReq);
    ValidationOutcome validationOutcome = new ValidationOutcome();
    if (!validationQuantityAndAmount(trxNoOilReq)) {
      setErrorQuantityAmountOnValidationOutcome(validationOutcome);
      return validationOutcome;
    }

    @NotNull
    String requestCode = trxNoOilReq.getRequestCode();

    StanziamentiDetailsType generico = StanziamentiDetailsType.GENERICO;
    try {

      if (!existsTransaction(requestCode, generico)) {

        @NotNull
        String vendorCode = trxNoOilReq.getVendorCode();
        log.debug("Retrieving ServicePartner by Vendor code: {}", vendorCode);
        final ServicePartner servicePartner = getServicePartnerByVendorCode(vendorCode, trxNoOilReq.getProductCode());
        log.debug("Service Partner found: {}", servicePartner);

        final StanziamentiParams stanziamentiParams = getStanziamentiParams(trxNoOilReq, trxNoOilReq.getUnit(), noOil, generico);
        log.debug("Params Stanziamenti found: {}", stanziamentiParams);

        TrackyCardGenerico trackyCardGenerico = trxReqConsumoMapper.toGenerico(trxNoOilReq);

        populateDeviceContractAndVehicleIsNotVirtualDevice(trxNoOilReq, trackyCardGenerico);

        trackyCardGenerico.setServicePartner(servicePartner);

        final Bucket bucket = new Bucket(UUID.randomUUID()
                                             .toString());

        ProcessingResult result = validateAndProcess(stanziamentiParams, servicePartner, trackyCardGenerico, bucket);
        validationOutcome = result.dettaglioStanziamentoCanBeProcessed() ? new ValidationOutcome() : result.getValidationOutcome();

        invioStanziamentiToNavIfValidationOk(result.getStanziamenti(), validationOutcome);
      } else {
        log.info("Transaction: [{}] already exists on dettaglio stanziamento {}", requestCode, generico);
      }
    } catch (Exception e) {
      addExceptionOnValidation(validationOutcome, e);
    }
    return validationOutcome;
  }

  private void setErrorQuantityAmountOnValidationOutcome(ValidationOutcome validationOutcome) {
    Message message = new Message(String.valueOf(VALIDATE_QUANTITY_AMOUNT_NEGATIVE_ERROR));
    message.withText(MSG_VALIDATE_QUANTITY_AMOUNT_NEGATIVE);
    validationOutcome.setFatalError(true);
    validationOutcome.addMessage(message);
  }

  private boolean validationQuantityAndAmount(TrxBasicReq trxReq) {
    boolean isValid = false;
    log.debug("IsStornoEnabled: {}", isStornoEnabled);
    if (StringUtils.isNotBlank(isStornoEnabled) && "true".equalsIgnoreCase(isStornoEnabled)) {
      isValid = true;
    } else {
      BigDecimal amount = trxReq.getAmount();
      BigDecimal quantity = trxReq.getQuantity();
      log.info("Amount: {} - Quantity: {}", amount, quantity);
      if (amount != null && quantity != null && amount.doubleValue() > 0 && quantity.doubleValue() > 0) {
        isValid = true;
      }
    }

    return isValid;
  }

  private boolean existsTransaction(@NotNull String requestCode, StanziamentiDetailsType type) {
    boolean existsTransaction = true;
    TrackyCardGlobalIdentifier globalIdentifier = new TrackyCardGlobalIdentifier(requestCode);
    log.debug("Search dettaglio stanziamento {} by global identifier: {}", type, globalIdentifier);
    Set<DettaglioStanziamento> dettaglio = dettaglioStanziamentoRepo.findByGlobalIdentifierId(globalIdentifier, type);
    if (dettaglio == null || dettaglio.isEmpty()) {
      log.debug("Not found transaction by global identifier: {} and detail type: {}", globalIdentifier, type);
      existsTransaction = false;
    }
    return existsTransaction;
  }

  private void addExceptionOnValidation(ValidationOutcome validationOutcome, Exception e) {
    String messageError = e.getMessage();
    log.error("Exception : ", e);

    Message message = null;
    if (messageError.contains("ErrorCode:")) {
      String errorTmp = StringUtils.replace(messageError, "ErrorCode:", "");
      String errorCode = StringUtils.substring(errorTmp, 0, errorTmp.lastIndexOf("Message:"));
      message = new Message(errorCode.trim());
      String msgError = StringUtils.substring(errorTmp, errorTmp.lastIndexOf("Message:"))
                                   .replace("Message:", "");
      message.withText(msgError.trim());
    } else {
      message = new Message(String.valueOf(VALIDATE_AND_PROCESS_ERROR));
      message.withText(MSG_VALIDATE_AND_PROCESS_CONSUMO);
    }

    validationOutcome.addMessage(message);
    validationOutcome.setFatalError(true);
  }

  private void populateDeviceContractAndVehicleIsNotVirtualDevice(TrxBasicReq trxOilReq, Consumo consumo) {
    if (isNotVirtualDevice(trxOilReq)) {
      storicoInformationService.findDevice(trxOilReq.getCardNumber(), TipoDispositivoEnum.TRACKYCARD)
                               .ifPresent(device -> {
                                 log.info("Device found: {}", device);
                                 setDevice(consumo, device);
                                 setContract(consumo, storicoInformationService.findByContract(device.getContractNumber()));

                                 if (isNotVehiclePresent(consumo)) {
                                   storicoInformationService.findVehicle(device, trxOilReq.getSubmissioDateTime()
                                                                                          .toInstant(),
                                                                         "")
                                                            .ifPresent(vehicle -> {

                                                              log.info("  : {}", vehicle);
                                                              setVehicle(consumo, vehicle);
                                                            });
                                 }
                               });
    }

  }

  private void setDevice(Consumo consumo, Device device) {
    if (consumo instanceof TrackyCardCarburantiOil) {
      TrackyCardCarburantiOil oil = (TrackyCardCarburantiOil) consumo;
      oil.setDevice(device);
    } else if (consumo instanceof TrackyCardGenerico) {
      TrackyCardGenerico noOil = (TrackyCardGenerico) consumo;
      noOil.setDevice(device);
    } else {
      log.warn("Consumo {} doesn't instance to {} or {}", TrackyCardCarburantiOil.class, TrackyCardGenerico.class);
    }
  }

  private void setContract(Consumo consumo, Optional<Contract> _contract) {
    if (consumo instanceof TrackyCardCarburantiOil) {
      TrackyCardCarburantiOil oil = (TrackyCardCarburantiOil) consumo;
      oil.setContract(_contract);
    } else if (consumo instanceof TrackyCardGenerico) {
      TrackyCardGenerico noOil = (TrackyCardGenerico) consumo;
      noOil.setContract(_contract);
    } else {
      log.warn("Consumo {} doesn't instance to {} or {}", TrackyCardCarburantiOil.class, TrackyCardGenerico.class);
    }
  }

  private void setVehicle(Consumo consumo, Vehicle vehicle) {
    if (consumo instanceof TrackyCardCarburantiOil) {
      TrackyCardCarburantiOil oil = (TrackyCardCarburantiOil) consumo;
      oil.setVehicle(vehicle);
    } else if (consumo instanceof TrackyCardGenerico) {
      TrackyCardGenerico noOil = (TrackyCardGenerico) consumo;
      noOil.setVehicle(vehicle);
    } else {
      log.warn("Consumo {} doesn't instance to {} or {}", TrackyCardCarburantiOil.class, TrackyCardGenerico.class);
    }
  }

  private boolean isNotVehiclePresent(final Consumo consumo) {
    Vehicle vehicle = null;
    if (consumo instanceof TrackyCardCarburantiOil) {
      vehicle = ((TrackyCardCarburantiOil) consumo).getVehicle();
    } else if (consumo instanceof TrackyCardGenerico) {
      vehicle = ((TrackyCardGenerico) consumo).getVehicle();
    } else {
      log.warn("Consumo {} doesn't instance to {} or {}", TrackyCardCarburantiOil.class, TrackyCardGenerico.class);
    }
    return !isVehiclePresent(vehicle);
  }

  private boolean isVehiclePresent(Vehicle vehicle) {
    return vehicle != null && vehicle.getLicensePlate() != null && StringUtils.isNotBlank(vehicle.getLicensePlate()
                                                                                                 .getLicenseId())
           && StringUtils.isNotBlank(vehicle.getLicensePlate()
                                            .getCountryId());
  }

  private boolean isNotVirtualDevice(TrxBasicReq trxOilReq) {
    return !trxOilReq.getVirtualCard();
  }

  private Optional<Device> getDeviceFromConsumo(final Consumo consumo) {
    if (consumo instanceof DeviceSupplier) {
      return Optional.ofNullable(((DeviceSupplier) consumo).getDevice());
    }
    return Optional.empty();
  }

  private void invioStanziamentiToNavIfValidationOk(List<Stanziamento> stanziamenti, ValidationOutcome validationOutcome) {
    if (isValidToSend(validationOutcome) && arePresentStanziamenti(stanziamenti)) {
      StanziamentoDTOMapper stanziamentoMapper = new StanziamentoDTOMapper();
      List<StanziamentoMessage> stanziamentiMsg = stanziamentoMapper.mapToStanziamentoDTO(stanziamenti);
      log.debug("Send Stanziamenti to NAV: {}", stanziamentiMsg);
      stanziamentiToNavPublisher.publish(stanziamentiMsg);
    } else {
      log.warn("Skip send stanziamenti to NAV, because process result validation is: {}", validationOutcome);
    }
  }

  private boolean isValidToSend(ValidationOutcome validationOutcome) {
    return validationOutcome != null && validationOutcome.isValidationOk();
  }

  private boolean arePresentStanziamenti(List<Stanziamento> stanziamenti) {
    return stanziamenti != null && !stanziamenti.isEmpty();
  }

  private ServicePartner getServicePartnerByVendorCode(@NotNull String vendorCode, @NotNull String productCode) throws NotFoundException {
    Optional<ServiceProvider> serviceProvider = Optional.ofNullable(spService.findByProviderCode(vendorCode));
    return serviceProvider.map(sp -> mapToServicePartner(sp))
                          .orElseThrow(() -> new NotFoundException(String.format("ErrorCode: %s Message: " + MSG_SERVICE_PARTNER,
                                                                                 SERVICE_PARTNER_ERROR, productCode, vendorCode)));
  }

  private ServicePartner mapToServicePartner(ServiceProvider sp) {
    ServicePartner servicePartner = new ServicePartner(String.valueOf(sp.getId()));
    log.debug("Set Format to API, because the transaction has arrived by ACTICO...");
    servicePartner.setFormat(Format.API);
    servicePartner.setPartnerCode(sp.getProviderCode());
    servicePartner.setPartnerName(sp.getProviderName());
    servicePartner.setPriceSoruce(sp.getPriceSource());
    return servicePartner;
  }

  private ProcessingResult validateAndProcess(StanziamentiParams stanziamentiParams, ServicePartner servicePartner, Consumo consumo,
                                              Bucket bucket) {
    ProcessingResult processingResult = null;
    try {
      log.debug("Validating and processing consumo...");
      if (consumo instanceof TrackyCardCarburantiOil) {
        TrackyCardCarburantiOil trackyCardCarburantiOil = (TrackyCardCarburantiOil) consumo;
        log.debug("Consuming record TrackyCardCarburantiOil := [{}] of ServicePartner := [{}]", trackyCardCarburantiOil, servicePartner);
        processingResult = trxOilProcessor.validateAndProcess(stanziamentiParams, trackyCardCarburantiOil, bucket);

      } else if (consumo instanceof TrackyCardGenerico) {
        TrackyCardGenerico trackyCardGenerico = (TrackyCardGenerico) consumo;
        log.debug("Consuming record TrackyCardGenerico := [{}] of ServicePartner := [{}]", trackyCardGenerico, servicePartner);
        processingResult = trxNoOildProcessor.validateAndProcess(stanziamentiParams, trackyCardGenerico, bucket);
      } else {
        throw new IllegalArgumentException("ErrorCode: " + VALIDATE_AND_PROCESS_ERROR + " Message: " + MSG_VALIDATE_AND_PROCESS_CONSUMO);
      }

      log.debug("ProcessingResult := [ ValidationOutcome: {} - Stanziamenti: {}]", processingResult.getValidationOutcome(),
                mapListOfCodeStanziamenti(processingResult));

    } catch (Exception e) {
      ValidationOutcome validationOutcome = new ValidationOutcome();
      addExceptionOnValidation(validationOutcome, e);
      processingResult = new ProcessingResult(consumo, validationOutcome);
      log.error("Problem on consuming Consumo := [{}]", consumo, e);

      // sendErrorNotification(servicePartner, consumo);
      StringBuilder sb = new StringBuilder("Error in processing TrxOilReq / TrxNoOilReq generating Consumo:=[");
      sb.append(consumo);
      sb.append("], with Exception:=[");
      sb.append(e.getClass());
      sb.append(" - ");
      sb.append(e.getMessage());
      notificationService.notify(new ConsumoNotReprocessableNotification(servicePartner.getFormat()
                                                                                       .toString(),
                                                                         "API_CALL", "FCJOB-199", sb.toString()));
    }

    return processingResult;
  }

  private List<String> mapListOfCodeStanziamenti(ProcessingResult processingResult) {
    return ((List<Stanziamento>) processingResult.getStanziamenti()).stream()
                                                                    .map(s -> s.getCode())
                                                                    .collect(toList());
  }

  private StanziamentiParams getStanziamentiParams(TrxBasicReq reqBase, String unit, String routeType,
                                                   StanziamentiDetailsType type) throws NotFoundException {

    @NotNull
    String articleCode = reqBase.getArticleCode();
    @NotNull
    String currency = reqBase.getCurrency();

    @NotNull
    String productCode = reqBase.getProductCode();

    @NotNull
    String vendorCode = reqBase.getVendorCode();

    @NotNull
    @Valid
    @DecimalMin("0")
    @DecimalMax("1")
    BigDecimal vat = reqBase.getVat();
    BigDecimal vatPercentage = vat.multiply(new BigDecimal(100));

    Optional<StanziamentiParams> stanziamentiParams = stanziamentiParamsService.findByTrx(routeType, type, articleCode, vendorCode, unit,
                                                                                          currency, vatPercentage, productCode);

    if (isNotPresentStanziamentiParams(stanziamentiParams)) {
      throw new NotFoundException("ErrorCode: " + STANZIAMENTI_PARAM_ERROR + " Message: " + MSG_STANZIAMENTI_PARAM + "Endpoint: "
                                  + routeType + ", Tipo dettaglio stanziamento: " + type + ", Codice articolo: " + articleCode
                                  + ", Codice fornitore: " + vendorCode + ", unità: " + unit + ", valuta: " + currency + ", IVA: "
                                  + vatPercentage + ", Codice prodotto: " + productCode);
    } else {
      return stanziamentiParams.get();
    }
  }

  private boolean isNotPresentStanziamentiParams(Optional<StanziamentiParams> stanziamentiParams) {
    return (stanziamentiParams.isPresent()) ? false : true;
  }

  // private void sendErrorNotification(ServicePartner servicePartner, Consumo consumo) {
  //
  // StringBuilder sb = new StringBuilder("error processing message");
  // sb.append(consumo);
  //
  // var errorMessage = new HashMap<String, Object>();
  // errorMessage.put("job_name", servicePartner.getFormat()
  // .toString());
  // errorMessage.put("original_message", sb.toString());
  //
  // final String code = "FCJOB-199";
  //
  // log.error("Sending notification code:=[{}], errorMessage:=[{}]", code, errorMessage);
  // notificationService.notify(code, errorMessage);
  // }

}
