package it.fai.ms.consumi.domain.consumi.pedaggi.tunnel;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Optional;

public class FrejusPrenotazioni extends Frejus implements TrustedCustomerSupplier,TrustedVechicleSupplier{

  private Customer customer;

  public FrejusPrenotazioni(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }


}
