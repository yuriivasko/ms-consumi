package it.fai.ms.consumi.scheduler.jobs;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;

public class StanziamentoDTOMapper {

  public StanziamentoDTOMapper() {
  }

  public List<StanziamentoMessage> mapToStanziamentoDTO(final List<Stanziamento> _stanziamenti) {
    return Optional.ofNullable(_stanziamenti)
                   .orElse(Collections.emptyList())
                   .stream()
                   .map(stanziamento -> mapToStanziamentoDTO(stanziamento))
                   .collect(toList());
  }

  public StanziamentoMessage mapToStanziamentoDTO(final Stanziamento _stanziamento) {

    final var allocationMessage = new StanziamentoMessage(_stanziamento.getCode());
    allocationMessage.setAnnoStanziamento(_stanziamento.getYear());
    allocationMessage.setClasseEuroVeicolo(_stanziamento.getVehicleEuroClass());
    allocationMessage.setCodicestanziamentoProvvisorio(_stanziamento.getCodiceStanziamentoProvvisorio());
    allocationMessage.setConguaglio(_stanziamento.isConguaglio());
    allocationMessage.setCosto(_stanziamento.getCosto() != null ? _stanziamento.getCosto()
                                                                             .doubleValue()
                                                              : null);
    allocationMessage.setDataErogazioneServizio(_stanziamento.getDataErogazioneServizio());
    allocationMessage.setDataAcquisizioneFlusso(_stanziamento.getDataAcquisizioneFlusso());
    allocationMessage.getDescriptions()[0] = _stanziamento.getDescriptions()[0];
    allocationMessage.getDescriptions()[1] = _stanziamento.getDescriptions()[1];
    allocationMessage.getDescriptions()[2] = _stanziamento.getDescriptions()[2];
    allocationMessage.setDocumentoDaFornitore(_stanziamento.getDocumentoDaFornitore());
    allocationMessage.setGeneraStanziamento(_stanziamento.getGeneraStanziamento() != null ? _stanziamento.getGeneraStanziamento()
                                                                                                       .intValue()
                                                                                        : -1);
    allocationMessage.setNr(_stanziamento.getTotalAllocationDetails());
    allocationMessage.setNrArticolo(_stanziamento.getArticleCode());
    allocationMessage.setNrCliente(_stanziamento.getNumeroCliente());
    allocationMessage.setNrFornitore(_stanziamento.getNumeroFornitore());
    allocationMessage.setPaese(_stanziamento.getPaese());
    allocationMessage.setPrezzo(_stanziamento.getPrezzo() != null ? _stanziamento.getPrezzo()
                                                                               .doubleValue()
                                                                : null);
    allocationMessage.setQuantita(_stanziamento.getQuantita() != null ? _stanziamento.getQuantita()
                                                                                   .doubleValue()
                                                                    : null);
    allocationMessage.setStatoStanziamento(_stanziamento.getInvoiceType()
                                                        .equals(InvoiceType.P) ? 1 : 2);
    allocationMessage.setTarga(_stanziamento.getVehicleLicensePlate());
    allocationMessage.setNazioneTarga(_stanziamento.getVehicleCountry());
    allocationMessage.setTipoFlusso(_stanziamento.getTipoFlusso() != null ? _stanziamento.getTipoFlusso()
                                                                                       .intValue()
                                                                        : -1);
    allocationMessage.setValuta(_stanziamento.getAmountCurrencyCode());

    return allocationMessage;
  }

}
