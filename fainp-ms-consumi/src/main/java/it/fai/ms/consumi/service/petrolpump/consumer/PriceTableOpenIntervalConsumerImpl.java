package it.fai.ms.consumi.service.petrolpump.consumer;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.navision.PricesByNavDTO.CostoRicavoEnum;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepositoryImpl;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.scheduler.jobs.StanziamentoDTOMapper;
import it.fai.ms.consumi.service.dettaglio_stranziamento.DettaglioStanziamentoCarburanteService;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import javassist.NotFoundException;

@Service
@Transactional
@Qualifier(PriceTableOpenIntervalConsumerImpl.OPEN_INTERVAL_PROCESSOR)
public class PriceTableOpenIntervalConsumerImpl implements PriceTableIntervalConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private List<String>                                 errors = null;
  protected NotificationService                        notificationService;
  private final DettaglioStanziamentoCarburanteService dsCarburanteService;
  private final PriceCalculatedByNavService            priceCalculatedByNavService;
  private final StanziamentoRepositoryImpl             stanziamentoRepository;
  private final StanziamentiToNavPublisher             stanziamentiNavJmsProducer;
  private final StanziamentoEntityMapper               stanziamentoMapper;

  public PriceTableOpenIntervalConsumerImpl(final DettaglioStanziamentoCarburanteService _dsCarburanteService,
                                            NotificationService _notificationService,
                                            final PriceCalculatedByNavService _priceCalculatedByNavService,
                                            final StanziamentoRepositoryImpl _stanziamentoRepository,
                                            final StanziamentiToNavPublisher _stanziamentoNavJmsProducer,
                                            final StanziamentoEntityMapper _stanziamentoMapper) {
    dsCarburanteService = _dsCarburanteService;
    notificationService = _notificationService;
    priceCalculatedByNavService = _priceCalculatedByNavService;
    stanziamentoRepository = _stanziamentoRepository;
    stanziamentiNavJmsProducer = _stanziamentoNavJmsProducer;
    stanziamentoMapper = _stanziamentoMapper;

    errors = new ArrayList<>();
  }

  @Override
  public void managePriceTable(PriceTableDTO priceTable) throws Exception {
    log.debug("Manage priceTable Open Interval....");
    PriceTableFilterDTO priceTableFilter = new PriceTableFilterDTO(priceTable);

    List<DettaglioStanziamentoCarburanteEntity> dsFuelEntity = dsCarburanteService.findToUpdatePriceTableOpen(priceTableFilter);
    Set<DettaglioStanziamentoCarburanteEntity> newList = new HashSet<>();
    BigDecimal referencePrice = priceTable.getReferencePrice();
    CostRevenue costoRicavo = priceTable.getCostRevenueFlag();

    newList = manageDettagliStanziamentoToOpenIntervalPrice(dsFuelEntity, costoRicavo, referencePrice);

    log.info("Managed {} DettagliStanziamentoCarburante of {}", newList.size(), dsFuelEntity.size());
    if (!errors.isEmpty()) {
      String errorMsg = String.format("Anomaly on manage changed price on Dettagli Stanziamento: %s", errors);
      log.error("ERROR: {}", errorMsg);
      Map<String, Object> map = new HashMap<>();
      map.put("error_message", errors);
      notificationService.notify("VARPRICE-002", map);
    }

  }

  private Set<DettaglioStanziamentoCarburanteEntity> manageDettagliStanziamentoToOpenIntervalPrice(List<DettaglioStanziamentoCarburanteEntity> dsFuel,
                                                                                                   CostRevenue costoRicavo,
                                                                                                   BigDecimal referencePrice) {
    Set<DettaglioStanziamentoCarburanteEntity> dsManaged = new HashSet<>();
    for (DettaglioStanziamentoCarburanteEntity ds : dsFuel) {
      try {
        log.trace("Start to manage Dettaglio Stanziamento {} to OPEN Interval price...", ds.getId());

        log.debug("Create twice clone of dettaglio stanziamento");
        var dettaglioToConguaglio = dsCarburanteService.cloneDettaglioStanziamento(ds);
        var dettaglioCloned2 = dsCarburanteService.cloneDettaglioStanziamento(ds);

        log.debug("Update Invoice Typde of Dettaglio Stanziamento to DEFINITIVO");
        ds = updateInvoiceTypeDettaglioToDefinitivo(ds);

        Instant ingestionTime = Instant.now();
        log.debug("New ingestion time: {}", ingestionTime);
        dettaglioToConguaglio.setIngestionTime(ingestionTime);
        dettaglioCloned2.setIngestionTime(ingestionTime);

        manageDettaglioToConguaglio(dettaglioToConguaglio);
        log.debug("Managed Dettaglio stanziamento to CONGUAGLIO");
        manageDettaglioCloned2AndNewStanziamento(dettaglioCloned2, costoRicavo, referencePrice);
        log.debug("Managed new Stanziamento");

        dsManaged.add(ds);
      } catch (Exception e) {
        log.error("Exception on processor open interval: ", e);
        String anomaly = String.format("Exception to manage change price on DettaglioStanziamento %s - %s", ds.getId(), e);
        errors.add(anomaly);
      }
    }

    return dsManaged;
  }

  private void manageDettaglioCloned2AndNewStanziamento(DettaglioStanziamentoCarburanteEntity dettaglio, CostRevenue costoRicavo,
                                                        BigDecimal referencePrice) throws NotFoundException {
    Optional<StanziamentoEntity> stanziamentoOpt = getStanziamentoProvvisorio(dettaglio);
    if (!checkExistsStanziamento(stanziamentoOpt)) {
      addAnomalyNotFoundStanziamento(dettaglio);
    } else {

      StanziamentoEntity stanziamento = stanziamentoOpt.get();

      dettaglio.setPrezzoUnitarioDiRiferimentoNoiva(referencePrice);

      CostoRicavoEnum costoRicavoEnum = getCostoRicavoEnum(costoRicavo);
      StanziamentiParams stanziamentiParams = instanceNewStanziamentiParams(dettaglio, stanziamento);
      BigDecimal calculatedPrice = priceCalculatedByNavService.getCalculatedAmountByNav(dettaglio, stanziamentiParams, referencePrice,
                                                                                        costoRicavoEnum);
      log.info("Calculated price: {}", calculatedPrice);
      setCalculatedPriceByCostRevenue(dettaglio, calculatedPrice, costoRicavo);

      StanziamentoEntity stanziamentoCloned = stanziamentoRepository.cloneAndModify(stanziamento, InvoiceType.P);
      stanziamentoCloned.setCodiceStanziamentoProvvisorio(null);
      setCalculatedPriceByCostRevenue(stanziamentoCloned, calculatedPrice, costoRicavo);

      dettaglio.getStanziamenti()
               .clear();
      dettaglio.getStanziamenti()
               .add(stanziamentoCloned);

      dettaglio = dsCarburanteService.update(dettaglio);
      log.debug("Saved Dettaglio of new Stanziamento: {} - {}", dettaglio.getId(), dettaglio.getGlobalIdentifier());

      stanziamentoCloned.getDettaglioStanziamenti()
                        .clear();
      stanziamentoCloned.getDettaglioStanziamenti()
                        .add(dettaglio);

      StanziamentoEntity stanziamentoCloneUpdated = stanziamentoRepository.updateEntity(stanziamentoCloned);
      log.debug("Saved new Stanziamento: {}", stanziamentoCloneUpdated.getCode());

      sendStanziamentoToNav(stanziamentoCloneUpdated);
    }
  }

  private StanziamentiParams instanceNewStanziamentiParams(DettaglioStanziamentoCarburanteEntity dettaglio,
                                                           StanziamentoEntity stanziamento) {
    String codiceFornitoreNav = dettaglio.getCodiceFornitoreNav();
    Supplier supplier = new Supplier((StringUtils.isNotBlank(codiceFornitoreNav)) ? codiceFornitoreNav : "FAI");
    StanziamentiParams stanziamentiParams = new StanziamentiParams(supplier);

    String articleCode = stanziamento.getArticleCode();
    stanziamentiParams.setArticle(new Article(articleCode));
    return stanziamentiParams;
  }

  private void setCalculatedPriceByCostRevenue(StanziamentoEntity stanziamento, BigDecimal calculatedPrice, CostRevenue costoRicavo) {

    if (costoRicavo.equals(CostRevenue.COST)) {
      stanziamento.setCosto(calculatedPrice);
    } else {
      stanziamento.setPrezzo(calculatedPrice);
    }
  }

  private void setCalculatedPriceByCostRevenue(DettaglioStanziamentoCarburanteEntity dettaglio, BigDecimal calculatedPrice,
                                               CostRevenue costoRicavo) {
    if (costoRicavo.equals(CostRevenue.COST)) {
      dettaglio.setCostoUnitarioCalcolatoNoiva(calculatedPrice);
    } else {
      dettaglio.setPrezzoUnitarioCalcolatoNoiva(calculatedPrice);
    }
  }

  private CostoRicavoEnum getCostoRicavoEnum(CostRevenue costoRicavo) {
    if (!costoRicavo.equals(CostRevenue.COST) && !costoRicavo.equals(CostRevenue.REVENUE)) {
      throw new IllegalArgumentException("The type of " + CostRevenue.class.getName() + " is not manage to this process.");
    }

    CostoRicavoEnum costoRicavoEnum = CostoRicavoEnum.NUMBER_0;
    if (costoRicavo.equals(CostRevenue.REVENUE)) {
      costoRicavoEnum = CostoRicavoEnum.NUMBER_1;
    }
    return costoRicavoEnum;
  }

  private void manageDettaglioToConguaglio(DettaglioStanziamentoCarburanteEntity dettConguaglio) throws NotFoundException {
    Optional<StanziamentoEntity> stanziamentoOpt = getStanziamentoProvvisorio(dettConguaglio);
    if (!checkExistsStanziamento(stanziamentoOpt)) {
      addAnomalyNotFoundStanziamento(dettConguaglio);
    } else {
      dettConguaglio.setPrezzoUnitarioCalcolatoNoiva(getNegativeNumber(dettConguaglio.getPrezzoUnitarioCalcolatoNoiva()));
      dettConguaglio.setCostoUnitarioCalcolatoNoiva(getNegativeNumber(dettConguaglio.getCostoUnitarioCalcolatoNoiva()));
      log.debug("Set storno true on dettaglio: {}", dettConguaglio);
      dettConguaglio.setStorno(true);

      StanziamentoEntity stanziamento = stanziamentoOpt.get();
      StanziamentoEntity stanziamentoConguaglio = stanziamentoRepository.cloneAndModify(stanziamento, InvoiceType.P);

      removeStanziamentiOnDettaglio(dettConguaglio);
      removeDettagliOnStanziamento(stanziamentoConguaglio);

      addDettaglioOnStanziamento(dettConguaglio, stanziamentoConguaglio);
      addStanziamentoOnDettaglio(stanziamentoConguaglio, dettConguaglio);

      stanziamentoConguaglio.setConguaglio(true);
      stanziamentoConguaglio.setPrezzo(getNegativeNumber(stanziamentoConguaglio.getPrezzo()));
      stanziamentoConguaglio.setCosto(getNegativeNumber(stanziamentoConguaglio.getCosto()));

      dettConguaglio = dsCarburanteService.update(dettConguaglio);
      log.debug("Saved Dettaglio to Conguaglio: {} - {}", dettConguaglio.getId(), dettConguaglio.getGlobalIdentifier());

      stanziamentoConguaglio = stanziamentoRepository.updateEntity(stanziamentoConguaglio);
      log.debug("Saved StanziamentoConguaglio: {}", stanziamentoConguaglio.getCode());

      sendStanziamentoToNav(stanziamentoConguaglio);
    }
  }

  private void sendStanziamentoToNav(StanziamentoEntity stanziamento) {
    log.info("Send Stanziamento {} to NAV...", (stanziamento != null) ? stanziamento.getCode() : null);
    if (stanziamento != null) {
      StanziamentoDTOMapper stanziamentoDTOMapper = new StanziamentoDTOMapper();
      List<StanziamentoMessage> stanziamentiMessage = stanziamentoDTOMapper.mapToStanziamentoDTO(Arrays.asList(stanziamentoMapper.toDomain(stanziamento)));
      stanziamentiNavJmsProducer.publish(stanziamentiMessage);
    } else {
      String anomaly = "Stanziamento to send at NAV is NULL";
      log.warn("ANOMALY - {}", anomaly);
      errors.add(anomaly);
    }
  }

  private void addStanziamentoOnDettaglio(StanziamentoEntity stanziamento, DettaglioStanziamentoCarburanteEntity dettaglio) {
    if (dettaglio != null) {
      dettaglio.getStanziamenti()
               .add(stanziamento);
    }
  }

  private void addDettaglioOnStanziamento(DettaglioStanziamentoCarburanteEntity dettaglio, StanziamentoEntity stanziamento) {
    if (stanziamento != null) {
      stanziamento.getDettaglioStanziamenti()
                  .add(dettaglio);
    }
  }

  private void removeDettagliOnStanziamento(StanziamentoEntity stanziamento) {
    if (stanziamento != null) {
      stanziamento.getDettaglioStanziamenti()
                  .clear();
    }
  }

  private void removeStanziamentiOnDettaglio(DettaglioStanziamentoCarburanteEntity dettaglio) {
    if (dettaglio != null) {
      dettaglio.getStanziamenti()
               .clear();
    }
  }

  private BigDecimal getNegativeNumber(BigDecimal val) {
    BigDecimal newVal = null;
    if (val != null) {
      newVal = val.multiply(new BigDecimal(-1));
    }
    return newVal;
  }

  private DettaglioStanziamentoCarburanteEntity updateInvoiceTypeDettaglioToDefinitivo(DettaglioStanziamentoCarburanteEntity ds) {
    log.info("Dettaglio to Update: {}", ds);
    ds.setTipoConsumo(InvoiceType.D);
    ds.setStorno(false);
    dsCarburanteService.update(ds);
    return ds;
  }

  private Optional<StanziamentoEntity> getStanziamentoProvvisorio(DettaglioStanziamentoCarburanteEntity ds) {
    Optional<StanziamentoEntity> optStanziamento = ds.getStanziamenti()
                                                     .stream()
                                                     .filter(s -> s.getStatoStanziamento()
                                                                   .equals(InvoiceType.P))
                                                     .findFirst();
    return optStanziamento;
  }

  private boolean checkExistsStanziamento(Optional<StanziamentoEntity> stanziamentoOpt) {
    if (stanziamentoOpt.isPresent())
      return true;
    return false;
  }

  private void addAnomalyNotFoundStanziamento(DettaglioStanziamentoCarburanteEntity ds) {
    String anomaly = String.format("Not found Stanziamento Provvisorio related on Dettaglio: %s", (ds.getId() != null) ? ds.getId() : null);

    log.warn("ANOMALY - {}", anomaly);
    errors.add(anomaly);
  }

}
