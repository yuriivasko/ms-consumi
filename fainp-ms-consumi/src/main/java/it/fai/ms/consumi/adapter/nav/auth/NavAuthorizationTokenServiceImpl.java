package it.fai.ms.consumi.adapter.nav.auth;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import it.fai.ms.consumi.client.AuthorizationApiService;
import it.fai.ms.consumi.config.ApplicationProperties;

@Service
public class NavAuthorizationTokenServiceImpl implements NavAuthorizationTokenService {

  private final ApplicationProperties   applicationProperties;
  private final AuthorizationApiService authorizationApi;

  @Inject
  public NavAuthorizationTokenServiceImpl(final ApplicationProperties _applicationProperties,
                                          final AuthorizationApiService _authorizationApi) {
    applicationProperties = _applicationProperties;
    authorizationApi = _authorizationApi;
  }

  @Override
  public String getAuthorizationToken() {
    return authorizationApi.getAuthorizationToken(applicationProperties.getRestclient()
                                                                       .getGrant_type(),
                                                  applicationProperties.getRestclient()
                                                                       .getUsername(),
                                                  applicationProperties.getRestclient()
                                                                       .getPassword());
  }

}
