package it.fai.ms.consumi.scheduler.jobs.flussi;

import java.time.Instant;

import it.fai.ms.consumi.domain.consumi.ServicePartner;

public interface FlussoConsumoJob {

  String getSource();

  String process(String _filename, long _startTime, Instant _ingestionTime, ServicePartner servicePartner);

}
