package it.fai.ms.consumi.web.rest;

import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsArticleDTO;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(StanziamentiParamsResource.BASE_PATH)
public class StanziamentiParamsResource {

  private final Logger log = LoggerFactory.getLogger(getClass());

  static final String BASE_PATH = "/api";
  static final String STANZIAMENTI_PARAMS = "/stanziamenti_params_articles";

  static final String API_STANZIAMENTI_PARAMS = BASE_PATH + STANZIAMENTI_PARAMS;

  private final StanziamentiParamsRepository stanziamentiParamsRepository;

  public StanziamentiParamsResource(StanziamentiParamsRepository stanziamentiParamsRepository) {
    this.stanziamentiParamsRepository = stanziamentiParamsRepository;
  }

  @GetMapping(STANZIAMENTI_PARAMS)
  public ResponseEntity<List<StanziamentiParamsArticleDTO>> findAllStanziamentiParamsArticles() {
    log.debug("Called forceSendTransaction");
    List<StanziamentiParamsArticleDTO> articles = stanziamentiParamsRepository.findAllStanziamentiParamsArticles();
    log.debug("Retrieved [{}] articles", articles.size());
    return ResponseEntity.ok(articles);

  }

}
