package it.fai.ms.consumi.client;

import org.springframework.stereotype.Service;

@Service
public class AuthorizationApiService {

  private final AuthorizationApiClient authorizationApiClient;

  public AuthorizationApiService(AuthorizationApiClient authorizationApiClient) {
    this.authorizationApiClient = authorizationApiClient;
  }

  public AuthorizationApi.LoginResponse login(String body) {
    return authorizationApiClient.login(body);
  }

  public String getAuthorizationToken(String grant_type, String username, String password) {
    return authorizationApiClient.getAuthorizationToken(grant_type, username, password);
  }
}