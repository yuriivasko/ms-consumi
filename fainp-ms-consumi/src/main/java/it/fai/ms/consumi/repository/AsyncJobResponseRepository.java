package it.fai.ms.consumi.repository;

import it.fai.ms.consumi.domain.AsyncJobResponse;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AsyncJobResponse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AsyncJobResponseRepository extends JpaRepository<AsyncJobResponse, Long> {

}
