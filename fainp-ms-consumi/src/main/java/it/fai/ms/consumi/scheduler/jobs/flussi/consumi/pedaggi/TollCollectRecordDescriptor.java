package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;


import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElcitDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TollCollectRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;


@Component
public class TollCollectRecordDescriptor
implements RecordDescriptor<TollCollectRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(TollCollectRecord.class);

  private final List<String> HEADER = Arrays.asList(
                                                    "invoiceRecipient",
                                                    "recipientCity", 
                                                    "statementLevel", 
                                                    "EDIPartnerCode", 
                                                    "RAGCode", 
                                                    "invoiceNumber", 
                                                    "invoiceDate", 
                                                    "dueDate", 
                                                    "positionNumber", 
                                                    "subPositionNumber", 
                                                    "processingDate", 
                                                    "supplierName",
                                                    "supplierAddress1", 
                                                    "supplierAddress2", 
                                                    "supplierUIN", 
                                                    "supplierTaxNumber", 
                                                    "cardNumber", 
                                                    "supplierNumber",
                                                    "registrationCountry",
                                                    "registrationNumber",
                                                    "serviceDescription",
                                                    "branch",
                                                    "supplierServiceDate",
                                                    "supplierCostCenter",
                                                    "supplierInvoiceNumber",
                                                    "supplierInvoiceDate",
                                                    "supplierStatementDate",
                                                    "saleCurrencySC",
                                                    "netAmountSC",
                                                    "vatPercentage",
                                                    "vatPercentageSC",
                                                    "grossAmountSC",
                                                    "currencyRate",
                                                    "statementCurrencyIC",
                                                    "netAmountIC",
                                                    "vatIC"
      );

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public String getHeaderBegin() {
    return "Invoice Recipient;Recipient City;Statement Level;EDI Partner Code;RAG Code;Invoice Number;Invoice Date;Due Date;Position Number;Subposition Number;Processing Date;Supplier Name;Supplier Address 1;Supplier Address 2;Supplier UIN;Supplier Tax Number;Card Number;Supplier Number;Registration Country;Registration Number;Service Description;Branch;Supplier Service Date;Supplier Cost Center;Supplier Invoice Number;Supplier Invoice Date;Supplier Statement Date;Sale Currency (SC);Net Amount (SC);VAT Percentage;VAT Percentage (SC);Gross Amount (SC);Currency Rate;Statement Currency (IC);Net Amount (IC);VAT (IC)";
  }

  @Override
  public String getFooterCodeBegin() {
    return "§ NO HEADER FILE §";
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  @Override
  public String extractRecordCode(String _data) {
    return "";
  }

  @Override
  public TollCollectRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {
    TollCollectRecord tollCollectRecord = new TollCollectRecord(fileName, rowNumber);


    if (rowNumber == 0) {
      SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
      SimpleDateFormat sdfTime = new SimpleDateFormat("HHmmss");
      tollCollectRecord.setRecordCode("DEFAULT");
      tollCollectRecord.setTransactionDetail("");
      Date now = getNow();
      tollCollectRecord.setCreationDateInFile(sdfDate.format(now));
      tollCollectRecord.setCreationTimeInFile(sdfTime.format(now));
      return tollCollectRecord;
    }


    tollCollectRecord.setTransactionDetail("");
    tollCollectRecord.setFromCsvRecord(_data, HEADER);
    tollCollectRecord.setRecordCode("DEFAULT");

    return tollCollectRecord;


  }

  protected Date getNow() {
    return new Date();
  }

  public void checkConfiguration(){
    log.trace("No need to check configuration...");
  }
}
