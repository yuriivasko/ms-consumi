package it.fai.ms.consumi.web.rest.errors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PayloadError implements Serializable {

	private final static long serialVersionUID = 4531478411725473551L;

	private final Logger log = LoggerFactory.getLogger(PayloadError.class);

	@JsonProperty("errors")
	private List<Error> errors = new ArrayList<Error>();

	public PayloadError() {}

	public static PayloadError builder() {
		return new PayloadError();
	}

	public PayloadError add(Integer code, String internalMessage) {
		errors.add(new Error(code, internalMessage));
		return this;
	}

	public PayloadError add(Errno errno) {
		return add(errno.code(), errno.internalMessage());
	}

	public PayloadError clear() {
		errors.clear();
		return this;
	}

	public Integer getCode(int index) {
		return errors.get(index).getCode();
	}

	public String getInternalMessage(int index) {
		return errors.get(index).getInternalMessage();
	}

	public List<Error> getErrors() {
		return errors;
	}

	public boolean containsError(Errno errno){
	    return errors.stream().anyMatch(error -> error.getCode().equals(errno.code()));
    }

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	public static Error buildError(Integer code, String internalMessage) {
	  return new Error(code, internalMessage);
  }

  public static Error buildError(Errno errno) {
    return new Error(errno.code(), errno.internalMessage());
  }

	public static class Error implements Serializable {

		private static final long serialVersionUID = 4532478431725463558L;

		private final Logger log = LoggerFactory.getLogger(Error.class);

		@JsonProperty("code")
		private Integer code = 0;

		@JsonProperty("internalMessage")
		private String internalMessage = "";

		public Error() {}

		public Error(Integer code, String internalMessage) {
			super();
			this.internalMessage = internalMessage;
			this.code = code;
		}

		public String getInternalMessage() {
			return internalMessage;
		}

		public void setInternalMessage(String internalMessage) {
			this.internalMessage = internalMessage;
		}

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

	}

}
