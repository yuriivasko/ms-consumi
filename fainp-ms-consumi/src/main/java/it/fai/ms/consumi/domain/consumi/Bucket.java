package it.fai.ms.consumi.domain.consumi;

import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

public class Bucket {

  private Instant      date;
  private String       fileName;
  private final String id;
  private boolean RUN_IN_PARALLEL;

  public static Bucket createBucket(String _filename, Instant _ingestionTime, boolean RUN_IN_PARALLEL){
    final Bucket bucket = new Bucket(UUID.randomUUID().toString());
    bucket.setDate(_ingestionTime);
    bucket.setFileName(_filename);
    bucket.RUN_IN_PARALLEL = RUN_IN_PARALLEL;
    return bucket;
  }

  public Bucket(final String _id) {
    id = Objects.requireNonNull(_id, "Parameter id is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Bucket obj = getClass().cast(_obj);
      res = Objects.equals(obj.id, id) && Objects.equals(obj.date, date) && Objects.equals(obj.fileName, fileName);
    }
    return res;
  }

  public Instant getDate() {
    return date;
  }

  public String getFileName() {
    return fileName;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, date, fileName);
  }

  public void setDate(final Instant _date) {
    date = _date;
  }

  public void setFileName(final String _fileName) {
    fileName = _fileName;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("id=")
                              .append(id)
                              .append(",=")
                              .append(fileName)
                              .append(",")
                              .append(date)
                              .append("]")
                              .toString();
  }

  public boolean isRUNNING_IN_PARALLEL() {
    return RUN_IN_PARALLEL;
  }
}
