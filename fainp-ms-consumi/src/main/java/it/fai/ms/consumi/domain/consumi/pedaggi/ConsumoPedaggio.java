package it.fai.ms.consumi.domain.consumi.pedaggi;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.RouteName;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

public abstract class ConsumoPedaggio
  extends Consumo
  implements VehicleAssociatedWithDeviceSupplier, RouteName {

  private Boolean        processed;
  private Device         device;
  private TollPoint      tollPointEntry;
  private TollPoint      tollPointExit;
  private Vehicle        vehicle;
  private BigDecimal     totalAmount;
  private String         splitNumber;
  private String         compensationNumber;
  private String         tspExitCountryCode;
  private String         tspExitNumber;
  private String         tspRespCountryCode;
  private String         tspRespNumber;
  private String         tollCharger;
  private String         tollGate;
  private String         exitTransitDateTime;
  private Instant        entryTime;
  private Instant        entryDate;
  private String         laneId;
  private String         networkCode;
  private String         additionalInfo;
  private String         roadType;
  private String         route;
  private String         routeName;
  private BigDecimal     imponibileAdr;
  private ESE_TOT_STATUS eseTotStatus;

  public ConsumoPedaggio(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final ConsumoPedaggio obj = getClass().cast(_obj);
      res = super.equals(_obj) && Objects.equals(obj.device, device) && Objects.equals(obj.vehicle, vehicle)
          && Objects.equals(obj.tollPointEntry, tollPointEntry) && Objects.equals(obj.tollPointExit, tollPointExit);
    }
    return res;
  }

  @Override
  public Device getDevice() {
    return device;
  }

  public TollPoint getTollPointEntry() {
    return tollPointEntry;
  }

  public TollPoint getTollPointExit() {
    return tollPointExit;
  }

  @Override
  public Vehicle getVehicle() {
    return vehicle;
  }

  public Boolean getProcessed() {
    return processed;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), device, tollPointEntry, tollPointExit, vehicle);
  }

  public void setDevice(final Device _device) {
    device = _device;
  }

  public void setTollPointEntry(final TollPoint _tollPointEntry) {
    tollPointEntry = _tollPointEntry;
  }

  public void setTollPointExit(final TollPoint _tollPointExit) {
    tollPointExit = _tollPointExit;
  }

  public void setVehicle(final Vehicle _vehicle) {
    vehicle = _vehicle;
  }

  public void setProcessed(Boolean processed) {
    this.processed = processed;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(final BigDecimal _totalAmount) {
    totalAmount = _totalAmount;
  }

  @Override
  public String getRouteName() {
    var descriptionEntryPoint = "";
    var descriptionExitPoint = "";
    if (tollPointEntry != null && tollPointEntry.getGlobalGate() != null)
      descriptionEntryPoint = StringUtils.defaultString(tollPointEntry.getGlobalGate().getDescription());

    if (tollPointExit != null && tollPointExit.getGlobalGate() != null)
      descriptionExitPoint = StringUtils.defaultString(tollPointExit.getGlobalGate() .getDescription());

    return descriptionEntryPoint + ((!descriptionEntryPoint.isEmpty() && !descriptionExitPoint.isEmpty())?" - ":"") + descriptionExitPoint;
  }

  public String getSplitNumber() {
    return splitNumber;
  }

  public void setSplitNumber(String splitNumber) {
    this.splitNumber = splitNumber;
  }

  public String getCompensationNumber() {
    return compensationNumber;
  }

  public void setCompensationNumber(String compensationNumber) {
    this.compensationNumber = compensationNumber;
  }

  public String getTspExitCountryCode() {
    return tspExitCountryCode;
  }

  public void setTspExitCountryCode(String tspExitCountryCode) {
    this.tspExitCountryCode = tspExitCountryCode;
  }

  public String getTspExitNumber() {
    return tspExitNumber;
  }

  public void setTspExitNumber(String tspExitNumber) {
    this.tspExitNumber = tspExitNumber;
  }

  public String getTspRespCountryCode() {
    return tspRespCountryCode;
  }

  public void setTspRespCountryCode(String tspRespCountryCode) {
    this.tspRespCountryCode = tspRespCountryCode;
  }

  public String getTspRespNumber() {
    return tspRespNumber;
  }

  public void setTspRespNumber(String tspRespNumber) {
    this.tspRespNumber = tspRespNumber;
  }

  public String getTollCharger() {
    return tollCharger;
  }

  public void setTollCharger(String tollCharger) {
    this.tollCharger = tollCharger;
  }

  public String getTollGate() {
    return tollGate;
  }

  public void setTollGate(String tollGate) {
    this.tollGate = tollGate;
  }

  public String getExitTransitDateTime() {
    return exitTransitDateTime;
  }

  public void setExitTransitDateTime(String exitTransitDateTime) {
    this.exitTransitDateTime = exitTransitDateTime;
  }

  public Instant getEntryTime() {
    return entryTime;
  }

  public void setEntryTime(Instant entryTime) {
    this.entryTime = entryTime;
  }

  public Instant getEntryDate() {
    return entryDate;
  }

  public void setEntryDate(Instant entryDate) {
    this.entryDate = entryDate;
  }

  public String getLaneId() {
    return laneId;
  }

  public void setLaneId(String laneId) {
    this.laneId = laneId;
  }

  public String getNetworkCode() {
    return networkCode;
  }

  public void setNetworkCode(String networkCode) {
    this.networkCode = networkCode;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getRoadType() {
    return roadType;
  }

  public void setRoadType(String roadType) {
    this.roadType = roadType;
  }

  public String getRoute() {
    return route;
  }

  public void setRoute(String route) {
    this.route = route;
  }

  public BigDecimal getImponibileAdr() {
    return imponibileAdr;
  }

  public void setImponibileAdr(BigDecimal imponibileAdr) {
    this.imponibileAdr = imponibileAdr;
  }

  public ESE_TOT_STATUS getEseTotStatus() {
    return eseTotStatus;
  }

  public void setEseTotStatus(ESE_TOT_STATUS eseTotStatus) {
    this.eseTotStatus = eseTotStatus;
  }

  @Override
  public Instant getDate() {
    if (this.getTollPointExit()!=null) {
      if (this.getTollPointExit().getTime() != null) {
        return this.getTollPointExit().getTime();
      } else {
        if (this.getSource().getAcquisitionDate()!=null)
          return this.getSource().getAcquisitionDate();
      }
    }
    return null;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ConsumoPedaggio [processed=");
    builder.append(processed);
    builder.append(", device=");
    builder.append(device);
    builder.append(", tollPointEntry=");
    builder.append(tollPointEntry);
    builder.append(", tollPointExit=");
    builder.append(tollPointExit);
    builder.append(", vehicle=");
    builder.append(vehicle);
    builder.append(", totalAmount=");
    builder.append(totalAmount);
    builder.append(", splitNumber=");
    builder.append(splitNumber);
    builder.append(", compensationNumber=");
    builder.append(compensationNumber);
    builder.append(", tspExitCountryCode=");
    builder.append(tspExitCountryCode);
    builder.append(", tspExitNumber=");
    builder.append(tspExitNumber);
    builder.append(", tspRespCountryCode=");
    builder.append(tspRespCountryCode);
    builder.append(", tspRespNumber=");
    builder.append(tspRespNumber);
    builder.append(", tollCharger=");
    builder.append(tollCharger);
    builder.append(", tollGate=");
    builder.append(tollGate);
    builder.append(", exitTransitDateTime=");
    builder.append(exitTransitDateTime);
    builder.append(", entryTime=");
    builder.append(entryTime);
    builder.append(", entryDate=");
    builder.append(entryDate);
    builder.append(", laneId=");
    builder.append(laneId);
    builder.append(", networkCode=");
    builder.append(networkCode);
    builder.append(", additionalInfo=");
    builder.append(additionalInfo);
    builder.append(", roadType=");
    builder.append(roadType);
    builder.append(", route=");
    builder.append(route);
    builder.append(", routeName=");
    builder.append(routeName);
    builder.append(", imponibileAdr=");
    builder.append(imponibileAdr);
    builder.append(", eseTotStatus=");
    builder.append(eseTotStatus);
    builder.append(", toString()=");
    builder.append(super.toString());
    builder.append("]");
    return builder.toString();
  }

  @Override
  public Optional<String> getSupplierVehicleFareClass() {
    return Optional.ofNullable(getVehicle()).map(vehicle -> vehicle.getFareClass());
  }

  
}
