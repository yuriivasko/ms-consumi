package it.fai.ms.consumi.domain.consumi;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.service.validator.ConsumoNoBlockingData;

import org.apache.commons.lang3.StringUtils;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

public abstract class Consumo
  implements DateSupplier, ContractSupplier, ParamStanziamentoMatchingParamsSupplier {

  private Amount                 amount;
  @Valid
  private Optional<Contract>     contract = Optional.empty();
  private final GlobalIdentifier globalIdentifier;
  private final CommonRecord record;
  private String                 navArticlesGroup;
  private String                 navSupplierCode;
  private String                 supplierInvoiceDocument;
  private final String           recordCode;
  private String                 region;
  private final Source           source;
  private Transaction            transaction;
  private ServicePartner         servicePartner;
  private ConsumoNoBlockingData consumoNoBlockingData;

  public Consumo(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    this.source = Objects.requireNonNull(_source, "Parameter source is mandatory");
    this.recordCode = Optional.ofNullable(StringUtils.trimToNull(_recordCode)).orElse("DEFAULT");
    this.globalIdentifier = _optionalGlobalIdentifier.orElseGet(() -> makeInternalGlobalIdentifier());
    this.record = record;
  }

  public Amount getAmount() {
    return amount;
  }

  public String getNavSupplierCode() {
    return navSupplierCode;
  }

  @Override
  public Optional<Contract> getContract() {
    return contract;
  }

  public GlobalIdentifier getGlobalIdentifier() {
    return globalIdentifier;
  }

  public String getGroupArticlesNav() {
    return navArticlesGroup;
  }

  public abstract InvoiceType getInvoiceType();

  public String getPartnerCode() {
    return servicePartner != null ? servicePartner.getPartnerCode() : null;
  }

  public String getRecordCode() {
    return recordCode;
  }

  @Override
  public String getRecordCodeToLookInParamStanz() {
    return recordCode;
  }

  public String getRegion() {
    return region;
  }

  public Source getSource() {
    return source;
  }

  @Override
  public String getSourceType() {
    return (source != null ? source.getType() : null);
  }

  public Transaction getTransaction() {
    return transaction;
  }

  @Override
  public String getTransactionDetailCode() {
    return (this.transaction != null ? this.transaction.getDetailCode() : null);
  }

  @Override
  public Optional<BigDecimal> getVatRate() {
    return getAmount() !=null ? getAmount().getVatRate():Optional.empty();
  }

  public void setAmount(final Amount _amount) {
    this.amount = _amount;
  }

  public void setNavSupplierCode(final String _navSupplierCode) {
    navSupplierCode = _navSupplierCode;
  }

  public void setContract(final Optional<Contract> _contract) {
    contract = _contract;
  }

  public void setGroupArticlesNav(final String _navArticlesGroup) {
    navArticlesGroup = _navArticlesGroup;
  }

  public void setRegion(final String _region) {
    region = _region;
  }

  public void setTransaction(final Transaction _transaction) {
    transaction = _transaction;
  }

  @Override
  public ServicePartner getServicePartner() {
    return servicePartner;
  }

  public void setServicePartner(final ServicePartner _servicePartner) {
    servicePartner = _servicePartner;
  }


  public String getSupplierInvoiceDocument() {
    return supplierInvoiceDocument;
  }

  public void setSupplierInvoiceDocument(String supplierInvoiceDocument) {
    this.supplierInvoiceDocument = supplierInvoiceDocument;
  }

  public String getJobName(){
    return Optional.ofNullable(this.getServicePartner())
      .map(s -> s.getFormat())
      .map(f-> f.name())
    .orElse("job handling" + this.getClass().getSimpleName());
  }

  public CommonRecord getRecord() {
    return record;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getGlobalIdentifier(), amount, contract, getDate(), recordCode, source, transaction, region, servicePartner, record);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Consumo obj = getClass().cast(_obj);
      res = Objects.equals(obj.getGlobalIdentifier(), getGlobalIdentifier())
        && Objects.equals(obj.amount, amount)
        && Objects.equals(obj.contract, contract)
        && Objects.equals(obj.getDate(), this.getDate())
        && Objects.equals(obj.recordCode, recordCode)
        && Objects.equals(obj.source, source)
        && Objects.equals(obj.transaction, transaction)
        && Objects.equals(obj.region, region)
        && Objects.equals(obj.servicePartner, servicePartner)
        && Objects.equals(obj.record, record);
    }
    return res;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Consumo [");
    builder.append("amount=");
    builder.append(amount);
    builder.append(", contract=");
    builder.append(contract);
    builder.append(", date=");
    builder.append(getDate());
    builder.append(", globalIdentifier=");
    builder.append(globalIdentifier);
    builder.append(", navArticlesGroup=");
    builder.append(navArticlesGroup);
    builder.append(", supplierCode=");
    builder.append(navSupplierCode);
    builder.append(", supplierInvoiceDocument=");
    builder.append(supplierInvoiceDocument);
    builder.append(", recordCode=");
    builder.append(recordCode);
    builder.append(", region=");
    builder.append(region);
    builder.append(", source=");
    builder.append(source);
    builder.append(", transaction=");
    builder.append(transaction);
    builder.append(", servicePartner=");
    builder.append(servicePartner);
    builder.append("]");
    return builder.toString();
  }

  protected abstract GlobalIdentifier makeInternalGlobalIdentifier();

  @Override
  public Optional<String> getSupplierVehicleFareClass() {
    return Optional.empty();
  }



  public ConsumoNoBlockingData getConsumoNoBlockingData() {
    return consumoNoBlockingData;
  }



  public void setConsumoNoBlockingData(ConsumoNoBlockingData consumoNoBlockingData) {
    this.consumoNoBlockingData = consumoNoBlockingData;
  }
}
