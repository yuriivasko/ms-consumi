package it.fai.ms.consumi.web.rest;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsArticleDTO;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.service.dettaglio_stranziamento.DettaglioStanziamentoCarburanteService;
import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteJmsProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(DettaglioStanziamentiResourceExt.BASE_PATH)
public class DettaglioStanziamentiResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  static final String BASE_PATH = "/api";
  static final String DETTAGLIO_STANZIAMENTO = "/dettaglio_stanziamento";
  static final String FORCE_PROPAGATE_TRANSACTION = "/force_propagate_transaction";

  static final String API_FORCE_PROPAGATE_TRANSACTION = BASE_PATH + DETTAGLIO_STANZIAMENTO + FORCE_PROPAGATE_TRANSACTION;

  private final DettaglioStanziamantiRepository dettaglioStaziamentoRepository;
  private final DettaglioCarburanteJmsProducer dettaglioCarburanteJmsProducer;

  public DettaglioStanziamentiResourceExt(DettaglioStanziamantiRepository dettaglioStaziamentoRepository, DettaglioCarburanteJmsProducer dettaglioCarburanteJmsProducer) {
    this.dettaglioStaziamentoRepository = dettaglioStaziamentoRepository;
    this.dettaglioCarburanteJmsProducer = dettaglioCarburanteJmsProducer;
  }

  @GetMapping(DETTAGLIO_STANZIAMENTO + FORCE_PROPAGATE_TRANSACTION + "/{detailsType}" + "/{transactionCode}")
  public ResponseEntity<String> forceSendTransaction(@PathVariable(name = "detailsType", required = true) StanziamentiDetailsType detailsType, @PathVariable(name = "transactionCode") String transactionCode) {
    try {
      String result = forcePropagationByCode(detailsType, transactionCode);
      return ResponseEntity.ok(result);
    } catch (Exception e){
      return ResponseEntity.unprocessableEntity().body(e.getMessage());
    }
  }

  private String forcePropagationByCode(StanziamentiDetailsType detailsType, String transactionCode) {
    switch (detailsType) {
      case CARBURANTI:
        try {
          DettaglioStanziamentoCarburanteEntity entity = (DettaglioStanziamentoCarburanteEntity) dettaglioStaziamentoRepository.findLastDettaglioCarburantiByGlobalIdentifierExcludingStorni(transactionCode).get();
          log.debug("Called forceSendTransaction");
          dettaglioCarburanteJmsProducer.sendDetailFuelMessage(entity);
          return "Transaction with transactionCode:=[" + transactionCode + "] enqueued for propagation";
        } catch (Exception e) {
          log.error("Error trying to propagate transaction with transactionCode:=[{}]!", transactionCode, e);
          throw new RuntimeException("\"Error trying to propagate transaction with transactionCode:=[\" + transactionCode + \"]", e);
        }
      default:
        log.error("Called forcePropagationByCode with wrong StanziamentiDetailsType! TransactionCode:=[{}]!", detailsType, transactionCode);
        throw new IllegalArgumentException("Operation not supported for detailType:=[" + detailsType + "]");

    }
  }
}
