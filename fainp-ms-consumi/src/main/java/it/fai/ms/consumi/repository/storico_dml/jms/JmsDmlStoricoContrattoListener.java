package it.fai.ms.consumi.repository.storico_dml.jms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.efservice.dto.ContrattoDMLDTO;
import it.fai.ms.common.dml.mappable.AbstractMappableDmlListener;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.consumi.repository.storico_dml.StoricoContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.jms.mapper.StoricoContrattoDmlListenerMapper;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsDmlStoricoContrattoListener
  extends AbstractMappableDmlListener<ContrattoDMLDTO, StoricoContratto>   implements JmsTopicListener{

  public JmsDmlStoricoContrattoListener(StoricoContrattoRepository repository,
                                     StoricoContrattoDmlListenerMapper contrattoDmlListenerMapper,ApplicationEventPublisher publisher){
    super(ContrattoDMLDTO.class, repository, contrattoDmlListenerMapper,publisher,false);
  }

  @Value("${application.isIntilialLoading:}")
  private String isInitialLoading;


  @Override
  public JmsTopicNames getTopicName() {
	  if ("true".equalsIgnoreCase(isInitialLoading)){
		  return JmsTopicNames.NO_LISTENER_3;
	  }
    return JmsTopicNames.DML_CONTRATTI_SAVE;
  }
}
