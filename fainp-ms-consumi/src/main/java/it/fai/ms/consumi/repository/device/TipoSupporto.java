package it.fai.ms.consumi.repository.device;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_supporto")
public class TipoSupporto implements Serializable {

  private static final long serialVersionUID = -8996385216085101808L;
  
  @Id
  @Column(name = "tipo_dispositivo")
  private String tipoDispositivo;

  @Column(name = "tipo_supporto_fix")
  private String tipoSupportoFix;

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getTipoSupportoFix() {
    return tipoSupportoFix;
  }

  public void setTipoSupportoFix(String tipoSupportoFix) {
    this.tipoSupportoFix = tipoSupportoFix;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TipoSupporto [tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", tipoSupportoFix=");
    builder.append(tipoSupportoFix);
    builder.append("]");
    return builder.toString();
  }

}
