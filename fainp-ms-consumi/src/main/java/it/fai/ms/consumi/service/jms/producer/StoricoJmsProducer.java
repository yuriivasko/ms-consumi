package it.fai.ms.consumi.service.jms.producer;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoOrdiniCliente;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoRichiesta;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;
import it.fai.ms.consumi.service.jms.mapper.DispositivoDMLDTOMapper;
import it.fai.ms.consumi.service.jms.mapper.StoricoClasseEuroDMLDTOMapper;
import it.fai.ms.consumi.service.jms.mapper.StoricoContrattoDMLDTOMapper;
import it.fai.ms.consumi.service.jms.mapper.StoricoOrdiniClienteDMLDTOMapper;
import it.fai.ms.consumi.service.jms.mapper.StoricoRichiestaDMLDTOMapper;

@Service
@Transactional
public class StoricoJmsProducer {

  private Logger _log = LoggerFactory.getLogger(getClass());

  public final static JmsQueueNames QUEUE = JmsQueueNames.ALLINEAMENTO_DML_SU_WARMDATA;

  private final JmsProperties jmsProperties;
  private final DispositivoDMLDTOMapper dispositivoDMLDTOMapper;
  private final StoricoOrdiniClienteDMLDTOMapper storicoOrdiniClienteDMLDTOMapper;
  private final StoricoRichiestaDMLDTOMapper storicoRichiestaDMLDTOMapper;
  private final StoricoContrattoDMLDTOMapper storicoContrattoDMLDTOMapper;
  private final StoricoClasseEuroDMLDTOMapper storicoVeicoloDMLDTOMapper;

  public StoricoJmsProducer(JmsProperties _jmsProperties,
                            DispositivoDMLDTOMapper _dispositivoDMLDTOMapper,
                            StoricoOrdiniClienteDMLDTOMapper _storicoOrdiniClienteDMLDTOMapper,
                            StoricoRichiestaDMLDTOMapper _storicoRichiestaDMLDTOMapper,
                            StoricoContrattoDMLDTOMapper _storicoContrattoDMLDTOMapper,
                            StoricoClasseEuroDMLDTOMapper _storicoVeicoloDMLDTOMapper) {
    jmsProperties = _jmsProperties;
    dispositivoDMLDTOMapper = _dispositivoDMLDTOMapper;
    storicoOrdiniClienteDMLDTOMapper = _storicoOrdiniClienteDMLDTOMapper;
    storicoRichiestaDMLDTOMapper = _storicoRichiestaDMLDTOMapper;
    storicoContrattoDMLDTOMapper = _storicoContrattoDMLDTOMapper; 
    storicoVeicoloDMLDTOMapper = _storicoVeicoloDMLDTOMapper;
  }

  public void send(final Serializable data) {
    _log.debug("JMS Send data : {} to queue : {}", data, QUEUE);
    JmsQueueSenderUtil.publish(jmsProperties, QUEUE, data);
  }
  
  public void send(final StoricoDispositivo data) {
    send(dispositivoDMLDTOMapper.toDTO(data));
  }

  public void send(final StoricoOrdiniCliente data) {
    send(storicoOrdiniClienteDMLDTOMapper.toDTO(data));
  }

  public void send(final StoricoRichiesta data) {
    send(storicoRichiestaDMLDTOMapper.toDTO(data));
  }
  
  public void send(final StoricoContratto data) {
    send(storicoContrattoDMLDTOMapper.toDTO(data));
  }
  
  public void send(final StoricoVeicolo data) {
    send(storicoVeicoloDMLDTOMapper.toDTO(data));
  }
  
  //  public void send(final Collection<?> rows) throws Exception {
//    _log.debug("JMS Send data collection (size : {})", rows != null ? rows.size() : -1);
//    Optional.ofNullable(rows)
//    .map(Collection::stream)
//    .orElseGet(Stream::empty)
//    .forEach(row -> {
//      send((Serializable) row);
//    });
//  }

}
