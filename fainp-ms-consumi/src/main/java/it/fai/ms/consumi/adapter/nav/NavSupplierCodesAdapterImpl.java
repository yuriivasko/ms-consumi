package it.fai.ms.consumi.adapter.nav;

import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.client.AnagaziendeService;

@Service
public class NavSupplierCodesAdapterImpl
  implements NavSupplierCodesAdapter {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final AnagaziendeService anagaziendeService;

  public NavSupplierCodesAdapterImpl(AnagaziendeService anagaziendeService) {
    this.anagaziendeService = anagaziendeService;
  }

  @Override
  public Set<String> getAllCodes() {

    final Set<String> codes = anagaziendeService.getAllFornitori().stream().map(
      fornitoreDTO -> fornitoreDTO.getNo()
    ).collect(Collectors.toSet());

    _log.info("Called nav to get all codes [total items {}]", codes.size());

    return codes;
  }

}
