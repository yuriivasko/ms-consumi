package it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class DgdTollGlobalIdentifier
  extends GlobalIdentifier {

  public DgdTollGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.EXTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    throw new IllegalStateException("newGlobalIdentifier operation is supported only for global identifier type internal");
  }

}
