package it.fai.ms.consumi.repository.dettaglio_stanziamento;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.GlobalIdentifierType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.domain.stanziamento.GeneraStanziamento;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity_;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico.DettaglioStanziamentoGenericoEntity_;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity_;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno.DettaglioStanziamentoTrenoEntity_;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity_;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;
import it.fai.ms.consumi.util.SqlUtil;

@Repository
@Validated
@Transactional
public class DettaglioStanziamentiRepositoryImpl implements DettaglioStanziamantiRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoEntityMapper dettaglioStanziamentoEntityMapper;
  private final StanziamentoEntityMapper          stanziamentoEntityMapper;
  private final EntityManager                     em;

  @Inject
  public DettaglioStanziamentiRepositoryImpl(final EntityManager _entityManager,
                                             final DettaglioStanziamentoEntityMapper _dettaglioStanzimentoEntityMapper,
                                             final StanziamentoEntityMapper _stanziamentoEntityMapper) {
    em = _entityManager;
    dettaglioStanziamentoEntityMapper = _dettaglioStanzimentoEntityMapper;
    stanziamentoEntityMapper = _stanziamentoEntityMapper;
  }

  @Override
  public Set<DettaglioStanziamento> findByCodiceStanziamento(@NotNull final String _codStanziamento) {

    var criteriaBuilder = em.getCriteriaBuilder();
    var criteriaQuery = criteriaBuilder.createQuery(DettaglioStanziamentoEntity.class);
    var root = criteriaQuery.from(DettaglioStanziamentoEntity.class);
    criteriaQuery.select(root);

    Join<Object, Object> join = root.join(DettaglioStanziamentoEntity_.STANZIAMENTI, JoinType.INNER);
    criteriaQuery.where(criteriaBuilder.equal(join.get(StanziamentoEntity_.CODE), _codStanziamento));

    TypedQuery<DettaglioStanziamentoEntity> query = em.createQuery(criteriaQuery);
    query.setMaxResults(1);

    logQuery(query);

    return query.getResultList()
                .stream()
                .map(dettaglioStanziamentoEntity -> dettaglioStanziamentoEntityMapper.toDomain(dettaglioStanziamentoEntity))
                .collect(toSet());
  }

  @Override
  public Set<DettaglioStanziamento> findByCodiceStanziamento(@NotNull final String _codStanziamento,
                                                             StanziamentiDetailsType stanziamentiDetailsType) {

    _log.debug("Searching findByCodiceStanziamento {} {}", _codStanziamento, stanziamentiDetailsType);

    if (stanziamentiDetailsType == StanziamentiDetailsType.PEDAGGI) {

      var criteriaBuilder = em.getCriteriaBuilder();
      var criteriaQuery = criteriaBuilder.createQuery(DettaglioStanziamentoPedaggioEntity.class);
      var root = criteriaQuery.from(DettaglioStanziamentoPedaggioEntity.class);
      criteriaQuery.select(root);

      Join<Object, Object> join = root.join(DettaglioStanziamentoPedaggioEntity_.STANZIAMENTI, JoinType.INNER);
      criteriaQuery.where(criteriaBuilder.equal(join.get(StanziamentoEntity_.CODE), _codStanziamento));

      TypedQuery<DettaglioStanziamentoPedaggioEntity> query = em.createQuery(criteriaQuery);
      query.setMaxResults(1);

      logQuery(query);

      return query.getResultList()
                  .stream()
                  .map(dettaglioStanziamentoEntity -> dettaglioStanziamentoEntityMapper.toDomain(dettaglioStanziamentoEntity))
                  .collect(toSet());

    }

    return findByCodiceStanziamento(_codStanziamento);
  }

  private void reAssociateStanziamentiWithAlreadyExistingOnDB(@NotNull final Set<StanziamentoEntity> allocationEntity,
                                                              @NotNull final DettaglioStanziamentoEntity _allocationDetailEntity) {

    allocationEntity.stream()
                    .forEach(stanziamentoEntity -> {
                      // il mapper stanziamento -> stanziamentoEntity ricrea anche i dettagliStanziamento, per cui li
                      // ripulisco
                      stanziamentoEntity.getDettaglioStanziamenti()
                                        .clear();
                    });

    // vengono mergiati gli stanziamenti con quelli presenti a DB, dovrebbe riassociare tutti i dettagli eventualmente
    // gia' persisiti
    final Set<StanziamentoEntity> stanziamentoEntities = allocationEntity.stream()
                                                                         .map(entity -> em.merge(entity))
                                                                         .collect(toSet());

    // associo gli stanziamenti appena mergiati al nuovo dettaglio
    _allocationDetailEntity.getStanziamenti()
                           .addAll(stanziamentoEntities);

    // associo il nuovo dettaglio (non ancora presisito) allo stanziamento mergiato
    stanziamentoEntities.stream()
                        .forEach(stanziamentoEntity -> {
                          stanziamentoEntity.getDettaglioStanziamenti()
                                            .add(_allocationDetailEntity);
                        });
  }

  private void reAssociateStanziamentiWithAlreadyExistingOnDB_PerformanceTuning(@NotNull final Set<StanziamentoEntity> allocationEntity,
                                                                                @NotNull DettaglioStanziamentoEntity _allocationDetailEntity) {

    allocationEntity.stream()
                    .forEach(stanziamentoEntity -> {
                      // il mapper stanziamento -> stanziamentoEntity ricrea anche i dettagliStanziamento, per cui li
                      // ripulisco
                      stanziamentoEntity.getDettaglioStanziamenti()
                                        .clear();
                    });

    // vengono mergiati gli stanziamenti con quelli presenti a DB, dovrebbe riassociare tutti i dettagli eventualmente
    // gia' persisiti
    final Set<StanziamentoEntity> stanziamentoEntities = allocationEntity.stream()
                                                                         .map(entity -> em.merge(entity))
                                                                         .collect(toSet());

    _log.trace("Inserting dettagliostanziamento_stanziamento {} ", _allocationDetailEntity);
    _allocationDetailEntity = em.merge(_allocationDetailEntity);

    for (StanziamentoEntity stanziamentoEntity : stanziamentoEntities) {
      if (_log.isDebugEnabled()) {
        _log.trace("Inserting dettagliostanziamento_stanziamento {} {}", _allocationDetailEntity.getId(), stanziamentoEntity.getCode());
      }
      Query query = em.createNativeQuery("insert into  dettagliostanziamento_stanziamento (dettaglio_stanziamenti_id, stanziamenti_codice_stanziamento) values ( ? , ? )");
      int updateCount = query.setParameter(1, _allocationDetailEntity.getId()
                                                                     .toString())
                             .setParameter(2, stanziamentoEntity.getCode())
                             .executeUpdate();
      if (_log.isDebugEnabled()) {
        if (updateCount > 0) {
          _log.trace(" DateQueuing updated on {} : {}", _allocationDetailEntity.getId(), stanziamentoEntity.getCode());
        } else {
          _log.trace(" DateQueuing not updated (id not found {} {})", _allocationDetailEntity.getId(), stanziamentoEntity.getCode());
        }
      }
    }
    em.flush();
  }

  @Override
  public DettaglioStanziamentoEntity update(@NotNull final DettaglioStanziamentoEntity _allocationDetailEntity) {
    _log.debug(" Updating  : {}", _allocationDetailEntity);

    @NotNull
    DettaglioStanziamentoEntity merged = em.merge(_allocationDetailEntity);
    _log.trace("Updated: {}", merged);
    return merged;
  }

  @Override
  public Instant findLastDettagliStanziamentoDate(String tipoFlusso) {
    CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
    CriteriaQuery<Object> criteriaQuery = criteriaBuilder.createQuery(Object.class);
    Root<DettaglioStanziamentoPedaggioEntity> root = criteriaQuery.from(DettaglioStanziamentoPedaggioEntity.class);

    criteriaQuery.select(criteriaBuilder.max(root.get(DettaglioStanziamentoPedaggioEntity_.DATA_ACQUISIZIONE_FLUSSO)));

    criteriaQuery.where(criteriaBuilder.equal(root.get(DettaglioStanziamentoPedaggioEntity_.SOURCE_TYPE), tipoFlusso));
    return (Instant) em.createQuery(criteriaQuery)
                       .getSingleResult();
  }

  @Override
  public List<DettaglioStanziamentoPedaggioEntity> findDettagliStanziamentiProvvisoriBySorgenteAndCodRaggruppamentiBeforeDate(String sorgente,
                                                                                                                              Map<String, Integer> codiceRaggruppamentoGiorniMap,
                                                                                                                              Instant ingestionTime) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<DettaglioStanziamentoPedaggioEntity> criteriaQuery = cb.createQuery(DettaglioStanziamentoPedaggioEntity.class);
    Root<DettaglioStanziamentoPedaggioEntity> root = criteriaQuery.from(DettaglioStanziamentoPedaggioEntity.class);
    final List<Predicate> orConditions = codiceRaggruppamentoGiorniMap.entrySet()
                                                                      .stream()
                                                                      .map(e -> cb.and(cb.equal(root.get(DettaglioStanziamentoPedaggioEntity_.ARTICLES_GROUP),
                                                                                                e.getKey()),
                                                                                       cb.lessThan(root.get(DettaglioStanziamentoPedaggioEntity_.DATA_ACQUISIZIONE_FLUSSO),
                                                                                                   ingestionTime.minus(e.getValue(),
                                                                                                                       ChronoUnit.DAYS))))
                                                                      .collect(Collectors.toList());

    final var predicates = List.of(cb.equal(root.get(DettaglioStanziamentoPedaggioEntity_.SOURCE_TYPE), sorgente),
                                   cb.equal(root.get(DettaglioStanziamentoPedaggioEntity_.INVOICE_TYPE), InvoiceType.P),
                                   cb.or(orConditions.toArray(new Predicate[0])))
                               .toArray(new Predicate[0]);
    criteriaQuery.where(predicates);
    return em.createQuery(criteriaQuery)
             .getResultList();
  }

  @Override
  public void save(@NotNull final DettaglioStanziamentoEntity _allocationDetailEntity) {

    _log.debug(" Persisting Entity  : {}", _allocationDetailEntity);

    final var allocationEntity = _allocationDetailEntity.getStanziamenti();
    reAssociateStanziamentiWithAlreadyExistingOnDB(allocationEntity, _allocationDetailEntity);
    em.persist(_allocationDetailEntity);
    em.flush();

    _log.trace(" Persisted Entity : {}", _allocationDetailEntity);
  }

  @Override
  public void save(@NotNull final DettaglioStanziamento _allocationDetail) {

    _log.debug(" Persisting  : {}", _allocationDetail);

    DettaglioStanziamentoEntity allocationDetailEntity = null;
    Optional<UUID> dbId = _allocationDetail.getGlobalIdentifier()
                                           .getDbId();
    if (dbId.isPresent()) {
      if (_allocationDetail instanceof DettaglioStanziamentoPedaggio) {
        allocationDetailEntity = em.find(DettaglioStanziamentoPedaggioEntity.class, dbId.get());
      } else {
        allocationDetailEntity = em.find(DettaglioStanziamentoEntity.class, dbId.get());
      }
    }
    if (allocationDetailEntity != null) {
      allocationDetailEntity = dettaglioStanziamentoEntityMapper.toEntity(_allocationDetail, allocationDetailEntity);
      final var allocationEntity = stanziamentoEntityMapper.toEntity(_allocationDetail.getStanziamenti());
      reAssociateStanziamentiWithAlreadyExistingOnDB(allocationEntity, allocationDetailEntity);
    } else {
      allocationDetailEntity = dettaglioStanziamentoEntityMapper.toEntity(_allocationDetail);
      final var allocationEntity = stanziamentoEntityMapper.toEntity(_allocationDetail.getStanziamenti());
      reAssociateStanziamentiWithAlreadyExistingOnDB_PerformanceTuning(allocationEntity, allocationDetailEntity);
    }

    _log.trace(" Persisted   : {}", allocationDetailEntity);
  }

  @Override
  public Optional<DettaglioStanziamentoEntity> findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(@NotNull final String _globalIdentifierId,
                                                                                                                    StanziamentiDetailsType detailType,
                                                                                                                    UUID idEntity,
                                                                                                                    CostRevenue costRevenueToFilter) {
    DettaglioStanziamentoEntity entity = null;
    TypedQuery<?> createdQuery = null;

    final var criteriaBuilder = em.getCriteriaBuilder();
    CriteriaQuery<DettaglioStanziamentoEntity> query = criteriaBuilder.createQuery(DettaglioStanziamentoEntity.class);
    Root<DettaglioStanziamentoEntity> root = query.from(DettaglioStanziamentoEntity.class);

    query.select(root);

    var predicates = new ArrayList<Predicate>();

    Path<Object> path = null;
    Predicate pGlobalIdentifier = null;
    Predicate pIdEntityNotIs = null;
    switch (detailType) {
    case PEDAGGI:
      CriteriaQuery<DettaglioStanziamentoPedaggioEntity> queryPedaggio = criteriaBuilder.createQuery(DettaglioStanziamentoPedaggioEntity.class);
      Root<DettaglioStanziamentoPedaggioEntity> rootPedaggio = queryPedaggio.from(DettaglioStanziamentoPedaggioEntity.class);
      path = rootPedaggio.get(DettaglioStanziamentoPedaggioEntity_.GLOBAL_IDENTIFIER);
      pGlobalIdentifier = criteriaBuilder.equal(path, _globalIdentifierId);
      predicates.add(pGlobalIdentifier);

      pIdEntityNotIs = criteriaBuilder.notEqual(rootPedaggio.get(DettaglioStanziamentoPedaggioEntity_.ID), idEntity);
      predicates.add(pIdEntityNotIs);
      queryPedaggio.where(predicates.toArray(new Predicate[] {}));
      queryPedaggio.select(rootPedaggio);
      queryPedaggio.orderBy(criteriaBuilder.desc(rootPedaggio.get(DettaglioStanziamentoPedaggioEntity_.INGESTION_TIME)));
      createdQuery = em.createQuery(queryPedaggio);
      break;
    case CARBURANTI:
      CriteriaQuery<DettaglioStanziamentoCarburanteEntity> queryCarburante = criteriaBuilder.createQuery(DettaglioStanziamentoCarburanteEntity.class);
      Root<DettaglioStanziamentoCarburanteEntity> rootCarburante = queryCarburante.from(DettaglioStanziamentoCarburanteEntity.class);
      path = rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.GLOBAL_IDENTIFIER);
      pGlobalIdentifier = criteriaBuilder.equal(path, _globalIdentifierId);
      predicates.add(pGlobalIdentifier);

      pIdEntityNotIs = criteriaBuilder.notEqual(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.ID), idEntity);
      predicates.add(pIdEntityNotIs);

      Predicate notStorno = criteriaBuilder.isFalse(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.STORNO));
      predicates.add(notStorno);


      if (costRevenueToFilter != null) {
        Predicate pCostRevenue = null;
        _log.trace("Cost revenue to filter: {}", costRevenueToFilter);
        if (CostRevenue.COST.equals(costRevenueToFilter)) {
          _log.trace("Filter to is not null {}", DettaglioStanziamentoCarburanteEntity_.COSTO_UNITARIO_CALCOLATO_NOIVA);
          pCostRevenue = criteriaBuilder.isNotNull(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.COSTO_UNITARIO_CALCOLATO_NOIVA));
        } else {
          _log.trace("Filter to is not null {}", DettaglioStanziamentoCarburanteEntity_.PREZZO_UNITARIO_CALCOLATO_NOIVA);
          pCostRevenue = criteriaBuilder.isNotNull(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.PREZZO_UNITARIO_CALCOLATO_NOIVA));
        }
        predicates.add(pCostRevenue);
      }

      queryCarburante.where(predicates.toArray(new Predicate[] {}));
      queryCarburante.select(rootCarburante);
      queryCarburante.orderBy(criteriaBuilder.desc(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.INGESTION_TIME)));
      createdQuery = em.createQuery(queryCarburante);
      break;
    case TRENI:
      CriteriaQuery<DettaglioStanziamentoTrenoEntity> queryTreni = criteriaBuilder.createQuery(DettaglioStanziamentoTrenoEntity.class);
      Root<DettaglioStanziamentoTrenoEntity> rootTreni = queryTreni.from(DettaglioStanziamentoTrenoEntity.class);
      path = rootTreni.get(DettaglioStanziamentoTrenoEntity_.GLOBAL_IDENTIFIER);
      pGlobalIdentifier = criteriaBuilder.equal(path, _globalIdentifierId);
      predicates.add(pGlobalIdentifier);

      pIdEntityNotIs = criteriaBuilder.notEqual(rootTreni.get(DettaglioStanziamentoTrenoEntity_.ID), idEntity);
      predicates.add(pIdEntityNotIs);
      queryTreni.where(predicates.toArray(new Predicate[] {}));
      queryTreni.select(rootTreni);
      queryTreni.orderBy(criteriaBuilder.desc(rootTreni.get(DettaglioStanziamentoTrenoEntity_.INGESTION_TIME)));
      createdQuery = em.createQuery(queryTreni);
      break;
    case GENERICO:
      CriteriaQuery<DettaglioStanziamentoGenericoEntity> queryGenerico = criteriaBuilder.createQuery(DettaglioStanziamentoGenericoEntity.class);
      Root<DettaglioStanziamentoGenericoEntity> rootGenerico = queryGenerico.from(DettaglioStanziamentoGenericoEntity.class);
      path = rootGenerico.get(DettaglioStanziamentoGenericoEntity_.GLOBAL_IDENTIFIER);
      pGlobalIdentifier = criteriaBuilder.equal(path, _globalIdentifierId);
      predicates.add(pGlobalIdentifier);

      pIdEntityNotIs = criteriaBuilder.notEqual(rootGenerico.get(DettaglioStanziamentoGenericoEntity_.ID), idEntity);
      predicates.add(pIdEntityNotIs);
      queryGenerico.where(predicates.toArray(new Predicate[] {}));
      queryGenerico.select(rootGenerico);
      queryGenerico.orderBy(criteriaBuilder.desc(rootGenerico.get(DettaglioStanziamentoGenericoEntity_.INGESTION_TIME)));
      createdQuery = em.createQuery(queryGenerico);
      break;
    }

    createdQuery.setMaxResults(1);

    @SuppressWarnings("unchecked")
    List<DettaglioStanziamentoEntity> list = (List<DettaglioStanziamentoEntity>) createdQuery.getResultList();
    if (list != null && !list.isEmpty()) {
      entity = list.get(0);
    }
    return Optional.ofNullable(entity);
  }

  public Optional<DettaglioStanziamentoEntity> findLastDettaglioCarburantiByGlobalIdentifierExcludingStorni(@NotNull final String transactionCode) {
    DettaglioStanziamentoEntity entity = null;
    TypedQuery<?> createdQuery = null;

    final var criteriaBuilder = em.getCriteriaBuilder();
    CriteriaQuery<DettaglioStanziamentoEntity> query = criteriaBuilder.createQuery(DettaglioStanziamentoEntity.class);
    Root<DettaglioStanziamentoEntity> root = query.from(DettaglioStanziamentoEntity.class);

    query.select(root);

    var predicates = new ArrayList<Predicate>();

    Path<Object> path = null;
    Predicate pGlobalIdentifier = null;
//    Predicate pIdEntityNotIs = null;

    CriteriaQuery<DettaglioStanziamentoCarburanteEntity> queryCarburante = criteriaBuilder.createQuery(DettaglioStanziamentoCarburanteEntity.class);
    Root<DettaglioStanziamentoCarburanteEntity> rootCarburante = queryCarburante.from(DettaglioStanziamentoCarburanteEntity.class);
    path = rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.GLOBAL_IDENTIFIER);
    pGlobalIdentifier = criteriaBuilder.or(
        criteriaBuilder.equal(path, transactionCode + "#" + GlobalIdentifierType.INTERNAL),
      criteriaBuilder.equal(path, transactionCode + "#" + GlobalIdentifierType.EXTERNAL)
    );
    predicates.add(pGlobalIdentifier);

    Predicate notStorno = criteriaBuilder.isFalse(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.STORNO));
    predicates.add(notStorno);

//    pIdEntityNotIs = criteriaBuilder.notEqual(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.ID), idEntity);
//    predicates.add(pIdEntityNotIs);

//    if (costRevenueToFilter != null) {
//      Predicate pCostRevenue = null;
//      _log.trace("Cost revenue to filter: {}", costRevenueToFilter);
//      if (CostRevenue.COST.equals(costRevenueToFilter)) {
//        _log.trace("Filter to is not null {}", DettaglioStanziamentoCarburanteEntity_.COSTO_UNITARIO_CALCOLATO_NOIVA);
//        pCostRevenue = criteriaBuilder.isNotNull(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.COSTO_UNITARIO_CALCOLATO_NOIVA));
//      } else {
//        _log.trace("Filter to is not null {}", DettaglioStanziamentoCarburanteEntity_.PREZZO_UNITARIO_CALCOLATO_NOIVA);
//        pCostRevenue = criteriaBuilder.isNotNull(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.PREZZO_UNITARIO_CALCOLATO_NOIVA));
//      }
//      predicates.add(pCostRevenue);
//    }

    queryCarburante.where(predicates.toArray(new Predicate[] {}));
    queryCarburante.select(rootCarburante);
    queryCarburante.orderBy(criteriaBuilder.desc(rootCarburante.get(DettaglioStanziamentoCarburanteEntity_.INGESTION_TIME)));
    createdQuery = em.createQuery(queryCarburante);


    createdQuery.setMaxResults(1);

    @SuppressWarnings("unchecked")
    List<DettaglioStanziamentoEntity> list = (List<DettaglioStanziamentoEntity>) createdQuery.getResultList();
    if (list != null && !list.isEmpty()) {
      entity = list.get(0);
    }
    return Optional.ofNullable(entity);
  }


  @Override
  public Set<DettaglioStanziamento> findByGlobalIdentifierId(@NotNull final GlobalIdentifier _globalIdentifierId,
                                                             StanziamentiDetailsType stanziamentiDetailsType) {
    Set<DettaglioStanziamento> allocationDetails = new HashSet<>();

    if (stanziamentiDetailsType == StanziamentiDetailsType.PEDAGGI) {

      final var criteriaBuilder = em.getCriteriaBuilder();
      final var query = criteriaBuilder.createQuery(DettaglioStanziamentoPedaggioEntity.class);
      final Root<DettaglioStanziamentoPedaggioEntity> root = query.from(DettaglioStanziamentoPedaggioEntity.class);
      query.select(root);

      _log.debug(" Searching1 {} by (like) gId : {}", DettaglioStanziamentoPedaggioEntity.class.getSimpleName(), _globalIdentifierId);

      final var predicates = List.of(criteriaBuilder.equal(root.get(DettaglioStanziamentoPedaggioEntity_.GLOBAL_IDENTIFIER),
                                                           _globalIdentifierId.encode()))
                                 .toArray(new Predicate[0]);
      query.where(predicates);

      allocationDetails = em.createQuery(query)
                            .getResultStream()
                            // .peek(entity -> _log.debug("Found {}", entity)) WARN: abilitando il log, il sistema fa 3
                            // query inutili: da verificare se è il caso di togliere dal toString gli oggetti lazy
                            .map(entity -> dettaglioStanziamentoEntityMapper.toDomain(entity))
                            .collect(toSet());

    } else {
      allocationDetails = findByGlobalIdentifierId(_globalIdentifierId);
    }

    return allocationDetails;
  }

  @Override
  public Set<DettaglioStanziamento> findByGlobalIdentifierId(@NotNull final GlobalIdentifier _globalIdentifierId) {

    Set<DettaglioStanziamento> allocationDetails = new HashSet<>();

    final var criteriaBuilder = em.getCriteriaBuilder();
    final var query = criteriaBuilder.createQuery(DettaglioStanziamentoEntity.class);
    final Root<DettaglioStanziamentoEntity> root = query.from(DettaglioStanziamentoEntity.class);
    query.select(root);

    _log.debug(" Searching2 {} by (like) gId : {}", Stanziamento.class.getSimpleName(), _globalIdentifierId);

    final var predicates = List.of(criteriaBuilder.equal(root.get(DettaglioStanziamentoEntity_.GLOBAL_IDENTIFIER),
                                                         _globalIdentifierId.encode()))
                               .toArray(new Predicate[0]);
    query.where(predicates);

    allocationDetails = em.createQuery(query)
                          .getResultStream()
                          // .peek(entity -> _log.debug("Found {}", entity)) WARN: abilitando il log, il sistema fa 3
                          // query inutili: da verificare se è il caso di togliere dal toString gli oggetti lazy
                          .map(entity -> dettaglioStanziamentoEntityMapper.toDomain(entity))
                          .collect(toSet());

    return allocationDetails;
  }

  private Optional<DettaglioStanziamento> findTempByGlobalIdentifierId(@NotNull final GlobalIdentifier _globalIdentifierId) {
    final var allocationDetails = findByGlobalIdentifierId(_globalIdentifierId);
    return allocationDetails.stream()
                            .filter(allocationDetail -> allocationDetail.getInvoiceType()
                                                                        .equals(InvoiceType.P))
                            .findFirst();
  }

  private Optional<DettaglioStanziamento> findLastByGlobalIdentifierId(@NotNull final GlobalIdentifier _globalIdentifierId) {
    final var allocationDetails = findByGlobalIdentifierId(_globalIdentifierId);
    return allocationDetails.stream()
                            .filter(allocationDetail -> allocationDetail.getInvoiceType()
                                                                        .equals(InvoiceType.D))
                            .findFirst();
  }

  @Override
  public Set<DettaglioStanziamento> findByCodiciStanziamentoAndTipoDettaglio(@NotNull Set<String> codiciStanziamenti,
                                                                             DettaglioStanziamentoType tipoDettaglio) {
    Set<DettaglioStanziamento> dettagliStanziamento = new HashSet<>();
    switch (tipoDettaglio) {
    case PEDAGGIO:
      dettagliStanziamento = findPedaggioByCodiciStanziamentoGroupedByNumeroFattura(codiciStanziamenti);
      break;

    case CARBURANTE:
      dettagliStanziamento = findCarburanteByCodiciStanziamentoGroupedByNumeroFattura(codiciStanziamenti);
      break;

    case GENERICO:
      dettagliStanziamento = findTrenoByCodiciStanziamentoGroupedByNumeroFattura(codiciStanziamenti);
      break;

    case TRENO:
      dettagliStanziamento = findGenericoByCodiciStanziamentoGroupedByNumeroFattura(codiciStanziamenti);
      break;

    default:
      _log.warn("Not managed {} as TypeAllocationDetail", tipoDettaglio);
      dettagliStanziamento = findByCodiciStanziamentoGroupedByNumeroFattura(codiciStanziamenti);
      break;
    }

    return dettagliStanziamento;
  }

  private Set<DettaglioStanziamento> findByCodiciStanziamentoGroupedByNumeroFattura(Set<String> codiciStanziamenti) {
    Set<DettaglioStanziamento> dettagliStanziamento = null;
    final var criteriaBuilder = em.getCriteriaBuilder();
    var query = criteriaBuilder.createQuery(DettaglioStanziamentoEntity.class);
    var root = query.from(DettaglioStanziamentoEntity.class);

    query.select(root);
    query.where(root.join(DettaglioStanziamentoEntity_.STANZIAMENTI, JoinType.INNER)
                    .get(StanziamentoEntity_.CODE)
                    .in(codiciStanziamenti));

    dettagliStanziamento = em.createQuery(query)
                             .getResultStream()
                             .map(entity -> {
                               _log.debug(" Found {}", entity);
                               return dettaglioStanziamentoEntityMapper.toDomain(entity);
                             })
                             .collect(toSet());

    return dettagliStanziamento;
  }

  private Set<DettaglioStanziamento> findPedaggioByCodiciStanziamentoGroupedByNumeroFattura(Set<String> codiciStanziamenti) {
    Set<DettaglioStanziamento> dettagliStanziamento = null;
    final var criteriaBuilder = em.getCriteriaBuilder();
    var query = criteriaBuilder.createQuery(DettaglioStanziamentoPedaggioEntity.class);
    var root = query.from(DettaglioStanziamentoPedaggioEntity.class);

    query.select(root);
    query.where(root.join(DettaglioStanziamentoEntity_.STANZIAMENTI, JoinType.INNER)
                    .get(StanziamentoEntity_.CODE)
                    .in(codiciStanziamenti));

    dettagliStanziamento = em.createQuery(query)
                             .getResultStream()
                             .map(entity -> {
                               _log.debug(" Found {}", entity);
                               return dettaglioStanziamentoEntityMapper.toDomain(entity);
                             })
                             .collect(toSet());

    return dettagliStanziamento;

  }

  private Set<DettaglioStanziamento> findCarburanteByCodiciStanziamentoGroupedByNumeroFattura(Set<String> codiciStanziamenti) {
    Set<DettaglioStanziamento> dettagliStanziamento = null;
    final var criteriaBuilder = em.getCriteriaBuilder();
    var query = criteriaBuilder.createQuery(DettaglioStanziamentoCarburanteEntity.class);
    var root = query.from(DettaglioStanziamentoCarburanteEntity.class);

    query.select(root);
    query.where(root.join(DettaglioStanziamentoEntity_.STANZIAMENTI, JoinType.INNER)
                    .get(StanziamentoEntity_.CODE)
                    .in(codiciStanziamenti));

    dettagliStanziamento = em.createQuery(query)
                             .getResultStream()
                             .map(entity -> {
                               _log.debug(" Found {}", entity);
                               return dettaglioStanziamentoEntityMapper.toDomain(entity);
                             })
                             .collect(toSet());

    return dettagliStanziamento;

  }

  private Set<DettaglioStanziamento> findTrenoByCodiciStanziamentoGroupedByNumeroFattura(Set<String> codiciStanziamenti) {
    Set<DettaglioStanziamento> dettagliStanziamento = null;
    final var criteriaBuilder = em.getCriteriaBuilder();
    var query = criteriaBuilder.createQuery(DettaglioStanziamentoTrenoEntity.class);
    var root = query.from(DettaglioStanziamentoTrenoEntity.class);

    query.select(root);
    query.where(root.join(DettaglioStanziamentoEntity_.STANZIAMENTI, JoinType.INNER)
                    .get(StanziamentoEntity_.CODE)
                    .in(codiciStanziamenti));

    dettagliStanziamento = em.createQuery(query)
                             .getResultStream()
                             .map(entity -> {
                               _log.debug(" Found {}", entity);
                               return dettaglioStanziamentoEntityMapper.toDomain(entity);
                             })
                             .collect(toSet());
    return dettagliStanziamento;
  }

  private Set<DettaglioStanziamento> findGenericoByCodiciStanziamentoGroupedByNumeroFattura(Set<String> codiciStanziamenti) {
    Set<DettaglioStanziamento> dettagliStanziamento = null;
    final var criteriaBuilder = em.getCriteriaBuilder();
    var query = criteriaBuilder.createQuery(DettaglioStanziamentoGenericoEntity.class);
    var root = query.from(DettaglioStanziamentoGenericoEntity.class);

    query.select(root);
    query.where(root.join(DettaglioStanziamentoEntity_.STANZIAMENTI, JoinType.INNER)
                    .get(StanziamentoEntity_.CODE)
                    .in(codiciStanziamenti));

    dettagliStanziamento = em.createQuery(query)
                             .getResultStream()
                             .map(entity -> {
                               _log.debug(" Found {}", entity);
                               return dettaglioStanziamentoEntityMapper.toDomain(entity);
                             })
                             .collect(toSet());
    return dettagliStanziamento;
  }

  private void logQuery(TypedQuery<?> query) {
    if (!_log.isDebugEnabled())
      return;

    String queryString = query.unwrap(org.hibernate.query.Query.class)
                              .getQueryString();
    _log.trace("Query string: {}", queryString);
  }

  @Override
  public List<DettaglioStanziamentoCarburanteEntity> findToUpdatePriceTable(PriceTableFilterDTO priceTableFilter) {

    _log.debug("{}", priceTableFilter);
    String codiceArticolo = priceTableFilter.getArticleCode();
    String supplierCode = priceTableFilter.getSupplierCode();
    InvoiceType invoiceType = priceTableFilter.getInvoiceType();
    CostRevenue costoRicavo = priceTableFilter.getCostRevenueFlag();
    Instant dateFrom = priceTableFilter.getDateFrom();
    Instant dateTo = priceTableFilter.getDateTo();

    final var criteriaBuilder = em.getCriteriaBuilder();
    final var query = criteriaBuilder.createQuery(DettaglioStanziamentoCarburanteEntity.class);
    final Root<DettaglioStanziamentoCarburanteEntity> root = query.from(DettaglioStanziamentoCarburanteEntity.class);
    query.select(root);

    List<Predicate> predicates = new ArrayList<>();

    Predicate p = null;
    Join<Object, Object> path = root.join(DettaglioStanziamentoCarburanteEntity_.STANZIAMENTI, JoinType.INNER);
    p = criteriaBuilder.equal(path.get(StanziamentoEntity_.ARTICLE_CODE), codiceArticolo);
    predicates.add(p);
    
    p = criteriaBuilder.equal(root.get(DettaglioStanziamentoCarburanteEntity_.CODICE_FORNITORE_NAV), supplierCode);
    predicates.add(p);

    p = criteriaBuilder.equal(path.get(StanziamentoEntity_.INVOICE_TYPE), invoiceType);
    predicates.add(p);

    GeneraStanziamento gs = costoRicavo == CostRevenue.COST ? GeneraStanziamento.COSTO
                                                            : (costoRicavo == CostRevenue.REVENUE) ? GeneraStanziamento.RICAVO : null;
    _log.trace("GeneraStanziamento: {}", gs);

    if (dateTo == null) {
      p = criteriaBuilder.greaterThanOrEqualTo(root.get(DettaglioStanziamentoCarburanteEntity_.DATA_ORA_UTILIZZO), dateFrom);
    } else {
      Predicate gt = criteriaBuilder.greaterThanOrEqualTo(root.get(DettaglioStanziamentoCarburanteEntity_.DATA_ORA_UTILIZZO), dateFrom);
      Predicate lt = criteriaBuilder.lessThan(root.get(DettaglioStanziamentoCarburanteEntity_.DATA_ORA_UTILIZZO), dateTo);
      p = criteriaBuilder.and(gt, lt);
    }
    predicates.add(p);

    p = criteriaBuilder.equal(path.get(StanziamentoEntity_.GENERA_STANZIAMENTO), gs);
    predicates.add(p);

    p = criteriaBuilder.equal(root.get(DettaglioStanziamentoCarburanteEntity_.TIPO_CONSUMO), invoiceType);
    predicates.add(p);

    p = criteriaBuilder.isFalse(path.get(StanziamentoEntity_.CONGUAGLIO));
    predicates.add(p);

    p = criteriaBuilder.isFalse(root.get(DettaglioStanziamentoCarburanteEntity_.STORNO));
    predicates.add(p);

    query.where(predicates.toArray(new Predicate[] {}));

    TypedQuery<DettaglioStanziamentoCarburanteEntity> createdQuery = em.createQuery(query);

    logQuery(createdQuery);

    var allocationDetailList = createdQuery.getResultStream()
                                           .filter(dse -> dse instanceof DettaglioStanziamentoCarburanteEntity && !dse.isStorno()
                                                          && dse.getTipoConsumo()
                                                                .equals(InvoiceType.P)
                                                          && dse.getCodiceFornitoreNav()
                                                                .equals(supplierCode))
                                           .collect(toList());
    return allocationDetailList;
  }

  public DettaglioStanziamentoEntity cloneAndCleanId(@NotNull DettaglioStanziamentoEntity _dettaglioEntity, boolean includeStanziamento) {
    UUID idOldEntity = _dettaglioEntity.getId();
    _log.debug("Cloning {} with ID (UUID): {}", _dettaglioEntity.getClass()
                                                               .getSimpleName(),
              idOldEntity);
    DettaglioStanziamentoEntity ds = null;
    if (_dettaglioEntity instanceof DettaglioStanziamentoCarburanteEntity) {
      DettaglioStanziamentoCarburanteEntity dsCarburanteEntity = (DettaglioStanziamentoCarburanteEntity) _dettaglioEntity;
      DettaglioStanziamentoCarburanteEntity dsc = new DettaglioStanziamentoCarburanteEntity(dsCarburanteEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsCarburanteEntity, dsc);
      if (includeStanziamento) {
        dsc.getStanziamenti()
           .addAll(dsCarburanteEntity.getStanziamenti());
      }
      dsc.clearId();

      _log.debug("Storno of the NEW dettaglio Carburante: {}", dsc.isStorno());
      _log.debug("New Dettaglio Carburante: {}", dsc);

      ds = dsc;
    } else if (_dettaglioEntity instanceof DettaglioStanziamentoTrenoEntity) {
      DettaglioStanziamentoTrenoEntity dsTrenoEntity = (DettaglioStanziamentoTrenoEntity) _dettaglioEntity;
      DettaglioStanziamentoTrenoEntity dst = new DettaglioStanziamentoTrenoEntity(dsTrenoEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsTrenoEntity, dst);
      if (includeStanziamento) {
        dst.getStanziamenti()
           .addAll(dsTrenoEntity.getStanziamenti());
      }
      dst.clearId();
      ds = dst;
    } else if (_dettaglioEntity instanceof DettaglioStanziamentoPedaggioEntity) {
      DettaglioStanziamentoPedaggioEntity dsCarburanteEntity = (DettaglioStanziamentoPedaggioEntity) _dettaglioEntity;
      DettaglioStanziamentoPedaggioEntity dsp = new DettaglioStanziamentoPedaggioEntity(dsCarburanteEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsCarburanteEntity, dsp);
      if (includeStanziamento) {
        dsp.getStanziamenti()
           .addAll(dsCarburanteEntity.getStanziamenti());
      }
      dsp.clearId();
      ds = dsp;
    } else if (_dettaglioEntity instanceof DettaglioStanziamentoGenericoEntity) {
      DettaglioStanziamentoGenericoEntity dsGenericoEntity = (DettaglioStanziamentoGenericoEntity) _dettaglioEntity;
      DettaglioStanziamentoGenericoEntity dsg = new DettaglioStanziamentoGenericoEntity(dsGenericoEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsGenericoEntity, dsg);
      if (includeStanziamento) {
        dsg.getStanziamenti()
           .addAll(dsGenericoEntity.getStanziamenti());
      }
      dsg.clearId();
      ds = dsg;
    }

    _log.debug("Cloned {} (Old ID: {} )", _dettaglioEntity.getClass()
                                                         .getSimpleName(),
              idOldEntity);
    return ds;
  }

  @Override
  public DettaglioStanziamentoEntity cloneAndModify(@NotNull DettaglioStanziamentoEntity _dettaglioEntity) {
    UUID idOldEntity = _dettaglioEntity.getId();
    _log.debug("Cloning {} with ID (UUID): {}", _dettaglioEntity.getClass()
                                                               .getSimpleName(),
              idOldEntity);
    DettaglioStanziamentoEntity ds = null;
    if (_dettaglioEntity instanceof DettaglioStanziamentoCarburanteEntity) {
      DettaglioStanziamentoCarburanteEntity dsCarburanteEntity = (DettaglioStanziamentoCarburanteEntity) _dettaglioEntity;
      DettaglioStanziamentoCarburanteEntity dsc = new DettaglioStanziamentoCarburanteEntity(dsCarburanteEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsCarburanteEntity, dsc);
      dsc.getStanziamenti()
         .addAll(dsCarburanteEntity.getStanziamenti());
      dsc.clearId();

      _log.debug("Storno of the NEW dettaglio Carburante: {}", dsc.isStorno());
      _log.debug("New Dettaglio Carburante: {}", dsc);

      ds = dsc;
    } else if (_dettaglioEntity instanceof DettaglioStanziamentoTrenoEntity) {
      DettaglioStanziamentoTrenoEntity dsTrenoEntity = (DettaglioStanziamentoTrenoEntity) _dettaglioEntity;
      DettaglioStanziamentoTrenoEntity dst = new DettaglioStanziamentoTrenoEntity(dsTrenoEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsTrenoEntity, dst);
      dst.getStanziamenti()
         .addAll(dsTrenoEntity.getStanziamenti());
      dst.clearId();
      ds = dst;
    } else if (_dettaglioEntity instanceof DettaglioStanziamentoPedaggioEntity) {
      DettaglioStanziamentoPedaggioEntity dsPedaggioEntity = (DettaglioStanziamentoPedaggioEntity) _dettaglioEntity;
      DettaglioStanziamentoPedaggioEntity dsp = new DettaglioStanziamentoPedaggioEntity(dsPedaggioEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsPedaggioEntity, dsp);
      dsp.getStanziamenti()
         .addAll(dsPedaggioEntity.getStanziamenti());
      dsp.clearId();
      ds = dsp;
    } else if (_dettaglioEntity instanceof DettaglioStanziamentoGenericoEntity) {
      DettaglioStanziamentoGenericoEntity dsGenericoEntity = (DettaglioStanziamentoGenericoEntity) _dettaglioEntity;
      DettaglioStanziamentoGenericoEntity dsg = new DettaglioStanziamentoGenericoEntity(dsGenericoEntity.getGlobalIdentifier());
      BeanUtils.copyProperties(dsGenericoEntity, dsg);
      dsg.getStanziamenti()
         .addAll(dsGenericoEntity.getStanziamenti());
      dsg.clearId();
      ds = dsg;
    }

    _log.debug("Cloned {} (Old ID: {} - New ID: {})", _dettaglioEntity.getClass()
                                                                     .getSimpleName(),
              idOldEntity, ds.getId());
    return ds;
  }

  @Override
  public DettaglioStanziamento clone(@NotNull DettaglioStanziamento _allocationDetail) {

    DettaglioStanziamento allocationDetail = null;

    final var globalIdentifier = _allocationDetail.getGlobalIdentifier();

    _log.debug("Cloning {} with gId {}", _allocationDetail.getClass()
                                                         .getSimpleName(),
              globalIdentifier);

    // findTemp si riferisce alla ricerca dei dettagli stanziamento provvisori.
    final var optionalAllocationDetail = findTempByGlobalIdentifierId(globalIdentifier);

    final var entity = dettaglioStanziamentoEntityMapper.toEntity(optionalAllocationDetail.orElseThrow())
                                                        .clearId();
    em.persist(entity);

    allocationDetail = dettaglioStanziamentoEntityMapper.toDomain(entity);

    _log.debug("Cloned {} (old gId {} - new gId {})", _allocationDetail.getClass()
                                                                      .getSimpleName(),
              globalIdentifier, allocationDetail.getGlobalIdentifier());

    return allocationDetail;
  }

  @Override
  public DettaglioStanziamento cloneAndChangeSign(@NotNull DettaglioStanziamento _allocationDetail) {

    DettaglioStanziamento allocationDetail = null;

    final var globalIdentifier = _allocationDetail.getGlobalIdentifier();

    _log.debug("Cloning and changing amount sign {} with gId {}", _allocationDetail.getClass()
                                                                                  .getSimpleName(),
              globalIdentifier);

    final var optionalAllocationDetail = findLastByGlobalIdentifierId(globalIdentifier);

    final var entity = dettaglioStanziamentoEntityMapper.toEntity(optionalAllocationDetail.orElseThrow())
                                                        .clearId();
    em.persist(entity.negateAmount());

    allocationDetail = dettaglioStanziamentoEntityMapper.toDomain(entity);

    _log.debug("Cloned and changed amlunt sign {} (old gId {} - new gId {})", _allocationDetail.getClass()
                                                                                              .getSimpleName(),
              globalIdentifier, allocationDetail.getGlobalIdentifier());

    return allocationDetail;
  }

  @Override
  public List<DettaglioStanziamentoEntity> getAllDettaglioStanziamentoCarburante() {
    final var criteriaBuilder = em.getCriteriaBuilder();
    final var query = criteriaBuilder.createQuery(DettaglioStanziamentoCarburanteEntity.class);
    final Root<DettaglioStanziamentoCarburanteEntity> root = query.from(DettaglioStanziamentoCarburanteEntity.class);
    query.select(root);

    List<DettaglioStanziamentoEntity> allStanziamenti = null;
    try {
      allStanziamenti = em.createQuery(query)
                          .getResultStream()
                          .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      allStanziamenti = new ArrayList<>();
      _log.warn(" No {} found", StanziamentoEntity.class.getSimpleName());
    }

    return allStanziamenti;
  }

  @Override
  public List<DettaglioStanziamentoEntity> getAllDettaglioStanziamentoPedaggio() {
    final var criteriaBuilder = em.getCriteriaBuilder();
    final var query = criteriaBuilder.createQuery(DettaglioStanziamentoPedaggioEntity.class);
    final Root<DettaglioStanziamentoPedaggioEntity> root = query.from(DettaglioStanziamentoPedaggioEntity.class);
    query.select(root);

    List<DettaglioStanziamentoEntity> allStanziamenti = null;
    try {
      allStanziamenti = em.createQuery(query)
                          .getResultStream()
                          .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      allStanziamenti = new ArrayList<>();
      _log.warn(" No {} found", StanziamentoEntity.class.getSimpleName());
    }

    return allStanziamenti;
  }

  @Override
  public List<DettaglioStanziamentoEntity> getAllDettaglioStanziamentoGenerico() {
    final var criteriaBuilder = em.getCriteriaBuilder();
    final var query = criteriaBuilder.createQuery(DettaglioStanziamentoGenericoEntity.class);
    final Root<DettaglioStanziamentoGenericoEntity> root = query.from(DettaglioStanziamentoGenericoEntity.class);
    query.select(root);

    List<DettaglioStanziamentoEntity> allStanziamenti = null;
    try {
      allStanziamenti = em.createQuery(query)
                          .getResultStream()
                          .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      allStanziamenti = new ArrayList<>();
      _log.warn(" No {} found", StanziamentoEntity.class.getSimpleName());
    }

    return allStanziamenti;
  }

  @Override
  public DettaglioStanziamento mapToDomain(DettaglioStanziamentoEntity dsEntity) {
    return dettaglioStanziamentoEntityMapper.toDomain(dsEntity);
  }

  @Override
  public DettaglioStanziamentoEntity mapToEntity(DettaglioStanziamento domain) {
    return dettaglioStanziamentoEntityMapper.toEntity(domain);
  }

  @Override
  @Deprecated // sostituito da vista con counter
  public boolean existByDeviceAndDateAndSignAndInvoiceType(DettaglioStanziamento _dettaglioStanziamento) {

    // Device device, Instant date, Transaction transaction, InvoiceType invoiceType
    final var criteriaBuilder = em.getCriteriaBuilder();
    final var query = criteriaBuilder.createQuery(Long.class);

    Root<? extends DettaglioStanziamentoEntity> root = null;
    List<Predicate> predicates = null;

    if (_dettaglioStanziamento instanceof DettaglioStanziamentoCarburante) {
      root = query.from(DettaglioStanziamentoCarburanteEntity.class);
      var dettaglioStanziamento = (DettaglioStanziamentoCarburante) _dettaglioStanziamento;
      predicates = List.of(criteriaBuilder.equal(root.get(DettaglioStanziamentoCarburanteEntity_.CODICE_TRACKYCARD),
                                                 dettaglioStanziamento.getCodiceTrackycard()),
                           criteriaBuilder.equal(root.get(DettaglioStanziamentoCarburanteEntity_.DATA_ORA_UTILIZZO),
                                                 dettaglioStanziamento.getEntryDateTime()),
                           criteriaBuilder.equal(root.get(DettaglioStanziamentoCarburanteEntity_.TIPO_CONSUMO),
                                                 dettaglioStanziamento.getInvoiceType()));
      if (_log.isDebugEnabled()) {
        _log.trace(" Searching {} by codiceTrackycard,EntryDateTime,InvoiceType(  : {}", Stanziamento.class.getSimpleName(),
                   dettaglioStanziamento.getCodiceTrackycard(), dettaglioStanziamento.getEntryDateTime(),
                   dettaglioStanziamento.getInvoiceType());
      }
    } else {
      _log.error("Method not implemented for class {}", _dettaglioStanziamento.getClass());
      return false;
    }

    query.select(criteriaBuilder.count(root));
    query.where(predicates.toArray(new Predicate[0]));
    return em.createQuery(query)
             .getSingleResult() > 0;
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true, propagation = Propagation.NOT_SUPPORTED)
  public List<UUID> findDettagliStanziamentoIdByCodiciStanziamento(List<String> _codiciStanziamento) {
    if (_codiciStanziamento == null || _codiciStanziamento.isEmpty()) {
      return new ArrayList<>();
    }
    
    List<UUID> rs = new ArrayList<>(_codiciStanziamento.size());
	List<List<String>> splittedData = SqlUtil.splitToSubList(_codiciStanziamento, 10000);

	for (List<String> codiciStanziamento : splittedData) {
		
    
    // @formatter:off
    String queryString = "" 
          + "SELECT "
          + " dettaglio_stanziamenti_id " 
          + " FROM dettagliostanziamento_stanziamento "
          + " WHERE stanziamenti_codice_stanziamento " 
          + " IN (§params§) ;";
    // @formatter:on
    String params = codiciStanziamento.stream()
                                      .map(s -> "'" + s + "'")
                                      .collect(Collectors.joining(","));
    queryString = queryString.replace("§params§", params);
    javax.persistence.Query nativeQuery = em.createNativeQuery(queryString);

    @SuppressWarnings("unchecked")
    List<UUID> resultList = (List<UUID>) nativeQuery.getResultList();
    rs.addAll(resultList);
  }
    
    return rs;
  }

}
