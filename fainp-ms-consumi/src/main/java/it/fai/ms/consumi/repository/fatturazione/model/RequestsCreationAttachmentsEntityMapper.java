package it.fai.ms.consumi.repository.fatturazione.model;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;

@Component
@Validated
public class RequestsCreationAttachmentsEntityMapper {

  public RequestsCreationAttachmentsEntityMapper() {
  }

  public RequestsCreationAttachments toDomain(final RequestsCreationAttachmentsEntity _entity) {
    if (_entity == null) {
      return null;
    }
    RequestsCreationAttachments domain = new RequestsCreationAttachments();
    domain.codiceFattura(_entity.getCodiceFattura())
          .dataFattura(_entity.getDataFattura())
          .codiceRaggruppamentoArticolo(_entity.getCodiceRaggruppamentoArticolo())
          .descrRaggruppamentoArticolo(_entity.getDescrRaggruppamentoArticolo())
          .nomeIntestazioneTessere(_entity.getNomeIntestazioneTessere())
          .uuidDocumento(_entity.getUuidDocumento())
          .importoTotale(_entity.getImportoTotale())
          .lang(_entity.getLang())
          .data(_entity.getData())
          .tipoServizio(_entity.getTipoServizio())
          .nazioneFatturazione(_entity.getNazioneFatturazione());
    domain.setId(_entity.getId());
    return domain;
  }

  public RequestsCreationAttachmentsEntity toEntity(final RequestsCreationAttachments _domain) {
    if (_domain == null) {
      return null;
    }
    RequestsCreationAttachmentsEntity entity = new RequestsCreationAttachmentsEntity();
    entity.codiceFattura(_domain.getCodiceFattura())
          .dataFattura(_domain.getDataFattura())
          .codiceRaggruppamentoArticolo(_domain.getCodiceRaggruppamentoArticolo())
          .descrRaggruppamentoArticolo(_domain.getDescrRaggruppamentoArticolo())
          .nomeIntestazioneTessere(_domain.getNomeIntestazioneTessere())
          .uuidDocumento(_domain.getUuidDocumento())
          .importoTotale(_domain.getImportoTotale())
          .lang(_domain.getLang())
          .data(_domain.getData())
          .tipoServizio(_domain.getTipoServizio())
          .nazioneFatturazione(_domain.getNazioneFatturazione());
    entity.setId(_domain.getId());
    return entity;
  }

}
