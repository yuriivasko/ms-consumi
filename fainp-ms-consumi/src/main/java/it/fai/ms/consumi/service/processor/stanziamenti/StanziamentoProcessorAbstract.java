package it.fai.ms.consumi.service.processor.stanziamenti;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;

public abstract class StanziamentoProcessorAbstract implements StanziamentoProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final StanziamentoMapper     mapper;
  private final StanziamentoRepository stanziamentiRepository;

  public StanziamentoProcessorAbstract(final StanziamentoRepository _stanziamentiRepository, final StanziamentoMapper _mapper) {
    stanziamentiRepository = _stanziamentiRepository;
    mapper = _mapper;
  }

  protected StanziamentoMapper getMapper() {
    return mapper;
  }
  
  protected Optional<Stanziamento> findAllocationRelatedProvvisorio(final Stanziamento _allocationLast, StanziamentiDetailsType stanziamentiDetailsType){
    if (_allocationLast.getInvoiceType() == InvoiceType.D && _allocationLast.getCodiceStanziamentoProvvisorio() != null) {
      Stanziamento stanziamentoProvvisorio = stanziamentiRepository.findByCodiceStanziamento(_allocationLast.getCodiceStanziamentoProvvisorio(),stanziamentiDetailsType);
      if (stanziamentoProvvisorio==null) {
        _log.warn("Attention! Stantiamento {} has a related Provvisorio but its code is not existing???");
      }
      return Optional.ofNullable(stanziamentoProvvisorio);
    }
    return Optional.empty();
  }
  
  protected Optional<Stanziamento> findAllocationRelatedProvvisorio(final Stanziamento _allocationLast){
    if (_allocationLast.getInvoiceType() == InvoiceType.D && _allocationLast.getCodiceStanziamentoProvvisorio() != null) {
      Stanziamento stanziamentoProvvisorio = stanziamentiRepository.findByCodiceStanziamento(_allocationLast.getCodiceStanziamentoProvvisorio());
      if (stanziamentoProvvisorio==null) {
        _log.warn("Attention! Stantiamento {} has a related Provvisorio but its code is not existing???");
      }
      return Optional.ofNullable(stanziamentoProvvisorio);
    }
    return Optional.empty();
  }
  
  protected Optional<Stanziamento> findAllocationNotSentToNavOnRepository(StanziamentoLogicKey logicKey) {
    return findAllocationOnRepository(true, logicKey);
  }
  
  protected Optional<Stanziamento> findAllocationNotSentToNavOnRepository(StanziamentoLogicKey logicKey, StanziamentiDetailsType stanziamentiDetailsType) {
    return findAllocationOnRepository(true, logicKey, stanziamentiDetailsType);
  }
  
  protected Optional<Stanziamento> findAllocationSentOrNotToNavOnRepository(StanziamentoLogicKey logicKey) {
    return findAllocationOnRepository(false, logicKey);
  }
  
  private Optional<Stanziamento> findAllocationOnRepository(boolean filterNotAlreadySentToNav, StanziamentoLogicKey logicKey, StanziamentiDetailsType stanziamentiDetailsType) {

//  quando dovrebbe trovare lo stanziamento da collegare al precedente cerca per Definitivo ma sul DB c'è solo il provvisorio

  final var stanziamentoLogicKey = logicKey;
  final var persistedAllocations = stanziamentiRepository.findStanziamentoByLogicKey(stanziamentoLogicKey,filterNotAlreadySentToNav, stanziamentiDetailsType );

  Optional<Stanziamento> persistedAllocation = null;

  if (persistedAllocations.isEmpty()) {
    persistedAllocation = Optional.empty();
    _log.debug(" No stanziamento {} for logic key {}", (filterNotAlreadySentToNav)?"not already sent to Nav":"",stanziamentoLogicKey);
  } else {
    if (persistedAllocations.size() == 1) {
      persistedAllocation = persistedAllocations.stream()
                                                .findFirst();
      
      if(_log.isTraceEnabled()) {
        _log.trace(" Found {} for logic key {} {}", persistedAllocation, stanziamentoLogicKey, (filterNotAlreadySentToNav)?"not already sent to Nav":"");
      }
  } else {
      persistedAllocation = persistedAllocations.stream()
                                                .findFirst();
      _log.warn(" --->>> More than 1 stanziamento {} for logic key {}",(filterNotAlreadySentToNav)?"not already sent to Nav":"", stanziamentoLogicKey);
      persistedAllocations.forEach(stanziamento -> _log.warn(" --->>> {}", stanziamento));
    }
  }

  return persistedAllocation;
}

  private Optional<Stanziamento> findAllocationOnRepository(boolean filterNotAlreadySentToNav, StanziamentoLogicKey logicKey) {

//    quando dovrebbe trovare lo stanziamento da collegare al precedente cerca per Definitivo ma sul DB c'è solo il provvisorio

    final var stanziamentoLogicKey = logicKey;
    final var persistedAllocations = stanziamentiRepository.findStanziamentoByLogicKey(stanziamentoLogicKey,filterNotAlreadySentToNav);

    Optional<Stanziamento> persistedAllocation = null;

    if (persistedAllocations.isEmpty()) {
      persistedAllocation = Optional.empty();
      _log.debug(" No stanziamento {} for logic key {}", (filterNotAlreadySentToNav)?"not already sent to Nav":"",stanziamentoLogicKey);
    } else {
      if (persistedAllocations.size() == 1) {
        persistedAllocation = persistedAllocations.stream()
                                                  .findFirst();
        
        if(_log.isTraceEnabled()) {
          _log.trace(" Found {} for logic key {} {}", persistedAllocation, stanziamentoLogicKey, (filterNotAlreadySentToNav)?"not already sent to Nav":"");
        }
    } else {
        persistedAllocation = persistedAllocations.stream()
                                                  .findFirst();
        _log.warn(" --->>> More than 1 stanziamento {} for logic key {}",(filterNotAlreadySentToNav)?"not already sent to Nav":"", stanziamentoLogicKey);
        persistedAllocations.forEach(stanziamento -> _log.warn(" --->>> {}", stanziamento));
      }
    }

    return persistedAllocation;
  }

  protected abstract Stanziamento allocationHasToBeCreated(DettaglioStanziamento _allocationDetail, StanziamentiParams _params);

  protected abstract Stanziamento allocationHasToBeUpdated(DettaglioStanziamento _allocationDetail, Stanziamento _allocation);

}
