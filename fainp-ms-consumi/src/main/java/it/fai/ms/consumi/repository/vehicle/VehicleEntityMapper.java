package it.fai.ms.consumi.repository.vehicle;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

@Component
@Validated
public class VehicleEntityMapper {

  public VehicleEntityMapper() {
  }

  public Vehicle toDomain(@NotNull final ViewStoricoDispositivoVeicoloContratto _storicoStatoDispositivo) {
    final var vehicle = new Vehicle();
    vehicle.setEuroClass(_storicoStatoDispositivo.getVeicoloClasseEuro());
    vehicle.setLicensePlate(new VehicleLicensePlate(_storicoStatoDispositivo.getVeicoloTarga(),
                                                    _storicoStatoDispositivo.getVeicoloNazione()));
    return vehicle;
  }
  
  public Vehicle toDomain(@NotNull final StoricoVeicolo _storicoVeicolo) {
    final var vehicle = new Vehicle();
    vehicle.setEuroClass(_storicoVeicolo.getClasseEuro());
    vehicle.setLicensePlate(new VehicleLicensePlate(_storicoVeicolo.getTarga(),
                                                    _storicoVeicolo.getNazione()));
    return vehicle;
  }

}
