package it.fai.ms.consumi.repository.storico_dml;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;

@Repository
public interface ViewStoricoDispositivoVeicoloContrattoRepository extends JpaRepository<ViewStoricoDispositivoVeicoloContratto, String> {

  Optional<ViewStoricoDispositivoVeicoloContratto> findFirstBySerialeDispositivoAndTipoDispositivoAndDataVariazioneBeforeOrderByDataVariazioneDesc(@NotNull String _deviceSeriale,
                                                                                                                               @NotNull TipoDispositivoEnum _deviceType,
                                                                                                                               @NotNull Instant _date);

}
