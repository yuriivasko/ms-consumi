package it.fai.ms.consumi.service.processor.stanziamenti;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Optional;

import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.Vehicle;

public class StanziamentoLogicKey {

  private String      articleCode;
  private String      countryId;
  private String      customerId;
  private LocalDate   date;
  private InvoiceType invoiceType;
  private String      licenseId;
  private String      supplierCode;

  public StanziamentoLogicKey(final Article _article, final Supplier _supplier, final Customer _customer, final InvoiceType _invoiceType,
                              final Instant _date, final Vehicle vehicle) {

    articleCode = Objects.requireNonNull(Optional.ofNullable(_article)
                                                 .orElseThrow(() -> new IllegalStateException("Article is mandatory"))
                                                 .getCode(),
                                         "Article code is mandatory");
    countryId = Objects.requireNonNull(Optional.ofNullable(_article)
                                               .orElseThrow(() -> new IllegalStateException("Article is mandatory"))
                                               .getCountry(),
                                       "Article country is mandatory");
    supplierCode = Objects.requireNonNull(Optional.ofNullable(_supplier)
                                                  .orElseThrow(() -> new IllegalStateException("Supplier is mandatory"))
                                                  .getCode(),
                                          "Supplier code is mandatory");
    customerId = Objects.requireNonNull(Optional.ofNullable(_customer)
                                                .orElseThrow(() -> new IllegalStateException("Customer is mandatory"))
                                                .getId(),
                                        "Customer id is mandatory");
    invoiceType = Objects.requireNonNull(Optional.ofNullable(_invoiceType)
                                                 .orElseThrow(() -> new IllegalStateException("InvoiceType is mandatory")),
                                         "Customer id is mandatory");
    date = LocalDate.ofInstant(Objects.requireNonNull(_date, "Date is mandatory"),
                               ZoneId.systemDefault());
    licenseId = Optional.ofNullable(Optional.ofNullable(vehicle)
                                            .orElseGet(null)
                                            .getLicensePlate())
                        .orElseGet(null)
                        .getLicenseId();
  }

  public StanziamentoLogicKey(String articleCode, String countryId, String supplierCode, String customerId, InvoiceType invoiceType, LocalDate date, String licenceId){
    this.articleCode = articleCode;
    this.countryId = countryId;
    this.supplierCode = supplierCode;
    this.customerId = customerId;
    this.invoiceType = invoiceType;
    this.date = date;
    this.licenseId = licenceId;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final StanziamentoLogicKey obj = getClass().cast(_obj);
      res = Objects.equals(obj.articleCode, articleCode) && Objects.equals(obj.countryId, countryId)
            && Objects.equals(obj.customerId, customerId) && Objects.equals(obj.date, date) && Objects.equals(obj.invoiceType, invoiceType)
            && Objects.equals(obj.licenseId, licenseId) && Objects.equals(obj.supplierCode, supplierCode);
    }
    return res;
  }

  public String getArticleCode() {
    return articleCode;
  }

  public String getCountryId() {
    return countryId;
  }

  public String getCustomerId() {
    return customerId;
  }

  public LocalDate getDate() {
    return date;
  }

  public InvoiceType getInvoiceType() {
    return invoiceType;
  }

  public String getLicenseId() {
    return licenseId;
  }

  public String getSupplierCode() {
    return supplierCode;
  }

  @Override
  public int hashCode() {
    return Objects.hash(articleCode, countryId, customerId, date, invoiceType, licenseId, supplierCode);
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("articleCode=")
                              .append(articleCode)
                              .append(",countryId=")
                              .append(countryId)
                              .append(",licenseId=")
                              .append(licenseId)
                              .append(",customerId=")
                              .append(customerId)
                              .append(",date=")
                              .append(date)
                              .append(",invoiceType=")
                              .append(invoiceType)
                              .append(",supplierCode=")
                              .append(supplierCode)
                              .append("]")
                              .toString();
  }

}
