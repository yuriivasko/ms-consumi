package it.fai.ms.consumi.service.record.tunnel;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.FrejusGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.FrejusPrenotazioni;
import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.FrejusRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;
import it.fai.ms.consumi.service.frejus.FrejusCustomerDataContainer;
import it.fai.ms.consumi.service.frejus.FrejusCustomerProvider;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class FrejusRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<FrejusRecord, Frejus> {

  private List<String>           tessereJolly;
  private FrejusCustomerProvider customerProvider;

  public FrejusRecordConsumoMapper(ApplicationProperties properties, FrejusCustomerProvider customerProvider) {
    this.customerProvider = customerProvider;
    this.tessereJolly = properties.getFrejusJobProperties()
                                  .getTessereJolly();
  }

  private final static class ToolPointData {
    String enterCode;
    String enterDesc;
    String exitCode;
    String exitDesc;

    public ToolPointData(String enterCode, String enterDesc, String exitCode, String exitDesc) {
      this.enterCode = enterCode;
      this.enterDesc = enterDesc;
      this.exitCode = exitCode;
      this.exitDesc = exitDesc;
    }
  }

  final static ToolPointData[] toolPoints                 = { new ToolPointData("25053391", "Francia", "25002999", "vs Francia"),                   // Francia->Italia
                                                              new ToolPointData("38032193", "Italia", "38032193", "vs Italia")                     // Italia->Francia
  };
  Map<String, String>          codCliTransactionDetailMap = Map.of("000867", "MBIT", "010268", "MBFR", "001036", "FJIT", "009010", "FJFR");

  private static final Logger log = LoggerFactory.getLogger(FrejusRecordConsumoMapper.class);

  @Override
  public Frejus mapRecordToConsumo(FrejusRecord commonRecord) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyyMMdd_dateformat, HHmm_timeformat);
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, "SITAF");

    source.setFileName(commonRecord.getFileName());
    source.setRowNumber(commonRecord.getRowNumber());
    Frejus toConsumo;
    Optional<Instant> dataTransazione = calculateInstantByDateTime(commonRecord.getDataTransazione(), yyyyMMddHHmmss_timestampformat);
    String transactionDetailCode = extractTransactionDetailCode(commonRecord.getParentRecord());
    String seriale = commonRecord.getNumeroTessera();
    TipoDispositivoEnum deviceType = TipoDispositivoEnum.TES_TRA_FREJUS_MONTE_BIANCO;
    TipoServizioEnum serviceType = TipoServizioEnum.PEDAGGI_TRAFORI_FREJUS_MONTE_BIANCO;

    boolean isMontebianco = transactionDetailCode.startsWith("MB");
    boolean isItalia = transactionDetailCode.endsWith("IT");
    if(!isMontebianco) {
      if (isItalia && isBuonoItalia(seriale)) {
        seriale = getBarCodeBuonoItalia(commonRecord);
        deviceType = TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_IT;
      } else if (!isItalia && isBuonoFrancia(seriale)) {
        seriale = getBarCodeBuonoFrancia(commonRecord);
        deviceType = TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_FR;
      }
    }
    if (isMontebianco && isPrenotazione(commonRecord)) {
      // prenotazioni
      FrejusPrenotazioni prenotazione = new FrejusPrenotazioni(source, commonRecord.getRecordCode(),
                                                               Optional.of(new FrejusGlobalIdentifier(commonRecord.getGlobalIdentifier()))
        , commonRecord);

      customerProvider.setData(prenotazione, seriale, dataTransazione, (FrejusCustomerDataContainer) commonRecord.getParentRecord());

      toConsumo = prenotazione;
    } else {
      toConsumo = new Frejus(source, commonRecord.getRecordCode(),
                             Optional.of(new FrejusGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);

      // CONSUMOPEDAGGIO.DEVICE


      Device device = new Device(seriale, deviceType);
//      device.setPan(pan);

      device.setServiceType(serviceType);
      toConsumo.setDevice(device);
    }

    toConsumo.setDirection(commonRecord.getSenso());

    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();

    transaction.setDetailCode(transactionDetailCode);
    toConsumo.setTransaction(transaction);

    // CONSUMO.VEICOLO
    Vehicle vehicle = toConsumo.getVehicle();
    if (vehicle == null)
      vehicle = new Vehicle();

    vehicle.setFareClass(commonRecord.getClassePedaggio());
    vehicle.setEuroClass(commonRecord.getClassificaECO());
    toConsumo.setVehicle(vehicle);

    // CONSUMO.AMOUNT
    Amount amount = new Amount();
    var importo = MonetaryUtils.strToMonetary(commonRecord.getImporto(), -2, commonRecord.getDefaultCurrency());
    amount.setVatRate(NumericUtils.strToBigDecimal(commonRecord.getPercIva(), 2, 100));
    amount.setAmountIncludedVat(importo);
    toConsumo.setAmount(amount);

    transaction.setSign(commonRecord.getSegnoImporto());

    // tipo definitivo settato di default nel implementazione del consumo

    Optional<ToolPointData> toolPoint;
    String senso = commonRecord.getSenso();
    if (senso != null) {
      switch (senso) {
      case "1":
        toolPoint = Optional.of(toolPoints[0]);
        break;
      case "2":
        toolPoint = Optional.of(toolPoints[1]);
        break;
      default:
        toolPoint = Optional.empty();
        log.warn("senso \"{}\" non supportato" + commonRecord.getSenso());
        break;
      }
    } else {
      toolPoint = Optional.empty();
    }

    TollPoint tollpointExit = new TollPoint();

    toolPoint.ifPresent(point -> {
      // CONSUMOPEDAGGIO.TOLLPOINTENTER
      TollPoint tollpointEnter = new TollPoint();
      GlobalGate ggateEnter = new GlobalGate(point.enterCode);
      ggateEnter.setDescription(point.enterDesc);
      tollpointEnter.setGlobalGate(ggateEnter);

      toConsumo.setTollPointEntry(tollpointEnter);

      // CONSUMOPEDAGGIO.TOLLPOINTEXIT

      GlobalGate ggateExit = new GlobalGate(point.exitCode);
      ggateExit.setDescription(point.exitDesc);
      tollpointExit.setGlobalGate(ggateExit);

    });

    dataTransazione.ifPresent(tollpointExit::setTime);
    toConsumo.setTollPointExit(tollpointExit);

    ConsumoPedaggio toConsumoPedaggio = toConsumo;
    toConsumoPedaggio.setProcessed(commonRecord.isProcessed());

    String importoPericoloseNoVat = commonRecord.getImportoPericoloseNoVat();
    if (commonRecord.getSegnoImportoPericolose() != null)
      importoPericoloseNoVat = commonRecord.getSegnoImportoPericolose() + commonRecord.getImportoPericoloseNoVat();
    toConsumo.setImponibileAdr(NumericUtils.strToBigDecimal(importoPericoloseNoVat, 2, 100));

    return toConsumo;
  }

  private boolean isPrenotazione(FrejusRecord commonRecord) {
    return tessereJolly.stream()
                       .anyMatch(tj -> tj.toLowerCase()
                                         .equals(commonRecord.getNumeroTessera()
                                                             .toLowerCase()));
  }

  private String getBarCodeBuonoFrancia(FrejusRecord commonRecord) {
    return commonRecord.getNumeroBuonoFrancia();
  }

  private String getBarCodeBuonoItalia(FrejusRecord commonRecord) {
    return commonRecord.getNumeroTessera()
                       .trim();
  }

  // private boolean isBuono(boolean isItalia, FrejusRecord commonRecord) {
  // // numero tessera parte da pos 2
  // String numeroTessera = commonRecord.getNumeroTessera();
  // if (isItalia) {
  // return isBuonoItalia(numeroTessera);
  // } else {
  // return isBuonoFrancia(numeroTessera);
  // }
  // }

  private boolean isBuonoFrancia(String numeroTessera) {
    // nella posizione 8 c’e’ il valore 5 ( 4 per le tessere - come da tracciato Sitaf)
    return numeroTessera != null && numeroTessera.length() >= 7 && "5".equals(numeroTessera.substring(6, 7));
  }

  private boolean isBuonoItalia(String numeroTessera) {
    // dalla posizione 14 per 7 caratteri ci sono blank
    return numeroTessera.trim()
                        .length() < 15;
  }

  private String extractTransactionDetailCode(BaseRecord header) {
    FrejusRecord frejusRecord = (FrejusRecord) header;
    var codCli = frejusRecord.getNumeroCliente();
    if (codCliTransactionDetailMap.containsKey(codCli)) {
      return codCliTransactionDetailMap.get(codCli);
    } else
      log.warn("numero cliente ({}) non supportato per i tracciati Frejus nel file {}", codCli, frejusRecord.getFileName());
    return null;
  }


}
