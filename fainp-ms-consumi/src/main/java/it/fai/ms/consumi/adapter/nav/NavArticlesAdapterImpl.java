package it.fai.ms.consumi.adapter.nav;

import java.math.RoundingMode;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.client.AnagaziendeService;
import it.fai.ms.consumi.domain.Article;
import liquibase.util.StringUtils;

@Service
public class NavArticlesAdapterImpl implements NavArticlesAdapter {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final AnagaziendeService anagaziendeService;

  public NavArticlesAdapterImpl(AnagaziendeService anagaziendeService) {
    this.anagaziendeService = anagaziendeService;
  }

  @Override
  public List<Article> getAllArticles() {
    final List<Article> articles = anagaziendeService.getAllArticoli().stream().map(
      articoloDTO -> {
        Article article = new Article(articoloDTO.getNo());

        final String description1 = StringUtils.trimToEmpty(articoloDTO.getDescription());
        final String description2 = StringUtils.trimToEmpty(articoloDTO.getDescription2());
        StringBuilder sb = new StringBuilder();
        sb.append(description1);
        if (StringUtils.isNotEmpty(description2)){
          sb.append(" " +description2);
        }
        article.setDescription(sb.toString());

        article.setCountry(articoloDTO.getCountryRegionOfOriginCode());
        if(StringUtils.isNotEmpty(articoloDTO.getValuta())){
          article.setCurrency(Currency.getInstance(articoloDTO.getValuta()));
        }
        article.setGrouping(articoloDTO.getRaggruppamentoArticolo());
        article.setMeasurementUnit(articoloDTO.getBaseUnitOfMeasure());
        article.setVatRate(articoloDTO.getPercentualeIva().setScale(2, RoundingMode.HALF_UP).floatValue());
        return article;
      }
    ).collect(Collectors.toList());

    _log.info("Called nav to get all articles [total items {}]", articles.size());

    return articles;
  }

}
