package it.fai.ms.consumi.service;

import java.util.List;
import java.util.UUID;

import it.fai.ms.common.jms.fattura.CarburantiBodyDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoCarburantiSezione2Entity;

public interface ViewAllegatiCarburantiService {

  List<ViewAllegatoCarburantiSezione2Entity> getDettagliStanziamento(List<UUID> dettagliStanziamentoId);

  List<CarburantiBodyDTO> createBodyMessageAndSection2(List<ViewAllegatoCarburantiSezione2Entity> viewAllegatiCarburante,
                                                       final RequestsCreationAttachments requestCreationAttachments);

}
