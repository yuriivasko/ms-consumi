package it.fai.ms.consumi.job;

import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.service.util.FaiConsumiFilesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;

@Component
public class ServiceProviderScheduledTask {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final JobRegistry jobRegistry;
  private String                                                            basePath;

  @Inject
  public ServiceProviderScheduledTask(final JobRegistry jobRegistry,
                                      @Value("${consumiFiles.path}") final String _path) {
    this.jobRegistry = jobRegistry;
    this.basePath = _path;
  }


  public void run(final ServiceProvider _serviceProvider) throws IOException {
	  
	synchronized (ServiceProviderScheduledTask.class) {

	    log.debug("[{}] , Runnable Task with ServiceProvider {} on thread {}", Instant.now(), _serviceProvider,
	               Thread.currentThread().getName());
	
	    Path fullPath = FaiConsumiFilesUtil.getAbsoluteOrRelativePath(basePath);
	
	    if (_serviceProvider.getFilePath() == null || _serviceProvider.getFilePath().isEmpty()) {
	      log.error("serviceProvider.FilePath cannot be empty: {}", _serviceProvider);
	      return;
	    }
	    if (_serviceProvider.getFileMatchRegex() == null || _serviceProvider.getFileMatchRegex().isEmpty()) {
	      log.error("serviceProvider.FileMatchRegex cannot be empty: {}", _serviceProvider);
	      return;
	    }
	
	    fullPath = fullPath.resolve(_serviceProvider.getFilePath());
	
	    try {
	      File f = new File(fullPath.toAbsolutePath().toString());
	      f.mkdirs();
	    } catch (Exception e) {
	      log.error("{}", e);
	    }
	
	    log.debug(" Full path for reading : {}", fullPath);
	
	    DirectoryStream<Path> dirStream = null;
	
	    try {
	      dirStream = Files.newDirectoryStream(fullPath, path -> path.toFile().isFile() && 
	                                           FaiConsumiFilesUtil.isNotAlreadyProcessed(path) && 
	                                           FaiConsumiFilesUtil.isNotProcessing(path) && 
	                                           path.getFileName().toFile().getName().matches( _serviceProvider.getFileMatchRegex()));
	
	      var optionalJob = jobRegistry.findJobByServiceProviderFormat(_serviceProvider.getFormat());
	
	      if (optionalJob.isPresent()) {
	        final AbstractJob<? extends CommonRecord, ? extends Consumo> job = optionalJob.get();
	        List<Path> orderedFileList = job.orderFileList(dirStream);
	
	        orderedFileList.forEach(file -> {
	
	          log.info("File found {}", file);
	          String fileName = FaiConsumiFilesUtil.resolveFileName(file);
	          String fileNameProcessing = FaiConsumiFilesUtil.renameToProcessing(fileName);
	
	          ServicePartner servicePartner = _serviceProvider.toServicePartner();
	          job.process(fileNameProcessing, System.currentTimeMillis(), Instant.now(), servicePartner);
	
	          FaiConsumiFilesUtil.renameToProcessed(fileName);
	        });
	      } else {
	        log.error("JOB {} not found", _serviceProvider.getFormat());
	      }
	    } catch (Exception e) {
	      log.error("Errore nel processare " + fullPath, e);
	    } finally {
	      if (dirStream != null)
	        dirStream.close();
	    }
	    log.info("Finish job {} for partner {} and path {}", _serviceProvider.getFormat(), _serviceProvider.getProviderCode(), fullPath);
	  }
  }


}
