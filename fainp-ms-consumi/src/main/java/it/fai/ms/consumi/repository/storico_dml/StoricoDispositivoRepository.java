package it.fai.ms.consumi.repository.storico_dml;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.consumi.repository.storico_dml.model.DmlEmbeddedId;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;

@Repository
public interface StoricoDispositivoRepository extends JpaRepository<StoricoDispositivo, DmlEmbeddedId>,InterfaceDmlRepository<StoricoDispositivo> {

  @Query("select distinct d from StoricoDispositivo d, StoricoContratto c " 
      + " where c.contrattoNumero = d.contrattoDmlUniqueIdentifier" 
      + " and c.codiceAzienda = :code")
  List<StoricoDispositivo> findAllByCustomerCode(@Param("code") String code);

}
