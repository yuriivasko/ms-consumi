package it.fai.ms.consumi.service.processor.consumi.trackycardcarbjolly;

import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd.TrackyCardCarbStdProcessor;

@Service
public class TrackyCardCarbJollyProcessorImpl implements TrackyCardCarbJollyProcessor {

  TrackyCardCarbStdProcessor trackyCardCarbStdProcessor;

  public TrackyCardCarbJollyProcessorImpl(TrackyCardCarbStdProcessor trackyCardCarbStdProcessor) {
    this.trackyCardCarbStdProcessor = trackyCardCarbStdProcessor;
  }

  @Override
  public ProcessingResult validateAndProcess(TrackyCardCarburantiJolly consumo, Bucket bucket) {
    return trackyCardCarbStdProcessor.validateAndProcess(consumo, bucket);
  }
}
