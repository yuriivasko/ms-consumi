package it.fai.ms.consumi.repository.fatturazione;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;

@Repository
@Validated
@Transactional
public class AllegatiConfigurationRepositoryImpl implements AllegatiConfigurationRepository {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final EntityManager                        em;
  private CriteriaBuilder                            cb;
  private CriteriaQuery<AllegatiConfigurationEntity> cq;

  public AllegatiConfigurationRepositoryImpl(final EntityManager _em) {
    em = _em;
  }

  @Override
  public AllegatiConfigurationEntity save(@NotNull AllegatiConfigurationEntity allegatiConfig) {
    if (allegatiConfig != null) {
      AllegatiConfigurationEntity allegatiConfigOnDb = findByCodiceRaggruppamentoArticoli(allegatiConfig.getCodiceRaggruppamentoArticolo());
      if (allegatiConfigOnDb == null) {
        em.persist(allegatiConfig);
      } else {
        allegatiConfig = em.merge(allegatiConfig);
      }
    }
    return allegatiConfig;
  }

  @Override
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true, propagation = Propagation.NOT_SUPPORTED)
  public AllegatiConfigurationEntity findByCodiceRaggruppamentoArticoli(@NotNull String codiceRaggrArticoli) {

    AllegatiConfigurationEntity result = null;
    var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(AllegatiConfigurationEntity.class);
    var root = cq.from(AllegatiConfigurationEntity.class);
    cq.select(root);
    if (StringUtils.isNotBlank(codiceRaggrArticoli)) {
      Predicate p = cb.equal(root.get("codiceRaggruppamentoArticolo"), codiceRaggrArticoli);
      cq.where(p);

      TypedQuery<AllegatiConfigurationEntity> createQuery = em.createQuery(cq);
      logQuery(createQuery);
      try {
        result = createQuery.getSingleResult();
      } catch (NoResultException e) {
        log.info("Not found result for CodiceRaggruppamentoArticoli: " + codiceRaggrArticoli);
        log.debug("", e);
      } catch (IllegalArgumentException e1) {
        log.debug("", e1);
      }
    }

    log.info("Founded: {}", result);
    return result;

  }

  private void logQuery(TypedQuery<?> createQuery) {
    String queryString = createQuery.unwrap(org.hibernate.query.Query.class)
                                    .getQueryString();
    log.debug("Query string: {}", queryString);
  }

}
