package it.fai.ms.consumi.service.processor.consumi.elvia;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface ElviaProcessor extends ConsumoProcessor<Elvia> {

}
