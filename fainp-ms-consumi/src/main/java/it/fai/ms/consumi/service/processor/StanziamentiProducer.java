package it.fai.ms.consumi.service.processor;

import java.util.List;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

//FIXME rinominare in StanziamentiPedaggiProducer perchè è usato solo da pedaggi
public interface StanziamentiProducer {

  List<Stanziamento> produce(@NotNull DettaglioStanziamento allocationDetail, @NotNull StanziamentiParams stanziamentiParams);

}
