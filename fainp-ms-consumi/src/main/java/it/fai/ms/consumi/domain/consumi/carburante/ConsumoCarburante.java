package it.fai.ms.consumi.domain.consumi.carburante;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

public abstract class ConsumoCarburante
  extends Consumo
  implements VehicleAssociatedWithDeviceSupplier {

  private Boolean     processed;
  private Device      device;
  private Vehicle     vehicle;
  private Instant     entryDateTime;
  private String      otherSupport;
  private Integer     counter;
  private BigDecimal  quantity;
  private BigDecimal  kilometers;
  private String      ticket;
  private String      productCode;
  private FuelStation fuelStation;
  private Amount      priceReference;
  private Amount      costComputed;
  private Amount      priceComputed;

  public ConsumoCarburante(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public Amount getAmount() {
    return priceComputed!=null ? priceComputed : priceReference!=null ? priceReference : costComputed ;
  }

  public Boolean getProcessed() {
    return processed;
  }

  public void setProcessed(Boolean processed) {
    this.processed = processed;
  }

  @Override
  public Device getDevice() {
    return device;
  }

  public void setDevice(Device device) {
    this.device = device;
  }

  @Override
  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public String getOtherSupport() {
    return otherSupport;
  }

  public void setOtherSupport(String otherSupport) {
    this.otherSupport = otherSupport;
  }

  public Integer getCounter() {
    return counter;
  }

  public void setCounter(Integer counter) {
    this.counter = counter;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getKilometers() {
    return kilometers;
  }

  public void setKilometers(BigDecimal kilometers) {
    this.kilometers = kilometers;
  }

  public String getTicket() {
    return ticket;
  }

  public void setTicket(String ticket) {
    this.ticket = ticket;
  }

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public FuelStation getFuelStation() {
    return fuelStation;
  }

  public void setFuelStation(FuelStation fuelStation) {
    this.fuelStation = fuelStation;
  }

  public Amount getPriceReference() {
    return priceReference;
  }

  public void setPriceReference(Amount prezzoRiferimento) {
    this.priceReference = prezzoRiferimento;
  }

  public Amount getPriceComputed() {
    return priceComputed;
  }

  public void setPriceComputed(Amount priceComputed) {
    this.priceComputed = priceComputed;
  }

  public Instant getEntryDateTime() {
    return entryDateTime;
  }

  public void setEntryDateTime(Instant entryDateTime) {
    this.entryDateTime = entryDateTime;
  }

  public Amount getCostComputed() {
    return costComputed;
  }

  public void setCostComputed(Amount costComputed) {
    this.costComputed = costComputed;
  }


  @Override
  public Instant getDate() {
    return entryDateTime;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = (prime * result) + ((costComputed == null) ? 0 : costComputed.hashCode());
    result = (prime * result) + ((counter == null) ? 0 : counter.hashCode());
    result = (prime * result) + ((device == null) ? 0 : device.hashCode());
    result = (prime * result) + ((entryDateTime == null) ? 0 : entryDateTime.hashCode());
    result = (prime * result) + ((fuelStation == null) ? 0 : fuelStation.hashCode());
    result = (prime * result) + ((kilometers == null) ? 0 : kilometers.hashCode());
    result = (prime * result) + ((quantity == null) ? 0 : quantity.hashCode());
    result = (prime * result) + ((otherSupport == null) ? 0 : otherSupport.hashCode());
    result = (prime * result) + ((priceComputed == null) ? 0 : priceComputed.hashCode());
    result = (prime * result) + ((priceReference == null) ? 0 : priceReference.hashCode());
    result = (prime * result) + ((processed == null) ? 0 : processed.hashCode());
    result = (prime * result) + ((productCode == null) ? 0 : productCode.hashCode());
    result = (prime * result) + ((ticket == null) ? 0 : ticket.hashCode());
    result = (prime * result) + ((vehicle == null) ? 0 : vehicle.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!super.equals(obj)) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ConsumoCarburante other = (ConsumoCarburante) obj;
    if (costComputed == null) {
      if (other.costComputed != null) {
        return false;
      }
    } else if (!costComputed.equals(other.costComputed)) {
      return false;
    }
    if (counter == null) {
      if (other.counter != null) {
        return false;
      }
    } else if (!counter.equals(other.counter)) {
      return false;
    }
    if (device == null) {
      if (other.device != null) {
        return false;
      }
    } else if (!device.equals(other.device)) {
      return false;
    }
    if (entryDateTime == null) {
      if (other.entryDateTime != null) {
        return false;
      }
    } else if (!entryDateTime.equals(other.entryDateTime)) {
      return false;
    }
    if (fuelStation == null) {
      if (other.fuelStation != null) {
        return false;
      }
    } else if (!fuelStation.equals(other.fuelStation)) {
      return false;
    }
    if (kilometers == null) {
      if (other.kilometers != null) {
        return false;
      }
    } else if (!kilometers.equals(other.kilometers)) {
      return false;
    }
    if (quantity == null) {
      if (other.quantity != null) {
        return false;
      }
    } else if (!quantity.equals(other.quantity)) {
      return false;
    }
    if (otherSupport == null) {
      if (other.otherSupport != null) {
        return false;
      }
    } else if (!otherSupport.equals(other.otherSupport)) {
      return false;
    }
    if (priceComputed == null) {
      if (other.priceComputed != null) {
        return false;
      }
    } else if (!priceComputed.equals(other.priceComputed)) {
      return false;
    }
    if (priceReference == null) {
      if (other.priceReference != null) {
        return false;
      }
    } else if (!priceReference.equals(other.priceReference)) {
      return false;
    }
    if (processed == null) {
      if (other.processed != null) {
        return false;
      }
    } else if (!processed.equals(other.processed)) {
      return false;
    }
    if (productCode == null) {
      if (other.productCode != null) {
        return false;
      }
    } else if (!productCode.equals(other.productCode)) {
      return false;
    }
    if (ticket == null) {
      if (other.ticket != null) {
        return false;
      }
    } else if (!ticket.equals(other.ticket)) {
      return false;
    }
    if (vehicle == null) {
      if (other.vehicle != null) {
        return false;
      }
    } else if (!vehicle.equals(other.vehicle)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ConsumoCarburante [");
    if (processed != null) {
      builder.append("processed=");
      builder.append(processed);
      builder.append(", ");
    }
    if (device != null) {
      builder.append("device=");
      builder.append(device);
      builder.append(", ");
    }
    if (vehicle != null) {
      builder.append("vehicle=");
      builder.append(vehicle);
      builder.append(", ");
    }
    if (entryDateTime != null) {
      builder.append("entryDateTime=");
      builder.append(entryDateTime);
      builder.append(", ");
    }
    if (otherSupport != null) {
      builder.append("otherSupport=");
      builder.append(otherSupport);
      builder.append(", ");
    }
    if (counter != null) {
      builder.append("counter=");
      builder.append(counter);
      builder.append(", ");
    }
    if (quantity != null) {
      builder.append("quantity=");
      builder.append(quantity);
      builder.append(", ");
    }
    if (kilometers != null) {
      builder.append("kilometers=");
      builder.append(kilometers);
      builder.append(", ");
    }
    if (ticket != null) {
      builder.append("ticket=");
      builder.append(ticket);
      builder.append(", ");
    }
    if (productCode != null) {
      builder.append("productCode=");
      builder.append(productCode);
      builder.append(", ");
    }
    if (fuelStation != null) {
      builder.append("fuelStation=");
      builder.append(fuelStation);
      builder.append(", ");
    }
    if (priceReference != null) {
      builder.append("priceExposed=");
      builder.append(priceReference);
      builder.append(", ");
    }
    if (priceComputed != null) {
      builder.append("priceComputed=");
      builder.append(priceComputed);
      builder.append(", ");
    }
    if (costComputed != null) {
      builder.append("costComputed=");
      builder.append(costComputed);
    }
    builder.append("]");
    return builder.toString();
  }

}
