package it.fai.ms.consumi.service.processor.consumi;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.service.processor.ProcessingResult;

public interface ConsumoProcessor<T extends Consumo> {

  ProcessingResult validateAndProcess(T consumo, Bucket bucket);

}