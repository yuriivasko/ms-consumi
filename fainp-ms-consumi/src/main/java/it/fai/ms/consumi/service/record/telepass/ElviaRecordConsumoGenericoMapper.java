package it.fai.ms.consumi.service.record.telepass;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAdjusters;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.consumi.generici.telepass.ElviaGenerico;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class ElviaRecordConsumoGenericoMapper
  extends AbstractRecordConsumoMapper implements RecordConsumoMapper<ElviaRecord, ElviaGenerico> {

  /*
   * (non-Javadoc)
   * @see it.fai.ms.consumi.service.record.telepass.RecordConsumoMapper#mapRecordToConsumo(it.fai.ms.consumi.repository.
   * flussi.record.model.CommonRecord)
   */
  @Override
  public ElviaGenerico mapRecordToConsumo(ElviaRecord commonRecord) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyy_MM_dd_dateformat, null);
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, TelepassSourceName.ELVIA);
    source.setFileName(commonRecord.getFileName());

    source.setRowNumber(commonRecord.getRowNumber());

    ElviaGenerico elviaGenerico = new ElviaGenerico(source, "TOT",
                                Optional.of(new ElviaGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);

    Transaction transaction = new Transaction();
    transaction.setDetailCode(commonRecord.getTypeMovement());
    transaction.setSign(commonRecord.getSignOfTheAmount());
    elviaGenerico.setTransaction(transaction);

    if (StringUtils.isNotBlank(commonRecord.getSubContractCode())) {
      Contract contract = new Contract(commonRecord.getSubContractCode());
      elviaGenerico.setContract(Optional.of(contract));
    }

    //lo fa il validatore
    //elviaGenerico.setSupplierCode(...);

    //l'analisi dice vuoto
    //elviaGenerico.setServicePartnerCode(commonRecord.getServicePartnerCode());

    //lo fa il validatore
    //elviaGenerico.setGroupArticlesNav(...);

    Instant movementDate = calculateInstantByDateAndTime(commonRecord.getDateMovement(),
                                                         commonRecord.getHourMovement(),
                                                         yyyy_MM_dd_dateformat,
                                                         HHmmss_dottet_timeformat).get();

    final ZoneId systemDefaultZoneId = ZoneId.systemDefault();
    final ZoneOffset utcOffset = ZoneOffset.UTC;
    LocalDateTime date = LocalDateTime.ofInstant(movementDate, systemDefaultZoneId);
    LocalDateTime firstDayOfMonth = date.with(TemporalAdjusters.firstDayOfMonth());
    LocalDateTime lastDayOfMonth = date.with(TemporalAdjusters.lastDayOfMonth());

    elviaGenerico.setStartDate(firstDayOfMonth.toInstant(utcOffset));
    elviaGenerico.setEndDate(lastDayOfMonth.toInstant(utcOffset));

    elviaGenerico.setQuantity(BigDecimal.ONE);

    // CONSUMO.AMOUNT
    Amount amount = new Amount();
    MonetaryAmount amountIncluededVat = null;
    amount.setExchangeRate(null); // Valore di default --> non viene usato
    MonetaryAmount totalAmount = null;
    amount.setVatRate(commonRecord.getVatRateBigDecimal());
    MonetaryAmount amountNotSubjectedToVat = commonRecord.getAmount_not_subjected_to_vat();

    //    totalAmount = commonRecord.getTotal_amount_value();
    //    modificato uso del discount: e' la cifra pagata (IVA inclusa) al netto degli sconti
    totalAmount = commonRecord.getDiscount_value();

    amountIncluededVat = totalAmount.subtract(amountNotSubjectedToVat);
    amount.setAmountIncludedVat(amountIncluededVat); //questo gia' calcola excludedvat


    elviaGenerico.setAmount(amount);
    elviaGenerico.setProcessed(commonRecord.isProcessed());


    // CONSUMO altri campi
    elviaGenerico.setDeviceCode(commonRecord.getSupportCode());

    //elviaGenerico.setRoute("VUOTO");

    //non presente in ELVIA
    //elviaGenerico.setLicencePlateOnRecord(...);

    //non presente in ELVIA
    //elviaGenerico.setAdditionalInformation(...);

    //non presente in ELVIA
//    elviaGenerico.setSupplierInvoiceDocument(...);

    return elviaGenerico;
  }


}
