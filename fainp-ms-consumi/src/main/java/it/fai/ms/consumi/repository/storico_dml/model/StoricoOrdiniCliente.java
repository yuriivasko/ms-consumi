package it.fai.ms.consumi.repository.storico_dml.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.fai.ms.common.dml.AbstractDml;

@Entity
@Table(name = "storico_ordini_cliente")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoOrdiniCliente implements  AbstractDml {

  private static final long serialVersionUID = 6832787594512528501L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Size(max = 255)
  @Column(name = "ordine_cliente_dml_unique_identifier", length = 255)
  private String dmlUniqueIdentifier;

  @Size(max = 255)
  @Column(name = "cliente_assegnatario", length = 255)
  private String clienteAssegnatario;

  @Size(max = 255)
  @Column(name = "tipo_ordine", length = 255)
  private String tipoOrdine;

  @Size(max = 255)
  @Column(name = "stato_ordine", length = 255)
  private String statoOrdine;

  @Size(max = 255)
  @Column(name = "numero_ordine", length = 255)
  private String numeroOrdine;

  @Size(max = 255)
  @Column(name = "codice_cliente", length = 255)
  private String codiceCliente;

  @Size(max = 1000)
  @Column(name = "ragione_sociale", length = 1000)
  private String ragioneSociale;

  @Size(max = 255)
  @Column(name = "consumo_mensile_previsto", length = 255)
  private String consumoMensilePrevisto;

  @Column(name = "data_ultima_variazione")
  private Instant dataUltimaVariazione;

  @Column(name = "dml_revision_timestamp")
  private Instant dmlRevisionTimestamp;

  @Size(max = 255)
  @Column(name = "richiedente", length = 255)
  private String richiedente;
  
  @Override
  @Transient
  public String getDmlUniqueIdentifier() {
    return dmlUniqueIdentifier;
  }
  
  public String getOrdineClienteDmlUniqueIdentifier() {
    return dmlUniqueIdentifier;
  }

  public StoricoOrdiniCliente ordineClienteDmlUniqueIdentifier(String ordineClienteDmlUniqueIdentifier) {
    this.dmlUniqueIdentifier = ordineClienteDmlUniqueIdentifier;
    return this;
  }

  public void setOrdineClienteDmlUniqueIdentifier(String ordineClienteDmlUniqueIdentifier) {
    this.dmlUniqueIdentifier = ordineClienteDmlUniqueIdentifier;
  }

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getClienteAssegnatario() {
    return clienteAssegnatario;
  }

  public StoricoOrdiniCliente clienteAssegnatario(String clienteAssegnatario) {
    this.clienteAssegnatario = clienteAssegnatario;
    return this;
  }

  public void setClienteAssegnatario(String clienteAssegnatario) {
    this.clienteAssegnatario = clienteAssegnatario;
  }

  public String getTipoOrdine() {
    return tipoOrdine;
  }

  public StoricoOrdiniCliente tipoOrdine(String tipoOrdine) {
    this.tipoOrdine = tipoOrdine;
    return this;
  }

  public void setTipoOrdine(String tipoOrdine) {
    this.tipoOrdine = tipoOrdine;
  }

  public String getStatoOrdine() {
    return statoOrdine;
  }

  public StoricoOrdiniCliente statoOrdine(String statoOrdine) {
    this.statoOrdine = statoOrdine;
    return this;
  }

  public void setStatoOrdine(String statoOrdine) {
    this.statoOrdine = statoOrdine;
  }

  public String getNumeroOrdine() {
    return numeroOrdine;
  }

  public StoricoOrdiniCliente numeroOrdine(String numeroOrdine) {
    this.numeroOrdine = numeroOrdine;
    return this;
  }

  public void setNumeroOrdine(String numeroOrdine) {
    this.numeroOrdine = numeroOrdine;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public StoricoOrdiniCliente codiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
    return this;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public StoricoOrdiniCliente ragioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
    return this;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getConsumoMensilePrevisto() {
    return consumoMensilePrevisto;
  }

  public StoricoOrdiniCliente consumoMensilePrevisto(String consumoMensilePrevisto) {
    this.consumoMensilePrevisto = consumoMensilePrevisto;
    return this;
  }

  public void setConsumoMensilePrevisto(String consumoMensilePrevisto) {
    this.consumoMensilePrevisto = consumoMensilePrevisto;
  }

  public Instant getDataUltimaVariazione() {
    return dataUltimaVariazione;
  }

  public StoricoOrdiniCliente dataUltimaVariazione(Instant dataUltimaVariazione) {
    this.dataUltimaVariazione = dataUltimaVariazione;
    return this;
  }

  public void setDataUltimaVariazione(Instant dataUltimaVariazione) {
    this.dataUltimaVariazione = dataUltimaVariazione;
  }

  public Instant getDmlRevisionTimestamp() {
    return dmlRevisionTimestamp;
  }

  public StoricoOrdiniCliente dmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
    return this;
  }

  public void setDmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
  }

  public String getRichiedente() {
    return richiedente;
  }

  public StoricoOrdiniCliente richiedente(String richiedente) {
    this.richiedente = richiedente;
    return this;
  }

  public void setRichiedente(String richiedente) {
    this.richiedente = richiedente;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StoricoOrdiniCliente storicoOrdiniCliente = (StoricoOrdiniCliente) o;
    if (storicoOrdiniCliente.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), storicoOrdiniCliente.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "StoricoOrdiniCliente{" + "id=" + getId() + ", ordineClienteDmlUniqueIdentifier='" + getOrdineClienteDmlUniqueIdentifier() + "'"
           + ", clienteAssegnatario='" + getClienteAssegnatario() + "'" + ", tipoOrdine='" + getTipoOrdine() + "'" + ", statoOrdine='"
           + getStatoOrdine() + "'" + ", numeroOrdine='" + getNumeroOrdine() + "'" + ", codiceCliente='" + getCodiceCliente() + "'"
           + ", ragioneSociale='" + getRagioneSociale() + "'" + ", consumoMensilePrevisto='" + getConsumoMensilePrevisto() + "'"
           + ", dataUltimaVariazione='" + getDataUltimaVariazione() + "'" + ", dmlRevisionTimestamp='" + getDmlRevisionTimestamp() + "'"
           + ", richiedente='" + getRichiedente() + "'" + "}";
  }
}
