package it.fai.ms.consumi.domain.consumi;

import java.util.Objects;

public class FuelStation {

  private final String id;
  private String       description;
  private String       fuelStationCode;
  private String       pumpNumber;

  public FuelStation(final String _id) {
    id = Objects.requireNonNull(_id, "Parameter id is mandatory");
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFuelStationCode() {
    return fuelStationCode;
  }

  public void setFuelStationCode(String fuelStationCode) {
    this.fuelStationCode = fuelStationCode;
  }

  public String getPumpNumber() {
    return pumpNumber;
  }

  public void setPumpNumber(String pumpNumber) {
    this.pumpNumber = pumpNumber;
  }

  public String getId() {
    return id;
  }

  @Override
  public String toString() {
    return "FuelStation [id=" + id + ", description=" + description + ", fuelStationCode=" + fuelStationCode + ", pumpNumber=" + pumpNumber
           + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + ((description == null) ? 0 : description.hashCode());
    result = (prime * result) + ((fuelStationCode == null) ? 0 : fuelStationCode.hashCode());
    result = (prime * result) + ((id == null) ? 0 : id.hashCode());
    result = (prime * result) + ((pumpNumber == null) ? 0 : pumpNumber.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    FuelStation other = (FuelStation) obj;
    if (description == null) {
      if (other.description != null) {
        return false;
      }
    } else if (!description.equals(other.description)) {
      return false;
    }
    if (fuelStationCode == null) {
      if (other.fuelStationCode != null) {
        return false;
      }
    } else if (!fuelStationCode.equals(other.fuelStationCode)) {
      return false;
    }
    if (id == null) {
      if (other.id != null) {
        return false;
      }
    } else if (!id.equals(other.id)) {
      return false;
    }
    if (pumpNumber == null) {
      if (other.pumpNumber != null) {
        return false;
      }
    } else if (!pumpNumber.equals(other.pumpNumber)) {
      return false;
    }
    return true;
  }

}
