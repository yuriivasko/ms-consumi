package it.fai.ms.consumi.repository.viewallegati;

import java.io.Serializable;

public class ViewAllegatoPedaggioSezione1Id implements Serializable {

  private static final long serialVersionUID = 3352852609859771560L;

  private String codiceContratto;

  private String tipoDispositivo;

  private String panNumber;

  // private String serialNumber;

  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getPanNumber() {
    return panNumber;
  }

  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }

  // public String getSerialNumber() {
  // return serialNumber;
  // }
  //
  // public void setSerialNumber(String serialNumber) {
  // this.serialNumber = serialNumber;
  // }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ViewAllegatoPedaggioSezione1Id [");
    sb.append("codiceContratto= ");
    sb.append(codiceContratto);
    sb.append(", tipoDispositivo= ");
    sb.append(tipoDispositivo);
    sb.append(", panNumber= ");
    sb.append(panNumber);
    // sb.append(", serialNumber= ");
    // sb.append(serialNumber);
    sb.append("]");
    return sb.toString();
  }

}
