package it.fai.ms.consumi.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "navcrm", url = "${application.restclient.url}", path = "/exage")
public interface AuthorizationApiClient
  extends AuthorizationApi {

  default String getAuthorizationToken(String grant_type, String username, String password) {
    LoginResponse result = this.login(String.format("grant_type=%s&username=%s&password=%s", grant_type, username, password));
    return result != null ? "Bearer " + result.getAccess_token() : "";
  }

}
