package it.fai.ms.consumi.repository.flussi.record.model.util;

import java.util.ArrayList;
import java.util.List;

public class CsvDescriptor {

  private String source_dir;
  private String backup_dir;

  private boolean hasHead = true;
  private boolean hasTail = true;

  private List<CsvItem> items = new ArrayList<>();

  public CsvDescriptor() {
    super();
  }

  public String getSource_dir() {
    return source_dir;
  }

  public void setSource_dir(String source_dir) {
    this.source_dir = source_dir;
  }

  public String getBackup_dir() {
    return backup_dir;
  }

  public void setBackup_dir(String backup_dir) {
    this.backup_dir = backup_dir;
  }

  public boolean isHasHead() {
    return hasHead;
  }

  public void setHasHead(boolean hasHead) {
    this.hasHead = hasHead;
  }

  public boolean isHasTail() {
    return hasTail;
  }

  public void setHasTail(boolean hasTail) {
    this.hasTail = hasTail;
  }

  public List<CsvItem> getItems() {
    return items;
  }

  public void setItems(List<CsvItem> items) {
    this.items = items;
  }

  public void addItem(String name, int pos) {
    this.items.add(new CsvItem(name, pos));
  }

}
