package it.fai.ms.consumi.repository.flussi.record.model;

import java.math.BigDecimal;

import javax.money.MonetaryAmount;

public interface MonetaryBaseRecord {

  MonetaryAmount getAmountIncludedVat();

  MonetaryAmount getAmountExcludedVat();

  BigDecimal getVatPercRate();

}
