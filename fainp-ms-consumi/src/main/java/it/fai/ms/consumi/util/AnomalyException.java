package it.fai.ms.consumi.util;

public class AnomalyException extends RuntimeException {
  
  public AnomalyException(String messageError, Throwable e) {
    super(messageError,e);
  }

  private static final long serialVersionUID = -857732604848169674L;

  
}
