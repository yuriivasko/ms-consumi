package it.fai.ms.consumi.domain.dettaglio_stanziamento.treno;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;

public class DettaglioStanziamentoTreno extends DettaglioStanziamento {

  public DettaglioStanziamentoTreno(final GlobalIdentifier _globalIdentifier) {
    super(_globalIdentifier);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      res = super.equals(_obj);
    }
    return res;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public String toString() {
    return new StringBuilder().append(super.toString())
                              .toString();
  }

}
