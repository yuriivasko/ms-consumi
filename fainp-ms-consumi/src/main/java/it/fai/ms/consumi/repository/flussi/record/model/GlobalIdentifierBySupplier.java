package it.fai.ms.consumi.repository.flussi.record.model;

public interface GlobalIdentifierBySupplier {

  String getGlobalIdentifier();

}
