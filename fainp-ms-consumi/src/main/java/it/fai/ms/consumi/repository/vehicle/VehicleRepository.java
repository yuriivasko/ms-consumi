package it.fai.ms.consumi.repository.vehicle;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

public interface VehicleRepository {

  Optional<Vehicle> findVehicleByVehicleUuid(@NotNull String vehicleUuid, @NotNull Instant instant);

  Optional<StoricoVeicolo> findVeicoloForDeviceAtDate(String deviceIdentifier, Instant dataRiferimento);

  Optional<String> findIdVeicoloFromStoricoAssociazioneDVForDeviceAtDate(String deviceIdentifier, Instant dataRiferimento);

}
