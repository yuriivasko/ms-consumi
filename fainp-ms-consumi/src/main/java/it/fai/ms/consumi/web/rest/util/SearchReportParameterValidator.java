package it.fai.ms.consumi.web.rest.util;


import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class SearchReportParameterValidator {

  private SearchReportParameterValidator(){}
  public static Optional<ValidationResult> validateParameters(
    Boolean invoiced,
    Boolean notInvoiced,
    LocalDate consumptionDateFrom,
    LocalDate consumptionDateTo,
    LocalDate invoiceDateFrom,
    LocalDate invoiceDateTo
  ){
    if (invoiced == null && notInvoiced == null) {
      return Optional.of(new ValidationResult("Invalid parameter combination. mandatory", "invoiced-notInvoiced"));
    }
    if ((invoiced != null && invoiced.booleanValue())
      && (invoiceDateFrom == null && invoiceDateTo == null)
      && (consumptionDateFrom == null || consumptionDateTo == null)
    ) {
      return Optional.of(new ValidationResult("Invalid parameter combination. Mandatory", "consumptionDateFrom-consumptionDateTo"));
    }
    if ((invoiced != null && invoiced.booleanValue())
      && (consumptionDateFrom == null && consumptionDateTo == null)
      && (invoiceDateFrom == null || invoiceDateTo == null)
    ) {
      return Optional.of(new ValidationResult("Invalid parameter combination. Mandatory", "consumptionDateFrom-consumptionDateTo"));
    }
    if (consumptionDateFrom != null && consumptionDateTo != null
      && ChronoUnit.DAYS.between(consumptionDateFrom, consumptionDateTo) > 365) {
      return Optional.of(new ValidationResult("Invalid parameter combination. max 1 year", "invoiceDateFrom-invoiceDateTo"));
    }
    if (invoiceDateFrom != null && invoiceDateTo != null
      && ChronoUnit.DAYS.between(invoiceDateFrom, invoiceDateTo) > 365) {
      return Optional.of(new ValidationResult("Invalid parameter combination. max 1 year", "invoiceDateFrom-invoiceDateTo"));
    }
    return Optional.empty();
  }


  static public class ValidationResult{
    private String message;
    private String param;

    public ValidationResult(String message, String param) {
      this.message = message;
      this.param = param;
    }

    public String getMessage() {
      return message;
    }

    public void setMessage(String message) {
      this.message = message;
    }

    public String getParam() {
      return param;
    }

    public void setParam(String param) {
      this.param = param;
    }

  }
}
