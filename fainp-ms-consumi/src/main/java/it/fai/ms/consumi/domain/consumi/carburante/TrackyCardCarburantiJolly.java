package it.fai.ms.consumi.domain.consumi.carburante;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Optional;

public class TrackyCardCarburantiJolly
  extends TrackyCardCarburantiStandard
  implements TrustedVechicleSupplier, TrustedCustomerSupplier {

  private Customer customer;

  public TrackyCardCarburantiJolly(Source _source, String _recordCode, Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }
}
