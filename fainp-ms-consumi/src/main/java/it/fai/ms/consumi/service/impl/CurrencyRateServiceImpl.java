package it.fai.ms.consumi.service.impl;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.CurrencyRate;
import it.fai.ms.consumi.repository.CurrencyRateRepository;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.dto.CurrencyRateDTO;
import it.fai.ms.consumi.service.mapper.CurrencyRateMapper;
import it.fai.ms.consumi.service.validator.ConsumoBlockingException;

@Service
@Transactional
public class CurrencyRateServiceImpl implements CurrencyRateService {

  private final Logger log = LoggerFactory.getLogger(CurrencyRateServiceImpl.class);

  private final CurrencyRateRepository currencyRateRepository;
  private final CurrencyRateMapper     currencyRateMapper;

  public CurrencyRateServiceImpl(CurrencyRateRepository currencyRateRepository, CurrencyRateMapper currencyRateMapper) {
    this.currencyRateRepository = currencyRateRepository;
    this.currencyRateMapper = currencyRateMapper;
  }

  /**
   * Save a currencyRate.
   *
   * @param currencyRateDTO
   *          the entity to save
   * @return the persisted entity
   */
  @Override
  public CurrencyRateDTO save(CurrencyRateDTO currencyRateDTO) {
    log.trace("Request to save CurrencyRate : {}", currencyRateDTO);
    CurrencyRate currencyRate = currencyRateMapper.toEntity(currencyRateDTO);
    currencyRate = currencyRateRepository.save(currencyRate);
    return currencyRateMapper.toDto(currencyRate);
  }

  /**
   * Get all the currencyRates.
   *
   * @return the list of entities
   */
  @Override
  @Transactional(readOnly = true)
  public List<CurrencyRateDTO> findAll() {
    log.trace("Request to get all CurrencyRates");
    return currencyRateRepository.findAll()
                                 .stream()
                                 .map(currencyRateMapper::toDto)
                                 .collect(Collectors.toCollection(LinkedList::new));
  }

  /**
   * Get all the currencyRates.
   *
   * @return the list of entities
   */
  @Transactional(readOnly = true)
  public List<CurrencyRateDTO> findByTimeAndCurrency(Instant time, String currency) {
    log.trace("Request to get all CurrencyRates");
    return currencyRateRepository.findByTimeAndCurrency(time, currency)
                                 .stream()
                                 .map(currencyRate -> currencyRateMapper.toDto(currencyRate))
                                 .collect(Collectors.toList());
  }

  /**
   * Update rate
   *
   * @return
   */
  public int updateRate(Instant time, String currency, BigDecimal rate) {
    log.trace("Update rate [time: {}, currency: {}, rate: {}]", time, currency, rate);
    return currencyRateRepository.setNewRateForCurrencyRate(rate, currency, time);
  }

  @Override
  public CurrencyRateDTO findOne(Long id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void delete(Long id) {
    // TODO Auto-generated method stub

  }

  @Override
  public void readCurrencyRateFile(String fname) {
    log.info("Reading currency file in [{}]", fname);
    try {
      File fXmlFile = new File(fname);

      if (log.isDebugEnabled()) {
        log.trace(" --- FILE CONTENT ---");
        Files.lines(fXmlFile.toPath())
             .forEach(s -> {
               log.trace(s);
             });
        log.trace(" --- END FILE CONTENT ---");
      }

      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(fXmlFile);
      doc.getDocumentElement()
         .normalize();
      NodeList nList = doc.getElementsByTagName("Cube");
      log.debug("Read file");

      for (int ind = 0; ind < nList.getLength(); ind++) {
        Node nNode = nList.item(ind);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          NodeList cList = nNode.getChildNodes();
          for (int ind1 = 0; ind1 < cList.getLength(); ind1++) {
            Node cNode = cList.item(ind1);
            if (cNode.getNodeType() == Node.ELEMENT_NODE) {
              Element cElement = (Element) cNode;
              String time = cElement.getAttribute("time");
              NodeList ccList = cNode.getChildNodes();
              for (int ind2 = 0; ind2 < ccList.getLength(); ind2++) {
                Node ccNode = ccList.item(ind2);
                if (ccNode.getNodeType() == Node.ELEMENT_NODE) {
                  Element ccElement = (Element) ccNode;

                  String currency = ccElement.getAttribute("currency");
                  String rate = ccElement.getAttribute("rate");

                  Instant iTime = new SimpleDateFormat("yyyy-MM-dd").parse(time)
                                                                    .toInstant();
                  List<CurrencyRateDTO> ldto = this.findByTimeAndCurrency(iTime, currency);

                  if (ldto.size() == 0) {
                    CurrencyRateDTO dto = new CurrencyRateDTO();
                    dto.setTime(iTime);
                    dto.setCurrency(currency);
                    dto.setRate(new BigDecimal(rate));
                    this.save(dto);
                  } else {
                    this.updateRate(iTime, currency, new BigDecimal(rate));
                  }
                }
              }
            }
          }
        }
      }
    } catch (Exception e) {
      log.error("Error parsing currency file!", e);
    }
  }

  @Override
  public Amount convertAmount(Amount amount, Currency targetCurrency, Instant time) {
    MonetaryAmount result = null;
    BigDecimal exchangeRate = null;

    if (time == null) {
      throw new IllegalArgumentException("time cannot be null");
    }

    if (targetCurrency.getCurrencyCode()
                      .equals("EUR")
        && !amount.hasCurrency(Monetary.getCurrency(targetCurrency.getCurrencyCode()))) {

      Optional<CurrencyRate> optionalRate = currencyRateRepository.findFirstByCurrencyAndTimeLessThanEqualOrderByTimeDesc(amount.getCurrency()
                                                                                                                                .getCurrencyCode(),
                                                                                                                          time);

      // SE non trovo il cambio o il cambio trovato è precedente al time in ingresso si almeno 2 giorni, è bloccante
      if (!optionalRate.isPresent() || optionalRate.get()
                                                   .getTime()
                                                   .isBefore(time.minus(2, ChronoUnit.DAYS))) {
        throw new ConsumoBlockingException("Cambio non trovato per currency " + amount.getCurrency() + " e data " + time);
      }

      exchangeRate = optionalRate.get()
                                 .getRate();
      BigDecimal convertedAmount = this.convertToEuroRate(exchangeRate, amount.getAmountExcludedVatBigDecimal());

      result = Monetary.getDefaultAmountFactory()
                       .setCurrency(targetCurrency.getCurrencyCode())
                       .setNumber(convertedAmount)
                       .create();
    } else {
      result = Monetary.getDefaultAmountFactory()
                       .setCurrency(amount.getAmountExcludedVat()
                                          .getCurrency())
                       .setNumber(amount.getAmountExcludedVat()
                                        .getNumber())
                       .create();
    }

    Amount convertedAmount = new Amount();
    convertedAmount.setVatRate(amount.getVatRateBigDecimal());
    convertedAmount.setExchangeRate(exchangeRate);
    convertedAmount.setAmountExcludedVat(result);
    return convertedAmount;
  }

  @Override
  public BigDecimal convertToEuroRate(BigDecimal rate, BigDecimal amount) {
    BigDecimal res = amount;
    if (rate.compareTo(BigDecimal.ZERO) != 0) {
      res = amount.divide(rate, 4, RoundingMode.CEILING);
    }
    return res;
  }
}
