package it.fai.ms.consumi.web.rest;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.consumi.domain.AsyncJobResponse;
import it.fai.ms.consumi.dto.AsyncJobCreatedDto;
import it.fai.ms.consumi.dto.AsyncJobResponseDto;
// import com.codahale.metrics.annotation.Timed;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.DispositiviDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.PadaggiDaFatturareCSVDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.PedaggiDaFatturareDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.ReportPedaggiDaFatturare;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.ViewReportConsumiPedaggiEntity;
import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;
import it.fai.ms.consumi.service.async.ReportAsyncService;
import it.fai.ms.consumi.service.jms.producer.report.JmsReportProducer;
import it.fai.ms.consumi.service.report.ReportCsv;
import it.fai.ms.consumi.web.rest.util.HeaderUtil;
import it.fai.ms.consumi.web.rest.util.ResponsePage;
import it.fai.ms.consumi.web.rest.util.SearchReportParameterValidator;

@RestController
@RequestMapping(SearchReportPedaggiResource.BASE_PATH)
public class SearchReportPedaggiResource {

  private final Logger log = LoggerFactory.getLogger(getClass());

  static final String BASE_PATH = "/api/pedaggi";

  private static final String BASE_POLLING_PATH = "/polling/report";

  private final ReportPedaggiDaFatturare reportPedaggi;
  private final ReportAsyncService       reportAsyncService;

  private final JmsReportProducer jmsReportProducer;

  private ReportCsv reportCsv;

  public SearchReportPedaggiResource(ReportPedaggiDaFatturare reportPedaggiDaFatturare, ReportAsyncService reportAsyncService,
                                     JmsReportProducer jmsReportProducer, ReportCsv reportCsv) {
    this.reportPedaggi = reportPedaggiDaFatturare;
    this.reportAsyncService = reportAsyncService;
    this.jmsReportProducer = jmsReportProducer;
    this.reportCsv = reportCsv;
  }

  @GetMapping("/{codClienteNav}/raggruppamenti")
  @ResponseBody
  public ResponseEntity<List<RaggruppamentoDTO>> searchRaggruppamenti(@PathVariable String codClienteNav) {
    log.debug("REST request to search report for type PedaggiDaFatturare by codice cliente: {}", codClienteNav);
    List<RaggruppamentoDTO> raggruppamentiToInvoice = new ArrayList<>();

    List<AllegatiConfigurationEntity> raggruppamentiAllegatiConfig = reportPedaggi.getRaggruppamentiByAllegatiConfigurationAndDetails(codClienteNav);
    if (raggruppamentiAllegatiConfig != null && !raggruppamentiAllegatiConfig.isEmpty()) {
      raggruppamentiToInvoice = raggruppamentiAllegatiConfig.stream()
                                                            .map(a -> new RaggruppamentoDTO(a.getCodiceRaggruppamentoArticolo(),
                                                                                            a.getDescrizione()))
                                                            .collect(toList());
    }

    log.info("Found {} raggruppamenti to invoice.", raggruppamentiToInvoice.size());
    return ResponseEntity.ok(raggruppamentiToInvoice);
  }

  @PostMapping("/{codClienteNav}/dispositivi/{tipoServizio}")
  @ResponseBody
  public ResponseEntity<List<DispositiviDto>> searchDispositivi(@PathVariable String codClienteNav, @PathVariable String tipoServizio,
                                                                @RequestBody(required = true) SearchReportFilterDTO filter) {

    log.debug("REST request to search report for type PedaggiDaFatturare {} {}", codClienteNav, tipoServizio);
    Optional<SearchReportParameterValidator.ValidationResult> validationResult = SearchReportParameterValidator.validateParameters(filter.getInvoiced(),
                                                                                                                                   filter.getNotInvoiced(),
                                                                                                                                   filter.getConsumptionDateFrom(),
                                                                                                                                   filter.getConsumptionDateTo(),
                                                                                                                                   filter.getInvoiceDateFrom(),
                                                                                                                                   filter.getInvoiceDateTo());
    if (validationResult.isPresent()) {
      return ResponseEntity.badRequest()
                           .headers(validationResult.map(v -> HeaderUtil.createAlert(v.getMessage(), v.getParam()))
                                                    .get())
                           .build();
    }

    List<DispositiviDto> dispositiviPedaggio = reportPedaggi.getDispositiviPedaggio(codClienteNav, tipoServizio,
                                                                                               filter.getInvoiced(),
                                                                                               filter.getNotInvoiced(),
                                                                                               filter.getConsumptionDateFrom(),
                                                                                               filter.getConsumptionDateTo(),
                                                                                               filter.getInvoiceDateFrom(),
                                                                                               filter.getInvoiceDateTo());

    log.info("Found {} dispositivi pedaggio", dispositiviPedaggio != null ? dispositiviPedaggio.size() : 0);
    return ResponseEntity.ok(dispositiviPedaggio);

  }

  @PostMapping("/{codClienteNav}/detail")
  @ResponseBody
  public ResponseEntity<PedaggiDafatturareDetailResponse> searchCustomerPedaggiDetail(@PathVariable String codClienteNav,
                                                                                      @RequestBody(
                                                                                                   required = true) SearchReportFilterDTO filter) {

    log.info("REST request to search report for type PedaggiDaFatturare {} {}", codClienteNav, filter.getDispositivi());
    Optional<SearchReportParameterValidator.ValidationResult> validationResult = SearchReportParameterValidator.validateParameters(filter.getInvoiced(),
                                                                                                                                   filter.getNotInvoiced(),
                                                                                                                                   filter.getConsumptionDateFrom(),
                                                                                                                                   filter.getConsumptionDateTo(),
                                                                                                                                   filter.getInvoiceDateFrom(),
                                                                                                                                   filter.getInvoiceDateTo());
    if (validationResult.isPresent()) {
    	log.error("Validation Error: {}",validationResult.get().getMessage());
      return ResponseEntity.badRequest()
                           .headers(validationResult.map(v -> HeaderUtil.createAlert(v.getMessage(), v.getParam()))
                                                    .get())
                           .build();
    }
    log.info("Searching report for type PedaggiDaFatturare {} {}", codClienteNav, filter.getDispositivi());
    List<PedaggiDaFatturareDto> pedaggi = reportPedaggi.getDetailPedaggi(codClienteNav, filter.getDispositivi(),
                                                                                         filter.getInvoiced(), filter.getNotInvoiced(),
                                                                                         filter.getConsumptionDateFrom(),
                                                                                         filter.getConsumptionDateTo(),
                                                                                         filter.getInvoiceDateFrom(),
                                                                                         filter.getInvoiceDateTo());
    log.info("Found {} pedaggi for type PedaggiDaFatturare {} {}", pedaggi.size(), codClienteNav, filter.getDispositivi());
    PedaggiDafatturareDetailResponse response = new PedaggiDafatturareDetailResponse(pedaggi);
    
    return ResponseEntity.ok(response);

  }

  @PostMapping("/{codClienteNav}/report/{lang}")
  @ResponseBody
  public ResponseEntity<AsyncJobCreatedDto> launchCsvReport(@PathVariable String codClienteNav, @PathVariable String lang,
                                                            @RequestParam(value = "zoneId", required = false) String zoneId,
                                                            @RequestBody(required = true) SearchReportFilterDTO filter) throws Exception {

    log.debug("REST request to generate CSV PedaggiDaFatturare {} {}", codClienteNav, filter.getDispositivi());
    Optional<SearchReportParameterValidator.ValidationResult> validationResult = SearchReportParameterValidator.validateParameters(filter.getInvoiced(),
                                                                                                                                   filter.getNotInvoiced(),
                                                                                                                                   filter.getConsumptionDateFrom(),
                                                                                                                                   filter.getConsumptionDateTo(),
                                                                                                                                   filter.getInvoiceDateFrom(),
                                                                                                                                   filter.getInvoiceDateTo());
    if (validationResult.isPresent()) {
      return ResponseEntity.badRequest()
                           .headers(validationResult.map(v -> HeaderUtil.createAlert(v.getMessage(), v.getParam()))
                                                    .get())
                           .build();
    }

    Locale locale = lang != null ? Locale.forLanguageTag(lang) : null;
    final ZoneId idZone = zoneId != null ? ZoneId.of(zoneId) : null;

    Long keyJob = reportAsyncService.generateKeyJob();
    try {

      List<ViewReportConsumiPedaggiEntity> consumi = reportPedaggi.queryDetailPedaggi(codClienteNav, filter.getDispositivi(),
                                                                                                 filter.getInvoiced(),
                                                                                                 filter.getNotInvoiced(),
                                                                                                 filter.getConsumptionDateFrom(),
                                                                                                 filter.getConsumptionDateTo(),
                                                                                                 filter.getInvoiceDateFrom(),
                                                                                                 filter.getInvoiceDateTo());
      
      AtomicLong counter = new AtomicLong(0);
      
      List<PadaggiDaFatturareCSVDto> reportData = consumi.stream()
                                                         .peek( c -> {
                                                           c.setSourceRowNumber(counter.addAndGet(1));
                                                         })
                                                         .map(pedaggiDaFatturareDto -> mapPedaggiDaFatturareDto(pedaggiDaFatturareDto))
                                                         .collect(Collectors.toList());
      if (reportData.size() == 0)
        throw new RuntimeException("no data");

      String json = reportCsv.toCsv(PadaggiDaFatturareCSVDto.class, reportData, locale, idZone);
      jmsReportProducer.sendMessage(keyJob, "report_pedaggi_", json);
    } catch (Exception e) {
      log.error("Error on generate report: ", e);
      throw new RuntimeException(e);
    }

    return ResponseEntity.ok(new AsyncJobCreatedDto(keyJob.toString()));
  }

  private PadaggiDaFatturareCSVDto mapPedaggiDaFatturareDto(ViewReportConsumiPedaggiEntity consumo) {
    return new PadaggiDaFatturareCSVDto(consumo);
  }

  @GetMapping(BASE_POLLING_PATH + "/{identificativo}")
  public ResponseEntity<AsyncJobResponseDto> pollingReport(@PathVariable Long identificativo) {
    log.debug("Rest request for polling for jobkey: {}", identificativo);
    AsyncJobResponse output = reportAsyncService.getAsyncJobById(identificativo);
    log.info("Response polling is {}", output);

    return ResponseEntity.ok(new AsyncJobResponseDto(output));
  }

  @SuppressWarnings("unused")
  private <T> ResponseEntity<ResponsePage<T>> toPagedResponse(Pageable pageable, List<T> result) {
    int start = (int) pageable.getOffset();
    int end = (int) (start + pageable.getPageSize()) > result.size() ? result.size() : (start + pageable.getPageSize());
    Page<T> page = new PageImpl<>(result.subList(start, end), pageable, result.size());

    return ResponseEntity.ok(new ResponsePage<>(page));
  }

  public class RaggruppamentoDTO implements Serializable {

    private static final long serialVersionUID = 2336457818450407968L;

    private String codiceRaggruppamento;
    private String descrizioneRaggruppamento;

    public RaggruppamentoDTO() {
    }

    public RaggruppamentoDTO(@NotNull String code, String description) {
      codiceRaggruppamento = code;
      if (StringUtils.isBlank(description)) {
        description = code;
      }
      descrizioneRaggruppamento = description;
    }

    public String getCodiceRaggruppamento() {
      return codiceRaggruppamento;
    }

    public void setCodiceRaggruppamento(String codiceRaggruppamento) {
      this.codiceRaggruppamento = codiceRaggruppamento;
    }

    public String getDescrizioneRaggruppamento() {
      return descrizioneRaggruppamento;
    }

    public void setDescrizioneRaggruppamento(String descrizioneRaggruppamento) {
      this.descrizioneRaggruppamento = descrizioneRaggruppamento;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("RaggruppamentoDTO [codiceRaggruppamento=");
      builder.append(codiceRaggruppamento);
      builder.append(", descrizioneRaggruppamento=");
      builder.append(descrizioneRaggruppamento);
      builder.append("]");
      return builder.toString();
    }

  }

}
