package it.fai.ms.consumi.client;

import java.text.MessageFormat;
import java.util.List;
import java.util.Objects;

public class CountryService {

  private List<CountryDTO> allCountriesSorted;

  public CountryService(List<CountryDTO> allCountries) {
    allCountriesSorted = allCountries;
  }

  public CountryDTO getByIsoAlpha2(String text) {
    return allCountriesSorted.stream()
                             .filter(c -> Objects.equals(text, c.getCodiceIso2()))
                             .findFirst()
                             .orElseThrow(() -> new RuntimeException(MessageFormat.format("Country Iso Alpha 2 \"{}\" non supportato", text)));
  }

  public CountryDTO getByIsoAlpha3(String text) {
    return allCountriesSorted.stream()
                             .filter(c -> Objects.equals(text, c.getCodice()))
                             .findFirst()
                             .orElseThrow(() -> new RuntimeException(MessageFormat.format("Country Iso Alpha 3 \"{}\" non supportato", text)));
  }

  public CountryDTO getByIsoNumerico(String text) {
    return allCountriesSorted.stream()
                             .filter(c -> Objects.equals(text, c.getIsoNumerico()))
                             .findFirst()
                             .orElseThrow(() -> new RuntimeException(MessageFormat.format("Country Iso Numerico \"{}\" non supportato", text)));
  }

  public CountryDTO getBySiglaAuto(String text) {
    return allCountriesSorted.stream()
                             .filter(c -> Objects.equals(text, c.getSiglaAuto()))
                             .findFirst()
                             .orElseThrow(() -> new RuntimeException(MessageFormat.format("Country Targa \"{}\" non supportato", text)));
  }
}
