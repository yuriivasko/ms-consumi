package it.fai.ms.consumi.repository.storico_dml.model;

import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.fai.ms.common.dml.AbstractDml;

@Entity
@Table(name = "storico_richiesta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoRichiesta implements  AbstractDml  {

  private static final long serialVersionUID = -3006824471051730870L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Size(max = 255)
  @Column(name = "richiesta_dml_unique_identifier", length = 255)
  private String dmlUniqueIdentifier;

  @Size(max = 255)
  @Column(name = "tipo_richiesta", length = 255)
  private String tipoRichiesta;

  @Size(max = 255)
  @Column(name = "associazione", length = 255)
  private String associazione;

  @Size(max = 255)
  @Column(name = "tipo_dispositivo", length = 255)
  private String tipoDispositivo;

  @Size(max = 255)
  @Column(name = "stato_richiesta", length = 255)
  private String statoRichiesta;


  @Column(name = "anomalia")
  private String anomalia;

  @Size(max = 255)
  @Column(name = "dispositivo_dml_unique_identifier", length = 255)
  private String dispositivoDmlUniqueIdentifier;

  @Size(max = 255)
  @Column(name = "ordine_cliente_dml_unique_identifier", length = 255)
  private String ordineClienteDmlUniqueIdentifier;

  @Column(name = "dml_revision_timestamp")
  private Instant dmlRevisionTimestamp;

  @Column(name = "data_ultima_variazione")
  private Instant dataUltimaVariazione;

  @Override
  @Transient
  public String getDmlUniqueIdentifier() {
    return dmlUniqueIdentifier;
  }

  public String getRichiestaDmlUniqueIdentifier() {
    return dmlUniqueIdentifier;
  }

  public StoricoRichiesta richiestaDmlUniqueIdentifier(String richiestaDmlUniqueIdentifier) {
    this.dmlUniqueIdentifier = richiestaDmlUniqueIdentifier;
    return this;
  }

  public void setRichiestaDmlUniqueIdentifier(String richiestaDmlUniqueIdentifier) {
    this.dmlUniqueIdentifier = richiestaDmlUniqueIdentifier;
  }

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTipoRichiesta() {
    return tipoRichiesta;
  }

  public StoricoRichiesta tipoRichiesta(String tipoRichiesta) {
    this.tipoRichiesta = tipoRichiesta;
    return this;
  }

  public void setTipoRichiesta(String tipoRichiesta) {
    this.tipoRichiesta = tipoRichiesta;
  }

  public String getAssociazione() {
    return associazione;
  }

  public StoricoRichiesta associazione(String associazione) {
    this.associazione = associazione;
    return this;
  }

  public void setAssociazione(String associazione) {
    this.associazione = associazione;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public StoricoRichiesta tipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getStatoRichiesta() {
    return statoRichiesta;
  }

  public StoricoRichiesta statoRichiesta(String statoRichiesta) {
    this.statoRichiesta = statoRichiesta;
    return this;
  }

  public void setStatoRichiesta(String statoRichiesta) {
    this.statoRichiesta = statoRichiesta;
  }

  public String getAnomalia() {
    return anomalia;
  }

  public StoricoRichiesta anomalia(String anomalia) {
    this.anomalia = anomalia;
    return this;
  }

  public void setAnomalia(String anomalia) {
    this.anomalia = anomalia;
  }

  public String getDispositivoDmlUniqueIdentifier() {
    return dispositivoDmlUniqueIdentifier;
  }

  public StoricoRichiesta dispositivoDmlUniqueIdentifier(String dispositivoDmlUniqueIdentifier) {
    this.dispositivoDmlUniqueIdentifier = dispositivoDmlUniqueIdentifier;
    return this;
  }

  public void setDispositivoDmlUniqueIdentifier(String dispositivoDmlUniqueIdentifier) {
    this.dispositivoDmlUniqueIdentifier = dispositivoDmlUniqueIdentifier;
  }

  public String getOrdineClienteDmlUniqueIdentifier() {
    return ordineClienteDmlUniqueIdentifier;
  }

  public StoricoRichiesta ordineClienteDmlUniqueIdentifier(String ordineClienteDmlUniqueIdentifier) {
    this.ordineClienteDmlUniqueIdentifier = ordineClienteDmlUniqueIdentifier;
    return this;
  }

  public void setOrdineClienteDmlUniqueIdentifier(String ordineClienteDmlUniqueIdentifier) {
    this.ordineClienteDmlUniqueIdentifier = ordineClienteDmlUniqueIdentifier;
  }

  @Override
  public Instant getDmlRevisionTimestamp() {
    return dmlRevisionTimestamp;
  }

  public StoricoRichiesta dmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
    return this;
  }

  public void setDmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
  }

  public Instant getDataUltimaVariazione() {
    return dataUltimaVariazione;
  }

  public StoricoRichiesta dataUltimaVariazione(Instant dataUltimaVariazione) {
    this.dataUltimaVariazione = dataUltimaVariazione;
    return this;
  }

  public void setDataUltimaVariazione(Instant dataUltimaVariazione) {
    this.dataUltimaVariazione = dataUltimaVariazione;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StoricoRichiesta storicoRichiesta = (StoricoRichiesta) o;
    if (storicoRichiesta.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), storicoRichiesta.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "StoricoRichiesta{" + "id=" + getId() + ", richiestaDmlUniqueIdentifier='" + getRichiestaDmlUniqueIdentifier() + "'"
           + ", tipoRichiesta='" + getTipoRichiesta() + "'" + ", associazione='" + getAssociazione() + "'" + ", tipoDispositivo='"
           + getTipoDispositivo() + "'" + ", statoRichiesta='" + getStatoRichiesta() + "'" + ", anomalia='" + getAnomalia() + "'"
           + ", dispositivoDmlUniqueIdentifier='" + getDispositivoDmlUniqueIdentifier() + "'" + ", ordineClienteDmlUniqueIdentifier='"
           + getOrdineClienteDmlUniqueIdentifier() + "'" + ", dmlRevisionTimestamp='" + getDmlRevisionTimestamp() + "'"
           + ", dataUltimaVariazione='" + getDataUltimaVariazione() + "'" + "}";
  }
}
