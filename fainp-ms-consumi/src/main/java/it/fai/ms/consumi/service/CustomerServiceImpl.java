package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.anagazienda.AziendaDTO;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.repository.customer.CustomerRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoAziendaRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAzienda;

@Service
@Validated
public class CustomerServiceImpl implements CustomerService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final CustomerRepository customerRepository;
  private final DeviceService      deviceService;
  private final ContractService    contractService;
  private final StoricoAziendaRepository storicoStatoAziendaRepository;
  
  final static String STATO_AZIENDA_ATTIVO = AziendaDTO.StatoEnum.ATTIVO.name();

  @Inject
  public CustomerServiceImpl(final CustomerRepository _customerRepository, final DeviceService _deviceService,
                             final ContractService _contractService, final StoricoAziendaRepository _storicoStatoAziendaRepository) {
    customerRepository = _customerRepository;
    deviceService = _deviceService;
    contractService = _contractService;
    storicoStatoAziendaRepository = _storicoStatoAziendaRepository;
  }

  @Override
  public Optional<Customer> findCustomerByUuid(@NotNull final String _customerUuid) {

    _log.debug("Searching customer for uuid {} ...", _customerUuid);

    final var optionalCustomer = customerRepository.findCustomerByUuid(_customerUuid);

    optionalCustomer.ifPresentOrElse(customer -> _log.debug("... found {}", customer),
                                     () -> _log.info("... customer uuid {} not found", _customerUuid));

    return optionalCustomer;
  }

  @Override
  public Optional<Customer> findCustomerByContrattoNumero(@NotNull final String _contrattoNumero) {

    _log.debug("Searching customer for uuid {} ...", _contrattoNumero);

    final var optionalCustomer = customerRepository.findCustomerByContrattoNumero(_contrattoNumero);

    optionalCustomer.ifPresentOrElse(customer -> _log.debug("... found {}", customer),
                                     () -> _log.info("... customer uuid {} not found", _contrattoNumero));

    return optionalCustomer;
  }

  @Override
  public Optional<Customer> findCustomerByDeviceSeriale(@NotNull String _deviceSeriale, @NotNull final TipoDispositivoEnum _deviceType) {

    _log.debug("Searching customer for device seriale {} type {} ...", _deviceSeriale,_deviceType);

    final var optionalCustomer = deviceService.findDeviceBySeriale(_deviceSeriale,_deviceType, null)
                                              .flatMap(device -> Optional.ofNullable(device.getContractNumber())
                                                                         .flatMap(contractId -> contractService.findContractByNumber(contractId)
                                                                                                               .flatMap(contract -> findCustomerByUuid(contract.getCompanyCode()))));

    optionalCustomer.ifPresentOrElse(customer -> _log.debug("... found {}", customer),
                                     () -> _log.info("... customer for device uuid {} not found", _deviceSeriale));

    return optionalCustomer;
  }

  @Override
  public boolean isCustomerActiveInDate(String _companyCode, Instant _date) {
    Optional<StoricoAzienda> customer = storicoStatoAziendaRepository.findFirstByCodiceAziendaAndDataVariazioneBeforeOrderByDataVariazioneDesc(_companyCode,_date);
    return customer.isPresent() && STATO_AZIENDA_ATTIVO.equals(customer.get().getStatoAzienda());
  }

}
