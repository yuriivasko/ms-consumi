package it.fai.ms.consumi.service.dettaglio_stranziamento;

import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;

/*
 * Utilizzata per il cambio prezzo da PriceTable
 */
@Service
@Transactional
public class DettaglioStanziamentoCarburanteServiceImpl implements DettaglioStanziamentoCarburanteService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamantiRepository dettaglioStanziamentoRepo;

  private final StanziamentoRepository stanziamentoRepository;

  public DettaglioStanziamentoCarburanteServiceImpl(final DettaglioStanziamantiRepository _dettaglioStanziamentoRepo,
                                                    final StanziamentoRepository _stanziamentoRepository) {
    dettaglioStanziamentoRepo = _dettaglioStanziamentoRepo;
    stanziamentoRepository = _stanziamentoRepository;
  }

  @Override
  public void save(DettaglioStanziamentoCarburante entity) {
    dettaglioStanziamentoRepo.save(entity);
  }

  @Override
  public void saveEntity(DettaglioStanziamentoCarburanteEntity entity) {
    dettaglioStanziamentoRepo.save(entity);
  }

  @Override
  public DettaglioStanziamentoCarburanteEntity update(DettaglioStanziamentoCarburanteEntity entity) {
    if (entity.getId() == null) {
      log.info("New Entity {}", entity.getClass()
                                      .getSimpleName());
    } else {
      log.info("Update Entity {}", entity.getId());
    }
    return (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamentoRepo.update(entity);
  }

  @Override
  public Set<DettaglioStanziamentoCarburante> findByCodiceStanziamento(final String codiceStanziamento) {
    Set<DettaglioStanziamento> dettagliStanziamenti = dettaglioStanziamentoRepo.findByCodiceStanziamento(codiceStanziamento);
    log.debug("Found dettagli stanziamenti: {}", dettagliStanziamenti);
    if (dettagliStanziamenti == null || dettagliStanziamenti.isEmpty()) {
      return null;
    }

    return dettagliStanziamenti.stream()
                               .filter(d -> (d instanceof DettaglioStanziamentoCarburante))
                               .map(d -> (DettaglioStanziamentoCarburante) d)
                               .collect(toSet());
  }

  @Override
  public List<DettaglioStanziamentoCarburanteEntity> findToUpdatePriceTableOpen(PriceTableFilterDTO priceTableFilter) {
    return findToUpdatePriceTableClosed(priceTableFilter);
  }

  @Override
  public List<DettaglioStanziamentoCarburanteEntity> findToUpdatePriceTableClosed(PriceTableFilterDTO priceTableFilter) {
    List<DettaglioStanziamentoCarburanteEntity> dsCarburanteEntity = new ArrayList<>();
    dsCarburanteEntity = dettaglioStanziamentoRepo.findToUpdatePriceTable(priceTableFilter);
    return dsCarburanteEntity;
  }

  @Override
  public StanziamentoEntity cloneStanziamentoCarburanteFromProvvisorio(StanziamentoEntity s) {
    return stanziamentoRepository.cloneAndModify(s, null);
  }

  @Override
  public StanziamentoEntity cloneStanziamentoCarburanteFromProvvisorio(StanziamentoEntity s, InvoiceType state) {
    return stanziamentoRepository.cloneAndModify(s, state);
  }

  @Override
  public DettaglioStanziamentoCarburanteEntity cloneDettaglioStanziamento(DettaglioStanziamentoCarburanteEntity dsEntity) {
    log.info("Cloning Entity: {}", dsEntity);
    return (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamentoRepo.cloneAndModify(dsEntity);
  }

  @Override
  public void saveStanziamento(@NotNull StanziamentoEntity stanziamento) {
    stanziamentoRepository.create(stanziamento);
  }

  @Override
  public DettaglioStanziamentoCarburante mapToDomain(DettaglioStanziamentoCarburanteEntity dsEntity) {
    return (DettaglioStanziamentoCarburante) dettaglioStanziamentoRepo.mapToDomain(dsEntity);
  }

  @Override
  public DettaglioStanziamentoCarburanteEntity mapToEntity(DettaglioStanziamentoCarburante ds) {
    return (DettaglioStanziamentoCarburanteEntity) dettaglioStanziamentoRepo.mapToEntity(ds);
  }

  @Override
  public void updateStanziamento(StanziamentoEntity stanziamento) {
    stanziamentoRepository.updateEntity(stanziamento);
    log.debug("Updated: {}", stanziamento);
  }

}
