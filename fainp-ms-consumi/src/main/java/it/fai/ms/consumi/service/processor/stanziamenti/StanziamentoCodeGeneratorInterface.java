package it.fai.ms.consumi.service.processor.stanziamenti;

import it.fai.ms.consumi.domain.InvoiceType;

public interface StanziamentoCodeGeneratorInterface {

  String generate(final InvoiceType _invoiceType);
}
