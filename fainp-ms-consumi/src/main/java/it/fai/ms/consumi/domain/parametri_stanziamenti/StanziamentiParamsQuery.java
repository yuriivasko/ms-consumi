package it.fai.ms.consumi.domain.parametri_stanziamenti;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

public class StanziamentiParamsQuery {

  private String       fareClass             = "";
  private final String recordCode;
  private final String source;
  private String       transactionDetail     = "";
  private BigDecimal   vatRate               = new BigDecimal("0.0").setScale(2, RoundingMode.HALF_UP);
  private boolean      vatMandatory = true;
  private String       codiceFornitoreLegacy = "";

  public StanziamentiParamsQuery(final String _source, final String _recordCode) {
    source = Objects.requireNonNull(_source, "Parameter source is mandatory");
    recordCode = Objects.requireNonNull(_recordCode, "Parameter record code is mandatory");
  }

  public String getFareClass() {
    return this.fareClass;
  }

  public String getRecordCode() {
    return this.recordCode;
  }

  public String getSource() {
    return this.source;
  }

  public String getTransactionDetail() {
    return this.transactionDetail;
  }

  public BigDecimal getVatRate() {
    return this.vatRate;
  }

  public void setFareClass(final String _fareClass) {
    this.fareClass = _fareClass;
  }

  public void setTransactionDetail(final String _transactionDetail) {
    this.transactionDetail = _transactionDetail;
  }

  public void setVatRate(final BigDecimal _vatRate) {
    this.vatRate = _vatRate;
  }

  public String getCodiceFornitoreLegacy() {
    return codiceFornitoreLegacy;
  }

  public void setCodiceFornitoreLegacy(String codiceFornitoreLegacy) {
    this.codiceFornitoreLegacy = codiceFornitoreLegacy;
  }

  public boolean isVatMandatory() {
    return vatMandatory;
  }

  public void setVatMandatory(boolean vatMandatory) {
    this.vatMandatory = vatMandatory;
  }

  @Override
  public String toString() {
    return "StanziamentiParamsQuery [fareClass=" + fareClass + ", recordCode=" + recordCode + ", source=" + source + ", transactionDetail="
           + transactionDetail + ", vatRate=" + vatRate + ", codiceFornitoreLegacy=" + codiceFornitoreLegacy + ", vatMandatory=" + vatMandatory + "]";
  }
}
