package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElceuDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.generici.HgvRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class HgvRecordDescriptor
  implements RecordDescriptor<HgvRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(HgvRecordDescriptor.class);

  private final List<String> HEADER = Arrays.asList(
    "numeroImmatricolazioneVeicolo", "paese", "fascia", "categoriaEuro", "dataInizio", "dataFine", "importo", "stato", "dataAcquisto", "acquistatoDa"
  );

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public String getHeaderBegin() {
    return "Numero di immatricolazione del veicolo,Paese,Fascia,Categoria Euro,Data di inizio,Data di fine,Importo,Stato,Data di acquisto,Acquistato da";
  }

  @Override
  public String getFooterCodeBegin() {
    return "§ NO HEADER / FOOTER FILE §";
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  @Override
  public String extractRecordCode(String _data) {
    return "";
  }

  @Override
  public HgvRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {
    HgvRecord tollCollectRecord = new HgvRecord(fileName, rowNumber);
    tollCollectRecord.setRecordCode("DEFAULT");
    tollCollectRecord.setTransactionDetail("");
    if (rowNumber != 0) {
      tollCollectRecord.setFromCsvRecord(_data, HEADER);
    }
    return tollCollectRecord;
  }

  protected Date getNow() {
    return new Date();
  }

  public void checkConfiguration(){
    log.trace("No need to check configuration...");
  }
}
