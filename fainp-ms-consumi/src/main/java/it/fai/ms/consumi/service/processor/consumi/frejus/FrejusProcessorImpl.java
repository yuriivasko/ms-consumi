package it.fai.ms.consumi.service.processor.consumi.frejus;

import java.util.Optional;

import javax.inject.Inject;
import javax.money.MonetaryAmount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.jms.producer.CambioStatoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class FrejusProcessorImpl extends ConsumoAbstractProcessor implements ConsumoProcessor<Frejus> {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final StanziamentiProducer                             stanziamentiProducer;
  private final FrejusConsumoDettaglioStanziamentoPedaggioMapper mapper;


  private final CambioStatoDaConsumoJmsProducer cambioStatoProducer;

  @Inject
  public FrejusProcessorImpl(final ConsumoValidatorService _consumoValidatorService, final StanziamentiProducer _stanziamentiProducer,
                             final FrejusConsumoDettaglioStanziamentoPedaggioMapper _mapper, final VehicleService _vehicleService,
                             final CustomerService _customerService, CambioStatoDaConsumoJmsProducer cambioStatoProducer) {
    super(_consumoValidatorService, _vehicleService, _customerService);
    stanziamentiProducer          = _stanziamentiProducer;
    mapper                        = _mapper;
    this.cambioStatoProducer      = cambioStatoProducer;
  }

  @Override
  public ProcessingResult<Frejus> validateAndProcess(final Frejus _sitaf, final Bucket _bucket) {
    return executeProcess(_sitaf, _bucket);
  }

  private ProcessingResult<Frejus> executeProcess(final Frejus _consumo, final Bucket _bucket) {

    _log.info(">");
    _log.info("{} processing start {}", _bucket, _consumo);
    _log.info("  --> {}", _consumo);

    var processingResult = validateAndNotify(_consumo);

    _log.debug(" - {} processing validation completed", _consumo);

    final var consumo = processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams().ifPresent(stanziamentiParams -> {

        Amount               consumoAmount     = consumo.getAmount();
        final MonetaryAmount amountIncludedVat = consumoAmount.getAmountIncludedVat();
        setVatRateFromStanziamentiParams(consumo, Optional.empty(), Optional.of(amountIncludedVat), stanziamentiParams);

        final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo, mapper);
        final var stanziamenti          = stanziamentiProducer.produce(dettaglioStanziamento, stanziamentiParams);
        processingResult.getStanziamenti().addAll(stanziamenti);


      });
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }
    if (!processingResult.getValidationOutcome().isFatalError() && consumo.getDevice()!=null) {
      TipoDispositivoEnum type = consumo.getDevice().getType();
      if (type == TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_FR || type == TipoDispositivoEnum.BUONI_FREJUS_MONTE_BIANCO_IT) {
        cambioStatoProducer.sendFatturato(consumo.getDevice(), consumo.getDate());
      }
    }
    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", _consumo.getSource());
    _log.info("<");

    return processingResult;
  }

}
