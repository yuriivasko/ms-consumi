package it.fai.ms.consumi.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import it.fai.ms.common.jms.fattura.canoni.EsitoCanoni;
import it.fai.ms.consumi.adapter.nav.auth.NavAuthorizationTokenService;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.client.AnagaziendeService;
import it.fai.ms.consumi.client.AnagaziendeServiceImpl;
import it.fai.ms.consumi.client.CanoniApiClient;
import it.fai.ms.consumi.client.StanziamentiApiAuthClient;
import it.fai.ms.consumi.client.StanziamentiApiService;
import it.fai.ms.consumi.client.StanziamentiApiServiceImpl;
import it.fai.ms.consumi.client.StanziamentiApiServiceReadOnlyImpl;
import it.fai.ms.consumi.service.canoni.CanoniClientService;
import it.fai.ms.consumi.service.canoni.CanoniClientServiceImpl;

@Configuration
public class RestClientConfiguration {


  @Autowired
  StanziamentiApiAuthClient stanziamentiApiAuthClient;

  @Autowired
  AnagaziendeClient anagaziendeClient;

  @Profile({ "test", "prod" })
  @Bean(name = "stanziamentiApiService")
  public StanziamentiApiService getStanziamentiApiService() {
    return new StanziamentiApiServiceImpl(stanziamentiApiAuthClient);
  }

  @Profile({ "!test", "!prod" })
  @Bean(name = "stanziamentiApiService")
  public StanziamentiApiService getStanziamentiApiServiceMock() {
    return new StanziamentiApiServiceReadOnlyImpl(stanziamentiApiAuthClient);
  }

  @Bean(name = "anagaziendeService")
  public AnagaziendeService getAnagaziendeService(ApplicationProperties applicationProperties) {
    return new AnagaziendeServiceImpl(applicationProperties, anagaziendeClient);
  }

  @Profile({ "test", "prod","testCanoniNavApi" })
  @Bean(name = "canoniClientService")
  public CanoniClientService getCanoniClientServiceImpl(NavAuthorizationTokenService navApiAuthClient,CanoniApiClient canoniClient) {
    return new CanoniClientServiceImpl(navApiAuthClient,canoniClient);
  }

  @Profile({ "!test", "!prod","!testCanoniNavApi" })
  @Bean(name = "canoniClientService")
  public CanoniClientService getCanoniClientServiceImplMock() {
    return new CanoniClientService() {
      private Logger log = LoggerFactory.getLogger(getClass());
      @Override
      public void postCanoniServizi(EsitoCanoni esito) {
        log.info("mock to send Canoni Servizi {}",esito);
      }

      @Override
      public void postCanoniDispositivi(EsitoCanoni esito) {
        log.info("mock to send Canoni Servizi {}",esito);
      }
    };
  }

}
