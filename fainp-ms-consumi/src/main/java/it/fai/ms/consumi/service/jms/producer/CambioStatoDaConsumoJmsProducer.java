package it.fai.ms.consumi.service.jms.producer;

import java.time.Instant;

import org.springframework.stereotype.Service;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.ms.common.jms.JmsDestination;
import it.fai.ms.common.jms.JmsObjectMessageSenderTemplate;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.efservice.message.consumi.CambioStatoDaConsumo;
import it.fai.ms.consumi.domain.Device;

@Service
public class CambioStatoDaConsumoJmsProducer extends JmsObjectMessageSenderTemplate<CambioStatoDaConsumo> {

  public CambioStatoDaConsumoJmsProducer(JmsProperties jmsProperties) {
    super(jmsProperties);
  }

  @Override
  public JmsDestination getJmsDestination() {
    return JmsQueueNames.CAMBIO_STATO_DA_CONSUMO;
  }

  public void sendFatturato(Device device, Instant dataModifica) {
    CambioStatoDaConsumo message = new CambioStatoDaConsumo(device.getType(),  device.getSeriale(), device.getDeviceUUID(),StatoDispositivo.FATTURATO);
    message.setDataModificaStato(dataModifica);
    sendMessage(message);

  }
}
