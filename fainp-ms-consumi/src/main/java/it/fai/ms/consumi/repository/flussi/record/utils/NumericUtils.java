package it.fai.ms.consumi.repository.flussi.record.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NumericUtils {

  public static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
  
  private static DecimalFormat df2 = new DecimalFormat(".##");

  protected Logger logger = LoggerFactory.getLogger(getClass());

  static {
    df2.setRoundingMode(RoundingMode.CEILING);
  }

  public NumericUtils() {
    super();
  }

  public static Integer strToInt(String value) {
    return ((value != null) && !value.isEmpty()) ? Integer.valueOf(value) : 0;
  }

  public static Long strToLong(String value) {
    return ((value != null) && !value.isEmpty()) ? Long.valueOf(value) : 0L;
  }

  public static Double strToDouble(String value) {
    return strToDouble(value, 1);
  }

  public static Double strToDouble(String value, int factor) {
    return ((value != null) && !value.isEmpty()) ? Double.valueOf(value.replace(",", ".")) / factor : 0.0;
  }

  public static Float strToFloat(String value, int factor) {
    return ((value != null) && !value.isEmpty()) ? Float.valueOf(value.replace(",", ".")) / factor : 0.0F;
  }
  
  private static BigDecimal scaleRound(BigDecimal numberToScale, int scale) {
    return scaleRound(numberToScale,scale,null);
  }
  public static BigDecimal scaleRound(BigDecimal numberToScale, int scale, RoundingMode roundingMode) {
    if (scale<0) {
      return numberToScale.movePointLeft(-1*scale);
    }
    return roundingMode!=null?numberToScale.setScale(scale,roundingMode):numberToScale.setScale(scale);
  }
  
  public static BigDecimal strToBigDecimal(String value) {
    return ((value != null) && !value.isEmpty()) ? new BigDecimal(value.replace(",", ".")) : BigDecimal.ZERO;
  }

  public static BigDecimal strToBigDecimal(String value, int scaleRoundUnaware) {
    return ((value != null) && !value.isEmpty()) ? scaleRound(new BigDecimal(value.replace(",", ".")),scaleRoundUnaware) : BigDecimal.ZERO;
  }

  public static BigDecimal strToBigDecimal(String value, int scaleRoundUnaware, int factor) {
    return ((value != null) && !value.isEmpty()) ? scaleRound(new BigDecimal(value.replace(",", ".")),scaleRoundUnaware).divide(new BigDecimal(factor)) : BigDecimal.ZERO;
  }

  public static Double fromString(String value) {
    return Double.parseDouble(value);
  }

  public static Double fromString(String integer, String decimal) {
    return fromString(integer + "." + decimal);
  }

  public static Double CurrencyRound(Double value) {
    return (value != null) ? Double.valueOf(df2.format(value)) : null;
  }

  public static int NextInt(int min, int max) {
    return ThreadLocalRandom.current()
                            .nextInt(min, max + 1);
  }

  public static boolean isNumeric(String s) {
    return (s != null) && s.matches("[-+]?\\d*\\.?\\d+");
  }

  public static boolean isBetween(Double value, double min, double max) {
    return value != null ? (value >= min) && (value <= max) : false;
  }

  public static boolean isBetween(String value, double min, double max) {
    return value != null ? isBetween(strToDouble(value), min, max) : false;
  }

  public boolean isDouble(String number) {
    try {
      Double.parseDouble(number);
    } catch (NumberFormatException e) {
      logger.error("{}", e.getMessage());
      return false;
    }
    return true;
  }

}
