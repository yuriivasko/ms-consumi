package it.fai.ms.consumi.service.validator;

import java.util.HashMap;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.jms.notification_v2.ConsumoReprocessableNotification;
import it.fai.ms.consumi.jms.notification_v2.ConsumoWarningNotification;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.processor.ProcessingResult;

@Service
@Primary
public class ConsumoValidatorServiceImpl implements ConsumoValidatorService {

  private final transient Logger log = LoggerFactory.getLogger(getClass());

  private final ConsumoBlockingValidatorService   blockingValidatorService;
  private final ConsumoNoBlockingValidatorService noBlockingValidatorService;
  private final ConsumoWarningValidatorService    warningValidatorService;
  private final NotificationService               notificationService;

  private final Environment env;

  @Inject
  public ConsumoValidatorServiceImpl(final Environment _env, final ConsumoBlockingValidatorService _blockingValidationsService,
                                     final ConsumoNoBlockingValidatorService _noBlockingValidatorService,
                                     final NotificationService _notificationService) {
    env = _env;
    blockingValidatorService = _blockingValidationsService;
    noBlockingValidatorService = _noBlockingValidatorService;
    warningValidatorService = null;
    this.notificationService = _notificationService;
  }

  public ConsumoValidatorServiceImpl(final Environment _env, final ConsumoBlockingValidatorService _blockingValidationsService,
                                     final ConsumoWarningValidatorService _warningValidatorService,
                                     final NotificationService _notificationService) {
    env = _env;
    blockingValidatorService = _blockingValidationsService;
    noBlockingValidatorService = null;
    warningValidatorService = _warningValidatorService;
    this.notificationService = _notificationService;
  }

  @Override
  public ValidationOutcome validate(@NotNull final Consumo _consumo) {

    ValidationOutcome validationOutcome = null;
    Optional<StanziamentiParams> stanziamentiParamsOptional = Optional.empty();

    final ValidationOutcome blockingValidationOutcome = blockingValidatorService.validate(_consumo);
    if (blockingValidationOutcome.isValidationOk()) {
      stanziamentiParamsOptional = blockingValidationOutcome.getStanziamentiParamsOptional();
      if (noBlockingValidatorService != null) {
        noBlockingValidatorService.storeForPostValidation(_consumo);
        validationOutcome = new ValidationOutcome();
      } else {
        final ValidationOutcome warningValidation = warningValidatorService.validate(_consumo);
        if (warningValidation.isValidationOk()) {
          validationOutcome = new ValidationOutcome();
        } else {
          validationOutcome = warningValidation;
        }
      }
    } else {
      validationOutcome = blockingValidationOutcome;
      validationOutcome.setFatalError(true);
    }
    if (stanziamentiParamsOptional.isPresent()) {
      validationOutcome.setStanziamentiParams(stanziamentiParamsOptional.get());
    }
    return validationOutcome;
  }

  @Override
  @Async
  public <C extends Consumo> void notifyProcessingResult(final ProcessingResult<C> processingResult) {
    C _consumo = processingResult.getConsumo();
    CommonRecord record = _consumo.getRecord();
    ValidationOutcome validationOutcome = processingResult.getValidationOutcome();
    Set<Message> messages = validationOutcome.getMessages();

    messages.forEach(message -> {
//      var messageTextKV = new HashMap<String, Object>();
//      messageTextKV.put("job_name", _consumo.getJobName());
//      messageTextKV.put("original_message", message.getText());
//      String original_record = Optional.ofNullable(_consumo.getRecord()).map(commonRecord -> commonRecord.toNotificationMessage()).orElse("no record");
//      messageTextKV.put("original_record", original_record);
//        notificationService.notify("CPV-" + message.getCode(), messageTextKV);

      if (validationOutcome.isFatalError()){
        notificationService.notify(new ConsumoReprocessableNotification(
          _consumo.getJobName(),
          record.getFileName(),
          record.getRowNumber(),
          record.getClass().getSimpleName(),
          record.getUuid(),
          record.getRecordCode(),
          record.getTransactionDetail(),
          record.toNotificationMessage(),
          "CPV-" + message.getCode(),
          message.getText()
        ));
      } else {
        notificationService.notify(new ConsumoWarningNotification(
          _consumo.getJobName(),
          record.getFileName(),
          record.getRowNumber(),
          record.getClass().getSimpleName(),
          record.getUuid(),
          record.getRecordCode(),
          record.getTransactionDetail(),
          record.toNotificationMessage(),
          "CPV-" + message.getCode(),
          message.getText()
        ));
      }

    });
  }

}
