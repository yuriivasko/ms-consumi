package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;

@Component
@Validated
public class DettaglioStanziamentoCarburanteEntityMapper {

  public DettaglioStanziamentoCarburanteEntityMapper() {
  }

  public DettaglioStanziamentoCarburante toDomain(@NotNull final DettaglioStanziamentoCarburanteEntity _entity) {
    final var allocationDetailDomain = new DettaglioStanziamentoCarburante(new GlobalIdentifier(GlobalIdentifier.decodeType(_entity.getGlobalIdentifier())) {
      @Override
      public String getId() {
        return decodeId(_entity.getGlobalIdentifier());
      }

      @Override
      public GlobalIdentifier newGlobalIdentifier() {
        return newGlobalIdentifier();
      }
    });

    allocationDetailDomain.setSource(new Source(_entity.getIngestionTime(), _entity.getDataAcquisizioneFlusso(), _entity.getTipoSorgente().name()));

    FuelStation fuelStation = new FuelStation(_entity.getPuntoErogazione()); // FIXME
    fuelStation.setFuelStationCode(_entity.getPuntoErogazione());
    fuelStation.setPumpNumber(_entity.getErogatore());
    allocationDetailDomain.setFuelStation(fuelStation);
    Transaction transaction = new Transaction();
    transaction.setDetailCode(_entity.getTransactionDetailCode());
    allocationDetailDomain.setTransaction(transaction);

    allocationDetailDomain.setOtherSupport(_entity.getAltroSupporto());

    allocationDetailDomain.setCustomer(new Customer(_entity.getCustomerId()));
    if(_entity.getContractCode() != null) {
      allocationDetailDomain.setContract(new Contract(_entity.getContractCode()));
    }

    allocationDetailDomain.setEntryDateTime(_entity.getDataOraUtilizzo()); // FIXME

    allocationDetailDomain.setRecordCode(_entity.getRecordCode());
    allocationDetailDomain.setQuantity(_entity.getQuantita());
    allocationDetailDomain.setKilometers(_entity.getKm());
    allocationDetailDomain.setMesaurementUnit(_entity.getUnitaMisura());

    allocationDetailDomain.setContract(Contract.newUnsafeContract(_entity.getContractCode(), _entity.getCustomerId(), null));

    allocationDetailDomain.setCustomer(new Customer(_entity.getCustomerId()));
    allocationDetailDomain.setSupplier(new Supplier(_entity.getCodiceFornitoreNav()));
    final VehicleLicensePlate vehicleLicensePlate = new VehicleLicensePlate(_entity.getVeicoloTarga(), _entity.getVeicoloNazioneTarga());

    final Vehicle vehicle = new Vehicle();
    vehicle.setEuroClass(_entity.getVeicoloClasse());
    vehicle.setLicensePlate(vehicleLicensePlate);
    allocationDetailDomain.setVehicle(vehicle);

    allocationDetailDomain.setStorno(_entity.isStorno());

    if (_entity.getPrezzoUnitarioDiRiferimentoNoiva() != null) {
      Amount priceExposed = new Amount();
      priceExposed.setVatRate(_entity.getPercIva());
      priceExposed.setAmountIncludedVat(Money.of(_entity.getPrezzoUnitarioDiRiferimentoNoiva(), _entity.getCurrencyCode()));
      allocationDetailDomain.setPriceReference(priceExposed);
    }

    if (_entity.getPrezzoUnitarioCalcolatoNoiva() != null) {
      Amount priceComputed = new Amount();
      priceComputed.setVatRate(_entity.getPercIva());
      priceComputed.setAmountExcludedVat(Money.of(_entity.getPrezzoUnitarioCalcolatoNoiva(), _entity.getCurrencyCode()));
      allocationDetailDomain.setPriceComputed(priceComputed);
    }

    if (_entity.getCostoUnitarioCalcolatoNoiva() != null) {
      Amount costComputed = new Amount();
      costComputed.setVatRate(_entity.getPercIva());
      costComputed.setAmountExcludedVat(Money.of(_entity.getCostoUnitarioCalcolatoNoiva(), _entity.getCurrencyCode()));
      allocationDetailDomain.setCostComputed(costComputed);
    }

    allocationDetailDomain.setInvoiceType(_entity.getTipoConsumo());
    return allocationDetailDomain;
  }

  public DettaglioStanziamentoCarburanteEntity toEntity(@NotNull final DettaglioStanziamentoCarburante _domain) {
    var allocationDetailEntity = new DettaglioStanziamentoCarburanteEntity(_domain.getGlobalIdentifier()
                                                                                  .encode());

    allocationDetailEntity.setPuntoErogazione(_domain.getFuelStation()
                                                     .getFuelStationCode());

    allocationDetailEntity.setCodiceTrackycard(_domain.getCodiceTrackycard());

    allocationDetailEntity.setAltroSupporto(_domain.getOtherSupport());
    allocationDetailEntity.setCustomerId((_domain.getCustomer() != null ? _domain.getCustomer()
                                                                                 .getId()
                                                                        : null));
    allocationDetailEntity.setContractCode(_domain.getContract()!=null ? _domain.getContract().getId() : null);
    allocationDetailEntity.setSourceRowNumber(_domain.getSource().getRowNumber());
    allocationDetailEntity.setDataOraUtilizzo(_domain.getEntryDateTime());
    allocationDetailEntity.setErogatore((_domain.getFuelStation() != null ? _domain.getFuelStation()
                                                                                   .getPumpNumber()
                                                                          : null));
    allocationDetailEntity.setTransactionDetailCode(null);
    if (_domain.getTransaction() != null) {
      allocationDetailEntity.setTransactionDetailCode(_domain.getTransaction()
                                                             .getDetailCode());
    }
    allocationDetailEntity.setRecordCode(_domain.getRecordCode());
    allocationDetailEntity.setQuantita((_domain.getQuantity()));
    allocationDetailEntity.setKm(_domain.getKilometers() );
    allocationDetailEntity.setUnitaMisura(_domain.getMesaurementUnit());

    allocationDetailEntity.setCodiceFornitoreNav((_domain.getSupplier() != null ? _domain.getSupplier()
                                                                                         .getCode()
                                                                                : null));
    if (_domain.getPriceReference() != null)
      allocationDetailEntity.setPrezzoUnitarioDiRiferimentoNoiva(new BigDecimal(_domain.getPriceReference()
                                                                                       .getAmountExcludedVat()
                                                                                       .getNumber().toString()
                                                                                       ));
    if (_domain.getPriceComputed() != null)
      allocationDetailEntity.setPrezzoUnitarioCalcolatoNoiva(new BigDecimal(_domain.getPriceComputed()
                                                                                   .getAmountExcludedVat()
                                                                                   .getNumber()
                                                                                   .toString()));
    if (_domain.getCostComputed() != null)
      allocationDetailEntity.setCostoUnitarioCalcolatoNoiva(new BigDecimal(_domain.getCostComputed()
                                                                                  .getAmountExcludedVat()
                                                                                  .getNumber()
                                                                                  .toString()));

    // amount = priceComputed != null ? priceComputed : costComputed;
    final Amount domainAmount = _domain.getAmount();
    BigDecimal vatRate = domainAmount.getVatRateBigDecimal();
    String currencyCode = domainAmount.getCurrency()
                                      .getCurrencyCode();
    BigDecimal exchangeRate = domainAmount.getExchangeRate();

    allocationDetailEntity.setPercIva(vatRate);
    allocationDetailEntity.setCurrencyCode(currencyCode);
    allocationDetailEntity.setExchangeRate(exchangeRate);
    allocationDetailEntity.setFileName((_domain.getSource() != null ? _domain.getSource()
                                                                             .getFileName()
                                                                    : null));
    allocationDetailEntity.setTipoConsumo(_domain.getInvoiceType());
    allocationDetailEntity.setTipoSorgente((_domain.getServicePartner() != null ? _domain.getServicePartner()
                                                                                         .getFormat()
                                                                                : null));
    allocationDetailEntity.setCodiceFornitoreNav(_domain.getNavSupplierCode());
    allocationDetailEntity.setPartnerCode((_domain.getServicePartner() != null ? _domain.getServicePartner()
                                                                                        .getPartnerCode()
                                                                               : null));

    allocationDetailEntity.setIngestionTime(_domain.getSource().getIngestionTime());
    allocationDetailEntity.setDataAcquisizioneFlusso(_domain.getSource().getAcquisitionDate());

    allocationDetailEntity.setVeicoloClasse((_domain.getVehicle() != null ? _domain.getVehicle()
                                                                                   .getEuroClass()
                                                                          : null));
    allocationDetailEntity.setVeicoloTarga((_domain.getVehicle() != null ? (_domain.getVehicle()
                                                                                   .getLicensePlate() != null ? _domain.getVehicle()
                                                                                                                       .getLicensePlate()
                                                                                                                       .getLicenseId()
                                                                                                              : null)
                                                                         : null));
    allocationDetailEntity.setVeicoloNazioneTarga(_domain.getVehicle() != null ? (_domain.getVehicle()
                                                                                         .getLicensePlate() != null ? _domain.getVehicle()
                                                                                                                             .getLicensePlate()
                                                                                                                             .getCountryId()
                                                                                                                    : null)
                                                                               : null);

    allocationDetailEntity.setStorno((_domain.isStorno() != null) ? _domain.isStorno() : false);

    return allocationDetailEntity;
  }

}
