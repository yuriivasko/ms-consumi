package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CurrencyRate.
 */
@Entity
@Table(name = "currency_rate")
public class CurrencyRate implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
  @SequenceGenerator(name = "sequenceGenerator")
  private Long id;

  @Column(name = "jhi_time")
  private Instant time;

  @Column(name = "currency")
  private String currency;

  @Column(name = "rate", precision = 10, scale = 2)
  private BigDecimal rate;

  // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Instant getTime() {
    return time;
  }

  public CurrencyRate time(Instant time) {
    this.time = time;
    return this;
  }

  public void setTime(Instant time) {
    this.time = time;
  }

  public String getCurrency() {
    return currency;
  }

  public CurrencyRate currency(String currency) {
    this.currency = currency;
    return this;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public BigDecimal getRate() {
    return rate;
  }

  public CurrencyRate rate(BigDecimal rate) {
    this.rate = rate;
    return this;
  }

  public void setRate(BigDecimal rate) {
    this.rate = rate;
  }
  // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CurrencyRate currencyRate = (CurrencyRate) o;
    if (currencyRate.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), currencyRate.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "CurrencyRate{" + "id=" + getId() + ", time='" + getTime() + "'" + ", currency='" + getCurrency() + "'" + ", rate=" + getRate()
           + "}";
  }
}
