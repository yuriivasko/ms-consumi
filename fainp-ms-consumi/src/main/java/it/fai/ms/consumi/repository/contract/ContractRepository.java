package it.fai.ms.consumi.repository.contract;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

public interface ContractRepository {

  Optional<Contract> findContractByUuid(@NotNull String contractUuid);

  Optional<StoricoContratto> findContrattoAtDate(String idContratto, Instant dataRiferimentoInizio);

}
