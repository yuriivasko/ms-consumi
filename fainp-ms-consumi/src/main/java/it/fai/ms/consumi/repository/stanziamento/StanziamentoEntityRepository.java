package it.fai.ms.consumi.repository.stanziamento;

import java.time.Instant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

@Repository
public interface StanziamentoEntityRepository extends JpaRepository<StanziamentoEntity, String> {

	Page<StanziamentoEntity> findByDataAcquisizioneFlussoAndDateQueuingIsNullAndDateNavSentIsNull(Instant data, Pageable pageable);

	Page<StanziamentoEntity> findByDataAcquisizioneFlussoAndDateNavSentIsNull(Instant data, Pageable pageable);
  
  

}
