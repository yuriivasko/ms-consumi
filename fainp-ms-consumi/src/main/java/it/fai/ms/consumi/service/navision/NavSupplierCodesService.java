package it.fai.ms.consumi.service.navision;

import java.util.Set;

public interface NavSupplierCodesService {

  Set<String> getAllCodes();

}
