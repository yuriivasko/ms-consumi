package it.fai.ms.consumi.repository.flussi.record.model.carburanti;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TrackyCardCarburantiJollyRecord
  extends TrackyCardCarburantiStandardRecord {

  String customerCode;
  String country;
  String licencePlate;
  String euroClass;

  @JsonCreator
  public TrackyCardCarburantiJollyRecord(@JsonProperty("fileName") String fileName, @JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public void setCustomerCode(String customerCode) {
    this.customerCode = customerCode;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getLicencePlate() {
    return licencePlate;
  }

  public void setLicencePlate(String licencePlate) {
    this.licencePlate = licencePlate;
  }

  public String getEuroClass() {
    return euroClass;
  }

  public void setEuroClass(String euroClass) {
    this.euroClass = euroClass;
  }

  @Override
  public String toString() {
    final String s = super.toString().replace("TrackyCardCarburantiStandardRecord{", "").replace("}", "");
    return "TrackyCardCarburantiJollyRecord{" + s + ", customerCode='" + customerCode + '\'' + ", country='" + country + '\'' + ", licencePlate='"
           + licencePlate + '\'' + ", euroClass='" + euroClass + '\''  + "}";
  }

}
