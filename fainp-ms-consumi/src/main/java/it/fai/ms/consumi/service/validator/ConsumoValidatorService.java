package it.fai.ms.consumi.service.validator;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.domain.consumi.Consumo;

public interface ConsumoValidatorService {

  ValidationOutcome validate(@NotNull Consumo consumo);

  <C extends Consumo> void notifyProcessingResult(ProcessingResult<C> processingResult);

}
