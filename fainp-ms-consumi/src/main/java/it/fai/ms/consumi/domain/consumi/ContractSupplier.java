package it.fai.ms.consumi.domain.consumi;

import java.util.Optional;

import it.fai.ms.consumi.domain.Contract;

public interface ContractSupplier {

  Optional<Contract> getContract();

}
