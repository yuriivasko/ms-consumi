package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import java.time.Instant;

import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(TrackyCardPedaggiStandardJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class TrackyCardPedaggiStandardJob extends AbstractJob<TrackyCardPedaggiStandardRecord, TrackyCardPedaggiStandardConsumo> {
  
  public final static String QUALIFIER = "trackyCard";
  

  public TrackyCardPedaggiStandardJob(PlatformTransactionManager transactionManager,
                    StanziamentiParamsValidator stanziamentiParamsValidator,
                    NotificationService notificationService,
                    RecordPersistenceService persistenceService,
                    RecordDescriptor<TrackyCardPedaggiStandardRecord> recordDescriptor,
                    ConsumoProcessor<TrackyCardPedaggiStandardConsumo> consumoProcessor,
                    RecordConsumoMapper<TrackyCardPedaggiStandardRecord, TrackyCardPedaggiStandardConsumo> recordConsumoMapper,
                    StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {
    super("trackyCard", transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor,
          consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
  }
  
  @Override
  protected RecordsFileInfo validateFile(String _filename, long _startTime, Instant _ingestionTime) {
    //no generic flat file validation: is CSV!
    var fileInfo = new RecordsFileInfo(_filename, _startTime, _ingestionTime);
    DartfordRecord header = new DartfordRecord(_filename, 0);
    fileInfo.setHeader(header);
    return fileInfo;
  }


  @Override
  public Format getJobQualifier() {
    return Format.TRACKY_CARD_PEDAGGI;
  }
}
 
