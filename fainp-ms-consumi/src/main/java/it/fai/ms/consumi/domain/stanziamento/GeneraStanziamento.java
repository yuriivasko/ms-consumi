package it.fai.ms.consumi.domain.stanziamento;

public enum GeneraStanziamento {
  NONE(0), COSTO_RICAVO(1), COSTO(2), RICAVO(3), NOTA_CREDITO(4), SOLO_PROVVIGIONI(5);

  private int generaStanziamento;

  GeneraStanziamento(int _generaStanziamento) {
    generaStanziamento = _generaStanziamento;
  }

  public int intValue() {
    return generaStanziamento;
  }

}
