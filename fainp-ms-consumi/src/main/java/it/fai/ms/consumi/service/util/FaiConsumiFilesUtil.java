package it.fai.ms.consumi.service.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.job.FileNameResolver;

public class FaiConsumiFilesUtil {

  public final static String PROCESSED = ".processed";
  public final static String PROCESSING = ".processing";


  private static final Logger _log = LoggerFactory.getLogger(FaiConsumiFilesUtil.class);

  public static Path getAbsoluteOrRelativePath(final String _basePath) {
    return _basePath.startsWith("/") ? Paths.get(_basePath) : Paths.get(System.getProperty("user.home") + "/" + _basePath);
  }
  
  public static String renameToProcessing(final String _file) {
    String filename = _file + PROCESSING;
    try {
      if (Paths.get(filename).toFile().exists()) {
        Files.move(Paths.get(filename), Paths.get(filename + "_old-" + System.currentTimeMillis()));
      }
      Files.move(Paths.get(_file), Paths.get(filename));
      return filename;
    } catch (final IOException _e) {
      _log.error("File {} rename failed", _file, _e);
      throw new RuntimeException("File "+_file+" rename failed in processing");
    }
  }

  public static void renameToProcessed(final String _file) {
    try {
      if (Paths.get(_file + PROCESSED).toFile().exists()) {
        Files.move(Paths.get(_file + PROCESSED), Paths.get(_file + PROCESSED + "_old-" + System.currentTimeMillis()));
      }
      
      if (Paths.get(_file + PROCESSING).toFile().exists()) {
        Files.move(Paths.get(_file + PROCESSING), Paths.get(_file + PROCESSED));
      }else if (Paths.get(_file).toFile().exists()) {
        Files.move(Paths.get(_file), Paths.get(_file + PROCESSED));
      }
    } catch (final IOException _e) {
      _log.error("File {} rename failed", _file, _e);
    }
  }
  
  public static String getFilename(String filename) {
	  return filename!=null ? filename.replace(PROCESSING, "") : null;
  }
  
  public static String resolveFileName(final Path _filePath) {
    return new FileNameResolver().resolveFileName(_filePath);
  }

  public static String resolveFileNameProcessing(Path _filePath) {
    return new FileNameResolver().resolveFileName(_filePath) + PROCESSING;
  }
  
  public static boolean isNotAlreadyProcessed(final Path _resolveFilePath) {
    return _resolveFilePath == null || !_resolveFilePath.toFile()
                                                        .getName()
                                                        .contains(FaiConsumiFilesUtil.PROCESSED);
  }
  
  public static boolean isNotProcessing(final Path _resolveFilePath) {
    return _resolveFilePath == null || !_resolveFilePath.toFile()
                                                        .getName()
                                                        .contains(FaiConsumiFilesUtil.PROCESSING);
  }
}
