package it.fai.ms.consumi.service.processor.consumi.elceu;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface ElceuProcessor extends ConsumoProcessor<Elceu> {

}
