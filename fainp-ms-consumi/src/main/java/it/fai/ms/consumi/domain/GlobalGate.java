package it.fai.ms.consumi.domain;

import java.util.Objects;

public class GlobalGate {

  private String       description;
  private final String id;

  public GlobalGate(final String _id) {
    id = Objects.requireNonNull(_id, "Parameter id is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final GlobalGate obj = getClass().cast(_obj);
      res = Objects.equals(obj.id, id);
    }
    return res;
  }

  public String getDescription() {
    return description;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, description);
  }

  public void setDescription(final String _description) {
    description = _description;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("id=")
                              .append(id)
                              .append(",description=")
                              .append(description)
                              .append("]")
                              .toString();
  }

}
