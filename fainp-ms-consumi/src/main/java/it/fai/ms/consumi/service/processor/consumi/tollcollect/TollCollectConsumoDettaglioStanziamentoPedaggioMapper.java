package it.fai.ms.consumi.service.processor.consumi.tollcollect;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class TollCollectConsumoDettaglioStanziamentoPedaggioMapper extends ConsumoDettaglioStanziamentoMapper {

  
  
  public TollCollectConsumoDettaglioStanziamentoPedaggioMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull Consumo consumo) {
    DettaglioStanziamentoPedaggio stanziamento = null;
     if ( consumo instanceof TollCollect ){
      var tollCollect = (TollCollect) consumo;
      stanziamento = (DettaglioStanziamentoPedaggio) toDettaglioStanziamentoPedaggio(tollCollect, tollCollect.getGlobalIdentifier());
      stanziamento.setRouteName(tollCollect.getRouteName());
      stanziamento.getSupplier().setDocument(tollCollect.getAdditionalInfo());
    }
    return stanziamento;
  }
}
