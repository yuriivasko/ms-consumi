package it.fai.ms.consumi.web.rest;

import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Map;

@RestController
@RequestMapping(NotConfirmedExpiredTemporaryAllocationResource.BASE_PATH)
public class NotConfirmedExpiredTemporaryAllocationResource {
    static final String BASE_PATH = "/api/public";
    //static final String BASE_PATH = "notconfirmedallocationsjob";

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService;


    public NotConfirmedExpiredTemporaryAllocationResource(NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {
        this.notConfirmedTemporaryAllocationsService = notConfirmedTemporaryAllocationsService;
    }

    @PostMapping("/notconfirmedallocationsjob")
    public void schedule(@RequestBody Map<String,Object> body){
        String expirationStr = (String) body.get("expiration");
        notConfirmedTemporaryAllocationsService.processSource((String)body.get("source"),parseFromString(expirationStr));
    }

    private Instant parseFromString(String dateStr){
        return new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd HH:mm:ss")
                .toFormatter()
                .withZone(ZoneId.systemDefault())
                .parse(dateStr, Instant::from);
    }

}
