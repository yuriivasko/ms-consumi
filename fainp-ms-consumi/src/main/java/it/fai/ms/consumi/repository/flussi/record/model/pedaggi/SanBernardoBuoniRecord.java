package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import org.apache.commons.csv.CSVFormat;

import java.util.Objects;
import java.util.UUID;

public class SanBernardoBuoniRecord extends CommonRecord implements GlobalIdentifierBySupplier {

  @JsonCreator
  public SanBernardoBuoniRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID().toString();
  }

  private String globalIdentifier;


  private String numero;
  private String ordineN;
  private String sequenza;
  private String dataEmissione;
  private String stato;
  private String classeAutorizzata;
  private String scadenza;
  private String targa;
  private String dataBlocco;
  private String commento;
  private String dataPassaggio;
  private String stazionePassaggio;
  private String pistaPassaggio;
  private String classePassaggio;

  public void setGlobalIdentifier(String globalIdentifier) {
    this.globalIdentifier = globalIdentifier;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getOrdineN() {
    return ordineN;
  }

  public void setOrdineN(String ordineN) {
    this.ordineN = ordineN;
  }

  public String getSequenza() {
    return sequenza;
  }

  public void setSequenza(String sequenza) {
    this.sequenza = sequenza;
  }

  public String getDataEmissione() {
    return dataEmissione;
  }

  public void setDataEmissione(String dataEmissione) {
    this.dataEmissione = dataEmissione;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public String getClasseAutorizzata() {
    return classeAutorizzata;
  }

  public void setClasseAutorizzata(String classeAutorizzata) {
    this.classeAutorizzata = classeAutorizzata;
  }

  public String getScadenza() {
    return scadenza;
  }

  public void setScadenza(String scadenza) {
    this.scadenza = scadenza;
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getDataBlocco() {
    return dataBlocco;
  }

  public void setDataBlocco(String dataBlocco) {
    this.dataBlocco = dataBlocco;
  }

  public String getCommento() {
    return commento;
  }

  public void setCommento(String commento) {
    this.commento = commento;
  }

  public String getDataPassaggio() {
    return dataPassaggio;
  }

  public void setDataPassaggio(String dataPassaggio) {
    this.dataPassaggio = dataPassaggio;
  }

  public String getStazionePassaggio() {
    return stazionePassaggio;
  }

  public void setStazionePassaggio(String stazionePassaggio) {
    this.stazionePassaggio = stazionePassaggio;
  }

  public String getPistaPassaggio() {
    return pistaPassaggio;
  }

  public void setPistaPassaggio(String pistaPassaggio) {
    this.pistaPassaggio = pistaPassaggio;
  }

  public String getClassePassaggio() {
    return classePassaggio;
  }

  public void setClassePassaggio(String classePassaggio) {
    this.classePassaggio = classePassaggio;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SanBernardoBuoniRecord that = (SanBernardoBuoniRecord) o;
    return Objects.equals(globalIdentifier, that.globalIdentifier) &&
      Objects.equals(numero, that.numero) &&
      Objects.equals(ordineN, that.ordineN) &&
      Objects.equals(sequenza, that.sequenza) &&
      Objects.equals(dataEmissione, that.dataEmissione) &&
      Objects.equals(stato, that.stato) &&
      Objects.equals(classeAutorizzata, that.classeAutorizzata) &&
      Objects.equals(scadenza, that.scadenza) &&
      Objects.equals(targa, that.targa) &&
      Objects.equals(dataBlocco, that.dataBlocco) &&
      Objects.equals(commento, that.commento) &&
      Objects.equals(dataPassaggio, that.dataPassaggio) &&
      Objects.equals(stazionePassaggio, that.stazionePassaggio) &&
      Objects.equals(pistaPassaggio, that.pistaPassaggio) &&
      Objects.equals(classePassaggio, that.classePassaggio);
  }

  @Override
  public int hashCode() {
    return Objects.hash(globalIdentifier, numero, ordineN, sequenza, dataEmissione, stato, classeAutorizzata, scadenza, targa, dataBlocco, commento, dataPassaggio, stazionePassaggio, pistaPassaggio, classePassaggio);
  }

  @Override
  public String toString() {
    return "SanBernardoBuoniRecord{" +
      "globalIdentifier='" + globalIdentifier + '\'' +
      ", numero='" + numero + '\'' +
      ", ordineN='" + ordineN + '\'' +
      ", sequenza='" + sequenza + '\'' +
      ", dataEmissione='" + dataEmissione + '\'' +
      ", stato='" + stato + '\'' +
      ", classeAutorizzata='" + classeAutorizzata + '\'' +
      ", scadenza='" + scadenza + '\'' +
      ", targa='" + targa + '\'' +
      ", dataBlocco='" + dataBlocco + '\'' +
      ", commento='" + commento + '\'' +
      ", dataPassaggio='" + dataPassaggio + '\'' +
      ", stazionePassaggio='" + stazionePassaggio + '\'' +
      ", pistaPassaggio='" + pistaPassaggio + '\'' +
      ", classePassaggio='" + classePassaggio + '\'' +
      '}';
  }

  @Override
  public CSVFormat getCsvFormat() {
    return CSVFormat.RFC4180.withDelimiter(';');
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }
}


