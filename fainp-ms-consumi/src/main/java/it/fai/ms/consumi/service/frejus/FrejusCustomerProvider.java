package it.fai.ms.consumi.service.frejus;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.FrejusJobProperties;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.FrejusPrenotazioni;

enum FatturazioneCsv {
  DATA, ORA, TARGA, TRC, ESATTORE, PISTA, CLASSE, DAC, NAZIONE, MONETA, IMPORTO, TIPO, NUMERO_CARTA
}

enum PrenotazioneCsv {
  DATA, TIPO, DIREZIONE, TARGA, AUTISTA, AZIENDA, NUMERO_CARTA, STATO, TRC, CLASSE_ECO, CATEGORIA, PISTA
}
@Service
public class FrejusCustomerProvider {
  private static final String    PROCESSED = ".processed";
  private final transient Logger log       = LoggerFactory.getLogger(getClass());
  private FrejusJobProperties    properties;

  public FrejusCustomerProvider(ApplicationProperties properties) {
    this.properties = properties.getFrejusJobProperties();
  }

  public void loadCustomerData(FrejusCustomerDataContainer container) {
    File[] fatturazioniReader = getFiles(properties.getFatturazioniFolder());
    File[] prenotazioniReader = getFiles(properties.getPrenotazioniFolder());

    container.setCustomerTrc(loadCustomerData(fatturazioniReader, prenotazioniReader));
  }



  List<FrejusCustomerData> loadCustomerData(File[] fatturazioniFiles, File[] prenotazioniFiles) {

    List<CSVRecord> prenotazioni = loadPrenotazioni(prenotazioniFiles);

    List<FrejusCustomerData> customerTrc = new ArrayList<>();

    for (File fatturazioneFile : fatturazioniFiles) {
      if (!fatturazioneFile.getName()
                           .endsWith(PROCESSED)) {
        try {
          log.debug("processing prenotazione File: {}", fatturazioneFile);
          CSVParser fatturazione = CSVFormat.RFC4180.withHeader(FatturazioneCsv.class)
                                                    .withDelimiter(';')
                                                    .parse(new FileReader(fatturazioneFile));

          fatturazione.forEach(fattura -> {
            try {
              if (isValid(fattura)) {
                FrejusCustomerData customer = createCustomerData(fattura);

                Optional<CSVRecord> prenotazione = findPrenotazione(prenotazioni, customer.trc);

                prenotazione.ifPresent(pren -> {
                  customer.azienda = pren.get(PrenotazioneCsv.AZIENDA)
                                         .trim();
                });
                log.debug("add trc {} with data {}", customer.trc, customer);
                customerTrc.add(customer);
              }
            } catch (Exception e) {
              log.error("skip customer line {}", fattura, e.getMessage());
              // skip line
            }
          });
          // markFileProcessed(fatturazioneFile);
        } catch (Exception e) {
          log.error("error fatturazionifile {}", fatturazioneFile, e);
          // skip file
        }
      }
    }
    return customerTrc;
  }

  private Optional<CSVRecord> findPrenotazione(List<CSVRecord> prenotazioni, String trc) {
    Optional<CSVRecord> prenotazione = prenotazioni.stream()
                                                   .filter(pren -> {
                                                     try {
                                                       return trc.equals(cleanString(pren.get(PrenotazioneCsv.TRC)));
                                                     } catch (Exception e) {
                                                       return false;// skip line
                                                     }
                                                   })
                                                   .findAny();
    return prenotazione;
  }

  private FrejusCustomerData createCustomerData(CSVRecord fattura) {
    FrejusCustomerData customer = new FrejusCustomerData();
    customer.trc = cleanString(fattura.get(FatturazioneCsv.TRC.ordinal()));
    customer.targa = fattura.get(FatturazioneCsv.TARGA.ordinal())
                            .trim();
    customer.numeroCarta = cleanCard(fattura.get(FatturazioneCsv.NUMERO_CARTA.ordinal()));
    customer.data = toDate(fattura.get(FatturazioneCsv.DATA.ordinal()));
    customer.ora = toTime(fattura.get(FatturazioneCsv.ORA.ordinal()));
    return customer;
  }

  private List<CSVRecord> loadPrenotazioni(File[] prenotazioniFiles) {
    List<CSVRecord> prenotazioni = new ArrayList<>();
    for (File prenotazioneFile : prenotazioniFiles) {
      if (!prenotazioneFile.getName()
                           .endsWith(PROCESSED)) {
        try {
          log.debug("processing prenotazione File: {}", prenotazioneFile);
          prenotazioni.addAll(CSVFormat.RFC4180.withDelimiter(';')
                                               .withHeader(PrenotazioneCsv.class)
                                               .parse(new FileReader(prenotazioneFile))
                                               .getRecords());
          // markFileProcessed(prenotazioneFile);
        } catch (Exception e) {
          log.error("error processing prenotazioni file {}", prenotazioneFile, e);
          // skip file
        }
      }
    }
    return prenotazioni;
  }


  private boolean isValid(CSVRecord fattura) {
    return fattura.size() >= FatturazioneCsv.values().length && !fattura.get(FatturazioneCsv.TRC)
                                                                        .trim()
                                                                        .toLowerCase()
                                                                        .contains("trc");
  }

  private String cleanCard(String string) {
    string = string.trim();
    if (string.length() > 19)
      return string.substring(0, 19);
    else
      return string;
  }

  private String cleanString(String string) {
    return string.replace('_', ' ')
                 .trim();
  }

  private LocalDate toDate(String string) {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    return LocalDate.parse(string.trim(), format);
  }

  private LocalTime toTime(String string) {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm:ss");
    return LocalTime.parse(string.trim(), format);
  }

  private boolean isDateEqual(Instant dt, Instant data) {
    return Duration.between(data, dt)
                   .abs()
                   .compareTo(Duration.ofSeconds(30)) < 0;
  }

  public void setData(FrejusPrenotazioni prenotazione, String pan, Optional<Instant> dataTransazione, FrejusCustomerDataContainer header) {
    var customerData = dataTransazione.flatMap(dt -> header.getCustomerTrc()
                                                           .stream()
                                                           .filter(data -> data.numeroCarta.equals(pan)
                                                                           && isDateEqual(dt, data.getDataTransazione()))
                                                           .findAny());
    customerData.ifPresent(data -> {
      prenotazione.setCustomer(new Customer(data.azienda));
      Vehicle vehicle = new Vehicle();
      vehicle.setLicensePlate(new VehicleLicensePlate(data.targa, VehicleLicensePlate.NO_COUNTRY));
      prenotazione.setVehicle(vehicle);
    });

  }

  private File[] getFiles(String folderPath) {
    File fatturazioniFolder = getAbsolutePath(folderPath).toFile();
    if (!fatturazioniFolder.exists())
      fatturazioniFolder.mkdirs();
    return  fatturazioniFolder.listFiles();
  }

  private Path getAbsolutePath(final String _basePath) {
    return _basePath.startsWith("/") ? Paths.get(_basePath) : Paths.get(System.getProperty("user.home") + "/" + _basePath);
  }
}
