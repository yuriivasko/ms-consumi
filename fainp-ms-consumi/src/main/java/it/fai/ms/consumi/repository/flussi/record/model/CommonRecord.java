package it.fai.ms.consumi.repository.flussi.record.model;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;

public abstract class CommonRecord extends BaseRecord {

  private final static long serialVersionUID = -4484494377444592662L;

  @JsonIgnore
  private transient final Logger _log = LoggerFactory.getLogger(getClass());

  @Valid
  protected Long    rowNumber            = 0L;

  @Valid
  protected String  fileName;

  //yyyyMMdd
  protected String  creationDateInFile;

  //HHmmss
  protected String  creationTimeInFile;

  protected boolean processed            = false;

  protected Date    rtdtm                = null;

  protected Date    rttime               = null;

  protected Integer anno                 = 0;

  protected BigDecimal   vatRateBigDecimal   = null;

//  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSX", timezone = "UTC")
  protected Instant ingestion_time       = Instant.now();

  private String    recordCode           = "";

  private String    transactionDetail    = "";

  private ServicePartner servicePartner;

  public CommonRecord(String fileName, long rowNumber) {
    super();
    if (fileName == null) {
      throw new IllegalStateException("filename and rownumber must be not null!!");
    }
    this.fileName = fileName;
    this.rowNumber = rowNumber;
      id = Objects.hash(this.fileName,this.rowNumber);
  }

  public Long getRowNumber() {
    return rowNumber;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public boolean isProcessed() {
    return processed;
  }

  public void setProcessed(boolean processed) {
    this.processed = processed;
  }

  public Date getRtdtm() {
    return rtdtm;
  }

  public void setRtdtm(Date rtdtm) {
    this.rtdtm = rtdtm;
  }

  public Date getRttime() {
    return rttime;
  }

  public void setRttime(Date rttime) {
    this.rttime = rttime;
  }

  public Integer getAnno() {
    return anno;
  }

  public void setAnno(Integer anno) {
    this.anno = anno;
  }

  public Instant getIngestion_time() {
    return ingestion_time;
  }

  public void setIngestion_time(Instant ingestion_time) {
    this.ingestion_time = ingestion_time;
  }

  public BigDecimal getVatRateBigDecimal() {
    return vatRateBigDecimal;
  }

  public void setVatRateDecimal(BigDecimal vatRateBigDecimal) {
    this.vatRateBigDecimal = vatRateBigDecimal;
  }

  public void   setFromString(final List<StringItem> _items, final String _data) {

    if (_log.isDebugEnabled())
      _log.debug("Row data to process : {}", _data.trim()
                                                  .replaceAll("\\s{2,}", " "));

    for (final StringItem item : _items) {

      if (_log.isTraceEnabled())
        _log.trace("Processing field '{}'", item.getMethod());

      final String methodName = "set" + item.getMethod()
                                            .substring(0, 1)
                                            .toUpperCase()
                                + item.getMethod()
                                      .substring(1);
      try {
        final Method method = getClass().getMethod(methodName, String.class);
        if (method != null) {
          final int start = item.getStart() - 1;
          final int end = item.getEnd();
          if (_data.length() >= end) {
            final String value = _data.substring(start, end)
                                      .trim();
            method.invoke(this, value);
            if (_log.isTraceEnabled())
              _log.trace(" Value is '{}'", value);
          } else {
            if(_data.length() > start) {
              logger.warn("String lenght don't match. Method " + methodName + " not call");
            }else {
              logger.warn("String lenght too small. Method " + methodName + " not call");
            }
          }
        } else {
          if (_log.isTraceEnabled()){
            //esegue già questo check nell'abstract method
            logger.trace(methodName + " not found");
          }
        }
      } catch (final NoSuchMethodException e) {
        if (_log.isTraceEnabled()) {
          //esegue già questo check nell'abstract method
          logger.trace(methodName + " not found");
        }
      } catch (final Exception e) {
        logger.error("Caught exception (won't be rethrown) while processing StringItem : " + item.toString(), e);
      }
    }
  }
  public void setFromCsvRecord(String data, List<String> fields) {
    Reader reader = new StringReader(data);
    Iterable<CSVRecord> records = null;
    try {

      records = getCsvFormat().parse(reader);
      CSVRecord record = records.iterator().next();
      Class<?> clazz = this.getClass();
      for(int i=0; i<fields.size(); i++){
        String item = fields.get(i);
        if(!"".equals(item)){
          String method = "set" + item
            .substring(0, 1)
            .toUpperCase()
            + item.substring(1);
          try {
            Method m = clazz.getMethod(method, String.class);
            if (m != null) {

              String value = record.get(i);
              // logger.info(data);
              // logger.info(method + " - " + value);
              m.invoke(this, value);
            } else {
              logger.warn(method + " not found");
            }
          } catch (Exception e) {
            logger.error("Caught exception (won't be rethrown) while processing CsvItem : " + item.toString(), e);
          }
        }
      }

    } catch (IOException e) {
      logger.error("An error occurredd parsing "+fileName+" row number "+rowNumber,e);
    }
  }

  @JsonIgnore
  public CSVFormat getCsvFormat() {
    return CSVFormat.RFC4180;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }

  public String getTransactionDetail() {
    return transactionDetail;
  }

  public void setTransactionDetail(String transactionDetail) {
    this.transactionDetail = transactionDetail;
  }

  public String getCreationDateInFile() {
    return creationDateInFile;
  }

  public void setCreationDateInFile(String creationDateInFile) {
    this.creationDateInFile = creationDateInFile;
  }

  public String getCreationTimeInFile() {
    return creationTimeInFile;
  }

  public void setCreationTimeInFile(String creationTimeInFile) {
    this.creationTimeInFile = creationTimeInFile;
  }

  public void setServicePartner(ServicePartner servicePartner) {
    this.servicePartner = servicePartner;
  }

  public ServicePartner getServicePartner() {
    return servicePartner;
  }

  public String toNotificationMessage() {
    return
      "recordType:= [" + this.getClass().getSimpleName() + "], " +
      "fileName:=[" + fileName + "], " +
      "recordUuid:=[" + getUuid() + "], " +
      "rowNumber:=[" + rowNumber + "], " +
      (ingestion_time == null ? "" : "ingestion_time:=[" + ingestion_time + "]") + ", " +
      (recordCode == null ? "" : "recordCode:=[" + recordCode + "]" ) + ", " +
      (transactionDetail == null ? "" : "transactionDetail:=[" + transactionDetail + "]") + ", " +
      (servicePartner == null ? "" : "servicePartner:=[" + servicePartner + "]");
  }
  
  public List<String> toMinimalInformationList() {
    return Arrays.asList(this.getClass().getSimpleName(), getUuid(), rowNumber+"", recordCode!=null ? recordCode : "", transactionDetail!=null ? transactionDetail :"");
  }

  public static String toNotificationMessage(List<String> data, String fileName, String ingestion_time, String servicePartner) {
    if(data == null) {
      data = Arrays.asList("n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a");
    }
    if(data.size()<5) {
      data.addAll(Arrays.asList("n/a","n/a","n/a","n/a","n/a","n/a","n/a","n/a"));
    }
    return
        "recordType:= [" + data.get(0) + "], " +
        "fileName:=[" + fileName + "], " +
        "recordUuid:=[" + data.get(1) + "], " +
        "rowNumber:=[" + data.get(2) + "], " +
        (ingestion_time == null ? "" : "ingestion_time:=[" + ingestion_time + "]") + ", " +
        (data.get(3) == null ? "" : "recordCode:=[" + data.get(3) + "]" ) + ", " +
        (data.get(4) == null ? "" : "transactionDetail:=[" + data.get(4) + "]") + ", " +
        (servicePartner == null ? "" : "servicePartner:=[" + servicePartner + "]");
  }
}
