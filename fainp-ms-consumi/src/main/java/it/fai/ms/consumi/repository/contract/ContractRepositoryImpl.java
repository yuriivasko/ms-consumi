package it.fai.ms.consumi.repository.contract;

import java.time.Instant;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.repository.storico_dml.StoricoContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto_;

@Repository
@Validated
@Transactional
public class ContractRepositoryImpl implements ContractRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final EntityManager        em;
  private final ContractEntityMapper entityMapper;
  private final StoricoContrattoRepository repo;


  @Inject
  public ContractRepositoryImpl(final EntityManager _entityManager, final ContractEntityMapper _entityMapper,StoricoContrattoRepository storicoContrattoRepository) {
    em = _entityManager;
    entityMapper = _entityMapper;
    this.repo = storicoContrattoRepository;
  }

  @Override
  public Optional<Contract> findContractByUuid(@NotNull final String _contractId) {

    _log.debug(" Searching {} by id {} ...", Contract.class.getSimpleName(), _contractId);

    var optionalContract = repo.findFirstByContrattoNumeroOrderByDataVariazioneDesc(_contractId).map(storicoStatoContratto -> entityMapper.toDomain(storicoStatoContratto));

    _log.debug(" Found : {}", optionalContract);

    return optionalContract;
  }

  @Override
  public Optional<StoricoContratto> findContrattoAtDate(String idContratto, Instant dataRiferimentoInizio) {
    var cb = em.getCriteriaBuilder();

    CriteriaQuery<StoricoContratto> query = cb.createQuery(StoricoContratto.class);
    Root<StoricoContratto>          root  = query.from(StoricoContratto.class);

    query.where(cb.and(cb.equal(root.get(StoricoContratto_.dmlUniqueIdentifier), idContratto),
                       cb.lessThanOrEqualTo(root.get(StoricoContratto_.dmlRevisionTimestamp), dataRiferimentoInizio)));
    query.orderBy(cb.desc(root.get(StoricoContratto_.dmlRevisionTimestamp)));
    return em.createQuery(query).getResultStream().findFirst();
  }

}
