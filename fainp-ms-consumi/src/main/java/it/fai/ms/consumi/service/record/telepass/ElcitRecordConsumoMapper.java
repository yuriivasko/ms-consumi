package it.fai.ms.consumi.service.record.telepass;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.ESE_TOT_STATUS;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elcit;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElcitGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class ElcitRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<ElcitRecord, Elcit> {

  @Override
  public Elcit mapRecordToConsumo(ElcitRecord commonRecord) throws Exception {
    ElcitRecord parentRecord = (ElcitRecord) commonRecord.getParentRecord();
    if (parentRecord == null) {
      throw new IllegalArgumentException("Not found Following record");
    }
    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(parentRecord, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant ingestionTime = parentRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(parentRecord.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, TelepassSourceName.ELCIT);
    source.setFileName(parentRecord.getFileName());
    source.setRowNumber(parentRecord.getRowNumber());
    Elcit toConsumo = new Elcit(source, parentRecord.getRecordCode(),
                                Optional.of(new ElcitGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);
    Consumo toConsumoBase = toConsumo;
    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();
    transaction.setDetailCode(parentRecord.getTransactionType());
    toConsumoBase.setTransaction(transaction);
    toConsumoBase.setSupplierInvoiceDocument(null); //assente in questo flusso
    if (parentRecord.getSubContractCode() != null && !parentRecord.getSubContractCode()
                                                                  .trim()
                                                                  .isEmpty()) {
      Contract contract = new Contract(parentRecord.getSubContractCode());
      toConsumoBase.setContract(Optional.of(contract));
    }
    // CONSUMO.AMOUNT
    Amount amount = new Amount();
    amount.setExchangeRate(null); // Valore di default --> non viene usato
    MonetaryAmount amountIncludedVat = null;

    ElcitRecord followingElcitRecord = (ElcitRecord) commonRecord;

    ESE_TOT_STATUS ese_tot_status = null;
    if (!followingElcitRecord.getRecordCode()
                             .equals("30")) {
      throw new IllegalArgumentException("Following Elcit record is not record type 30");
    }

    if (!checkEqualFields(parentRecord, followingElcitRecord)) {
      throw new IllegalArgumentException("Following Elcit record is not related to Eclit record");
    }

    amountIncludedVat = followingElcitRecord.getAmount_vat();
    amount.setVatRate(followingElcitRecord.getVatRateBigDecimal());
    amount.setAmountIncludedVat(amountIncludedVat); //this auto-calculate excluded VAT
    if (amount.getVatRateBigDecimal().compareTo(BigDecimal.ZERO) != 0) {
      ese_tot_status = ESE_TOT_STATUS.TOT;
    }else {
      ese_tot_status = ESE_TOT_STATUS.ESE;
    }
    toConsumoBase.setAmount(amount);


    ConsumoPedaggio toConsumoPedaggio = toConsumo;
    toConsumoPedaggio.setProcessed(parentRecord.isProcessed());
    toConsumoPedaggio.setEseTotStatus(ese_tot_status);

    // CONSUMOPEDAGGIO.DEVICE
    ElcitRecord elcitRecord = parentRecord;

    final Optional<TipoDispositivoEnum> tipoDispositivoByTypeOfSupport = TelepassDeviceType.getTipoDispositivoByTypeOfSupport(elcitRecord.getDeviceType());

    var vehicle = new Vehicle();
    Device device = null;
    if (isLicencePlateTypeOfMovement(elcitRecord)){
      //when movemenet is isLicencePlateTypeOfMovement the deviceCode is license plate id
      final String licenseId = elcitRecord.getLicensePlate();

      final VehicleLicensePlate licensePlate = new VehicleLicensePlate(licenseId, VehicleLicensePlate.NO_COUNTRY);
      vehicle.setLicensePlate(licensePlate);
      vehicle.setFareClass(elcitRecord.getPricingClass());
    } else {
      String deviceCode = elcitRecord.getDeviceCode();

      vehicle.setFareClass(elcitRecord.getPricingClass());
      if (tipoDispositivoByTypeOfSupport.isPresent()) {
        switch(tipoDispositivoByTypeOfSupport.get()) {
          case TELEPASS_EUROPEO:
          case TELEPASS_EUROPEO_SAT:
            device = new Device(tipoDispositivoByTypeOfSupport.get());
            device.setPan(deviceCode);
            break;
          case VIACARD:
            //trim a 9 caratteri
            deviceCode = StringUtils.right(deviceCode, 9);
            device = new Device(deviceCode, tipoDispositivoByTypeOfSupport.get());
            break;
          default:
            device = new Device(deviceCode, tipoDispositivoByTypeOfSupport.get());
        }
        device.setServiceType(TipoServizioEnum.PEDAGGI_ITALIA); // E' l'unico servizio che arriva su questo flusso per i telepass europei e europei sat
      }
    }
    toConsumoPedaggio.setVehicle(vehicle);
    toConsumoPedaggio.setDevice(device);

    //
    // //CONSUMOPEDAGGIO.TOLLPOINTENTRY
    TollPoint tollpointEntry = new TollPoint();
    GlobalGate globalGateEntry = new GlobalGate(elcitRecord.getEntryGate());
    tollpointEntry.setGlobalGate(globalGateEntry);
    if (StringUtils.isNotBlank(elcitRecord.getDescription())) {
      globalGateEntry.setDescription(StringUtils.left(elcitRecord.getDescription(), 13).trim());
    }
    // Non c'è l'EntryTimestamp

    toConsumoPedaggio.setTollPointEntry(tollpointEntry);

    //
    // //CONSUMOPEDAGGIO.TOLLPOINTEXIT
    TollPoint tollpointExit = new TollPoint();
    GlobalGate globalGateExit = new GlobalGate(elcitRecord.getExitGate());
    tollpointExit.setGlobalGate(globalGateExit);
    if (StringUtils.isNotBlank(elcitRecord.getDescription())) {
      globalGateExit.setDescription(StringUtils.mid(elcitRecord.getDescription(), 13, 13).trim());
    }
    if (elcitRecord.getDate() != null && elcitRecord.getTime() != null) {
      tollpointExit.setTime(calculateInstantByDateAndTime(elcitRecord.getDate().replaceAll("-", ""), elcitRecord.getTime().replace(".", ""), yyyyMMdd_dateformat,
                                                          HHmmss_timeformat).get());
    }

    toConsumoPedaggio.setTollPointExit(tollpointExit);
    //
    // //CONSUMOPEDAGGIO. altri campi
    // toConsumoPedaggio.setTotalAmount(new BigDecimal(commonRecord.getTotalAmount())); // Calcolato
    toConsumoPedaggio.setTollGate(elcitRecord.getExitGate());
    toConsumoPedaggio.setNetworkCode(elcitRecord.getNetCode());
    toConsumoPedaggio.setExitTransitDateTime(elcitRecord.getDate() + elcitRecord.getTime());
    toConsumoPedaggio.setRoute(elcitRecord.getDescription());
    // //toConsumoPedaggio.setImponOibileAdr(imponibileAdr); --> non applicabile

    return toConsumo;
  }

  private boolean checkEqualFields(ElcitRecord commonRecord, ElcitRecord followingElcitRecord) {
    return commonRecord.getDate()
                       .equals(followingElcitRecord.getDate())
           && commonRecord.getTime()
                          .equals(followingElcitRecord.getTime())
           && commonRecord.getNetCode()
                          .equals(followingElcitRecord.getNetCode());
  }

  private boolean isLicencePlateTypeOfMovement(ElcitRecord elcitRecord) {
    return "AC".equals(elcitRecord.getTransactionType());
  }

}
