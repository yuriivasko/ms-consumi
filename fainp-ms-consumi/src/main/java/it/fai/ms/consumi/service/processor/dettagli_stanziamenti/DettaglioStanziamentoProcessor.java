package it.fai.ms.consumi.service.processor.dettagli_stanziamenti;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

public interface DettaglioStanziamentoProcessor {

  Optional<DettaglioStanziamento> process(@NotNull DettaglioStanziamento allocationDetail,
                                          @NotNull StanziamentiParams allocationParams);

}
