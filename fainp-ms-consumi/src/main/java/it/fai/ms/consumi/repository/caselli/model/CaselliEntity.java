package it.fai.ms.consumi.repository.caselli.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A Caselli.
 */
@Entity
@Table(name = "caselli")
public class CaselliEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "gate_identifier")
  private String gateIdentifier;

  @Column(name = "motorways")
  private String motorways;

  @Column(name = "gate_number")
  private String gateNumber;

  @Column(name = "lane_code")
  private String laneCode;

  @Column(name = "description")
  private String description;

  @Column(name = "end_date")
  private String endDate;

  @Column(name = "main_type_of_gate")
  private String mainTypeOfGate;

  @Column(name = "sub_type_of_gate")
  private String subTypeOfGate;

  @Column(name = "distance")
  private String distance;

  @Column(name = "global_gate_identifier")
  private String globalGateIdentifier;

  @Column(name = "network")
  private String network;

  // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNetwork() {
    return network;
  }

  public void setNetwork(String network) {
    this.network = network;
  }

  public CaselliEntity network(String network) {
    this.network = network;
    return this;
  }

  public String getGateIdentifier() {
    return gateIdentifier;
  }

  public CaselliEntity gateIdentifier(String gateIdentifier) {
    this.gateIdentifier = gateIdentifier;
    return this;
  }

  public void setGateIdentifier(String gateIdentifier) {
    this.gateIdentifier = gateIdentifier;
  }

  public String getMotorways() {
    return motorways;
  }

  public CaselliEntity motorways(String motorways) {
    this.motorways = motorways;
    return this;
  }

  public void setMotorways(String motorways) {
    this.motorways = motorways;
  }

  public String getGateNumber() {
    return gateNumber;
  }

  public CaselliEntity gateNumber(String gateNumber) {
    this.gateNumber = gateNumber;
    return this;
  }

  public void setGateNumber(String gateNumber) {
    this.gateNumber = gateNumber;
  }

  public String getLaneCode() {
    return laneCode;
  }

  public CaselliEntity laneCode(String laneCode) {
    this.laneCode = laneCode;
    return this;
  }

  public void setLaneCode(String laneCode) {
    this.laneCode = laneCode;
  }

  public String getDescription() {
    return description;
  }

  public CaselliEntity description(String description) {
    this.description = description;
    return this;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getEndDate() {
    return endDate;
  }

  public CaselliEntity endDate(String endDate) {
    this.endDate = endDate;
    return this;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getMainTypeOfGate() {
    return mainTypeOfGate;
  }

  public CaselliEntity mainTypeOfGate(String mainTypeOfGate) {
    this.mainTypeOfGate = mainTypeOfGate;
    return this;
  }

  public void setMainTypeOfGate(String mainTypeOfGate) {
    this.mainTypeOfGate = mainTypeOfGate;
  }

  public String getSubTypeOfGate() {
    return subTypeOfGate;
  }

  public CaselliEntity subTypeOfGate(String subTypeOfGate) {
    this.subTypeOfGate = subTypeOfGate;
    return this;
  }

  public void setSubTypeOfGate(String subTypeOfGate) {
    this.subTypeOfGate = subTypeOfGate;
  }

  public String getDistance() {
    return distance;
  }

  public CaselliEntity distance(String distance) {
    this.distance = distance;
    return this;
  }

  public void setDistance(String distance) {
    this.distance = distance;
  }

  public String getGlobalGateIdentifier() {
    return globalGateIdentifier;
  }

  public CaselliEntity globalGateIdentifier(String globalGateIdentifier) {
    this.globalGateIdentifier = globalGateIdentifier;
    return this;
  }

  public void setGlobalGateIdentifier(String globalGateIdentifier) {
    this.globalGateIdentifier = globalGateIdentifier;
  }

  // jhipster-needle-entity-add-getters-setters - Jhipster will add getters and setters here, do not remove

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CaselliEntity caselli = (CaselliEntity) o;
    if (caselli.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), caselli.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder()
        .append("CaselliEntity [id=")
        .append(id)
        .append(", gateIdentifier=")
        .append(gateIdentifier)
        .append(", motorways=")
        .append(motorways)
        .append(", gateNumber=")
        .append(gateNumber)
        .append(", laneCode=")
        .append(laneCode)
        .append(", description=")
        .append(description)
        .append(", endDate=")
        .append(endDate)
        .append(", mainTypeOfGate=")
        .append(mainTypeOfGate)
        .append(", subTypeOfGate=")
        .append(subTypeOfGate)
        .append(", distance=")
        .append(distance)
        .append(", globalGateIdentifier=")
        .append(globalGateIdentifier)
        .append(", network=")
        .append(network)
        .append("]");
    return builder.toString();
  }

}
