package it.fai.ms.consumi.service.validator;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

@Service
public class TrxConsumoValidatorServiceImpl
  extends ConsumoValidatorServiceImpl
  implements TrxConsumoValidatorService {
  
  final static ConsumoBlockingValidatorService consumoBlockingValidatorService =  new ConsumoBlockingValidatorService() {
    public ValidationOutcome validate(@NotNull Consumo consumo) {
      return new ValidationOutcome();
    }
  };

  @Inject
  public TrxConsumoValidatorServiceImpl(final Environment env, final ConsumoWarningValidatorService _warningValidationsService, final NotificationService notificationService) {

    //no blocking validator to implement
    super(env, consumoBlockingValidatorService, _warningValidationsService, notificationService);
  }
}
