package it.fai.ms.consumi.service.processor.consumi.asfinag;

import it.fai.ms.consumi.domain.consumi.pedaggi.asfinag.Asfinag;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface AsfinagProcessor
  extends ConsumoProcessor<Asfinag> {

}
