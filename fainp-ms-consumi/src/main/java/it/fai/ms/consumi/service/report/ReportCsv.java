package it.fai.ms.consumi.service.report;

import java.io.IOException;
import java.time.ZoneId;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.utils.report.CsvSerializer;
import it.fai.ms.consumi.config.MessageSourceConfiguration;

@Service
@Transactional
public class ReportCsv {

	private final MessageSource messageSource;

	public ReportCsv(@Qualifier(MessageSourceConfiguration.REPORT_MESSAGES) MessageSource messageSource) {
		super();
		this.messageSource = messageSource;
	}

	public <T> String toCsv(Class<T> clazz,List<T> reportData,Locale locale, ZoneId zoneId) {

	  if(zoneId == null) zoneId = ZoneId.of("Europe/Rome");
      CsvSerializer<T> serializer = new CsvSerializer<>(clazz,  messageSource);

      try {
        StringBuilder out = new StringBuilder();
          serializer.serialize(reportData.stream(), out, locale, zoneId);
          return out.toString();
      } catch (IOException e) {
          throw new RuntimeException("Error in serialize Data", e);
      }

  }
}
