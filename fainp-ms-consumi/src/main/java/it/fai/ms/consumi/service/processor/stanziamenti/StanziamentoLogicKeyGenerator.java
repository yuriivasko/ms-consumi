package it.fai.ms.consumi.service.processor.stanziamenti;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Objects;
import java.util.Optional;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleSupplier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

public class StanziamentoLogicKeyGenerator {

  private final String article;
  private final String countryId;
  private final String supplier;
  private final String customer;
  private final InvoiceType invoiceType;
  private final LocalDate date;
  private String vehicleLicenceId;


  public StanziamentoLogicKeyGenerator(final StanziamentiParams _stanziamentiParams, final DettaglioStanziamento _allocationDetail) {
    Objects.requireNonNull(_stanziamentiParams);
    Objects.requireNonNull(_allocationDetail);
    article = Objects.requireNonNull(Optional.ofNullable(_stanziamentiParams.getArticle())
        .orElseThrow(() -> new IllegalStateException("Article is mandatory"))
        .getCode(),
      "Article code is mandatory");
    countryId = Objects.requireNonNull(Optional.ofNullable(_stanziamentiParams.getArticle())
        .orElseThrow(() -> new IllegalStateException("Article is mandatory"))
        .getCountry(),
      "Article country is mandatory");
    supplier = Objects.requireNonNull(Optional.ofNullable(_stanziamentiParams.getSupplier())
        .orElseThrow(() -> new IllegalStateException("Supplier is mandatory"))
        .getCode(),
      "Supplier code is mandatory");
    customer = Objects.requireNonNull(Optional.ofNullable(_allocationDetail.getCustomer())
        .orElseThrow(() -> new IllegalStateException("Customer is mandatory"))
        .getId(),
      "Customer id is mandatory");
    invoiceType = Objects.requireNonNull(Optional.ofNullable(_allocationDetail.getInvoiceType())
        .orElseThrow(() -> new IllegalStateException("InvoiceType is mandatory")),
      "Customer id is mandatory");
    date = LocalDate.ofInstant(Objects.requireNonNull(_allocationDetail.getDate(), "Date is mandatory"),
      ZoneId.systemDefault());
    if(_allocationDetail instanceof VehicleSupplier){
      vehicleLicenceId= Optional.ofNullable(Optional.ofNullable(((VehicleSupplier)_allocationDetail).getVehicle())
        .orElseGet(null)
        .getLicensePlate())
        .orElseGet(null)
        .getLicenseId();
    }

  }

  public StanziamentoLogicKeyGenerator(String countryId,String article, String supplier, String customer, InvoiceType invoiceType, LocalDate date, String vehicleLicenceId ){
    this.countryId = countryId;
    this.article = article;
    this.supplier = supplier;
    this.customer = customer;
    this.invoiceType = invoiceType;
    this.date = date;
    this.vehicleLicenceId = vehicleLicenceId;
  }



  public StanziamentoLogicKey generate() {
    final var logicKey = new StanziamentoLogicKey(article, countryId,supplier,
                                                  customer, invoiceType,
                                                  date, vehicleLicenceId);
    return logicKey;
  }
}
