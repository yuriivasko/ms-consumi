package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import it.fai.ms.common.jms.fattura.PedaggioBodyDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione2Entity;

public interface ViewAllegatiPedaggiService {
  
  List<ViewAllegatoPedaggioSezione2Entity> getDettagliStanziamento(List<UUID> dettagliStanziamentoId);

  List<PedaggioBodyDTO> createBodyMessageAndSection2(List<ViewAllegatoPedaggioSezione2Entity> dataAllegatiPedaggio,
                                                     final RequestsCreationAttachments requestCreationAttachments);

  List<PedaggioBodyDTO> addSection1OnBody(List<ViewAllegatoPedaggioSezione2Entity> dataAllegatiPedaggi, Instant invoiceDate,
                                          List<PedaggioBodyDTO> bodyWithoutSection1);

}
