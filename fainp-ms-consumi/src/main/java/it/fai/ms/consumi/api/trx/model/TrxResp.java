package it.fai.ms.consumi.api.trx.model;

import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * TrxResp
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-20T16:08:12.419Z")

public class TrxResp   {
  @JsonProperty("requestCode")
  private String requestCode = null;

  @JsonProperty("success")
  private Integer success = null;

  @JsonProperty("utcTimestamp")
  private OffsetDateTime utcTimestamp = null;

  @JsonProperty("errorCode")
  private Integer errorCode = null;

  @JsonProperty("errorMessage")
  private String errorMessage = null;

  public TrxResp requestCode(String requestCode) {
    this.requestCode = requestCode;
    return this;
  }

  /**
   * Unique authorization request ID (per Vendor) used by the Vendor for its internal reference (the same as that sent on the request).
   * @return requestCode
  **/
  @ApiModelProperty(required = true, value = "Unique authorization request ID (per Vendor) used by the Vendor for its internal reference (the same as that sent on the request).")
  @NotNull


  public String getRequestCode() {
    return requestCode;
  }

  public void setRequestCode(String requestCode) {
    this.requestCode = requestCode;
  }

  public TrxResp success(Integer success) {
    this.success = success;
    return this;
  }

  /**
   * =1 if the authorization request has been handled correctly, =0 otherwise (i.e. authorization error). NOTE: success=1 does not mean that the authorization has been given.
   * minimum: 0
   * maximum: 1
   * @return success
  **/
  @ApiModelProperty(required = true, value = "=1 if the authorization request has been handled correctly, =0 otherwise (i.e. authorization error). NOTE: success=1 does not mean that the authorization has been given.")
  @NotNull

@Min(0) @Max(1) 
  public Integer getSuccess() {
    return success;
  }

  public void setSuccess(Integer success) {
    this.success = success;
  }

  public TrxResp utcTimestamp(OffsetDateTime utcTimestamp) {
    this.utcTimestamp = utcTimestamp;
    return this;
  }

  /**
   * UTC date and time in ISO 8601 format (e.g. '2018-08-13T17:27:54.494Z').
   * @return utcTimestamp
  **/
  @ApiModelProperty(required = true, value = "UTC date and time in ISO 8601 format (e.g. '2018-08-13T17:27:54.494Z').")
  @NotNull

  @Valid

  public OffsetDateTime getUtcTimestamp() {
    return utcTimestamp;
  }

  public void setUtcTimestamp(OffsetDateTime utcTimestamp) {
    this.utcTimestamp = utcTimestamp;
  }

  public TrxResp errorCode(Integer errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  /**
   * Error code provided for debugging purpose when success=0.
   * @return errorCode
  **/
  @ApiModelProperty(required = true, value = "Error code provided for debugging purpose when success=0.")
  @NotNull


  public Integer getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }

  public TrxResp errorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
    return this;
  }

  /**
   * Error description provided for debugging purpose when success=0.
   * @return errorMessage
  **/
  @ApiModelProperty(required = true, value = "Error description provided for debugging purpose when success=0.")
  @NotNull


  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrxResp trxResp = (TrxResp) o;
    return Objects.equals(this.requestCode, trxResp.requestCode) &&
        Objects.equals(this.success, trxResp.success) &&
        Objects.equals(this.utcTimestamp, trxResp.utcTimestamp) &&
        Objects.equals(this.errorCode, trxResp.errorCode) &&
        Objects.equals(this.errorMessage, trxResp.errorMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(requestCode, success, utcTimestamp, errorCode, errorMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TrxResp {\n");
    
    sb.append("    requestCode: ").append(toIndentedString(requestCode)).append("\n");
    sb.append("    success: ").append(toIndentedString(success)).append("\n");
    sb.append("    utcTimestamp: ").append(toIndentedString(utcTimestamp)).append("\n");
    sb.append("    errorCode: ").append(toIndentedString(errorCode)).append("\n");
    sb.append("    errorMessage: ").append(toIndentedString(errorMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

