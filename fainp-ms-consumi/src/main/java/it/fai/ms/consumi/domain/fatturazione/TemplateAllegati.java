package it.fai.ms.consumi.domain.fatturazione;

public enum TemplateAllegati {
  
  PEDAGGIO, CARBURANTE, NO_DISPOSITIVO, CON_DISPOSITIVO, TRENI;
  
}
