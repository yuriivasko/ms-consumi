package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.defaultIfBlank;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.ReportWidgetHomepage;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.WidgetDaFatturareDTO;

@Service
@Transactional(isolation=Isolation.READ_UNCOMMITTED, propagation= Propagation.REQUIRES_NEW)
public class ReportCarburantiDaFatturare extends ReportWidgetHomepage {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private EntityManager em;

  private final PetrolPumpService petrolPointClient;

  public ReportCarburantiDaFatturare(EntityManager em, PetrolPumpService petrolPointClient) {
    this.em                = em;
    this.petrolPointClient = petrolPointClient;
  }

  public List<it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto> getDispositivi(String codClienteNav,
                                                                                                        Boolean invoiced,
                                                                                                        Boolean notInvoiced,
                                                                                                        LocalDate consumptionDateFrom,
                                                                                                        LocalDate consumptionDateTo,
                                                                                                        LocalDate invoiceDateFrom,
                                                                                                        LocalDate invoiceDateTo) {

    CriteriaBuilder cb    = em.getCriteriaBuilder();
    var             query = cb.createQuery(it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto.class);
    var             root  = query.from(ViewReportConsumiCarburanti.class);

    query.multiselect(root.get(ViewReportConsumiCarburanti_.CUSTOMER_ID), root.get(ViewReportConsumiCarburanti_.VEICOLO_TARGA),
                      root.get(ViewReportConsumiCarburanti_.VEICOLO_NAZIONE_TARGA),
                      root.get(ViewReportConsumiCarburanti_.CODICE_TRACKYCARD));
    List<Predicate> predicates = new ArrayList<>();
    predicates.add(cb.equal(root.get(ViewReportConsumiCarburanti_.CUSTOMER_ID), codClienteNav));
    List<Predicate> filters = fillFilters(invoiced, notInvoiced, consumptionDateFrom, consumptionDateTo, invoiceDateFrom,
                                          invoiceDateTo, cb, root);
    predicates.addAll(filters);
    query.where(cb.and(predicates.toArray(new Predicate[0])

    ));
    query.groupBy(Arrays.asList(root.get(ViewReportConsumiCarburanti_.CUSTOMER_ID),
                                root.get(ViewReportConsumiCarburanti_.VEICOLO_TARGA),
                                root.get(ViewReportConsumiCarburanti_.VEICOLO_NAZIONE_TARGA),
                                root.get(ViewReportConsumiCarburanti_.CODICE_TRACKYCARD)));

    List<it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto> resultList = em.createQuery(query)
      .getResultList();
    // set id for frontend
    AtomicLong l = new AtomicLong(0);
    resultList.stream().forEach(d -> {
      d.setId(l.getAndIncrement());
      d.setDeviceType(TipoDispositivoEnum.TRACKYCARD);
    });
    return resultList;
  }

  private List<Predicate> fillFilters(boolean invoiced, boolean notInvoiced, LocalDate consumptionDateFrom,
                                      LocalDate consumptionDateTo, LocalDate invoiceDateFrom, LocalDate invoiceDateTo,
                                      CriteriaBuilder cb, Root<ViewReportConsumiCarburanti> root) {
    List<Predicate> predicates = new ArrayList<>();
    if (invoiced && !notInvoiced) {
      predicates.add(cb.isNotNull(root.get(ViewReportConsumiCarburanti_.NUMERO_FATTURA)));
    }
    if (notInvoiced && !invoiced) {
      predicates.add(cb.isNull(root.get(ViewReportConsumiCarburanti_.NUMERO_FATTURA)));
    }

    if (consumptionDateFrom != null && consumptionDateTo != null) {
      predicates
        .add(cb.between(root.get(ViewReportConsumiCarburanti_.DATA_EROGAZIONE_SERVIZIO), consumptionDateFrom, consumptionDateTo));
    }
    if (invoiceDateFrom != null && invoiceDateTo != null) {
      Instant invoiceInstantFrom = invoiceDateFrom.atStartOfDay().toInstant(ZoneOffset.UTC);
      // in order to include all the minutes in the days, select the next days and subtracts 1 sec
      Instant invoiceInstantTo = invoiceDateTo.plus(1l, ChronoUnit.DAYS)
        .atStartOfDay()
        .minus(1, ChronoUnit.SECONDS)
        .toInstant(ZoneOffset.UTC);
      predicates.add(cb.between(root.get(ViewReportConsumiCarburanti_.DATA_FATTURA), invoiceInstantFrom, invoiceInstantTo));
    }
    return predicates;
  }

  public List<CarburantiDetailDto> getDetail(DispositiviDto dispositivo,
                                             String codClienteNav, boolean invoiced, boolean notInvoiced,
                                             LocalDate consumptionDateFrom, LocalDate consumptionDateTo,
                                             LocalDate invoiceDateFrom, LocalDate invoiceDateTo) {

    CriteriaBuilder cb    = em.getCriteriaBuilder();
    var             query = cb.createQuery(ViewReportConsumiCarburanti.class);
    var             root  = query.from(ViewReportConsumiCarburanti.class);
    query.select(root);
    List<Predicate> predicates = new ArrayList<>();
    predicates.add(cb.equal(root.get(ViewReportConsumiCarburanti_.customerId), codClienteNav));
    predicates.add(cb.isNotNull(root.get(ViewReportConsumiCarburanti_.prezzoUnitarioCalcolatoNoiva)));

    if(dispositivo.getLicensePlateCountry()!=null) {
      predicates.add(cb.equal(root.get(ViewReportConsumiCarburanti_.veicoloNazioneTarga), dispositivo.getLicensePlateCountry()));
    }else {
      predicates.add(cb.isNull(root.get(ViewReportConsumiCarburanti_.veicoloNazioneTarga)));
    }
    if(dispositivo.getLicensePlate() != null) {
      predicates.add(cb.equal(root.get(ViewReportConsumiCarburanti_.veicoloTarga), dispositivo.getLicensePlate()));
    }else {
      predicates.add(cb.isNull(root.get(ViewReportConsumiCarburanti_.veicoloTarga)));
    }
    if (dispositivo.getObu() != null) {
      predicates.add(cb.equal(root.get(ViewReportConsumiCarburanti_.codiceTrackycard), dispositivo.getObu()));
    }else {
      predicates.add(cb.isNull(root.get(ViewReportConsumiCarburanti_.codiceTrackycard)));
    }
    List<Predicate> filters = fillFilters(invoiced, notInvoiced, consumptionDateFrom, consumptionDateTo, invoiceDateFrom,
                                          invoiceDateTo, cb, root);
    predicates.addAll(filters);
    query.where(cb.and(predicates.toArray(new Predicate[predicates.size()])

    ));

    List<ViewReportConsumiCarburanti> resultList = em.createQuery(query).getResultList();
    return resultList.stream()
      .map(e -> new CarburantiDetailDto(e.getId() , e.getCodiceTrackycard(), e.getVeicoloNazioneTarga(), e.getVeicoloTarga(),
                                        e.getErogatore(), e.getDataOraUtilizzo(), e.getQuantita(), e.getMeasurementUnit(),
                                        e.getArticoloDescrizione(), getPointDescription(e)))
      .collect(Collectors.toList());
  }

  private String getPointDescription(ViewReportConsumiCarburanti entity) {
    String puntoErogazione = entity.getPuntoErogazione();
    if (puntoErogazione == null)
      return "";
    PointDTO point = null;
    try {
      point = petrolPointClient.getPoint(puntoErogazione);
    } catch (Exception ex) {
      log.warn("cannot find point", ex);
    }
    if (point == null)
      return puntoErogazione;
    return defaultIfBlank(point.getDescription(), defaultIfBlank(point.getName(), puntoErogazione));
  }

//  public List<String> getTotalFuelToInvoice(String codiceCliente, Set<String> raggruppamenti) {
//    Query queryFuel = getQueryWidgetType(DettaglioStanziamentoType.CARBURANTE, codiceCliente);
//    return getResultToWidget(queryFuel);
//  }
//
//  private Query getQueryWidgetType(DettaglioStanziamentoType type, String codiceCliente) {
//    String sqlString = null;
//    if (DettaglioStanziamentoType.CARBURANTE.equals(type)) {
//      sqlString = "SELECT SUM(quantita) as quantita, unita_di_misura as unitaDiMisura "
//                  + " FROM view_report_consumi_carburanti WHERE data_fattura is NULL AND codice_cliente_nav = ? "
//                  + " and prezzo_unitario_calcolato_noiva is not NULL" + " GROUP BY unita_di_misura;";
//    } else {
//      log.error("Not implmemented query to this type: {}", type);
//    }
//
//    Query query = em.createNativeQuery(sqlString);
//    query.setParameter(1, codiceCliente);
//
//    return query;
//  }
  
  
  
  public List<String> getTotalFuelToInvoice(String codiceCliente, Set<String> article) {
    return getTotalToInvoice(em, codiceCliente, article);
  }

}
