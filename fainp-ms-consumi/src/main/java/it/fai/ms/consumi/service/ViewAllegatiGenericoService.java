package it.fai.ms.consumi.service;

import java.util.List;
import java.util.UUID;

import it.fai.ms.common.jms.fattura.GenericoDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;

public interface ViewAllegatiGenericoService {

  List<ViewAllegatoGenericoSezione2Entity> getDettagliStanziamento(List<UUID> dettagliStanziamentoId);

  List<GenericoDTO> createBodyMessage(List<ViewAllegatoGenericoSezione2Entity> codiciStanziamenti,
                                                 final RequestsCreationAttachments requestCreationAttachments,
                                                 final TemplateAllegati template);
}
