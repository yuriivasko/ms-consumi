package it.fai.ms.consumi.service.record.tesseresanbernardo;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.TessereSanBernardoGlobalIdentifier;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoViaggiRecord;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.SanBernardoRecordFileInfo;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import org.springframework.stereotype.Component;


import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Optional;

@Component
public class SanBernardoRecordConsumoMapper
  extends AbstractRecordConsumoMapper
  implements RecordConsumoMapper<SanBernardoViaggiRecord, SanBernardo> {


  public static final String SOURCE_TYPE = "elenco_viaggi_gsb";


  @Override
  public SanBernardo mapRecordToConsumo(SanBernardoViaggiRecord commonRecord) throws Exception {

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    Instant ingestionTime = getNow();

    Source source = new Source(ingestionTime, ingestionTime, SOURCE_TYPE);
    source.setFileName(commonRecord.getFileName());
    source.setRowNumber(commonRecord.getRowNumber());
    SanBernardo consumo = new SanBernardo(source,
      null,
      Optional.of(new TessereSanBernardoGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);
    boolean isTessereSanBernardo = commonRecord.getTipoTitolo().equals(SanBernardo.SAN_BERNARDO_TESSERE);
    Device device = new Device(commonRecord.getPanNumber(), isTessereSanBernardo ? TipoDispositivoEnum.TES_TRAF_GRAN_SANBERNARDO : TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO);
    device.setPan(commonRecord.getPanNumber());
    device.setServiceType(TipoServizioEnum.PEDAGGI_GRAN_SAN_BERNARDO);
    consumo.setDevice(device);
    //tipo consumo by default is D
    TollPoint tp = new TollPoint();
    Instant date = sdf.parse(commonRecord.getData()).toInstant();
    tp.setTime(date);
    consumo.setTollPointExit(tp);
    consumo.setVehicle(Vehicle.newEmptyVehicleWithFareClass(commonRecord.getClasse()));
//    if(!isTessereSanBernardo){
//
//      String customerId = sanBernardoRecordFileInfo.getBuoniOrdiniSanBernardoMap().get(commonRecord.getPanNumber());
//      if(customerId==null){
//        throw new Exception("Customer not found for device "+commonRecord.getPanNumber());
//      }
//      consumo.setCustomerId(customerId);
//    }

    return consumo;
  }

  protected Instant getNow() {
    return Instant.now();
  }


}
