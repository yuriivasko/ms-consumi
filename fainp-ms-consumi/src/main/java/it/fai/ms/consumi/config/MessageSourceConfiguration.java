package it.fai.ms.consumi.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessageSourceConfiguration {
  public static final String BASE_PATH = "i18n/";
  public static final String REPORT_MESSAGES = "reportMessages";

  @Bean
  @Qualifier(REPORT_MESSAGES)
  public MessageSource resourceBundleMessageSource(){
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasename(BASE_PATH + REPORT_MESSAGES);
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }
}