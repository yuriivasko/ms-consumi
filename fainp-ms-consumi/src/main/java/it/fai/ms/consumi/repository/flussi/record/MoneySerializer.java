package it.fai.ms.consumi.repository.flussi.record;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import javax.money.MonetaryAmount;
import java.io.IOException;

public class MoneySerializer
  extends JsonSerializer<MonetaryAmount> {

  public MoneySerializer() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public void serialize(MonetaryAmount monetaryAmount, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
    jsonGenerator.writeStartObject();
    if (monetaryAmount == null){
      jsonGenerator.writeNull();
    } else {
      jsonGenerator.writeStringField("value", monetaryAmount.toString());
    }
    jsonGenerator.writeEndObject();
  }

}
