package it.fai.ms.consumi.repository.stanziamento.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.ArchivioDettaglioStanziamentoEntity;

@Entity
@Table(name = "archivio_stanziamento")
@Immutable
public class ArchivioStanziamentoEntity extends MappedStanziamentoEntity{

  @ManyToMany(mappedBy = "stanziamenti")
  private final Set<ArchivioDettaglioStanziamentoEntity> dettaglioStanziamenti = new HashSet<>();

  @SuppressWarnings("unchecked")
  @Override
  public Set<ArchivioDettaglioStanziamentoEntity> getDettaglioStanziamenti() {
    return dettaglioStanziamenti;
  }

  
}
