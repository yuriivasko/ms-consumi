package it.fai.ms.consumi.service.validator.internal;

import java.util.HashMap;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.repository.viewdettaglistanziamenti.ViewDettaglioStanziamentiCounter;
import it.fai.ms.consumi.repository.viewdettaglistanziamenti.ViewDettaglioStanziamentiCounterRepository;

@Service
public class ConsumoValidator {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final ViewDettaglioStanziamentiCounterRepository                            viewDettaglioStanziamentiCounterRepository;
  private final NotificationService     notificationService;

  @Inject
  private Environment env;

  @Inject
  public ConsumoValidator(final ViewDettaglioStanziamentiCounterRepository viewDettaglioStanziamentiCounterRepository, final NotificationService _notificationService) {
    this.viewDettaglioStanziamentiCounterRepository = viewDettaglioStanziamentiCounterRepository;
    notificationService = _notificationService;
  }

  /*
   *  se il esiste un consumo uguale (es. dispositivo+data+ora+importoEsegno+InvoiceType) → anomalia (non bloccante)
   */
  public void validateDettagliStanziamento(final String codiceStanziamento) {
    if ("true".equalsIgnoreCase(env.getProperty("validator.disablewarnings"))) {
      _log.warn("ATTENTION! warningValidatorService is disabled trough \"validator.disablewarnings\" property!");
      return;

    }
    _log.debug("validateDettaglioStanziamanto {}",codiceStanziamento);

    Set<ViewDettaglioStanziamentiCounter> counters = viewDettaglioStanziamentiCounterRepository.findDettaglioStanziamentiCounterGreaterThan1ByCodiceStanzimento(codiceStanziamento);

    for (ViewDettaglioStanziamentiCounter item : counters) {
        _log.warn("SENDING ANOMALIA: Dettaglio stanziamento già inserito per dispositivo,data,segno e invoiceType: "+item.toString());
        var messageTextKV = new HashMap<String, Object>();
        messageTextKV.put("message", "Dettaglio stanziamento già esistente per dispositivo,data,segno e invoiceType");
        messageTextKV.put("detail", item.toString());

        notificationService.notify("CPV-w007", messageTextKV);
    }
  }

}
