package it.fai.ms.consumi.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

import it.fai.ms.consumi.domain.enumeration.Format;

public class TransactionFuelDetailDTO implements Serializable {

  private static final long serialVersionUID = -7122246521515748591L;

  private String globalIdentifier;

  private UUID detailId;

  private BigDecimal quantity;

  private BigDecimal percIva;

  private BigDecimal price;

  private BigDecimal cost;

  private Format tipoSorgente;

  private Instant dataOraUtilizzo;

  private String codiceTrackyCard;

  private String puntoErogazione;

  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public TransactionFuelDetailDTO globalIdentifier(String globalIdentifier) {
    this.globalIdentifier = globalIdentifier;
    return this;
  }

  public void setGlobalIdentifier(String globalIdentifier) {
    this.globalIdentifier = globalIdentifier;
  }

  public UUID getDetailId() {
    return detailId;
  }

  public TransactionFuelDetailDTO detailId(UUID detailId) {
    this.detailId = detailId;
    return this;
  }

  public void setDetailId(UUID detailId) {
    this.detailId = detailId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public TransactionFuelDetailDTO quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getPercIva() {
    return percIva;
  }

  public TransactionFuelDetailDTO percIva(BigDecimal percIva) {
    this.percIva = percIva;
    return this;
  }

  public void setPercIva(BigDecimal percIva) {
    this.percIva = percIva;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public TransactionFuelDetailDTO price(BigDecimal price) {
    this.price = price;
    return this;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getCost() {
    return cost;
  }

  public TransactionFuelDetailDTO cost(BigDecimal cost) {
    this.cost = cost;
    return this;
  }

  public void setCost(BigDecimal cost) {
    this.cost = cost;
  }

  public Format getTipoSorgente() {
    return tipoSorgente;
  }

  public TransactionFuelDetailDTO tipoSorgente(Format tipoSorgente) {
    this.tipoSorgente = tipoSorgente;
    return this;
  }

  public void setTipoSorgente(Format tipoSorgente) {
    this.tipoSorgente = tipoSorgente;
  }

  public Instant getDataOraUtilizzo() {
    return dataOraUtilizzo;
  }

  public TransactionFuelDetailDTO dataOraUtilizzo(Instant dataOraUtilizzo) {
    this.dataOraUtilizzo = dataOraUtilizzo;
    return this;
  }

  public void setDataOraUtilizzo(Instant dataOraUtilizzo) {
    this.dataOraUtilizzo = dataOraUtilizzo;
  }

  public String getCodiceTrackyCard() {
    return codiceTrackyCard;
  }

  public TransactionFuelDetailDTO codiceTrackyCard(String codiceTrackyCard) {
    this.codiceTrackyCard = codiceTrackyCard;
    return this;
  }

  public void setCodiceTrackyCard(String codiceTrackyCard) {
    this.codiceTrackyCard = codiceTrackyCard;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public TransactionFuelDetailDTO puntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
    return this;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TransactionFuelDetailDTO [globalIdentifier=");
    builder.append(globalIdentifier);
    builder.append(", detailId=");
    builder.append(detailId);
    builder.append(", quantity=");
    builder.append(quantity);
    builder.append(", percIva=");
    builder.append(percIva);
    builder.append(", price=");
    builder.append(price);
    builder.append(", cost=");
    builder.append(cost);
    builder.append(", tipoSorgente=");
    builder.append(tipoSorgente);
    builder.append(", dataOraUtilizzo=");
    builder.append(dataOraUtilizzo);
    builder.append(", codiceTrackyCard=");
    builder.append(codiceTrackyCard);
    builder.append(", puntoErogazione=");
    builder.append(puntoErogazione);
    builder.append("]");
    return builder.toString();
  }

}
