package it.fai.ms.consumi.domain.consumi;

public interface RouteName {

  String getRouteName();

}
