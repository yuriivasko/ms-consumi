package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;

public interface DeviceService {

  Optional<Device> findDeviceBySeriale(@NotNull String _deviceSeriale, @NotNull TipoDispositivoEnum _deviceType, String _contract);

  Optional<Device> findDeviceByLicensePlate(String licensePlate, TipoDispositivoEnum type, Instant _associationDate);

  Optional<Device> findDeviceByPan(String _pan, TipoServizioEnum _serviceType, String _contract);

  Optional<ViewStoricoDispositivoVeicoloContratto> findStoricoDispositivo(@NotNull String _deviceSeriale, @NotNull TipoDispositivoEnum _deviceType,
                                                                @NotNull Instant _date);

  Optional<StoricoStatoServizio> findStoricoStatoServizio(@NotNull String _deviceUuid,
                                                          @NotNull TipoServizioEnum _serviceType,
                                                          @NotNull Instant _date);

  List<StoricoStatoServizio> findStatiServizioInterval(String deviceDmlUniqueIdentifier, String tipoServizio, Instant dataInizio,
                                                       Instant dataFine);
}
