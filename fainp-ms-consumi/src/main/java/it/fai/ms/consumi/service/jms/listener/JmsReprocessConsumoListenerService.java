package it.fai.ms.consumi.service.jms.listener;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.JmsObjectMessageListenerTemplate;
import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.consumi.ConsumoReprocessDTO;
import it.fai.ms.common.jms.notification_v2.EnrichedNotification;
import it.fai.ms.consumi.jms.notification_v2.ConsumoNotReprocessableNotification;
import it.fai.ms.consumi.service.processor.RecordConsumoReprocessorService;
import it.fai.ms.consumi.service.processor.RecordNotReprocessedException;
import it.fai.ms.consumi.service.processor.RecordNotRetrievableException;
import it.fai.ms.consumi.web.rest.errors.CustomException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("no-jmslistener") // Workaround per bug aggregazione sui consumi riprocessati
// in questo modo, avendo un singolo listener e quindi un singolo thread, l'aggregazione non verrà mai fatta per gli stanziamenti riprocessati evitando così che la concurrency faccia aggregazioni incorrette.
public class JmsReprocessConsumoListenerService
  extends JmsObjectMessageListenerTemplate<ConsumoReprocessDTO>
  implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private RecordConsumoReprocessorService recordConsumoReprocessorService;
  private NotificationService notificationService;

  public JmsReprocessConsumoListenerService(RecordConsumoReprocessorService recordConsumoReprocessorResource, NotificationService notificationService) {
    this.recordConsumoReprocessorService = recordConsumoReprocessorResource;
    this.notificationService = notificationService;
  }

  @Override
  protected void consumeMessage(ConsumoReprocessDTO typedMessage) {
    try {
      recordConsumoReprocessorService.reprocessSingleRecord(typedMessage.getRecordType(), typedMessage.getRecordUuid());
    } catch (RecordNotReprocessedException e) {
      log.warn("Reprocessing gives error: {} - {}", e.getClass(), e.getMessage());
    } catch (RecordNotRetrievableException e) {
      log.warn("Reprocessing gives error: {} - {}", e.getClass(), e.getMessage());
      log.warn("Sending NOT REPROCESSABLE notification: {} - {}", e.getClass(), e.getMessage());
      notificationService.notify(new ConsumoNotReprocessableNotification(
          typedMessage.getJobName(),
          typedMessage.getFilename(),
          StringUtils.isNumeric(typedMessage.getFileLine()) ? Long.valueOf(typedMessage.getFileLine()) : -1,
        "FCJOB-199",
          "Record not retrievable, cannot reprocess this notification (id:=[" + typedMessage.getNotificationId() + "]): " + typedMessage.getText()
        )
      );
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.CONSUMO_REPROCESS;
  }
}
