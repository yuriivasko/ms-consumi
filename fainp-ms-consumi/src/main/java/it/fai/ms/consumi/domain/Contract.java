package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.util.Objects;

public class Contract implements Serializable {
  private static final long serialVersionUID = 4596855810352241955L;

  private String       companyCode;
  private final String id;
  private String       state;

  private Contract(String id, String companyCode, String state) {
    this.id = id;
    this.companyCode = companyCode;
    this.state = state;
  }

  public static Contract newUnsafeContract(String _id, String _companyCode, String _state){
    return new Contract(
      _id != null ? _id : "",
      _companyCode != null ? _companyCode : "",
      _state != null ? _state : ""
    );
  }

  public Contract(final String _id) {
    id = Objects.requireNonNull(_id, "Parameter id is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Contract obj = getClass().cast(_obj);
      res = Objects.equals(obj.id, id) && Objects.equals(obj.companyCode, companyCode) && Objects.equals(obj.state, state);
    }
    return res;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getId() {
    return id;
  }

  public String getState() {
    return state;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, companyCode, state);
  }

  public void setCompanyCode(final String _companyCode) {
    companyCode = _companyCode;
  }

  public void setState(final String _state) {
    state = _state;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("id=")
                              .append(id)
                              .append(",companyCode=")
                              .append(companyCode)
                              .append(",state=")
                              .append(state)
                              .append("]")
                              .toString();
  }

}
