package it.fai.ms.consumi.service.canoni;

import it.fai.ms.common.jms.fattura.canoni.EsitoCanoni;

public interface CanoniClientService {

  void postCanoniDispositivi(EsitoCanoni esito);

  void postCanoniServizi(EsitoCanoni esito);

}