package it.fai.ms.consumi.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import it.fai.ms.consumi.web.rest.errors.CustomException;
import it.fai.ms.consumi.web.rest.errors.PayloadError;

public abstract class AbstractResource {

  public final static String BASE_PATH = "/api";

  protected final Logger log = LoggerFactory.getLogger(getClass());

  public AbstractResource() { }

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<PayloadError> exceptionHandler(CustomException ex) {
    return ResponseEntity.status(ex.getHttpStatus())
        .body(ex.getPayloadError());
  }  
  
}
