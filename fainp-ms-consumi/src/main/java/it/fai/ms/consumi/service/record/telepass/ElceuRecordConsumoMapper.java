package it.fai.ms.consumi.service.record.telepass;

import java.time.Instant;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elceu;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElceuGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryRecord;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElceuRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class ElceuRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<ElceuRecord, Elceu> {

  @Override
  public Elceu mapRecordToConsumo(ElceuRecord commonRecord) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant ingestionTime = commonRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());
    Source source = new Source(ingestionTime, acquisitionDate, TelepassSourceName.ELCEU);
    source.setFileName(commonRecord.getFileName());
    source.setRowNumber(commonRecord.getRowNumber());
    Elceu toConsumo = new Elceu(source, commonRecord.getRecordCode(),
                                Optional.of(new ElceuGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);
    Consumo toConsumoBase = toConsumo;
    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();
    transaction.setDetailCode(commonRecord.getTransactionDetail());
    toConsumoBase.setTransaction(transaction);
    toConsumoBase.setSupplierInvoiceDocument(commonRecord.getDocumentNumber());
    // CONTRACT -> assente su questo flusso

    // CONSUMO.AMOUNT
    MonetaryRecord monetaryRecord = commonRecord;
    Amount amount = new Amount();
    amount.setExchangeRate(null);
    amount.setVatRate(monetaryRecord.getVatRateBigDecimal());
    MonetaryAmount amountExcludedVat = (monetaryRecord.getAmount_novat() != null) ? monetaryRecord.getAmount_novat() : monetaryRecord.getAmount_novat_excluded_discount();
    if (amountExcludedVat!=null && !amountExcludedVat.isZero()) {
      amount.setAmountExcludedVat(amountExcludedVat);
    }else {
      amount.setAmountIncludedVat(monetaryRecord.getAmount_vat());
    }
    toConsumoBase.setAmount(amount);

    ConsumoPedaggio toConsumoPedaggio = toConsumo;
    toConsumoPedaggio.setProcessed(commonRecord.isProcessed());

    // CONSUMOPEDAGGIO.DEVICE
    ElceuRecord elceuRecord = commonRecord;
//  var obu = (StringUtils.isBlank(elceuRecord.getObu())?elceuRecord.getExternalOBUIdentifier():elceuRecord.getObu());
//  Device device = new Device(obu,TipoDispositivoEnum.TELEPASS_EUROPEO);
    //L'Obu non è obbligatorio e non e' detto che sia il nostro "obunumber". la coppia PAN+TIPOSERVIZIO invece e' sempre univoca
    Device device = new Device(TipoDispositivoEnum.TELEPASS_EUROPEO);
    device.setPan(elceuRecord.getTelepassPanNumber());
    device.setServiceType(elceuRecord.getService_type());
    toConsumoPedaggio.setDevice(device);

    // CONSUMOPEDAGGIO.TOLLPOINTENTRY
    TollPoint tollpointEntry = new TollPoint();
    GlobalGate globalGateEntry = new GlobalGate(elceuRecord.getEntryGateCode());
    // globalGateEntry.setDescription();
    tollpointEntry.setGlobalGate(globalGateEntry);
    if (elceuRecord.getEntryTimestamp() != null && !elceuRecord.getEntryTimestamp()
                                                               .isEmpty()) {
      tollpointEntry.setTime(parseTimestamp(elceuRecord.getEntryTimestamp()));
    } else if (elceuRecord.getEntryDate() != null && !elceuRecord.getEntryDate()
                                                                 .isEmpty()) {
      tollpointEntry.setTime(calculateInstantByDateAndTime(elceuRecord.getEntryDate(), elceuRecord.getEntryTime(), yyyyMMdd_dateformat,
                                                           HHmmss_timeformat).get());
    }
    toConsumoPedaggio.setTollPointEntry(tollpointEntry);

    // CONSUMOPEDAGGIO.TOLLPOINTEXIT
    TollPoint tollpointExit = new TollPoint();
    GlobalGate globalGateExit = new GlobalGate(elceuRecord.getExitGateCode());
    // globalGateExit.setDescription(); //TODO STCON

    if (elceuRecord.getExitTimestamp() != null && !elceuRecord.getExitTimestamp()
                                                               .isEmpty()) {
      tollpointExit.setTime(parseTimestamp(elceuRecord.getExitTimestamp()));
    } else if (elceuRecord.getExitDate() != null && !elceuRecord.getExitDate()
                                                                 .isEmpty()) {
      tollpointExit.setTime(calculateInstantByDateAndTime(elceuRecord.getExitDate(), elceuRecord.getExitTime(), yyyyMMdd_dateformat,
                                                          HHmmss_timeformat).get());
    }

    tollpointExit.setGlobalGate(globalGateExit);
    toConsumoPedaggio.setTollPointExit(tollpointExit);
    
    // CONSUMOPEDAGGIO.VEHICLE
    var vehicle = new Vehicle();
    vehicle.setFareClass(elceuRecord.getPricingClass());
    toConsumoPedaggio.setVehicle(vehicle);
    

    // CONSUMOPEDAGGIO. altri campi

    // toConsumoPedaggio.setTotalAmount(new BigDecimal(commonRecord.getTotalAmount())); // Calcolato
    toConsumoPedaggio.setSplitNumber(elceuRecord.getSplitNumber());
    toConsumoPedaggio.setCompensationNumber(elceuRecord.getCompensationNumber());
    toConsumoPedaggio.setTspExitCountryCode(elceuRecord.getTspExitCountryCode());
    toConsumoPedaggio.setTspExitNumber(elceuRecord.getTspExitNumber());
    toConsumoPedaggio.setTspRespCountryCode(elceuRecord.getTspRespCountryCode());
    toConsumoPedaggio.setTspRespNumber(elceuRecord.getTspRespNumber());
    toConsumoPedaggio.setTollCharger(elceuRecord.getTollCharger());
    toConsumoPedaggio.setTollGate(elceuRecord.getTollGate());
    toConsumoPedaggio.setLaneId(elceuRecord.getLaneId());
    toConsumoPedaggio.setNetworkCode(elceuRecord.getEuropeanNetworkCode());
    toConsumoPedaggio.getTransaction()
                     .setSign(elceuRecord.getSignOfTransaction());
    toConsumoPedaggio.setExitTransitDateTime(elceuRecord.getExitTransitDateTime());
    toConsumoPedaggio.setAdditionalInfo(elceuRecord.getAdditionalInfo());
    toConsumoPedaggio.setRoute(elceuRecord.getRoute());
    toConsumoPedaggio.setRoadType(elceuRecord.getRoadType());
    toConsumoPedaggio.setRegion(elceuRecord.getRegion());

    // toConsumoPedaggio.setImponOibileAdr(imponibileAdr); --> non applicabile

    return toConsumo;
  }


}
