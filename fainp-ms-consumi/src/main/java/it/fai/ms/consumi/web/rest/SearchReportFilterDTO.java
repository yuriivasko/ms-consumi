package it.fai.ms.consumi.web.rest;


import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class SearchReportFilterDTO {
  private boolean invoiced;
  private boolean notInvoiced;
  private LocalDate consumptionDateFrom;
  private LocalDate consumptionDateTo;
  private LocalDate invoiceDateFrom;
  private LocalDate invoiceDateTo;
  private List<DispositiviDto> dispositivi;

  public SearchReportFilterDTO() {
  }

  public SearchReportFilterDTO(boolean invoiced, boolean notInvoiced, LocalDate consumptionDateFrom, LocalDate consumptionDateTo, LocalDate invoiceDateFrom, LocalDate invoiceDateTo) {
    this.invoiced = invoiced;
    this.notInvoiced = notInvoiced;
    this.consumptionDateFrom = consumptionDateFrom;
    this.consumptionDateTo = consumptionDateTo;
    this.invoiceDateFrom = invoiceDateFrom;
    this.invoiceDateTo = invoiceDateTo;
  }

  public boolean getInvoiced() {
    return invoiced;
  }

  public void setInvoiced(boolean invoiced) {
    this.invoiced = invoiced;
  }

  public boolean getNotInvoiced() {
    return notInvoiced;
  }

  public void setNotInvoiced(boolean notInvoiced) {
    this.notInvoiced = notInvoiced;
  }

  public LocalDate getConsumptionDateFrom() {
    return consumptionDateFrom;
  }

  public void setConsumptionDateFrom(LocalDate consumptionDateFrom) {
    this.consumptionDateFrom = consumptionDateFrom;
  }

  public LocalDate getConsumptionDateTo() {
    return consumptionDateTo;
  }

  public void setConsumptionDateTo(LocalDate consumptionDateTo) {
    this.consumptionDateTo = consumptionDateTo;
  }

  public LocalDate getInvoiceDateFrom() {
    return invoiceDateFrom;
  }

  public void setInvoiceDateFrom(LocalDate invoiceDateFrom) {
    this.invoiceDateFrom = invoiceDateFrom;
  }

  public LocalDate getInvoiceDateTo() {
    return invoiceDateTo;
  }

  public void setInvoiceDateTo(LocalDate invoiceDateTo) {
    this.invoiceDateTo = invoiceDateTo;
  }

  public List<DispositiviDto> getDispositivi() {
    return dispositivi;
  }

  public void setDispositivi(List<DispositiviDto> dispositivi) {
    this.dispositivi = dispositivi;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SearchReportFilterDTO that = (SearchReportFilterDTO) o;
    return Objects.equals(invoiced, that.invoiced) &&
      Objects.equals(notInvoiced, that.notInvoiced) &&
      Objects.equals(consumptionDateFrom, that.consumptionDateFrom) &&
      Objects.equals(consumptionDateTo, that.consumptionDateTo) &&
      Objects.equals(invoiceDateFrom, that.invoiceDateFrom) &&
      Objects.equals(invoiceDateTo, that.invoiceDateTo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(invoiced, notInvoiced, consumptionDateFrom, consumptionDateTo, invoiceDateFrom, invoiceDateTo);
  }

  @Override
  public String toString() {
    return "SearchReportFilterDTO{" +
      "invoiced=" + invoiced +
      ", notInvoiced=" + notInvoiced +
      ", consumptionDateFrom=" + consumptionDateFrom +
      ", consumptionDateTo=" + consumptionDateTo +
      ", invoiceDateFrom=" + invoiceDateFrom +
      ", invoiceDateTo=" + invoiceDateTo +
      '}';
  }
}
