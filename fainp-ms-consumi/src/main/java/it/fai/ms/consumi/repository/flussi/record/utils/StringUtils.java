package it.fai.ms.consumi.repository.flussi.record.utils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public final class StringUtils {

  private static final String AB  = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz()[]{}?!&+-*/";
  private static SecureRandom rnd = new SecureRandom();

  public StringUtils() {
    super();
  }

  public static String Random(int len) {
    StringBuilder sb = new StringBuilder(len);
    for (int i = 0; i < len; i++) {
      sb.append(AB.charAt(rnd.nextInt(AB.length())));
    }
    return sb.toString();
  }

  public static String MD5(String input) {
    try {
      java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
      byte[] array = md.digest(input.getBytes("UTF-8"));
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < array.length; i++) {
        sb.append(String.format("%02x", array[i]));
      }
      return sb.toString();
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
      return null;
    }
  }

  public static String Join(String[] elements) {
    return Join("", elements);
  }

  public static String Join(String delimiter, String[] elements) {
    return delimiter != null && elements != null ? String.join(delimiter, elements) : "";
  }

  public static String Concat(String delimiter, String... elements) {
    return Join(delimiter, elements);
  }

}
