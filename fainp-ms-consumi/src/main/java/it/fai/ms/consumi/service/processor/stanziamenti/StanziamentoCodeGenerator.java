package it.fai.ms.consumi.service.processor.stanziamenti;

import java.util.UUID;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.InvoiceType;

@Component
public class StanziamentoCodeGenerator implements StanziamentoCodeGeneratorInterface {

  public StanziamentoCodeGenerator() {
  }

  public String generate(final InvoiceType _invoiceType) {
    return generate(_invoiceType.name());
  }

  private String generate(final String _suffix) {

    // FIXME!! Use random UUID after tests
    UUID uuid = UUID.randomUUID();
    String uuidUpperCase = uuid.toString()
                               .toUpperCase();
    String code = uuidUpperCase + _suffix;
    return code;

    // return String.valueOf(System.currentTimeMillis() + random.nextInt() + _suffix);
  }

}
