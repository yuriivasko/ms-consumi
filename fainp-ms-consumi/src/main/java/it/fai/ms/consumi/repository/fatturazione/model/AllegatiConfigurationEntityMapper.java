package it.fai.ms.consumi.repository.fatturazione.model;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.fatturazione.AllegatiConfiguration;

@Component
@Validated
public class AllegatiConfigurationEntityMapper {
  
  public AllegatiConfigurationEntityMapper() {
  }

  public AllegatiConfiguration toDomain(final AllegatiConfigurationEntity _entity) {
    AllegatiConfiguration domain = new AllegatiConfiguration();
    domain.setCodiceRaggruppamentoArticolo(_entity.getCodiceRaggruppamentoArticolo());
    domain.setDescrizione(_entity.getDescrizione());
    domain.setGeneraAllegato(_entity.isGeneraAllegato());
    domain.setTipoDettaglioStanziamento(_entity.getTipoDettaglioStanziamento());
    domain.setTransCodeAllegatoFix(_entity.getTransCodeAllegatoFix());
    domain.setTemplate(_entity.getTemplate());
    return domain;
  }

  public AllegatiConfigurationEntity toEntity(final AllegatiConfiguration _domain) {
    AllegatiConfigurationEntity entity = new AllegatiConfigurationEntity();
    entity.setCodiceRaggruppamentoArticolo(_domain.getCodiceRaggruppamentoArticolo());
    entity.setDescrizione(_domain.getDescrizione());
    entity.setGeneraAllegato(_domain.isGeneraAllegato());
    entity.setTipoDettaglioStanziamento(_domain.getTipoDettaglioStanziamento());
    entity.setTransCodeAllegatoFix(_domain.getTransCodeAllegatoFix());
    entity.setTemplate(_domain.getTemplate());
    return entity;
  }

}
