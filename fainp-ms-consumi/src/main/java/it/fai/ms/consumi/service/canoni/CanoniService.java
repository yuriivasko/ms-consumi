package it.fai.ms.consumi.service.canoni;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.repository.canoni.CanoniRepository;
import it.fai.ms.consumi.repository.contract.ContractRepository;
import it.fai.ms.consumi.repository.device.DeviceRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.vehicle.VehicleRepository;

@Service
public class CanoniService {

  private Logger                    log = LoggerFactory.getLogger(getClass());

  private final ContractRepository contractRepo;
  private final VehicleRepository  vehicleRepo;
  private final DeviceRepository   deviceRepo;

  private CanoniRepository canoniRepository;

  public CanoniService(CanoniRepository canoniRepository, ContractRepository contractRepo, VehicleRepository vehicleRepo,
                       DeviceRepository deviceRepo) {
    this.canoniRepository = canoniRepository;
    this.contractRepo     = contractRepo;
    this.vehicleRepo      = vehicleRepo;
    this.deviceRepo       = deviceRepo;
  }



  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.SUPPORTS)
  public Stream<DispositivoForCanoni> canoniDispositivi(Instant dataDa, Instant dataA, TipoDispositivoEnum tipoDispositivo,
                                                        String codiceCliente) {
    log.info("executing canoni dispositivi for client {} tipo {} da {} a {}", codiceCliente, tipoDispositivo, dataDa, dataA);
    return groupByVehiclePrimaAttivazione(canoniRepository.dispositiviAttivi(dataDa, dataA, tipoDispositivo, codiceCliente)
      .map(d -> addData(d, dataA)));
  }

  @Transactional(isolation = Isolation.READ_UNCOMMITTED,propagation = Propagation.SUPPORTS)
  public Stream<DispositivoForCanoni> canoniServizi(Instant dataDa, Instant dataA, TipoServizioEnum tipoServizio, String codiceCliente) {

    return groupByVehiclePrimaAttivazione(
                                          canoniRepository.serviziAttivi(dataDa, dataA, tipoServizio, codiceCliente)
                                            .map(d -> addData(d, dataA)));
  }

  public Stream<DispositivoForCanoni> groupByVehiclePrimaAttivazione(Stream<DispositivoForCanoni> stream) {
    Map<Object, Optional<DispositivoForCanoni>> map = stream
      .collect(groupingBy(d -> DispositivoForCanoni.createFromVehicleDataPrimaAttivazione(d), reducing((d1, d2) -> d1.combine(d2))));
    return map.values()
      .stream()
      .map(o -> (DispositivoForCanoni) o.get());
  }

  private DispositivoForCanoni addData(DispositivoForCanoni d,  Instant dataRiferimento) {
    if (d.getCodiceCliente() == null) {
      if (d.getIdContratto() == null) {
        Optional<StoricoDispositivo> device = deviceRepo.findDeviceAtDate(d.getDeviceID(), dataRiferimento);
        device.ifPresent(dev -> d.setIdContratto(dev.getDmlUniqueIdentifier()));
      }
      var contratto = contractRepo.findContrattoAtDate(d.getIdContratto(), dataRiferimento);
      contratto.ifPresent(c -> d.setCodiceCliente(c.getCodiceAzienda()));
    }
    var veicolo = vehicleRepo.findVeicoloForDeviceAtDate(d.getDeviceID(), dataRiferimento);
    veicolo.ifPresent(v -> {
      d.setVeicle(v);
    });

    return d;
  }


}
