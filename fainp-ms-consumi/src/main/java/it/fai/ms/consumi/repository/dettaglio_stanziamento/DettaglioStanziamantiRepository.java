package it.fai.ms.consumi.repository.dettaglio_stanziamento;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import it.fai.ms.common.jms.dto.petrolpump.CostRevenue;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;

public interface DettaglioStanziamantiRepository {

  DettaglioStanziamento clone(@NotNull DettaglioStanziamento ds);

  Set<DettaglioStanziamento> findByCodiceStanziamento(@NotNull String dettaglioStanziamentoId);

  Set<DettaglioStanziamento> findByCodiceStanziamento(@NotNull String dettaglioStanziamentoId,
                                                      StanziamentiDetailsType stanziamentiDetailsType);

  Set<DettaglioStanziamento> findByCodiciStanziamentoAndTipoDettaglio(@NotNull Set<String> codes,
                                                                      final DettaglioStanziamentoType tipoDettaglio);

  List<DettaglioStanziamentoCarburanteEntity> findToUpdatePriceTable(PriceTableFilterDTO priceTableFilter);

  Set<DettaglioStanziamento> findByGlobalIdentifierId(@NotNull GlobalIdentifier globalIdentifier);

  void save(@NotNull DettaglioStanziamento dettaglioStanziamento);

  List<DettaglioStanziamentoEntity> getAllDettaglioStanziamentoCarburante();

  List<DettaglioStanziamentoEntity> getAllDettaglioStanziamentoPedaggio();

  List<DettaglioStanziamentoEntity> getAllDettaglioStanziamentoGenerico();

  void save(@NotNull DettaglioStanziamentoEntity _allocationDetailEntity);

  DettaglioStanziamento mapToDomain(DettaglioStanziamentoEntity dsEntity);

  DettaglioStanziamentoEntity mapToEntity(DettaglioStanziamento clone);

  DettaglioStanziamentoEntity cloneAndModify(@NotNull DettaglioStanziamentoEntity _dettaglioEntity);

  @Deprecated // sostituito da vista con counter
  boolean existByDeviceAndDateAndSignAndInvoiceType(DettaglioStanziamento _dettaglioStanziamento);

  DettaglioStanziamentoEntity update(@NotNull DettaglioStanziamentoEntity _allocationDetailEntity);

  Instant findLastDettagliStanziamentoDate(String tipoFlusso);

  List<DettaglioStanziamentoPedaggioEntity> findDettagliStanziamentiProvvisoriBySorgenteAndCodRaggruppamentiBeforeDate(String sorgente,
                                                                                                                       Map<String, Integer> codiceRaggruppamentoGiorniMap,
                                                                                                                       Instant ingestionTime);

  DettaglioStanziamentoEntity cloneAndCleanId(@NotNull DettaglioStanziamentoEntity _dettaglioEntity, boolean includeStanziamento);

  DettaglioStanziamento cloneAndChangeSign(@NotNull DettaglioStanziamento _allocationDetail);

  Set<DettaglioStanziamento> findByGlobalIdentifierId(@NotNull GlobalIdentifier globalIdentifier,
                                                      StanziamentiDetailsType stanziamentiDetailsType);

  Optional<DettaglioStanziamentoEntity> findByGlobalIdentifierAndTypeAndIdNotIsAndCostRevenueExcludingStorni(@NotNull String _globalIdentifierId,
                                                                                                             StanziamentiDetailsType detailsType, UUID uuid, CostRevenue costProceedstoFind);

  Optional<DettaglioStanziamentoEntity> findLastDettaglioCarburantiByGlobalIdentifierExcludingStorni(@NotNull final String _globalIdentifierId);


    List<UUID> findDettagliStanziamentoIdByCodiciStanziamento(List<String> codiciStanziamento);
}
