package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.SecondaryDeviceSupplier;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.consumi.pedaggi.ESE_TOT_STATUS;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Objects;
import java.util.Optional;

public class Elvia
  extends ConsumoPedaggio
  implements SecondaryDeviceSupplier {

  private String movementDescription;
  private Amount eseAmount;
  private String eseGlobalIdentifier;

  public Elvia(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public Amount getAmount() {
    if (getEseTotStatus() == null || getEseTotStatus() == ESE_TOT_STATUS.TOT)
      return super.getAmount();
    else
      return eseAmount;
  }

  public void setEseAmount(Amount eseAmount) {
    this.eseAmount = eseAmount;
  }

  public Amount getEseAmount() {
    return eseAmount;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Elvia obj = getClass().cast(_obj);
      res = super.equals(_obj);
    }
    return res;
  }

  @Override
  public InvoiceType getInvoiceType() {
    return InvoiceType.D;
  }

  @Override
  public String getRecordCodeToLookInParamStanz() {
    if (getEseTotStatus() == null) {
      return super.getRecordCode(); //TODO forse qui sarebbe da lanciare eccezione
    }
    return getEseTotStatus().name();
  }

  @Override
  public String getRouteName() {
    // TODO: campo "Movement Description" splittabile.Inoltre aggiungere "IMPORTO ESENTE" se esente
    return movementDescription;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode());
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<Device> getSecondaryDevice() {
    return Optional.ofNullable(getVehicle())
                   .map(vehicle -> vehicle.getLicensePlate())
                   .map(vehicleLicensePlate -> vehicleLicensePlate.getLicenseId())
                   .map(licensePlate -> {
                     Device device = new Device(licensePlate, TipoDispositivoEnum.TELEPASS_ITALIANO);
                     device.setServiceType(TipoServizioEnum.PEDAGGI_ITALIA);
                     return device;
                   });
  }

  public void setEseGlobalIdentifier(String eseGlobalIdentifier) {
    this.eseGlobalIdentifier = eseGlobalIdentifier;
  }

  public String getEseGlobalIdentifier() {
    return eseGlobalIdentifier;
  }
}
