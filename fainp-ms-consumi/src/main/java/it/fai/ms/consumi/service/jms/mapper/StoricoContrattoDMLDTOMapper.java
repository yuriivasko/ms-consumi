package it.fai.ms.consumi.service.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.ContrattoDMLDTO;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@Component
public class StoricoContrattoDMLDTOMapper {

  public ContrattoDMLDTO toDTO(final StoricoContratto data) {
    ContrattoDMLDTO dto = new ContrattoDMLDTO();
    dto.setDmlRevisionTimestamp(data.getDmlRevisionTimestamp());
    dto.setDmlUniqueIdentifier(data.getDmlUniqueIdentifier());
    dto.setCodiceAzienda(data.getCodiceAzienda());
    dto.setCodContrattoCliente(data.getContrattoNumero());
    dto.setDataUltimaVariazione(data.getDataVariazione());
    dto.setPrimario(Boolean.TRUE.equals(data.getPrimario()));
    dto.setStato(data.getStatoContratto());
    return dto;
  }

}
