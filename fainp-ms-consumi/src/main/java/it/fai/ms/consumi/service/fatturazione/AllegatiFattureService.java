package it.fai.ms.consumi.service.fatturazione;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import feign.FeignException;
import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.consumi.client.efservice.EfserviceRestClient;
import it.fai.ms.consumi.client.efservice.dto.ClienteFaiDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.fatturazione.AllegatiConfigurationRepository;
import it.fai.ms.consumi.repository.fatturazione.RequestsCreationAttachmentsRepository;
import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.service.jms.producer.AttachmentInvoiceJmsProducer;

@Service
@Transactional(noRollbackFor = { FeignException.class, IllegalArgumentException.class }, isolation = Isolation.READ_UNCOMMITTED)
public class AllegatiFattureService {

  public static final String errorCodeRaggruppamentoArticolo = "Not found AllegatiConfiguration by Codice raggruppamento articoli: ";
  public static final String errorNomeInstestazioneTessere   = "Not found NomeIntestazioneTessere for codice cliente fatturazione: ";
  public static final String errorParamStanziamenti          = "Not found Parametri Stanziamenti for codice raggruppamento articolo: ";
  private Logger             log                             = LoggerFactory.getLogger(getClass());

  private final RequestsCreationAttachmentsRepository requestCreationAttachmentRepo;
  private final AllegatiConfigurationRepository       allegatiConfigRepo;
  private final EfserviceRestClient                   efServiceRestClient;
  private final AttachmentInvoiceJmsProducer          attachmentInvoiceProducer;
  private final StanziamentiParamsRepository          stanziamentiParamRepo;

  public AllegatiFattureService(final AllegatiConfigurationRepository _allegatiConfigRepo,
                                final RequestsCreationAttachmentsRepository _requestCreationAttachmentRepo,
                                final EfserviceRestClient _efServiceRestClient,
                                final AttachmentInvoiceJmsProducer _attachmentInvoiceProducer,
                                final StanziamentiParamsRepository _stanziamentiParamRepo) {
    allegatiConfigRepo = _allegatiConfigRepo;
    requestCreationAttachmentRepo = _requestCreationAttachmentRepo;
    efServiceRestClient = _efServiceRestClient;
    attachmentInvoiceProducer = _attachmentInvoiceProducer;
    stanziamentiParamRepo = _stanziamentiParamRepo;
  }

  public void manageRaggruppamentoArticoli(final String invoiceCode, final Instant dataInvoice,
                                           final RaggruppamentoArticoliDTO raggruppamentoArticoliDTO, final BigDecimal importoTotale,
                                           final String lang, RequestsCreationAttachments requestCreationAttachments) {

    String codiceRaggrArticoli = raggruppamentoArticoliDTO.getCodice();
    AllegatiConfigurationEntity allegatiConfig = allegatiConfigRepo.findByCodiceRaggruppamentoArticoli(codiceRaggrArticoli);
    if (allegatiConfig == null) {
      throw new IllegalArgumentException(errorCodeRaggruppamentoArticolo + codiceRaggrArticoli);
    }
    DettaglioStanziamentoType tipoDettaglioStanziamento = allegatiConfig.getTipoDettaglioStanziamento();
    log.debug("Tipo Dettaglio Stanziamento: {}", tipoDettaglioStanziamento);
    if (allegatiConfig.isGeneraAllegato() && tipoDettaglioStanziamento != null) {

      String tipoServizio = allegatiConfig.getTransCodeAllegatoFix();
      
      TemplateAllegati template = allegatiConfig.getTemplate();

      if (requestCreationAttachments != null) {
    	  log.warn("Esiste già un richiesta: {}",requestCreationAttachments);
      }

	    Optional<StanziamentiParams> stanzParamOpt = stanziamentiParamRepo.findFirstByCodiceRaggruppamentoArticolo(codiceRaggrArticoli);
	    if (!stanzParamOpt.isPresent()) {
	      throw new IllegalArgumentException(errorParamStanziamenti + codiceRaggrArticoli);
	    }
	
	    StanziamentiParams stanziamentiParams = stanzParamOpt.get();
	    log.debug("Param stanziamenti {} by codice raggruppamento articolo {}", stanziamentiParams, codiceRaggrArticoli);
	    String nazioneFatturazione = stanziamentiParams.getArticle()
	                                                   .getCountry();
	
	    String codiceClienteFatturazione = raggruppamentoArticoliDTO.getCodiceClienteFatturazione();
	    ClienteFaiDTO clienteFaiDto = efServiceRestClient.findByCodiceCliente(codiceClienteFatturazione);
	    log.debug("ClienteFai retrieved by EfServiceRestClient: {}", clienteFaiDto);
	    if (clienteFaiDto.getId() == null && clienteFaiDto.getCodiceCliente() == null) {
	      throw new IllegalArgumentException(errorNomeInstestazioneTessere + codiceClienteFatturazione);
	    }
	    String nomeIntestazioneTessere = clienteFaiDto.getNomeIntestazioneTessere();
	    String descrRaggrupArticolo = allegatiConfig.getDescrizione();
	    requestCreationAttachments = new RequestsCreationAttachments().id(requestCreationAttachments!=null ? requestCreationAttachments.getId() : null)
	    																.codiceFattura(invoiceCode)
	                                                                  .codiceRaggruppamentoArticolo(codiceRaggrArticoli)
	                                                                  .descrRaggruppamentoArticolo(descrRaggrupArticolo)
	                                                                  .nomeIntestazioneTessere(nomeIntestazioneTessere)
	                                                                  .importoTotale(importoTotale)
	                                                                  .dataFattura(dataInvoice)
	                                                                  .data(Instant.now())
	                                                                  .lang(lang)
	                                                                  .tipoServizio(tipoServizio)
	                                                                  .nazioneFatturazione(nazioneFatturazione);
	    requestCreationAttachments = requestCreationAttachmentRepo.save(requestCreationAttachments);
	    log.info("Persisted Request Attachment Invoice: {}", requestCreationAttachments);
    
      List<String> codiciStanziamenti = raggruppamentoArticoliDTO.getCodiciStanziamenti();
      log.info("Manage CodiciStanziamenti [{}] per raggruppamento fattura: {}", codiciStanziamenti, codiceRaggrArticoli);

      attachmentInvoiceProducer.generateAttachmentInvoiceAndSendToQueue(raggruppamentoArticoliDTO, tipoDettaglioStanziamento,
                                                                        requestCreationAttachments, template);

    } else {
      log.warn("Skip generation attachment invoice, because configuration table to raggruppamento articoli {} is set to false",
               allegatiConfig);
    }
  }
  @Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true, propagation = Propagation.REQUIRES_NEW)
  public List<RequestsCreationAttachments> findByCodiceFatturaNewTransaction(String invoiceCode) {
    log.debug("Search request by codice fattura: {}", invoiceCode);
    return requestCreationAttachmentRepo.findByCodiceFattura(invoiceCode);
  }
  
  public List<RequestsCreationAttachments> findByCodiceFattura(String invoiceCode) {
    log.debug("Search request by codice fattura: {}", invoiceCode);
    return requestCreationAttachmentRepo.findByCodiceFattura(invoiceCode);
  }

  public void sendMessage(RequestsCreationAttachments requestsCreationAttachments) {
    attachmentInvoiceProducer.sendMessageGenerateRequest(requestsCreationAttachments);

  }

}
