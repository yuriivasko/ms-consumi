package it.fai.ms.consumi.service.petrolpump.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;

@Service
public class PriceTableConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final PriceTableIntervalConsumer openIntervalConsumer;
  private final PriceTableIntervalConsumer closeIntervalConsumer;

  public PriceTableConsumer(@Qualifier(PriceTableIntervalConsumer.OPEN_INTERVAL_PROCESSOR) final PriceTableIntervalConsumer openInterval,
                             @Qualifier(PriceTableIntervalConsumer.CLOSED_INTERVAL_PROCESSOR) final PriceTableIntervalConsumer closedInterval) {
    openIntervalConsumer = openInterval;
    closeIntervalConsumer = closedInterval;
  }

  public void consumeMessage(PriceTableDTO dto) throws Exception {
    log.debug("Consume message: {}", dto);
    if (dto.getValidUntil() == null) {
      log.info("Open interval...");
      openIntervalConsumer.managePriceTable(dto);
    } else {
      log.info("Closed interval...");
      closeIntervalConsumer.managePriceTable(dto);
    }
    log.info("Consumed message {}", dto.getClass());
  }

}
