package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class LocalizationProvider {
	DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss");
	Locale locale;
	ZoneId zoneId;
	
	public LocalizationProvider(Locale locale, ZoneId zoneId) {
		this.locale = locale;
		this.zoneId = zoneId!=null?zoneId:ZoneId.of("Europe/Rome");
	}
	public String formatDate(Instant dataIngresso) {
		return dataIngresso.atZone(zoneId).format(dateFormatter);

	}
	public String formatTime(Instant dataIngresso) {
		return dataIngresso.atZone(zoneId).format(timeFormatter);
		
	}
	
	public Locale getLocale() {
		return locale;
	}
}
