package it.fai.ms.consumi.client;

import java.time.Instant;
import java.util.List;

import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;


public interface PetrolPumpService{

  List<PriceTableDTO> getPriceTableList(String authorizationToken, String codiceFornitore, String codiceArticolo, Instant date);
  
  PointDTO getPoint(String code);
}
