package it.fai.ms.consumi.scheduler.jobs.flussi.stcon;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.stcon.StconRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class StconDescriptor implements RecordDescriptor<StconRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(StconDescriptor.class);

  public final static String HEADER_BEGIN      = "01";
  public final static String BEGIN_10          = "10";
  public final static String FOOTER_CODE_BEGIN = "90";

  public static FileDescriptor descriptor = new FileDescriptor();

  static {
    descriptor.skipItem("recordCode", 1, 2);
    descriptor.addItem("gateIdentifier", 3, 12);
    descriptor.addItem("motorways", 13, 32);
    descriptor.addItem("gateNumber", 33, 36);
    descriptor.addItem("laneCode", 37, 40);
    descriptor.addItem("description", 41, 140);
    descriptor.addItem("endDate", 141, 148);
    descriptor.addItem("mainTypeOfGate", 149, 149);
    descriptor.addItem("subTypeOfGate", 150, 150);
    descriptor.addItem("distance", 151, 160);
    descriptor.addItem("globalGateIdentifier", 161, 169);
    descriptor.skipItem("filler", 170, 500);
  }
  // static {
  // descriptor.skipItem("recordCode", 1, 2);
  // descriptor.addItem("gateIdentifier", 3, 12);
  // descriptor.addItem("motorways", 13, 16);
  // descriptor.addItem("gateNumber", 17, 20);
  // descriptor.addItem("laneCode", 21, 24);
  // descriptor.addItem("description", 25, 124);
  // descriptor.addItem("endDate", 125, 132);
  // descriptor.addItem("mainTypeOfGate", 133, 133);
  // descriptor.addItem("subTypeOfGate", 134, 134);
  // descriptor.addItem("distance", 135, 144);
  // descriptor.addItem("globalGateIdentifier", 145, 153);
  // }
  // Trailer
  static final FileDescriptor descriptor90          = new FileDescriptor();
  static final String         numberOfDetailRecords = "numberOfDetailRecords";
  static {
    descriptor90.addItem("recordCode", 1, 2);
    descriptor90.addItem("transmitterCode", 3, 10);
    descriptor90.addItem("receiver", 11, 18);
    descriptor90.addItem("fileCreationDate", 19, 26);
    descriptor90.addItem("fileCreationTime", 27, 32);
    descriptor90.addItem("fileNumber", 33, 36);
    descriptor90.addItem("dataType", 37, 46);
    descriptor90.addItem(numberOfDetailRecords, 47, 54);
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    Optional<StringItem> stringItem = StconDescriptor.descriptor90.getItem(StconDescriptor.numberOfDetailRecords);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in StconDescriptor!");
    }
    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return StconDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return StconDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 2;
  }

  @Override
  public StconRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String _fileName, long _rowNumber) {
    var entity = new StconRecord(_fileName, _rowNumber);
    switch (_recordCode) {

    case HEADER_BEGIN:
      break;

    case BEGIN_10:
      entity.setFromString(StconDescriptor.descriptor.getItems(), _data);
      break;

    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(StconDescriptor.class, StconRecord.class).checkConfiguration();
  }

}
