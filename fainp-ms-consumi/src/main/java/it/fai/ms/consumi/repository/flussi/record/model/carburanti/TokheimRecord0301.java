package it.fai.ms.consumi.repository.flussi.record.model.carburanti;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TokheimRecord0301 extends TokheimRecord {

  private static final long serialVersionUID = -4011067042779616082L;

  String recordCode;
//  String filler1;
  //AAAAMMGG
  String dataErogazione;
  //HHMMSS
  String oraErogazione;
  String nrTransazione1;
  String nrTrackycard;
  String segnoImponibile;
//  String filler2;
  String tipoTransazione;
  String nrTransazione2;
//  String filler3;
  String valuta;
//  String filler4;

  @JsonCreator
  public TokheimRecord0301(@JsonProperty("fileName") String fileName, @JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }

  @Override
  public boolean toBeSkipped() {
    return true;
  }

  @Override
  public int getNestedLevel() {
    return 1;
  }
  @Override
  public TokheimRecord0201 getParentRecord() {
    return (TokheimRecord0201) super.getParentRecord();
  }
  @Override
  public String getRecordCode() {
    return recordCode;
  }
  @Override
  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }
  public String getDataErogazione() {
    return dataErogazione;
  }
  public void setDataErogazione(String dataErogazione) {
    this.dataErogazione = dataErogazione;
  }
  public String getOraErogazione() {
    return oraErogazione;
  }
  public void setOraErogazione(String oraErogazione) {
    this.oraErogazione = oraErogazione;
  }
  public String getNrTransazione1() {
    return nrTransazione1;
  }
  public void setNrTransazione1(String nrTransazione1) {
    this.nrTransazione1 = nrTransazione1;
  }
  public String getNrTrackycard() {
    return nrTrackycard;
  }
  public void setNrTrackycard(String nrTrackycard) {
    this.nrTrackycard = nrTrackycard;
  }
  public String getSegnoImponibile() {
    return segnoImponibile;
  }
  public void setSegnoImponibile(String segnoImponibile) {
    this.segnoImponibile = segnoImponibile;
  }
  public String getTipoTransazione() {
    return tipoTransazione;
  }
  public void setTipoTransazione(String tipoTransazione) {
    this.tipoTransazione = tipoTransazione;
  }
  public String getNrTransazione2() {
    return nrTransazione2;
  }
  public void setNrTransazione2(String nrTransazione2) {
    this.nrTransazione2 = nrTransazione2;
  }
  public String getValuta() {
    return valuta;
  }
  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

}
