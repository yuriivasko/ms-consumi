package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.efservice.message.consumi.AllineamentoDaConsumoMessage;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TollCollectRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.jms.producer.AllineamentoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service(TollCollectJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class TollCollectJob
  extends AbstractJob<TollCollectRecord, TollCollect> {

  public static final String QUALIFIER = "TollCollectJob";
  private final AllineamentoDaConsumoJmsProducer allineamentiDaConsumoProducer;

  public TollCollectJob(PlatformTransactionManager transactionManager,
                        StanziamentiParamsValidator stanziamentiParamsValidator,
                        NotificationService notificationService,
                        RecordPersistenceService persistenceService,
                        RecordDescriptor<TollCollectRecord> recordDescriptor,
                        ConsumoProcessor<TollCollect> consumoProcessor,
                        RecordConsumoMapper<TollCollectRecord, TollCollect> recordConsumoMapper,
                        StanziamentiToNavPublisher stanziamentiToNavJmsProducer,
                        AllineamentoDaConsumoJmsProducer allineamentiDaConsumoProducer) {

    super("TOLL_COLLECT", transactionManager, stanziamentiParamsValidator, notificationService,
      persistenceService, recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
    this.allineamentiDaConsumoProducer = allineamentiDaConsumoProducer;
  }




  @Override
  public Format getJobQualifier() {
    return Format.TOLL_COLLECT;
  }

  @Override
  protected RecordsFileInfo validateFile(String _filename, long _startTime, Instant _ingestionTime) {
    //no generic flat file validation: is CSV!
    return new RecordsFileInfo(_filename, _startTime, _ingestionTime);
  }


//  @Override
//  protected void postFileElaboration(String filename,
//                                     Instant ingestionTime,
//                                     ServicePartner servicePartner,
//                                     Map<String, Stanziamento> stanziamentiDaSpedireANAV,
//                                     ArrayList<String> errorList,
//                                     RecordsFileInfo recordsFileInfo) throws IOException {
//
//    List<AllineamentoDaConsumoMessage> allineamentiDaConsumo = stanziamentiDaSpedireANAV.values().stream()
//      .flatMap(s->s.getDettaglioStanziamenti().stream())
//      .filter(ds->ds instanceof DettaglioStanziamentoPedaggio)
//      .map(ds->{
//        DettaglioStanziamentoPedaggio dsPedaggio = (DettaglioStanziamentoPedaggio)ds;
//        return new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder()
//          .deviceType(TipoDispositivoEnum.TOLL_COLLECT)
//          .licensePlateNumber(dsPedaggio.getVehicle().getLicensePlate().getLicenseId())
//          .licensePlateNation(dsPedaggio.getVehicle().getLicensePlate().getCountryId())
//          .build();
//      })
//    .distinct()
//    .collect(Collectors.toMinimalInformationList());
//    for (AllineamentoDaConsumoMessage allineamentoDaConsumo: allineamentiDaConsumo){
//      allineamentiDaConsumoProducer.sendMessage(allineamentoDaConsumo);
//    }
//    super.postFileElaboration(filename, ingestionTime, servicePartner, stanziamentiDaSpedireANAV, errorList, recordsFileInfo);
//  }

  @Override
  protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
    super.afterStanziamentiSent(sourceType, stanziamentiDaSpedireANAV);
    List<AllineamentoDaConsumoMessage> allineamentiDaConsumo = stanziamentiDaSpedireANAV.values().stream()
      .flatMap(s->s.getDettaglioStanziamenti().stream())
      .filter(ds->ds instanceof DettaglioStanziamentoPedaggio)
      .map(ds->{
        DettaglioStanziamentoPedaggio dsPedaggio = (DettaglioStanziamentoPedaggio)ds;
        return new AllineamentoDaConsumoMessage.AllineamentoDaConsumoBuilder()
          .deviceType(TipoDispositivoEnum.TOLL_COLLECT)
          .licensePlateNumber(dsPedaggio.getVehicle().getLicensePlate().getLicenseId())
          .licensePlateNation(dsPedaggio.getVehicle().getLicensePlate().getCountryId())
          .build();
      })
      .distinct()
      .collect(Collectors.toList());
    for (AllineamentoDaConsumoMessage allineamentoDaConsumo: allineamentiDaConsumo){
      allineamentiDaConsumoProducer.sendMessage(allineamentoDaConsumo);
    }
  }
}
