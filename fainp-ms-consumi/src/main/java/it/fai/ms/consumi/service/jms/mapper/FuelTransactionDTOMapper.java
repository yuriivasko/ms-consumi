package it.fai.ms.consumi.service.jms.mapper;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.consumi.fuel.DresserWSTransactionRecord;
import it.fai.ms.common.jms.dto.consumi.fuel.FuelTransactionRecord;
import it.fai.ms.common.jms.dto.consumi.fuel.Q8WSTransactionRecord;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;

@Service
public class FuelTransactionDTOMapper {

  private        Logger            log                    = LoggerFactory.getLogger(getClass());
  private DateTimeFormatter Q8_DATETIME_FORMATTER  = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSSXXX");
  private DateTimeFormatter DRESSER_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
  private DateTimeFormatter DRESSER_TIME_FORMATTER = DateTimeFormatter.ofPattern("HHmmss");
  private DateTimeFormatter TRACKYCARD_RECORD_STD_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
  private DateTimeFormatter TRACKYCARD_RECORD_STD_TIME_FORMATTER = DateTimeFormatter.ofPattern("HHmmss");

  public TrackyCardCarburantiStandardRecord toTrackyCardCarburantiStandardRecord(Instant ingestionTime,
                                                                                 StanziamentiParams stanziamentiParams,
                                                                                 ServicePartner servicePartner,
                                                                                 String fileName,
                                                                                 FuelTransactionRecord fuelTransactionRecord) {

    log.debug("Mapping ingestionTime:=[{}],\n StanziamentiParams:=[{}],\n ServicePartner:=[{}],\n fileName:=[{}],\n FuelTransactionRecord:=[{}]",
              ingestionTime, stanziamentiParams, servicePartner, fileName, fuelTransactionRecord);

    TrackyCardCarburantiStandardRecord tcsRecord = new TrackyCardCarburantiStandardRecord(fileName, -1);

    tcsRecord.setFileName(fileName);
    tcsRecord.setPartnerCode(servicePartner.getPartnerCode());
    tcsRecord.setRecordCode(stanziamentiParams.getRecordCode());
    tcsRecord.setCounter(""); //OK
    tcsRecord.setIngestion_time(ingestionTime);
    tcsRecord.setProductCode(stanziamentiParams.getArticle().getCode());
    tcsRecord.setVatPercentage("" + stanziamentiParams.getArticle().getVatRate());

    if (fuelTransactionRecord instanceof Q8WSTransactionRecord) {
      mapToTrackyCardCarburantiStandardRecord(tcsRecord, (Q8WSTransactionRecord) fuelTransactionRecord);
    } else {
      mapToTrackyCardCarburantiStandardRecord(tcsRecord, (DresserWSTransactionRecord) fuelTransactionRecord);
    }

    log.debug("Returning TrackyCardCarburantiStandardRecord:=[{}]", tcsRecord);
    return tcsRecord;
  }

  public TrackyCardCarburantiStandardRecord mapToTrackyCardCarburantiStandardRecord(TrackyCardCarburantiStandardRecord tcsRecord,
                                                                                    DresserWSTransactionRecord wsRecord) {

    log.debug("wsRecord is DresserWSTransactionRecord! Mapping specific fields...");

    final String pointCode = wsRecord.getPointCod();
    final String pan = wsRecord.getFaipassNr();
    final String date = wsRecord.getTransData();
    final String time = wsRecord.getTransOra();
    final String pumpNumber = wsRecord.getTransErogatore();
    final String litres = wsRecord.getTransQta();
    final String kilometers = wsRecord.getTransKm();
    final String ticket =  wsRecord.getKey();

    final String currency = wsRecord.getTransValuta();
    final String unitPrice = wsRecord.getTransPrezzoUnitario();
    final String vatPercentage = wsRecord.getTransIvaPerc();

    tcsRecord.setPointCode(pointCode);

    mapTrackyCardPan(tcsRecord, pan);

    tcsRecord.setDate(fromDateToTcsDate(date));
    tcsRecord.setTime(fromTimeToTcsTime(time));
    tcsRecord.setCreationDateInFile(fromDateToCreationDateInFile(date));
    tcsRecord.setCreationTimeInFile(fromTimeToCreationTimeInFile(time));
    tcsRecord.setPumpNumber(pumpNumber);
    tcsRecord.setLitres(litres);
    tcsRecord.setUnitOfMeasure("LT");
    tcsRecord.setKilometers(kilometers);
    tcsRecord.setTicket(ticket);
    tcsRecord.setPriceReferenceVatIncluded(unitPrice,currency);

    //overriding default vat
    if (StringUtils.isNotBlank(vatPercentage)) {
      tcsRecord.setVatPercentage(vatPercentage);
    }
    tcsRecord.setCurrency(currency);

    return tcsRecord;
  }

  public TrackyCardCarburantiStandardRecord mapToTrackyCardCarburantiStandardRecord(TrackyCardCarburantiStandardRecord tcsRecord,
                                                                                    Q8WSTransactionRecord wsRecord) {

    log.debug("wsRecord is Q8WSTransactionRecord! Mapping specific fields...");

    final String pointCode = wsRecord.getCodPVFaiService();
    final String pan = wsRecord.getCardNumber();
    final String dateTime = wsRecord.getData();

    final String pumpNumber = StringUtils.substringAfter(wsRecord.getMessage(), "|");
    final String litres = wsRecord.getQuantity();
    final String um = wsRecord.getUm();
    final String kilometers = StringUtils.substringBefore(wsRecord.getMessage(), "|");
    final String ticket =  wsRecord.getLocal_TransactionKey_Partner();
//    final String productCode = wsRecord.getArticleCod_Partner();
    final String currency = wsRecord.getCurrency();
    final String unitPrice = wsRecord.getUnitAmount_VATincluded();
    final String vatPercentage = wsRecord.getIvaTaxRate();

    tcsRecord.setPointCode(pointCode);

    mapTrackyCardPan(tcsRecord, pan);

    tcsRecord.setDate(fromDateTimeToTcsDate(dateTime));
    tcsRecord.setTime(fromDateTimeToTcsTime(dateTime));
    tcsRecord.setCreationDateInFile(fromDateTimeToCreationDateInFile(dateTime));
    tcsRecord.setCreationTimeInFile(fromDateTimeToCreationTimeInFile(dateTime));
    tcsRecord.setPumpNumber(pumpNumber);
    tcsRecord.setLitres(litres);
    tcsRecord.setUnitOfMeasure(um);
    tcsRecord.setKilometers(kilometers);
    tcsRecord.setTicket(ticket);
    tcsRecord.setPriceReferenceVatIncluded(unitPrice, currency);
    
    //overriding default vat
    if (StringUtils.isNotBlank(vatPercentage)) {
      tcsRecord.setVatPercentage(vatPercentage);
    }
    tcsRecord.setCurrency(currency);

    return tcsRecord;
  }

  public void mapTrackyCardPan(TrackyCardCarburantiStandardRecord tcsRecord, String tackyCardSerialNumber) {
    tcsRecord.setPanField1IsoCode(StringUtils.left(tackyCardSerialNumber, 6));
    tcsRecord.setPanField2GroupCode(StringUtils.mid(tackyCardSerialNumber, 6, 2));
    tcsRecord.setPanField3CustomerCode(StringUtils.mid(tackyCardSerialNumber, 8, 6));
    tcsRecord.setPanField4DeviceNumber(StringUtils.mid(tackyCardSerialNumber, 14, 4));
    tcsRecord.setPanField5ControlDigit(StringUtils.right(tackyCardSerialNumber, 1));
  }

  public String fromDateToTcsDate(String date) {
    TemporalAccessor tc = DRESSER_DATE_FORMATTER.parse(date);
    return TRACKYCARD_RECORD_STD_DATE_FORMATTER.format(tc);
  }

  public String fromTimeToTcsTime(String time) {
    TemporalAccessor tc = DRESSER_TIME_FORMATTER.parse(time);
    return TRACKYCARD_RECORD_STD_TIME_FORMATTER.format(tc);
  }

  public String fromDateToCreationDateInFile(String date) {
    TemporalAccessor tc = DRESSER_DATE_FORMATTER.parse(date);
    return TRACKYCARD_RECORD_STD_DATE_FORMATTER.format(tc);
  }

  public String fromTimeToCreationTimeInFile(String time) {
    TemporalAccessor tc = DRESSER_TIME_FORMATTER.parse(time);
    return TRACKYCARD_RECORD_STD_TIME_FORMATTER.format(tc);
  }

  public String fromDateTimeToTcsDate(String dateTime) {
    TemporalAccessor tc = Q8_DATETIME_FORMATTER.parse(dateTime);
    return TRACKYCARD_RECORD_STD_DATE_FORMATTER.format(tc);
  }

  public String fromDateTimeToTcsTime(String dateTime) {
    TemporalAccessor tc = Q8_DATETIME_FORMATTER.parse(dateTime);
    return TRACKYCARD_RECORD_STD_TIME_FORMATTER.format(tc);
  }

  public String fromDateTimeToCreationDateInFile(String dateTime) {
    TemporalAccessor tc = Q8_DATETIME_FORMATTER.parse(dateTime);
    return TRACKYCARD_RECORD_STD_DATE_FORMATTER.format(tc);
  }

  public String fromDateTimeToCreationTimeInFile(String dateTime) {
    TemporalAccessor tc = Q8_DATETIME_FORMATTER.parse(dateTime);
    return TRACKYCARD_RECORD_STD_TIME_FORMATTER.format(tc);
  }
}
