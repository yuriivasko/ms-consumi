package it.fai.ms.consumi.repository.fatturazione;

import java.util.List;

import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;

public interface RequestsCreationAttachmentsRepository {

  RequestsCreationAttachments find(Long id);

  RequestsCreationAttachments save(RequestsCreationAttachments entity);

  void delete(Long id);

  void delete(RequestsCreationAttachments entity);

  RequestsCreationAttachments findByCodiceFatturaAndCodiceRaggruppamentoArticoli(String numeroFattura, String codiceRaggruppamentoArticoli);

  List<RequestsCreationAttachments> findByCodiceFattura(String numeroFattura);

}
