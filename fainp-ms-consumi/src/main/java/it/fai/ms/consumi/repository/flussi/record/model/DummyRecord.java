package it.fai.ms.consumi.repository.flussi.record.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DummyRecord
  extends CommonRecord {

  private final static long serialVersionUID = -4484494377444592662L;

  private transient final Logger log = LoggerFactory.getLogger(getClass());

  @JsonCreator
  public DummyRecord() {
    super("dummy record, for Consumi without file", -1);
  }

  public String toMessage() {
    return "";
  }



}
