package it.fai.ms.consumi.service.petrolpump.consumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.scheduler.jobs.StanziamentoDTOMapper;
import it.fai.ms.consumi.service.dettaglio_stranziamento.DettaglioStanziamentoCarburanteService;
import it.fai.ms.consumi.service.dto.PriceTableFilterDTO;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;

@Service
@Transactional
@Qualifier(PriceTableClosedIntervalConsumerImpl.CLOSED_INTERVAL_PROCESSOR)
public class PriceTableClosedIntervalConsumerImpl implements PriceTableIntervalConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private List<String>                                 errors = null;
  protected NotificationService                        notificationService;
  private final DettaglioStanziamentoCarburanteService dsCarburanteService;
  private final StanziamentiToNavPublisher           stanziamentiNavJmsProducer;
  private final StanziamentoEntityMapper               stanziamentoMapper;

  public PriceTableClosedIntervalConsumerImpl(final DettaglioStanziamentoCarburanteService _dsCarburanteService,
                                               NotificationService _notificationService,
                                               final StanziamentiToNavPublisher _stanziamentiNavJmsProducer,
                                               final StanziamentoEntityMapper _stanziamentoMapper) {
    dsCarburanteService = _dsCarburanteService;
    notificationService = _notificationService;
    stanziamentiNavJmsProducer = _stanziamentiNavJmsProducer;
    stanziamentoMapper = _stanziamentoMapper;
    errors = new ArrayList<>();
  }

  @Override
  public void managePriceTable(PriceTableDTO priceTable) {
    log.debug("Manage priceTable Closed Interval....");

    PriceTableFilterDTO priceTableFilterDTO = new PriceTableFilterDTO(priceTable);
    List<DettaglioStanziamentoCarburanteEntity> dettagliStanziamentiEntity = new ArrayList<>();
    dettagliStanziamentiEntity = dsCarburanteService.findToUpdatePriceTableClosed(priceTableFilterDTO);

    Set<DettaglioStanziamentoCarburanteEntity> newList = new HashSet<>();

    for (DettaglioStanziamentoCarburanteEntity ds : dettagliStanziamentiEntity) {
      Optional<StanziamentoEntity> optStanziamento = getStanziamentoProvvisorio(ds);
      if (!existsStanziamento(optStanziamento)) {
        addAnomalyNotFoundStanziamento(ds);
      } else {
        manageDettaglioStanziamentoCloseIntervalPrice(ds, optStanziamento);
        newList.add(ds);
      }
    }

    log.info("Updated {} DettagliStanziamentoCarburante of {}", newList.size(), dettagliStanziamentiEntity.size());
    if (!errors.isEmpty()) {
      String errorMsg = String.format("Anomaly on manage changed price on Dettagli Stanziamento: %s", errors);
      log.error("ERROR: {}", errorMsg);
      sendAnomaly();
    }
  }

  private void sendAnomaly() {
    Map<String, Object> map = new HashMap<>();
    map.put("error_message", errors);
    notificationService.notify("VARPRICE-001", map);
  }

  private void manageDettaglioStanziamentoCloseIntervalPrice(DettaglioStanziamentoCarburanteEntity ds,
                                                             Optional<StanziamentoEntity> optStanziamento) {
    try {
      StanziamentoEntity stanziamento = optStanziamento.get();
      StanziamentoEntity stanziamentoCloned = dsCarburanteService.cloneStanziamentoCarburanteFromProvvisorio(stanziamento, InvoiceType.D);
      ds.setTipoConsumo(InvoiceType.D);
      setDettagliToStanziamento(ds, stanziamentoCloned);
      ds = dsCarburanteService.update(ds);
      addStanziamentoOnDettaglio(ds, stanziamentoCloned);
      dsCarburanteService.updateStanziamento(stanziamentoCloned);
      log.info("Create new stanziamento definitivo: {} from Stanziamento provvisorio: {}", stanziamentoCloned, stanziamento);

      sendStanziamentoToNav(stanziamentoCloned);
    } catch (Exception e) {
      String anomaly = String.format("Exception to manage change price on DettaglioStanziamento %s : %s", ds.getId(), e);
      errors.add(anomaly);
    }

  }

  private void sendStanziamentoToNav(StanziamentoEntity stanziamento) {
    log.info("Send Stanziamento {} to NAV...", (stanziamento != null) ? stanziamento.getCode() : null);
    if (stanziamento != null) {
      StanziamentoDTOMapper stanziamentoDTOMapper = new StanziamentoDTOMapper();
      List<StanziamentoMessage> stanziamentiMessage = stanziamentoDTOMapper.mapToStanziamentoDTO(Arrays.asList(stanziamentoMapper.toDomain(stanziamento)));
      stanziamentiNavJmsProducer.publish(stanziamentiMessage);
    } else {
      String anomaly = "Stanziamento to send at NAV is NULL";
      log.warn("ANOMALY - {}", anomaly);
      errors.add(anomaly);
    }
  }

  private void addStanziamentoOnDettaglio(DettaglioStanziamentoCarburanteEntity dettaglio, StanziamentoEntity stanziamento) {
    log.debug("Add relation between dettaglio: {} and stanziamento {}", dettaglio, stanziamento);
    if (dettaglio != null) {
      dettaglio.getStanziamenti()
               .add(stanziamento);
    }
  }

  private void setDettagliToStanziamento(DettaglioStanziamentoCarburanteEntity dettaglio, StanziamentoEntity stanziamento) {
    log.debug("Add relation between stanziamento: {} and dettaglio {}", stanziamento, dettaglio);
    if (stanziamento != null) {
      stanziamento.getDettaglioStanziamenti()
                  .add(dettaglio);
    }
  }

  private void addAnomalyNotFoundStanziamento(DettaglioStanziamentoCarburanteEntity ds) {
    String anomaly = String.format("Not found Stanziamento Provvisorio related on Dettaglio: %s", (ds != null) ? ds.getId() : null);
    log.warn("ANOMALY - {}", anomaly);
    errors.add(anomaly);

  }

  private boolean existsStanziamento(Optional<StanziamentoEntity> optStanziamento) {
    if (optStanziamento.isPresent())
      return true;
    return false;
  }

  private Optional<StanziamentoEntity> getStanziamentoProvvisorio(DettaglioStanziamentoCarburanteEntity ds) {
    Set<StanziamentoEntity> stanziamenti = ds.getStanziamenti();
    Optional<StanziamentoEntity> optStanziamento = stanziamenti.stream()
                                                               .filter(s -> s.getStatoStanziamento()
                                                                             .equals(InvoiceType.P))
                                                               .findFirst();
    return optStanziamento;
  }

}
