package it.fai.ms.consumi.repository.storico_dml.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.AbstractJmsDmlListener;
import it.fai.ms.common.dml.efservice.dto.RichiestaDMLDTO;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.consumi.repository.storico_dml.StoricoRichiestaRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoRichiesta;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsDmlStoricoRichiestaListener extends AbstractJmsDmlListener<RichiestaDMLDTO, StoricoRichiesta>   implements JmsTopicListener{
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  public JmsDmlStoricoRichiestaListener(StoricoRichiestaRepository repository,
                                      ApplicationEventPublisher publisher) throws Exception {
    super(RichiestaDMLDTO.class, repository,publisher,false);
  }

  @Override
  public StoricoRichiesta getEntityToPersist(final RichiestaDMLDTO dto, StoricoRichiesta _rf) {
    final StoricoRichiesta rf = _rf!=null ? _rf : new StoricoRichiesta();
    log.debug("Received entity to persist "+dto+" while richiesta is "+_rf);
    rf.setAnomalia(dto.getAnomalia());
    rf.setAssociazione(dto.getAssociazione());
    rf.setDataUltimaVariazione(dto.getDataModificaStato());
    rf.setDispositivoDmlUniqueIdentifier(dto.getDispositivoDmlUniqueIdentifier());
    rf.setDmlRevisionTimestamp(dto.getDmlRevisionTimestamp());
    rf.setRichiestaDmlUniqueIdentifier(dto.getDmlUniqueIdentifier());
    //rf.setNumeroContratto(dto.getNumeroContratto());
    //rf.setOperazioneDisponibili(dto.getOperazioneDisponibili());
    rf.setOrdineClienteDmlUniqueIdentifier(dto.getOrdineClienteDmlUniqueIdentifier());
    rf.setStatoRichiesta(dto.getStatoRichiesta());
    rf.setTipoDispositivo(dto.getTipoDispositivo());
    rf.setTipoRichiesta(dto.getTipoRichiesta());

    return rf;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.DML_RICHIESTE_SAVE;
  }
  
}
