package it.fai.ms.consumi.repository.storico_dml;

import java.time.Instant;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.common.dml.InterfaceDmlRepository;
import it.fai.ms.consumi.repository.storico_dml.model.DmlEmbeddedId;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAzienda;

@Repository
public interface StoricoAziendaRepository extends JpaRepository<StoricoAzienda, DmlEmbeddedId>,InterfaceDmlRepository<StoricoAzienda> {
  
  public Optional<StoricoAzienda> findFirstByDmlUniqueIdentifierAndDmlRevisionTimestampBeforeOrderByDmlRevisionTimestampDesc(String dmlUniqueIdentifier,Instant dataVariazione);
  
  default Optional<StoricoAzienda> findFirstByCodiceAziendaAndDataVariazioneBeforeOrderByDataVariazioneDesc(String codiceAzienda,Instant dataVariazione){
    return findFirstByDmlUniqueIdentifierAndDmlRevisionTimestampBeforeOrderByDmlRevisionTimestampDesc(codiceAzienda, dataVariazione);
  }


}
