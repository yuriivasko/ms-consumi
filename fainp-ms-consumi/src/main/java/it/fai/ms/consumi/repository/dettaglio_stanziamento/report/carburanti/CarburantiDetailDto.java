package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti;

import java.math.BigDecimal;
import java.time.Instant;

import it.fai.ms.common.utils.report.CsvHeaderTranslate;
import it.fai.ms.common.utils.report.NestedBeanSerializer;


public class CarburantiDetailDto {

  public static final class Quantita{
    public Quantita() {}
    public Quantita(BigDecimal quantita, String unitaDiMisura) {
      valore = quantita;
      unita = unitaDiMisura;
    }
    public String unita;
    public BigDecimal valore;
    @Override
    public String toString() {

      return valore.toString() + " " + unita;
    }
  }


  @CsvHeaderTranslate(skip = true)
  private String     id;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.codiceDispositivo")
  private String     codiceDispositivo;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.nazioneTarga")
  private String     nazioneTarga;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.targa")
  private String     targa;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.stazione")
  private String     erogatore;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.dataUtilizzo"
      ,splitDateTime=true
      ,timeMessageKey = "report.header.consumi.oraUtilizzo"
      ,formatDate = "dd/MM/yyyy")
  private Instant    dataUtilizzo;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.quantita", customSerializer = NestedBeanSerializer.class)
  private Quantita quantita;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.descrizioneArticolo")
  private String     descrizioneArticolo;
  @CsvHeaderTranslate(messageKey = "report.header.consumi.descrizionePuntoDiErogazione")
  private String puntoErogazione;



  public CarburantiDetailDto(String id, String codiceDispositivo, String nazioneTarga, String targa, String erogatore,
                             Instant dataUtilizzo,
                              BigDecimal quantita, String unitaDiMisura, String articoloDescrizione, String puntoErogazione) {
    super();
    this.setId(id);
    this.setCodiceDispositivo(codiceDispositivo);
    this.setNazioneTarga(nazioneTarga);
    this.setTarga(targa);
    this.setErogatore(erogatore);
    this.setDataUtilizzo(dataUtilizzo);
    this.setQuantita(new Quantita(quantita,unitaDiMisura));
    this.setDescrizioneArticolo(articoloDescrizione);
    this.setPuntoErogazione(puntoErogazione);
  }

  public CarburantiDetailDto() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCodiceDispositivo() {
    return codiceDispositivo;
  }

  public void setCodiceDispositivo(String codiceDispositivo) {
    this.codiceDispositivo = codiceDispositivo;
  }

  public String getNazioneTarga() {
    return nazioneTarga;
  }

  public void setNazioneTarga(String nazioneTarga) {
    this.nazioneTarga = nazioneTarga;
  }

  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public String getErogatore() {
    return erogatore;
  }

  public void setErogatore(String erogatore) {
    this.erogatore = erogatore;
  }

  public Instant getDataUtilizzo() {
    return dataUtilizzo;
  }

  public void setDataUtilizzo(Instant dataUtilizzo) {
    this.dataUtilizzo = dataUtilizzo;
  }

  public String getDescrizioneArticolo() {
    return descrizioneArticolo;
  }

  public void setDescrizioneArticolo(String descrizioneArticolo) {
    this.descrizioneArticolo = descrizioneArticolo;
  }

  public Quantita getQuantita() {
    return quantita;
  }

  public void setQuantita(Quantita quantita) {
    this.quantita = quantita;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

}
