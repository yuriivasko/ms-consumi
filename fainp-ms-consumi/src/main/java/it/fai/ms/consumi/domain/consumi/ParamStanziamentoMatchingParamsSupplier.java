package it.fai.ms.consumi.domain.consumi;

import java.math.BigDecimal;
import java.util.Optional;

public interface ParamStanziamentoMatchingParamsSupplier {

  String getRecordCodeToLookInParamStanz();

  String getTransactionDetailCode();

  String getSourceType();

  Optional<BigDecimal> getVatRate();

  ServicePartner getServicePartner();
  
  Optional<String> getSupplierVehicleFareClass();

}
