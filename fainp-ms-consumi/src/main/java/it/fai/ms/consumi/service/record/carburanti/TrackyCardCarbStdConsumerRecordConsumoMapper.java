package it.fai.ms.consumi.service.record.carburanti;

import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TrackyCardCarbStdConsumerRecordConsumoMapper
  extends AbstractRecordConsumoMapper {

  TrackyCardCarbStdRecordConsumoMapper trackyCardCarbStdRecordConsumoMapper;

  public TrackyCardCarbStdConsumerRecordConsumoMapper(TrackyCardCarbStdRecordConsumoMapper trackyCardCarbStdRecordConsumoMapper) {
    this.trackyCardCarbStdRecordConsumoMapper = trackyCardCarbStdRecordConsumoMapper;
  }

  public TrackyCardCarburantiStandard mapRecordToConsumo(StanziamentiParams _stanziamentiParams, ServicePartner _servicePartner, TrackyCardCarburantiStandardRecord commonRecord) throws Exception {
    //Riutilizzo il mapper standard modificando solo le proprietà necessarie ad identificare il consumo come proveniente da WS
    TrackyCardCarburantiStandard recordConsumoStd = trackyCardCarbStdRecordConsumoMapper.mapRecordToConsumo(commonRecord);

    Source source = new Source(recordConsumoStd.getSource().getIngestionTime(), recordConsumoStd.getSource().getAcquisitionDate(), _stanziamentiParams.getSource());

    TrackyCardCarburantiStandard recordConsumoStdWs = new TrackyCardCarburantiStandard(source,
                                                                                       commonRecord.getRecordCode(),
                                                                                       Optional.of(new TrackyCardGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);
    BeanUtils.copyProperties(recordConsumoStd, recordConsumoStdWs);
    recordConsumoStdWs.getTransaction().setDetailCode(_stanziamentiParams.getTransactionDetail());
    recordConsumoStdWs.setServicePartner(_servicePartner);

    return recordConsumoStdWs;
  }

}
