package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.csv.CSVFormat;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;

public class LiberTRecord extends CommonRecord  implements GlobalIdentifierBySupplier{

  private static final long serialVersionUID = 3736772687714475507L;
  private String nAbbonne;
  private String contract;
  private String nBadge;
  private String anneeFacturation;
  private String moisFacturation;
  private String dateHeureEntree;
  private String dateHeureSortie;
  private String codeAutorouteEntree;
  private String codeAutorouteSortie;
  private String nGareEntree;
  private String nomGareEntree;
  private String nGareSortie;
  private String nomGareSortie;
  private String nbKms;
  private String classeVehicule;
  private String montantTTC;
  private String tauxTVA;

  private String globalIdentifier;

  @JsonCreator
  public LiberTRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID()
        .toString();
  }

  public String getContract() {
    return contract;
  }

  public void setContract(String contract) {
    this.contract = contract;
  }

  public String getNBadge() {
    return nBadge;
  }

  public void setNBadge(String nBadge) {
    this.nBadge = nBadge;
  }

  public String getAnneeFacturation() {
    return anneeFacturation;
  }

  public void setAnneeFacturation(String anneeFacturation) {
    this.anneeFacturation = anneeFacturation;
  }

  public String getMoisFacturation() {
    return moisFacturation;
  }

  public void setMoisFacturation(String moisFacturation) {
    this.moisFacturation = moisFacturation;
  }

  public String getDateHeureEntree() {
    return dateHeureEntree;
  }

  public void setDateHeureEntree(String dateHeureEntree) {
    this.dateHeureEntree = dateHeureEntree;
  }

  public String getDateHeureSortie() {
    return dateHeureSortie;
  }

  public void setDateHeureSortie(String dateHeureSortie) {
    this.dateHeureSortie = dateHeureSortie;
  }

  public String getCodeAutorouteEntree() {
    return codeAutorouteEntree;
  }

  public void setCodeAutorouteEntree(String codeAutorouteEntree) {
    this.codeAutorouteEntree = codeAutorouteEntree;
  }

  public String getCodeAutorouteSortie() {
    return codeAutorouteSortie;
  }

  public void setCodeAutorouteSortie(String codeAutorouteSortie) {
    this.codeAutorouteSortie = codeAutorouteSortie;
  }

  public String getNGareEntree() {
    return nGareEntree;
  }

  public void setNGareEntree(String nGareEntree) {
    this.nGareEntree = nGareEntree;
  }

  public String getNomGareEntree() {
    return nomGareEntree;
  }

  public void setNomGareEntree(String nomGareEntree) {
    this.nomGareEntree = nomGareEntree;
  }

  public String getNGareSortie() {
    return nGareSortie;
  }

  public void setNGareSortie(String nGareSortie) {
    this.nGareSortie = nGareSortie;
  }

  public String getNomGareSortie() {
    return nomGareSortie;
  }

  public void setNomGareSortie(String nomGareSortie) {
    this.nomGareSortie = nomGareSortie;
  }

  public String getNbKms() {
    return nbKms;
  }

  public void setNbKms(String nbKms) {
    this.nbKms = nbKms;
  }

  public String getClasseVehicule() {
    return classeVehicule;
  }

  public void setClasseVehicule(String classeVehicule) {
    this.classeVehicule = classeVehicule;
  }

  public String getMontantTTC() {
    return montantTTC;
  }

  public void setMontantTTC(String montantTTC) {
    this.montantTTC = montantTTC;
  }

  public String getTauxTVA() {
    return tauxTVA;
  }

  public void setTauxTVA(String tauxTVA) {
    this.tauxTVA = tauxTVA;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }


  @Override
  public CSVFormat getCsvFormat() {
    return super.getCsvFormat().withDelimiter(';');
  }

  public String getNAbbonne() {
    return nAbbonne;
  }

  public void setNAbbonne(String nAbbonne) {
    this.nAbbonne = nAbbonne;
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }
}
