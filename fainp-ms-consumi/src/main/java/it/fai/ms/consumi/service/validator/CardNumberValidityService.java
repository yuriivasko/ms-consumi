package it.fai.ms.consumi.service.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.utils.LuhnUtils;

@Service
public class CardNumberValidityService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private LuhnUtils luhnUtils;

  public CardNumberValidityService() {
    this.luhnUtils = new LuhnUtils();
  }

  public boolean hasValidCheckingDigit(String cardNumber) {
    log.debug("Check validity of [{}]", cardNumber);
    final boolean hasValidCheckingDigit = luhnUtils.hasValidCheckingDigit(cardNumber);
    log.debug("card number [{}]!", hasValidCheckingDigit ? "is valid" : "is NOT valid");
    return hasValidCheckingDigit;
  }
}
