package it.fai.ms.consumi.service.processor.stanziamenti.pedaggi;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoLogicKey;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoLogicKeyGenerator;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoProcessorAbstract;
import it.fai.ms.consumi.service.record.telepass.TelepassSourceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;

@Service
@Transactional
public class StanziamentoProcessorPedaggio extends StanziamentoProcessorAbstract {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  @Inject
  public StanziamentoProcessorPedaggio(final StanziamentoRepository _stanziamentiRepository, final StanziamentoPedaggioMapper _mapper) {
    super(_stanziamentiRepository, _mapper);
  }

  @Override
  public List<Stanziamento> processClonedDStanziamentoForCase2aWithDiffAmount(@NotNull final DettaglioStanziamento _allocationDetailDCloned,
                                                                              @NotNull final StanziamentiParams _stanziamentiParams) {

    throwsExceptionIfCustomerIsNotValid(_allocationDetailDCloned);
    if (_allocationDetailDCloned.getStanziamenti().isEmpty()) {
      _allocationDetailDCloned.setInvoiceType(InvoiceType.P); //looking like previous
      final var optionalAllocation = findAllocationSentOrNotToNavOnRepository(new StanziamentoLogicKeyGenerator(_stanziamentiParams, _allocationDetailDCloned).generate());
      _allocationDetailDCloned.setInvoiceType(InvoiceType.D); //resetting to original
      if (!optionalAllocation.isPresent()) {
        throw new IllegalStateException("Cannot find any (previous) Allocation by cloned AllocationDetail P for case 2b-clonedD with different amount!");
      }
      _allocationDetailDCloned.getStanziamenti().add(optionalAllocation.get());
    }
    

    final List<Stanziamento> allocationsToReturn = new LinkedList<>();
    final var allocationDetail = (DettaglioStanziamentoPedaggio) _allocationDetailDCloned;

    switch (allocationDetail.getInvoiceType()) {

    case P:
      throw new IllegalStateException("AllocationDetail P not valid for case 2b-clonedD with different amount");
    case D:

      // retrieve code
      final var previousAllocationCode = extractPreviousAllocationCode(allocationDetail);

      // add allocation P
      final var createdAllocationForCaseD = allocationHasToBeCreated(allocationDetail, _stanziamentiParams);
      createdAllocationForCaseD.setInvoiceType(InvoiceType.P);
      createdAllocationForCaseD.setCodiceStanziamentoProvvisorio(previousAllocationCode);
      createdAllocationForCaseD.setConguaglio(true);
      allocationsToReturn.add(createdAllocationForCaseD);
      _log.debug("[input allocation detail is D] Stanziamento P for case 2b-clonedD created : {}", createdAllocationForCaseD);
      break;
    default:
      break;
    }

    return allocationsToReturn;
  }

  @Override
  public List<Stanziamento> processCreatedStanziamentoForCase2aWithDiffAmount(@NotNull final DettaglioStanziamento _allocationDetailCreated,
                                                                              @NotNull final StanziamentiParams _stanziamentiParams) {

    throwsExceptionIfCustomerIsNotValid(_allocationDetailCreated);

    final List<Stanziamento> allocationsToReturn = new LinkedList<>();
    final var allocationDetail = (DettaglioStanziamentoPedaggio) _allocationDetailCreated;

    switch (allocationDetail.getInvoiceType()) {

    case P:
      throw new IllegalStateException("AllocationDetail P not valid for case 2b-created with different amount");
    case D:
      Stanziamento otherAllocationForCaseD = null;

      // add allocation P if Source is Telepass
      if (TelepassSourceName.isTelepassSource(_stanziamentiParams.getSource())) {

          otherAllocationForCaseD = allocationHasToBeCreated(allocationDetail, _stanziamentiParams);
          otherAllocationForCaseD.setConguaglio(true);
          otherAllocationForCaseD.setInvoiceType(InvoiceType.P);
          allocationsToReturn.add(otherAllocationForCaseD);
          _log.debug("[input allocation detail is D] Stanziamento P for case 2b-created created : {}", otherAllocationForCaseD);

      }
      StanziamentoLogicKey logicKey = new StanziamentoLogicKeyGenerator(
        _stanziamentiParams.getArticle().getCountry(),
        _stanziamentiParams.getArticle().getCode(),
        _stanziamentiParams.getSupplier().getCode(),
        allocationDetail.getCustomer().getId(),
        InvoiceType.D,
        LocalDate.ofInstant(allocationDetail.getDate(), ZoneId.systemDefault()),
        allocationDetail.getVehicle().getLicensePlate().getLicenseId()).generate();
      final var optionalAllocation = findAllocationNotSentToNavOnRepository(logicKey);
      if(optionalAllocation.isPresent()){
        _log.debug("Stanziamento D already exists for logic key {} it will be updated",logicKey);
        final var updatedAllocation = allocationHasToBeUpdated(allocationDetail,optionalAllocation.get());
        if(optionalAllocation.get().getCodiceStanziamentoProvvisorio()!=null){
          updatedAllocation.setCodiceStanziamentoProvvisorio(optionalAllocation.get().getCodiceStanziamentoProvvisorio());
        }
        allocationsToReturn.add(updatedAllocation);
        _log.debug("[input allocation detail is D] Stanziamento D for case 2b-created updated : {}", updatedAllocation);

      }else {
        // add allocation D
        final var createdAllocationForCaseD = allocationHasToBeCreated(allocationDetail, _stanziamentiParams);
        if (otherAllocationForCaseD != null) {
          createdAllocationForCaseD.setCodiceStanziamentoProvvisorio(otherAllocationForCaseD.getCode());
        }
        allocationsToReturn.add(createdAllocationForCaseD);
        _log.debug("[input allocation detail is D] Stanziamento D for case 2b-created created : {}", createdAllocationForCaseD);
      }
      break;
    default:
      break;
    }

    return allocationsToReturn;
  }

  @Override
  public List<Stanziamento> processStanziamento(@NotNull DettaglioStanziamento _allocationDetail,
                                                @NotNull StanziamentiParams _stanziamentiParams) {

    throwsExceptionIfCustomerIsNotValid(_allocationDetail);

    final List<Stanziamento> allocationsToReturn = new LinkedList<>();
    final var allocationDetail = (DettaglioStanziamentoPedaggio) _allocationDetail;

    StanziamentoLogicKey logicKey = new StanziamentoLogicKeyGenerator(
      _stanziamentiParams.getArticle().getCountry(),
      _stanziamentiParams.getArticle().getCode(),
      _stanziamentiParams.getSupplier().getCode(),
      _allocationDetail.getCustomer().getId(),
      _allocationDetail.getInvoiceType(),
      LocalDate.ofInstant(_allocationDetail.getDate(), ZoneId.systemDefault()),
      ((DettaglioStanziamentoPedaggio) _allocationDetail).getVehicle().getLicensePlate().getLicenseId())
      .generate();
    final var optionalAllocation = findAllocationNotSentToNavOnRepository(logicKey,_stanziamentiParams.getStanziamentiDetailsType());

    if (optionalAllocation.isPresent()) {

      switch (allocationDetail.getInvoiceType()) {

      case P:
        // update amount and counter for allocation P
        final var updatedAllocationForCaseP = allocationHasToBeUpdated(allocationDetail, optionalAllocation.get());
        allocationsToReturn.add(updatedAllocationForCaseP);
        _log.debug("[input allocation detail is P] Stanziamento P for case 1 updated : {}", updatedAllocationForCaseP);
        break;

      case D:
          var otherAllocationForCaseDOpt = findAllocationRelatedProvvisorio(optionalAllocation.get(),_stanziamentiParams.getStanziamentiDetailsType());
          if (otherAllocationForCaseDOpt.isPresent()) {
            var updatedAllocationForCaseD =  allocationHasToBeUpdated(allocationDetail, otherAllocationForCaseDOpt.get());
            allocationsToReturn.add(updatedAllocationForCaseD);
            _log.debug("[input allocation detail is D] Stanziamento D for cases 1 and 2b updated : {}", updatedAllocationForCaseD);
          }
          // update amount for allocation D
          final var updatedAllocationForCaseD = allocationHasToBeUpdated(allocationDetail, optionalAllocation.get());
          allocationsToReturn.add(updatedAllocationForCaseD);
          _log.debug("[input allocation detail is D] Stanziamento D for cases 1 and 2b updated : {}", updatedAllocationForCaseD);        break;
      default:
        break;
      }

    } else {

      switch (allocationDetail.getInvoiceType()) {

      case P:
        // add allocation P
        final var createdAllocationForCaseP = allocationHasToBeCreated(allocationDetail, _stanziamentiParams);
        allocationsToReturn.add(createdAllocationForCaseP);
        _log.debug("[input allocation detail is P] Stanziamento P for case 1 created : {}", createdAllocationForCaseP);
        break;

      case D:
        Stanziamento otherAllocationForCaseD = null;

        // add allocation P if Source is Telepass
        if (TelepassSourceName.isTelepassSource(_stanziamentiParams.getSource())) {
          otherAllocationForCaseD = allocationHasToBeCreated(allocationDetail, _stanziamentiParams);
          otherAllocationForCaseD.setInvoiceType(InvoiceType.P);
          otherAllocationForCaseD.setConguaglio(true);
          allocationsToReturn.add(otherAllocationForCaseD);
          _log.debug("[input allocation detail is D] Stanziamento P for cases 1 and 2b created : {}", otherAllocationForCaseD);
        }
        // add allocation D
        final var createdAllocationForCaseD = allocationHasToBeCreated(allocationDetail, _stanziamentiParams);
        if (otherAllocationForCaseD != null) {
          createdAllocationForCaseD.setCodiceStanziamentoProvvisorio(otherAllocationForCaseD.getCode());
        }
        allocationsToReturn.add(createdAllocationForCaseD);
        _log.debug("[input allocation detail is D] Stanziamento D for cases 1 and 2b created : {}", createdAllocationForCaseD);
        break;
      default:
        break;
      }

    }

    return allocationsToReturn;
  }

  @Override
  public List<Stanziamento> processStanziamentoForCase2aWithSameAmount(@NotNull final DettaglioStanziamento persistedAllocationDetailChangedToD,
                                                                       @NotNull final StanziamentiParams _stanziamentiParams) {
    throwsExceptionIfCustomerIsNotValid(persistedAllocationDetailChangedToD);
    
    final List<Stanziamento> allocationsToReturn = new LinkedList<>();
    final var allocationDetailPersistedAndChangedToD = (DettaglioStanziamentoPedaggio) persistedAllocationDetailChangedToD;

//    StanziamentoLogicKey generate = new StanziamentoLogicKeyGenerator(_stanziamentiParams, allocationDetailPersistedAndChangedToD).generate();
    StanziamentoLogicKey generate = new StanziamentoLogicKeyGenerator(_stanziamentiParams.getArticle().getCountry(),
      _stanziamentiParams.getArticle().getCode(),
      _stanziamentiParams.getSupplier().getCode(),
      allocationDetailPersistedAndChangedToD.getCustomer().getId(),
      allocationDetailPersistedAndChangedToD.getInvoiceType(),
      LocalDate.ofInstant(allocationDetailPersistedAndChangedToD.getDate(), ZoneId.systemDefault()),
      allocationDetailPersistedAndChangedToD.getVehicle().getLicensePlate().getLicenseId()).generate();
    final var allocationByStanziamentoLogicKey = findAllocationSentOrNotToNavOnRepository(generate);

    if (allocationByStanziamentoLogicKey.isPresent() && !allocationByStanziamentoLogicKey.get().isSentToNav()) {

      switch (allocationDetailPersistedAndChangedToD.getInvoiceType()) {

      case P:
        throw new IllegalStateException("AllocationDetail P not valid for case 2a with same amount");
      case D:

        // add allocation D
        final var updatedAllocationForCaseD = allocationHasToBeUpdated(allocationDetailPersistedAndChangedToD, allocationByStanziamentoLogicKey.get());

        allocationsToReturn.add(updatedAllocationForCaseD);
        _log.debug("[input allocation detail is D] Stanziamento D updated for case 2a with same amount : {}", updatedAllocationForCaseD);
        break;
      default:
        break;
      }

    } else {

      switch (allocationDetailPersistedAndChangedToD.getInvoiceType()) {

      case P:
        throw new IllegalStateException("AllocationDetail P not valid for case 2a with same amount");
      case D:

        // add allocation D
        final var createdAllocationForCaseD = allocationHasToBeCreated(allocationDetailPersistedAndChangedToD, _stanziamentiParams);
        generate = new StanziamentoLogicKeyGenerator(_stanziamentiParams.getArticle().getCountry(),
          _stanziamentiParams.getArticle().getCode(),
          _stanziamentiParams.getSupplier().getCode(),
          allocationDetailPersistedAndChangedToD.getCustomer().getId(),
          InvoiceType.P,
          LocalDate.ofInstant(allocationDetailPersistedAndChangedToD.getDate(), ZoneId.systemDefault()),
          allocationDetailPersistedAndChangedToD.getVehicle().getLicensePlate().getLicenseId()).generate();
        final var temporayAllocationByStanziamentoLogicKey = findAllocationSentOrNotToNavOnRepository(generate);
        if (temporayAllocationByStanziamentoLogicKey.isPresent()) {
          createdAllocationForCaseD.setCodiceStanziamentoProvvisorio(temporayAllocationByStanziamentoLogicKey.get().getCode());
        }
        
        allocationsToReturn.add(createdAllocationForCaseD);
        _log.debug("[input allocation detail is D] Stanziamento D created for case 2a with same amount : {}", createdAllocationForCaseD);
        break;

      default:
        break;
      }
    }

    return allocationsToReturn;
  }

  private String extractPreviousAllocationCode(final DettaglioStanziamento _allocationDetail) {
    var previousAllocationCode = "";
    if (_allocationDetail.getStanziamenti()
                         .size() == 1) {
      previousAllocationCode = _allocationDetail.getStanziamenti()
                                                .stream()
                                                .findFirst()
                                                .get()
                                                .getCode();
      _log.debug("Found temp allocation code : {}", previousAllocationCode);
    } else {
      if (_allocationDetail.getStanziamenti()
                           .isEmpty()) {
        _log.warn("I need the previous allocation code, but AllocationDetail hasn't Allocations");
      } else {
        _log.warn("I need the previous allocation code, but AllocationDetail has more than one Allocation : {}",
                  _allocationDetail.getStanziamenti()
                                   .size());
      }
    }
    return previousAllocationCode;
  }

  private void throwsExceptionIfCustomerIsNotValid(final DettaglioStanziamento _allocationDetail) {
    if (!_allocationDetail.hasCustomer()) {
      throw new IllegalStateException("DettaglioStanziamento must contain a cutomer");
    }
  }

  @Override
  protected Stanziamento allocationHasToBeCreated(final DettaglioStanziamento _allocationDetail, final StanziamentiParams _params) {
    final var allocation = getMapper().toStanziamento(_allocationDetail, _params);
    allocation.getDettaglioStanziamenti().add(_allocationDetail);
    if (_allocationDetail instanceof DettaglioStanziamentoPedaggio){
      allocation.addImponibileAdr(((DettaglioStanziamentoPedaggio)_allocationDetail).getImponibileAdr());
    }

    _log.debug("[allocation create] AllocationDetail {} added to new Allocation : {}", _allocationDetail, allocation);

    return allocation;
  }

  @Override
  protected Stanziamento allocationHasToBeUpdated(final DettaglioStanziamento _allocationDetail, final Stanziamento _allocation) {
    final var allocationDetail = (DettaglioStanziamentoPedaggio) _allocationDetail;
    final var allocation = _allocation;

    _log.debug("[allocation update] Allocation counter was : {}", allocation.getTotalAllocationDetails());
    allocation.incrementTotalAllocationDetails();
    _log.debug("[allocation update] Allocation counter is  : {}", allocation.getTotalAllocationDetails());

    _log.debug("[allocation update] Allocation amount to add : {}", allocationDetail.getAmount());
    _log.debug("[allocation update] Allocation costo  was    : {} - prezzo was : {}", allocation.getCosto(), allocation.getPrezzo());
    allocation.addAmountExcludedVat(allocationDetail.getAmount());
    _log.debug("[allocation update] Allocation costo  is     : {} - prezzo is  : {}", allocation.getCosto(), allocation.getPrezzo());

    allocation.addImponibileAdr(allocationDetail.getImponibileAdr());
    return allocation;
  }

}
