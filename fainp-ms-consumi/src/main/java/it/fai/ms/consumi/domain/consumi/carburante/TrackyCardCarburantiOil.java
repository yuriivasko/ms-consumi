package it.fai.ms.consumi.domain.consumi.carburante;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Optional;

public class TrackyCardCarburantiOil extends TrackyCardCarburantiStandard implements TrustedCustomerSupplier, TrustedVechicleSupplier {

  private Customer customer;

  public TrackyCardCarburantiOil(final Source _source, final String _recordCode,
                                 final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public InvoiceType getInvoiceType() {
    // Il valore verrà impostato a valle del processo, durante la scrittura del dettaglio stanziamento
    return null;
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

}
