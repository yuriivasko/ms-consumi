package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti;

import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiJollyRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

@Service(TrackyCardCarburantiJollyJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class TrackyCardCarburantiJollyJob
  extends AbstractJob<TrackyCardCarburantiJollyRecord, TrackyCardCarburantiJolly> {

  public final static String QUALIFIER = "TrackyCardCarburantiJollyJob";

  public TrackyCardCarburantiJollyJob(PlatformTransactionManager transactionManager,
                                      StanziamentiParamsValidator stanziamentiParamsValidator,
                                      NotificationService notificationService,
                                      RecordPersistenceService persistenceService,
                                      RecordDescriptor<TrackyCardCarburantiJollyRecord> recordDescriptor,
                                      ConsumoProcessor<TrackyCardCarburantiJolly> consumoProcessor,
                                      RecordConsumoMapper<TrackyCardCarburantiJollyRecord,
                                        TrackyCardCarburantiJolly> recordConsumoMapper,
                                      StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    super("TRACKYCARD_JOLLY", transactionManager, stanziamentiParamsValidator, notificationService,
          persistenceService, recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
  }

  @Override
  public Format getJobQualifier() {
    return Format.FAISERVICE_JOLLY;
  }

}
