package it.fai.ms.consumi.api.trx.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * TrxBasicReq
 */
@ApiModel(description = "Common Rquest Schema", subTypes = { TrxNoOilReq.class, TrxOilReq.class })
public class TrxBasicReq {
  @JsonProperty("requestCode")
  private String requestCode = null;

  @JsonProperty("productCode")
  private String productCode = null;

  @JsonProperty("articleCode")
  private String articleCode = null;

  @JsonProperty("cardNumber")
  private String cardNumber = null;

  @JsonProperty("virtualCard")
  private boolean virtualCard = false;

  @JsonProperty("buyerCode")
  private String buyerCode = null;

  @JsonProperty("outletCode")
  private String outletCode = null;

  @JsonProperty("vendorCode")
  private String vendorCode = null;

  @JsonProperty("quantity")
  private BigDecimal quantity = null;

  @JsonProperty("amount")
  private BigDecimal amount = null;

  @JsonProperty("currency")
  private String currency = null;

  @JsonProperty("vat")
  private BigDecimal vat = null;

  @JsonProperty("submissioDateTime")
  private OffsetDateTime submissioDateTime = null;

  @JsonProperty("plate")
  private String plate = null;

  @JsonProperty("intVRC")
  private String intVRC = null;

  @JsonProperty("km")
  private Integer km = null;

  @JsonProperty("driverId")
  private String driverId = null;

  public TrxBasicReq requestCode(String requestCode) {
    this.requestCode = requestCode;
    return this;
  }

  /**
   * Unique authorization request ID (per Vendor) used by the Vendor for its internal reference - e.g. Refueling
   * operation number.
   * 
   * @return requestCode
   **/
  @ApiModelProperty(
                    required = true,
                    value = "Unique authorization request ID (per Vendor) used by the Vendor for its internal reference - e.g. Refueling operation number.")
  @NotNull

  public String getRequestCode() {
    return requestCode;
  }

  public void setRequestCode(String requestCode) {
    this.requestCode = requestCode;
  }

  public TrxBasicReq productCode(String productCode) {
    this.productCode = productCode;
    return this;
  }

  /**
   * Code uniquely identifying the product being sold.
   * 
   * @return productCode
   **/
  @ApiModelProperty(required = true, value = "Code uniquely identifying the product being sold.")
  @NotNull

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public TrxBasicReq articleCode(String articleCode) {
    this.articleCode = articleCode;
    return this;
  }

  /**
   * Code uniquely identifying the product being sold.
   * 
   * @return articleCode
   **/
  @ApiModelProperty(required = true, value = "Code uniquely identifying the product being sold.")
  @NotNull

  public String getArticleCode() {
    return articleCode;
  }

  public void setArticleCode(String articleCode) {
    this.articleCode = articleCode;
  }

  public TrxBasicReq cardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
    return this;
  }

  /**
   * number of the TrackyCard used for the payment.
   * 
   * @return cardNumber
   **/
  @ApiModelProperty(required = true, value = "number of the TrackyCard used for the payment.")
  @NotNull

  public String getCardNumber() {
    return cardNumber;
  }

  public void setCardNumber(String cardNumber) {
    this.cardNumber = cardNumber;
  }

  public TrxBasicReq virtaulCard(boolean virtualCard) {
    this.virtualCard = virtualCard;
    return this;
  }

  /**
   * number of the TrackyCard used for the payment.
   * 
   * @return cardNumber
   **/
  @ApiModelProperty(required = true, value = "true if the card is virtual")
  @NotNull

  public boolean getVirtualCard() {
    return virtualCard;
  }

  public void setVirtualCard(boolean virtualCard) {
    this.virtualCard = virtualCard;
  }

  public TrxBasicReq buyerCode(String buyerCode) {
    this.buyerCode = buyerCode;
    return this;
  }

  /**
   * Code uniquely identifying the Buyer (as agreed with FAI Service).
   * 
   * @return buyerCode
   **/
  @ApiModelProperty(required = true, value = "Code uniquely identifying the Buyer (as agreed with FAI Service).")
  @NotNull

  public String getBuyerCode() {
    return buyerCode;
  }

  public void setBuyerCode(String buyerCode) {
    this.buyerCode = buyerCode;
  }

  public TrxBasicReq outletCode(String outletCode) {
    this.outletCode = outletCode;
    return this;
  }

  /**
   * Code uniquely identifying the Vendor's outlet (as agreed with FAI Service).
   * 
   * @return outletCode
   **/
  @ApiModelProperty(required = true, value = "Code uniquely identifying the Vendor's outlet (as agreed with FAI Service).")
  @NotNull

  public String getOutletCode() {
    return outletCode;
  }

  public void setOutletCode(String outletCode) {
    this.outletCode = outletCode;
  }

  public TrxBasicReq vendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
    return this;
  }

  /**
   * Unique code identifying the Vendor in the TrackyCard Transactions Authorization System (as provided by FAI
   * Service).
   * 
   * @return vendorCode
   **/
  @ApiModelProperty(
                    required = true,
                    value = "Unique code identifying the Vendor in the TrackyCard Transactions Authorization System (as provided by FAI Service).")
  @NotNull

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public TrxBasicReq quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * Quantity of the product being sold.
   * 
   * @return quantity
   **/
  @ApiModelProperty(required = true, value = "Quantity of the product being sold.")
  @NotNull

  @Valid
  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public TrxBasicReq amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Amount being paid for the product being sold.
   * 
   * @return amount
   **/
  @ApiModelProperty(required = true, value = "Amount being paid for the product being sold.")
  @NotNull

  @Valid
  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public TrxBasicReq currency(String currency) {
    this.currency = currency;
    return this;
  }

  /**
   * Code identifying the currency of the amount.
   * 
   * @return currency
   **/
  @ApiModelProperty(required = true, value = "Code identifying the currency of the amount.")
  @NotNull

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public TrxBasicReq vat(BigDecimal vat) {
    this.vat = vat;
    return this;
  }

  /**
   * VAT percentage INCLUDED in the amount. minimum: 0 maximum: 1
   * 
   * @return vat
   **/
  @ApiModelProperty(required = true, value = "VAT percentage INCLUDED in the amount.")
  @NotNull

  @Valid
  @DecimalMin("0")
  @DecimalMax("1")
  public BigDecimal getVat() {
    return vat;
  }

  public void setVat(BigDecimal vat) {
    this.vat = vat;
  }

  public TrxBasicReq submissioDateTime(OffsetDateTime submissioDateTime) {
    this.submissioDateTime = submissioDateTime;
    return this;
  }

  /**
   * Data richiesta autorizzazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z
   * 
   * @return submissioDateTime
   **/
  @ApiModelProperty(
                    required = true,
                    value = "Data richiesta autorizzazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z")
  @NotNull

  @Valid

  public OffsetDateTime getSubmissioDateTime() {
    return submissioDateTime;
  }

  public void setSubmissioDateTime(OffsetDateTime submissioDateTime) {
    this.submissioDateTime = submissioDateTime;
  }

  public TrxBasicReq plate(String plate) {
    this.plate = plate;
    return this;
  }

  /**
   * Registration plate of the vehicle associated with the transaction.
   * 
   * @return plate
   **/
  @ApiModelProperty(value = "Registration plate of the vehicle associated with the transaction.")

  public String getPlate() {
    return plate;
  }

  public void setPlate(String plate) {
    this.plate = plate;
  }

  public TrxBasicReq intVRC(String intVRC) {
    this.intVRC = intVRC;
    return this;
  }

  /**
   * International vehicle registration code - e.g. 'I' = Italy.
   * 
   * @return intVRC
   **/
  @ApiModelProperty(value = "International vehicle registration code - e.g. 'I' = Italy.")

  public String getIntVRC() {
    return intVRC;
  }

  public void setIntVRC(String intVRC) {
    this.intVRC = intVRC;
  }

  public TrxBasicReq km(Integer km) {
    this.km = km;
    return this;
  }

  /**
   * Kilometres reading of the vehicle's odometer at the time of the transaction. minimum: 0
   * 
   * @return km
   **/
  @ApiModelProperty(value = "Kilometres reading of the vehicle's odometer at the time of the transaction.")

  @Min(0)
  public Integer getKm() {
    return km;
  }

  public void setKm(Integer km) {
    this.km = km;
  }

  public TrxBasicReq driverId(String driverId) {
    this.driverId = driverId;
    return this;
  }

  /**
   * ID number of the driver of the vehicle making the transaction.
   * 
   * @return driverId
   **/
  @ApiModelProperty(value = "ID number of the driver of the vehicle making the transaction.")

  public String getDriverId() {
    return driverId;
  }

  public void setDriverId(String driverId) {
    this.driverId = driverId;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrxBasicReq trxBasicReq = (TrxBasicReq) o;
    return Objects.equals(this.requestCode, trxBasicReq.requestCode) && Objects.equals(this.productCode, trxBasicReq.productCode)
           && Objects.equals(this.articleCode, trxBasicReq.articleCode) && Objects.equals(this.cardNumber, trxBasicReq.cardNumber)
           && Objects.equals(this.virtualCard, trxBasicReq.virtualCard) && Objects.equals(this.buyerCode, trxBasicReq.buyerCode)
           && Objects.equals(this.outletCode, trxBasicReq.outletCode) && Objects.equals(this.vendorCode, trxBasicReq.vendorCode)
           && Objects.equals(this.quantity, trxBasicReq.quantity) && Objects.equals(this.amount, trxBasicReq.amount)
           && Objects.equals(this.currency, trxBasicReq.currency) && Objects.equals(this.vat, trxBasicReq.vat)
           && Objects.equals(this.submissioDateTime, trxBasicReq.submissioDateTime) && Objects.equals(this.plate, trxBasicReq.plate)
           && Objects.equals(this.intVRC, trxBasicReq.intVRC) && Objects.equals(this.km, trxBasicReq.km)
           && Objects.equals(this.driverId, trxBasicReq.driverId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(requestCode, productCode, articleCode, cardNumber, buyerCode, outletCode, vendorCode, quantity, amount, currency,
                        vat, submissioDateTime, plate, intVRC, km, driverId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TrxBasicReq {\n");

    sb.append("    requestCode: ")
      .append(toIndentedString(requestCode))
      .append("\n");
    sb.append("    productCode: ")
      .append(toIndentedString(productCode))
      .append("\n");
    sb.append("    articleCode: ")
      .append(toIndentedString(articleCode))
      .append("\n");
    sb.append("    cardNumber: ")
      .append(toIndentedString(cardNumber))
      .append("\n");
    sb.append("    virtualCard: ")
      .append(toIndentedString(virtualCard))
      .append("\n");
    sb.append("    buyerCode: ")
      .append(toIndentedString(buyerCode))
      .append("\n");
    sb.append("    outletCode: ")
      .append(toIndentedString(outletCode))
      .append("\n");
    sb.append("    vendorCode: ")
      .append(toIndentedString(vendorCode))
      .append("\n");
    sb.append("    quantity: ")
      .append(toIndentedString(quantity))
      .append("\n");
    sb.append("    amount: ")
      .append(toIndentedString(amount))
      .append("\n");
    sb.append("    currency: ")
      .append(toIndentedString(currency))
      .append("\n");
    sb.append("    vat: ")
      .append(toIndentedString(vat))
      .append("\n");
    sb.append("    submissioDateTime: ")
      .append(toIndentedString(submissioDateTime))
      .append("\n");
    sb.append("    plate: ")
      .append(toIndentedString(plate))
      .append("\n");
    sb.append("    intVRC: ")
      .append(toIndentedString(intVRC))
      .append("\n");
    sb.append("    km: ")
      .append(toIndentedString(km))
      .append("\n");
    sb.append("    driverId: ")
      .append(toIndentedString(driverId))
      .append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString()
            .replace("\n", "\n    ");
  }
}
