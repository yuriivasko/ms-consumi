package it.fai.ms.consumi.repository.storico_dml.jms.mapper;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.AssociazioneDvDMLDTO;
import it.fai.ms.common.dml.mappable.DmlListenerMapper;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAssociazioneDispositivoVeicolo;

@Component
public class StoricoAssociazioneDvDmlListenerMapper implements DmlListenerMapper<AssociazioneDvDMLDTO, StoricoAssociazioneDispositivoVeicolo> {

  @Override
  public StoricoAssociazioneDispositivoVeicolo toORD(AssociazioneDvDMLDTO dml, StoricoAssociazioneDispositivoVeicolo storicoAssDv) {
    String identificativoDispositivo = dml.getIdentificativoDispositivo();
    boolean isNullOrEmptyIdentifierDevice = isNullOrEmpty(identificativoDispositivo);
    if (isNullOrEmptyIdentifierDevice)
      return null;

    if (storicoAssDv == null) {
      storicoAssDv = new StoricoAssociazioneDispositivoVeicolo();
    }
    storicoAssDv.setDmlUniqueIdentifier(identificativoDispositivo);
    storicoAssDv.setDmlRevisionTimestamp(dml.getDmlRevisionTimestamp());
    storicoAssDv.setVeicoloDmlUniqueIdentifier(dml.getIdentificativoVeicolo());
    storicoAssDv.setDataAssociazione(dml.getData());
    return storicoAssDv;
  }

  private boolean isNullOrEmpty(String param) {
    if (StringUtils.isBlank(param))
      return true;
    return false;
  }

}
