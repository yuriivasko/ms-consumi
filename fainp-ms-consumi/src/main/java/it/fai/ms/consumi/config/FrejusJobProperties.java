package it.fai.ms.consumi.config;

import java.util.List;

public class FrejusJobProperties {

  List<String> tessereJolly;
  private String fatturazioniFolder;
  private String prenotazioniFolder;

  public FrejusJobProperties() {
  }

  public List<String> getTessereJolly() {
    return tessereJolly;
  }

  public void setTessereJolly(List<String> tessereJolly) {
    this.tessereJolly = tessereJolly;
  }

  public String getFatturazioniFolder() {
    return fatturazioniFolder;
  }

  public String getPrenotazioniFolder() {
    return prenotazioniFolder;
  }

  public void setFatturazioniFolder(String fatturazioniFolder) {
    this.fatturazioniFolder = fatturazioniFolder;
  }

  public void setPrenotazioniFolder(String prenotazioniFolder) {
    this.prenotazioniFolder = prenotazioniFolder;
  }

}
