package it.fai.ms.consumi.service.processor.consumi.dartford;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.generici.dartford.Dartford;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
class DartfordConsumoDettaglioStanziamentoGenricoMapper extends ConsumoDettaglioStanziamentoMapper {

  public DartfordConsumoDettaglioStanziamentoGenricoMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamento dettaglioStanziamentoGenerico = null;
    if (_consumo instanceof Dartford) {
      Dartford dartfordConsumo = (Dartford) _consumo;
      dettaglioStanziamentoGenerico = toDettaglioStanziamentoGenerico(dartfordConsumo, dartfordConsumo.getGlobalIdentifier());
    }
    return dettaglioStanziamentoGenerico;
  }

}
