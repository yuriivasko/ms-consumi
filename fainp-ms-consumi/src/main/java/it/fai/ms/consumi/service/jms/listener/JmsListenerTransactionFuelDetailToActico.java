package it.fai.ms.consumi.service.jms.listener;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.document.allegatifattura.AllegatiFattureDTO;
import it.fai.ms.consumi.service.dto.TransactionFuelDetailDTO;
import it.fai.ms.consumi.service.jms.producer.DettaglioCarburanteTransactionJmsProducer;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsListenerTransactionFuelDetailToActico implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final DettaglioCarburanteTransactionJmsProducer producer;

  @Autowired
  public JmsListenerTransactionFuelDetailToActico(final DettaglioCarburanteTransactionJmsProducer producer) {
    this.producer = producer;
  }

  public void onMessage(Message _message) {
    TransactionFuelDetailDTO dto = null;
    try {
      try {
        Serializable object = ((ObjectMessage) _message).getObject();
        dto = (TransactionFuelDetailDTO) object;
      } catch (JMSException e) {
        log.error("JMS ObjectMessage isn't {}", AllegatiFattureDTO.class.getSimpleName());
        throw new RuntimeException(e);
      }
      log.info("Received JMS message {}", dto);
      producer.sendDetailFuelMessage(dto);
    } catch (Exception _e) {
      log.error("Error consuming message {}", _message, _e);
      throw new RuntimeException(_e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.TRANSACTION_FUEL_DETAIL;
  }
}
