package it.fai.ms.consumi.repository.tollcharger.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A Caselli.
 */
@Entity
@Table(name = "toll_charger")
public class TollChargerEntity implements Serializable {

  private static final long serialVersionUID = -8050373305449553266L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "code")
  private String code;

  @Column(name = "description")
  private String description;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TollChargerEntity id(Long id) {
    this.id=id;
    return this;
  }

  public TollChargerEntity code(String code) {
    this.code=code;
    return this;
  }

  public TollChargerEntity description(String description) {
    this.description=description;
    return this;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder()
        .append("TollChargerEntity [id=")
        .append(id)
        .append(", code=")
        .append(code)
        .append(", description=")
        .append(description)
        .append("]");
    return builder.toString();
  }

}
