package it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class TrackyCardCarbStdDettaglioStanziamentoMapper extends ConsumoDettaglioStanziamentoMapper {

  public TrackyCardCarbStdDettaglioStanziamentoMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(Consumo _consumo) {
    DettaglioStanziamento dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof TrackyCardCarburantiStandard) {
      TrackyCardCarburantiStandard trkcardcarbstd = (TrackyCardCarburantiStandard) _consumo;
      dettaglioStanziamentoPedaggio = toDettaglioStanziamentoCarburanti(trkcardcarbstd, trkcardcarbstd.getGlobalIdentifier());
    }
    return dettaglioStanziamentoPedaggio;
  }

}
