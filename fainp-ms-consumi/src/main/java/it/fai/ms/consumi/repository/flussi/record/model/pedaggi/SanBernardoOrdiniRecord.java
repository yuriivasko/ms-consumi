package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import org.apache.commons.csv.CSVFormat;

import java.util.Objects;
import java.util.UUID;

public class SanBernardoOrdiniRecord extends CommonRecord
  implements GlobalIdentifierBySupplier {

  @JsonCreator
  public SanBernardoOrdiniRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID().toString();
  }

  private String globalIdentifier;


  private String ordineN;
  private String dataEmissione;
  private String tipoUtente;
  private String cliente;
  private String nominativo;
  private String stato;
  private String dataBlocco;
  private String commento;
  private String nBVTotali;
  private String nBVValidi;
  private String nBVEsauriti;
  private String nBVScaduti;
  private String nBVListaNera;
  private String nBVListaInvalidati;
  private String transitiTotali;
  private String transitiEffettuati;
  private String transitiResidui;


  public void setGlobalIdentifier(String globalIdentifier) {
    this.globalIdentifier = globalIdentifier;
  }

  public String getOrdineN() {
    return ordineN;
  }

  public void setOrdineN(String ordineN) {
    this.ordineN = ordineN;
  }

  public String getDataEmissione() {
    return dataEmissione;
  }

  public void setDataEmissione(String dataEmissione) {
    this.dataEmissione = dataEmissione;
  }

  public String getTipoUtente() {
    return tipoUtente;
  }

  public void setTipoUtente(String tipoUtente) {
    this.tipoUtente = tipoUtente;
  }

  public String getCliente() {
    return cliente;
  }

  public void setCliente(String cliente) {
    this.cliente = cliente;
  }

  public String getNominativo() {
    return nominativo;
  }

  public void setNominativo(String nominativo) {
    this.nominativo = nominativo;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public String getDataBlocco() {
    return dataBlocco;
  }

  public void setDataBlocco(String dataBlocco) {
    this.dataBlocco = dataBlocco;
  }

  public String getCommento() {
    return commento;
  }

  public void setCommento(String commento) {
    this.commento = commento;
  }

  public String getNBVTotali() {
    return nBVTotali;
  }

  public void setNBVTotali(String nBVTotali) {
    this.nBVTotali = nBVTotali;
  }

  public String getNBVValidi() {
    return nBVValidi;
  }

  public void setNBVValidi(String nBVValidi) {
    this.nBVValidi = nBVValidi;
  }

  public String getNBVEsauriti() {
    return nBVEsauriti;
  }

  public void setNBVEsauriti(String nBVEsauriti) {
    this.nBVEsauriti = nBVEsauriti;
  }

  public String getNBVScaduti() {
    return nBVScaduti;
  }

  public void setNBVScaduti(String nBVScaduti) {
    this.nBVScaduti = nBVScaduti;
  }

  public String getNBVListaNera() {
    return nBVListaNera;
  }

  public void setNBVListaNera(String nBVListaNera) {
    this.nBVListaNera = nBVListaNera;
  }

  public String getNBVListaInvalidati() {
    return nBVListaInvalidati;
  }

  public void setNBVListaInvalidati(String nBVListaInvalidati) {
    this.nBVListaInvalidati = nBVListaInvalidati;
  }

  public String getTransitiTotali() {
    return transitiTotali;
  }

  public void setTransitiTotali(String transitiTotali) {
    this.transitiTotali = transitiTotali;
  }

  public String getTransitiEffettuati() {
    return transitiEffettuati;
  }

  public void setTransitiEffettuati(String transitiEffettuati) {
    this.transitiEffettuati = transitiEffettuati;
  }

  public String getTransitiResidui() {
    return transitiResidui;
  }

  public void setTransitiResidui(String transitiResidui) {
    this.transitiResidui = transitiResidui;
  }

  @Override
  public String toString() {
    return "SanBernardoOrdiniRecord{" +
      "globalIdentifier='" + globalIdentifier + '\'' +
      ", ordineN='" + ordineN + '\'' +
      ", dataEmissione='" + dataEmissione + '\'' +
      ", tipoUtente='" + tipoUtente + '\'' +
      ", cliente='" + cliente + '\'' +
      ", nominativo='" + nominativo + '\'' +
      ", stato='" + stato + '\'' +
      ", dataBlocco='" + dataBlocco + '\'' +
      ", commento='" + commento + '\'' +
      ", nBVTotali='" + nBVTotali + '\'' +
      ", nBVValidi='" + nBVValidi + '\'' +
      ", nBVEsauriti='" + nBVEsauriti + '\'' +
      ", nBVScaduti='" + nBVScaduti + '\'' +
      ", nBVListaNera='" + nBVListaNera + '\'' +
      ", nBVListaInvalidati='" + nBVListaInvalidati + '\'' +
      ", transitiTotali='" + transitiTotali + '\'' +
      ", transitiEffettuati='" + transitiEffettuati + '\'' +
      ", transitiResidui='" + transitiResidui + '\'' +
      '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SanBernardoOrdiniRecord that = (SanBernardoOrdiniRecord) o;
    return Objects.equals(globalIdentifier, that.globalIdentifier) &&
      Objects.equals(ordineN, that.ordineN) &&
      Objects.equals(dataEmissione, that.dataEmissione) &&
      Objects.equals(tipoUtente, that.tipoUtente) &&
      Objects.equals(cliente, that.cliente) &&
      Objects.equals(nominativo, that.nominativo) &&
      Objects.equals(stato, that.stato) &&
      Objects.equals(dataBlocco, that.dataBlocco) &&
      Objects.equals(commento, that.commento) &&
      Objects.equals(nBVTotali, that.nBVTotali) &&
      Objects.equals(nBVValidi, that.nBVValidi) &&
      Objects.equals(nBVEsauriti, that.nBVEsauriti) &&
      Objects.equals(nBVScaduti, that.nBVScaduti) &&
      Objects.equals(nBVListaNera, that.nBVListaNera) &&
      Objects.equals(nBVListaInvalidati, that.nBVListaInvalidati) &&
      Objects.equals(transitiTotali, that.transitiTotali) &&
      Objects.equals(transitiEffettuati, that.transitiEffettuati) &&
      Objects.equals(transitiResidui, that.transitiResidui);
  }

  @Override
  public int hashCode() {
    return Objects.hash(globalIdentifier, ordineN, dataEmissione, tipoUtente, cliente, nominativo, stato, dataBlocco, commento, nBVTotali, nBVValidi, nBVEsauriti, nBVScaduti, nBVListaNera, nBVListaInvalidati, transitiTotali, transitiEffettuati, transitiResidui);
  }

  @Override
  public CSVFormat getCsvFormat() {
    return CSVFormat.RFC4180.withDelimiter(';');
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }
}
