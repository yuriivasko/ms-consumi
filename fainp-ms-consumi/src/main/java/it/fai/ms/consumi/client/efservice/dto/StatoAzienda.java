package it.fai.ms.consumi.client.efservice.dto;

/**
 * The StatoAzienda enumeration.
 */
public enum StatoAzienda {
    ATTIVO, SOSPESO, BLOCCATO
}
