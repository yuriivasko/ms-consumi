package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardDescriptor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.DgdTollRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class DgdTollRecordDescriptor //AutostradeCHDescriptor
  implements RecordDescriptor<DgdTollRecord> {

  private static final transient Logger         log          = LoggerFactory.getLogger(DgdTollRecordDescriptor.class);

  public final static String HEADER_BEGIN = "§ NO HEADER FILE §";
  public final static String BEGIN_N           = "N";
  public final static String FOOTER_CODE_BEGIN = "T";

  // Descrittore RECORD
  public final static FileDescriptor descriptor02 = new FileDescriptor();
  public static final String         counter      = "counter";

  static {
    descriptor02.addItem("recordCode", 1, 1);
    descriptor02.addItem("tollPointCodeEntry", 2, 5);
    descriptor02.addItem("tollPointNameEntry", 6, 25);
    descriptor02.addItem("tollPointCodeExit", 26, 29);
    descriptor02.addItem("tollPointNameExit", 30, 49);
    descriptor02.addItem("dateIn", 50, 57);
    descriptor02.addItem("timeIn", 58, 63);
    descriptor02.addItem("dateOut", 64, 71);
    descriptor02.addItem("timeOut", 72, 77);
    descriptor02.addItem("cardNumber", 78, 96);
//    descriptor02.addItem("cardNumber", 97, 102); //validita' regionale o sacdenza???
    descriptor02.addItem("currency", 103, 105);
    descriptor02.addItem("sign", 106, 106);
    descriptor02.addItem("taxableAmount", 107, 116);
    descriptor02.addItem("taxAmount", 118, 127);
    descriptor02.addItem("taxRate", 129, 133);
    descriptor02.addItem("taxedAmount", 135, 144);
    descriptor02.addItem("documentNumber", 145, 162);
    descriptor02.addItem(counter, 163, 168);
  }

  // Descrittore FOOTER
  public final static FileDescriptor descriptor03 = new FileDescriptor();

  static {
    descriptor03.addItem("recordCode", 1, 1);
    descriptor03.addItem("fileName", 6, 25);
    descriptor03.addItem("sequenceNumber", 50, 57);
    descriptor03.addItem("totalRows", 58, 63);
    descriptor03.addItem("currency", 103, 105);
    descriptor03.addItem("sign", 106, 106);
    descriptor03.addItem("totalTaxableAmount", 107, 116);
    descriptor03.addItem("totalTaxAmount", 118, 127);
    descriptor03.addItem("totalTaxedAmount", 135, 144);
    descriptor03.addItem(counter, 163, 168);
  }

  @Override
  public String extractRecordCode(String _data){
    return StringUtils.left(_data, 1);
  }

  @Override
  public DgdTollRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {

    final DgdTollRecord entity = new DgdTollRecord(fileName, rowNumber);
    switch (_recordCode) {
    case BEGIN_N:
      entity.setFromString(DgdTollRecordDescriptor.descriptor02.getItems(), _data);
      break;
    case FOOTER_CODE_BEGIN:
      entity.setFromString(DgdTollRecordDescriptor.descriptor03.getItems(), _data);
      break;
    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    Optional<StringItem> stringItem = DgdTollRecordDescriptor.descriptor03.getItem(DgdTollRecordDescriptor.counter);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in DgdTollFileDescriptor!");
    }
    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    //non ha header
    return DgdTollRecordDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return DgdTollRecordDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(DgdTollRecordDescriptor.class, DgdTollRecord.class).checkConfiguration();
  }
}
