package it.fai.ms.consumi.service.processor.consumi.vacon;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Vacon;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.repository.CaselliEntityRepository;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class VaconConsumoDettaglioStanziamentoPedaggioMapper extends ConsumoDettaglioStanziamentoMapper {

  private CaselliEntityRepository caselliRepository = null;


  public VaconConsumoDettaglioStanziamentoPedaggioMapper(CaselliEntityRepository caselliRepository) {
    this.caselliRepository = caselliRepository;
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamentoPedaggio dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof Vacon) {
      Vacon vaconConsumo = (Vacon) _consumo;
      updateDescriptionTollPointGateByCaselli(vaconConsumo.getTollPointEntry(),caselliRepository);
      updateDescriptionTollPointGateByCaselli(vaconConsumo.getTollPointExit(),caselliRepository);
      dettaglioStanziamentoPedaggio = toDettaglioStanziamentoPedaggio(vaconConsumo, vaconConsumo.getGlobalIdentifier());
      
   
    }
    return dettaglioStanziamentoPedaggio;
  }

}
