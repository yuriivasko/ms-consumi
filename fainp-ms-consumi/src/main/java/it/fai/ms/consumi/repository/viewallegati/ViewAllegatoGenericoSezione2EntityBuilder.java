package it.fai.ms.consumi.repository.viewallegati;

import java.time.LocalDateTime;

public class ViewAllegatoGenericoSezione2EntityBuilder {
  private String id;
  private String codiceContratto;
  private String tipoDispositivo;
  private String panNumber;
  private String serialNumber;
  private String nazioneVeicolo;
  private String targaVeicolo;
  private String classificazioneEuro;
  private String classeTariffaria;
  private LocalDateTime dataInizio;
  private LocalDateTime dataFine;
  private String causale;
  private String prodotto;
  private String descrizione;
  private Double quantita;
  private Double importo;
  private Double importoConIva;
  private Double imponibile;
  private String valuta;
  private Double cambio;
  private String tratta;
  private Double percIva;
  private String raggruppamentoArticoli;
  private String codiceFornitoreNav;
  private String recordCode; 
  private String puntoErogazione;

  public ViewAllegatoGenericoSezione2EntityBuilder setId(String id) {
    this.id = id;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
    return this;
  }
  
  public ViewAllegatoGenericoSezione2EntityBuilder setPanNumber(String panNumber) {
    this.panNumber = panNumber;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setNazioneVeicolo(String nazioneVeicolo) {
    this.nazioneVeicolo = nazioneVeicolo;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setTargaVeicolo(String targaVeicolo) {
    this.targaVeicolo = targaVeicolo;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setClassificazioneEuro(String classificazioneEuro) {
    this.classificazioneEuro = classificazioneEuro;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setClasseTariffaria(String classeTariffaria) {
    this.classeTariffaria = classeTariffaria;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setDataInizio(LocalDateTime dataInizio) {
    this.dataInizio = dataInizio;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setDataFine(LocalDateTime dataFine) {
    this.dataFine = dataFine;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setCausale(String causale) {
    this.causale = causale;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setProdotto(String prodotto) {
    this.prodotto = prodotto;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setDescrizione(String descrizione) {
    this.descrizione = descrizione;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setQuantita(Double quantita) {
    this.quantita = quantita;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setImporto(Double importo) {
    this.importo = importo;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setImportoConIva(Double importoConIva) {
    this.importoConIva = importoConIva;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setImponibile(Double imponibile) {
    this.imponibile = imponibile;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setValuta(String valuta) {
    this.valuta = valuta;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setCambio(Double cambio) {
    this.cambio = cambio;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setTratta(String tratta) {
    this.tratta = tratta;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setPercIva(Double percIva) {
    this.percIva = percIva;
    return this;
  }

  public ViewAllegatoGenericoSezione2EntityBuilder setRaggruppamentoArticoli(String raggruppamentoArticoli) {
    this.raggruppamentoArticoli = raggruppamentoArticoli;
    return this;
  }
  
  public ViewAllegatoGenericoSezione2EntityBuilder setCodiceFornitoreNav(String codiceFornitoreNav) {
    this.codiceFornitoreNav = codiceFornitoreNav;
    return this;
  }
  public ViewAllegatoGenericoSezione2EntityBuilder setRecordCode(String recordCode) {
    this.recordCode = recordCode;
    return this;
  }
  public ViewAllegatoGenericoSezione2EntityBuilder setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
    return this;
  }

  public ViewAllegatoGenericoSezione2Entity createViewAllegatoGenericoSezione2Entity() {
    ViewAllegatoGenericoSezione2Entity entity = new ViewAllegatoGenericoSezione2Entity();
    entity.populate(codiceContratto, tipoDispositivo, panNumber, serialNumber, nazioneVeicolo, targaVeicolo, classificazioneEuro, classeTariffaria, dataInizio, dataFine, causale, prodotto, descrizione, quantita, importo, importoConIva, imponibile, valuta, cambio, tratta, percIva, raggruppamentoArticoli, codiceFornitoreNav, recordCode, puntoErogazione);
    entity.setId(id);
    return entity;
  }
}
