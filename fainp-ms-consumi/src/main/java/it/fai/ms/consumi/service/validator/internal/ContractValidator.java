package it.fai.ms.consumi.service.validator.internal;

import java.time.Instant;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.validator.TimeValidator;

public interface ContractValidator extends TimeValidator {

  ValidationOutcome findAndSetContract(Contract contract);

  ValidationOutcome contractActiveInDate(String id, Instant date);

}
