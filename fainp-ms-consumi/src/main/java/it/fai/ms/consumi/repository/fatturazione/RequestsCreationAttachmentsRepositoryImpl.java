package it.fai.ms.consumi.repository.fatturazione;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.dto.RequestCreationAttachmentDTO;
import it.fai.ms.consumi.repository.fatturazione.model.RequestsCreationAttachmentsEntity;
import it.fai.ms.consumi.repository.fatturazione.model.RequestsCreationAttachmentsEntityMapper;

@Repository
@Validated
@Transactional
public class RequestsCreationAttachmentsRepositoryImpl implements RequestsCreationAttachmentsRepository {

  private final transient Logger log = LoggerFactory.getLogger(getClass());

  private final EntityManager                           em;
  private final RequestsCreationAttachmentsEntityMapper entityMapper;

  @Autowired
  public RequestsCreationAttachmentsRepositoryImpl(final EntityManager _entityManager,
                                                   final RequestsCreationAttachmentsEntityMapper _entityMapper) {
    em = _entityManager;
    entityMapper = _entityMapper;
  }

  @Override
  public RequestsCreationAttachments find(Long id) {

    RequestsCreationAttachments result = null;

    var criteriaBuilder = em.getCriteriaBuilder();
    var criteriaQuery = criteriaBuilder.createQuery(RequestsCreationAttachmentsEntity.class);

    var root = criteriaQuery.from(RequestsCreationAttachmentsEntity.class);
    criteriaQuery.select(root);

    List<Predicate> predicates = null;
    if (id != null) {
      RequestCreationAttachmentDTO dto = new RequestCreationAttachmentDTO().id(id);
      predicates = createPredicates(root, dto, criteriaBuilder);
      criteriaQuery.where(predicates.toArray(new Predicate[] {}));

      var typedQuery = em.createQuery(criteriaQuery);
      logQuery(typedQuery);
      RequestsCreationAttachmentsEntity singleResult = typedQuery.getSingleResult();
      result = entityMapper.toDomain(singleResult);
    }

    log.info("Found: {}", result);
    return result;
  }

  @Override
  public RequestsCreationAttachments save(RequestsCreationAttachments _domain) {
    RequestsCreationAttachmentsEntity entity = entityMapper.toEntity(_domain);
    if (entity != null) {
      if (entity.getId() == null) {
        log.debug("Persist: {}", entity);
        em.persist(entity);
      } else {
        log.debug("Update: {}", entity);
        entity = em.merge(entity);
      }
    }
    log.debug("Save: {}", entity);
    return entity == null ? null : entityMapper.toDomain(entity);
  }

  @Override
  public void delete(Long id) {
    if (id != null) {
      var cb = this.em.getCriteriaBuilder();
      // create delete
      var delete = cb.createCriteriaDelete(RequestsCreationAttachmentsEntity.class);
      // set the root class
      var root = delete.from(RequestsCreationAttachmentsEntity.class);
      // set where clause
      delete.where(cb.equal(root.get("id"), id));
      // perform update
      em.createQuery(delete)
        .executeUpdate();
    } else {
      log.error("Not delete entity, because id is null");
    }
  }

  @Override
  public void delete(RequestsCreationAttachments domain) {
    log.debug("Remove " + ((domain != null) ? domain.getClass()
                                                    .getSimpleName()
                                              + ": " + domain
                                            : null));
    if (domain != null) {
      delete(domain.getId());
    }
  }

  @Override
  public RequestsCreationAttachments findByCodiceFatturaAndCodiceRaggruppamentoArticoli(String numeroFattura,
                                                                                        String codiceRaggruppamentoArticoli) {

    RequestsCreationAttachmentsEntity result = null;

    var criteriaBuilder = em.getCriteriaBuilder();
    var criteriaQuery = criteriaBuilder.createQuery(RequestsCreationAttachmentsEntity.class);

    var root = criteriaQuery.from(RequestsCreationAttachmentsEntity.class);
    criteriaQuery.select(root);

    List<Predicate> predicates = null;

    if (isNotBlank(numeroFattura) && isNotBlank(codiceRaggruppamentoArticoli)) {
      RequestCreationAttachmentDTO dto = new RequestCreationAttachmentDTO().codiceFattura(numeroFattura)
                                                                           .codiceRaggruppamentoArticoli(codiceRaggruppamentoArticoli);
      predicates = createPredicates(root, dto, criteriaBuilder);
      criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

      TypedQuery<RequestsCreationAttachmentsEntity> createQuery = em.createQuery(criteriaQuery);
      createQuery.setMaxResults(1);
      logQuery(createQuery);
      List<RequestsCreationAttachmentsEntity> resultList = createQuery.getResultList();
      if (resultList != null && !resultList.isEmpty()) {
        result = resultList.get(0);
      }
    }

    log.info("Found: {}", result);
    return entityMapper.toDomain(result);
  }

  @Override
  @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.NOT_SUPPORTED)
  public List<RequestsCreationAttachments> findByCodiceFattura(String numeroFattura) {
    List<RequestsCreationAttachmentsEntity> results = null;

    var criteriaBuilder = em.getCriteriaBuilder();
    var criteriaQuery = criteriaBuilder.createQuery(RequestsCreationAttachmentsEntity.class);

    var root = criteriaQuery.from(RequestsCreationAttachmentsEntity.class);
    criteriaQuery.select(root);

    List<Predicate> predicates = null;

    if (isNotBlank(numeroFattura)) {
      RequestCreationAttachmentDTO dto = new RequestCreationAttachmentDTO().codiceFattura(numeroFattura);
      predicates = createPredicates(root, dto, criteriaBuilder);
      criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));

      TypedQuery<RequestsCreationAttachmentsEntity> createQuery = em.createQuery(criteriaQuery);
      logQuery(createQuery);
      List<RequestsCreationAttachmentsEntity> resultList = createQuery.getResultList();
      if (resultList != null && !resultList.isEmpty()) {
        results = resultList;
      }
    }
    return results == null ? new ArrayList<>()
                           : results.stream()
                                    .map(entity -> entityMapper.toDomain(entity))
                                    .collect(toList());
  }

  private List<Predicate> createPredicates(Root<RequestsCreationAttachmentsEntity> root, RequestCreationAttachmentDTO dto,
                                           CriteriaBuilder cb) {
    List<Predicate> predicates = new ArrayList<>();
    Long id = dto.getId();
    if (id != null && id > 0) {
      Predicate p = cb.equal(root.get("id"), id);
      predicates.add(p);
    }

    String codiceFattura = dto.getCodiceFattura();
    if (isNotBlank(codiceFattura)) {
      Predicate p = cb.equal(root.get("codiceFattura"), codiceFattura);
      predicates.add(p);
    }
    String codiceRaggruppamentoArticoli = dto.getCodiceRaggruppamentoArticoli();
    if (isNotBlank(codiceRaggruppamentoArticoli)) {
      Predicate p = cb.equal(root.get("codiceRaggruppamentoArticolo"), codiceRaggruppamentoArticoli);
      predicates.add(p);
    }

    return predicates;
  }

  private void logQuery(TypedQuery<?> createQuery) {
    String queryString = createQuery.unwrap(org.hibernate.query.Query.class)
                                    .getQueryString();
    log.debug("Query string: {}", queryString);
  }

}
