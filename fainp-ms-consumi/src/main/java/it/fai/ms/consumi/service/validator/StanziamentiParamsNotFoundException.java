package it.fai.ms.consumi.service.validator;

public class StanziamentiParamsNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 5956373169923797067L;

  public StanziamentiParamsNotFoundException() {
  }

  public StanziamentiParamsNotFoundException(final String _message) {
    super(_message);
  }

}
