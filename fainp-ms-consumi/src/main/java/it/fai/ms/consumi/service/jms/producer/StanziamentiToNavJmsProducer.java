package it.fai.ms.consumi.service.jms.producer;

import static it.fai.ms.common.jms.JmsQueueNames.STANZIAMENTI_TO_NAV;
import static it.fai.ms.consumi.service.jms.model.StanziamentoMessage.extractCodes;
import static it.fai.ms.consumi.service.jms.model.StanziamentoMessage.now;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;

@Service
@Transactional
public class StanziamentiToNavJmsProducer implements StanziamentiToNavPublisher {

  public static final String QUEUE = JmsQueueNames.STANZIAMENTI_TO_NAV.name();

  private static ArrayList<StanziamentoMessage> copyToSerializableCollection(final List<StanziamentoMessage> _stanziamentoMessages) {
    return new ArrayList<>(Optional.ofNullable(_stanziamentoMessages)
                                   .orElse(Collections.emptyList()));
  }

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final JmsProperties          jmsProperties;
  private final StanziamentoRepository stanziamentoRepository;
  
  public StanziamentiToNavJmsProducer(final JmsProperties _jmsProperties, final StanziamentoRepository _stanziamentoRepository) {
    jmsProperties = _jmsProperties;
    stanziamentoRepository = _stanziamentoRepository;
  }

  /* (non-Javadoc)
   * @see it.fai.ms.consumi.service.jms.producer.StanziamentiToNavPublisher#publish(java.util.List)
   */
  @Override
  public void publish(final List<StanziamentoMessage> _stanziamentoMessages) {
    
    final var stanziamentoMessages = copyToSerializableCollection(_stanziamentoMessages);

    publishToQueue(stanziamentoMessages);
    updateDateQueuing(_stanziamentoMessages);

    stanziamentoMessages.forEach(stanziamentoMessage -> _log.info("Published to queue : {}", stanziamentoMessage));
  }

  private void publishToQueue(final ArrayList<StanziamentoMessage> _stanziamentoMessages) {
    JmsQueueSenderUtil.publish(jmsProperties, STANZIAMENTI_TO_NAV, _stanziamentoMessages);
  }

  private void updateDateQueuing(final List<StanziamentoMessage> _stanziamentoMessages) {
    stanziamentoRepository.updateDateQueuing(extractCodes(_stanziamentoMessages), now());
  }

}
