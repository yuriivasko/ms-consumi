package it.fai.ms.consumi.domain;

public interface TrustedCustomerSupplier {
  Customer getCustomer();
}
