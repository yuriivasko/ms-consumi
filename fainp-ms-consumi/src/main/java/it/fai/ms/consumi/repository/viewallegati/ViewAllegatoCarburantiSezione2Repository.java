package it.fai.ms.consumi.repository.viewallegati;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface ViewAllegatoCarburantiSezione2Repository {

  List<ViewAllegatoCarburantiSezione2Entity> findByDettaglioStanziamentoId(List<UUID> dettagliStanziamentoId);

}
