package it.fai.ms.consumi.api.trx;

import java.time.OffsetDateTime;
import java.time.ZoneId;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.consumi.api.trx.model.TrxNoOilReq;
import it.fai.ms.consumi.api.trx.model.TrxOilReq;
import it.fai.ms.consumi.api.trx.model.TrxResp;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.trx.TrxJob;

/**
 * REST controller for managing Api Transactions.
 */
@RestController
@RequestMapping("/api/trx")
public class TransactionsApi {

  private final Logger log = LoggerFactory.getLogger(TransactionsApi.class);

  private final TrxJob trxJob;

  public TransactionsApi(final TrxJob _trxJob) {
    trxJob = _trxJob;
  }

  /**
   * POST /noOil : Create a new NoOil transaction.
   */
  @PostMapping("/noOil")
  public ResponseEntity<TrxResp> trxNoOil(@Valid @RequestBody TrxNoOilReq req) {
    log.debug("REST request to save TrxNoOilReq : {}", req);
    TrxResp result = new TrxResp();
    ValidationOutcome validation = trxJob.consumeMessage(req);
    if (validation != null && validation.isFatalError()) {
      setErrorOnResult(validation, result);
    } else {
      result.setSuccess(1);
    }
    result.setRequestCode(req.getRequestCode());
    result.setUtcTimestamp(OffsetDateTime.now(ZoneId.of("Z")));
    log.info("Result: {}", result);
    return ResponseEntity.ok(result);
  }

  /**
   * POST /oil : Create a a new oil transaction.
   */
  @PostMapping("/oil")
  public ResponseEntity<TrxResp> trxOil(@Valid @RequestBody TrxOilReq req) {
    log.debug("REST request to save TrxOilReq : {}", req);
    TrxResp result = new TrxResp();
    ValidationOutcome validation = trxJob.consumeMessage(req);
    if (validation != null && validation.isFatalError()) {
      setErrorOnResult(validation, result);
    } else {
      result.setSuccess(1);
    }
    result.setRequestCode(req.getRequestCode());
    result.setUtcTimestamp(OffsetDateTime.now(ZoneId.of("Z")));
    log.info("Result: {}", result);
    return ResponseEntity.ok(result);
  }

  private void setErrorOnResult(ValidationOutcome validation, TrxResp result) {
    result.setSuccess(0);
    Message message = validation.getMessages()
                                .stream()
                                .findFirst()
                                .get();
    result.errorCode(Integer.parseInt(message.getCode()));
    result.errorMessage(message.getText());
  }

}
