package it.fai.ms.consumi.service.navision;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.money.Monetary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import feign.FeignException;
import it.fai.ms.consumi.adapter.nav.auth.NavAuthorizationTokenService;
import it.fai.ms.consumi.client.StanziamentiApiService;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.navision.PricesByNavDTO;
import it.fai.ms.consumi.domain.navision.PricesByNavDTO.CostoRicavoEnum;
import it.fai.ms.consumi.domain.navision.PricesByNavReturnDTO;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;

@Service
@Transactional
public class PriceCalculatedByNavService {

  // private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
  public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
                                                                     .withZone(ZoneId.of("Europe/Berlin"));

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private StanziamentiApiService stanziamentiApi;

  private NavAuthorizationTokenService navAuthorizationTokenService;

  public PriceCalculatedByNavService(final NavAuthorizationTokenService navAuthorizationTokenService) {
    this.navAuthorizationTokenService = navAuthorizationTokenService;
  }

  public Number getCalculatedPriceByNav(PricesByNavDTO requestPriceDTO) {
    log.debug("getCalculatedPriceByNav Req {}", requestPriceDTO);
    PricesByNavReturnDTO res = null;
    try {
      String authorizationToken = navAuthorizationTokenService.getAuthorizationToken();
      res = stanziamentiApi.getPricesByNavPost(authorizationToken, requestPriceDTO);
      log.debug("getCalculatedPriceByNav Res {}", res);
    } catch (FeignException e) {
      log.error("Impossibile recuperare il prezzo calcolato da Navision: request {}", requestPriceDTO, e);
      throw new RuntimeException(e);
    }

    return res.getPrezzoCalcolatoNoIva();
  }

  public void setPriceCalculatedAmountByNav(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                            StanziamentiParams _stanziamentiParams) {
    _dettaglioStanziamentoCarburante.setPriceComputed(new Amount());
    setCalculatedAmountByNav(_dettaglioStanziamentoCarburante, _stanziamentiParams, _dettaglioStanziamentoCarburante.getPriceReference(),
                             _dettaglioStanziamentoCarburante.getPriceComputed(), CostoRicavoEnum.NUMBER_1);
  }

  public void setCostCalculatedAmountByNav(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                           StanziamentiParams _stanziamentiParams) {
    _dettaglioStanziamentoCarburante.setCostComputed(new Amount());
    setCalculatedAmountByNav(_dettaglioStanziamentoCarburante, _stanziamentiParams, _dettaglioStanziamentoCarburante.getPriceReference(),
                             _dettaglioStanziamentoCarburante.getCostComputed(), CostoRicavoEnum.NUMBER_0);
  }

  private void setCalculatedAmountByNav(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                        StanziamentiParams _stanziamentiParams, Amount amountReference, Amount amountComputed,
                                        CostoRicavoEnum costoRicavoEnum) {
    Number importoNoIva = new BigDecimal(amountReference.getAmountExcludedVat()
                                                        .getNumber()
                                                        .toString(),
                                         MathContext.DECIMAL32);

    PricesByNavDTO req = new PricesByNavDTO();
    req.setClasseTariffaria(_stanziamentiParams.getFareClass());
    req.setCodiceArticoloNav(_stanziamentiParams.getArticle()
                                                .getCode());
    req.setCodiceCliente(_dettaglioStanziamentoCarburante.getCustomer()
                                                         .getId());
    req.setCodiceFornitoreNav(_dettaglioStanziamentoCarburante.getNavSupplierCode());
    req.setCostoRicavo(costoRicavoEnum); // 0 = Costo (passivo), 1 = Ricavo (attivo)
    req.setDataDiRiferimento(FORMATTER.format(_dettaglioStanziamentoCarburante.getEntryDateTime()));
    req.setImportoNoIvaRiferimento(importoNoIva.doubleValue());

    log.info("setCalculatedAmountByNav Amount request: {}", importoNoIva);
    Number tmp = this.getCalculatedPriceByNav(req);
    BigDecimal valueRes = new BigDecimal(tmp.toString(), MathContext.DECIMAL32);

    log.info("setCalculatedAmountByNav Amount response: {}", valueRes);

    amountComputed.setAmountExcludedVat(Monetary.getDefaultAmountFactory()
                                                .setCurrency(amountReference.getAmountExcludedVat()
                                                                            .getCurrency())
                                                .setNumber(valueRes)
                                                .create());
    amountComputed.setVatRate(BigDecimal.valueOf(_stanziamentiParams.getArticle()
                                                                    .getVatRate()));
  }

  public BigDecimal getCalculatedAmountByNav(DettaglioStanziamentoCarburanteEntity _dettaglioStanziamentoCarburanteEntity,
                                             StanziamentiParams _stanziamentiParams, Number importoNoIva, CostoRicavoEnum costoRicavoEnum) {
    PricesByNavDTO req = new PricesByNavDTO();
    req.setClasseTariffaria(_stanziamentiParams.getFareClass());
    req.setCodiceArticoloNav(_stanziamentiParams.getArticle()
                                                .getCode());
    req.setCodiceCliente(_dettaglioStanziamentoCarburanteEntity.getCustomerId());
    req.setCodiceFornitoreNav(_dettaglioStanziamentoCarburanteEntity.getCodiceFornitoreNav());
    req.setCostoRicavo(costoRicavoEnum); // 0 = Costo (passivo), 1 = Ricavo (attivo)
    req.setDataDiRiferimento(FORMATTER.format(_dettaglioStanziamentoCarburanteEntity.getDataOraUtilizzo()));
    req.setImportoNoIvaRiferimento(importoNoIva.doubleValue());

    log.debug("Amount request: {}", importoNoIva);
    Number tmp = this.getCalculatedPriceByNav(req);
    BigDecimal valueRes = new BigDecimal(tmp.toString(), MathContext.DECIMAL32);

    log.debug("Amount response: {}", valueRes);

    return valueRes;
  }

}
