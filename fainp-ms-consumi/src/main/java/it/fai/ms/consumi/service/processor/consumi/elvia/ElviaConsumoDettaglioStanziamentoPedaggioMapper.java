package it.fai.ms.consumi.service.processor.consumi.elvia;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class ElviaConsumoDettaglioStanziamentoPedaggioMapper extends ConsumoDettaglioStanziamentoMapper {

  public ElviaConsumoDettaglioStanziamentoPedaggioMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamento dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof Elvia) {
      Elvia elviaConsumo = (Elvia) _consumo;
      dettaglioStanziamentoPedaggio = toDettaglioStanziamentoPedaggio(elviaConsumo, elviaConsumo.getGlobalIdentifier());
    }
    return dettaglioStanziamentoPedaggio;
  }

}
