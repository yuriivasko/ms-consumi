package it.fai.ms.consumi.domain.consumi.pedaggi.tunnel;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class GranSanBernardoGlobalIdentifier extends GlobalIdentifier {

  public GranSanBernardoGlobalIdentifier(final GlobalIdentifierType _type) {
    super(_type);
  }

  @Override
  public String getId() {
    // TODO Auto-generated method stub
    return "::notAvailable::";
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    throw new UnsupportedOperationException("not implemented");
  }

}
