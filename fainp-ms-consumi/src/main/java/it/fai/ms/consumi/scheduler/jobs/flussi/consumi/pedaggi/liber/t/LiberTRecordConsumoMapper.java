package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Consumer;

import static it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils.strToMonetary;

@Service
public class LiberTRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<LiberTRecord, LiberTConsumo> {

  private static final String LIBER_T_SOURCE = "LiberT";

  @Override
  public LiberTConsumo mapRecordToConsumo(LiberTRecord record) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(record, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant           ingestionTime              = record.getIngestion_time();
    Instant           acquisitionDate            = optionalCreationDateInFile.orElse(record.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, LIBER_T_SOURCE);
    source.setFileName(record.getFileName());
    source.setRowNumber(record.getRowNumber());

    LiberTConsumo consumo = new LiberTConsumo(source, record.getRecordCode(),
                                              Optional.of(new LiberTGlobalIdentifier(record.getGlobalIdentifier())), record);

//    consumo.setTransaction(init(new Transaction(),t->{
//      t.setDetailCode("LIBERT");
//    }));

    consumo.setAmount(AmountBuilder.builder()
                                   .amountIncludedVat(strToMonetary(record.getMontantTTC(), "EUR"))
                                   .vatRate(new BigDecimal(record.getTauxTVA()))
                                   .build());

    consumo.setVehicle(init(new Vehicle(),v ->{
      v.setFareClass(record.getClasseVehicule());
    }));

    consumo.setDevice(init(new Device(TipoDispositivoEnum.LIBER_T),d->{
      d.setPan(parsePan(record.getNBadge()));
      d.setServiceType(TipoServizioEnum.PEDAGGI_FRANCIA);
    }));

    consumo.setTollPointEntry(init(new TollPoint(),t->{
      t.setGlobalGate(init(new GlobalGate(record.getNGareEntree()),g->{
        g.setDescription(record.getNomGareEntree());
      }));
      setTime(t, record.getDateHeureEntree());
    }));

    consumo.setTollPointExit(init(new TollPoint(),t->{
      t.setGlobalGate(init(new GlobalGate(record.getNGareSortie()),g->{
        g.setDescription(record.getNomGareSortie());
      }));
      setTime(t, record.getDateHeureSortie());
    }));



    return consumo;

  }
  private void setTime(TollPoint t, String dateHeureSortie) {
    try {
      super.calculateInstantByDateTime(dateHeureSortie,"dd/MM/yyyy HH:mm:ss").ifPresent(i->{
        t.setTime(i);
      });
    } catch (ParseException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
private String parsePan(String nBadge) {
    return "250100193116" + StringUtils.leftPad(nBadge, 5, "0") + "00010000101";
  }
public static <T> T init(T obj, Consumer<T> initializer) {
  initializer.accept(obj);
  return obj;
}
}
