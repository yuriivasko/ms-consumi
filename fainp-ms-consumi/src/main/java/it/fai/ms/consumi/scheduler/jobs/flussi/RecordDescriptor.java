package it.fai.ms.consumi.scheduler.jobs.flussi;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;

@Service
public interface RecordDescriptor<T extends CommonRecord> {

  Map<Integer, Integer> getStartEndPosTotalRows();

  String getHeaderBegin();

  String getFooterCodeBegin();

  int getLinesToSubtractToMatchDeclaration();

  default boolean skipWellKnownRecordCodeToIgnore(String recordCode){
    return false;
  }

  default String extractRecordCode(String _data){
    return _data.substring(0, 2);
  }

  T decodeRecordCodeAndCallSetFromString(final String _data, final String _recordCode, String _fileName, long _rowNumber);

  public static Map<Integer, Integer> getStartEndForFields(FileDescriptor descriptor,String ... fields) {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    for (String descName : fields) {
      Optional<StringItem> stringItem = descriptor.getItem(descName);
      if (!stringItem.isPresent()) {
        throw new IllegalStateException("Fatal error in RecordDescriptor! "+descName);
      }
      startEndPosTotalRows.put(stringItem.get()
                                         .getStart(),
                               stringItem.get()
                                         .getEnd());

    }
    return startEndPosTotalRows;
  }

  default boolean  isFooterLine(String line) {
    return line.startsWith(getFooterCodeBegin());
  }

  default boolean isHeaderLine(String line) {
    return line.startsWith(getHeaderBegin());
  }

  void checkConfiguration();

}
