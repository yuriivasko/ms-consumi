package it.fai.ms.consumi.domain.consumi.treni;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.FareClassSupplier;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Optional;

public abstract class ConsumoTreno
  extends Consumo
  implements FareClassSupplier {

  private String fareClass;

  public ConsumoTreno(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public String getFareClass() {
    return fareClass;
  }

  public void setFareClass(final String _fareClass) {
    this.fareClass = _fareClass;
  }

}
