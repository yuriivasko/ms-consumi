package it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Optional;

public class SanBernardo extends ConsumoPedaggio {

  public static final String SAN_BERNARDO_BUONI = "Buono Virtuale";
  public static final String SAN_BERNARDO_TESSERE = "Carta SISEX";
  private String customerId = null;


  public SanBernardo(Source _source, String _recordCode, Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  @Override
  public InvoiceType getInvoiceType() {
    return InvoiceType.D;
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }


}
