package it.fai.ms.consumi.service.processor;

import feign.FeignException;
import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import it.fai.ms.consumi.service.record.RecordConsumoReprocessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RecordConsumoReprocessorService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public static final String RECORD_REPROCESSED = "Record with type:=[$type] and uuid:=[$uuid] was reprocessed!";

  private ElasticSearchFeignClient esClient;
  private RecordConsumoReprocessor recordConsumoReprocessor;

  public RecordConsumoReprocessorService(ElasticSearchFeignClient esClient, RecordConsumoReprocessor recordConsumoReprocessor) {
    this.esClient = esClient;
    this.recordConsumoReprocessor = recordConsumoReprocessor;
  }

  public String reprocessSingleRecord(String type, String uuid) {

    CommonRecord record = retrieveRecord(type, uuid);
    reprocessRecord(record);

    return RECORD_REPROCESSED.replace("$type", type).replace("$uuid", uuid);
  }

  private CommonRecord retrieveRecord(String type, String uuid) {
    log.debug("Try to retrieve record...");
    try {

      ElasticSearchRecord<? extends CommonRecord> record = esClient.getRecord(type, uuid);
      if (record == null) {
        log.warn("Record found!... Returning error.");
        throw new RecordNotRetrievableException(type, uuid);
      }

      if (record.getRawdata() == null) {
        log.warn("Record Rawdata empty!... Returning error.");
        throw new RecordNotRetrievableException(type, uuid);
      }

      CommonRecord rawdata = record.getRawdata();
      log.debug("Record found:=[{}]", rawdata.toNotificationMessage());
      return rawdata;

    } catch (FeignException e){
      log.debug("Record or elastic search endpoint not found... Returning error.");
      throw new RecordNotRetrievableException(type, uuid, e);
    }
  }

  private void reprocessRecord(CommonRecord record) {
    log.debug("Reprocessing single record...");

    SingleRecordProcessingResult reprocessingResult = recordConsumoReprocessor.processSingleRecord(record);
    if (!reprocessingResult.isOK()){
      log.debug("Reprocessing gone bad! Returning error!");
      throw new RecordNotReprocessedException(record);
    }
    log.debug("Reprocessing OK!");
  }

}

