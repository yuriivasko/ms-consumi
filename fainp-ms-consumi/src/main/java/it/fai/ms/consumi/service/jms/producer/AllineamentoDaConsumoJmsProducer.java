package it.fai.ms.consumi.service.jms.producer;

import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.JmsDestination;
import it.fai.ms.common.jms.JmsObjectMessageSenderTemplate;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.efservice.message.consumi.AllineamentoDaConsumoMessage;

@Service
public class AllineamentoDaConsumoJmsProducer extends JmsObjectMessageSenderTemplate<AllineamentoDaConsumoMessage> {

  public AllineamentoDaConsumoJmsProducer(JmsProperties jmsProperties) {
    super(jmsProperties);
  }

  @Override
  public JmsDestination getJmsDestination() {
    return JmsQueueNames.ALLINEAMENTI_DA_CONSUMO;
  }
}
