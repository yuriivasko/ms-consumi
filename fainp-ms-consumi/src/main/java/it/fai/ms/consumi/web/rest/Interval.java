package it.fai.ms.consumi.web.rest;

import java.time.Instant;

public  class Interval{
		public Instant startDate;
		public Instant endDate;

		
		public Instant getStartDate() {
			return startDate;
		}
		public void setStartDate(Instant startDate) {
			this.startDate = startDate;
		}
		public Instant getEndDate() {
			return endDate;
		}
		public void setEndDate(Instant endDate) {
			this.endDate = endDate;
		}
	}