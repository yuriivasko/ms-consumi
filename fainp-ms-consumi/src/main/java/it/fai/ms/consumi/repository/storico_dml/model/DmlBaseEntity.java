package it.fai.ms.consumi.repository.storico_dml.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import it.fai.ms.common.dml.AbstractDml;

@MappedSuperclass
@IdClass(DmlEmbeddedId.class)
public class DmlBaseEntity implements AbstractDml {
  private static final long serialVersionUID = -8779419556107774378L;
  
  @Id
  @Size(max = 255)
  @Column(name = "dml_unique_identifier", length = 255)
  private String dmlUniqueIdentifier;
  
  @Id
  @Column(name = "dml_revision_timestamp")
  private Instant dmlRevisionTimestamp;

  public String getDmlUniqueIdentifier() {
    return dmlUniqueIdentifier;
  }

  public void setDmlUniqueIdentifier(String dmlUniqueIdentifier) {
    this.dmlUniqueIdentifier = dmlUniqueIdentifier;
  }

  public Instant getDmlRevisionTimestamp() {
    return dmlRevisionTimestamp;
  }

  public void setDmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((dmlRevisionTimestamp == null) ? 0 : dmlRevisionTimestamp.hashCode());
    result = prime * result + ((dmlUniqueIdentifier == null) ? 0 : dmlUniqueIdentifier.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    DmlBaseEntity other = (DmlBaseEntity) obj;
    if (dmlRevisionTimestamp == null) {
      if (other.dmlRevisionTimestamp != null)
        return false;
    } else if (!dmlRevisionTimestamp.equals(other.dmlRevisionTimestamp))
      return false;
    if (dmlUniqueIdentifier == null) {
      if (other.dmlUniqueIdentifier != null)
        return false;
    } else if (!dmlUniqueIdentifier.equals(other.dmlUniqueIdentifier))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "DmlBaseEntity [dmlUniqueIdentifier=" + dmlUniqueIdentifier + ", dmlRevisionTimestamp=" + dmlRevisionTimestamp + "]";
  }
}
