package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.service.CacheService;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Vacon;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

import java.time.Instant;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/*
 * Pedaggi Europeo Telepass Definitivo
 */
@Service(VaconJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class VaconJob extends AbstractJob<VaconRecord, Vacon> {

  public final static String QUALIFIER = "vaconJob";

  private final transient Logger log = LoggerFactory.getLogger(getClass());
  private final NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService;

  @Autowired
  private CacheService cacheService;

  public VaconJob(final PlatformTransactionManager transactionManager,
                  final StanziamentiParamsValidator stanziamentiParamsValidator,
                  final NotificationService notificationService,
                  final RecordPersistenceService persistenceService,
                  final RecordDescriptor<VaconRecord> recordDescriptor,
                  final ConsumoProcessor<Vacon> consumoProcessor,
                  final RecordConsumoMapper<VaconRecord, Vacon> recordConsumoMapper,
                  final StanziamentiToNavPublisher stanziamentiToNavJmsProducer,
                  final NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {

    super("VACON",transactionManager, stanziamentiParamsValidator, notificationService, persistenceService,
          recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
    this.notConfirmedTemporaryAllocationsService = notConfirmedTemporaryAllocationsService;
  }

  @Override
  public Format getJobQualifier() {
    return Format.VACON;
  }

  @Override
  protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
    super.afterStanziamentiSent(sourceType, stanziamentiDaSpedireANAV);
    notConfirmedTemporaryAllocationsService.processSource("elceu", Instant.now());
  }

  @Override
  protected boolean isRecordToBeSkipped(String line, long detailIndex, VaconRecord record) {
    if (super.isRecordToBeSkipped(line, detailIndex, record)){
      return true;
    }

    if (VaconDescriptor.BEGIN_15.equals(record.getRecordCode())){
      Cache germanyCache = cacheService.getCache("VaconJob_Germany_Cache");
      String previousStoredRecordKey = germanyCache.get(record.getFileName() + "§" + record.getTripId(), String.class);
      if (previousStoredRecordKey == null){
        germanyCache.put(record.getFileName() + "§" + record.getTripId(), record.getFileName() + "§" + record.getTripId());
      } else {
        return true;
      }
    }
    return false;
  }
}
