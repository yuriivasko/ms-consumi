package it.fai.ms.consumi.job;

import java.nio.file.Path;

public class FileNameResolver {

  public FileNameResolver() {
  }

  public String resolveFileName(final Path _filePath) {
    return _filePath.normalize()
                    .toFile()
                    .getAbsolutePath();
  }

}
