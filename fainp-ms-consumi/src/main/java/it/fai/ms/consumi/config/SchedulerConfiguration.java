package it.fai.ms.consumi.config;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import it.fai.ms.consumi.job.ServiceProviderScheduledTask;
import it.fai.ms.consumi.repository.ServiceProviderRepository;

// TODO move in AsyncConfiguration ?
@Configuration
public class SchedulerConfiguration implements SchedulingConfigurer {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  @Autowired
  private ServiceProviderScheduledTask serviceProviderScheduledTask;

  @Autowired
  private ServiceProviderRepository spService;


  @Bean(destroyMethod = "shutdown")
  public Executor taskExecutor2() {
    return Executors.newScheduledThreadPool(100);
  }

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
    taskRegistrar.setScheduler(taskExecutor2());
    spService.findAll()
             .stream()
             .filter(serviceProvider -> serviceProvider.getCronExpression() != null)
             .forEach(serviceProviderWithCronExpression -> {
               _log.info("ServiceProvider {} cron expression : {}", serviceProviderWithCronExpression.getServiceName(),
                         serviceProviderWithCronExpression.getCronExpression());
               taskRegistrar.addCronTask(() -> {
                 try {
                   serviceProviderScheduledTask.run(serviceProviderWithCronExpression);
                 } catch (final IOException _e) {
                   _log.error("Failure processing  {}", serviceProviderWithCronExpression, _e);
                 }
               }, serviceProviderWithCronExpression.getCronExpression());
             });

  }
}