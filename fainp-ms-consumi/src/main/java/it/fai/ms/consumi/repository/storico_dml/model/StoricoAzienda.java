package it.fai.ms.consumi.repository.storico_dml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "storico_azienda")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoAzienda extends DmlBaseEntity {

  private static final long serialVersionUID = 1L;

  @Size(max = 255)
  @Column(name = "stato_azienda", length = 255)
  private String statoAzienda;

  public String getStatoAzienda() {
    return statoAzienda;
  }

  public void setStatoAzienda(String statoAzienda) {
    this.statoAzienda = statoAzienda;
  }

 
}
