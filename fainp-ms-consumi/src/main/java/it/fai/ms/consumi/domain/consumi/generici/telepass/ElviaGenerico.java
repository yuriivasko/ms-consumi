package it.fai.ms.consumi.domain.consumi.generici.telepass;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.generici.ConsumoGenerico;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

public class ElviaGenerico
    extends ConsumoGenerico {

  public ElviaGenerico(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public InvoiceType getInvoiceType() {
    return InvoiceType.D;
  }


  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final ElviaGenerico obj = getClass().cast(_obj);
      res = super.equals(_obj);
    }
    return res;
  }


  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode());
  }

  @Override
  public Instant getDate() {
    return this.getEndDate();
  }
}
