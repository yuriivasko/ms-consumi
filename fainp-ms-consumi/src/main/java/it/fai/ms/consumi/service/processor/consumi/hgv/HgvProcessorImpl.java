package it.fai.ms.consumi.service.processor.consumi.hgv;

import java.util.Optional;

import javax.inject.Inject;
import javax.money.MonetaryAmount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.generico.DettaglioStanziamentoGenericoProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class HgvProcessorImpl
  extends ConsumoAbstractProcessor implements HgvProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoGenericoProcessor                  dettaglioStanziamentoGenericoProcessor;
  private final HgvConsumoDettaglioStanziamentoGenricoMapper mapper;

  @Inject
  public HgvProcessorImpl(final ConsumoValidatorService _consumoValidatorService, final DettaglioStanziamentoGenericoProcessor _stanziamentiProducer,
                          final HgvConsumoDettaglioStanziamentoGenricoMapper _mapper,
                          final VehicleService _vehicleService, final CustomerService _customerService) {
    super(_consumoValidatorService,  _vehicleService, _customerService);
    dettaglioStanziamentoGenericoProcessor = _stanziamentiProducer;
    mapper = _mapper;
  }

  @Override
  public ProcessingResult validateAndProcess(final Hgv _elvia, final Bucket _bucket) {
    return executeProcess(_elvia, _bucket);
  }

  private ProcessingResult executeProcess(final Consumo _consumo, final Bucket _bucket) {

    _log.info(">");
    _log.info("> processing start {}", _bucket.getFileName());
    _log.info("  --> {}", _consumo);

    var processingResult = validateAndNotify(_consumo);

    _log.debug(" - {} processing validation completed", _consumo);

    final var consumo = processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
                      .ifPresent(stanziamentiParams -> {

                        final Amount consumoAmount = consumo.getAmount();
                        final MonetaryAmount amountExcludedVat = consumoAmount.getAmountExcludedVat();
                        setVatRateIfNotPresent(consumo, Optional.of(amountExcludedVat), Optional.empty(), stanziamentiParams);

                        final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
                        final var stanziamenti = dettaglioStanziamentoGenericoProcessor.produce((DettaglioStanziamentoGenerico) dettaglioStanziamento, stanziamentiParams);
                        processingResult.getStanziamenti().addAll(stanziamenti);
                      });
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }

    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", _consumo.getSource());
    _log.info("<");

    return processingResult;
  }

  private DettaglioStanziamento mapConsumoToDettaglioStanziamento(final Consumo _consumo) {
    return mapConsumoToDettaglioStanziamento(_consumo, mapper);
  }

}
