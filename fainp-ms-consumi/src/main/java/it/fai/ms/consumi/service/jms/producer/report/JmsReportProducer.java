package it.fai.ms.consumi.service.jms.producer.report;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.consumi.report.ReportToGenerateDTO;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;

@Service
@Transactional
public class JmsReportProducer {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  public JmsReportProducer(final JmsProperties _jmsProperties) {
    jmsProperties = _jmsProperties;
  }

  public void sendMessage(Long keyJob, String fileName, String dataReportCsv) {
    ReportToGenerateDTO dto = new ReportToGenerateDTO(keyJob, fileName, dataReportCsv);
    if(log.isTraceEnabled())
    	log.trace("Send DTO: {} to generate Report", dto);
    JmsQueueSenderUtil.publish(jmsProperties, JmsQueueNames.REPORT_FATTURE, dto);
  }

}
