package it.fai.ms.consumi.client;

import static it.fai.ms.consumi.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import it.fai.ms.common.jms.dto.vehicle.CentroDiCostroSearchDTO;

@FeignClient(name = "faivehicle")
public interface VehicleClient {

  String BASE_PATH = "/api/public";

  @GetMapping(BASE_PATH + "/centrodicosto")
  public String getCentroDiCosto(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken, @RequestBody CentroDiCostroSearchDTO dto);

}
