package it.fai.ms.consumi.domain;

import it.fai.ms.consumi.domain.consumi.DateSupplier;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;

public interface VehicleAssociatedWithDeviceSupplier extends VehicleSupplier, DeviceSupplier, DateSupplier {

}
