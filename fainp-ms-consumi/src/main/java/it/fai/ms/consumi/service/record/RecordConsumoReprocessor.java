package it.fai.ms.consumi.service.record;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.service.processor.SingleRecordProcessingResult;

public interface RecordConsumoReprocessor {
  <T extends CommonRecord, K extends Consumo> SingleRecordProcessingResult processSingleRecord(T foundRecord);
}
