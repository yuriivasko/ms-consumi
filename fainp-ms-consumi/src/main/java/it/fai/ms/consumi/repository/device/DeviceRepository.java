package it.fai.ms.consumi.repository.device;

import java.time.Instant;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;

public interface DeviceRepository {

  Optional<Device> findDeviceBySeriale(@NotNull String _deviceSeriale, @NotNull TipoDispositivoEnum _devicetype, String _contractNumber);

  Optional<Device> findDeviceByLicensePlate(String _licensePlate, TipoDispositivoEnum _deviceType, Instant _associationDate);

  Optional<Device> findDeviceByPan(String _pan, TipoServizioEnum _servicetype, String _contractNumber);

  Optional<StoricoDispositivo> findDeviceAtDate(String deviceID, Instant dataRiferimentoInizio);
}
