package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.tokheim;

import java.util.Map;

import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0201;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0301;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0302;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Service
public class TokheimRecordDescriptor implements RecordDescriptor<TokheimRecord> {

  private static final Logger log = LoggerFactory.getLogger(TokheimRecordDescriptor.class);

  // Descrittore Header
  public final static String         HEADER_CODE_BEGIN = "0101";
  public final static FileDescriptor header            = new FileDescriptor();
  static {
    int start = 0;
    header.addItem("recordCode", 1, start += 4);
    header.skipItem("fileNumber", 5, start += 6);
    header.skipItem("filler1", 11, start += 1);
    header.addItem("creationDateInFile", 12, start += 8);
    header.skipItem("tipoCreazione", 20, start += 1);
    header.skipItem("nomeFornitore", 21, start += 20);
    header.skipItem("filler2", 41, start += 13);
    header.skipItem("empty", 54, start += 146);
  }

  // Descrittore Footer
  public final static String         FOOTER_CODE_BEGIN = "0109";
  static final Map<Integer, Integer> startEndPosTotalRows;
  public final static FileDescriptor footer            = new FileDescriptor();
  static {
    int start = 0;
    footer.addItem("recordCode", 1, start += 4);
    // (aaaammggoomiss)
    footer.skipItem("fileNumber", 5, start += 6);
    footer.addItem("recordNumber",  11, start += 12);
    footer.skipItem("filler1", 23, start += 33);
    footer.skipItem("empty", 56, start += 144);

    startEndPosTotalRows = RecordDescriptor.getStartEndForFields(footer, "recordNumber");
  }

  public final static FileDescriptor record0201            = new FileDescriptor();
  static {
    int start = 0;
    record0201.addItem("recordCode", 1, start += 4);
    record0201.addItem("fileNumber", 5, start += 6);
    record0201.skipItem("filler1", 11, start += 1);
    record0201.addItem("NrProgressivo", 12, start += 6);
    record0201.skipItem("filler2", 18, start += 18);
    record0201.addItem("nomePoint", 36, start += 22);
    record0201.addItem("cittaPoint", 58, start += 24);
    record0201.addItem("capPoint", 82, start += 6);
    record0201.addItem("indirizzoPoint", 88, start += 20);
    start+=10;
    record0201.addItem("codiceEsternoPoint", 118, start += 15);
    record0201.skipItem("empty", 133, start += 67);
  }

  public final static FileDescriptor record0209            = new FileDescriptor();
  static {
    int start = 0;
    record0209.addItem("recordCode", 1, start += 4);
    record0209.addItem("fileNumber", 5, start += 6);
    record0209.skipItem("filler1", 11, start += 1);
    record0209.addItem("NrProgressivo", 12, start += 6);
    record0209.skipItem("filler2", 18, start += 182);
  }

  public final static FileDescriptor record0301            = new FileDescriptor();
  static {
    int start = 0;
    record0301.addItem("recordCode", 1, start += 4);
    record0301.skipItem("filler1", 5, start += 16);
    //AAAAMMGG
    record0301.addItem("dataErogazione", 21, start += 8);
    //HHMMSS
    record0301.addItem("oraErogazione", 29, start += 6);
    record0301.addItem("nrTransazione1", 35, start += 4);
    record0301.addItem("nrTrackycard", 39, start += 19);
    record0301.addItem("segnoImponibile", 28, start += 1);
    record0301.skipItem("filler2", 58, start += 8);
    start = 88;
    record0301.addItem("tipoTransazione", 89, start += 2);
    record0301.addItem("nrTransazione2", 91, start += 9);
    record0301.skipItem("filler3", 100, start += 8);
    record0301.addItem("valuta", 108, start += 3);
    record0301.skipItem("filler4", 111, start += 89);
  }

  public final static FileDescriptor record0302            = new FileDescriptor();
  static {
    int start = 0;
    record0302.addItem("recordCode", 1, start += 4);
    record0302.addItem("NrErogatore", 5, start += 2);
    record0302.addItem("codiceArticoloFornitore", 7, start += 4);
    record0302.addItem("segno", 11, start += 1);
    record0302.addItem("quantita", 12, start += 6);
    record0302.addItem("segnoUnitario", 18, start += 1);
    record0302.addItem("prezzoUnitario", 19, start += 9);
    record0302.addItem("segnoImponibile", 28, start += 1);
    record0302.addItem("imponibile", 29, start += 8);
    record0302.addItem("percentualeIva", 37, start += 4);
    record0302.addItem("segnoImposta", 41, start += 1);
    record0302.addItem("imposta", 42, start += 8);
    record0302.skipItem("filler1", 50, start += 150);
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String extractRecordCode(String _data) {
    return _data.substring(0, 4);
  }


  @Override
  public String getHeaderBegin() {
    return HEADER_CODE_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  @Override
  public  boolean skipWellKnownRecordCodeToIgnore(String recordCode){
    return "0209".equals(recordCode);
  }

  @Override
  public TokheimRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String _fileName, long _rowNumber) {
    TokheimRecord entity = null;
    switch (_recordCode) {
      case HEADER_CODE_BEGIN:
        entity = new TokheimRecord(_fileName, _rowNumber);
        entity.setFromString(header.getItems(), _data);
        break;
      case "0201":
        entity = new TokheimRecord0201(_fileName, _rowNumber);
        entity.setFromString(record0201.getItems(), _data);
        break;
      case "0209":
        log.warn("RecordCode {} not supported", _recordCode);
        break;
      case "0301":
        entity = new TokheimRecord0301(_fileName, _rowNumber);
        entity.setFromString(record0301.getItems(), _data);
        break;
      case "0302":
        entity = new TokheimRecord0302(_fileName, _rowNumber);
        entity.setFromString(record0302.getItems(), _data);
        break;
      case FOOTER_CODE_BEGIN:
        entity = new TokheimRecord(_fileName, _rowNumber);
        entity.setFromString(footer.getItems(), _data);
        break;
      default:
        log.warn("RecordCode {} not supported", _recordCode);
        break;
    }
    return entity;
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(TokheimRecordDescriptor.class, TokheimRecord.class).checkConfiguration();
  }

}
