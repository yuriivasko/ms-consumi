package it.fai.ms.consumi.domain.consumi.carburante;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Optional;

public class TokheimConsumo extends ConsumoCarburante {

  public TokheimConsumo(Source _source, String _recordCode, Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public InvoiceType getInvoiceType() {
    // Il valore verrà impostato a valle del processo, durante la scrittura del dettaglio stanziamento
    return null;
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }

}
