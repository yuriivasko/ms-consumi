package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class ElviaDescriptor implements RecordDescriptor<ElviaRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(ElviaDescriptor.class);

  public final static String BEGIN_DT          = "DT";
  public final static String HEADER_BEGIN      = "HR";
  public final static String FOOTER_CODE_BEGIN = "TR";

  // Header
  private final static FileDescriptor descriptorHR = new FileDescriptor();

  static {
    descriptorHR.addItem("recordCode", 1, 2);
    descriptorHR.addItem("creationDateInFile", 12, 21);
    descriptorHR.skipItem("receivingCode", 27, 35);
    descriptorHR.skipItem("receivingNominative", 36, 95);
    descriptorHR.skipItem("currencyCode", 96, 96);
  }

  public static FileDescriptor descriptorDT = new FileDescriptor();

  static {
    descriptorDT.addItem("recordCode", 1, 2);
    descriptorDT.addItem("partnerCode", 3, 11);
    descriptorDT.addItem("subContractCode", 12, 20);
    descriptorDT.addItem("typeMovement", 21, 22); // --> transactionType
    descriptorDT.addItem("dateMovement", 23, 32); // --> date
    descriptorDT.addItem("hourMovement", 33, 40); // --> time
    descriptorDT.addItem("originalAmount", 41, 50); // --> totalAmount
    descriptorDT.addItem("amountNotSubjectedToVat", 51, 60);
    descriptorDT.addItem("discount", 61, 70);
    descriptorDT.addItem("typeSupport", 71, 72); // --> deviceType
    descriptorDT.addItem("supportDescription", 73, 92);
    descriptorDT.addItem("supportCode", 93, 112); // --> deviceCode //sembra il pan vedi: ElviaRecordConsumoMapper -> device.setPan(supportCode);
    descriptorDT.addItem("movementDescription", 113, 148); // --> description
    descriptorDT.addItem("tollClass", 149, 150); // --> pricingClass
    descriptorDT.addItem("dateOfDebit", 151, 160);
    descriptorDT.addItem("signOfTheAmount", 161, 161); // -->signOfTheTransaction
    descriptorDT.addItem("networkCode", 162, 163); // --> netCode
    descriptorDT.addItem("entryGateCode", 164, 167);
    descriptorDT.addItem("exitGateCode", 168, 171);
    descriptorDT.addItem("dateOfInvoice", 172, 179);
    descriptorDT.addItem("vatDescription", 180, 189);
    descriptorDT.addItem("vatRate", 190, 193);
    descriptorDT.addItem("routeID", 194, 198);
    descriptorDT.addItem("vatCode", 199, 199);
  }

  // Trailer
  static final FileDescriptor descriptorTR = new FileDescriptor();
  static final String totalNumberOfRecordsDetailInDebt = "totalNumberOfRecordsDetailInDebt";
  static final String totalNumberOfRecordsDetailInAccredit = "totalNumberOfRecordsDetailInAccredit";
  static {
    descriptorTR.addItem("recordCode", 1, 2);
    descriptorTR.addItem("fileCreationDate", 12, 21);
    descriptorTR.addItem("amountTotalInDebt", 22, 36);
    descriptorTR.addItem("totalNumberOfRecordsDetailInDebt", 37, 48);
    descriptorTR.addItem("amountTotalMovimentInAccredit", 49, 63);
    descriptorTR.addItem("totalNumberOfRecordsDetailInAccredit", 64, 75);
  }

  public ElviaRecord decodeRecordCodeAndCallSetFromString(final String _data, final String _recordCode, String fileName, long rowNumber) {
    var entity = new ElviaRecord( fileName,  rowNumber);
    switch (_recordCode) {

    case HEADER_BEGIN:
      entity.setFromString(ElviaDescriptor.descriptorHR.getItems(), _data);
      break;

    case BEGIN_DT:
      entity.setFromString(ElviaDescriptor.descriptorDT.getItems(), _data);
      break;

    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();

    Optional<StringItem> stringItem = ElviaDescriptor.descriptorTR.getItem(ElviaDescriptor.totalNumberOfRecordsDetailInDebt);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in ElviaDescriptor!");
    }

    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    stringItem = ElviaDescriptor.descriptorTR.getItem(ElviaDescriptor.totalNumberOfRecordsDetailInAccredit);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in ElviaDescriptor!");
    }

    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return ElviaDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return ElviaDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 2;
  }

  public void checkConfiguration(){
    new RecordDescriptorChecker(ElviaDescriptor.class, ElviaRecord.class).checkConfiguration();
  }

}
