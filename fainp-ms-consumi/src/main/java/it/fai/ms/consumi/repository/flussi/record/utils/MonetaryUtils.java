package it.fai.ms.consumi.repository.flussi.record.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

public final class MonetaryUtils {
  
  public static final String DEFAULT_CURRENCY_CODE = "EUR";
  public static final CurrencyUnit DEFAULT_CURRENCY_UNIT = Monetary.getCurrency(DEFAULT_CURRENCY_CODE);
  
  public static CurrencyUnit getCurrencyByIsoNumber(String currencyIsoNumber){
    if (currencyIsoNumber!=null) {
      int isoNumericCode = Integer.parseInt(currencyIsoNumber);
      return Monetary.getCurrencies().stream()
          .filter(c -> c.getNumericCode() == isoNumericCode)
          .findAny()
          .orElse(null);
    }
    return null;
  }
  
  public static MonetaryAmount strToMonetary(String value, String currencyCode) {
    return ((value == null) || value.trim()
        .isEmpty() ? Money.of(0, "EUR") : Money.of(NumericUtils.strToBigDecimal(value.trim()), currencyCode));
  }
  
  public static MonetaryAmount strToMonetary(String value, int scaleRoundUnaware, String currencyCode) {
    return ((value == null) || value.trim()
                                    .isEmpty() ? Money.of(0, "EUR") : Money.of(NumericUtils.strToBigDecimal(value.trim(), scaleRoundUnaware), currencyCode));
  }
  
  public static MonetaryAmount strToMonetaryIsoNumeric(String value, int scale, String currencyCodeIsoNumeric) {
    return ((value == null) || value.trim().isEmpty() ? Money.of(0, "EUR") :
      Money.of(NumericUtils.strToBigDecimal(value.trim(), scale), getCurrencyByIsoNumber(currencyCodeIsoNumeric)));
  }

  public static MonetaryAmount strToMonetaryIsoNumeric(String value, int scale, String currencyCodeIsoNumeric, int scalePostParsing) {
    return ((value == null) || value.trim().isEmpty() ? Money.of(0, "EUR") :
      Money.of(NumericUtils.strToBigDecimal(value.trim(), scale).setScale(scalePostParsing,RoundingMode.HALF_UP), getCurrencyByIsoNumber(currencyCodeIsoNumeric)));
  }


  public static MonetaryAmount amountNoVatFromAmountWithVat(final MonetaryAmount amountWithVat, final BigDecimal _vatPercentage) {
    return Monetary.getDefaultAmountFactory()
                   .setAmount(div(amountWithVat, _vatPercentage))
                   .create();
  }

  public static MonetaryAmount amountWithVatFromAmountNoVat(final MonetaryAmount _amountNoVat, final BigDecimal _vatPercentage) {
    return Monetary.getDefaultAmountFactory()
                   .setAmount(mul(_amountNoVat, _vatPercentage))
                   .create();
  }

  private static MonetaryAmount div(final MonetaryAmount _monetaryAmount, final BigDecimal _vatPercentage) {
    MonetaryAmount monetaryAmount = null;
    if (_vatPercentage != null && _monetaryAmount != null) {
      monetaryAmount = _monetaryAmount.divide((BigDecimal.valueOf(1)
                                                         .add((_vatPercentage.divide(NumericUtils.ONE_HUNDRED, 6, RoundingMode.CEILING)))));
    } else {
      monetaryAmount = _monetaryAmount;
    }
    return monetaryAmount;

  }
  

  private static MonetaryAmount mul(final MonetaryAmount _monetaryAmount, final BigDecimal _vatPercentage) {
    MonetaryAmount monetaryAmount = null;
    if (_vatPercentage != null && _monetaryAmount != null) {
      monetaryAmount = _monetaryAmount.multiply((BigDecimal.valueOf(1)
                                                           .add((_vatPercentage.divide(BigDecimal.valueOf(100), 6,
                                                                                       RoundingMode.CEILING)))));
    } else {
      monetaryAmount = _monetaryAmount;
    }
    return monetaryAmount;
  }

  private MonetaryUtils() {
    super();
  }

}
