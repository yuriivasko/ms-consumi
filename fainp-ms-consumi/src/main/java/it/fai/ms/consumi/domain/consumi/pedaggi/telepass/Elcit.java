package it.fai.ms.consumi.domain.consumi.pedaggi.telepass;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.SecondaryDeviceSupplier;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Objects;
import java.util.Optional;

public class Elcit
  extends ConsumoPedaggio
  implements SecondaryDeviceSupplier {

  private String description;

  public Elcit(final Source _source, final String _recordCode, final Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Elcit obj = getClass().cast(_obj);
      res = super.equals(_obj);
    }
    return res;
  }

  @Override
  public InvoiceType getInvoiceType() {
    return InvoiceType.P;
  }

  @Override
  public String getRouteName() {
    // TODO campo "description" splittabile. Inoltre aggiungere "IMPORTO ESENTE" se esente
    return description;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode());
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }

  @Override
  public Optional<Device> getSecondaryDevice() {
    return Optional.ofNullable(getVehicle())
            .map(vehicle -> vehicle.getLicensePlate())
            .map(vehicleLicensePlate -> vehicleLicensePlate.getLicenseId())
            .map(licensePlate -> {
              Device device = new Device(licensePlate, TipoDispositivoEnum.TELEPASS_ITALIANO);
              device.setServiceType(TipoServizioEnum.PEDAGGI_ITALIA);
              return device;
            });
  }
}
