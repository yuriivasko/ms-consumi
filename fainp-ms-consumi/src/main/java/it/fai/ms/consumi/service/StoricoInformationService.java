package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Vehicle;

@Service
@Transactional
public class StoricoInformationService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ContractService contractService;
  private final DeviceService   deviceService;
  private final VehicleService  vehicleService;

  public StoricoInformationService(final ContractService contractService, final DeviceService deviceService,
                                   final VehicleService vehicleService) {
    this.contractService = contractService;
    this.deviceService = deviceService;
    this.vehicleService = vehicleService;
  }

  public Optional<Device> findDevice(String serialNumber, TipoDispositivoEnum deviceType) {
    log.debug("Find device by seriale {} and type {}", serialNumber, deviceType);
    return deviceService.findDeviceBySeriale(serialNumber, TipoDispositivoEnum.TRACKYCARD, null);
  }

  public Optional<Contract> findByContract(String contractNumber) {
    return contractService.findContractByNumber(contractNumber);
  }

  public Optional<Vehicle> findVehicle(Device device, Instant time, String fareClass) {
    return vehicleService.newVehicleByVehicleUUID_WithFareClass(device, time, fareClass);
  }

}
