package it.fai.ms.consumi.service.consumer;

import static it.fai.ms.consumi.service.jms.model.StanziamentoMessage.extractCodes;
import static it.fai.ms.consumi.service.jms.model.StanziamentoMessage.now;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.client.StanziamentiPostException;
import it.fai.ms.consumi.domain.navision.Stanziamento;
import it.fai.ms.consumi.domain.navision.StanziamentoError;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.scheduler.jobs.StanziamentoDTOMapper;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;
import it.fai.ms.consumi.service.jms.model.StanziamentoToNavMapper;
import it.fai.ms.consumi.service.navision.StanziamentiToNavService;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;

@Service
@Transactional(noRollbackFor=StanziamentiPostException.class, isolation=Isolation.READ_UNCOMMITTED)
public class StanziamentiToNavConsumer {

  private final StanziamentiToNavService stanziamentiToNavService;
  private final StanziamentoRepository   stanziamentoRepository;
  private final StanziamentoToNavMapper  stanziamentoToNavMapper;
  private final StanziamentiToNavPublisher stanziamentiToNavJmsProducer;

  private Logger log = LoggerFactory.getLogger(getClass());

  public StanziamentiToNavConsumer(final StanziamentiToNavService _stanziamentiToNavService,
                                   final StanziamentoToNavMapper _stanziamentoToNavMapper,
                                   final StanziamentoRepository _stanziamentoRepository,
                                   final StanziamentiToNavPublisher _stanziamentiToNavJmsProducer) {
    stanziamentiToNavService = _stanziamentiToNavService;
    stanziamentoToNavMapper = _stanziamentoToNavMapper;
    stanziamentoRepository = _stanziamentoRepository;
    stanziamentiToNavJmsProducer = _stanziamentiToNavJmsProducer;
  }

  public void consume(final List<StanziamentoMessage> _stanziamentoMessages) {
    List<StanziamentoMessage> sentMessages = sendToNav(_stanziamentoMessages);
    updateDateNavSent(sentMessages);
  }

  private List<Stanziamento> convertToStanziamentoToNav(final List<StanziamentoMessage> _stanziamentoMessages) {
    return Optional.ofNullable(_stanziamentoMessages)
                   .orElse(Collections.emptyList())
                   .stream()
                   .map(stanziamentoMessage -> stanziamentoToNavMapper.toDomainNav(stanziamentoMessage))
                   .collect(toList());
  }

  private List<StanziamentoMessage> sendToNav(final List<StanziamentoMessage> _stanziamentoMessages) {
    try {
      stanziamentiToNavService.notifyStanziamentoToNav(convertToStanziamentoToNav(_stanziamentoMessages));
      return _stanziamentoMessages;
    } catch (StanziamentiPostException e) {
      return errorHandler(e.getStanziamentoError(), _stanziamentoMessages);
    }
  }

  private void updateDateNavSent(final List<StanziamentoMessage> _stanziamentoMessages) {
    stanziamentoRepository.updateDateNavSent(extractCodes(_stanziamentoMessages), now());
  }

  public List<StanziamentoMessage> mapStanziamenti(List<it.fai.ms.consumi.domain.stanziamento.Stanziamento> stanziamenti) {
    return new StanziamentoDTOMapper().mapToStanziamentoDTO(stanziamenti);
  }

  private List<StanziamentoMessage> errorHandler(StanziamentoError stanziamentoError, List<StanziamentoMessage> righe) {
    if(righe.size()<2) {
      throw new RuntimeException("NoHandler: Errore nell'invio a Nav dello stanziamento "+stanziamentoError+" \n "+righe);
    }
    
    log.warn("Handling: Errore nell'invio a Nav dello stanziamento "+stanziamentoError);
    
    StanziamentoMessage toError = null;
    List<StanziamentoMessage> sentMessages = new ArrayList<>();
    List<StanziamentoMessage> toResend = new ArrayList<>();
    for (StanziamentoMessage stanziamentoMessage : righe) {
      //se ho trovato lo stanziamento in errore, devo reinviare tutti gli stanziamenti dopo quello di errore (si suppone che quelli prima siano stati correttamente gestiti da Nav)
      if(toError!= null) {
        toResend.add(stanziamentoMessage);
      }
      
      // cerco lo stanziamento in errore
      if(stanziamentoMessage.getCode().equals(stanziamentoError.getCodiceStanziamento())) {
        toError = stanziamentoMessage;
      }
      
      // finchè non trovo l'errore, gli stanziamenti sono stati inviati con successo
      if(toError== null) {
        sentMessages.add(stanziamentoMessage);
      }
    }
    
    // reinvio gli stanziamenti non processati da Nav
    stanziamentiToNavJmsProducer.publish(toResend);

    // reinvio il singolo stanziamento andato in errore, se l'errore persiste andrà a finire in deadletter
    stanziamentiToNavJmsProducer.publish(Arrays.asList(toError));
    
    return sentMessages;
  }
}
