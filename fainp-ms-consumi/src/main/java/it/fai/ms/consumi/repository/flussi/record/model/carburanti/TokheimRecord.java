package it.fai.ms.consumi.repository.flussi.record.model.carburanti;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;

import java.util.UUID;

public  class TokheimRecord extends CommonRecord implements GlobalIdentifierBySupplier{

  private static final long serialVersionUID = -8130388473640120399L;
  private String globalIdentifier;

  @JsonCreator
  public TokheimRecord(@JsonProperty("fileName") String fileName, @JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID()
        .toString();
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }
 

}
