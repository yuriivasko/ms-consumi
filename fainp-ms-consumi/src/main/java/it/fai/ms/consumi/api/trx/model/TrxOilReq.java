package it.fai.ms.consumi.api.trx.model;

import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * TrxOilReq
 */
@ApiModel(value="TrxOilReq", description="Oil Request Schema",parent=TrxBasicReq.class)
public class TrxOilReq extends TrxBasicReq  {

  @JsonProperty("unit")
  private String unit = null;

  @JsonProperty("supplyDateTime")
  private OffsetDateTime supplyDateTime = null;

  @JsonProperty("pump")
  private String pump = null;

  public TrxOilReq unit(String unit) {
    this.unit = unit;
    return this;
  }

  /**
   * Code identifying the quantity's unit measure. 'LT' = Litres and 'KG' = Kilograms.
   * @return unit
  **/
  @ApiModelProperty(required = true, value = "Code identifying the quantity's unit measure. 'LT' = Litres and 'KG' = Kilograms.")
  @NotNull


  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public TrxOilReq supplyDateTime(OffsetDateTime supplyDateTime) {
    this.supplyDateTime = supplyDateTime;
    return this;
  }

  /**
   * Data erogazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z
   * @return supplyDateTime
  **/
  @ApiModelProperty(required = true, value = "Data erogazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z")
  @NotNull

  @Valid

  public OffsetDateTime getSupplyDateTime() {
    return supplyDateTime;
  }

  public void setSupplyDateTime(OffsetDateTime supplyDateTime) {
    this.supplyDateTime = supplyDateTime;
  }

  public TrxOilReq pump(String pump) {
    this.pump = pump;
    return this;
  }

  /**
   * Pump number or code used for the refueling operation.
   * @return pump
  **/
  @ApiModelProperty(value = "Pump number or code used for the refueling operation.")


  public String getPump() {
    return pump;
  }

  public void setPump(String pump) {
    this.pump = pump;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrxOilReq trxOilReq = (TrxOilReq) o;
    return Objects.equals(this.unit, trxOilReq.unit) &&
        Objects.equals(this.supplyDateTime, trxOilReq.supplyDateTime) &&
        Objects.equals(this.pump, trxOilReq.pump) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(unit, supplyDateTime, pump, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TrxOilReq {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    unit: ").append(toIndentedString(unit)).append("\n");
    sb.append("    supplyDateTime: ").append(toIndentedString(supplyDateTime)).append("\n");
    sb.append("    pump: ").append(toIndentedString(pump)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

