package it.fai.ms.consumi.adapter.elasticsearch;

import feign.Headers;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//@FeignClient(name = "es-consumi", url = "${es-consumi.ribbon.listOfServers}", configuration= ConsumiClientConfig.class)
public interface ElasticSearchFeignClient {

  @Headers("Content-InvoiceType: application/json")
  @RequestMapping(method = RequestMethod.PUT, path = "/consumi/{recordType}/{id}")
  void create(@PathVariable("id") String id, @PathVariable("recordType") String recordType, ElasticSearchRecord<? extends CommonRecord> record);

  //@Headers("Content-InvoiceType: application/json")
  //@RequestMapping(method = RequestMethod.GET, path = "/consumi/{type}/{uuid}/_source")
  @GetMapping("/consumi/{type}/{uuid}/_source")
  ElasticSearchRecord<? extends CommonRecord> getRecord(@PathVariable("type") String type, @PathVariable("uuid") String uuid);
}

