package it.fai.ms.consumi.repository.viewallegati;

import static java.util.stream.Collectors.toSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ViewAllegatoGenericoSezione2RepositoryImpl implements ViewAllegatoGenericoSezione2Repository {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final EntityManager em;

  public ViewAllegatoGenericoSezione2RepositoryImpl(final EntityManager _em) {
    em = _em;
  }


  @Override
  public List<ViewAllegatoGenericoSezione2Entity> findByDettaglioStanziamentoId(List<UUID> dettagliStanziamentoId) {
    final var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ViewAllegatoGenericoSezione2Entity.class);
    Root<ViewAllegatoGenericoSezione2Entity> root = cq.from(ViewAllegatoGenericoSezione2Entity.class);

    cq.select(root);
    Predicate pId = root.get(ViewAllegatoGenericoSezione2Entity_.ID)
      .in(dettagliStanziamentoId);
    cq.where(pId);

    List<Order> orderList = new ArrayList<>();
    orderList.add(cb.asc(root.get(ViewAllegatoGenericoSezione2Entity_.CODICE_CONTRATTO)));
    orderList.add(cb.asc(root.get(ViewAllegatoGenericoSezione2Entity_.TIPO_DISPOSITIVO)));
    orderList.add(cb.asc(root.get(ViewAllegatoGenericoSezione2Entity_.SERIAL_NUMBER)));

    cq.orderBy(orderList);

    TypedQuery<ViewAllegatoGenericoSezione2Entity> query = em.createQuery(cq);
    logQuery(query);
    return query.getResultList();
  }

  private void logQuery(TypedQuery<?> query) {
    log.info("Query: {}", query.unwrap(Query.class)
             .getQueryString());
  }

}
