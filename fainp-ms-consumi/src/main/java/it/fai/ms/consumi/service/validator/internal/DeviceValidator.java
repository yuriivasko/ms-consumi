package it.fai.ms.consumi.service.validator.internal;

import java.time.Duration;
import java.time.Instant;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.service.validator.TimeValidator;

public interface DeviceValidator extends TimeValidator {

	@Deprecated
  ValidationOutcome deviceIsAssociatedWithContract(Device device, Contract contract);

  ValidationOutcome findAndSetDevice(Device device);

  ValidationOutcome findAndSetDevice(Vehicle vehicle, Device device, Instant date);

  ValidationOutcome deviceWasActiveAndAssociatedWithContractInDate(String _deviceSeriale, TipoDispositivoEnum _tipoDispositivo,
                                                                   String _contractUuid, Instant date, Duration offset);

  ValidationOutcome deviceWasAssociatedWithContractInDate(String _deviceSeriale,
                                                          TipoDispositivoEnum _tipoDispositivo,
                                                          String _contractUuid,
                                                          Instant date,
                                                          Duration offset);

  void sendDispositiviBloccatiFattureMessage(Consumo _consumo, String deviceCode, TipoDispositivoEnum tipoDispositivo);

  ValidationOutcome serviceWasActiveInDate(Device device, Instant date, Duration offset);

  void sendDispositiviBloccatiFattureMessage(String _deviceSeriale, TipoDispositivoEnum _tipoDispositivo, Instant consumoDate,
                                             String companyCode, String groupArticlesNav, Float amountExcludedVat);

}
