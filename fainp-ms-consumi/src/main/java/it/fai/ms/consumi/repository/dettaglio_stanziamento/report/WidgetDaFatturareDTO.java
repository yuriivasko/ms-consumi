package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import java.io.Serializable;
import java.math.BigDecimal;

public class WidgetDaFatturareDTO implements Serializable {

  private static final long serialVersionUID = -3191434736383534998L;

  private BigDecimal amount;
  
  private String unitaDiMisura;
  
  public WidgetDaFatturareDTO() {
    
  }
  
  public WidgetDaFatturareDTO(BigDecimal amount, String unitaDiMisura) {
    this.amount = amount;
    this.unitaDiMisura = unitaDiMisura;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getUnitaDiMisura() {
    return unitaDiMisura;
  }

  public void setUnitaDiMisura(String unitaDiMisura) {
    this.unitaDiMisura = unitaDiMisura;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("WidgetDaFatturareDTO [amount=");
    builder.append(amount);
    builder.append(", unitaDiMisura=");
    builder.append(unitaDiMisura);
    builder.append("]");
    return builder.toString();
  }
  
}
