package it.fai.ms.consumi.service.processor.stanziamenti.pedaggi;

import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGeneratorInterface;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoMapper;

@Component
public class StanziamentoPedaggioMapper extends StanziamentoMapper {

  public StanziamentoPedaggioMapper(StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator) {
    super(stanziamentoCodeGenerator);
  }

}
