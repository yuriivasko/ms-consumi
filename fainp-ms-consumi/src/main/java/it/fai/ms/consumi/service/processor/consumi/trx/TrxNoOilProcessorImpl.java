package it.fai.ms.consumi.service.processor.consumi.trx;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.generici.trackycard.TrackyCardGenerico;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.consumi.trackygenerico.TrackyCardGenricoDettaglioStanziamentoMapper;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.generico.DettaglioStanziamentoGenericoProcessor;
import it.fai.ms.consumi.service.validator.TrxConsumoValidatorService;

@Service
public class TrxNoOilProcessorImpl
  extends ConsumoAbstractProcessor
  implements TrxNoOilProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoGenericoProcessor       dettaglioStanziamentoGenericoProcessor;
  private final TrackyCardGenricoDettaglioStanziamentoMapper mapper;

  @Inject
  public TrxNoOilProcessorImpl(final TrxConsumoValidatorService trackyCardGenericoValidatorService,
                                final DettaglioStanziamentoGenericoProcessor _dettaglioStanziamentoProcessor,
                                final TrackyCardGenricoDettaglioStanziamentoMapper _mapper,
                                final VehicleService _vehicleService,
                                final CustomerService _customerService) {

    super(trackyCardGenericoValidatorService,  _vehicleService, _customerService);
    dettaglioStanziamentoGenericoProcessor = _dettaglioStanziamentoProcessor;
    mapper = _mapper;
  }

  @Override
  public ProcessingResult validateAndProcess(final StanziamentiParams stanziamentiParams,
                                             final TrackyCardGenerico trackyCardGenerico,
                                             final Bucket bucket) {
    return executeProcess(stanziamentiParams, trackyCardGenerico, bucket);
  }

  private ProcessingResult executeProcess(final StanziamentiParams stanziamentiParams,
                                          final TrackyCardGenerico trackyCardGenerico,
                                          final Bucket bucket) {

    _log.info(">");
    _log.info("> processing start {}", bucket.getFileName());
    _log.info("  --> {}", trackyCardGenerico);

    var processingResult = validateAndNotify(trackyCardGenerico);
    trackyCardGenerico.setGroupArticlesNav(stanziamentiParams.getArticle().getGrouping());
    trackyCardGenerico.setNavSupplierCode(stanziamentiParams.getSupplier().getCode());

    _log.debug(" - {} processing validation completed", trackyCardGenerico);

    final var consumo = processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
      final var stanziamenti = dettaglioStanziamentoGenericoProcessor.produce(dettaglioStanziamento, stanziamentiParams);
      processingResult.getStanziamenti().addAll(stanziamenti);
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }

    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", trackyCardGenerico.getSource());
    _log.info("<");

    return processingResult;
  }

  private DettaglioStanziamentoGenerico mapConsumoToDettaglioStanziamento(final Consumo trackyCardGenerico) {
    var dettaglioStanziamento = (DettaglioStanziamentoGenerico) mapper.mapConsumoToDettaglioStanziamento(trackyCardGenerico);
    //FIXME verificare se deve fare questi settaggi.. Non credo.
    addVehicle(trackyCardGenerico, dettaglioStanziamento);
    addCustomer(trackyCardGenerico, dettaglioStanziamento); //Il Buyer è il nostro codice cliente
    _log.info("Mapped {}", dettaglioStanziamento);
    return dettaglioStanziamento;
  }

}
