package it.fai.ms.consumi.domain.navision;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-06-29T01:45:58.957+02:00")
public class Stanziamento {

  @JsonProperty("codice_Stanziamento")
  private String codiceStanziamento = null;

  /**
   * 0 = None, 1 = Provvisorio, 2 = Definitivo
   */
  public enum StatoStanziamentoEnum {

    NUMBER_0(0), NUMBER_1(1), NUMBER_2(2);

    private Integer value;

    StatoStanziamentoEnum(Integer value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatoStanziamentoEnum fromValue(String text) {
      for (StatoStanziamentoEnum stateEnum : StatoStanziamentoEnum.values()) {
        if (String.valueOf(stateEnum.value)
                  .equals(text)) {
          return stateEnum;
        }
      }
      return null;
    }
  }

  @JsonProperty("stato_Stanziamento")
  private StatoStanziamentoEnum statoStanziamento = null;

  /**
   * 0 = None, 1 = My-Fai, 2 = Traghetti, 3 = Accettazione domanda per sconti Italia, 4 = Liquidazione sconti Italia, 5
   * = Presentazione domanda rimborso IVA, 6 = Liquidazione rimborso IVA
   */
  public enum TipoFlussoEnum {

    NUMBER_0(0), NUMBER_1(1), NUMBER_2(2), NUMBER_3(3), NUMBER_4(4), NUMBER_5(5), NUMBER_6(6);

    private Integer value;

    TipoFlussoEnum(Integer value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TipoFlussoEnum fromValue(String text) {
      for (TipoFlussoEnum typeEnum : TipoFlussoEnum.values()) {
        if (String.valueOf(typeEnum.value)
                  .equals(text)) {
          return typeEnum;
        }
      }
      return null;
    }
  }

  @JsonProperty("tipo_flusso")
  private TipoFlussoEnum tipoFlusso = null;

  @JsonProperty("data_erogazione_servizio")
  private String dataErogazioneServizio = null;

  @JsonProperty("nr_fornitore")
  private String nrFornitore = null;

  @JsonProperty("nr_cliente")
  private String nrCliente = null;

  @JsonProperty("nr_articolo")
  private String nrArticolo = null;

  @JsonProperty("paese")
  private String paese = null;

  @JsonProperty("anno_Stanziamento")
  private Integer annoStanziamento = null;

  @JsonProperty("targa")
  private String targa = null;

  @JsonProperty("classe_veicolo_Euro")
  private String classeVeicoloEuro = null;

  @JsonProperty("data_registrazione")
  private String dataRegistrazione = null;

  @JsonProperty("prezzo")
  private Double prezzo = null;

  @JsonProperty("costo")
  private Double costo = null;

  @JsonProperty("quantità")
  private Double quantit = null;

  @JsonProperty("nr_")
  private Double nr_ = null;

  @JsonProperty("valuta")
  private String valuta = null;

  @JsonProperty("da_rifatturare")
  private Boolean daRifatturare = null;

  @JsonProperty("descrizione_aggiuntiva")
  private String descrizioneAggiuntiva = null;

  /**
   * 0 = None, 1 = Costo/Ricavo, 2 = Costo, 3 = Ricavo, 4 = Nota Credito
   */
  public enum GeneraStanziamentoEnum {

    NUMBER_0(0), NUMBER_1(1), NUMBER_2(2), NUMBER_3(3), NUMBER_4(4);

    private Integer value;

    GeneraStanziamentoEnum(Integer value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static GeneraStanziamentoEnum fromValue(String text) {
      for (GeneraStanziamentoEnum b : GeneraStanziamentoEnum.values()) {
        if (String.valueOf(b.value)
                  .equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("genera_Stanziamento")
  private GeneraStanziamentoEnum generaStanziamento = null;

  @JsonProperty("nr_Stanziamento")
  private String nrStanziamento = "";

  @JsonProperty("errore")
  private String errore = "";

  @JsonProperty("nr_Stanziamento_Passivo")
  private String nrStanziamentoPassivo = "";

  @JsonProperty("descrizione_aggiuntiva_2")
  private String descrizioneAggiuntiva2 = "";

  @JsonProperty("descrizione_aggiuntiva_3")
  private String descrizioneAggiuntiva3 = "";

  @JsonProperty("codice_Nota")
  private String codiceNota = "";

  @JsonProperty("storno")
  private String storno = "";

  @JsonProperty("conguaglio")
  private Boolean conguaglio = null;

  @JsonProperty("cdStanzProv")
  private String cdStanzProv = null;

  @JsonProperty("nrDocFor")
  private String nrDocFor = null;

  @JsonProperty("nazioneTarga")
  private String nazioneTarga = null;

  @JsonProperty("data_acquisizione_flusso")
  private String dataAcquisizioneFlusso = null;

  public Stanziamento codiceStanziamento(final String _code) {
    this.codiceStanziamento = _code;
    return this;
  }

  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Size(min = 0, max = 20)
  public String getCodiceStanziamento() {
    return codiceStanziamento;
  }

  public void setCodiceStanziamento(final String _code) {
    this.codiceStanziamento = _code;
  }

  public Stanziamento statoStanziamento(StatoStanziamentoEnum statoStanziamento) {
    this.statoStanziamento = statoStanziamento;
    return this;
  }

  @ApiModelProperty(required = true, value = "0 = None, 1 = Provvisorio, 2 = Definitivo")
  @NotNull
  public StatoStanziamentoEnum getStatoStanziamento() {
    return statoStanziamento;
  }

  public void setStatoStanziamento(StatoStanziamentoEnum statoStanziamento) {
    this.statoStanziamento = statoStanziamento;
  }

  public Stanziamento tipoFlusso(TipoFlussoEnum tipoFlusso) {
    this.tipoFlusso = tipoFlusso;
    return this;
  }

  /**
   * 0 = None, 1 = My-Fai, 2 = Traghetti, 3 = Accettazione domanda per sconti Italia, 4 = Liquidazione sconti Italia, 5
   * = Presentazione domanda rimborso IVA, 6 = Liquidazione rimborso IVA
   *
   * @return tipoFlusso
   **/
  @ApiModelProperty(
                    value = "0 = None, 1 = My-Fai, 2 = Traghetti, 3 = Accettazione domanda per sconti Italia, 4 = Liquidazione sconti Italia,   5 = Presentazione domanda rimborso IVA, 6 = Liquidazione rimborso IVA")
  public TipoFlussoEnum getTipoFlusso() {
    return tipoFlusso;
  }

  public void setTipoFlusso(TipoFlussoEnum tipoFlusso) {
    this.tipoFlusso = tipoFlusso;
  }

  public Stanziamento dataErogazioneServizio(String dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
    return this;
  }

  @ApiModelProperty(value = "")
  @Valid
  public String getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public void setDataErogazioneServizio(String dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }

  public Stanziamento nrFornitore(String nrFornitore) {
    this.nrFornitore = nrFornitore;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getNrFornitore() {
    return nrFornitore;
  }

  public void setNrFornitore(String nrFornitore) {
    this.nrFornitore = nrFornitore;
  }

  public Stanziamento nrCliente(String nrCliente) {
    this.nrCliente = nrCliente;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getNrCliente() {
    return nrCliente;
  }

  public void setNrCliente(String nrCliente) {
    this.nrCliente = nrCliente;
  }

  public Stanziamento nrArticolo(String nrArticolo) {
    this.nrArticolo = nrArticolo;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getNrArticolo() {
    return nrArticolo;
  }

  public void setNrArticolo(String nrArticolo) {
    this.nrArticolo = nrArticolo;
  }

  public Stanziamento paese(String paese) {
    this.paese = paese;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 10)
  public String getPaese() {
    return paese;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public Stanziamento annoStanziamento(Integer annoStanziamento) {
    this.annoStanziamento = annoStanziamento;
    return this;
  }

  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Integer getAnnoStanziamento() {
    return annoStanziamento;
  }

  public void setAnnoStanziamento(Integer annoStanziamento) {
    this.annoStanziamento = annoStanziamento;
  }

  public Stanziamento targa(String targa) {
    this.targa = targa;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getTarga() {
    return targa;
  }

  public void setTarga(String targa) {
    this.targa = targa;
  }

  public Stanziamento classeVeicoloEuro(String classeVeicoloEuro) {
    this.classeVeicoloEuro = classeVeicoloEuro;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getClasseVeicoloEuro() {
    return classeVeicoloEuro;
  }

  public void setClasseVeicoloEuro(String classeVeicoloEuro) {
    this.classeVeicoloEuro = classeVeicoloEuro;
  }

  public Stanziamento dataRegistrazione(String dataRegistrazione) {
    this.dataRegistrazione = dataRegistrazione;
    return this;
  }

  @ApiModelProperty(value = "")
  @Valid
  public String getDataRegistrazione() {
    return dataRegistrazione;
  }

  public void setDataRegistrazione(String dataRegistrazione) {
    this.dataRegistrazione = dataRegistrazione;
  }

  public Stanziamento prezzo(Double prezzo) {
    this.prezzo = prezzo;
    return this;
  }

  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Double getPrezzo() {
    return prezzo;
  }

  public void setPrezzo(Double prezzo) {
    this.prezzo = prezzo;
  }

  public Stanziamento costo(Double costo) {
    this.costo = costo;
    return this;
  }

  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Double getCosto() {
    return costo;
  }

  public void setCosto(Double costo) {
    this.costo = costo;
  }

  public Stanziamento quantit(Double quantit) {
    this.quantit = quantit;
    return this;
  }

  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Double getQuantit() {
    return quantit;
  }

  public void setQuantit(Double quantit) {
    this.quantit = quantit;
  }

  public Stanziamento nr_(Double nr_) {
    this.nr_ = nr_;
    return this;
  }

  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Double getNr_() {
    return nr_;
  }

  public void setNr_(Double nr_) {
    this.nr_ = nr_;
  }

  public Stanziamento valuta(String valuta) {
    this.valuta = valuta;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 10)
  public String getValuta() {
    return valuta;
  }

  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

  public Stanziamento daRifatturare(Boolean daRifatturare) {
    this.daRifatturare = daRifatturare;
    return this;
  }

  @ApiModelProperty(required = true, value = "")
  @NotNull
  public Boolean isDaRifatturare() {
    return daRifatturare;
  }

  public void setDaRifatturare(Boolean daRifatturare) {
    this.daRifatturare = daRifatturare;
  }

  public Stanziamento descrizioneAggiuntiva(String descrizioneAggiuntiva) {
    this.descrizioneAggiuntiva = descrizioneAggiuntiva;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 50)
  public String getDescrizioneAggiuntiva() {
    return descrizioneAggiuntiva;
  }

  public void setDescrizioneAggiuntiva(String descrizioneAggiuntiva) {
    this.descrizioneAggiuntiva = descrizioneAggiuntiva;
  }

  public Stanziamento generaStanziamento(GeneraStanziamentoEnum generaStanziamento) {
    this.generaStanziamento = generaStanziamento;
    return this;
  }

  @ApiModelProperty(value = "0 = None, 1 = Costo/Ricavo, 2 = Costo,   3 = Ricavo, 4 = Nota Credito")
  public GeneraStanziamentoEnum getGeneraStanziamento() {
    return generaStanziamento;
  }

  public void setGeneraStanziamento(GeneraStanziamentoEnum generaStanziamento) {
    this.generaStanziamento = generaStanziamento;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getNrStanziamento() {
    return nrStanziamento;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 250)
  public String getErrore() {
    return errore;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getNrStanziamentoPassivo() {
    return nrStanziamentoPassivo;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 50)
  public String getDescrizioneAggiuntiva2() {
    return descrizioneAggiuntiva2;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 50)
  public String getDescrizioneAggiuntiva3() {
    return descrizioneAggiuntiva3;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 10)
  public String getCodiceNota() {
    return codiceNota;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getStorno() {
    return storno;
  }

  public Stanziamento conguaglio(Boolean conguaglio) {
    this.conguaglio = conguaglio;
    return this;
  }

  @ApiModelProperty(value = "")
  public Boolean isConguaglio() {
    return conguaglio;
  }

  public void setConguaglio(Boolean conguaglio) {
    this.conguaglio = conguaglio;
  }

  public Stanziamento cdStanzProv(String cdStanzProv) {
    this.cdStanzProv = cdStanzProv;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getCdStanzProv() {
    return cdStanzProv;
  }

  public void setCdStanzProv(String cdStanzProv) {
    this.cdStanzProv = cdStanzProv;
  }

  public Stanziamento nrDocFor(String nrDocFor) {
    this.nrDocFor = nrDocFor;
    return this;
  }

  @ApiModelProperty(value = "")
  @Size(min = 0, max = 20)
  public String getNrDocFor() {
    return nrDocFor;
  }

  public void setNrDocFor(String nrDocFor) {
    this.nrDocFor = nrDocFor;
  }

  public Stanziamento nazioneTarga(String nazioneTarga) {
    this.nazioneTarga = nazioneTarga;
    return this;
  }

  /**
   * Get nazioneTarga
   * @return nazioneTarga
   **/
  @ApiModelProperty(value = "")

  @Size(min=0,max=10)
  public String getNazioneTarga() {
    return nazioneTarga;
  }

  public void setNazioneTarga(String nazioneTarga) {
    this.nazioneTarga = nazioneTarga;
  }

  public Stanziamento dataAcquisizioneFlusso(String dataAcquisizioneFlusso) {
    this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
    return this;
  }

  /**
   * Get dataAcquisizioneFlusso
   * @return dataAcquisizioneFlusso
   **/
  @ApiModelProperty(value = "")

  @Valid

  public String getDataAcquisizioneFlusso() {
    return dataAcquisizioneFlusso;
  }

  public void setDataAcquisizioneFlusso(String dataAcquisizioneFlusso) {
    this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Stanziamento stanziamento = (Stanziamento) o;
    return Objects.equals(this.codiceStanziamento, stanziamento.codiceStanziamento)
           && Objects.equals(this.statoStanziamento, stanziamento.statoStanziamento)
           && Objects.equals(this.tipoFlusso, stanziamento.tipoFlusso)
           && Objects.equals(this.dataErogazioneServizio, stanziamento.dataErogazioneServizio)
           && Objects.equals(this.nrFornitore, stanziamento.nrFornitore) && Objects.equals(this.nrCliente, stanziamento.nrCliente)
           && Objects.equals(this.nrArticolo, stanziamento.nrArticolo) && Objects.equals(this.paese, stanziamento.paese)
           && Objects.equals(this.annoStanziamento, stanziamento.annoStanziamento) && Objects.equals(this.targa, stanziamento.targa)
           && Objects.equals(this.classeVeicoloEuro, stanziamento.classeVeicoloEuro)
           && Objects.equals(this.dataRegistrazione, stanziamento.dataRegistrazione) && Objects.equals(this.prezzo, stanziamento.prezzo)
           && Objects.equals(this.costo, stanziamento.costo) && Objects.equals(this.quantit, stanziamento.quantit)
           && Objects.equals(this.nr_, stanziamento.nr_) && Objects.equals(this.valuta, stanziamento.valuta)
           && Objects.equals(this.daRifatturare, stanziamento.daRifatturare)
           && Objects.equals(this.descrizioneAggiuntiva, stanziamento.descrizioneAggiuntiva)
           && Objects.equals(this.generaStanziamento, stanziamento.generaStanziamento)
           && Objects.equals(this.nrStanziamento, stanziamento.nrStanziamento) && Objects.equals(this.errore, stanziamento.errore)
           && Objects.equals(this.nrStanziamentoPassivo, stanziamento.nrStanziamentoPassivo)
           && Objects.equals(this.descrizioneAggiuntiva2, stanziamento.descrizioneAggiuntiva2)
           && Objects.equals(this.descrizioneAggiuntiva3, stanziamento.descrizioneAggiuntiva3)
           && Objects.equals(this.codiceNota, stanziamento.codiceNota) && Objects.equals(this.storno, stanziamento.storno)
           && Objects.equals(this.conguaglio, stanziamento.conguaglio) && Objects.equals(this.cdStanzProv, stanziamento.cdStanzProv)
           && Objects.equals(this.nrDocFor, stanziamento.nrDocFor)
           && Objects.equals(this.nazioneTarga, stanziamento.nazioneTarga)
           && Objects.equals(this.dataAcquisizioneFlusso, stanziamento.dataAcquisizioneFlusso);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codiceStanziamento, statoStanziamento, tipoFlusso, dataErogazioneServizio, nrFornitore, nrCliente, nrArticolo,
                        paese, annoStanziamento, targa, classeVeicoloEuro, dataRegistrazione, prezzo, costo, quantit, nr_, valuta,
                        daRifatturare, descrizioneAggiuntiva, generaStanziamento, nrStanziamento, errore, nrStanziamentoPassivo,
                        descrizioneAggiuntiva2, descrizioneAggiuntiva3, codiceNota, storno, conguaglio, cdStanzProv, nrDocFor, nazioneTarga, dataAcquisizioneFlusso);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Stanziamento {\n");
    sb.append("    codiceStanziamento: ")
      .append(toIndentedString(codiceStanziamento))
      .append("\n");
    sb.append("    statoStanziamento: ")
      .append(toIndentedString(statoStanziamento))
      .append("\n");
    sb.append("    tipoFlusso: ")
      .append(toIndentedString(tipoFlusso))
      .append("\n");
    sb.append("    dataErogazioneServizio: ")
      .append(toIndentedString(dataErogazioneServizio))
      .append("\n");
    sb.append("    nrFornitore: ")
      .append(toIndentedString(nrFornitore))
      .append("\n");
    sb.append("    nrCliente: ")
      .append(toIndentedString(nrCliente))
      .append("\n");
    sb.append("    nrArticolo: ")
      .append(toIndentedString(nrArticolo))
      .append("\n");
    sb.append("    paese: ")
      .append(toIndentedString(paese))
      .append("\n");
    sb.append("    annoStanziamento: ")
      .append(toIndentedString(annoStanziamento))
      .append("\n");
    sb.append("    targa: ")
      .append(toIndentedString(targa))
      .append("\n");
    sb.append("    classeVeicoloEuro: ")
      .append(toIndentedString(classeVeicoloEuro))
      .append("\n");
    sb.append("    dataRegistrazione: ")
      .append(toIndentedString(dataRegistrazione))
      .append("\n");
    sb.append("    prezzo: ")
      .append(toIndentedString(prezzo))
      .append("\n");
    sb.append("    costo: ")
      .append(toIndentedString(costo))
      .append("\n");
    sb.append("    quantit: ")
      .append(toIndentedString(quantit))
      .append("\n");
    sb.append("    nr_: ")
      .append(toIndentedString(nr_))
      .append("\n");
    sb.append("    valuta: ")
      .append(toIndentedString(valuta))
      .append("\n");
    sb.append("    daRifatturare: ")
      .append(toIndentedString(daRifatturare))
      .append("\n");
    sb.append("    descrizioneAggiuntiva: ")
      .append(toIndentedString(descrizioneAggiuntiva))
      .append("\n");
    sb.append("    generaStanziamento: ")
      .append(toIndentedString(generaStanziamento))
      .append("\n");
    sb.append("    nrStanziamento: ")
      .append(toIndentedString(nrStanziamento))
      .append("\n");
    sb.append("    errore: ")
      .append(toIndentedString(errore))
      .append("\n");
    sb.append("    nrStanziamentoPassivo: ")
      .append(toIndentedString(nrStanziamentoPassivo))
      .append("\n");
    sb.append("    descrizioneAggiuntiva2: ")
      .append(toIndentedString(descrizioneAggiuntiva2))
      .append("\n");
    sb.append("    descrizioneAggiuntiva3: ")
      .append(toIndentedString(descrizioneAggiuntiva3))
      .append("\n");
    sb.append("    codiceNota: ")
      .append(toIndentedString(codiceNota))
      .append("\n");
    sb.append("    storno: ")
      .append(toIndentedString(storno))
      .append("\n");
    sb.append("    conguaglio: ")
      .append(toIndentedString(conguaglio))
      .append("\n");
    sb.append("    cdStanzProv: ")
      .append(toIndentedString(cdStanzProv))
      .append("\n");
    sb.append("    nrDocFor: ")
      .append(toIndentedString(nrDocFor))
      .append("\n");
    sb.append("    nazioneTarga: ").append(toIndentedString(nazioneTarga)).append("\n");
    sb.append("    dataAcquisizioneFlusso: ").append(toIndentedString(dataAcquisizioneFlusso)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString()
            .replace("\n", "\n    ");
  }

}
