package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti;

import java.util.Arrays;
import java.util.Currency;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;

@Service
public class DettaglioStanziamentoCaburantiDaConsumoProcessorImpl {

  private final PriceCalculatedByNavService priceCalculatedByNavClient;
  private final CurrencyRateService         currencyRateService;

  @Inject
  public DettaglioStanziamentoCaburantiDaConsumoProcessorImpl(PriceCalculatedByNavService _priceCalculatedByNavCService,
                                                              CurrencyRateService _currencyRateService) {
    priceCalculatedByNavClient = _priceCalculatedByNavCService;
    currencyRateService = _currencyRateService;
  }

  public List<DettaglioStanziamentoCarburante> process(DettaglioStanziamentoCarburante _dettaglioStanziamentoCarburante,
                                                       StanziamentiParams _stanziamentiParams) {

    Currency targetCurrency = _stanziamentiParams.getArticle()
                                                 .getCurrency();
    
    Amount amount = _dettaglioStanziamentoCarburante.getPriceReference();
    amount = currencyRateService.convertAmount(amount, targetCurrency, _dettaglioStanziamentoCarburante.getEntryDateTime());
    _dettaglioStanziamentoCarburante.setPriceReference(amount);

    // Costo -> 0 = Costo (passivo)
    if (_stanziamentiParams.isCosto()) {
      priceCalculatedByNavClient.setCostCalculatedAmountByNav(_dettaglioStanziamentoCarburante, _stanziamentiParams);
    }

    // Ricavo -> 1 = Ricavo (attivo)
    if (_stanziamentiParams.isRicavo()) {
      priceCalculatedByNavClient.setPriceCalculatedAmountByNav(_dettaglioStanziamentoCarburante, _stanziamentiParams);
    }

    _dettaglioStanziamentoCarburante.setInvoiceType(InvoiceType.D); // definitivo

    return Arrays.asList(_dettaglioStanziamentoCarburante);
  }

}
