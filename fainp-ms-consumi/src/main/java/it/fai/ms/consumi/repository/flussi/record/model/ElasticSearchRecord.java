package it.fai.ms.consumi.repository.flussi.record.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.Valid;
import java.time.Instant;

public class ElasticSearchRecord<T extends CommonRecord> {

  @Valid
  private String id;
  @Valid
  private T rawdata;
  @JsonIgnore
  private String     recordType;
  @Valid
  private String     fileName;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Europe/Rome")
  private Instant    ingestionTime;
  private String recordCode;
  private String transactionDetail;
  

  public String getId() {
    return id;
  }
  

  public Instant getIngestionTime() {
    return ingestionTime;
  }

  public void setIngestionTime(Instant ingestionTime) {
    this.ingestionTime = ingestionTime;
  }




  public String getTransactionDetail() {
    return transactionDetail;
  }

  public void setTransactionDetail(String transactionDetail) {
    this.transactionDetail = transactionDetail;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }

  public ElasticSearchRecord() {
  }

  public String getFileName() {
    return fileName;
  }


  public void setFileName(final String _fileName) {
    this.fileName = _fileName;
  }


  public T getRawdata() {
    return rawdata;
  }

  public void setRawdata(T rawdata) {
    if (rawdata != null) {
      this.id = rawdata.getUuid();
    }
    this.rawdata = rawdata;
  }

  public String getRecordType() {
    return recordType;
  }

  public void setRecordType(String recordType) {
    this.recordType = recordType;
  }

}
