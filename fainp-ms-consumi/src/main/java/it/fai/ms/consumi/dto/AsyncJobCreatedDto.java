package it.fai.ms.consumi.dto;

public class AsyncJobCreatedDto {
	private String job;
	
	
	public AsyncJobCreatedDto(String id) {
		job = id;
	}


	public String getJob() {
		return job;
	}


	public void setJob(String job) {
		this.job = job;
	}

	
}
