package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.FrejusRecordDescriptor;
import it.fai.ms.consumi.service.frejus.FrejusCustomerData;
import it.fai.ms.consumi.service.frejus.FrejusCustomerDataContainer;

import java.util.List;
import java.util.UUID;

/**
 * @author filippor Frejus - Montebianco
 */
public class FrejusRecord extends CommonRecord implements GlobalIdentifierBySupplier, FrejusCustomerDataContainer {

  private static final long serialVersionUID = 38248022704610596L;
  /**
   * Numero del Cliente (2+4)
   */
  String                    numeroCliente;
  /**
   * Codice ISO dell'emettitore SITAF (308134)
   */
  // String isoEmettitore;
  // /**
  // * Tipo Titolo (4:Tessera; 5: Buono)
  // */
  // String tipoTitolo;
  String                    segnoImportoPericolose;
  String                    importoPericoloseNoVat;
  String                    percIva;
  // /**
  // * Numero del Gruppo (1-99) (00: Cliente individuale)
  // */
  // String numeroGruppo;
  // /**
  // * Numero della Società di post-pagamento (1001-99999)
  // */
  // String numeroSocieta;
  /**
   * Numero di Tessera o Buono (1-99999)
   */
  String numeroTessera;
  // /**
  // * Carattere numerico di controllo (LUHN)
  // */
  // String controlChar;
  /**
   * Data, ora, minuti e secondi della transazione (aaaammggoomiss)
   */
  String dataTransazione;
  /**
   * Segno dell'importo del transito ("+" ou "-")
   */
  String segnoImporto;
  /**
   * Importo della transazione in centesimi di EURO
   */
  String importo;
  /**
   * Numero di fattura ("+": Fattura; "-": Nota di Credito)
   */
  String numeroFattura;
  /**
   * Numero d'ordine della società nel raggruppamento (1-99999) (00000 per i clienti individuali)
   */
  String numeroSocietaInGruppo;
  /**
   * Senso: 1 - Francia->Italia; 2 - Italia->Francia
   */
  String senso;
  /**
   * Modalità di transito (01 - Corsa semplice;  11 - Supplemento materie pericolose)
   */
  String modalitaTransito;
  /**
   * 0 - Pista manuale; 1 - Pista automatica
   */
  String pista;
  /**
   * Categoria del veicolo
   */
  String categoriaVeicolo;
  /**
   * Sottoclassifica ECO (utilizzo futuro)
   */
  String classificaECO;
  /**
   * Stazione del transito (25002999 - Frejus Francia; 38032193 - Frejus Italia)
   */
  String stazioneTransito;

  private String classePedaggio;

  private String globalIdentifier;

  private String numeroBuonoFrancia;


  @JsonCreator
  public FrejusRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID()
                           .toString();
  }


  public static final String       DEFAULT_CURRENCY = "EUR";

  private List<FrejusCustomerData> customerTrc;

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public String getNumeroCliente() {
    return numeroCliente;
  }

  public void setNumeroCliente(String numeroCliente) {
    this.numeroCliente = numeroCliente;
  }

  public String getNumeroTessera() {
    return numeroTessera;
  }

  public void setNumeroTessera(String numeroTessera) {
    this.numeroTessera = numeroTessera;
  }

  public String getDataTransazione() {
    return dataTransazione;
  }

  public void setDataTransazione(String dataTransazione) {
    this.dataTransazione = dataTransazione;
  }

  public String getSegnoImporto() {
    return segnoImporto;
  }

  public void setSegnoImporto(String segnoImporto) {
    this.segnoImporto = segnoImporto;
  }

  public String getImporto() {
    return importo;
  }

  public void setImporto(String importo) {
    this.importo = importo;
  }

  public String getNumeroFattura() {
    return numeroFattura;
  }

  public void setNumeroFattura(String numeroFattura) {
    this.numeroFattura = numeroFattura;
  }

  public String getNumeroSocietaInGruppo() {
    return numeroSocietaInGruppo;
  }

  public void setNumeroSocietaInGruppo(String numeroSocietaInGruppo) {
    this.numeroSocietaInGruppo = numeroSocietaInGruppo;
  }

  public String getSenso() {
    return senso;
  }

  public void setSenso(String senso) {
    this.senso = senso;
  }

  public String getModalitaTransito() {
    return modalitaTransito;
  }

  public void setModalitaTransito(String modalitaTransito) {
    this.modalitaTransito = modalitaTransito;
  }

  public String getPista() {
    return pista;
  }

  public void setPista(String pista) {
    this.pista = pista;
  }

  public String getCategoriaVeicolo() {
    return categoriaVeicolo;
  }

  public void setCategoriaVeicolo(String categoriaVeicolo) {
    this.categoriaVeicolo = categoriaVeicolo;
  }

  public String getClassificaECO() {
    return classificaECO;
  }

  public void setClassificaECO(String classificaECO) {
    this.classificaECO = classificaECO;
  }

  public String getStazioneTransito() {
    return stazioneTransito;
  }

  public void setStazioneTransito(String stazioneTransito) {
    this.stazioneTransito = stazioneTransito;
  }

  public String getDefaultCurrency() {
    return DEFAULT_CURRENCY;
  }

  public String getSegnoImportoPericolose() {
    return segnoImportoPericolose;
  }

  public void setSegnoImportoPericolose(String segnoImportoPericolose) {
    this.segnoImportoPericolose = segnoImportoPericolose;
  }

  public String getImportoPericoloseNoVat() {
    return importoPericoloseNoVat;
  }

  public void setImportoPericoloseNoVat(String importoPericolose) {
    this.importoPericoloseNoVat = importoPericolose;
  }

  public String getPercIva() {
    return percIva;
  }

  public void setPercIva(String percIva) {
    this.percIva = percIva;
  }

  public void setGlobalIdentifier(String globalIdentifier) {
    this.globalIdentifier = globalIdentifier;
  }

  public String getClassePedaggio() {
    return classePedaggio;
  }

  public void setClassePedaggio(String classePedaggio) {
    this.classePedaggio = classePedaggio;
  }

  @Override
  public void setCustomerTrc(List<FrejusCustomerData> customerTrc) {
    this.customerTrc = customerTrc;

  }

  @Override
  public List<FrejusCustomerData> getCustomerTrc() {
    return customerTrc;
  }

  public String getNumeroBuonoFrancia() {
    return numeroBuonoFrancia;
  }

  public void setNumeroBuonoFrancia(String numeroBuonoFrancia) {
    this.numeroBuonoFrancia = numeroBuonoFrancia;
  }

  @Override
  public int getNestedLevel() {
    return FrejusRecordDescriptor.HEADER_CODE_BEGIN.equals(getRecordCode()) ? -1 : 0;
  }

  @JsonIgnore
  public boolean toBeSkipped() {
    return FrejusRecordDescriptor.HEADER_CODE_BEGIN.equals(getRecordCode());
  }
}
