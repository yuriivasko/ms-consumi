package it.fai.ms.consumi.repository.viewallegati;

import java.io.Serializable;

import it.fai.common.enumeration.TipoDispositivoEnum;

public class ViewAllegatiFattureId implements Serializable {

  private static final long serialVersionUID = 2655008532044750711L;

  private String codiceStanziamento;

  private String codiceContratto;

  private TipoDispositivoEnum deviceType;

  private String panNumber;
  
  private String serialNumber;

  private String countryLicensePlate;

  private String licensePlate;

  public String getCodiceStanziamento() {
    return codiceStanziamento;
  }

  public void setCodiceStanziamento(String codiceStanziamento) {
    this.codiceStanziamento = codiceStanziamento;
  }

  public TipoDispositivoEnum getDeviceType() {
    return deviceType;
  }

  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public void setDeviceType(TipoDispositivoEnum deviceType) {
    this.deviceType = deviceType;
  }

  public String getPanNumber() {
    return panNumber;
  }

  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getCountryLicensePlate() {
    return countryLicensePlate;
  }

  public void setCountryLicensePlate(String countryLicensePlate) {
    this.countryLicensePlate = countryLicensePlate;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((codiceContratto == null) ? 0 : codiceContratto.hashCode());
    result = prime * result + ((codiceStanziamento == null) ? 0 : codiceStanziamento.hashCode());
    result = prime * result + ((countryLicensePlate == null) ? 0 : countryLicensePlate.hashCode());
    result = prime * result + ((deviceType == null) ? 0 : deviceType.hashCode());
    result = prime * result + ((licensePlate == null) ? 0 : licensePlate.hashCode());
    result = prime * result + ((panNumber == null) ? 0 : panNumber.hashCode());
    result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ViewAllegatiFattureId other = (ViewAllegatiFattureId) obj;
    if (codiceContratto == null) {
      if (other.codiceContratto != null)
        return false;
    } else if (!codiceContratto.equals(other.codiceContratto))
      return false;
    if (codiceStanziamento == null) {
      if (other.codiceStanziamento != null)
        return false;
    } else if (!codiceStanziamento.equals(other.codiceStanziamento))
      return false;
    if (countryLicensePlate == null) {
      if (other.countryLicensePlate != null)
        return false;
    } else if (!countryLicensePlate.equals(other.countryLicensePlate))
      return false;
    if (deviceType != other.deviceType)
      return false;
    if (licensePlate == null) {
      if (other.licensePlate != null)
        return false;
    } else if (!licensePlate.equals(other.licensePlate))
      return false;
    if (panNumber == null) {
      if (other.panNumber != null)
        return false;
    } else if (!panNumber.equals(other.panNumber))
      return false;
    if (serialNumber == null) {
      if (other.serialNumber != null)
        return false;
    } else if (!serialNumber.equals(other.serialNumber))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "ViewAllegatiFattureId [codiceStanziamento=" + codiceStanziamento + ", codiceContratto=" + codiceContratto + ", deviceType="
           + deviceType + ", panNumber=" + panNumber + ", serialNumber=" + serialNumber + ", countryLicensePlate=" + countryLicensePlate + ", licensePlate=" + licensePlate
           + "]";
  }

}
