package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.generici.DartfordRecordDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Service
public class LiberTRecordDescriptor implements RecordDescriptor<LiberTRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(LiberTRecord.class);

  private final List<String> HEADER = Arrays.asList(
      "nAbbonne",
      "contract",
      "nBadge",
      "anneeFacturation",
      "moisFacturation",
      "dateHeureEntree",
      "dateHeureSortie",
      "codeAutorouteEntree",
      "codeAutorouteSortie",
      "nGareEntree",
      "nomGareEntree",
      "nGareSortie",
      "nomGareSortie",
      "nbKms",
      "classeVehicule",
      "montantTTC",
      "tauxTVA"
  );

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    return Collections.EMPTY_MAP;
  }

  @Override
  public String getHeaderBegin() {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean isHeaderLine(String line) {
    return line.contains("N° Abonn");
  }
  @Override
  public boolean isFooterLine(String line) {
    return false;
  }

  @Override
  public String getFooterCodeBegin() {
    return "§£@senza footer";
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  @Override
  public String extractRecordCode(String _data) {
    return "DEFAULT";
  }

  @Override
  public LiberTRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {
    LiberTRecord recordObject = new LiberTRecord(fileName, rowNumber);



    recordObject.setTransactionDetail("");
    recordObject.setFromCsvRecord(_data, HEADER);
    recordObject.setRecordCode("DEFAULT");

    return recordObject;

  }

  public void checkConfiguration(){
    log.trace("No need to check configuration...");
  }
}
