package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class ElcitDescriptor implements RecordDescriptor<ElcitRecord> {

  public final static String HEADER_BEGIN      = "01";
  public final static String BEGIN_20          = "20";
  public final static String BEGIN_30          = "30";
  public final static String FOOTER_CODE_BEGIN = "90";

  private static final transient Logger log = LoggerFactory.getLogger(ElcitDescriptor.class);

  // Header
  private final static FileDescriptor descriptor01 = new FileDescriptor();

  static {
    descriptor01.addItem("recordCode", 1, 2);
    descriptor01.addItem("partnerCountryCode", 3, 5);
    descriptor01.skipItem("transmitter", 6, 10);
    descriptor01.skipItem("transmitterCountryCode", 11, 13);
    descriptor01.skipItem("receiver", 14, 18);
    descriptor01.addItem("creationDateInFile", 19, 26);
    descriptor01.addItem("creationTimeInFile", 27, 32);
    descriptor01.skipItem("fileNumber", 33, 36);
    descriptor01.skipItem("dataType", 37, 46);
    descriptor01.skipItem("fileReleaseNumber", 47, 52);
  }

  public static FileDescriptor descriptor = new FileDescriptor();

  static {
    descriptor.addItem("recordCode", 1, 2);
    descriptor.addItem("deviceCode", 3, 22);
    descriptor.addItem("deviceType", 23, 24);
    descriptor.addItem("date", 25, 34);
    descriptor.addItem("time", 35, 42);
    descriptor.addItem("netCode", 43, 44);
    descriptor.addItem("entryGate", 45, 48);
    descriptor.addItem("exitGate", 49, 52);
    descriptor.addItem("description", 53, 82);
    descriptor.addItem("pricingClass", 83, 84);
    descriptor.addItem("km", 85, 89);
    descriptor.addItem("currencyCode", 90, 92);
    descriptor.addItem("transactionType", 93, 94);
    descriptor.addItem("totalAmount", 95, 104);
    descriptor.addItem("signOfTheTransaction", 105, 105);
    descriptor.addItem("licensePlate", 106, 120);
    descriptor.addItem("routeID", 121, 125);
    descriptor.addItem("subContractCode", 152, 160);
  }

  public static FileDescriptor vatDescriptor = new FileDescriptor();

  static {
    vatDescriptor.addItem("recordCode", 1, 2);
    vatDescriptor.addItem("deviceCode", 3, 22);
    vatDescriptor.addItem("deviceType", 23, 24);
    vatDescriptor.addItem("date", 25, 34);
    vatDescriptor.addItem("time", 35, 42);
    vatDescriptor.addItem("netCode", 43, 44);
    vatDescriptor.addItem("entryGate", 45, 48);
    vatDescriptor.addItem("exitGate", 49, 52);
    vatDescriptor.addItem("vatCode", 53, 55);
    vatDescriptor.addItem("vatRate", 56, 58);
    vatDescriptor.addItem("lawReference", 59, 74);
    vatDescriptor.addItem("amount", 75, 83);
  }

  // Trailer
  static final FileDescriptor descriptor90          = new FileDescriptor();
  static final String         numberOfDetailRecords = "numberOfDetailRecords";
  static {
    descriptor90.addItem("recordCode", 1, 2);
    descriptor90.addItem("transmitterCode", 3, 10);
    descriptor90.addItem("receiver", 11, 18);
    descriptor90.addItem("fileCreationDate", 19, 26);
    descriptor90.addItem("fileCreationTime", 27, 32);
    descriptor90.addItem("fileNumber", 33, 36);
    descriptor90.addItem("dataType", 37, 46);
    descriptor90.addItem(numberOfDetailRecords, 47, 54);
  }

 @Override
 public ElcitRecord decodeRecordCodeAndCallSetFromString(final String _data, final String _recordCode,String fileName, long rowNumber) {
    var entity = new ElcitRecord( fileName,  rowNumber);
    switch (_recordCode) {

    case HEADER_BEGIN:
      entity.setFromString(ElcitDescriptor.descriptor01.getItems(), _data);
      break;

    case BEGIN_20:
      entity.setFromString(ElcitDescriptor.descriptor.getItems(), _data);
      break;

    case BEGIN_30:
      entity.setFromString(ElcitDescriptor.vatDescriptor.getItems(), _data);
      break;

    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }


  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    Optional<StringItem> stringItem = ElcitDescriptor.descriptor90.getItem(ElcitDescriptor.numberOfDetailRecords);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in ElcitDescriptor!");
    }
    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return ElcitDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return ElcitDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 2;
  }

  public void checkConfiguration(){
    new RecordDescriptorChecker(ElcitDescriptor.class, ElcitRecord.class).checkConfiguration();
  }

}
