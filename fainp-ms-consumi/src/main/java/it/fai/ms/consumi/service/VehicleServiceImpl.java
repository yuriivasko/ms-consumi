package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.repository.vehicle.VehicleRepository;

@Service
@Validated
public class VehicleServiceImpl implements VehicleService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final VehicleRepository vehicleRepository;

  @Inject
  public VehicleServiceImpl(final VehicleRepository _vehicleRepository) {
    vehicleRepository = _vehicleRepository;
  }

  @Override
  public Optional<Vehicle> newVehicleByVehicleUUID_WithFareClass(@NotNull final Device _device, @NotNull final Instant _instant,
                                                             @NotNull final String _fareClass) {

    String devideVehicleUUID = _device.getVehicleUUID();
    if(StringUtils.isBlank(devideVehicleUUID))
      return Optional.empty();


    _log.debug("Searching vehicle by device uuid {} in date {} ...", _device, _instant);

    final var storicoAssociazioneDVVechicleUUID = vehicleRepository.findIdVeicoloFromStoricoAssociazioneDVForDeviceAtDate(_device.getDeviceUUID(), _instant);
    String vehicleUUUID = storicoAssociazioneDVVechicleUUID.orElse(devideVehicleUUID);

    final var optionalVehicle = vehicleRepository.findVehicleByVehicleUuid(vehicleUUUID, _instant);

    optionalVehicle.ifPresentOrElse(vehicle -> {
      vehicle.setFareClass(_fareClass);
      _log.debug("... found {}", vehicle);
    }, () -> _log.info("... vehicle by vehicle uuid {} and date {} not found", vehicleUUUID, _instant));

    return optionalVehicle;
  }
}
