package it.fai.ms.consumi.api.trx.model.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.api.trx.model.TrxBasicReq;
import it.fai.ms.consumi.api.trx.model.TrxNoOilReq;
import it.fai.ms.consumi.api.trx.model.TrxOilReq;
import it.fai.ms.consumi.client.CountryDTO;
import it.fai.ms.consumi.client.GeoClient;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiOil;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.domain.consumi.generici.trackycard.TrackyCardGenerico;
import it.fai.ms.consumi.repository.flussi.record.model.DummyRecord;

@Service
public class TrxReqConsumoMapper {

  private final static String ACTICO = "ACTICO";
  private final GeoClient     geoClient;
  private final String        authorizationHeader;

  private Logger log = LoggerFactory.getLogger(getClass());

  public TrxReqConsumoMapper(GeoClient geoClient, @Value("${application.authorizationHeader}") String authorizationHeader) {
    this.geoClient = geoClient;
    this.authorizationHeader = authorizationHeader;
  }

  public TrackyCardCarburantiOil toCarburante(TrxOilReq trxReq) {

    log.info("Map {} to Carburante.", trxReq);
    OffsetDateTime submissioDateTime = trxReq.getSubmissioDateTime();
    Source source = new Source(Instant.now(), submissioDateTime.toInstant(), ACTICO);
    source.setFileName(ACTICO + "-" + Instant.now()
                                             .toString());

    TrackyCardCarburantiOil toCarburante = new TrackyCardCarburantiOil(source, "",
                                                                       Optional.of(new TrackyCardGlobalIdentifier(trxReq.getRequestCode())),
                                                                       new DummyRecord());

    toCarburante.setCustomer(new Customer(trxReq.getBuyerCode()));

    Transaction transaction = new Transaction();
    transaction.setDetailCode(trxReq.getArticleCode());
    transaction.setSign("+");
//Dà problemi con il dettaglio stanziamento, dove moltiplica gli amount positivi con -1 se il Sign è negativo
//    int signum = trxReq.getAmount()
//      .signum();
//    if (signum == -1) {
//      transaction.setSign("-");
//    }
    toCarburante.setTransaction(transaction);

    toCarburante.setGroupArticlesNav(null);
    toCarburante.setRegion(null);

    toCarburante.setProcessed(false);
    Device device = new Device(trxReq.getCardNumber(), TipoDispositivoEnum.TRACKYCARD);
    device.setPan(null); // per noi la trackycard non ha pan inteso come identificativo del servizio. Il pan della carta
                         // E' il seriale!

    boolean isVirtuale = trxReq.getVirtualCard();
    device.setVirtuale(isVirtuale);
    device.setServiceType(TipoServizioEnum.valueOf(trxReq.getProductCode()));
    toCarburante.setDevice(device);

    String outletCode = trxReq.getOutletCode();
    String pump = trxReq.getPump();
    FuelStation fuelStation = new FuelStation(outletCode);
    fuelStation.setFuelStationCode(outletCode);
    fuelStation.setPumpNumber(pump);
    toCarburante.setFuelStation(fuelStation);

    toCarburante.setOtherSupport(null);
    toCarburante.setKilometers(trxReq.getKm() != null ? new BigDecimal(trxReq.getKm()) : null);
    toCarburante.setProductCode(trxReq.getProductCode());

    if (StringUtils.isNotBlank(trxReq.getPlate())) {
      Vehicle vehicle = new Vehicle();
      vehicle.setFareClass(null);
      String intVRC = trxReq.getIntVRC();
      if (StringUtils.isBlank(intVRC)) {
        intVRC = "IT";
      } else {
        intVRC = veichleCountryFromIntVRC(intVRC);
      }
      VehicleLicensePlate vlp = new VehicleLicensePlate(trxReq.getPlate(), intVRC);
      vehicle.setLicensePlate(vlp);
      toCarburante.setVehicle(vehicle);
    } else {
      log.warn("Plate is not set on api transaction DTO");
    }

    Amount priceExposed = generatePriceExposedByTrxBasicReq(trxReq);
    toCarburante.setPriceReference(priceExposed);

    BigDecimal quantity = trxReq.getQuantity();
//    if(quantity.compareTo(BigDecimal.ZERO) < 0) {
//      quantity = quantity.abs();
//    }
    toCarburante.setQuantity(quantity);

    OffsetDateTime supplyDateTime = trxReq.getSupplyDateTime();
    toCarburante.setEntryDateTime(supplyDateTime.toInstant());

    return toCarburante;
  }

  public TrackyCardGenerico toGenerico(TrxNoOilReq trxReq) {

    log.info("Map {} to Generico.", trxReq);

    OffsetDateTime submissioDateTime = trxReq.getSubmissioDateTime();
    Source source = new Source(submissioDateTime.toInstant(), Instant.now(), ACTICO);
    source.setFileName(ACTICO + "-" + Instant.now()
                                             .toString());

    TrackyCardGenerico toGenerico = new TrackyCardGenerico(source, "", Optional.of(new TrackyCardGlobalIdentifier(trxReq.getRequestCode())),
                                                           new DummyRecord());

    Transaction transaction = new Transaction();
    transaction.setDetailCode(trxReq.getArticleCode());
    transaction.setSign("+");
//Dà problemi con il dettaglio stanziamento, dove moltiplica gli amount positivi con -1 se il Sign è negativo
//    int signum = trxReq.getAmount()
//                       .signum();
//    if (signum == -1) {
//      transaction.setSign("-");
//    }
    toGenerico.setTransaction(transaction);

    toGenerico.setGroupArticlesNav(null);
    toGenerico.setRegion(null);

    toGenerico.setProcessed(false);
    toGenerico.setDeviceCode(trxReq.getCardNumber());
    Device device = new Device(trxReq.getCardNumber(), TipoDispositivoEnum.TRACKYCARD);
    device.setPan(null); // per noi la trackycard non ha pan inteso come identificativo del servizio. Il pan della carta
                         // E' il seriale!

    boolean isVirtuale = trxReq.getVirtualCard();
    device.setVirtuale(isVirtuale);
    device.setServiceType(TipoServizioEnum.valueOf(trxReq.getProductCode()));
    toGenerico.setDevice(device);

    String buyerCode = trxReq.getBuyerCode();
    toGenerico.setCustomer(new Customer(buyerCode));

    String supplierDocumentNo = trxReq.getSupplierDocumentNo();
    toGenerico.setSupplierInvoiceDocument(supplierDocumentNo);
    String additionalDescription = trxReq.getAdditionalDescription();
    toGenerico.setAdditionalInformation(additionalDescription);

    OffsetDateTime supplyDateStart = trxReq.getSupplyDateStart();
    toGenerico.setStartDate(supplyDateStart.toInstant());

    OffsetDateTime supplyDateEnd = trxReq.getSupplyDateEnd();
    toGenerico.setEndDate(supplyDateEnd.toInstant());

    String highwaySection = trxReq.getHighwaySection();
    toGenerico.setRoute(highwaySection);

    toGenerico.setLicencePlateOnRecord(trxReq.getPlate());

    if (StringUtils.isNotBlank(trxReq.getPlate())) {
      Vehicle vehicle = new Vehicle();
      vehicle.setFareClass(trxReq.getFareClass());
      String intVRC = trxReq.getIntVRC();
      if (StringUtils.isBlank(intVRC)) {
        intVRC = "IT";
      } else {
        intVRC = veichleCountryFromIntVRC(intVRC);
      }
      VehicleLicensePlate vlp = new VehicleLicensePlate(trxReq.getPlate(), intVRC);
      vehicle.setLicensePlate(vlp);
      toGenerico.setVehicle(vehicle);
    } else {
      log.warn("Plate is not set on api transaction DTO");
    }

    Amount priceExposed = generatePriceExposedByTrxBasicReq(trxReq);
    toGenerico.setAmount(priceExposed);

    BigDecimal quantity = trxReq.getQuantity();
//    if(quantity.compareTo(BigDecimal.ZERO) < 0) {
//      quantity = quantity.abs();
//    }
    toGenerico.setQuantity(quantity);
    
    toGenerico.setTipoDispositivo(TipoDispositivoEnum.TRACKYCARD.name());
    toGenerico.setPuntoErogazione(trxReq.getOutletCode());
    toGenerico.setKm(trxReq.getKm() + "");
    return toGenerico;
  }

  private Amount generatePriceExposedByTrxBasicReq(TrxBasicReq trxReq) {
    Amount priceExposed = new Amount();
    BigDecimal amount = trxReq.getAmount();

    BigDecimal singlePriceIncludeIva = BigDecimal.ZERO;
    BigDecimal quantity = trxReq.getQuantity();
    if (quantity.compareTo(BigDecimal.ZERO) != 0) {
      singlePriceIncludeIva = amount.divide(quantity, 5, RoundingMode.HALF_UP);
    }
    log.debug("SinglePriceIncludeIva: {} - Quantity: {} - Amount: {}", singlePriceIncludeIva, quantity, amount);

    BigDecimal vat = trxReq.getVat();
    BigDecimal iva = vat.multiply(new BigDecimal(100));
    priceExposed.setVatRate(iva);

    String currency = trxReq.getCurrency();
    Money priceIncludedIva = Money.of(singlePriceIncludeIva, currency);

    priceExposed.setAmountIncludedVat(priceIncludedIva);
    priceExposed.setExchangeRate(null);
    return priceExposed;
  }

  private String veichleCountryFromIntVRC(String intVrc) {
    List<CountryDTO> countries = this.geoClient.getAllCountriesSorted(authorizationHeader);
    return countries.stream()
                    .filter(countryDTO -> countryDTO.getSiglaAuto()
                                                    .equalsIgnoreCase(intVrc))
                    .map(CountryDTO::getCodice)
                    .findFirst()
                    .orElse(intVrc);
  }

}
