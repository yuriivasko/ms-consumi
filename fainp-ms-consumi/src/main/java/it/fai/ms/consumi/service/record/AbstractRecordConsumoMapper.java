package it.fai.ms.consumi.service.record;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

public abstract class AbstractRecordConsumoMapper {

  protected static final String yyyyMMdd_dateformat            = "yyyyMMdd";
  protected static final String yyyy_MM_dd_dateformat          = "yyyy-MM-dd";
  protected static final String HHmmss_dottet_timeformat       = "HH.mm.ss";
  protected static final String HHmmss_timeformat              = "HHmmss";
  protected static final String HHmm_timeformat                = "HHmm";
  protected static final String yyyyMMddHHmmss_timestampformat = "yyyyMMddHHmmss";

  protected Optional<Instant> calculateInstantCreationInFile(final CommonRecord record, final String datePattern,
                                                             final String timePattern) throws ParseException {

    if (record.getCreationDateInFile() != null) {
      return calculateInstantByDateAndTime(record.getCreationDateInFile(), record.getCreationTimeInFile(), datePattern, timePattern);
    } else {
      return Optional.ofNullable(null);
    }

  }

  protected Instant parseTimestamp(String timestamp) throws ParseException {
    return new SimpleDateFormat(yyyyMMddHHmmss_timestampformat).parse(timestamp)
                                                               .toInstant();
  }

  protected Optional<Instant> calculateInstantByDateAndTime(final String entryDate, final String entryTime, final String datePattern,
                                                            final String timePattern) throws ParseException {
    if (entryDate != null && !entryDate.isEmpty()) {
      if (entryTime != null && !entryTime.isEmpty() && (timePattern == null || timePattern.isEmpty()))
        throw new IllegalStateException("entryTime specified required not-null timepattern");
      String completePattern = datePattern
                               + (entryTime != null && !entryTime.isEmpty() && timePattern != null && !timePattern.isEmpty() ? timePattern
                                                                                                                             : "");
      String completeDateAndTime = entryDate + (entryTime != null ? entryTime : "");
      return calculateInstantByDateTime(completeDateAndTime, completePattern);
    } else {
      return Optional.ofNullable(null);
    }
  }

  protected Optional<Instant> calculateInstantByDateTime(String completeDateAndTime, String completePattern) throws ParseException {
    if (completeDateAndTime != null && !completeDateAndTime.isEmpty()) {
      var dateFormat = new SimpleDateFormat(completePattern);
      var date = dateFormat.parse(completeDateAndTime);
      if (date.after(maxDate())) {
        return Optional.of(maxDate().toInstant());
      } else {
        return Optional.of(date.toInstant());
      }
    } else {
      return Optional.empty();
    }
  }

  private Date maxDate() {
    final var calendar = GregorianCalendar.getInstance();
    calendar.set(9999, 11, 30, 23, 59);
    return calendar.getTime();
  }
}
