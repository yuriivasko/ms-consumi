package it.fai.ms.consumi.service.validator;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.jms.notification_v2.ConsumoWarningNotification;
import it.fai.ms.consumi.repository.flussi.record.model.util.MinimalRecordInformation;
import it.fai.ms.consumi.repository.flussi.record.utils.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.TrustedCustomerSupplier;
import it.fai.ms.consumi.domain.TrustedStatoDispositivo;
import it.fai.ms.consumi.domain.TrustedVechicleSupplier;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.DateSupplier;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.service.validator.internal.ConsumoValidator;
import it.fai.ms.consumi.service.validator.internal.ContractValidator;
import it.fai.ms.consumi.service.validator.internal.CustomerValidator;
import it.fai.ms.consumi.service.validator.internal.DeviceValidator;

@Service
@Transactional(propagation=Propagation.REQUIRES_NEW, isolation=Isolation.READ_UNCOMMITTED)
public class ConsumoNoBlockingValidatorService {


  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final CustomerValidator customerValidator;
  private final DeviceValidator   deviceValidator;
  private final ContractValidator contractValidator;
  private final NotificationService     notificationService;
  private final ConsumoValidator consumoValidator;


  @Inject
  public ConsumoNoBlockingValidatorService(final CustomerValidator _customerValidator, final DeviceValidator _deviceValidator,
                                            final ContractValidator _contractValidator, final ConsumoValidator _consumoValidator, final NotificationService _notificationService) {

    customerValidator = _customerValidator;
    deviceValidator = _deviceValidator;
    contractValidator = _contractValidator;
    notificationService = _notificationService;
    consumoValidator = _consumoValidator;
  }
  
  public ValidationOutcome validateAndNotify(final ConsumoNoBlockingData _consumo, String fileName, String ingestion_time, String servicePartner) {
    ValidationOutcome validationOutcome = validate(_consumo);
//    String consumoToString = CommonRecord.toNotificationMessage(_consumo.getRecordData(), fileName, ingestion_time, servicePartner);
    notifyProcessingResult(validationOutcome, _consumo.getRecordData(), fileName, ingestion_time, servicePartner, _consumo.getJobName());
    return validationOutcome;
  }

  public void storeForPostValidation(@NotNull final Consumo _consumo) {
    ConsumoNoBlockingData rs = toConsumoNoBlockingData(_consumo);
    _consumo.setConsumoNoBlockingData(rs);
  }

  public ValidationOutcome validate(@NotNull final Consumo _consumo) {
    ConsumoNoBlockingData rs = toConsumoNoBlockingData(_consumo);
    return validate(rs);
  }

  private ConsumoNoBlockingData toConsumoNoBlockingData(@NotNull Consumo _consumo) {
    String companyCode;
    if (_consumo instanceof TrustedCustomerSupplier) {
      companyCode = ((TrustedCustomerSupplier) _consumo).getCustomer()
        .getId();
    } else {
      companyCode = _consumo.getContract()
        .get()
        .getCompanyCode();
    }

    boolean hasDevice = _consumo instanceof DeviceSupplier && ((DeviceSupplier) _consumo).getDevice() != null;
    Device device = hasDevice ? ((DeviceSupplier) _consumo).getDevice() : null;


    boolean isTrustedStatoDispositivo = _consumo instanceof TrustedStatoDispositivo;

    Instant consumoDate = _consumo.getDate();
    String contractCode =  (_consumo.getContract() != null && _consumo.getContract().isPresent()) ? _consumo.getContract().get().getId() : null;
    Float amountExcludedVat = (_consumo.getAmount() != null && _consumo.getAmount().getAmountExcludedVat() != null) ? _consumo.getAmount()
      .getAmountExcludedVat()
      .getNumber()
      .numberValueExact(Float.class) : null;
    String  groupArticlesNav = _consumo.getGroupArticlesNav();

    boolean isTrustedVechicleSupplier = _consumo instanceof TrustedVechicleSupplier;
    final Vehicle vehicle = isTrustedVechicleSupplier ? ((TrustedVechicleSupplier) _consumo).getVehicle() : null;
    List<String> recordData = _consumo.getRecord()!=null ? MinimalRecordInformation.fromCommonRecord(_consumo.getRecord()).toList() : Arrays.asList("");


    ConsumoNoBlockingData rs = new ConsumoNoBlockingData();
    rs.setAmountExcludedVat(amountExcludedVat);
    rs.setCompanyCode(companyCode);
    rs.setConsumoDate(consumoDate);
    rs.setContractCode(contractCode);
    rs.setDevice(device);
    rs.setGroupArticlesNav(groupArticlesNav);
    rs.setHasDevice(hasDevice);
    rs.setJobName(_consumo.getJobName());
    rs.setRecordData(recordData);
    rs.setTrustedStatoDispositivo(isTrustedStatoDispositivo);
    rs.setTrustedVechicleSupplier(isTrustedVechicleSupplier);
    rs.setVehicleNull(vehicle==null);
    if(vehicle!=null) {
      rs.setCompleteVehicle(vehicle.isComplete());
    }
    return rs;
  }
  private ValidationOutcome validate(@NotNull final ConsumoNoBlockingData data) {
    final var validationOutcome = new ValidationOutcome();
    
    Device device = data.getDevice();
    String deviceId = device!=null ? device.getId() : null;
    final TipoDispositivoEnum tipoDispositivo = device!=null ? device.getType() : null;
    boolean isDeviceNotVirtual = (data.getDevice()!=null) ? !data.getDevice().isVirtuale() :  true;
    String companyCode = data.getCompanyCode();
    Instant consumoDate = data.getConsumoDate();
    String contractCode = data.getContractCode();
    boolean hasDevice = data.isHasDevice();
    boolean isTrustedStatoDispositivo = data.isTrustedStatoDispositivo();
    Float amountExcludedVat = data.getAmountExcludedVat();
    String groupArticlesNav = data.getGroupArticlesNav();
    boolean isTrustedVechicleSupplier = data.isTrustedVechicleSupplier();
    boolean isCompleteVehicle = data.isCompleteVehicle();
    boolean isVehicleNull = data.isVehicleNull();

    var _validationOutcome = customerWasActiveInDate(companyCode,consumoDate );
    validationOutcome.addValidationOutcomeMessages(_validationOutcome);

    if (isDeviceNotVirtual) {
      _validationOutcome = contractWasActiveInDate(contractCode, consumoDate);
      validationOutcome.addValidationOutcomeMessages(_validationOutcome);

      _validationOutcome = deviceWasActiveAndAssociatedWithContractInDate(contractCode, hasDevice, deviceId, tipoDispositivo, consumoDate, isTrustedStatoDispositivo,amountExcludedVat,groupArticlesNav,companyCode);
      validationOutcome.addValidationOutcomeMessages(_validationOutcome);

      _validationOutcome = serviceWasActiveInDate(device, consumoDate);
      validationOutcome.addValidationOutcomeMessages(_validationOutcome);

      if (isTrustedVechicleSupplier) {
        if (isVehicleNull || !isCompleteVehicle) {
          validationOutcome.addMessage(new Message("w006").withText("Vehicle not present"));
        }
      }
    } else {
      _log.info("Skip validation because device set in consumo is virtual");
    }

    _log.info("Waring validations result : {}", validationOutcome);
    

    return validationOutcome;
  }

  // quando il codice cliente non era attivo alla data di consumo → anomalia (non bloccante)
  private ValidationOutcome customerWasActiveInDate(String companyCode, Instant consumoDate ) {
    return customerValidator.customerActiveInDate(companyCode, consumoDate);
  }

  private ValidationOutcome contractWasActiveInDate(String contractCode, Instant consumoDate) {
    if (contractCode!=null) {
      return contractValidator.contractActiveInDate(contractCode, consumoDate);
    }
    return null;
  }

  // - se l'abbinamento contratto cliente e dispositivo non era attivo alla data del consumo→ anomalia (non bloccante)
  // - se il dispositivo era in stato disattivo alla data di transito + 24h → anomalia (non bloccante)
  private ValidationOutcome deviceWasActiveAndAssociatedWithContractInDate(String contractCode, boolean hasDevice, String deviceCode, TipoDispositivoEnum tipoDispositivo, Instant consumoDate, boolean isTrustedStatoDispositivo, Float amountExcludedVat, String groupArticlesNav, String companyCode) {
    ValidationOutcome validationOutcome = null;
    if (hasDevice) {

      if (contractCode == null) {
        validationOutcome = new ValidationOutcome();
        validationOutcome.addMessage(new Message("w004").withText("The consumo own device information but does not contain contract information"));
        return validationOutcome;
      }

      if(contractCode !=null){
          if (isTrustedStatoDispositivo) {
            validationOutcome =  deviceValidator.deviceWasAssociatedWithContractInDate(deviceCode, tipoDispositivo,
                                                                         contractCode,
                                                                         consumoDate, DateSupplier.VALIDATOR_DATE_OFFSET);
          } else {
            validationOutcome = deviceValidator.deviceWasActiveAndAssociatedWithContractInDate(deviceCode,
                                                                                  tipoDispositivo,
                                                                                  contractCode,
                                                                                  consumoDate,
              DateSupplier.VALIDATOR_DATE_OFFSET);
          }
      }
      if(validationOutcome==null)validationOutcome = new ValidationOutcome();
      
      if (validationOutcome != null) {
        // controllo se devo inviare la notifica di tipo DispositiviBloccati che fatturano
        if (validationOutcome.getMessages()
                             .stream()
                             .anyMatch(m -> m.getCode()
                                             .equals("w003"))) {
          // FIXME la data inviata è in UTC.. Corretto?
          deviceValidator.sendDispositiviBloccatiFattureMessage( deviceCode, tipoDispositivo, consumoDate, companyCode, groupArticlesNav, amountExcludedVat) ;
        }
      }
    }

    return validationOutcome;
  }

  private ValidationOutcome serviceWasActiveInDate(final Device device, Instant consumoDate) {
    if (device != null) {
      return deviceValidator.serviceWasActiveInDate(device, consumoDate, DateSupplier.VALIDATOR_DATE_OFFSET);
    }
    return null;
  }
  
  public void validateStanziamenti(Set<String> _stanziamentiCodeList) {
    for (String code : _stanziamentiCodeList) {
      consumoValidator.validateDettagliStanziamento(code);
    }
  }
  private void notifyProcessingResult(ValidationOutcome validationOutcome, List<String> recordData, String fileName, String ingestion_time, String servicePartner, String jobName) {
    Set<Message> messages = validationOutcome.getMessages();
//    List<String> record = recordData;
    MinimalRecordInformation minimalRecordInformation = MinimalRecordInformation.fromList(recordData);
    String notificationMessageText = CommonRecord.toNotificationMessage(recordData, fileName, ingestion_time, servicePartner);

    messages.forEach(message -> {
//      var messageTextKV = new HashMap<String, Object>();
//      messageTextKV.put("job_name", jobName);  //_consumo.getJobName()
//      messageTextKV.put("original_message", message.getText());
//      messageTextKV.put("original_record", record);
//      notificationService.notify("CPV-" + message.getCode(), messageTextKV);

        notificationService.notify(new ConsumoWarningNotification(
          jobName,
          fileName,
          minimalRecordInformation.getRowNumberAsLong(),
          minimalRecordInformation.getSimpleName(),
          minimalRecordInformation.getUuid(),
          minimalRecordInformation.getRecordCode(),
          minimalRecordInformation.getTransactionDetail(),
          notificationMessageText,
          "CPV-" + message.getCode(),
          message.getText()
        ));
    });
  }
}
