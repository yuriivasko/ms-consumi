package it.fai.ms.consumi.web.rest;

import it.fai.ms.consumi.service.processor.RecordConsumoReprocessorService;
import it.fai.ms.consumi.service.processor.RecordNotReprocessedException;
import it.fai.ms.consumi.service.processor.RecordNotRetrievableException;
import it.fai.ms.consumi.web.rest.errors.CustomException;
import it.fai.ms.consumi.web.rest.errors.Errno;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static it.fai.ms.consumi.web.rest.RecordConsumoReprocessorResource.BASE_PATH;

@RestController
@RequestMapping(BASE_PATH)
public class RecordConsumoReprocessorResource extends AbstractResource {

  private final Logger log = LoggerFactory.getLogger(getClass());

  static final String BASE_PATH = "/api/reprocessor";

  private final RecordConsumoReprocessorService recordConsumoReprocessorService;

  public RecordConsumoReprocessorResource(RecordConsumoReprocessorService recordConsumoReprocessorService) {
    this.recordConsumoReprocessorService = recordConsumoReprocessorService;
  }

  @GetMapping("reprocess/{type}/{uuid}")
  public ResponseEntity<String> reprocessRecord(@PathVariable("type") String type, @PathVariable("uuid") String uuid) throws CustomException {

    log.debug("Called reprocessRecord with type:=[{}], uuid:=[{}]", type, uuid);
    try {

      String message = recordConsumoReprocessorService.reprocessSingleRecord(type, uuid);

      log.debug("Reprocess OK, return OK message");
      return ResponseEntity.ok(message);

    } catch (RecordNotRetrievableException | RecordNotReprocessedException e) {
      log.warn("Reprocess go bad! Return KO message:=[{}]", e);
      throw CustomException.builder(HttpStatus.UNPROCESSABLE_ENTITY)
        .add(e instanceof RecordNotRetrievableException ? Errno.RECORD_NOT_RETRIEVABLE : Errno.RECORD_NOT_REPROCESSED);
    }
  }


}

