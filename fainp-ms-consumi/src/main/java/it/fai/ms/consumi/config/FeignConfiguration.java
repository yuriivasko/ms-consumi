package it.fai.ms.consumi.config;

import javax.money.MonetaryAmount;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.client.AnagaziendeClient;
import it.fai.ms.consumi.client.AuthorizationApiClient;
import it.fai.ms.consumi.client.CanoniApiClient;
import it.fai.ms.consumi.client.DocumentClient;
import it.fai.ms.consumi.client.GeoClient;
import it.fai.ms.consumi.client.PetrolPumpClient;
import it.fai.ms.consumi.client.StanziamentiApiAuthClient;
import it.fai.ms.consumi.repository.flussi.record.MoneyDeserializer;
import it.fai.ms.consumi.repository.flussi.record.MoneySerializer;

@Configuration
@EnableFeignClients(
                    basePackageClasses = { ElasticSearchFeignClient.class, AuthorizationApiClient.class, StanziamentiApiAuthClient.class,
                                           PetrolPumpClient.class, DocumentClient.class, AnagaziendeClient.class, GeoClient.class,CanoniApiClient.class})
public class FeignConfiguration {

  @Bean
  feign.Logger.Level feignLoggerLevel() {
    return feign.Logger.Level.FULL;
  }
  
  public static SpringDecoder buildSpringDecoder() {
    return new SpringDecoder(getHttpMessageConvertersObjectFactory());
  }
  
  public static SpringEncoder buildSpringEncoder() {
    return new SpringEncoder(getHttpMessageConvertersObjectFactory());
  }

  public static ObjectFactory<HttpMessageConverters> getHttpMessageConvertersObjectFactory() {
    ObjectMapper customObjectMapper = new ObjectMapper();
    customObjectMapper.findAndRegisterModules();
    customObjectMapper.registerModule(monetaryAmountStringDeserializer());
    customObjectMapper.registerModule(monetaryAmountStringSerializer());
    HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter(customObjectMapper);
    return () -> new HttpMessageConverters(jacksonConverter);
  }

  private static SimpleModule monetaryAmountStringDeserializer(){
    SimpleModule module = new SimpleModule();
    module.addDeserializer(MonetaryAmount.class, new MoneyDeserializer());
    return module;
  }

  private static SimpleModule monetaryAmountStringSerializer(){
    SimpleModule module = new SimpleModule();
    module.addSerializer(MonetaryAmount.class, new MoneySerializer());
    return module;
  }
}
