package it.fai.ms.consumi.service;

import java.util.Optional;

import it.fai.ms.consumi.repository.tollcharger.model.TollChargerEntity;

public interface TollChargerService {

  Optional<TollChargerEntity> findById(Long id);

  Optional<TollChargerEntity> findOneByCode(String code);

  TollChargerEntity lastInsertId();

}
