package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.pedaggi;

import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.DettaglioStanziamentoDefinitivoProcessor;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.DettaglioStanziamentoProvvisorioProcessor;

@Service
public class DettaglioStanziamentoPedaggioProcessorImpl implements DettaglioStanziamentoPedaggioProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoDefinitivoProcessor  lastAllocationDetailProcessor;
  private final DettaglioStanziamentoProvvisorioProcessor tempAllocationDetailProcessor;

  @Inject
  public DettaglioStanziamentoPedaggioProcessorImpl(final DettaglioStanziamentoProvvisorioProcessor _tempAllocationDetailProcessor,
                                                    final DettaglioStanziamentoDefinitivoProcessor _lastAllocationDetailProcessor) {
    lastAllocationDetailProcessor = _lastAllocationDetailProcessor;
    tempAllocationDetailProcessor = _tempAllocationDetailProcessor;
  }

  @Override
  public Optional<DettaglioStanziamento> process(@NotNull final DettaglioStanziamento _allocationDetail,
                                                 @NotNull final StanziamentiParams _allocationParams) {

    _log.debug(" ->  {}  <- processing : {}", _allocationDetail.getInvoiceType(), _allocationDetail);

    Optional<DettaglioStanziamento> allocations = null;

    allocations = selectProcessorAndProcess(_allocationDetail, _allocationParams);
    
    if(_log.isDebugEnabled()) {
	    allocations.ifPresentOrElse(allocationDetail -> {
	      _log.debug(" --> generated {} allocations", allocationDetail.getStanziamenti()
	                                                                 .size());
	      allocationDetail.getStanziamenti()
	                      .stream()
	                      .forEach(allocation -> _log.debug("  --> {}", allocation));
	    }, () -> _log.debug(" --> allocations not generated"));
    }
    return allocations;
  }

  private Optional<DettaglioStanziamento> selectProcessorAndProcess(final DettaglioStanziamento _allocationDetail,
                                                                    final StanziamentiParams _allocationParams) {
    Optional<DettaglioStanziamento> allocations = null;
    switch (_allocationDetail.getInvoiceType()) {
    case P:
      allocations = tempAllocationDetailProcessor.process(_allocationDetail, _allocationParams);
      break;
    case D:
      allocations = lastAllocationDetailProcessor.process(_allocationDetail, _allocationParams);
      break;
    default:
      throw new IllegalStateException("Not processable " + _allocationDetail + " [not managed invoice type "
                                      + _allocationDetail.getInvoiceType() + "]");
    }
    return allocations;
  }

}
