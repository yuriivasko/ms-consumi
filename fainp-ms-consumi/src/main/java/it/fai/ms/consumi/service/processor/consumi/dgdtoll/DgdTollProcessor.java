package it.fai.ms.consumi.service.processor.consumi.dgdtoll;

import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface DgdTollProcessor extends ConsumoProcessor<DgdToll> {

}
