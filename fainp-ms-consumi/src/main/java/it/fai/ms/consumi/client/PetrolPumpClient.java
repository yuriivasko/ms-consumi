package it.fai.ms.consumi.client;

import static it.fai.ms.consumi.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import java.time.Instant;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;

@FeignClient(name = "faipetrolpump")
public interface PetrolPumpClient {

  String BASE_PATH = "/api/ext";

  @GetMapping(BASE_PATH + "/price-table-list/{pointCode}/{codiceArticolo}/{date}")
  public List<PriceTableDTO> getPriceTableList(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                               @PathVariable("pointCode") String pointCode,
                                               @PathVariable("codiceArticolo") String codiceArticolo, @PathVariable("date") Instant date);

  @GetMapping("/api/_search/"+"point/{externalCode}")
  @ResponseBody
  public PointDTO searchPoint(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                              @PathVariable("externalCode") String externalCode);
  @GetMapping("/api/_search/"+"pointByCode/{code}")
  @ResponseBody
  public PointDTO getPoint(@RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                              @PathVariable("code") String code);
}
