package it.fai.ms.consumi.api.trx.model;

import java.time.OffsetDateTime;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * TrxNoOilReq
 */

@ApiModel(value="TrxNoOilReq", description="NoOil Request Schema",parent=TrxBasicReq.class)
public class TrxNoOilReq extends TrxBasicReq  {

  @JsonProperty("unit")
  private String unit = null;

  @JsonProperty("supplyDateStart")
  private OffsetDateTime supplyDateStart = null;

  @JsonProperty("supplyDateEnd")
  private OffsetDateTime supplyDateEnd = null;

  @JsonProperty("supplierDocumentNo")
  private String supplierDocumentNo = null;

  @JsonProperty("highwaySection")
  private String highwaySection = null;

  @JsonProperty("fareClass")
  private String fareClass = null;

  @JsonProperty("additionalDescription")
  private String additionalDescription = null;

  public TrxNoOilReq unit(String unit) {
    this.unit = unit;
    return this;
  }

  /**
   * Code identifying the quantity's unit measure. 'LT' = Litres and 'KG' = Kilograms.
   * @return unit
  **/
  @ApiModelProperty(value = "Code identifying the quantity's unit measure. 'LT' = Litres and 'KG' = Kilograms.")


  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public TrxNoOilReq supplyDateStart(OffsetDateTime supplyDateStart) {
    this.supplyDateStart = supplyDateStart;
    return this;
  }

  /**
   * Data inizio erogazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z
   * @return supplyDateStart
  **/
  @ApiModelProperty(required = true, value = "Data inizio erogazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z")
  @NotNull

  @Valid

  public OffsetDateTime getSupplyDateStart() {
    return supplyDateStart;
  }

  public void setSupplyDateStart(OffsetDateTime supplyDateStart) {
    this.supplyDateStart = supplyDateStart;
  }

  public TrxNoOilReq supplyDateEnd(OffsetDateTime supplyDateEnd) {
    this.supplyDateEnd = supplyDateEnd;
    return this;
  }

  /**
   * Data fine erogazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z
   * @return supplyDateEnd
  **/
  @ApiModelProperty(required = true, value = "Data fine erogazione as defined by RFC 3339, section 5.6, for example, 2017-07-21T17:32:28Z")
  @NotNull

  @Valid

  public OffsetDateTime getSupplyDateEnd() {
    return supplyDateEnd;
  }

  public void setSupplyDateEnd(OffsetDateTime supplyDateEnd) {
    this.supplyDateEnd = supplyDateEnd;
  }

  public TrxNoOilReq supplierDocumentNo(String supplierDocumentNo) {
    this.supplierDocumentNo = supplierDocumentNo;
    return this;
  }

  /**
   * Numero documento fornitore
   * @return supplierDocumentNo
  **/
  @ApiModelProperty(value = "Numero documento fornitore")


  public String getSupplierDocumentNo() {
    return supplierDocumentNo;
  }

  public void setSupplierDocumentNo(String supplierDocumentNo) {
    this.supplierDocumentNo = supplierDocumentNo;
  }

  public TrxNoOilReq highwaySection(String highwaySection) {
    this.highwaySection = highwaySection;
    return this;
  }

  /**
   * Tratta autostradale.
   * @return highwaySection
  **/
  @ApiModelProperty(value = "Tratta autostradale.")


  public String getHighwaySection() {
    return highwaySection;
  }

  public void setHighwaySection(String highwaySection) {
    this.highwaySection = highwaySection;
  }

  public TrxNoOilReq fareClass(String fareClass) {
    this.fareClass = fareClass;
    return this;
  }

  /**
   * Classe tariffaria del veicolo.
   * @return fareClass
  **/
  @ApiModelProperty(value = "Classe tariffaria del veicolo.")


  public String getFareClass() {
    return fareClass;
  }

  public void setFareClass(String fareClass) {
    this.fareClass = fareClass;
  }

  public TrxNoOilReq additionalDescription(String additionalDescription) {
    this.additionalDescription = additionalDescription;
    return this;
  }

  /**
   * Descrizione aggiuntiva.
   * @return additionalDescription
  **/
  @ApiModelProperty(value = "Descrizione aggiuntiva.")


  public String getAdditionalDescription() {
    return additionalDescription;
  }

  public void setAdditionalDescription(String additionalDescription) {
    this.additionalDescription = additionalDescription;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TrxNoOilReq trxNoOilReq = (TrxNoOilReq) o;
    return Objects.equals(this.unit, trxNoOilReq.unit) &&
        Objects.equals(this.supplyDateStart, trxNoOilReq.supplyDateStart) &&
        Objects.equals(this.supplyDateEnd, trxNoOilReq.supplyDateEnd) &&
        Objects.equals(this.supplierDocumentNo, trxNoOilReq.supplierDocumentNo) &&
        Objects.equals(this.highwaySection, trxNoOilReq.highwaySection) &&
        Objects.equals(this.fareClass, trxNoOilReq.fareClass) &&
        Objects.equals(this.additionalDescription, trxNoOilReq.additionalDescription) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(unit, supplyDateStart, supplyDateEnd, supplierDocumentNo, highwaySection, fareClass, additionalDescription, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TrxNoOilReq {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    unit: ").append(toIndentedString(unit)).append("\n");
    sb.append("    supplyDateStart: ").append(toIndentedString(supplyDateStart)).append("\n");
    sb.append("    supplyDateEnd: ").append(toIndentedString(supplyDateEnd)).append("\n");
    sb.append("    supplierDocumentNo: ").append(toIndentedString(supplierDocumentNo)).append("\n");
    sb.append("    highwaySection: ").append(toIndentedString(highwaySection)).append("\n");
    sb.append("    fareClass: ").append(toIndentedString(fareClass)).append("\n");
    sb.append("    additionalDescription: ").append(toIndentedString(additionalDescription)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

