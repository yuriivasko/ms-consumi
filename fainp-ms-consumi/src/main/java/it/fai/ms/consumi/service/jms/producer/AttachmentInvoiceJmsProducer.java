package it.fai.ms.consumi.service.jms.producer;

import static java.util.stream.Collectors.toSet;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.consumi.invoice.AttachmentInvoiceCreatedDTO;
import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.service.jms.producer.invoice.IGenerateInvoiceAttachment;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class AttachmentInvoiceJmsProducer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  private final StanziamentoRepository stanziamentiRepo;

  private final List<IGenerateInvoiceAttachment> generateInvoiceAttachment;

  public AttachmentInvoiceJmsProducer(final JmsProperties _jmsProperties, final StanziamentoRepository _stanziamentiRepo,
                                      final List<IGenerateInvoiceAttachment> _generateInvoiceAttachment) {
    jmsProperties = _jmsProperties;
    stanziamentiRepo = _stanziamentiRepo;
    generateInvoiceAttachment = _generateInvoiceAttachment;
  }

  public void generateAttachmentInvoiceAndSendToQueue(final RaggruppamentoArticoliDTO raggruppamentoArticoliDTO,
                                                      final DettaglioStanziamentoType tipoDettaglio,
                                                      final RequestsCreationAttachments requestCreationAttachments, final TemplateAllegati template) {

      List<String> codiciStanziamenti = raggruppamentoArticoliDTO.getCodiciStanziamenti();
      String codiceClienteFatturazione = raggruppamentoArticoliDTO.getCodiceClienteFatturazione();
      updateStanziamentiWithCodeAndDataInvoice(codiciStanziamenti, requestCreationAttachments, codiceClienteFatturazione);

    sendAttachmentInvoice(raggruppamentoArticoliDTO, tipoDettaglio, requestCreationAttachments, template);
  }

  public void sendAttachmentInvoice(final RaggruppamentoArticoliDTO raggruppamentoArticoliDTO,
                                    final DettaglioStanziamentoType tipoDettaglio,
                                    final RequestsCreationAttachments requestCreationAttachments, final TemplateAllegati template) {
    for (IGenerateInvoiceAttachment service : generateInvoiceAttachment) {
      Object message = service.generateDtoToAttachmentInvoice(tipoDettaglio, requestCreationAttachments, raggruppamentoArticoliDTO, template);
      if (message != null) {
        sendMessage((Serializable) message);
        return;
      }
    }

    log.warn("Unable to send attachment invoice message. Service not found for the type : {}", tipoDettaglio);
  }

  private void sendMessage(Serializable message) {
    JmsQueueNames queue = JmsQueueNames.GENERA_ALLEGATO_FATTURE_REQUEST;
    log.debug("Sending message {} to queue: {}", message, queue);
    JmsQueueSenderUtil.publishTransactional(jmsProperties, queue, message);
    log.debug("The message {} has been sent to queue {}", message, queue);
  }

  private void updateStanziamentiWithCodeAndDataInvoice(List<String> codiciStanziamenti,
                                                        RequestsCreationAttachments requestCreationAttachments,
                                                        String codiceClienteFatturazione) {
    String codiceFattura = requestCreationAttachments.getCodiceFattura();
    Instant dataFattura = requestCreationAttachments.getDataFattura();

    codiciStanziamenti.stream()
                      .map(code -> {
                        Stanziamento s = new Stanziamento(code);
                        s.setNumeroFattura(codiceFattura);
                        s.setDataFattura(dataFattura);
                        s.setCodiceClienteFatturazione(codiceClienteFatturazione);
                        stanziamentiRepo.updateInvoiceData(s);
                        return s;
                      })
                      .collect(toSet());
  }

  public void sendMessageGenerateRequest(RequestsCreationAttachments reqAttachment) {
    AttachmentInvoiceCreatedDTO dto = new AttachmentInvoiceCreatedDTO();
    dto.setNumeroFattura(reqAttachment.getCodiceFattura());
    dto.setCodiceRaggruppamentoArticoli(reqAttachment.getCodiceRaggruppamentoArticolo());
    dto.setUuidDocumento(reqAttachment.getUuidDocumento());
    JmsQueueNames queueResponse = JmsQueueNames.GENERA_ALLEGATO_FATTURE_RESPONSE;
    log.debug("Send message request: {} on queue: {}", dto, queueResponse);
    JmsQueueSenderUtil.publish(jmsProperties, queueResponse, dto);
    log.debug("Message has been sent...");
  }

}
