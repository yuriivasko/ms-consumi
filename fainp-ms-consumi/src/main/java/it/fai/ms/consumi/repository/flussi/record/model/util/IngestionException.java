package it.fai.ms.consumi.repository.flussi.record.model.util;

public class IngestionException {

  public static ErrDescription CLIENT_NOT_FOUND            = new ErrDescription(1000, "Codice cliente non presente");
  public static ErrDescription CAR_LICENSE_PLATE_NOT_FOUND = new ErrDescription(1001, "Targa non presente");
  public static ErrDescription INCOMPLETE_DATA             = new ErrDescription(1002, "Cliente o fornitore o articolo non presente");
  public static ErrDescription SUPPLIER_NOT_FOUND          = new ErrDescription(1003, "Codice fornitore non presente");
  public static ErrDescription AMOUNT_NULL                 = new ErrDescription(1004, "Importo nullo");

  public IngestionException() {
    super();
  }

  public static class ErrDescription {

    public long   Code        = 0;
    public String Description = "";

    public ErrDescription(long code, String description) {
      this.Code = code;
      this.Description = description;
    }

  }

}
