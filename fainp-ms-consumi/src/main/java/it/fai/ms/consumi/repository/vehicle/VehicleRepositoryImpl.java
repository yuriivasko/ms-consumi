package it.fai.ms.consumi.repository.vehicle;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAssociazioneDispositivoVeicolo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAssociazioneDispositivoVeicolo_;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo_;

@Repository
@Validated
@Transactional
public class VehicleRepositoryImpl implements VehicleRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final EntityManager       em;
  private final VehicleEntityMapper entityMapper;

  @Inject
  public VehicleRepositoryImpl(final EntityManager _entityManager, final VehicleEntityMapper _entityMapper) {
    em           = _entityManager;
    entityMapper = _entityMapper;
  }

  @Override
  public Optional<Vehicle> findVehicleByVehicleUuid(@NotNull String _vehicleUUID, @NotNull Instant _instant) {

    _log.debug(" Searching vehicle by vehicleUUID {} and instant {}", _vehicleUUID, _instant);

    var query = em.getCriteriaBuilder().createQuery(StoricoVeicolo.class);
    var root  = query.from(StoricoVeicolo.class);

    final var predicates = List
      .of(em.getCriteriaBuilder().equal(root.get(StoricoVeicolo_.DML_UNIQUE_IDENTIFIER), _vehicleUUID),
          em.getCriteriaBuilder().lessThanOrEqualTo(root.get(StoricoVeicolo_.DML_REVISION_TIMESTAMP), _instant))
      .toArray(new Predicate[0]);

    query.where(predicates);
    query.orderBy(em.getCriteriaBuilder().desc(root.get(StoricoVeicolo_.DML_REVISION_TIMESTAMP)));

    var optionalVehicle = em.createQuery(query).getResultStream().map(s -> entityMapper.toDomain(s)).findFirst();

    _log.debug(" Found : {}", optionalVehicle);

    return optionalVehicle;
  }

  public Optional<String> findIdVeicoloFromStoricoAssociazioneDVForDeviceAtDate(String deviceIdentifier, Instant dataRiferimento) {
    CriteriaBuilder cb    = em.getCriteriaBuilder();
    var             query = cb.createQuery(StoricoAssociazioneDispositivoVeicolo.class);

    var assDV = query.from(StoricoAssociazioneDispositivoVeicolo.class);
    query.where(cb.and(cb.equal(assDV.get(StoricoAssociazioneDispositivoVeicolo_.dmlUniqueIdentifier), deviceIdentifier)),
                cb.lessThanOrEqualTo(assDV.get(StoricoAssociazioneDispositivoVeicolo_.dataAssociazione), dataRiferimento));

    query.orderBy(cb.desc(assDV.get(StoricoAssociazioneDispositivoVeicolo_.dataAssociazione)));

    return em.createQuery(query).setFirstResult(0).setMaxResults(1).getResultStream().findFirst().map(a -> a.getVeicoloDmlUniqueIdentifier());

  }

  @Override
  public Optional<StoricoVeicolo> findVeicoloForDeviceAtDate(String deviceIdentifier, Instant dataRiferimento) {
    return findIdVeicoloFromStoricoAssociazioneDVForDeviceAtDate(deviceIdentifier, dataRiferimento).flatMap(s -> {
      CriteriaBuilder cb    = em.getCriteriaBuilder();
      var             query = cb.createQuery(StoricoVeicolo.class);

      var assDV = query.from(StoricoVeicolo.class);
      query.where(cb.and(cb.equal(assDV.get(StoricoVeicolo_.dmlUniqueIdentifier), s),
                  cb.lessThanOrEqualTo(assDV.get(StoricoVeicolo_.dmlRevisionTimestamp), dataRiferimento)));

      query.orderBy(cb.desc(assDV.get(StoricoVeicolo_.dmlRevisionTimestamp)));

      return em.createQuery(query).getResultStream().findFirst();

    });
  }
}
