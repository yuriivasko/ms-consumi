package it.fai.ms.consumi.domain;

import java.util.Objects;

public class Transaction {

  private String detailCode;
  private String sign = "+";

  public Transaction() {
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Transaction obj = getClass().cast(_obj);
      res = Objects.equals(obj.detailCode, detailCode) && Objects.equals(obj.sign, sign);
    }
    return res;
  }

  public String getDetailCode() {
    return detailCode;
  }

  public String getSign() {
    return sign;
  }

  @Override
  public int hashCode() {
    return Objects.hash(detailCode, sign);
  }

  public void setDetailCode(final String _detailCode) {
    detailCode = _detailCode;
  }

  public void setSign(final String _sign) {
    sign = _sign;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("detailCode=")
                              .append(detailCode)
                              .append(",sign=")
                              .append(sign)
                              .append("]")
                              .toString();
  }

}
