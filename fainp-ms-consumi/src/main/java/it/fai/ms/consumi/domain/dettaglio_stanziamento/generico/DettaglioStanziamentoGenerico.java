package it.fai.ms.consumi.domain.dettaglio_stanziamento.generico;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;

public class DettaglioStanziamentoGenerico
  extends DettaglioStanziamento {

  public DettaglioStanziamentoGenerico(final GlobalIdentifier _globalIdentifier) {
    super(_globalIdentifier);
  }

  private String     partnerCode;
  private Instant    startDate;
  private BigDecimal quantity;
  private String     deviceCode;
  private Vehicle    vehicle;
  private String     route;
  private String     additionalInformation;
  private String     licensePlateOnRecord;
  private String     tipoDispositivo;
  private String     puntoErogazione;
  private String     km;

  public String getPartnerCode() {
    return partnerCode;
  }

  public void setPartnerCode(String partnerCode) {
    this.partnerCode = partnerCode;
  }

  public Instant getStartDate() {
    return startDate;
  }

  public void setStartDate(Instant startDate) {
    this.startDate = startDate;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getDeviceCode() {
    return deviceCode;
  }

  public void setDeviceCode(String deviceCode) {
    this.deviceCode = deviceCode;
  }

  public Vehicle getVehicle() {
    return vehicle;
  }

  public void setVehicle(Vehicle vehicle) {
    this.vehicle = vehicle;
  }

  public String getRoute() {
    return route;
  }

  public void setRoute(String route) {
    this.route = route;
  }

  public String getAdditionalInformation() {
    return additionalInformation;
  }

  public void setAdditionalInformation(String additionalInformation) {
    this.additionalInformation = additionalInformation;
  }

  public void setLicensePlateOnRecord(String licensePlateOnRecord) {
    this.licensePlateOnRecord = licensePlateOnRecord;
  }

  public String getLicensePlateOnRecord() {
    return licensePlateOnRecord;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }
}
