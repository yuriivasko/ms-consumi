package it.fai.ms.consumi.repository.storico_dml.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.anagazienda.dto.AziendaDMLDTO;
import it.fai.ms.common.dml.mappable.DmlListenerMapper;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoAzienda;

@Component
public class StoricoAziendaDmlListenerMapper
  implements DmlListenerMapper<AziendaDMLDTO,StoricoAzienda> {

  @Override
  public StoricoAzienda toORD(AziendaDMLDTO dto, StoricoAzienda azienda) {

    if (azienda == null) {
      azienda = new StoricoAzienda();
    }

    azienda.setDmlUniqueIdentifier(dto.getCodiceAzienda());
    azienda.setDmlRevisionTimestamp(dto.getDmlRevisionTimestamp());
    azienda.setStatoAzienda(dto.getStato());

    return azienda;
  }
}
