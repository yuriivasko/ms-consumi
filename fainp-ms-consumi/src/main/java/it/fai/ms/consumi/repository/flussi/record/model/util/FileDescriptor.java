package it.fai.ms.consumi.repository.flussi.record.model.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FileDescriptor {

  private String source_dir;
  private String backup_dir;

  private boolean hasHead = true;
  private boolean hasTail = true;

  private List<StringItem> items = new ArrayList<>();

  public FileDescriptor() {
    super();
  }

  public String getSource_dir() {
    return source_dir;
  }

  public void setSource_dir(String source_dir) {
    this.source_dir = source_dir;
  }

  public String getBackup_dir() {
    return backup_dir;
  }

  public void setBackup_dir(String backup_dir) {
    this.backup_dir = backup_dir;
  }

  public boolean isHasHead() {
    return hasHead;
  }

  public void setHasHead(boolean hasHead) {
    this.hasHead = hasHead;
  }

  public boolean isHasTail() {
    return hasTail;
  }

  public void setHasTail(boolean hasTail) {
    this.hasTail = hasTail;
  }

  public List<StringItem> getItems() {
    return items;
  }

  public void setItems(List<StringItem> items) {
    this.items = items;
  }
  
  public void skipItem(String name, int start, int end) {
    //just do not add to items (do nothing)
  }

  public void addItem(String name, int start, int end) {
    this.items.add(new StringItem(name, start, end));
  }

  public Optional<StringItem> getItem(String itemName) {
    return items.stream()
                .filter(item -> item.getMethod()
                                    .equals(itemName))
                .findFirst();
  }

}
