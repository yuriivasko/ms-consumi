package it.fai.ms.consumi.domain.consumi;

import it.fai.ms.consumi.domain.Device;

public interface DeviceSupplier {

  Device getDevice();

}
