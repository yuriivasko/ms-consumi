package it.fai.ms.consumi.client;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import feign.FeignException;
import feign.Response;
import feign.codec.Decoder;
import feign.codec.ErrorDecoder;
import it.fai.ms.consumi.config.FeignConfiguration;
import it.fai.ms.consumi.domain.navision.StanziamentoError;

@Component
public class FeignErrorInterceptor implements ErrorDecoder {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ErrorDecoder defaultErrorDecoder = new Default();
  private final Decoder defaultDecoder = FeignConfiguration.buildSpringDecoder();


  @Override
  public Exception decode(String methodKey, Response response) {
	  
	  StringBuilder sb = new StringBuilder("Error on call feign client: ");
	    sb.append("\n");
	    sb.append("Response ");
	    sb.append(methodKey + ": ");
	    sb.append((response != null) ? response.request() : null);
	    sb.append("\n");
	    sb.append(methodKey + ": ");
	    sb.append(response);
	    sb.append("\n");
    
    try {
      Exception ex = null;
      if (methodKey.startsWith("StanziamentiApiAuthClient#stanziamentiPost")) {
        ex = decodeStanziamentiPostException(methodKey, response, sb.toString());
      }
  
      if (ex != null)
        return ex;
    }catch (Exception e) {
      log.error("Errore nella gestione degli errori rest",e);
    }

    log.error(sb.toString());
    return defaultErrorDecoder.decode(methodKey, response);
  }

  public StanziamentiPostException decodeStanziamentiPostException(String methodKey, Response response, String errorMessage) {
    StanziamentiPostException exception = null;
    if (response.status() == 400) {
      StanziamentoError obj;
      try {
        obj = (StanziamentoError)defaultDecoder.decode(response, StanziamentoError.class);
        exception = new StanziamentiPostException(obj, errorMessage);
      } catch (FeignException | IOException e) {
        log.error("Unable to Desiarilize response",e);
      }
    }
    return exception;
  }
}
