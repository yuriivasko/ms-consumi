package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.treno;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.treno.DettaglioStanziamentoTreno;

@Component
@Validated
public class DettaglioStanziamentoTrenoEntityMapper {

  public DettaglioStanziamentoTrenoEntityMapper() {
  }

  public DettaglioStanziamentoTreno toDomain(@NotNull final DettaglioStanziamentoTrenoEntity _entity) {
    var domain = new DettaglioStanziamentoTreno(new GlobalIdentifier(GlobalIdentifier.decodeType(_entity.getGlobalIdentifier())) {
      @Override
      public String getId() {
        return decodeId(_entity.getGlobalIdentifier());
      }

      @Override
      public GlobalIdentifier newGlobalIdentifier() {
        return newGlobalIdentifier();
      }
    });

    // TODO
    return domain;
  }

  public DettaglioStanziamentoTrenoEntity toEntity(@NotNull final DettaglioStanziamentoTreno _domain) {
    var entity = new DettaglioStanziamentoTrenoEntity(_domain.getGlobalIdentifier()
                                                             .encode());
    return entity;
  }

}
