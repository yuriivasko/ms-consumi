package it.fai.ms.consumi.domain;

import java.util.Objects;

public class VehicleLicensePlate {

  public static String NO_COUNTRY = "NO_COUNTRY";

  private final String countryId;
  private final String licenseId;

  public VehicleLicensePlate(final String _licenseId, final String _countryId) {
    licenseId = Objects.requireNonNull(_licenseId, "License id must be not null");
    countryId = Objects.requireNonNull(_countryId, "Country id must be not null");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final VehicleLicensePlate obj = getClass().cast(_obj);
      res = Objects.equals(obj.countryId, countryId) && Objects.equals(obj.licenseId, licenseId);
    }
    return res;
  }

  public String getCountryId() {
    return countryId;
  }

  public String getLicenseId() {
    return licenseId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(countryId, licenseId);
  }

  public String plate() {
    return countryId + "_" + licenseId;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("countryId=")
                              .append(countryId)
                              .append(",licenseId=")
                              .append(licenseId)
                              .append("]")
                              .toString();
  }

}
