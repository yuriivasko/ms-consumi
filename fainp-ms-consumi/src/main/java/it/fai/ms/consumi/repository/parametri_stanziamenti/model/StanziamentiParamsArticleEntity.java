package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class StanziamentiParamsArticleEntity {

  @Column(name = "codice_articolo", nullable = false)
  private final String code;

  @Column(name = "articolo_descrizione")
  private String description;

  protected StanziamentiParamsArticleEntity() {
    code = null;
  }

  public StanziamentiParamsArticleEntity(final String _code) {
    code = _code;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final StanziamentiParamsArticleEntity obj = getClass().cast(_obj);
      res = Objects.equals(obj.code, code);
    }
    return res;
  }

  public String getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }

  public void setDescription(final String _description) {
    description = _description;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("code=")
                              .append(code)
                              .append(",desc=")
                              .append(description)
                              .append("]")
                              .toString();
  }

}
