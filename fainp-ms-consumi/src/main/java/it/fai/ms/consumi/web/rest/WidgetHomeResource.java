package it.fai.ms.consumi.web.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.CacheControl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.ReportCarburantiDaFatturare;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi.ReportPedaggiDaFatturare;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;

@RestController
@RequestMapping(WidgetHomeResource.BASE_PATH)
public class WidgetHomeResource {

  static final String BASE_PATH = "/api/public/widget";

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String TOT_CARBURANTI_DA_FATTURARE = "/carburantidafatturare";
  private final String TOT_PEDAGGI_DA_FATTURARE    = "/pedaggidafatturare";

  private final ReportCarburantiDaFatturare reportCarburanteDaFatturare;
  private final ReportPedaggiDaFatturare    reportPedaggiDaFatturare;

  private final StanziamentiParamsService stanzianemtiParamService;

  public WidgetHomeResource(final ReportCarburantiDaFatturare _reportCarburanteDaFatturare,
                            final ReportPedaggiDaFatturare _reportPedaggiDaFatturare,
                            final StanziamentiParamsService _stanzianemtiParamService) {
    reportCarburanteDaFatturare = _reportCarburanteDaFatturare;
    reportPedaggiDaFatturare = _reportPedaggiDaFatturare;
    stanzianemtiParamService = _stanzianemtiParamService;
  }

  @GetMapping(TOT_CARBURANTI_DA_FATTURARE + "/{codiceCliente}")
  public ResponseEntity<List<String>> getTotalFuelToInvoice(@PathVariable("codiceCliente") String codiceCliente) {
    log.debug("Request to retrieve total fuel to invoice by codice cliente: {}", codiceCliente);

    Set<String> articleCodes = getArticleCodesByTipoDettaglioStanziamento(StanziamentiDetailsType.CARBURANTI);
    
    List<String> result = new ArrayList<>();
    result = reportCarburanteDaFatturare.getTotalFuelToInvoice(codiceCliente, articleCodes);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.maxAge(4, TimeUnit.HOURS))
                         .body(result);
  }

  @GetMapping(TOT_PEDAGGI_DA_FATTURARE + "/{codiceCliente}")
  public ResponseEntity<List<String>> getTotalTollToInvoice(@PathVariable("codiceCliente") String codiceCliente) {
    log.debug("Request to retrieve total Toll to invoice by codice cliente: {}", codiceCliente);

    Set<String> articleCodes = getArticleCodesByTipoDettaglioStanziamento(StanziamentiDetailsType.PEDAGGI);
    
    List<String> result = new ArrayList<>();
    result = reportPedaggiDaFatturare.getTotalTollToInvoice(codiceCliente, articleCodes);
    return ResponseEntity.ok()
                         .cacheControl(CacheControl.maxAge(4, TimeUnit.HOURS))
                         .body(result);
  }

  private Set<String> getArticleCodesByTipoDettaglioStanziamento(StanziamentiDetailsType type) {
    Set<String> articleCodes = stanzianemtiParamService.findArticleCodesByTipoDettaglioStanziamento(type);
    return articleCodes;

  }

}
