package it.fai.ms.consumi.service.validator;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;

import javax.validation.constraints.NotNull;

public interface TrxConsumoValidatorService
  extends ConsumoValidatorService{

  ValidationOutcome validate(@NotNull Consumo consumo);

}
