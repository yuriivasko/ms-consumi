package it.fai.ms.consumi.service.validator;

import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.TrustedCustomerSupplier;
import it.fai.ms.consumi.domain.VehicleAssociatedWithDeviceSupplier;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.ContractSupplier;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.consumi.ParamStanziamentoMatchingParamsSupplier;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.validator.internal.ContractValidator;
import it.fai.ms.consumi.service.validator.internal.DeviceValidator;

@Service
@Validated
@Primary
public class ConsumoBlockingValidatorServiceImpl implements ConsumoBlockingValidatorService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final ContractValidator contractValidator;
  private final DeviceValidator deviceValidator;
  private final StanziamentiParamsValidator stanziamentiParamsValidator;

  @Inject
  public ConsumoBlockingValidatorServiceImpl(final ContractValidator _contractValidator, final DeviceValidator _deviceValidator,
                                             final StanziamentiParamsValidator _stanziamentiParamsValidator) {
    contractValidator = _contractValidator;
    deviceValidator = _deviceValidator;
    stanziamentiParamsValidator = _stanziamentiParamsValidator;
  }

  @Override
  public ValidationOutcome validate(@NotNull final Consumo _consumo) {
    //regole su:
    //https://exagespa.atlassian.net/wiki/spaces/FAIMYS/pages/163184653/Consumi+-+Regole+generali+e+ERRORI+bloccanti+e+non#Consumi-RegolegeneralieERRORIbloccantienon-ERRORIBLOCCANTI(impedisconolacreazionedidettaglioestanziamento)

    final var validationOutcome = new ValidationOutcome();

    _log.trace("starting validateStanziamentiParams");
    final var validationForStanziamentiParams = validateStanziamentiParams(_consumo);
    validationOutcome.addValidationOutcomeMessages(validationForStanziamentiParams);

    if (!validationOutcome.isValidationOk()) {
      validationOutcome.setFatalError(true);
      _log.error("Blocking validations with StanziamentiParams! : {}",validationOutcome);
      //inutile proseguire col resto
      return validationOutcome;
    }

    _log.trace("starting getDeviceFromConsumo");
    Optional<Device> deviceOptional = getDeviceFromConsumo(_consumo);
    ValidationOutcome deviceValidationOutcome = null;
    if (deviceOptional.isPresent()) {
      deviceValidationOutcome = deviceValidator.findAndSetDevice(deviceOptional.get());
      validationOutcome.addValidationOutcomeMessages(
        Optional.ofNullable(deviceValidationOutcome)
      );
      //OK - Setta il contratto dal device
      setContractIdByDevice(_consumo);
    }

    _log.trace("starting validateAndSetContract");
    //FIXME qui trova il contratto tramite il numero presente nel consumo, andrebbe agginuto il produttore
    //Setta il contratto
    final var validationForContract = validateAndSetContract(_consumo);
    validationOutcome.addValidationOutcomeMessages(validationForContract);

    _log.trace("starting validateAndSetDeviceByVehicle");
    if (deviceValidationOutcome == null && !_consumo.getContract().isPresent()) {
      //OK - Setta il contratto dal device
      final var validateForDeviceByVehicle = validateAndSetDeviceByVehicle(_consumo);
      validationOutcome.addValidationOutcomeMessages(validateForDeviceByVehicle);
    }

    _log.trace("starting validateCustomer");
    final var validationForCustomer = validateCustomer(_consumo);
    validationOutcome.addValidationOutcomeMessages(validationForCustomer);

    // se almeno una validazione è KO, il consumo deve essere bloccato
    if (!validationOutcome.isValidationOk()) {
      validationOutcome.setFatalError(true);
    } else {
      // altrimenti verifico che il company code sia stato settato
      if (!(_consumo instanceof TrustedCustomerSupplier)) {
        if (_consumo.getContract() == null ||
          !_consumo.getContract().isPresent() ||
          _consumo.getContract().get().getCompanyCode() == null ||
          _consumo.getContract().get().getCompanyCode().isEmpty()) {
          String msg = "Customer not found for consumo " + _consumo;
          _log.error(msg);
          validationOutcome.addMessage(new Message("b007").withText(msg));
          validationOutcome.setFatalError(true);
        }
      }
      validationOutcome.setStanziamentiParams(validationForStanziamentiParams.get().getStanziamentiParamsOptional().get());
    }

    _log.debug("Blocking validations result : {}", validationOutcome);
    return validationOutcome;
  }


  private Optional<ValidationOutcome> validateAndSetDeviceByVehicle(final Consumo _consumo) {
    Optional<ValidationOutcome> optionalValidationOutcome;


    if (_consumo instanceof VehicleAssociatedWithDeviceSupplier) {
      final VehicleAssociatedWithDeviceSupplier vehicleAssociatedWithDeviceSupplier = (VehicleAssociatedWithDeviceSupplier) _consumo;

      optionalValidationOutcome = Optional.ofNullable(
        deviceValidator.findAndSetDevice(
          vehicleAssociatedWithDeviceSupplier.getVehicle(),
          vehicleAssociatedWithDeviceSupplier.getDevice(),
          vehicleAssociatedWithDeviceSupplier.getDate()
        )
      );

      //TODO è corretto?
      //se la validazione non ha trovato un device valido e se il device non contiene un codice contratto
      getDeviceFromConsumoAfterVehicleSet(_consumo)
        .map(d -> d.getContractNumber())
        .map(s -> new Contract(s))
        .ifPresentOrElse(contract -> {
          //if isPresent
          ValidationOutcome validationOutcomeContract = contractValidator.findAndSetContract(contract);
          if (validationOutcomeContract.isValidationOk()) {
            _log.trace("Found contract from device by vehicle");
            _consumo.setContract(Optional.of(contract));
          } else {
            _log.trace("No valid contract found from device by vehicle");
          }
        },
          //orElse
          () -> _log.trace("Cannot find contract from device by vehicle")
        );

    } else {
      _log.trace("{} isn't a vehicle and devide supplier", _consumo);
      optionalValidationOutcome = Optional.empty();
    }

    return optionalValidationOutcome;
  }

  private Optional<Device> getDeviceFromConsumo(final Consumo consumo) {
    if (consumo instanceof DeviceSupplier) {
    	Optional<Device> dev = Optional.ofNullable(((DeviceSupplier) consumo).getDevice());
    	if(dev.isPresent()) {
    		if( consumo.getContract() != null && consumo.getContract().isPresent() && consumo.getContract().get().getId() != null) {
    			dev.get().setContractNumber(consumo.getContract().get().getId());
    		}else {
    			dev.get().setContractNumber(null);
    		}
    	}
    	return dev;
    }
    return Optional.empty();
  }
  
  private Optional<Device> getDeviceFromConsumoAfterVehicleSet(final Consumo consumo) {
	    if (consumo instanceof DeviceSupplier) {
	      return Optional.ofNullable(((DeviceSupplier) consumo).getDevice());
	    }
	    return Optional.empty();
	  }

  private Optional<ValidationOutcome> validateAndSetContract(final ContractSupplier _consumo) {
    Optional<ValidationOutcome> optionalValidationOutcome = _consumo.getContract()
      .map(contract -> {
        _log.trace("Contract code to validate : {}", contract.getId());
        return contractValidator.findAndSetContract(contract);
      });
    _log.trace("Found contract code:=[{}]", _consumo.getContract());
    return optionalValidationOutcome;
  }

  private void setContractIdByDevice(final Consumo _consumo) {
    if (_consumo instanceof DeviceSupplier) {
      final Device device = ((DeviceSupplier) _consumo).getDevice();
      if (device != null && device.getContractNumber() != null && (_consumo.getContract()==null || !_consumo.getContract().isPresent() || _consumo.getContract().get().getId()==null)) {

          // uso il contratto trovato sul dispositivo (la validazione di coerenza è stata fatta in precedenza)
          _consumo.setContract(Optional.of(new Contract(device.getContractNumber())));
      }
    }
  }

  /*
    Mail: Delucidazioni controlli bloccanti acquisizione consumi
    Vincenzo:
    Ci stiamo trovando nella situazione in cui abbiamo solo il codice supporto sul flusso che esiste lato nostro. Dunque la validazione passa il primo punto. Passa il secondo perchè c'è il codice supporto e salta il terzo perchè sulla transazione (ELCEU) non c'è il codice contratto.
    Morale, arriviamo al dettaglio stanziamento senza codice cliente.
    Come si deve comportare il sistema in questo caso?

    Marina:
    Come da analisi, non avendo la possibilita’ di recuperare il codice cliente, genera un errore bloccante
    */
  public Optional<ValidationOutcome> validateCustomer(@NotNull Consumo _consumo) {
    var validationOutcome = new ValidationOutcome();

    if (_consumo instanceof TrustedCustomerSupplier) {
      final TrustedCustomerSupplier trustedCustomerSupplier = (TrustedCustomerSupplier) _consumo;
      Optional<String> optionalCustomerId = Optional.ofNullable(trustedCustomerSupplier.getCustomer())
        .flatMap(
          customer -> Optional.ofNullable(StringUtils.trimToNull(customer.getId()))
        );
      if (!optionalCustomerId.isPresent()) {
        var message = new Message("b007").withText("Customer not found: " + trustedCustomerSupplier.getCustomer());
        validationOutcome.addMessage(message);
        validationOutcome.setFatalError(true);
      }
    }

    return Optional.of(validationOutcome);
  }

  private Optional<ValidationOutcome> validateStanziamentiParams(final ParamStanziamentoMatchingParamsSupplier _paramStanziamentoMatchingParamsSupplier) {
    _log.trace("Record code to validate for param stanziamenti : {}", _paramStanziamentoMatchingParamsSupplier.getRecordCodeToLookInParamStanz());
    return Optional.ofNullable(stanziamentiParamsValidator.existsParamsStanziamento(_paramStanziamentoMatchingParamsSupplier));
  }
}
