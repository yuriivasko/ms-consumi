package it.fai.ms.consumi.service.record.carburanti;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.consumi.carburante.ConsumoCarburante;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.TimeUtils;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class TrackyCardCarbStdRecordConsumoMapper extends AbstractRecordConsumoMapper
  implements RecordConsumoMapper<TrackyCardCarburantiStandardRecord, TrackyCardCarburantiStandard> {

  public static final String TRACKYCARD_STANDARD_SOURCE = "trackycard_standard";

  @Override
  public TrackyCardCarburantiStandard mapRecordToConsumo(TrackyCardCarburantiStandardRecord commonRecord) throws Exception {

    TrackyCardCarburantiStandardRecord tccsRecord = commonRecord;
    String pan = tccsRecord.getPanField1IsoCode() + tccsRecord.getPanField2GroupCode() + tccsRecord.getPanField3CustomerCode()
                 + tccsRecord.getPanField4DeviceNumber() + tccsRecord.getPanField5ControlDigit();


    // creation date/time in file (header) yyyyMMdd_dateformat, HHmmss_timeformat
    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(commonRecord, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant ingestionTime = tccsRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(commonRecord.getIngestion_time());

    Source source;
//    if (_source != null) {
//      source = _source;
//    } else if (_stanziamentiParams != null) {
//      source = new Source(ingestionTime, acquisitionDate, _stanziamentiParams.getSource());
//    } else {
      source = new Source(ingestionTime, acquisitionDate, TRACKYCARD_STANDARD_SOURCE);
//    }

    // CONSUMO.SOURCE
    source.setFileName(commonRecord.getFileName());


    source.setRowNumber(commonRecord.getRowNumber());
    TrackyCardCarburantiStandard toConsumo = new TrackyCardCarburantiStandard(source, commonRecord.getRecordCode(),
                                                                              Optional.of(new TrackyCardGlobalIdentifier(commonRecord.getGlobalIdentifier())), commonRecord);
    Consumo toConsumoBase = toConsumo;

    // CONSUMO.TRANSACTION
    Transaction transaction = new Transaction();
//    if (_stanziamentiParams != null){
//      transaction.setDetailCode(_stanziamentiParams.getTransactionDetail());
//    } else {
      transaction.setDetailCode(commonRecord.getProductCode());
//    }
    transaction.setSign(null); // Non disponibile nel passaggio Record -> Consumo
    toConsumoBase.setTransaction(transaction);

    toConsumoBase.setGroupArticlesNav(null);
    // toConsumoBase.setServicePartnerCode(null);// Non disponibile nel passaggio Record -> Consumo
    toConsumoBase.setRegion(null);

    ConsumoCarburante toConsumoCarburante = toConsumo;
    toConsumoCarburante.setProcessed(commonRecord.isProcessed());
    Device device = new Device(pan,TipoDispositivoEnum.TRACKYCARD);
    device.setPan(null); //per noi la trackycard non ha pan inteso come identificativo del servizio. Il pan della carta E' il seriale!

    device.setServiceType(TipoServizioEnum.CARBURANTI);

    toConsumoCarburante.setDevice(device);

    FuelStation fuelStation = new FuelStation(tccsRecord.getPointCode());
    fuelStation.setDescription(null);
    fuelStation.setFuelStationCode(tccsRecord.getPointCode());
    fuelStation.setPumpNumber(tccsRecord.getPumpNumber());
    toConsumoCarburante.setFuelStation(fuelStation);

    String date = (StringUtils.isEmpty(tccsRecord.getDate()) ? "19700101" : tccsRecord.getDate()); // AAAAMMGG
    String time = (StringUtils.isEmpty(tccsRecord.getTime()) ? "000000" : tccsRecord.getTime()); // HHMMSS
    Date d = TimeUtils.fromString(date + time, TimeUtils.DATE_FORMAT + TimeUtils.TIME_FORMAT);
    toConsumoCarburante.setEntryDateTime(d.toInstant());
    toConsumoCarburante.setOtherSupport(null); // TODO - Questo otherSupport serve???
    toConsumoCarburante.setCounter(NumericUtils.strToInt(tccsRecord.getCounter()));
    toConsumoCarburante.setQuantity(NumericUtils.strToBigDecimal(tccsRecord.getLitres(), -2));
    toConsumoCarburante.setKilometers(NumericUtils.strToBigDecimal(tccsRecord.getKilometers(), -2));
    toConsumoCarburante.setTicket(tccsRecord.getTicket());
    toConsumoCarburante.setProductCode(tccsRecord.getProductCode());

    Vehicle vehicle = new Vehicle();
    vehicle.setLicensePlate(null);// Non disponibile nel passaggio Record -> Consumo
    vehicle.setFareClass(null);// Non disponibile nel passaggio Record -> Consumo
    VehicleLicensePlate vlp = new VehicleLicensePlate("::licenseId::", "::countryId::");// Non disponibile nel passaggio
    // Record -> Consumo
    vehicle.setLicensePlate(vlp);
    toConsumoCarburante.setVehicle(vehicle);


    //    descriptor02.addItem("priceFaiReservedVatIncluded", 80, 86); //unit_price_vat_included
    MonetaryAmount value = tccsRecord.getMonetaryPriceReferenceVatIncluded();
    if ((value != null) && !value.isZero()) {
      Amount priceExposed = new Amount();
      priceExposed.setVatRate(new BigDecimal(tccsRecord.getVat_percentage_float()));
      priceExposed.setAmountIncludedVat(tccsRecord.getMonetaryPriceReferenceVatIncluded());
      priceExposed.setExchangeRate(null);// Non disponibile nel passaggio Record -> Consumo
      toConsumoCarburante.setPriceReference(priceExposed);
    }

    return toConsumo;
  }


}


