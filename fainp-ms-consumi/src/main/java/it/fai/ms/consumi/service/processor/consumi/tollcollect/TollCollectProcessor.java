package it.fai.ms.consumi.service.processor.consumi.tollcollect;

import it.fai.ms.consumi.domain.consumi.pedaggi.tollcollect.TollCollect;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;

public interface TollCollectProcessor  extends ConsumoProcessor<TollCollect> {
}
