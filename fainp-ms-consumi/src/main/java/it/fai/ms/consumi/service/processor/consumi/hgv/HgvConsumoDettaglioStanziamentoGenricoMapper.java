package it.fai.ms.consumi.service.processor.consumi.hgv;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
class HgvConsumoDettaglioStanziamentoGenricoMapper extends ConsumoDettaglioStanziamentoMapper {

  public HgvConsumoDettaglioStanziamentoGenricoMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamento dettaglioStanziamentoGenerico = null;
    if (_consumo instanceof Hgv) {
      Hgv hgvConsumo = (Hgv) _consumo;
      dettaglioStanziamentoGenerico = toDettaglioStanziamentoGenerico(hgvConsumo, hgvConsumo.getGlobalIdentifier());
    }
    return dettaglioStanziamentoGenerico;
  }

}
