package it.fai.ms.consumi.service.petrolpump;

import java.time.Instant;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.client.PetrolPumpService;
import it.fai.ms.consumi.config.ApplicationProperties;

@Service
@Transactional
public class PriceCalculatedByPriceTableService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private PetrolPumpService petrolPumpService;

  private ApplicationProperties applicationProperties;

  public PriceCalculatedByPriceTableService(final ApplicationProperties applicationProperties) {
    this.applicationProperties = applicationProperties;
  }
  
  public List<PriceTableDTO> getPriceTableList(String codiceFornitore,String codiceArticolo,Instant date) {
    log.info("Request getPriceTable by codiceFornitore: {}, codiceArticolo: {} and date: {}", codiceFornitore, codiceArticolo, date);
    String authorizationToken = applicationProperties.getAuthorizationHeader();
    log.info("authorizationToken: {}", authorizationToken);
    List<PriceTableDTO> res = null;
    try {
     res = petrolPumpService.getPriceTableList(authorizationToken, codiceFornitore, codiceArticolo, date);
     log.debug("getPriceTable Res {}", res);
    }catch (Throwable e) {
      log.error("Non è stato possibile chiamare Petrolpump con {} {} {} {}",authorizationToken, codiceFornitore, codiceArticolo, date,e);
      throw e;
    }

    log.info("PriceTableDTOs retrivied req:{} res:{}",res);
    return res;
  }

}
