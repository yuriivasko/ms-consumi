package it.fai.ms.consumi.service.processor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.service.CurrencyRateService;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.DettaglioStanziamentoProcessor;

@Service
@Transactional
//FIXME rinominare in StanziamentiPedaggiProducerImpl perchè è usato solo da pedaggi
public class StanziamentiProducerImpl implements StanziamentiProducer {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoProcessor allocationDetailProcessor;

  private final CurrencyRateService currencyRateService;

  @Inject
  public StanziamentiProducerImpl(final DettaglioStanziamentoProcessor _allocationDetailProcessor, CurrencyRateService _currencyRateService) {
    allocationDetailProcessor = _allocationDetailProcessor;
    currencyRateService = _currencyRateService;
  }
  @Override
  public List<Stanziamento> produce(final @NotNull DettaglioStanziamento _allocationDetail,
                                    final @NotNull StanziamentiParams _stanziamentiParams) {

    _log.debug("converting amount if needed...");
    addVatIfNeeded(_allocationDetail, _stanziamentiParams);
    convertAmount(_allocationDetail, _stanziamentiParams);
    final var optionalAllocationDetailProcessed = allocationDetailProcessor.process(_allocationDetail, _stanziamentiParams);

    final List<Stanziamento> allocations = new LinkedList<>();
    if (optionalAllocationDetailProcessed.isPresent()) {
      allocations.addAll(optionalAllocationDetailProcessed.get()
                                                          .getStanziamenti());
    }

    return allocations;
  }

  private void addVatIfNeeded(DettaglioStanziamento allocationDetail, StanziamentiParams stanziamentiParams) {
    if (allocationDetail.getAmount().getVatRateBigDecimal() == null ||
        allocationDetail.getAmount().getVatRateBigDecimal().setScale(2, RoundingMode.HALF_UP).equals(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP))) {
      final Float vatRate = stanziamentiParams.getArticle().getVatRate();
      final BigDecimal vatRate1 = new BigDecimal(vatRate);
      allocationDetail.getAmount().setVatRate(vatRate1);
    }
  }

  private void convertAmount(@NotNull DettaglioStanziamento allocationDetail,
                             @NotNull StanziamentiParams allocationParams) {
    final Amount amount = allocationDetail.getAmount();
    final Currency currency = allocationParams.getArticle().getCurrency();
    final Instant date = allocationDetail.getDate();
    _log.debug("current amount:=[{}], currency:=[{}], date:=[{}]", amount, currency, date);
    Amount convertedAmount = currencyRateService.convertAmount(amount, currency, date);
    allocationDetail.setAmount(convertedAmount);
    _log.debug("converted amount:=[{}]", convertedAmount);
  }
}
