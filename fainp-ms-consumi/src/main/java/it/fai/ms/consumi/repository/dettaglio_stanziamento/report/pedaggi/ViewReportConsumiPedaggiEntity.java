package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

import it.fai.common.enumeration.TipoDispositivoEnum;

@Entity(name="view_report_consumi_pedaggi")
@Immutable
public class ViewReportConsumiPedaggiEntity {

  @Column(name = "raggruppamento_articoli_nav")
  private String articlesGroup;

  @Column(name = "device_type")
  @Enumerated(EnumType.STRING)
  private TipoDispositivoEnum deviceType;

  @Column(name = "obu", nullable = true)
  private String deviceObu;

  @Column(name = "data_fattura")
  private Instant dataFattura;

  @Column(name = "data_erogazione_servizio")
  private LocalDate dataErogazioneServizio;

  @Column(name = "nr_cliente")
  private String numeroCliente;
  
  @Column(name = "nome_intestazione_tessere")
  private String nomeIntestazioneTessere;
  
  @Column(name = "tipo_movimento")
  private String tipoMovimento;

  @Column(name = "numero_fattura")
  private String numeroFattura;

  @Column(name = "veicolo_classe_euro")
  private String veicoloClasse;

  @Column(name = "veicolo_nazione_targa")
  private String veicoloNazioneTarga;

  @Column(name = "veicolo_targa")
  private String veicoloTarga;

  @Column(name = "exit_timestamp")
  private Instant exitTimestamp;

  @Column(name = "entry_global_gate_identifier_description")
  private String entryGlobalGateIdentifierDescription;

  @Column(name = "exit_global_gate_identifier_description")
  private String exitGlobalGateIdentifierDescription;

  @Column(name = "amount_novat", precision = 16, scale = 5)
  private BigDecimal amountExcludedVat;

  @Column(name = "amount_includingvat", precision = 16, scale = 5)
  private BigDecimal amountIncludedVat;

  @Column(name = "currency_code", nullable = false)
  private String currencyCode = "EUR";

  @Column(name = "row_number")
  private long sourceRowNumber;

  @Column(name = "record_code")
  private String recordCode;

  @Column(name = "entry_timestamp")
  private Instant entryTimestamp;

  @Column(name = "tratta")
  private String tratta;

  @Column(name = "entry_global_gate_identifier")
  private String entryGlobalGateIdentifier;

  @Column(name = "exit_global_gate_identifier")
  private String exitGlobalGateIdentifier;

  @Column(name = "perc_iva", precision = 19, scale = 2)
  private BigDecimal amountVatRate;

  @Column(name = "exchange_rate", precision = 16, scale = 5)
  private BigDecimal exchangeRate;

  @Id
  private String id;

  @Column(name = "codice_cliente_nav", nullable = false)
  private String customerId;
  
  @Column(name = "contract_code", nullable = true)
  private String contractCode;

  public ViewReportConsumiPedaggiEntity() {
    super();
  }

  public Instant getDataFattura() {
    return dataFattura;
  }

  public void setDataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
  }

  public String getNumeroCliente() {
    return numeroCliente;
  }

  public void setNumeroCliente(String numeroCliente) {
    this.numeroCliente = numeroCliente;
  }

  public String getNumeroFattura() {
    return numeroFattura;
  }

  public void setNumeroFattura(String numeroFattura) {
    this.numeroFattura = numeroFattura;
  }

  public String getVeicoloClasse() {
    return veicoloClasse;
  }

  public void setVeicoloClasse(String veicoloClasse) {
    this.veicoloClasse = veicoloClasse;
  }

  public String getVeicoloNazioneTarga() {
    return veicoloNazioneTarga;
  }

  public void setVeicoloNazioneTarga(String veicoloNazioneTarga) {
    this.veicoloNazioneTarga = veicoloNazioneTarga;
  }

  public String getVeicoloTarga() {
    return veicoloTarga;
  }

  public void setVeicoloTarga(String veicoloTarga) {
    this.veicoloTarga = veicoloTarga;
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public LocalDate getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public void setDataErogazioneServizio(LocalDate dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }



  public String getArticlesGroup() {
    return articlesGroup;
  }

  public void setArticlesGroup(String articlesGroup) {
    this.articlesGroup = articlesGroup;
  }

  public TipoDispositivoEnum getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(TipoDispositivoEnum deviceType) {
    this.deviceType = deviceType;
  }

  public String getDeviceObu() {
    return deviceObu;
  }

  public void setDeviceObu(String deviceObu) {
    this.deviceObu = deviceObu;
  }

  public Instant getExitTimestamp() {
    return exitTimestamp;
  }

  public void setExitTimestamp(Instant exitTimestamp) {
    this.exitTimestamp = exitTimestamp;
  }

  public String getEntryGlobalGateIdentifierDescription() {
    return entryGlobalGateIdentifierDescription;
  }

  public void setEntryGlobalGateIdentifierDescription(String entryGlobalGateIdentifierDescription) {
    this.entryGlobalGateIdentifierDescription = entryGlobalGateIdentifierDescription;
  }

  public String getExitGlobalGateIdentifierDescription() {
    return exitGlobalGateIdentifierDescription;
  }

  public void setExitGlobalGateIdentifierDescription(String exitGlobalGateIdentifierDescription) {
    this.exitGlobalGateIdentifierDescription = exitGlobalGateIdentifierDescription;
  }

  public BigDecimal getAmountExcludedVat() {
    return amountExcludedVat;
  }

  public void setAmountExcludedVat(BigDecimal amountExcludedVat) {
    this.amountExcludedVat = amountExcludedVat;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public long getSourceRowNumber() {
    return sourceRowNumber;
  }

  public void setSourceRowNumber(long sourceRowNumber) {
    this.sourceRowNumber = sourceRowNumber;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }

  public Instant getEntryTimestamp() {
    return entryTimestamp;
  }

  public void setEntryTimestamp(Instant entryTimestamp) {
    this.entryTimestamp = entryTimestamp;
  }

  public String getTratta() {
    return tratta;
  }

  public void setTratta(String tratta) {
    this.tratta = tratta;
  }

  public String getEntryGlobalGateIdentifier() {
    return entryGlobalGateIdentifier;
  }

  public void setEntryGlobalGateIdentifier(String entryGlobalGateIdentifier) {
    this.entryGlobalGateIdentifier = entryGlobalGateIdentifier;
  }

  public String getExitGlobalGateIdentifier() {
    return exitGlobalGateIdentifier;
  }

  public void setExitGlobalGateIdentifier(String exitGlobalGateIdentifier) {
    this.exitGlobalGateIdentifier = exitGlobalGateIdentifier;
  }

  public BigDecimal getAmountIncludedVat() {
    return amountIncludedVat;
  }

  public void setAmountIncludedVat(BigDecimal amountIncludedVat) {
    this.amountIncludedVat = amountIncludedVat;
  }

  public BigDecimal getAmountVatRate() {
    return amountVatRate;
  }

  public void setAmountVatRate(BigDecimal amountVatRate) {
    this.amountVatRate = amountVatRate;
  }

  public BigDecimal getExchangeRate() {
    return exchangeRate;
  }

  public void setExchangeRate(BigDecimal exchangeRate) {
    this.exchangeRate = exchangeRate;
  }

  public String getContractCode() {
    return contractCode;
  }

  public void setContractCode(String contractCode) {
    this.contractCode = contractCode;
  }

  public String getNomeIntestazioneTessere() {
    return nomeIntestazioneTessere;
  }

  public void setNomeIntestazioneTessere(String nomeIntestazioneTessere) {
    this.nomeIntestazioneTessere = nomeIntestazioneTessere;
  }

  public String getTipoMovimento() {
    return tipoMovimento;
  }

  public void setTipoMovimento(String tipoMovimento) {
    this.tipoMovimento = tipoMovimento;
  }
   
  
}

