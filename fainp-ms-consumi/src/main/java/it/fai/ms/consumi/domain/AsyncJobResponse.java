package it.fai.ms.consumi.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A AsyncJobResponse.
 */
@Entity
@Table(name = "async_job_response")
public class AsyncJobResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "status")
    private AsyncJobStatusEnum status;

    @Column(name = "result_data")
    private String resultData;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AsyncJobStatusEnum getStatus() {
        return status;
    }

    public AsyncJobResponse status(AsyncJobStatusEnum status) {
        this.status = status;
        return this;
    }

    public void setStatus(AsyncJobStatusEnum pending) {
        this.status = pending;
    }

    public String getResultData() {
        return resultData;
    }

    public AsyncJobResponse resultData(String resultData) {
        this.resultData = resultData;
        return this;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AsyncJobResponse asyncJobResponse = (AsyncJobResponse) o;
        if (asyncJobResponse.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), asyncJobResponse.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AsyncJobResponse{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", resultData='" + getResultData() + "'" +
            "}";
    }
}
