package it.fai.ms.consumi.service.processor.consumi.elvia;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.generici.telepass.ElviaGenerico;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class ElviaConsumoGenericoDettaglioStanziamentoGenericoMapper
  extends ConsumoDettaglioStanziamentoMapper {

  public ElviaConsumoGenericoDettaglioStanziamentoGenericoMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamento dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof ElviaGenerico) {
      ElviaGenerico elviaConsumo = (ElviaGenerico) _consumo;
      //TODO verificare se ci sono altri mappaggi solo per Elvia da aggiungere
      dettaglioStanziamentoPedaggio = toDettaglioStanziamentoGenerico(elviaConsumo, elviaConsumo.getGlobalIdentifier());
    }
    return dettaglioStanziamentoPedaggio;
  }

}
