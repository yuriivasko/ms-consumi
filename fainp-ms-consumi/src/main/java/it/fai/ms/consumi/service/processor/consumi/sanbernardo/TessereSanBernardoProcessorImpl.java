package it.fai.ms.consumi.service.processor.consumi.sanbernardo;

import java.math.BigDecimal;
import java.time.temporal.TemporalAccessor;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.apache.commons.lang3.StringUtils;
import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.AmountBuilder;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.domain.navision.PricesByNavDTO;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.jms.producer.CambioStatoDaConsumoJmsProducer;
import it.fai.ms.consumi.service.navision.PriceCalculatedByNavService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class TessereSanBernardoProcessorImpl extends ConsumoAbstractProcessor implements TessereSanBernardoProcessor {

  private final transient Logger log = LoggerFactory.getLogger(getClass());

  private static final String DEFAULT_RECORD_CODE = "DEFAULT";

  private final StanziamentiProducer stanziamentiProducer;
  private final TessereSanBernardoConsumoDettaglioStanziamentoMapper dettaglioStanziamentoMapper;
  private final PriceCalculatedByNavService priceCalculatedByNavService;

  private final CambioStatoDaConsumoJmsProducer cambioStatoProducer;


  public TessereSanBernardoProcessorImpl(ConsumoValidatorService _consumoValidatorService,
                                         VehicleService _vehicleService,
                                         CustomerService _customerService,
                                         StanziamentiProducer stanziamentiProducer,
                                         TessereSanBernardoConsumoDettaglioStanziamentoMapper dettaglioStanziamentoMapper,
                                         PriceCalculatedByNavService priceCalculatedByNavService, CambioStatoDaConsumoJmsProducer cambioStatoProducer) {
    super(_consumoValidatorService,  _vehicleService, _customerService);
    this.stanziamentiProducer = stanziamentiProducer;
    this.dettaglioStanziamentoMapper = dettaglioStanziamentoMapper;
    this.priceCalculatedByNavService = priceCalculatedByNavService;
    this.cambioStatoProducer = cambioStatoProducer;
  }

  @Override
  public ProcessingResult validateAndProcess(SanBernardo sanBernardo, Bucket bucket) {
    log.info(">");
    log.info("{} processing start {}", bucket, sanBernardo);
    log.info("  --> {}", sanBernardo);

    var processingResult = validateAndNotify(sanBernardo);

    log.debug(" - {} processing validation completed", sanBernardo);

    final var consumo = processingResult.getConsumo();
    //attach amount / prices + codice fornitore nav +
//        StanziamentiParamsQuery query = new StanziamentiParamsQuery(SOURCE_TYPE, DEFAULT_RECORD_CODE);
//        StanziamentiParams stanziamentiParams = stanziamentiParamsService.findByQuery(query).orElseThrow(()->new StanziamentiParamsNotFoundException("Source: "+SOURCE_TYPE+" and record code "+DEFAULT_RECORD_CODE+" not found"));
    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
        .ifPresent(stanziamentiParams -> {

                      /*
                      PricesByNavDTO requestPriceDTORicavo = createPriceByNavDTO(
                                stanziamentiParams,
                                customer.getId(),
                                stanziamentiParams.getSupplier().getCode(),
                                CostoRicavo.R,
                                consumo.getDate(),
                                null);
                        Number priceNoIvaRicavo = priceCalculatedByNavService.getCalculatedPriceByNav(requestPriceDTORicavo);
                      PricesByNavDTO requestPriceDTOCosto = createPriceByNavDTO(
                        stanziamentiParams,
                        customer.getId(),
                        stanziamentiParams.getSupplier().getCode(),
                        CostoRicavo.C,
                        consumo.getDate(),
                        null);
                      Number priceNoIvaCosto = priceCalculatedByNavService.getCalculatedPriceByNav(requestPriceDTOCosto);
                        MonetaryAmount amountExcludedVat  = Money.of(priceNoIva,stanziamentiParams.getArticle().getCurrency().getCurrencyCode());
                        Amount amount = AmountBuilder.builder()
                                .amountExcludedVat(amountExcludedVat)
                                .vatRate(new BigDecimal(stanziamentiParams.getArticle().getVatRateBigDecimal()))
                                .build();
                       */
          SanBernardo consumoSanBernardo = (SanBernardo) consumo;
          Customer customer = StringUtils.isBlank(consumoSanBernardo.getCustomerId())?getCustomer(consumoSanBernardo.getDevice()).get():new Customer(consumoSanBernardo.getCustomerId());
          String fareClass = consumoSanBernardo.getVehicle().getFareClass();
          String code = stanziamentiParams.getArticle().getCode();
          PricesByNavDTO requestPriceDTO = createPriceByNavDTO(
            code,
            fareClass,
            customer.getId(),
            stanziamentiParams.getSupplier().getCode(),
            CostoRicavo.R, //stanziamentiParams.getCostoRicavo(), //di default CR diventa C, mentre per questo flusso la chiamata va fatta per R, quindi lo forzo.
            consumo.getDate(),
            null);
          Number priceNoIva = priceCalculatedByNavService.getCalculatedPriceByNav(requestPriceDTO);
          MonetaryAmount amountExcludedVat = Money.of(priceNoIva, stanziamentiParams.getArticle().getCurrency().getCurrencyCode());
          Amount amount = AmountBuilder.builder()
            .amountExcludedVat(amountExcludedVat)
            .vatRate(new BigDecimal(stanziamentiParams.getArticle().getVatRate()))
            .build();
          consumo.setAmount(amount);

          final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo, dettaglioStanziamentoMapper);
          final var stanziamenti = stanziamentiProducer.produce(dettaglioStanziamento, stanziamentiParams);
          processingResult.getStanziamenti().addAll(stanziamenti);
        });
    } else {
      log.warn(" - {} can't be processed", consumo);
    }
    if (!processingResult.getValidationOutcome().isFatalError() && consumo.getDevice()!=null) {
      TipoDispositivoEnum type = consumo.getDevice().getType();
      if (type == TipoDispositivoEnum.BUONI_GRAN_SAN_BERNARDO) {
        cambioStatoProducer.sendFatturato(consumo.getDevice(), consumo.getDate());
      }
    }

    log.info("  --> {}", processingResult);
    log.info("< processing completed {}", sanBernardo.getSource());
    log.info("<");

    return processingResult;
  }

  private Optional<Customer> getCustomer(Device device) {
    Optional<Customer> customerOpt = Optional.empty();
    String contractNumberInDevice = device.getContractNumber();
    if (contractNumberInDevice != null) {
      customerOpt = customerService.findCustomerByContrattoNumero(contractNumberInDevice);
    }
    return customerOpt.isPresent() ? customerOpt : customerService.findCustomerByDeviceSeriale(device.getId(), device.getType());
  }

  private PricesByNavDTO createPriceByNavDTO(String code, String fareClass, String codiceCliente, String codiceFornitoreNav, CostoRicavo costoRicavoEnum, TemporalAccessor entryDateTime, Number importoNoIvaRiferimento) {
    PricesByNavDTO req = new PricesByNavDTO();
    req.setClasseTariffaria(fareClass);
    req.setCodiceArticoloNav(code);
    req.setCodiceCliente(codiceCliente);
    req.setCodiceFornitoreNav(codiceFornitoreNav);
    PricesByNavDTO.CostoRicavoEnum costoRicavoEnumNav = costoRicavoEnum.equals(CostoRicavo.R) ? PricesByNavDTO.CostoRicavoEnum.NUMBER_1 : PricesByNavDTO.CostoRicavoEnum.NUMBER_0;
    req.setCostoRicavo(costoRicavoEnumNav); // 0 = Costo (passivo), 1 = Ricavo (attivo)
    req.setDataDiRiferimento(PriceCalculatedByNavService.FORMATTER.format(entryDateTime));
    req.setImportoNoIvaRiferimento(importoNoIvaRiferimento);

    return req;
  }


}
