package it.fai.ms.consumi.client;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.consumi.domain.navision.PricesByNavDTO;
import it.fai.ms.consumi.domain.navision.PricesByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.PutAllegatiByNavReturnDTO;
import it.fai.ms.consumi.domain.navision.Stanziamento;

public class StanziamentiApiServiceReadOnlyImpl
 implements StanziamentiApiService {

  private Logger                    log = LoggerFactory.getLogger(getClass());
  
  private final StanziamentiApiAuthClient stanziamentiApiAuthClient;

  public StanziamentiApiServiceReadOnlyImpl(StanziamentiApiAuthClient stanziamentiApiAuthClient) {
    log.warn(" *** USING READONLY IMPLEMENTATION of StanziamentiApiService! *** ");
    this.stanziamentiApiAuthClient = stanziamentiApiAuthClient;

  }

  @Override
  public void stanziamentiPost(String authorizationToken, List<Stanziamento> righe) {
    log.warn(" *** MOCKED IMPLEMENTATION! *** ");
    log.warn(" real client will sent to stanziamentiPost righe:=[{}] ", righe);
    log.warn(" *** MOCKED IMPLEMENTATION! *** ");
  }

  @Override
  public PutAllegatiByNavReturnDTO putAllegatiByNav(String requestHeader, String numeroFattura, List<String> codiciAllegati) {
    log.warn(" *** MOCKED IMPLEMENTATION! *** ");
    log.warn(" real client will sent to putAllegatiByNav numeroFattura:=[{}], codiciAllegati:=[{}] ", numeroFattura, codiciAllegati);
    final PutAllegatiByNavReturnDTO putAllegatiByNavReturnDTO = new PutAllegatiByNavReturnDTO();
    putAllegatiByNavReturnDTO.setResultCode("");
    putAllegatiByNavReturnDTO.setResultMessage("");
    log.warn(" returning fake response:=[{}]", putAllegatiByNavReturnDTO);
    log.warn(" *** MOCKED IMPLEMENTATION! *** ");
    return putAllegatiByNavReturnDTO;
  }

  public PricesByNavReturnDTO getPricesByNavPost(String requestHeader, PricesByNavDTO pricesByNavDTO) {
    return stanziamentiApiAuthClient.getPricesByNavPost(requestHeader, pricesByNavDTO);
  }

}
