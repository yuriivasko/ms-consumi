package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.util.Currency;
import java.util.Objects;
import java.util.Optional;

import javax.validation.constraints.NotNull;

public class Article implements Serializable {
  private static final long serialVersionUID = -8643760036099650894L;

  private final String code;
  @NotNull
  private String       country;
  @NotNull
  private Currency     currency    = Currency.getInstance("EUR");
  private String       description = "";
  @NotNull
  private String       grouping;
  private String       measurementUnit;
  @NotNull
  private Float        vatRate;

  public Article(final String _code) {
    code = Objects.requireNonNull(_code, "Parameter code is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Article obj = getClass().cast(_obj);
      res = Objects.equals(obj.code, code) && Objects.equals(obj.vatRate, vatRate) && Objects.equals(obj.country, country)
            && Objects.equals(obj.grouping, grouping) && Objects.equals(obj.measurementUnit, measurementUnit);
    }
    return res;
  }

  public String getCode() {
    return code;
  }

  public String getCountry() {
    return country;
  }

  public Currency getCurrency() {
    return currency;
  }

  public String getDescription() {
    return description;
  }

  public String getGrouping() {
    return grouping;
  }

  public String getMeasurementUnit() {
    return measurementUnit;
  }

  public Float getVatRate() {
    return vatRate;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, vatRate, country, grouping, measurementUnit);
  }

  public void setCountry(final String _country) {
    country = _country;
  }

  public void setCurrency(final Currency _currency) {
    currency = _currency;
  }

  public void setDescription(final String _description) {
    description = Optional.ofNullable(_description)
                          .orElse("");
  }

  public void setGrouping(final String _grouping) {
    grouping = _grouping;
  }

  public void setMeasurementUnit(String _measurementUnit) {
    measurementUnit = _measurementUnit;
  }

  public void setVatRate(final Float _vatRate) {
    vatRate = _vatRate;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("code=")
                              .append(code)
                              .append(",description=")
                              .append(description)
                              .append(",grouping=")
                              .append(grouping)
                              .append(",country=")
                              .append(country)
                              .append(",")
                              .append(currency)
                              .append(",vatRate=")
                              .append(vatRate)
                              .append(",measurementUnit=")
                              .append(measurementUnit)
                              .append("]")
                              .toString();
  }

}
