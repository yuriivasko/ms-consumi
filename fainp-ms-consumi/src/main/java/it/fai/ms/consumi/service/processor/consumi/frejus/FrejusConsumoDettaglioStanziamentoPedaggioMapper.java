package it.fai.ms.consumi.service.processor.consumi.frejus;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.pedaggi.tunnel.Frejus;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class FrejusConsumoDettaglioStanziamentoPedaggioMapper extends ConsumoDettaglioStanziamentoMapper<Frejus, DettaglioStanziamentoPedaggio> {



  public FrejusConsumoDettaglioStanziamentoPedaggioMapper() {
    super();
  }

  @Override
  public DettaglioStanziamentoPedaggio mapConsumoToDettaglioStanziamento(@NotNull final Frejus _consumo) {
    DettaglioStanziamentoPedaggio dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof Frejus) {
      var sitaf = (Frejus) _consumo;
      dettaglioStanziamentoPedaggio = toDettaglioStanziamentoPedaggio(sitaf, sitaf.getGlobalIdentifier());

    }
    return dettaglioStanziamentoPedaggio;
  }
}