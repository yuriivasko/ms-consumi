package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.service.NotConfirmedTemporaryAllocationsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.generici.telepass.ElviaGenerico;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElviaRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaGenericoProcessor;
import it.fai.ms.consumi.service.processor.consumi.elvia.ElviaProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoGenericoMapper;
import it.fai.ms.consumi.service.record.telepass.ElviaRecordConsumoMapper;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

import java.time.Instant;
import java.util.Map;
import java.util.Optional;

/*
 * Pedaggi Italia Telepass Definitivo (Elvia sono i definitivi)
 */
@Service(ElviaJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class ElviaJob
  extends AbstractJob<ElviaRecord, Elvia> {

  public final static String QUALIFIER = "elviaJob";

  private final transient Logger log = LoggerFactory.getLogger(getClass());

  @Value("${abstractjob.disablecaso3}")
  public String DEVCONF_DISABLECASO3 = "false";

  private final ConsumoProcessor<ElviaGenerico>                 consumoGenericoProcessor;
  private final RecordConsumoMapper<ElviaRecord, ElviaGenerico> recordConsumoGenericoMapper;
  private final NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService;

  public ElviaJob(final PlatformTransactionManager transactionManager,
                  final StanziamentiParamsValidator stanziamentiParamsValidator,
                  final NotificationService notificationService,
                  final RecordPersistenceService persistenceService,
                  final ElviaDescriptor recordDescriptor,

                  final ElviaProcessor defaultProcessor,
                  final ElviaRecordConsumoMapper defaultRecordConsumoMapper,

                  final ElviaGenericoProcessor consumoGenericoProcessor,
                  final ElviaRecordConsumoGenericoMapper recordConsumoGenericoMapper,

                  final StanziamentiToNavPublisher stanziamentiToNavJmsProducer,
                  final NotConfirmedTemporaryAllocationsService notConfirmedTemporaryAllocationsService) {

    super("ELVIA",transactionManager, stanziamentiParamsValidator, notificationService, persistenceService,
          recordDescriptor, defaultProcessor, defaultRecordConsumoMapper, stanziamentiToNavJmsProducer);

    this.recordConsumoGenericoMapper = recordConsumoGenericoMapper;
    this.consumoGenericoProcessor = consumoGenericoProcessor;
    this.notConfirmedTemporaryAllocationsService = notConfirmedTemporaryAllocationsService;

  }

  @Override
  public Format getJobQualifier() {
    return Format.ELVIA;
  }

  @Override
  public RecordConsumoMapper<ElviaRecord, ? extends Consumo> getRecordConsumoMapperBy(ElviaRecord record) {
    Optional<StanziamentiDetailsType> stanziamentiDetailsType = stanziamentiParamsValidator.findConsumoTypeByTracciatoAndTransactionDetail(Format.ELVIA, record.getTypeMovement());
    RecordConsumoMapper<ElviaRecord, ? extends Consumo> mapper = recordConsumoMapper;

    if (stanziamentiDetailsType.isPresent()){
      switch (stanziamentiDetailsType.get()) {
        case GENERICO:
          log.info("Chosen recordConsumoGenericoMapper based on record:=[{}]", record);
          mapper =  recordConsumoGenericoMapper;
          break;
        default:
          log.info("Chosen recordConsumoMapper based on record:=[{}]", record);
          mapper = recordConsumoMapper;
          break;
      }
    }
    return mapper;
  }

  @Override
  public ProcessingResult consumoValidateAndProcess(Bucket bucket, Consumo consumo) {
    log.debug("Multiple consumo job! Choosing processor based on consumo:=[{}]", consumo);
    if (consumo instanceof ElviaGenerico) {
      log.info("Chosen consumoGenericoProcessor for this consumo:=[{}]", consumo);
      return consumoGenericoProcessor.validateAndProcess((ElviaGenerico) consumo, bucket);
    } else {
      return consumoProcessor.validateAndProcess((Elvia) consumo, bucket);
    }
  }

  @Override
  protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
    super.afterStanziamentiSent(sourceType, stanziamentiDaSpedireANAV);
    if ("true".equals(DEVCONF_DISABLECASO3)){
      log.warn("Caso3 disabled with property application(-prod?).yml abstractjob.disablecaso3");
      return;
    }
    notConfirmedTemporaryAllocationsService.processSource("elcit", Instant.now());
  }
}
