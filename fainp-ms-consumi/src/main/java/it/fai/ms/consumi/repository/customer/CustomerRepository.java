package it.fai.ms.consumi.repository.customer;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.Customer;

public interface CustomerRepository {

  Optional<Customer> findCustomerByContrattoNumero(@NotNull String _customerId);

  Optional<Customer> findCustomerByUuid(@NotNull String customerUuid);

}
