package it.fai.ms.consumi.domain.consumi.pedaggi.asfinag;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class AsfinagGlobalIdentifier
  extends GlobalIdentifier {

  public AsfinagGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.EXTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    throw new IllegalStateException("newGlobalIdentifier operation is supported only for global identifier type internal");
  }

}
