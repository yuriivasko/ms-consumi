package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.util.Objects;

public class Customer implements Serializable {
  private static final long serialVersionUID = -1099960359574642200L;

  private final String id;

  public Customer(final String _id) {
    id = Objects.requireNonNull(_id, "Parameter id is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Customer obj = getClass().cast(_obj);
      res = Objects.equals(obj.id, id);
    }
    return res;
  }

  public String getId() {
    return id;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Customer [id=");
    builder.append(id);
    builder.append("]");
    return builder.toString();
  }

}
