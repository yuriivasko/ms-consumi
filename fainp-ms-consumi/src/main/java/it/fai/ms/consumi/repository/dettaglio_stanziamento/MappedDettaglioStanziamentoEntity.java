package it.fai.ms.consumi.repository.dettaglio_stanziamento;

import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class MappedDettaglioStanziamentoEntity {

  @Column(name = "global_identifier", nullable=false)
  private String globalIdentifier;

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final MappedDettaglioStanziamentoEntity obj = getClass().cast(_obj);
      res = Objects.equals(obj.getId(), getId()) && Objects.equals(obj.globalIdentifier, globalIdentifier);
    }
    return res;
  }

  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public abstract <Y extends MappedDettaglioStanziamentoEntity> Set<Y> getStanziamenti();

  public abstract <T> T getId() ;

  @Override
  public int hashCode() {
    return Objects.hash(getId(), globalIdentifier);
  }
}
