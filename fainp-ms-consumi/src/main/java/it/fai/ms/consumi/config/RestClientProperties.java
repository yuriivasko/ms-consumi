package it.fai.ms.consumi.config;

public class RestClientProperties {

  /**
   * https://exagespa.atlassian.net/wiki/spaces/FAIMYS/pages/20971604/Ambiente+di+Test POST
   * https://apitest.faiservice.it:447/exage/login grant_type=password&username=Fai&password=FaiWbApi2017
   */

  private String grant_type;
  private String username;
  private String password;
  private String url;

  public String getGrant_type() {
    return grant_type;
  }

  public void setGrant_type(String grant_type) {
    this.grant_type = grant_type;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

}
