package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.EmbeddableDettaglioStanziamentoEntityGeneric;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.ViewDettaglioStanziamentoEntity;


@Entity
@Table(name = "view_all_dettaglio_stanziamento_pedaggi")
@DiscriminatorValue(value = "PEDAGGI")
public class ViewDettaglioStanziamentoPedaggioEntity extends ViewDettaglioStanziamentoEntity {

  @Embedded
  private EmbeddableDettaglioStanziamentoEntityGeneric genericDettaglioStanziamento;

  @Embedded
  private EmbeddableDettaglioStanziamentoPedaggioEntity genericDettaglioStanziamentoPedaggio;

  
  public EmbeddableDettaglioStanziamentoPedaggioEntity getGenericDettaglioStanziamentoPedaggio() {
    return genericDettaglioStanziamentoPedaggio;
  }

  public void setGenericDettaglioStanziamentoCarburante(EmbeddableDettaglioStanziamentoPedaggioEntity genericDettaglioStanziamentoPedaggio) {
    this.genericDettaglioStanziamentoPedaggio = genericDettaglioStanziamentoPedaggio;
  }


  public EmbeddableDettaglioStanziamentoEntityGeneric getGenericAttributes() {
    return genericDettaglioStanziamento;
  }

  @Override
  public String toString() {
    return "ViewDettaglioStanziamentoPedaggioEntity [" + genericDettaglioStanziamento + genericDettaglioStanziamentoPedaggio + "]";
  }

}
