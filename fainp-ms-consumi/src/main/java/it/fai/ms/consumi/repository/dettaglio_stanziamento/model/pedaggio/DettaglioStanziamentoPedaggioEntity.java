package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric;


@Entity
@Table(name = "dettaglio_stanziamento_pedaggi")
@DiscriminatorValue(value = "PEDAGGI")
public class DettaglioStanziamentoPedaggioEntity extends DettaglioStanziamentoEntityGeneric {

  @Column(name = "imponibile_adr", precision = 16, scale = 5)
  private BigDecimal adrTaxableAmount;

  @Column(name = "amount_novat", precision = 16, scale = 5)
  private BigDecimal amountExcludedVat;

  @Column(name = "amount_includingvat", precision = 16, scale = 5)
  private BigDecimal amountIncludedVat;

  @Column(name = "perc_iva", precision = 19, scale = 2)
  private BigDecimal amountVatRate;

  @Column(name = "raggruppamento_articoli_nav")
  private String articlesGroup;

  @Column(name = "obu", nullable = true)
  private String deviceObu;

  @Column(name = "pan_number")
  private String devicePan;

  @Column(name = "device_type")
  @Enumerated(EnumType.STRING)
  private TipoDispositivoEnum deviceType;

  @Column(name = "entry_global_gate_identifier")
  private String entryGlobalGateIdentifier;

  @Column(name = "entry_global_gate_identifier_description")
  private String entryGlobalGateIdentifierDescription;

  @Column(name = "entry_timestamp")
  private Instant entryTimestamp;

  @Column(name = "exit_global_gate_identifier")
  private String exitGlobalGateIdentifier;

  @Column(name = "exit_global_gate_identifier_description")
  private String exitGlobalGateIdentifierDescription;

  @Column(name = "exit_timestamp")
  private Instant exitTimestamp;

  @Column(name = "tipo_consumo")
  @Enumerated(EnumType.STRING)
  private InvoiceType invoiceType;

  @Column(name = "network_code")
  private String networdCode;

//  @Column(name = "service_partner_code")
//  private String servicePartnerCode = "";

  @Column(name = "region")
  private String region;

  @Column(name = "row_number")
  private long sourceRowNumber;

  @NotNull
  @Column(name = "tipo_sorgente", nullable = false)
  private String sourceType;

  @Column(name = "codice_fornitore_nav")
  private String supplierCode;

  @Column(name = "codice_fornitore_legacy")
  private String supplierCodeLegacy;

  @Column(name = "documento_da_fornitore")
  private String supplierDocument;

  @Column(name = "transaction_detail_code")
  private String transactionDetailCode;

  @Column(name = "tratta")
  private String tratta;

  @Column(name = "veicolo_classe_euro")
  private String vehicleEuroClass;

  @Column(name = "veicolo_classe_tariffaria")
  private String vehicleFareClass;

  @Column(name = "veicolo_nazione_targa")
  private String vehicleLicenseCountryId;

  @Column(name = "veicolo_targa")
  private String vehicleLicenseLicenseId;

  public DettaglioStanziamentoPedaggioEntity(@NotNull final String _globalIdentifier) {
    super(_globalIdentifier);
  }

  public DettaglioStanziamentoPedaggioEntity(@NotNull String _globalIdentifier, Optional<UUID> id) {
    super(_globalIdentifier, id);
  }



  protected DettaglioStanziamentoPedaggioEntity() {
  }

  @Override
  public DettaglioStanziamentoEntity changeInvoiceTypeToD() {
    invoiceType = InvoiceType.D;
    return this;
  }

  public BigDecimal getAdrTaxableAmount() {
    return adrTaxableAmount;
  }

  public BigDecimal getAmountIncludingVat() {
    return amountIncludedVat;
  }

  public BigDecimal getAmountNoVat() {
    return amountExcludedVat;
  }

  public BigDecimal getAmountVatRate() {
    return amountVatRate;
  }

  public String getArticlesGroup() {
    return articlesGroup;
  }

  public String getDeviceObu() {
    return deviceObu;
  }

  public String getDevicePan() {
    return devicePan;
  }

  public TipoDispositivoEnum getDeviceType() {
    return deviceType;
  }

  public String getEntryGlobalGateIdentifier() {
    return entryGlobalGateIdentifier;
  }

  public String getEntryGlobalGateIdentifierDescription() {
    return entryGlobalGateIdentifierDescription;
  }

  public Instant getEntryTimestamp() {
    return entryTimestamp;
  }

  public String getExitGlobalGateIdentifier() {
    return exitGlobalGateIdentifier;
  }

  public String getExitGlobalGateIdentifierDescription() {
    return exitGlobalGateIdentifierDescription;
  }

  public Instant getExitTimestamp() {
    return exitTimestamp;
  }

  public InvoiceType getInvoiceType() {
    return invoiceType;
  }

  public String getNetwordCode() {
    return networdCode;
  }

//  public String getServicePartnerCode() {
//    return servicePartnerCode;
//  }

  public String getRegion() {
    return region;
  }

  public long getSourceRowNumber() {
    return sourceRowNumber;
  }

  public String getSourceType() {
    return sourceType;
  }

  public String getSupplierCode() {
    return supplierCode;
  }

  public void setSupplierCode(String supplierCode) {
    this.supplierCode = supplierCode;
  }

  public String getSupplierCodeLegacy() {
    return supplierCodeLegacy;
  }

  public String getSupplierDocument() {
    return supplierDocument;
  }

  public String getTransactionDetailCode() {
    return transactionDetailCode;
  }

  public String getTratta() {
    return tratta;
  }

  public String getVehicleEuroClass() {
    return vehicleEuroClass;
  }

  public String getVehicleFareClass() {
    return vehicleFareClass;
  }

  public String getVehicleLicenseCountryId() {
    return vehicleLicenseCountryId;
  }

  public String getVehicleLicenseLicenseId() {
    return vehicleLicenseLicenseId;
  }

  @Override
  public DettaglioStanziamentoEntity negateAmount() {
    if (amountExcludedVat != null) {
      amountExcludedVat = amountExcludedVat.negate();
    }
    if (amountIncludedVat != null) {
      amountIncludedVat = amountIncludedVat.negate();
    }
    if (adrTaxableAmount != null) {
      adrTaxableAmount = adrTaxableAmount.negate();
    }
    return this;
  }

  public void setAdrTaxableAmount(final BigDecimal _adrTaxableAmount) {
    adrTaxableAmount = _adrTaxableAmount;
  }

  public void setAmountExcludedVat(final BigDecimal _amountExcludedVat) {
    amountExcludedVat = _amountExcludedVat;
  }

  public void setAmountIncludedVat(final BigDecimal _amountIncludedVat) {
    amountIncludedVat = _amountIncludedVat;
  }

  public void setAmountVatRate(final BigDecimal _amountVatRate) {
    amountVatRate = _amountVatRate;
  }

  public void setCodiceFornitoreNav(final String _supplierCode) {
    supplierCode = _supplierCode;
  }

  public void setDeviceObu(final String _deviceObu) {
    deviceObu = _deviceObu;
  }

  public void setDevicePan(final String _devicePan) {
    devicePan = _devicePan;
  }

  public void setDeviceType(final TipoDispositivoEnum _deviceType) {
    deviceType = _deviceType;
  }

  public void setEntryGlobalGateIdentifier(final String _entryGlobalGateIdentifier) {
    entryGlobalGateIdentifier = _entryGlobalGateIdentifier;
  }

  public void setEntryGlobalGateIdentifierDescription(final String _entryGlobalGateIdentifierDescription) {
    entryGlobalGateIdentifierDescription = _entryGlobalGateIdentifierDescription;
  }

  public void setEntryTimestamp(final Instant _entryTimestamp) {
    entryTimestamp = _entryTimestamp;
  }

  public void setExitGlobalGateIdentifier(final String _exitGlobalGateIdentifier) {
    exitGlobalGateIdentifier = _exitGlobalGateIdentifier;
  }

  public void setExitGlobalGateIdentifierDescription(final String _exitGlobalGateIdentifierDescription) {
    exitGlobalGateIdentifierDescription = _exitGlobalGateIdentifierDescription;
  }

  public void setExitTimestamp(final Instant _exitTimestamp) {
    exitTimestamp = _exitTimestamp;
  }

  public void setInvoiceType(final InvoiceType _invoiceType) {
    invoiceType = _invoiceType;
  }

  public void setNetwordCode(final String _networdCode) {
    networdCode = _networdCode;
  }

//  public void setServicePartnerCode(final String _partnerCode) {
//    servicePartnerCode = _partnerCode;
//  }

  public void setRaggruppamentoArticoliNav(final String _articlesGroup) {
    articlesGroup = _articlesGroup;
  }

  public void setArticlesGroup(String articlesGroup) {
    this.articlesGroup = articlesGroup;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public void setSourceRowNumber(final long _sourceRowNumber) {
    sourceRowNumber = _sourceRowNumber;
  }

  public void setSourceType(final String _sourceType) {
    sourceType = _sourceType;
  }

  public void setSupplierCodeLegacy(final String _supplierCodeLegacy) {
    this.supplierCodeLegacy = _supplierCodeLegacy;
  }

  public void setSupplierDocument(final String _supplierDocument) {
    supplierDocument = _supplierDocument;
  }

  public void setTransactionDetailCode(final String _transactionDetailCode) {
    transactionDetailCode = _transactionDetailCode;
  }

  public void setTratta(String tratta) {
    this.tratta = tratta;
  }

  public void setVehicleEuroClass(final String _vehicleEuroClass) {
    vehicleEuroClass = _vehicleEuroClass;
  }

  public void setVehicleFareClass(final String _vehicleFareClass) {
    vehicleFareClass = _vehicleFareClass;
  }

  public void setVehicleLicenseCountryId(final String _vehicleLicenseCountryId) {
    vehicleLicenseCountryId = _vehicleLicenseCountryId;
  }

  public void setVehicleLicenseLicenseId(final String _vehicleLicenseLicenseId) {
    vehicleLicenseLicenseId = _vehicleLicenseLicenseId;
  }

  public BigDecimal getAmountExcludedVat() {
    return amountExcludedVat;
  }

  public BigDecimal getAmountIncludedVat() {
    return amountIncludedVat;
  }


}
