package it.fai.ms.consumi.service.frejus;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
public class FrejusCustomerData {

  public String trc;
  public String targa;
  public String numeroCarta;
  public LocalDate data;
  public LocalTime ora;
  public String azienda;

  public Instant getDataTransazione() {
    return data.atTime(ora).atZone(ZoneId.systemDefault()).toInstant();
  }
}
