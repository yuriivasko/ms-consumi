package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecord;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecordBody;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t.LiberTGlobalIdentifier;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Consumer;

@Component
public class TrackyCardPedaggiStandardRecordConsumoMapper extends AbstractRecordConsumoMapper 
                                                     implements RecordConsumoMapper<TrackyCardPedaggiStandardRecord, TrackyCardPedaggiStandardConsumo> {

  
  
  
  private static final String TRACKY_CARD_SOURCE = "TrackyCard";

  @Override
  public TrackyCardPedaggiStandardConsumo mapRecordToConsumo(TrackyCardPedaggiStandardRecord record) throws Exception {
    TrackyCardPedaggiStandardRecordBody recordBody;
    if(record instanceof TrackyCardPedaggiStandardRecordBody)
       recordBody = (TrackyCardPedaggiStandardRecordBody) record;
    else {
      return null;
    }

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(recordBody, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant           ingestionTime              = recordBody.getIngestion_time();
    Instant           acquisitionDate            = optionalCreationDateInFile.orElse(recordBody.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, TRACKY_CARD_SOURCE);
    source.setFileName(record.getFileName());
    source.setRowNumber(recordBody.getRowNumber());

    TrackyCardPedaggiStandardConsumo consumo = new TrackyCardPedaggiStandardConsumo(source, recordBody.getRecordCode(),
                                                            Optional.of(new LiberTGlobalIdentifier(recordBody.getGlobalIdentifier())), record);

    String imponibile = (new BigDecimal(recordBody.getImponibileImposta()).setScale(2, RoundingMode.HALF_UP)).toString();

    consumo.setAmount(AmountBuilder.builder()
                      .amountExcludedVat(MonetaryUtils.strToMonetary(recordBody.getImponibile()       , -2, recordBody.getValuta()))
                      //.amountIncludedVat(MonetaryUtils.strToMonetary(recordBody.getImponibileImposta(),-2, recordBody.getValuta()))  
                      .vatRate(NumericUtils.strToBigDecimal(recordBody.getPercentualeIVA(), 2,100))  
                      .build()); 
    
   
    consumo.setVehicle(init(new Vehicle(),v ->{
      v.setFareClass(recordBody.getClasseVeicolo() );                             // getClasseVehicule()
    }));

    // n.seriale
    consumo.setDevice(init(new Device(TipoDispositivoEnum.TRACKYCARD),d->{
      d.setSeriale(parsePan(recordBody));
      // d.setPan(parsePan(recordBody));  
    }));

    consumo.setTollPointEntry(init(new TollPoint(),t->{
      t.setGlobalGate(init(new GlobalGate(recordBody.getCaselloIngresso()),g->{   // getNGareEntree()
        g.setDescription(recordBody.getCaselloIngresso());                        // getNomGareEntree()
      }));
      setTime(t, recordBody.getDataIngresso()+recordBody.getOraIngresso() );          // getDateHeureEntree()
    }));

    consumo.setTollPointExit(init(new TollPoint(),t->{
      t.setGlobalGate(init(new GlobalGate(recordBody.getCaselloUscita()),g->{     // getNGareSortie()
        g.setDescription(recordBody.getCaselloUscita());                          // getNomGareSortie()
      }));
      setTime(t, recordBody.getDataUscita()+recordBody.getOraUscita());               // getDateHeureSortie()
    }));
    
    Vehicle vehicle = new Vehicle();
    vehicle.setFareClass(recordBody.getClasseVeicolo());
    consumo.setVehicle(vehicle);
    return consumo;

  }
  
  // dd/MM/yyyy HH:mm:ss
  private void setTime(TollPoint t, String dateHeureSortie) {
    try {
      super.calculateInstantByDateTime(dateHeureSortie,"yyyyMMddHHmmss").ifPresent(i->{
        t.setTime(i);
      });
    } catch (ParseException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  private String parsePan(TrackyCardPedaggiStandardRecordBody recordBody) { 
    return recordBody.getCodiceISOIIN()+recordBody.getCodiceGruppo()+recordBody.getCodiceCliente()+recordBody.getNumeroTessera()+recordBody.getCifradiControllo();
  }
  public static <T> T init(T obj, Consumer<T> initializer) {
    initializer.accept(obj);
    return obj;
  }

}
