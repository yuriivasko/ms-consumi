package it.fai.ms.consumi.domain.consumi.carburante.trackycard;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class TrackyCardGlobalIdentifier extends GlobalIdentifier {

  public TrackyCardGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.INTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    throw new UnsupportedOperationException("not implemented");
  }

}
