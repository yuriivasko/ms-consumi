package it.fai.ms.consumi.repository.storico_dml.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.fai.common.enumeration.TipoDispositivoEnum;

@Entity
@Table(name = "storico_dispositivo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoDispositivo extends DmlBaseEntity {
  private static final long serialVersionUID = 3709654263007839842L;

  @Size(max = 255)
  @Column(name = "contratto_dml_unique_identifier", length = 255)
  private String contrattoDmlUniqueIdentifier;
  
  @Size(max = 255)
  @Column(name = "veicolo_dml_unique_identifier", length = 255)
  private String veicoloDmlUniqueIdentifier;
  
  @Column(name = "seriale_dispositivo", length = 255)
  private String serialeDispositivo;

  @Size(max = 255)
  @Column(name = "stato", length = 255)
  private String stato;

  @Column(name = "data_modifica_stato")
  private Instant dataModificaStato;

  @Size(max = 255)
  @Column(name = "targa_2", length = 255)
  private String targa2;

  @Size(max = 255)
  @Column(name = "targa_3", length = 255)
  private String targa3;

  @Column(name = "tipo_dispositivo")
  @Enumerated(EnumType.STRING)
  private TipoDispositivoEnum tipoDispositivo;
  
  @Column(name = "note_operatore")
  private String noteOperatore;


  @Column(name = "note_tecniche")
  private String noteTecniche;
  
  @Column(name = "tipo_hardware")
  private String tipoHardware;
 
  public String getContrattoDmlUniqueIdentifier() {
    return contrattoDmlUniqueIdentifier;
  }


  public void setContrattoDmlUniqueIdentifier(String contrattoDmlUniqueIdentifier) {
    this.contrattoDmlUniqueIdentifier = contrattoDmlUniqueIdentifier;
  }


  public String getStato() {
    return stato;
  }


  public void setStato(String stato) {
    this.stato = stato;
  }


  public Instant getDataModificaStato() {
    return dataModificaStato;
  }


  public void setDataModificaStato(Instant dataModificaStato) {
    this.dataModificaStato = dataModificaStato;
  }


  public String getTarga2() {
    return targa2;
  }


  public void setTarga2(String targa2) {
    this.targa2 = targa2;
  }


  public String getTarga3() {
    return targa3;
  }


  public void setTarga3(String targa3) {
    this.targa3 = targa3;
  }

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }


  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }


  public String getNoteOperatore() {
    return noteOperatore;
  }


  public void setNoteOperatore(String noteOperatore) {
    this.noteOperatore = noteOperatore;
  }


  public String getNoteTecniche() {
    return noteTecniche;
  }


  public void setNoteTecniche(String noteTecniche) {
    this.noteTecniche = noteTecniche;
  }
  

  public String getSerialeDispositivo() {
    return serialeDispositivo;
  }


  public void setSerialeDispositivo(String serialeDispositivo) {
    this.serialeDispositivo = serialeDispositivo;
  }
  
  
  public String getVeicoloDmlUniqueIdentifier() {
    return veicoloDmlUniqueIdentifier;
  }
 
  public void setVeicoloDmlUniqueIdentifier(String veicoloDmlUniqueIdentifier) {
    this.veicoloDmlUniqueIdentifier = veicoloDmlUniqueIdentifier;
  }
   
  

  public String getTipoHardware() {
    return tipoHardware;
  }


  public void setTipoHardware(String tipoHardware) {
    this.tipoHardware = tipoHardware;
  }


  @Override
  public String toString() {
    return "StoricoDispositivo [contrattoDmlUniqueIdentifier=" + contrattoDmlUniqueIdentifier + ", veicoloDmlUniqueIdentifier="
           + veicoloDmlUniqueIdentifier + ", serialeDispositivo=" + serialeDispositivo + ", stato=" + stato + ", dataModificaStato="
           + dataModificaStato + ", targa2=" + targa2 + ", targa3=" + targa3 + ", tipoDispositivo=" + tipoDispositivo + ", noteOperatore="
           + noteOperatore + ", noteTecniche=" + noteTecniche + "]";
  }
}