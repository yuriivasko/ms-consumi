package it.fai.ms.consumi.service.processor.stanziamenti.generico;

import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGeneratorInterface;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGenerator;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoMapper;

@Component
public class StanziamentoGenericoMapper
  extends StanziamentoMapper {

  public StanziamentoGenericoMapper(StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator) {
    super(stanziamentoCodeGenerator);
  }

}
