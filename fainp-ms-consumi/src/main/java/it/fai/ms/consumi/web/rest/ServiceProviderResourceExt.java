package it.fai.ms.consumi.web.rest;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.enumeration.PriceSource;
import it.fai.ms.consumi.repository.ServiceProviderRepository;

/**
 * REST controller for managing ServiceProvider.
 */
@RestController
@RequestMapping("/api")
public class ServiceProviderResourceExt {

  private final Logger log = LoggerFactory.getLogger(getClass());

  public final static String SERVICE_PROVIDER_PRICETABLE = "/public/service-provider/pricetable";

  private final ServiceProviderRepository serviceProviderRepository;

  public ServiceProviderResourceExt(ServiceProviderRepository serviceProviderRepository) {
    this.serviceProviderRepository = serviceProviderRepository;
  }

  @GetMapping(SERVICE_PROVIDER_PRICETABLE)
  public ResponseEntity<List<String>> getServiceProviderToPriceTable() throws URISyntaxException {
    log.debug("REST request to save ServiceProvider to price table");

    PriceSource byPricesTable = PriceSource.by_prices_table;
    List<ServiceProvider> serviceProviderByPriceTable = serviceProviderRepository.findByPriceSourceOrderByProviderCodeAsc(byPricesTable);
    if (serviceProviderByPriceTable == null || serviceProviderByPriceTable.isEmpty()) {
      return ResponseEntity.ok(new ArrayList<>());
    }

    List<String> serviceProviderPriceTable = serviceProviderByPriceTable.stream()
                                                                        .map(sp -> new ServiceProviderPriceTableDTO(sp.getProviderCode(),
                                                                                                                    sp.getProviderName()).compose())
                                                                        .collect(toList());

    log.debug("Service provider by price table: {}", serviceProviderPriceTable);
    return ResponseEntity.ok(serviceProviderPriceTable);
  }

  public class ServiceProviderPriceTableDTO implements Serializable {

    private static final long serialVersionUID = -8154462350121663377L;

    private String providerCode;
    private String providerName;

    public ServiceProviderPriceTableDTO(String providerCode, String providerName) {
      this.providerCode = providerCode;
      this.providerName = providerName;
    }

    public String getProviderCode() {
      return providerCode;
    }

    public void setProviderCode(String providerCode) {
      this.providerCode = providerCode;
    }

    public String getProviderName() {
      return providerName;
    }

    public void setProviderName(String providerName) {
      this.providerName = providerName;
    }

    public String compose() {
      return this.providerCode + " - " + this.providerName;
    };

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("ServiceProvicePriceTableDTO [providerCode=");
      builder.append(providerCode);
      builder.append(", providerName=");
      builder.append(providerName);
      builder.append("]");
      return builder.toString();
    }

  }

}
