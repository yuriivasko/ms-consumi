package it.fai.ms.consumi.domain.navision;

import java.util.Objects;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Modello di errore inserimento stanziamenti Exage
 */
@ApiModel(description = "Modello di errore inserimento stanziamenti Exage")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-06-29T01:45:58.957+02:00")

public class StanziamentoError {

  @JsonProperty("codiceStanziamento")
  private String codiceStanziamento = null;

  @JsonProperty("error")
  private String error = null;

  public StanziamentoError codiceStanziamento(String codiceStanziamento) {
    this.codiceStanziamento = codiceStanziamento;
    return this;
  }

  /**
   * Codice stanziamento della riga per cui è stato generato l'errore
   *
   * @return codiceStanziamento
   **/
  @ApiModelProperty(value = "Codice stanziamento della riga per cui è stato generato l'errore")

  public String getCodiceStanziamento() {
    return codiceStanziamento;
  }

  public void setCodiceStanziamento(String codiceStanziamento) {
    this.codiceStanziamento = codiceStanziamento;
  }

  public StanziamentoError error(String error) {
    this.error = error;
    return this;
  }

  /**
   * Messaggio di errore
   *
   * @return error
   **/
  @ApiModelProperty(value = "Messaggio di errore")

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StanziamentoError stanziamentoError = (StanziamentoError) o;
    return Objects.equals(this.codiceStanziamento, stanziamentoError.codiceStanziamento)
           && Objects.equals(this.error, stanziamentoError.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codiceStanziamento, error);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StanziamentoError {\n");
    sb.append("    codiceStanziamento: ")
      .append(toIndentedString(codiceStanziamento))
      .append("\n");
    sb.append("    error: ")
      .append(toIndentedString(error))
      .append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString()
            .replace("\n", "\n    ");
  }
}
