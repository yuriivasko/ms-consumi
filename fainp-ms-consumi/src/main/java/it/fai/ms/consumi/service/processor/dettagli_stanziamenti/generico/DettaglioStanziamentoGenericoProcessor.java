package it.fai.ms.consumi.service.processor.dettagli_stanziamenti.generico;

import java.util.List;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;

public interface DettaglioStanziamentoGenericoProcessor {

  List<Stanziamento> produce(DettaglioStanziamentoGenerico dettaglioStanziamentoGenerico, StanziamentiParams stanziamentiParams);

}
