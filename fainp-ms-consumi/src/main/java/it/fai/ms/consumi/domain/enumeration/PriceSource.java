package it.fai.ms.consumi.domain.enumeration;

public enum PriceSource {

  by_prices_table, by_invoice

}
