package it.fai.ms.consumi.repository.storico_dml.jms;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.jms.Message;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.dml.DmlProcessor;
import it.fai.ms.common.dml.efservice.dto.DispositivoDMLDTO;
import it.fai.ms.common.dml.efservice.dto.DispositivoServizioDMLDTO;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.common.jms.JmsUtils;
import it.fai.ms.consumi.repository.storico_dml.StoricoDispositivoRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoStatoServizioRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.service.DeviceService;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsDmlStoricoDispositivoListener implements JmsTopicListener {

  @Value("${application.isIntilialLoading:}")
  private String isInitialLoading;

  @PostConstruct
  public void postConstruct() {
    log.error(" ### isInitialLoading:" + isInitialLoading + " ### ");
  }

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private final DmlProcessor<DispositivoDMLDTO, StoricoDispositivo> processorDispositivo;
  // AbstractDmlProcessor<DispositivoServizioDMLDTO, StoricoStatoServizio> processorServizio;

  private final StoricoStatoServizioRepository repoServizi;

  private final DeviceService deviceService;

  public JmsDmlStoricoDispositivoListener(StoricoDispositivoRepository repository, StoricoStatoServizioRepository repoServizi,
                                          ApplicationEventPublisher publisher, DeviceService deviceService) throws Exception {
    this.repoServizi = repoServizi;
    this.deviceService = deviceService;

    processorDispositivo = new DmlProcessor<>(repository, publisher, false, this::getEntityToPersistDispositivo);

    // processorServizio = new AbstractDmlProcessor<>(repoServizi, publisher, false, this::getEntityToPersistServizio);

  }

  private StoricoDispositivo getEntityToPersistDispositivo(DispositivoDMLDTO dto, StoricoDispositivo rf) {

    //NON SALVO I DISPOSITIVI SENZA SERIALE O CONTRATTO
    if (dto.getSeriale() == null || dto.getSeriale().trim().equals("NO_SERIALE") || StringUtils.isBlank(dto.getContrattoDmlUniqueIdentifier())) {
      return null;
    }

    if (rf == null) {
      rf = new StoricoDispositivo();
    }
    rf.setContrattoDmlUniqueIdentifier(dto.getContrattoDmlUniqueIdentifier());
    rf.setDataModificaStato(dto.getDataModificaStato());
    rf.setDmlUniqueIdentifier(dto.getDmlUniqueIdentifier());
    rf.setDmlRevisionTimestamp(dto.getDmlRevisionTimestamp());
    rf.setNoteOperatore(dto.getNoteOperatore());
    rf.setNoteTecniche(dto.getNoteTecniche());
    rf.setSerialeDispositivo(dto.getSeriale());
    rf.setStato(dto.getStato());
    rf.setTarga2(dto.getTarga2());
    rf.setTarga3(dto.getTarga3());
    rf.setTipoDispositivo(TipoDispositivoEnum.valueOf(dto.getTipoDispositivo()));
    rf.setVeicoloDmlUniqueIdentifier(dto.getVehicleUuid());
    rf.setTipoHardware(dto.getTipoHardware());

    return rf;
  }

  List<StoricoStatoServizio> getEntityToPersistServizio(DispositivoServizioDMLDTO input, StoricoDispositivo ed) {
    if (input.getDataInizio() == null)
      return Collections.emptyList();
    log.debug("search storico stato servizio servizi attivi for {}", input);


    StoricoStatoServizio outInactiveBefore = null;
    StoricoStatoServizio outActive = null;
    Instant endActiveDate = input.getDataFine();
    Instant endInactiveDate = null;

    List<StoricoStatoServizio> activeInInterval;
    //short circuit return if is initial loading
    if ("true".equalsIgnoreCase(isInitialLoading)) {
      log.debug("Is initial loading! returning from message elaboration...");
      activeInInterval = Collections.emptyList();
    } else {
      activeInInterval = deviceService.findStatiServizioInterval(ed.getDmlUniqueIdentifier(), input.getTipoServizio(), input.getDataInizio(), input.getDataFine());
    }
    log.debug("found  storico stato servizio {}", activeInInterval);

    for (StoricoStatoServizio storicoStatoServizio : activeInInterval) {
      if (storicoStatoServizio.getDataVariazione() == null) {
        repoServizi.delete(storicoStatoServizio);
      } else {
        if (outInactiveBefore == null && storicoStatoServizio.getDataVariazione()
          .isBefore(input.getDataInizio())) {
          outInactiveBefore = storicoStatoServizio;
        } else if (storicoStatoServizio.getDataVariazione()
          .equals(input.getDataInizio())) {
          if (outActive == null)
            outActive = storicoStatoServizio;
          else
            repoServizi.delete(storicoStatoServizio);
        } else {
          log.debug("delete {}", storicoStatoServizio);
          repoServizi.delete(storicoStatoServizio);
        }

        if (endActiveDate == null) {
          endActiveDate = storicoStatoServizio.getDataFineVariazione();
        }
        if (endInactiveDate == null && endActiveDate != null && storicoStatoServizio.getDataFineVariazione() != null
          && storicoStatoServizio.getDataFineVariazione()
          .isAfter(endActiveDate)) {
          endInactiveDate = storicoStatoServizio.getDataFineVariazione();
        }
      }
    }

    List<StoricoStatoServizio> toSave = new ArrayList<>();
    if (outInactiveBefore != null) {
      outInactiveBefore.setDataFineVariazione(input.getDataInizio());
      outInactiveBefore.setStato("NON_ATTIVO");
      toSave.add(outInactiveBefore);
    }

    if (outActive == null) {
      outActive = new StoricoStatoServizio();
    }

    outActive.setDataVariazione(input.getDataInizio());
    outActive.setDataFineVariazione(endActiveDate);
    outActive.setDispositivoDmlUniqueIdentifier(ed.getDmlUniqueIdentifier());
    outActive.setPan(input.getPan());
    outActive.setStato("ATTIVO");
    outActive.setTipoServizio(input.getTipoServizio());
    toSave.add(outActive);

    if (endActiveDate != null) {
      var outInactiveAfter = new StoricoStatoServizio();
      outInactiveAfter.setDataVariazione(endActiveDate);
      outInactiveAfter.setDataFineVariazione(endInactiveDate);
      outInactiveAfter.setDispositivoDmlUniqueIdentifier(ed.getDmlUniqueIdentifier());
      outInactiveAfter.setPan(input.getPan());
      outInactiveAfter.setStato("NON_ATTIVO");
      outInactiveAfter.setTipoServizio(input.getTipoServizio());
      toSave.add(outInactiveAfter);
    }
    log.debug("persit {}", toSave);
    return toSave;
  }


  @Override
  public JmsTopicNames getTopicName() {
    if ("true".equalsIgnoreCase(isInitialLoading)) {
      return JmsTopicNames.NO_LISTENER_1;
    }
    return JmsTopicNames.DML_DISPOSITIVI_SAVE;
  }

  @Override
  public void onMessage(Message message) {
    DispositivoDMLDTO dto = null;
    try {
      dto = JmsUtils.getMessageObject(message, DispositivoDMLDTO.class);
      log.debug("Receive DML DTO {} ", dto);
      StoricoDispositivo ed = processorDispositivo.processDto(dto);

      if (ed == null) {
        log.warn("Skip because SERIALE dispositivo is null or is not set");
        return;
      }

      dto.getDeviceServiceDmlDTO()
        .forEach(ser -> {
          var e = getEntityToPersistServizio(ser, ed);
          repoServizi.saveAll(e);
        });

    } catch (Exception e) {
      log.error("Errore durante il processamento del messaggio dml " + dto, e);
      e.printStackTrace();
      throw new RuntimeException(e);
    }

  }

}
