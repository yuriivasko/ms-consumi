package it.fai.ms.consumi.service.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.DispositivoDMLDTO;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;

@Component
public class DispositivoDMLDTOMapper {

  public DispositivoDMLDTO toDTO(final StoricoDispositivo data) {
    DispositivoDMLDTO dto = new DispositivoDMLDTO();
    dto.setContrattoDmlUniqueIdentifier(data.getContrattoDmlUniqueIdentifier());
    dto.setDmlRevisionTimestamp(data.getDmlRevisionTimestamp());
    dto.setDmlUniqueIdentifier(data.getDmlUniqueIdentifier());
    dto.setDataModificaStato(data.getDataModificaStato());
    dto.setNoteOperatore(data.getNoteOperatore());
    dto.setNoteTecniche(dto.getNoteTecniche());
    dto.setSeriale(data.getSerialeDispositivo());
    dto.setStato(data.getStato());
    dto.setTarga2(data.getTarga2());
    dto.setTarga3(data.getTarga3());
    dto.setTipoDispositivo(data.getTipoDispositivo() != null ? data.getTipoDispositivo().name() : null);
    dto.setVehicleUuid(data.getVeicoloDmlUniqueIdentifier());
    dto.setTipoHardware(data.getTipoHardware());
    return dto;
  }
  
}
