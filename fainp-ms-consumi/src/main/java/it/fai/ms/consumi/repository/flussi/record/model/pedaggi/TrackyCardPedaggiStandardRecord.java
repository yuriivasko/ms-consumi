package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;

public class TrackyCardPedaggiStandardRecord extends CommonRecord implements GlobalIdentifierBySupplier {
  
  private static final long serialVersionUID = -8130388473640120399L;
  private String globalIdentifier;
  private String recordNumber;

  @JsonCreator
  public TrackyCardPedaggiStandardRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber, @JsonProperty("globalIdentifier") String globalIdentifier) {
    super(fileName, rowNumber);
    this.globalIdentifier = globalIdentifier;
  }

  public TrackyCardPedaggiStandardRecord(String fileName, long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID()
        .toString();
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public String getRecordNumber() {
    return recordNumber;
  }

  public void setRecordNumber(String recordNumber) {
    this.recordNumber = recordNumber;
  }
  
}
