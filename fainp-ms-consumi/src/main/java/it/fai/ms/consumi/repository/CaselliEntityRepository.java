package it.fai.ms.consumi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.consumi.repository.caselli.model.CaselliEntity;

@Repository
public interface CaselliEntityRepository extends JpaRepository<CaselliEntity, Long> {

  Optional<CaselliEntity> findOneByGateIdentifier(String gateIndentifier);

  Optional<CaselliEntity> findOneByGlobalGateIdentifier(String globalGateIndentifier);

  CaselliEntity findFirstByOrderByIdDesc();

  Optional<CaselliEntity> findOneByGateIdentifierAndEndDateAndNetwork(String gate, String endDate, String network);

  Optional<CaselliEntity> findOneByGlobalGateIdentifierAndEndDateAndNetwork(String gate, String endDate, String network);

  List<CaselliEntity> findByGateIdentifier(String gate);

  List<CaselliEntity> findByGlobalGateIdentifier(String gate);

  List<CaselliEntity> findByGateIdentifierOrderByEndDateAsc(String code);

  List<CaselliEntity> findByGlobalGateIdentifierOrderByEndDateAsc(String code);

  void deleteByGateIdentifier(String gate);

}
