package it.fai.ms.consumi.domain.fatturazione;

import java.math.BigDecimal;
import java.time.Instant;

public class RequestsCreationAttachments {

  private Long id;

  private String codiceFattura;

  private Instant dataFattura;

  private String codiceRaggruppamentoArticolo;

  private String descrRaggruppamentoArticolo;

  private String nomeIntestazioneTessere;

  private String uuidDocumento;

  private BigDecimal importoTotale;

  private Instant data;

  private String lang;

  private String tipoServizio;

  private String nazioneFatturazione;
  
  public RequestsCreationAttachments id(Long id) {
	  this.id = id;
	  return this;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceFattura() {
    return codiceFattura;
  }

  public RequestsCreationAttachments codiceFattura(String codiceFattura) {
    this.codiceFattura = codiceFattura;
    return this;
  }

  public void setCodiceFattura(String codiceFattura) {
    this.codiceFattura = codiceFattura;
  }

  public Instant getDataFattura() {
    return dataFattura;
  }

  public void setDataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
  }

  public RequestsCreationAttachments dataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
    return this;
  }

  public String getCodiceRaggruppamentoArticolo() {
    return codiceRaggruppamentoArticolo;
  }

  public RequestsCreationAttachments codiceRaggruppamentoArticolo(String codiceRaggruppamentoArticolo) {
    this.codiceRaggruppamentoArticolo = codiceRaggruppamentoArticolo;
    return this;
  }

  public void setCodiceRaggruppamentoArticolo(String codiceRaggruppamentoArticolo) {
    this.codiceRaggruppamentoArticolo = codiceRaggruppamentoArticolo;
  }

  public String getDescrRaggruppamentoArticolo() {
    return descrRaggruppamentoArticolo;
  }

  public RequestsCreationAttachments descrRaggruppamentoArticolo(String descrRaggruppamentoArticolo) {
    this.descrRaggruppamentoArticolo = descrRaggruppamentoArticolo;
    return this;
  }

  public void setDescrRaggruppamentoArticolo(String descrRaggruppamentoArticolo) {
    this.descrRaggruppamentoArticolo = descrRaggruppamentoArticolo;
  }

  public String getNomeIntestazioneTessere() {
    return nomeIntestazioneTessere;
  }

  public RequestsCreationAttachments nomeIntestazioneTessere(String nomeIntestazioneTessere) {
    this.nomeIntestazioneTessere = nomeIntestazioneTessere;
    return this;
  }

  public void setNomeIntestazioneTessere(String nomeIntestazioneTessere) {
    this.nomeIntestazioneTessere = nomeIntestazioneTessere;
  }

  public String getUuidDocumento() {
    return uuidDocumento;
  }

  public RequestsCreationAttachments uuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
    return this;
  }

  public void setUuidDocumento(String uuidDocumento) {
    this.uuidDocumento = uuidDocumento;
  }

  public BigDecimal getImportoTotale() {
    return importoTotale;
  }

  public RequestsCreationAttachments importoTotale(BigDecimal importoTotale) {
    this.importoTotale = importoTotale;
    return this;
  }

  public void setImportoTotale(BigDecimal importoTotale) {
    this.importoTotale = importoTotale;
  }

  public Instant getData() {
    return data;
  }

  public RequestsCreationAttachments data(Instant data) {
    this.data = data;
    return this;
  }

  public void setData(Instant data) {
    this.data = data;
  }

  public String getLang() {
    return lang;
  }

  public RequestsCreationAttachments lang(String lang) {
    this.lang = lang;
    return this;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public String getTipoServizio() {
    return tipoServizio;
  }
  
  public RequestsCreationAttachments tipoServizio(String tipoServizio) {
    this.tipoServizio = tipoServizio;
    return this;
  }

  public void setTipoServizio(String tipoServizio) {
    this.tipoServizio = tipoServizio;
  }

  public String getNazioneFatturazione() {
    return nazioneFatturazione;
  }
  
  public RequestsCreationAttachments nazioneFatturazione(String nazioneFatturazione) {
    this.nazioneFatturazione = nazioneFatturazione;
    return this;
  }

  public void setNazioneFatturazione(String nazioneFatturazione) {
    this.nazioneFatturazione = nazioneFatturazione;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("RequestsCreationAttachments [");
    sb.append("id= ");
    sb.append(id);
    sb.append(", codiceFattura= ");
    sb.append(codiceFattura);
    sb.append(", dataFattura= ");
    sb.append(dataFattura);
    sb.append(", codiceRaggruppamentoArticolo= ");
    sb.append(codiceRaggruppamentoArticolo);
    sb.append(", descrRaggruppamentoArticolo= ");
    sb.append(descrRaggruppamentoArticolo);
    sb.append(", nomeIntestazioneTessere= ");
    sb.append(nomeIntestazioneTessere);
    sb.append(", uuidDocumento= ");
    sb.append(uuidDocumento);
    sb.append(", importoTotale= ");
    sb.append(importoTotale);
    sb.append(", data= ");
    sb.append(data);
    sb.append(", lang= ");
    sb.append(lang);
    sb.append(", tipoServizio= ");
    sb.append(tipoServizio);
    sb.append(", nazioneFatturazione= ");
    sb.append(nazioneFatturazione);
    sb.append("]");
    return sb.toString();
  }

}
