package it.fai.ms.consumi.client;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Country entity.
 */
public class CountryDTO
  implements Serializable {

  private Long id;

  private String nome;

  private String codice;

  private String codiceIso2;

  private String isoNumerico;

  private String lingua;

  private String nomeInglese;

  private String siglaAuto;

  private String valuta;

  private Integer ordinamento;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getCodice() {
    return codice;
  }

  public void setCodice(String codice) {
    this.codice = codice;
  }

  public String getCodiceIso2() {
    return codiceIso2;
  }

  public void setCodiceIso2(String codiceIso2) {
    this.codiceIso2 = codiceIso2;
  }

  public String getIsoNumerico() {
    return isoNumerico;
  }

  public void setIsoNumerico(String isoNumerico) {
    this.isoNumerico = isoNumerico;
  }

  public String getLingua() {
    return lingua;
  }

  public void setLingua(String lingua) {
    this.lingua = lingua;
  }

  public String getNomeInglese() {
    return nomeInglese;
  }

  public void setNomeInglese(String nomeInglese) {
    this.nomeInglese = nomeInglese;
  }

  public String getSiglaAuto() {
    return siglaAuto;
  }

  public void setSiglaAuto(String siglaAuto) {
    this.siglaAuto = siglaAuto;
  }

  public String getValuta() {
    return valuta;
  }

  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

  public Integer getOrdinamento() {
    return ordinamento;
  }

  public void setOrdinamento(Integer ordinamento) {
    this.ordinamento = ordinamento;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    CountryDTO countryDTO = (CountryDTO) o;
    if (countryDTO.getId() == null || getId() == null) {
      return false;
    }
    return Objects.equals(getId(), countryDTO.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(getId());
  }

  @Override
  public String toString() {
    return "CountryDTO{" + "id=" + getId() + ", nome='" + getNome() + "'" + ", codice='" + getCodice() + "'" + ", codiceIso2='"
           + getCodiceIso2() + "'" + ", isoNumerico='" + getIsoNumerico() + "'" + ", lingua='" + getLingua() + "'" + ", nomeInglese='"
           + getNomeInglese() + "'" + ", siglaAuto='" + getSiglaAuto() + "'" + ", valuta='" + getValuta() + "'" + ", ordinamento='"
           + getOrdinamento() + "'" + "}";
  }
}
