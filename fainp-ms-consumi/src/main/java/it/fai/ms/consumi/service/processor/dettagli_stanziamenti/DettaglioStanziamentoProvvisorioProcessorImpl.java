package it.fai.ms.consumi.service.processor.dettagli_stanziamenti;

import java.util.HashMap;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoCollection;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.pedaggi.StanziamentoProcessorPedaggio;

@Service
public class DettaglioStanziamentoProvvisorioProcessorImpl implements DettaglioStanziamentoProvvisorioProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamantiRepository allocationDetailRepository;
  private final StanziamentoProcessorPedaggio   allocationProcessor;
  private final NotificationService             notificationService;


  @Inject
  public DettaglioStanziamentoProvvisorioProcessorImpl(final DettaglioStanziamantiRepository _allocationDetailRepository,
                                                       final StanziamentoProcessorPedaggio _allocationProcessor,
                                                       NotificationService notificationService) {
    allocationDetailRepository = _allocationDetailRepository;
    allocationProcessor = _allocationProcessor;
    this.notificationService = notificationService;
  }

  @Override
  public Optional<DettaglioStanziamento> process(final DettaglioStanziamento _allocationDetail,
                                                 final StanziamentiParams _allocationParams) {

    Optional<DettaglioStanziamento> processedAllocationDetail;

    final var globalIdentifier = _allocationDetail.getGlobalIdentifier();

    _log.info("  - {} - processing gId {}", _allocationDetail.getInvoiceType(), globalIdentifier);

    final var allocationDetails = allocationDetailRepository.findByGlobalIdentifierId(globalIdentifier,_allocationParams!=null ? _allocationParams.getStanziamentiDetailsType() : null);

    if (allocationDetails.isEmpty()) {
      final var allocationDetail = process_case_1___noPersistedAllocationDetailsFound(_allocationDetail, _allocationParams);
      processedAllocationDetail = Optional.ofNullable(allocationDetail);
    } else {
      final var allocationDetail = process_case_2___foundPersistedAllocationDetails(_allocationDetail,
                                                                                    new DettaglioStanziamentoCollection(allocationDetails));
      processedAllocationDetail = Optional.ofNullable(allocationDetail);
    }

    return processedAllocationDetail;
  }

  private DettaglioStanziamento createAllocationDetail(final DettaglioStanziamento _allocationDetail) {
    try {
      allocationDetailRepository.save(_allocationDetail);
    } catch (final Throwable _t) {
      _log.error("DettaglioStanziamentoP not saved : {}", _allocationDetail);
      _log.error(" Exception message : {}", _t.getMessage());
      _log.debug("", _t);
    }
    return _allocationDetail;
  }

  private DettaglioStanziamento process_case_1___noPersistedAllocationDetailsFound(final DettaglioStanziamento _allocationDetail,
                                                                                   final StanziamentiParams _allocationParams) {

    DettaglioStanziamento allocationDetailToReturn = null;

    _log.info(" case 1 (no allocation details): {} {} not found for gId {}", _allocationDetail.getClass()
                                                                                              .getSimpleName(),
              _allocationDetail.getInvoiceType(), _allocationDetail.getGlobalIdentifier());

    final var allocations = allocationProcessor.processStanziamento(_allocationDetail, _allocationParams);
    _allocationDetail.getStanziamenti()
                     .addAll(allocations);

    allocationDetailToReturn = createAllocationDetail(_allocationDetail);

    _log.info(" case 1 (no allocation details): {} {} created  with gId {}", _allocationDetail.getClass()
                                                                                              .getSimpleName(),
              _allocationDetail.getInvoiceType(), _allocationDetail.getGlobalIdentifier());

    return allocationDetailToReturn;
  }

  private DettaglioStanziamento process_case_2___foundPersistedAllocationDetails(final DettaglioStanziamento _allocationDetail,
                                                                                 final DettaglioStanziamentoCollection _persistedAllocationDetailCollection) {

    DettaglioStanziamento allocationDetailToReturn = null;

    _log.info(" case 2 (found allocation details): found {} persisted {}", _persistedAllocationDetailCollection.size(),
              _allocationDetail.getClass()
                               .getSimpleName());
    _persistedAllocationDetailCollection.getDettaglioStanziamentoSet()
                                        .forEach(allocationDetail -> {
                                          _log.info(" case 2 (found allocation details): {} {}", allocationDetail.getInvoiceType(),
                                                    allocationDetail.getGlobalIdentifier());
                                        });

    if (_persistedAllocationDetailCollection.thereAreOnlyDettaglioStanziamentoProvvisorio()) {

      allocationDetailToReturn = process_case_2_a_found_onlyTemp(_allocationDetail);

    } else if (_persistedAllocationDetailCollection.thereAreOnlyDettaglioStanziamentoDefinitivo()) {

      allocationDetailToReturn = process_case_2_b_found_onlyLast(_allocationDetail);

    } else if (_persistedAllocationDetailCollection.thereAreBothDettaglioStanziamentoTypes()) {

      allocationDetailToReturn = process_case_2_c_found_both(_allocationDetail);

    }

    return allocationDetailToReturn;
  }

  private DettaglioStanziamento process_case_2_a_found_onlyTemp(final DettaglioStanziamento _allocationDetail) {
    _log.info("  found only {} of type {} - skip processing -", _allocationDetail.getClass()
                                                                                 .getSimpleName(),
              InvoiceType.P);

    return null;
  }

  private DettaglioStanziamento process_case_2_b_found_onlyLast(final DettaglioStanziamento _allocationDetail) {
    _log.info("  found only {} of type {} - skip processing -", _allocationDetail.getClass()
                                                                                 .getSimpleName(),
              InvoiceType.D);
//    _log.info("  found only {} of type {}   --> notify error", _allocationDetail.getClass()
//                                                                              .getSimpleName(),
//              InvoiceType.D);

    final String simpleName = _allocationDetail.getClass().getSimpleName();

    // FIXME verificare messaggio
    _log.warn("SENDING ANOMALIA: Attesi {} di tipo {}, ma trovati solo di tipo {}: ",
              simpleName, InvoiceType.P.extendedName, InvoiceType.D.extendedName);

    var messageTextKV = new HashMap<String, Object>();
    messageTextKV.put("simpleName", simpleName);
    messageTextKV.put("expected", InvoiceType.P.extendedName);
    messageTextKV.put("actual", InvoiceType.D.extendedName);
    messageTextKV.put("dettaglio", _allocationDetail.toString());

    notificationService.notify("DSP-001", messageTextKV);

    return null;
  }

  private DettaglioStanziamento process_case_2_c_found_both(final DettaglioStanziamento _allocationDetail) {
    _log.info("  found both {} of type {} and {} - skip processing -", _allocationDetail.getClass()
                                                                                        .getSimpleName(),
              InvoiceType.P, InvoiceType.D);
//    _log.info("  found only {} of type {} and {}   --> notify error", _allocationDetail.getClass()
//                                                                                     .getSimpleName(),
//              InvoiceType.P, InvoiceType.D);

    // FIXME verificare messaggio
    final String simpleName = _allocationDetail.getClass().getSimpleName();

    _log.warn("SENDING ANOMALIA: Attesi {} di tipo {}, ma trovati solo di entrambi i tipi {}, {}: ",
              simpleName, InvoiceType.P.extendedName, InvoiceType.P.extendedName, InvoiceType.D.extendedName);

    var messageTextKV = new HashMap<String, Object>();
    messageTextKV.put("simpleName", simpleName);
    messageTextKV.put("expected", InvoiceType.P.extendedName);
    messageTextKV.put("both", InvoiceType.P.extendedName + ", " + InvoiceType.D.extendedName);
    messageTextKV.put("dettaglio", _allocationDetail.toString());

    notificationService.notify("DSP-002", messageTextKV);

    return null;
  }

}
