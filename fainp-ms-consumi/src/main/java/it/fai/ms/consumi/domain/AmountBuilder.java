package it.fai.ms.consumi.domain;

import java.math.BigDecimal;

import javax.money.MonetaryAmount;

public class AmountBuilder {
  private MonetaryAmount amountExcludedVat;
  private MonetaryAmount amountIncludedVat;
  private BigDecimal exchangeRate;
  private BigDecimal     vatRate;


  private AmountBuilder(){

  }

  public static AmountBuilder builder(){
    return new AmountBuilder();
  }

  public AmountBuilder amountExcludedVat(MonetaryAmount amountExcludedVat){
    this.amountExcludedVat = amountExcludedVat;
    return this;
  }

  public AmountBuilder amountIncludedVat(MonetaryAmount amountIncludedVat){
    this.amountIncludedVat = amountIncludedVat;
    return this;
  }

  public AmountBuilder exchangeRate(BigDecimal exchangeRate){
    this.exchangeRate = exchangeRate;
    return this;
  }

  public AmountBuilder vatRate(BigDecimal vatRate){
    this.vatRate = vatRate;
    return this;
  }

  public Amount build(){
    Amount amount = new Amount();
    amount.setVatRate(vatRate);
    amount.setExchangeRate(exchangeRate);
    amount.setAmountExcludedVat(amountExcludedVat);
    amount.setAmountIncludedVat(amountIncludedVat);
    return amount;
  }
}
