package it.fai.ms.consumi.repository;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "cliente")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ClienteEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NotNull
  @Column(name = "codice_cliente", nullable = false)
  private String codiceCliente;

  @Column(name = "email")
  private String email;

  @Column(name = "stato")
  private String stato;

  @Column(name = "numero_account")
  private Long numeroAccount;

  @Column(name = "ragione_sociale")
  private String ragioneSociale;

  @Column(name = "codice_fiscale")
  private String codiceFiscale;

  @Column(name = "partita_iva")
  private String partitaIva;

  @Column(name = "codice_agente")
  private String codiceAgente;

  @Column(name = "raggruppamento_impresa")
  private String raggruppamentoImpresa;

  @Column(name = "descrizione_raggruppamento_impresa")
  private String descrizioneRaggruppamentoImpresa;

  @Column(name = "consorzio_padre")
  private Boolean consorzioPadre;

  @NotNull
  @Column(name = "via", nullable = false)
  private String via;

  @NotNull
  @Column(name = "citta", nullable = false)
  private String citta;

  @Column(name = "cap")
  private String cap;

  @Column(name = "provincia")
  private String provincia;

  @Size(max = 2)
  @Column(name = "paese", length = 2)
  private String paese;

  @Column(name = "email_legale_rappresentante")
  private String emailLegaleRappresentante;

  @Column(name = "nome_legale_rappresentante")
  private String nomeLegaleRappresentante;

  @Column(name = "cognome_legale_rappresentante")
  private String cognomeLegaleRappresentante;

  @Column(name = "contatto_telefonico_legale_rappresentante")
  private String contattoTelefonicoLegaleRappresentante;

  @Column(name = "codice_cliente_fatturazione")
  private String codiceClienteFatturazione;

  @NotNull
  @Column(name = "dml_revision_timestamp", nullable = false)
  private Instant dmlRevisionTimestamp;

  @Column(name = "intestazione_tessere")
  private String intestazioneTessere;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCodiceCliente() {
    return codiceCliente;
  }

  public void setCodiceCliente(String codiceCliente) {
    this.codiceCliente = codiceCliente;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getStato() {
    return stato;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public Long getNumeroAccount() {
    return numeroAccount;
  }

  public void setNumeroAccount(Long numeroAccount) {
    this.numeroAccount = numeroAccount;
  }

  public String getRagioneSociale() {
    return ragioneSociale;
  }

  public void setRagioneSociale(String ragioneSociale) {
    this.ragioneSociale = ragioneSociale;
  }

  public String getCodiceFiscale() {
    return codiceFiscale;
  }

  public void setCodiceFiscale(String codiceFiscale) {
    this.codiceFiscale = codiceFiscale;
  }

  public String getPartitaIva() {
    return partitaIva;
  }

  public void setPartitaIva(String partitaIva) {
    this.partitaIva = partitaIva;
  }

  public String getCodiceAgente() {
    return codiceAgente;
  }

  public void setCodiceAgente(String codiceAgente) {
    this.codiceAgente = codiceAgente;
  }

  public String getRaggruppamentoImpresa() {
    return raggruppamentoImpresa;
  }

  public void setRaggruppamentoImpresa(String raggruppamentoImpresa) {
    this.raggruppamentoImpresa = raggruppamentoImpresa;
  }

  public String getDescrizioneRaggruppamentoImpresa() {
    return descrizioneRaggruppamentoImpresa;
  }

  public void setDescrizioneRaggruppamentoImpresa(String descrizioneRaggruppamentoImpresa) {
    this.descrizioneRaggruppamentoImpresa = descrizioneRaggruppamentoImpresa;
  }

  public Boolean getConsorzioPadre() {
    return consorzioPadre;
  }

  public void setConsorzioPadre(Boolean consorzioPadre) {
    this.consorzioPadre = consorzioPadre;
  }

  public String getVia() {
    return via;
  }

  public void setVia(String via) {
    this.via = via;
  }

  public String getCitta() {
    return citta;
  }

  public void setCitta(String citta) {
    this.citta = citta;
  }

  public String getCap() {
    return cap;
  }

  public void setCap(String cap) {
    this.cap = cap;
  }

  public String getProvincia() {
    return provincia;
  }

  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }

  public String getPaese() {
    return paese;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public String getEmailLegaleRappresentante() {
    return emailLegaleRappresentante;
  }

  public void setEmailLegaleRappresentante(String emailLegaleRappresentante) {
    this.emailLegaleRappresentante = emailLegaleRappresentante;
  }

  public String getNomeLegaleRappresentante() {
    return nomeLegaleRappresentante;
  }

  public void setNomeLegaleRappresentante(String nomeLegaleRappresentante) {
    this.nomeLegaleRappresentante = nomeLegaleRappresentante;
  }

  public String getCognomeLegaleRappresentante() {
    return cognomeLegaleRappresentante;
  }

  public void setCognomeLegaleRappresentante(String cognomeLegaleRappresentante) {
    this.cognomeLegaleRappresentante = cognomeLegaleRappresentante;
  }

  public String getContattoTelefonicoLegaleRappresentante() {
    return contattoTelefonicoLegaleRappresentante;
  }

  public void setContattoTelefonicoLegaleRappresentante(String contattoTelefonicoLegaleRappresentante) {
    this.contattoTelefonicoLegaleRappresentante = contattoTelefonicoLegaleRappresentante;
  }

  public String getCodiceClienteFatturazione() {
    return codiceClienteFatturazione;
  }

  public void setCodiceClienteFatturazione(String codiceClienteFatturazione) {
    this.codiceClienteFatturazione = codiceClienteFatturazione;
  }

  public Instant getDmlRevisionTimestamp() {
    return dmlRevisionTimestamp;
  }

  public void setDmlRevisionTimestamp(Instant dmlRevisionTimestamp) {
    this.dmlRevisionTimestamp = dmlRevisionTimestamp;
  }

  public String getIntestazioneTessere() {
    return intestazioneTessere;
  }

  public void setIntestazioneTessere(String intestazioneTessere) {
    this.intestazioneTessere = intestazioneTessere;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((cap == null) ? 0 : cap.hashCode());
    result = prime * result + ((citta == null) ? 0 : citta.hashCode());
    result = prime * result + ((codiceAgente == null) ? 0 : codiceAgente.hashCode());
    result = prime * result + ((codiceCliente == null) ? 0 : codiceCliente.hashCode());
    result = prime * result + ((codiceClienteFatturazione == null) ? 0 : codiceClienteFatturazione.hashCode());
    result = prime * result + ((codiceFiscale == null) ? 0 : codiceFiscale.hashCode());
    result = prime * result + ((cognomeLegaleRappresentante == null) ? 0 : cognomeLegaleRappresentante.hashCode());
    result = prime * result + ((consorzioPadre == null) ? 0 : consorzioPadre.hashCode());
    result = prime * result + ((contattoTelefonicoLegaleRappresentante == null) ? 0 : contattoTelefonicoLegaleRappresentante.hashCode());
    result = prime * result + ((descrizioneRaggruppamentoImpresa == null) ? 0 : descrizioneRaggruppamentoImpresa.hashCode());
    result = prime * result + ((dmlRevisionTimestamp == null) ? 0 : dmlRevisionTimestamp.hashCode());
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + ((emailLegaleRappresentante == null) ? 0 : emailLegaleRappresentante.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((intestazioneTessere == null) ? 0 : intestazioneTessere.hashCode());
    result = prime * result + ((nomeLegaleRappresentante == null) ? 0 : nomeLegaleRappresentante.hashCode());
    result = prime * result + ((numeroAccount == null) ? 0 : numeroAccount.hashCode());
    result = prime * result + ((paese == null) ? 0 : paese.hashCode());
    result = prime * result + ((partitaIva == null) ? 0 : partitaIva.hashCode());
    result = prime * result + ((provincia == null) ? 0 : provincia.hashCode());
    result = prime * result + ((raggruppamentoImpresa == null) ? 0 : raggruppamentoImpresa.hashCode());
    result = prime * result + ((ragioneSociale == null) ? 0 : ragioneSociale.hashCode());
    result = prime * result + ((stato == null) ? 0 : stato.hashCode());
    result = prime * result + ((via == null) ? 0 : via.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ClienteEntity other = (ClienteEntity) obj;
    if (cap == null) {
      if (other.cap != null)
        return false;
    } else if (!cap.equals(other.cap))
      return false;
    if (citta == null) {
      if (other.citta != null)
        return false;
    } else if (!citta.equals(other.citta))
      return false;
    if (codiceAgente == null) {
      if (other.codiceAgente != null)
        return false;
    } else if (!codiceAgente.equals(other.codiceAgente))
      return false;
    if (codiceCliente == null) {
      if (other.codiceCliente != null)
        return false;
    } else if (!codiceCliente.equals(other.codiceCliente))
      return false;
    if (codiceClienteFatturazione == null) {
      if (other.codiceClienteFatturazione != null)
        return false;
    } else if (!codiceClienteFatturazione.equals(other.codiceClienteFatturazione))
      return false;
    if (codiceFiscale == null) {
      if (other.codiceFiscale != null)
        return false;
    } else if (!codiceFiscale.equals(other.codiceFiscale))
      return false;
    if (cognomeLegaleRappresentante == null) {
      if (other.cognomeLegaleRappresentante != null)
        return false;
    } else if (!cognomeLegaleRappresentante.equals(other.cognomeLegaleRappresentante))
      return false;
    if (consorzioPadre == null) {
      if (other.consorzioPadre != null)
        return false;
    } else if (!consorzioPadre.equals(other.consorzioPadre))
      return false;
    if (contattoTelefonicoLegaleRappresentante == null) {
      if (other.contattoTelefonicoLegaleRappresentante != null)
        return false;
    } else if (!contattoTelefonicoLegaleRappresentante.equals(other.contattoTelefonicoLegaleRappresentante))
      return false;
    if (descrizioneRaggruppamentoImpresa == null) {
      if (other.descrizioneRaggruppamentoImpresa != null)
        return false;
    } else if (!descrizioneRaggruppamentoImpresa.equals(other.descrizioneRaggruppamentoImpresa))
      return false;
    if (dmlRevisionTimestamp == null) {
      if (other.dmlRevisionTimestamp != null)
        return false;
    } else if (!dmlRevisionTimestamp.equals(other.dmlRevisionTimestamp))
      return false;
    if (email == null) {
      if (other.email != null)
        return false;
    } else if (!email.equals(other.email))
      return false;
    if (emailLegaleRappresentante == null) {
      if (other.emailLegaleRappresentante != null)
        return false;
    } else if (!emailLegaleRappresentante.equals(other.emailLegaleRappresentante))
      return false;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (intestazioneTessere == null) {
      if (other.intestazioneTessere != null)
        return false;
    } else if (!intestazioneTessere.equals(other.intestazioneTessere))
      return false;
    if (nomeLegaleRappresentante == null) {
      if (other.nomeLegaleRappresentante != null)
        return false;
    } else if (!nomeLegaleRappresentante.equals(other.nomeLegaleRappresentante))
      return false;
    if (numeroAccount == null) {
      if (other.numeroAccount != null)
        return false;
    } else if (!numeroAccount.equals(other.numeroAccount))
      return false;
    if (paese == null) {
      if (other.paese != null)
        return false;
    } else if (!paese.equals(other.paese))
      return false;
    if (partitaIva == null) {
      if (other.partitaIva != null)
        return false;
    } else if (!partitaIva.equals(other.partitaIva))
      return false;
    if (provincia == null) {
      if (other.provincia != null)
        return false;
    } else if (!provincia.equals(other.provincia))
      return false;
    if (raggruppamentoImpresa == null) {
      if (other.raggruppamentoImpresa != null)
        return false;
    } else if (!raggruppamentoImpresa.equals(other.raggruppamentoImpresa))
      return false;
    if (ragioneSociale == null) {
      if (other.ragioneSociale != null)
        return false;
    } else if (!ragioneSociale.equals(other.ragioneSociale))
      return false;
    if (stato == null) {
      if (other.stato != null)
        return false;
    } else if (!stato.equals(other.stato))
      return false;
    if (via == null) {
      if (other.via != null)
        return false;
    } else if (!via.equals(other.via))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Cliente [id=" + id + ", codiceCliente=" + codiceCliente + ", email=" + email + ", stato=" + stato + ", numeroAccount="
           + numeroAccount + ", ragioneSociale=" + ragioneSociale + ", codiceFiscale=" + codiceFiscale + ", partitaIva=" + partitaIva
           + ", codiceAgente=" + codiceAgente + ", raggruppamentoImpresa=" + raggruppamentoImpresa + ", descrizioneRaggruppamentoImpresa="
           + descrizioneRaggruppamentoImpresa + ", consorzioPadre=" + consorzioPadre + ", via=" + via + ", citta=" + citta + ", cap=" + cap
           + ", provincia=" + provincia + ", paese=" + paese + ", emailLegaleRappresentante=" + emailLegaleRappresentante
           + ", nomeLegaleRappresentante=" + nomeLegaleRappresentante + ", cognomeLegaleRappresentante=" + cognomeLegaleRappresentante
           + ", contattoTelefonicoLegaleRappresentante=" + contattoTelefonicoLegaleRappresentante + ", codiceClienteFatturazione="
           + codiceClienteFatturazione + ", dmlRevisionTimestamp=" + dmlRevisionTimestamp + ", intestazioneTessere=" + intestazioneTessere
           + "]";
  }

}
