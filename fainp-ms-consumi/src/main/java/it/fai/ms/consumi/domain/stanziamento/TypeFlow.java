package it.fai.ms.consumi.domain.stanziamento;

public enum TypeFlow {

  NONE(0), MYFAI(1), TRAGHETTI(2), ACCETTAZIONE_DOMANDA_SCONTI_ITALIA(3), LIQUIDAZIONE_SCONTI_ITALIA(4), PRESENTAZIONE_DOMANDA_RIMBORSO_IVA(
      5), LIQUIDAZIONE_RIMBORSO_IVA(6);

  private int tipoFlusso;

  TypeFlow(int _tipoFlusso) {
    tipoFlusso = _tipoFlusso;
  }

  public int intValue() {
    return tipoFlusso;
  }

}
