package it.fai.ms.consumi.service.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.consumi.report.ReportToGenerateDTO;
import it.fai.ms.consumi.service.async.ReportAsyncService;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsListenerReportGenerated implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final ReportAsyncService reportAsyncService;

  public JmsListenerReportGenerated(final ReportAsyncService _reportAsyncService) {
    reportAsyncService = _reportAsyncService;
  }

  @Override
  public void onMessage(Message message) {
    log.debug("Received jms message {}", message);
    ReportToGenerateDTO reportGeneratedDTO = null;
    try {
      try {
        reportGeneratedDTO = (ReportToGenerateDTO) ((ObjectMessage) message).getObject();
      } catch (JMSException e) {
        log.error("Cast exception message: {}", message, e);
        throw e;
      }
      reportAsyncService.saveResponse(reportGeneratedDTO.getKeyJob(), reportGeneratedDTO.getFileName());
    } catch (Exception e) {
      log.error("Exception: ", e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.RESPONSE_REPORT_FATTURE;
  }
}
