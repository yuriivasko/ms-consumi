package it.fai.ms.consumi.service.impl;

import static java.util.stream.Collectors.toList;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.fattura.HeaderDispositivo;
import it.fai.ms.common.jms.fattura.PedaggioBodyDTO;
import it.fai.ms.common.jms.fattura.PedaggioSezione1;
import it.fai.ms.common.jms.fattura.PedaggioSezione2;
import it.fai.ms.common.jms.fattura.ReportDispositiviDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.repository.device.TipoSupporto;
import it.fai.ms.consumi.repository.device.TipoSupportoRepository;
import it.fai.ms.consumi.repository.tollcharger.model.TollChargerEntity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione2Entity;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoPedaggioSezione2Repository;
import it.fai.ms.consumi.service.TollChargerService;
import it.fai.ms.consumi.service.ViewAllegatiPedaggiService;

@Service
@Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
public class ViewAllegatiPedaggiServiceImpl implements ViewAllegatiPedaggiService {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final static String UNDERSCORE = "_";

  private final ViewAllegatoPedaggioSezione2Repository sezione2Repo;

  private final TipoSupportoRepository tipoSupportoRepo;

  private final TollChargerService toolChargerService;

  public ViewAllegatiPedaggiServiceImpl(final ViewAllegatoPedaggioSezione2Repository _sezione2Repo,
                                        final TipoSupportoRepository _tipoSupportoRepo,
                                        final TollChargerService _toolChargerService) {
    sezione2Repo = _sezione2Repo;
    tipoSupportoRepo = _tipoSupportoRepo;
    toolChargerService = _toolChargerService;
  }

  @Override
  public List<ViewAllegatoPedaggioSezione2Entity> getDettagliStanziamento(List<UUID> dettagliStanziamentoId) {
    log.debug("Search dettagli stanziamento to {}", dettagliStanziamentoId);
    List<ViewAllegatoPedaggioSezione2Entity> dataAllegatiPedaggio = sezione2Repo.findByDettaglioStanziamentoId(dettagliStanziamentoId);
    log.debug("Found {} elements", dataAllegatiPedaggio != null ? dataAllegatiPedaggio.size() : 0);
    return dataAllegatiPedaggio;
  }

  @Override
  public List<PedaggioBodyDTO> createBodyMessageAndSection2(List<ViewAllegatoPedaggioSezione2Entity> dataAllegatiPedaggio,
                                                            final RequestsCreationAttachments requestCreationAttachments) {
    Long tStart = System.currentTimeMillis();

    final String tipoServizio = requestCreationAttachments.getTipoServizio();
    final String nazioneFatturazione = requestCreationAttachments.getNazioneFatturazione();
    log.debug("TipoServizio: {} - NazioneFatturazione: {}", tipoServizio, nazioneFatturazione);

    Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapContrattoAllegatiPedaggi = new LinkedHashMap<>();
    dataAllegatiPedaggio.forEach(v -> {
      List<ViewAllegatoPedaggioSezione2Entity> list = new ArrayList<>();
      String codiceContratto = v.getCodiceContratto();
      if (mapContrattoAllegatiPedaggi.containsKey(codiceContratto)) {
        list = mapContrattoAllegatiPedaggi.get(codiceContratto);
      }
      list.add(v);
      mapContrattoAllegatiPedaggi.put(codiceContratto, list);
    });

    Set<String> codiciContratto = mapContrattoAllegatiPedaggi.keySet();
    log.debug("Find {} codici contratto", ((codiciContratto != null) ? codiciContratto.size() : null));
    List<PedaggioBodyDTO> bodyWithSection2 = codiciContratto.stream()
                                                            .map(contratto -> {
                                                              PedaggioBodyDTO pedaggioBody = new PedaggioBodyDTO();
                                                              pedaggioBody.setContratto(contratto);

                                                              List<ReportDispositiviDTO> reportDispositiviDTO = null;
                                                              List<ViewAllegatoPedaggioSezione2Entity> dettagli = mapContrattoAllegatiPedaggi.get(contratto);

                                                              if (dettagli != null && !dettagli.isEmpty()) {
                                                                log.debug("Size Dettagli: {}", dettagli.size());
                                                                log.trace("Dettagli: {}", dettagli);

                                                                Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapDispositivi = createMapByDispositivo(dettagli,
                                                                                                                                                              false);
                                                                Set<String> keyDispositivi = mapDispositivi.keySet();
                                                                reportDispositiviDTO = keyDispositivi.stream()
                                                                                                     .map(keyDevice -> {
                                                                                                       log.trace("Manage device key: {}",
                                                                                                                 keyDevice);
                                                                                                       ReportDispositiviDTO reportDispositivi = new ReportDispositiviDTO();
                                                                                                       List<ViewAllegatoPedaggioSezione2Entity> list = mapDispositivi.get(keyDevice);
                                                                                                       reportDispositivi.setHeaderDispositivo(createHeaderDispositivo(list.get(0),
                                                                                                                                                                      tipoServizio,
                                                                                                                                                                      nazioneFatturazione));
                                                                                                       List<PedaggioSezione2> consumi = list.stream()
                                                                                                                                            .map(d -> {
                                                                                                                                              log.debug("mapToPedaggioSezione2 from Dettaglio: {}",
                                                                                                                                                        d);
                                                                                                                                              PedaggioSezione2 pedaggioSezione2 = mapToPedaggioSezione2(d);
                                                                                                                                              return pedaggioSezione2;
                                                                                                                                            })
                                                                                                                                            .collect(toList());
                                                                                                       reportDispositivi.setConsumi(consumi);
                                                                                                       return reportDispositivi;
                                                                                                     })
                                                                                                     .collect(toList());
                                                              } else {
                                                                log.warn("Not found dettagli to contratto: {} and dettagli stanziamento: {}",
                                                                         contratto, dataAllegatiPedaggio);
                                                              }
                                                              pedaggioBody.setReportDispositiviDTO(reportDispositiviDTO);
                                                              return (pedaggioBody);
                                                            })
                                                            .collect(toList());

    log.info("Created body with section 2 in {} ms", System.currentTimeMillis() - tStart);
    return bodyWithSection2;
  }

  @Override
  public List<PedaggioBodyDTO> addSection1OnBody(List<ViewAllegatoPedaggioSezione2Entity> dataAllegatiPedaggio, Instant invoiceDate,
                                                 List<PedaggioBodyDTO> bodyWithoutSection1) {
    Long tStart = System.currentTimeMillis();
    log.info("Add section1 on body");
    Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapSection1 = createMapByDispositivo(dataAllegatiPedaggio, true);
    List<PedaggioBodyDTO> pedaggiBody = new ArrayList<>();
    pedaggiBody = bodyWithoutSection1.stream()
                                     .map(pedaggio -> {
                                       final Long t1 = System.currentTimeMillis();
                                       final String contratto = pedaggio.getContratto();
                                       log.debug("Contratto: {}", contratto);
                                       Set<String> keySet = mapSection1.keySet();
                                       List<PedaggioSezione1> pedaggiSection1 = keySet.stream()
                                                                                      .map(k -> {
                                                                                        final Long t2 = System.currentTimeMillis();
                                                                                        List<ViewAllegatoPedaggioSezione2Entity> list = mapSection1.get(k);
                                                                                        ViewAllegatoPedaggioSezione2Entity element = list.get(0);
                                                                                        PedaggioSezione1 pedaggioSection1 = generatePedaggioSezione1(element);

                                                                                        LocalDate invoiceLocalDate = LocalDateTime.ofInstant(invoiceDate,
                                                                                                                                             ZoneOffset.UTC)
                                                                                                                                  .toLocalDate();
                                                                                        setTotaliOnPedaggioSection1(pedaggioSection1,
                                                                                                                    mapSection1, k,
                                                                                                                    invoiceLocalDate);
                                                                                        log.debug("Generated pedaggio section 1 in {} ms",
                                                                                                  System.currentTimeMillis() - t2);
                                                                                        return pedaggioSection1;
                                                                                      })
                                                                                      .collect(toList());

                                       List<PedaggioSezione1> reportMensileDTO = pedaggio.getReportMensileDTO();
                                       if (reportMensileDTO == null) {
                                         reportMensileDTO = new ArrayList<>();
                                       }
                                       log.trace("Add all pedaggiSezione1: {}", pedaggiSection1);
                                       reportMensileDTO.addAll(pedaggiSection1);
                                       pedaggio.setReportMensileDTO(reportMensileDTO);
                                       log.debug("Generated report mensile in {} ms", System.currentTimeMillis() - t1);
                                       return pedaggio;
                                     })
                                     .collect(toList());

    log.debug("Generated all section 1 in {} ms", System.currentTimeMillis() - tStart);
    return pedaggiBody;
  }

  private Map<String, List<ViewAllegatoPedaggioSezione2Entity>> createMapByDispositivo(List<ViewAllegatoPedaggioSezione2Entity> dettagli,
                                                                                       boolean skipClasseEuroKey) {

    Map<String, List<ViewAllegatoPedaggioSezione2Entity>> map = new LinkedHashMap<>();
    dettagli.forEach(d -> {
      List<ViewAllegatoPedaggioSezione2Entity> viewEntities = new ArrayList<>();
      String key = composeKey(d, skipClasseEuroKey);
      if (map.containsKey(key)) {
        viewEntities = map.get(key);
      }
      viewEntities.add(d);
      log.trace("put list size: {} in {}", viewEntities.size(), key);
      map.put(key, viewEntities);
    });
    return map;
  }

  private void setTotaliOnPedaggioSection1(PedaggioSezione1 pedaggioSection1,
                                           Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapSection1, String key,
                                           LocalDate invoiceLocalDate) {
    Double totale = 0d;
    Double totaleMeseCorrente = getTotaleMeseCorrente(mapSection1, key, invoiceLocalDate);
    log.debug("Key: {} - Totale mese corrente: {}", key, totaleMeseCorrente);
    totale += totaleMeseCorrente;
    pedaggioSection1.setTotaleMeseCorrente(totaleMeseCorrente);
    Double totaleMesePrecedente = getTotaleMesePrecedente(mapSection1, key, invoiceLocalDate);
    log.debug("Key: {} - Totale mese precendenti: {}", key, totaleMesePrecedente);
    totale += totaleMesePrecedente;
    pedaggioSection1.setTotaleMesePrecedente(totaleMesePrecedente);
    Double totale2MesiPrecedenti = getTotale2MesiPrecedenti(mapSection1, key, invoiceLocalDate);
    log.debug("Key: {} - Totale 2 mesi precendenti: {}", key, totale2MesiPrecedenti);
    totale += totale2MesiPrecedenti;
    pedaggioSection1.setTotale2MesiPrecedenti(totale2MesiPrecedenti);
    Double totaleAltriMesi = getTotaleAltriMesi(mapSection1, key, invoiceLocalDate);
    log.debug("Key: {} - Totale altri mesi precedenti: {}", key, totaleAltriMesi);
    totale += totaleAltriMesi;
    pedaggioSection1.setTotaleAltriMesiPrecedenti(totaleAltriMesi);

    pedaggioSection1.setTotale(totale);
  }

  private PedaggioSezione1 generatePedaggioSezione1(ViewAllegatoPedaggioSezione2Entity element) {
    PedaggioSezione1 pedaggio = new PedaggioSezione1();
    pedaggio.setCodiceContratto(element.getCodiceContratto());
    pedaggio.setTipoDispositivo(element.getTipoDispositivo());
    pedaggio.setPannumber(StringUtils.isNotBlank(element.getPanNumber()) ? element.getPanNumber() : element.getSerialNumber());
    pedaggio.setNazioneVeicolo(element.getNazioneVeicolo());
    pedaggio.setTargaVeicolo(element.getTargaVeicolo());

    return pedaggio;
  }

  private Double getTotaleAltriMesi(Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapSection1, String k,
                                    LocalDate invoiceLocalDate) {
    List<ViewAllegatoPedaggioSezione2Entity> list = mapSection1.get(k);
    Double total = getTotale(list, invoiceLocalDate, 3, false);
    log.debug("Total other previous month by date '{}': {}", invoiceLocalDate, total);
    return total;
  }

  private Double getTotale2MesiPrecedenti(Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapSection1, String k,
                                          LocalDate invoiceLocalDate) {
    List<ViewAllegatoPedaggioSezione2Entity> list = mapSection1.get(k);
    Double total = getTotale(list, invoiceLocalDate, 2, true);
    log.debug("Total 2 previous month by date '{}': {}", invoiceLocalDate, total);
    return total;
  }

  private Double getTotaleMesePrecedente(Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapSection1, String k,
                                         LocalDate invoiceLocalDate) {
    List<ViewAllegatoPedaggioSezione2Entity> list = mapSection1.get(k);
    Double total = getTotale(list, invoiceLocalDate, 1, true);
    log.debug("Total previous month by date '{}': {}", invoiceLocalDate, total);
    return total;
  }

  private Double getTotaleMeseCorrente(Map<String, List<ViewAllegatoPedaggioSezione2Entity>> mapSection1, String k,
                                       LocalDate invoiceLocalDate) {
    List<ViewAllegatoPedaggioSezione2Entity> list = mapSection1.get(k);
    Double total = getTotale(list, invoiceLocalDate, 0, true);
    log.debug("Total current month by date '{}': {}", invoiceLocalDate, total);
    return total;
  }

  private Double getTotale(final List<ViewAllegatoPedaggioSezione2Entity> list, LocalDate invoiceLocalDate, int diffToCurrentMonth,
                           boolean equal) {
    LocalDate date = invoiceLocalDate;
    if (diffToCurrentMonth > 0) {
      log.debug("minus {} month", diffToCurrentMonth);
      date = invoiceLocalDate.minusMonths(diffToCurrentMonth);
    }
    int month = date.getMonthValue();
    int year = date.getYear();
    Double total = list.stream()
                       .filter(v -> checkDateIsEqualOrLessEqual(v, month, year, equal))
                       .collect(Collectors.summingDouble(v -> v.getImporto()));
    log.debug("Totale month {} - year {}: {}", month, year, total);
    return total;
  }

  private boolean checkDateIsEqualOrLessEqual(ViewAllegatoPedaggioSezione2Entity v, int month, int year, boolean equal) {
    boolean isValidDate = false;
    LocalDate data = v.getDataUscita();
    log.debug("Data to check: {}", data);
    if (equal) {
      if (data.getMonthValue() == month && data.getYear() == year)
        isValidDate = true;
    } else {
      if (data.getMonthValue() <= month && data.getYear() <= year)
        isValidDate = true;
    }
    return isValidDate;
  }

  private String composeKey(ViewAllegatoPedaggioSezione2Entity v, boolean skipClasseEuro) {
    String codiceContratto = v.getCodiceContratto();
    String tipoDispositivo = v.getTipoDispositivo();
    String panNumber = v.getPanNumber();
    String serialNumber = v.getSerialNumber();
    String nazioneVeicolo = v.getNazioneVeicolo();
    String targaVeicolo = v.getTargaVeicolo();
    String classificazioneEuro = v.getClassificazioneEuro();

    String key = String.join(UNDERSCORE, codiceContratto, tipoDispositivo, panNumber, serialNumber, nazioneVeicolo, targaVeicolo);
    if (!skipClasseEuro) {
      key = String.join(UNDERSCORE, key, classificazioneEuro);
    }
    log.trace("Key: {}", key);
    return key;
  }

  private HeaderDispositivo createHeaderDispositivo(final ViewAllegatoPedaggioSezione2Entity entity, final String tipoServizio,
                                                    final String nazioneFatturazione) {
    HeaderDispositivo headerDispositivo = new HeaderDispositivo();
    String tipoDispositivo = entity.getTipoDispositivo();
    headerDispositivo.setTipoDispositivo(tipoDispositivo);
    headerDispositivo.setTargaVeicolo(entity.getTargaVeicolo());
    headerDispositivo.setNazioneVeicolo(entity.getNazioneVeicolo());
    headerDispositivo.setClasseTariffaria(entity.getClasseTariffaria());
    headerDispositivo.setPanNumber(entity.getPanNumber());
    headerDispositivo.setSerialNumber(entity.getSerialNumber());
    headerDispositivo.setClassificazioneEuro(entity.getClassificazioneEuro());

    headerDispositivo.setTipoSupportoFix(getTipoSupportoByNomeTipoDispositivo(tipoDispositivo));
    headerDispositivo.setTipoServizio(tipoServizio);
    headerDispositivo.setNazioneFatturazione(nazioneFatturazione);
    log.debug("Header dispositivo: {}", headerDispositivo);
    return headerDispositivo;
  }

  public String getTipoSupportoByNomeTipoDispositivo(String tipoDispositivo) {
    String tipoSupporto = "";
    log.debug("Get TipoSupporto by TipoDispositivo: {}", tipoDispositivo);
    if (StringUtils.isNotBlank(tipoDispositivo)) {
      TipoSupporto deviceType = tipoSupportoRepo.findByTipoDispositivo(tipoDispositivo);
      if (deviceType != null) {
        tipoSupporto = deviceType.getTipoSupportoFix();
      }
    }
    log.debug("Found tipoSupporto: {}", tipoSupporto);
    return tipoSupporto;
  }

  private PedaggioSezione2 mapToPedaggioSezione2(ViewAllegatoPedaggioSezione2Entity d) {
    PedaggioSezione2 pedaggioSezione2 = new PedaggioSezione2();

    String causale = StringUtils.isNotBlank(d.getCausale()) ? d.getCausale() : "";
    pedaggioSezione2.setCausale(causale);

    manageAndSetFieldsAutostrada(d, pedaggioSezione2);

    pedaggioSezione2.setCodiceIngresso(d.getCodiceIngresso());
    pedaggioSezione2.setDescrizioneCaselloIngresso(d.getDescrizioneIngresso());
    LocalDate dataIngresso = d.getDataIngresso();
    pedaggioSezione2.setDataIngressoTransito(dataIngresso);
    pedaggioSezione2.setOraIngressoTransito(d.getOraIngresso());

    pedaggioSezione2.setCodiceUscita(d.getCodiceUscita());
    pedaggioSezione2.setDescrizioneCaselloUscita(d.getDescrizioneUscita());
    LocalDate dataUscita = d.getDataUscita();
    pedaggioSezione2.setDataUscitaTransito(dataUscita);
    pedaggioSezione2.setOraUscitaTransito(d.getOraUscita());

    pedaggioSezione2.setImportoNoTax(d.getImporto());
    pedaggioSezione2.setImportoTax(d.getImportoIva());
    pedaggioSezione2.setValuta(d.getValuta());
    pedaggioSezione2.setCambio(d.getCambio());
    pedaggioSezione2.setPercIva(d.getPercIva());
    pedaggioSezione2.setTipoConsumo(d.getTipoConsumo());
    pedaggioSezione2.setTipoHardware(d.getTipoHardware());

    pedaggioSezione2.setNrTransazione(d.getNumeroTransazione());
    pedaggioSezione2.setClasseTariffa(d.getClasseTariffaria());

    pedaggioSezione2.setCentroDiCosto(null);

    return pedaggioSezione2;
  }

  private void manageAndSetFieldsAutostrada(ViewAllegatoPedaggioSezione2Entity d, PedaggioSezione2 pedaggioSezione2) {
    pedaggioSezione2.setAutostrada("");
    pedaggioSezione2.setDescrizioneAutostrada("");
    String networkCode = d.getAutostrada();
    if (StringUtils.isNotBlank(networkCode)) {
      pedaggioSezione2.setAutostrada(networkCode);
      String networkDescription = getNetworkDescriptionByCode(networkCode);
      pedaggioSezione2.setDescrizioneAutostrada(networkDescription);
    }

  }

  public String getNetworkDescriptionByCode(String networkCode) {
    String networkDescription = "";

    Optional<TollChargerEntity> tollChargerOpt = toolChargerService.findOneByCode(networkCode);
    if (tollChargerOpt.isPresent()) {
      TollChargerEntity tollCharger = tollChargerOpt.get();
      networkDescription = tollCharger.getDescription();
      log.debug("Description network: {}", networkDescription);
    } else {
      log.debug("Not found Toll Charger by networkCode: {}", networkCode);
    }

    return networkDescription;
  }

  public class TipoSupportoCache {

    private String  tipoSupporto;
    private Instant lastInsert;

    public TipoSupportoCache() {
    }

    public TipoSupportoCache(String tipoSupporto) {
      this.tipoSupporto = tipoSupporto;
      this.lastInsert = Instant.now();
    }

    public String getTipoSupporto() {
      return tipoSupporto;
    }

    public void setTipoSupporto(String tipoSupporto) {
      this.tipoSupporto = tipoSupporto;
    }

    public Instant getLastInsert() {
      return lastInsert;
    }

    public void setLastInsert(Instant lastInsert) {
      this.lastInsert = lastInsert;
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("TipoSupportoCache [tipoSupporto=");
      builder.append(tipoSupporto);
      builder.append(", lastInsert=");
      builder.append(lastInsert);
      builder.append("]");
      return builder.toString();
    }
  }

}
