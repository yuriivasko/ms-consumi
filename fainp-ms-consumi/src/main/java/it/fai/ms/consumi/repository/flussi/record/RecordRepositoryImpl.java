package it.fai.ms.consumi.repository.flussi.record;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.adapter.elasticsearch.ElasticSearchFeignClient;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.ElasticSearchRecord;

@Repository
@Validated
public class RecordRepositoryImpl<T extends CommonRecord> implements RecordRepository<T> {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final ElasticSearchFeignClient elasticSearchClient;

  @Inject
  public RecordRepositoryImpl(ElasticSearchFeignClient _elasticSearchClient) {
    elasticSearchClient = _elasticSearchClient;
  }

  @Override
  public void persist(@NotNull final T _record) {

    _log.debug(" Persisting {}", _record);

    try {
      ElasticSearchRecord<T> es_record = new ElasticSearchRecord<>();
      es_record.setRecordType(_record.getClass()
                                     .getSimpleName());
      es_record.setRawdata(_record);
      es_record.setIngestionTime(_record.getIngestion_time());
      es_record.setRecordCode(_record.getRecordCode());
      es_record.setTransactionDetail(_record.getTransactionDetail());
      es_record.setFileName(_record.getFileName());
      elasticSearchClient.create(es_record.getId(), es_record.getRecordType(), es_record);
    } catch (final Throwable _t) {
      _log.error("ElasticSearch record persist failure [{}-{}]", _record.getFileName(), _record.getRowNumber(), _t);
    }
  }

}
