package it.fai.ms.consumi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.enumeration.PriceSource;

@Repository
public interface ServiceProviderRepository extends JpaRepository<ServiceProvider, Long> {

  ServiceProvider findByProviderCode(String providerCode);

  ServiceProvider findFirstByProviderCode(String providerCode);

  List<ServiceProvider> findByPriceSourceOrderByProviderCodeAsc(PriceSource priceSource);

}
