package it.fai.ms.consumi.adapter.nav;

import java.util.List;

import it.fai.ms.consumi.domain.Article;

public interface NavArticlesAdapter {

  List<Article> getAllArticles();

}
