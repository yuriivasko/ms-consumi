package it.fai.ms.consumi.repository.flussi.record;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Repository;

import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;

@Repository
public interface RecordRepository<T extends BaseRecord> {

  void persist(@NotNull T record);

}
