package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.List;

import it.fai.ms.consumi.repository.caselli.model.CaselliEntity;
import it.fai.ms.consumi.repository.flussi.record.model.stcon.StconRecord;

public interface CaselliService {

  CaselliEntity save(StconRecord record);

  CaselliEntity save(StconRecord record, String firstLine);

  CaselliEntity findByGateIdentifier(String code, Instant dtm);

  List<CaselliEntity> findByGateIdentifier(String code);

  CaselliEntity findByGlobalGateIdentifier(String code, Instant dtm);

  List<CaselliEntity> findByGlobalGateIdentifier(String code);

}
