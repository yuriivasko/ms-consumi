package it.fai.ms.consumi.service.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.vehicle.dto.StoricoClasseEuroDMLDTO;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

@Component
public class StoricoClasseEuroDMLDTOMapper {

  // TODO : restano campi da mappare

  public StoricoClasseEuroDMLDTO toDTO(final StoricoVeicolo data) {
    StoricoClasseEuroDMLDTO dto = new StoricoClasseEuroDMLDTO();
    dto.setDmlRevisionTimestamp(data.getDmlRevisionTimestamp());
    dto.setDmlUniqueIdentifier(data.getDmlUniqueIdentifier());
    dto.setClasseEuro(data.getClasseEuro());
    dto.setVehicleDmlUniqueIdentifier(data.getDmlUniqueIdentifier());
    dto.setDataVariazione(data.getDataVariazione());
    //TODO: dto.setDataFineVariazione(data.get);
    return dto;
  }

}
