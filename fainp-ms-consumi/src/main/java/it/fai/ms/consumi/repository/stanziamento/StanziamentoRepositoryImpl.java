package it.fai.ms.consumi.repository.stanziamento;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntityMapper;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity_;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoCodeGeneratorInterface;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoLogicKey;
import it.fai.ms.consumi.util.SqlUtil;

@Repository
@Validated
@Transactional
public class StanziamentoRepositoryImpl implements StanziamentoRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoEntityMapper  dettaglioStanziamentoEntityMapper;
  private final EntityManager                      em;
  private final StanziamentoEntityMapper           stanziamentoEntityMapper;
  private final StanziamentoCodeGeneratorInterface stanziamentoCodeGenerator;

  @Autowired
  DettaglioStanziamantiRepository dettaglioStanziamantiRepository;

  public StanziamentoRepositoryImpl(final EntityManager _entityManager, final StanziamentoEntityMapper _entityMapper,
                                    final DettaglioStanziamentoEntityMapper _dettaglioStanziamentoEntityMapper,
                                    final StanziamentoCodeGeneratorInterface _stanziamentoCodeGenerator) {
    em = _entityManager;
    stanziamentoEntityMapper = _entityMapper;
    dettaglioStanziamentoEntityMapper = _dettaglioStanziamentoEntityMapper;
    stanziamentoCodeGenerator = _stanziamentoCodeGenerator;
  }

  @Override
  public StanziamentoEntity cloneAndModify(@NotNull final StanziamentoEntity _stanziamento, final InvoiceType _invoiceType) {

    _log.debug(" Cloning and modify : {}", _stanziamento.getCode());

    final var stanziamentoEntity = find(_stanziamento.getCode());

    final var newStanziamentoEntity = new StanziamentoEntity(null);
    BeanUtils.copyProperties(stanziamentoEntity, newStanziamentoEntity);
    if (_invoiceType != null) {
      newStanziamentoEntity.setStatoStanziamento(_invoiceType);
    }
    newStanziamentoEntity.refreshCode(stanziamentoCodeGenerator);
    if (_stanziamento.getStatoStanziamento()
                     .equals(InvoiceType.P)) {
      newStanziamentoEntity.setCodiceStanziamentoProvvisorio(_stanziamento.getCode());
    }
    _log.debug(" Entity cloned : old entity code {} - new entity code {}", _stanziamento.getCode(), newStanziamentoEntity.getCode());
    if (_stanziamento.getCode()
                     .equals(newStanziamentoEntity.getCode())) {
      throw new IllegalArgumentException("Old entity code is equal to new entity code. This is not possible.");
    }

    return newStanziamentoEntity;
  }

  @Override
  public Set<String> findByCodiceStanziamentoIn(final Set<String> _codes) {
    if (_codes == null || _codes.isEmpty()) {
      return Collections.emptySet();
    }
    
    CriteriaBuilder cb = em.getCriteriaBuilder();
	List<List<String>> splittedData = SqlUtil.splitToSubList(_codes,SqlUtil.MAX_SIZE_PARAMETER);

	final Set<String> rsAll = new HashSet<>();

	for (Collection<String> data : splittedData) {
		
	    CriteriaQuery<StanziamentoEntity> query = cb.createQuery(StanziamentoEntity.class);
	    final Root<StanziamentoEntity> root = query.from(StanziamentoEntity.class);
	    query.select(root);
	    final List<Predicate> predicates = new ArrayList<>();
	    Predicate pIn = root.get(StanziamentoEntity_.CODE)
	                        .in(data);
	    predicates.add(pIn);
	    query.where(predicates.toArray(new Predicate[predicates.size()]));
	    TypedQuery<StanziamentoEntity> typedQuery = em.createQuery(query);
	    rsAll.addAll( typedQuery.getResultStream()
	                     .map(s -> s.getCode())
	                     .collect(toSet()));
    
	}
	
	return rsAll;
  }

  @Override
  public Stanziamento findByCodiceStanziamento(String _code) {
    _log.debug(" Searching {} by code : {}", Stanziamento.class.getSimpleName(), _code);
    final var entity = find(_code);
    _log.debug(" Found {}", entity);
    return stanziamentoEntityMapper.toDomain(entity);

  }

  @Override
  public Stanziamento findByCodiceStanziamento(String _code, StanziamentiDetailsType stanziamentiDetailsType) {
    _log.debug(" Searching {} by code : {}", Stanziamento.class.getSimpleName(), _code);
    final var entity = find(_code);
    _log.debug(" Found {}", entity);
    return stanziamentoEntityMapper.toDomain(entity,
                                             dettaglioStanziamantiRepository.findByCodiceStanziamento(_code, stanziamentiDetailsType));

  }

  @Override
  public StanziamentoEntity findByCode(final String _code) {

    _log.debug(" Searching {} by code : {}", Stanziamento.class.getSimpleName(), _code);

    final var entity = find(_code);

    _log.debug(" Found {}", entity);

    return entity;
  }

  @Override
  public List<Stanziamento> getAll() {

    List<Stanziamento> allStanziamenti = null;

    try {

      final CriteriaQuery<StanziamentoEntity> query = em.getCriteriaBuilder()
                                                        .createQuery(StanziamentoEntity.class);
      final Root<StanziamentoEntity> root = query.from(StanziamentoEntity.class);
      query.select(root);

      allStanziamenti = em.createQuery(query)
                          .getResultStream()
                          .peek(entity -> _log.debug(" Found {}", entity))
                          .map(entity -> stanziamentoEntityMapper.toDomain(entity))
                          .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      allStanziamenti = new ArrayList<>();
      _log.warn(" No {} found", StanziamentoEntity.class.getSimpleName());
    }

    return allStanziamenti;
  }

  @Override
  public Set<Stanziamento> findStanziamentoByLogicKey(@NotNull StanziamentoLogicKey _stanziamentoLogicKey,
                                                      boolean filterNotAlreadySentToNav) {
    return findStanziamentoByLogicKey(_stanziamentoLogicKey, filterNotAlreadySentToNav, null);
  }

  @Override
  public Set<Stanziamento> findStanziamentoByLogicKey(@NotNull StanziamentoLogicKey _stanziamentoLogicKey,
                                                      boolean filterNotAlreadySentToNav, StanziamentiDetailsType stanziamentiDetailsType) {

    _log.debug(" Searching {} by : {}", Stanziamento.class.getSimpleName(), _stanziamentoLogicKey);

    var criteriaQuery = em.getCriteriaBuilder()
                          .createQuery(StanziamentoEntity.class);
    var root = criteriaQuery.from(StanziamentoEntity.class);
    criteriaQuery.select(root);
    final List<Predicate> andPredicates = new ArrayList<>();

    // CHE MINCHIA SERVE QUESTA QUERY ??????
    /*
     * List results = em.createNativeQuery("select dsp.* \n" +
     * "from dettaglio_stanziamento_pedaggi dsp inner join dettaglio_stanziamento ds\n" + "on (ds.id=dsp.id)\n" +
     * "inner join dettagliostanziamento_stanziamento ds_s \n" + "on ds_s.dettaglio_stanziamenti_id = dsp.id\n" +
     * "inner join stanziamento s\n" + "on s.codice_stanziamento = ds_s.stanziamenti_codice_stanziamento\n" +
     * "where s.data_erogazione_servizio = ?\n" + "and s.nr_articolo = ? " + "and s.nr_fornitore=? " + "and s.targa=? "
     * + "and s.nr_cliente=? " + "and s.paese=?") .setParameter(1,_stanziamentoLogicKey.getDate())
     * .setParameter(2,_stanziamentoLogicKey.getArticleCode()) .setParameter(3,_stanziamentoLogicKey.getSupplierCode())
     * .setParameter(4,_stanziamentoLogicKey.getLicenseId()) .setParameter(5,_stanziamentoLogicKey.getCustomerId())
     * .setParameter(6,_stanziamentoLogicKey.getCountryId()) .getResultList();
     */

    andPredicates.add(em.getCriteriaBuilder()
                        .equal(root.get(StanziamentoEntity_.DATA_EROGAZIONE_SERVIZIO), _stanziamentoLogicKey.getDate()));
    andPredicates.add(em.getCriteriaBuilder()
                        .equal(root.get(StanziamentoEntity_.ARTICLE_CODE), _stanziamentoLogicKey.getArticleCode()));
    andPredicates.add(em.getCriteriaBuilder()
                        .equal(root.get(StanziamentoEntity_.NUMERO_FORNITORE), _stanziamentoLogicKey.getSupplierCode()));
    andPredicates.add(em.getCriteriaBuilder()
                        .equal(root.get(StanziamentoEntity_.PAESE), _stanziamentoLogicKey.getCountryId()));
    andPredicates.add(em.getCriteriaBuilder()
                        .equal(root.get(StanziamentoEntity_.NUMERO_CLIENTE), _stanziamentoLogicKey.getCustomerId()));
    andPredicates.add(em.getCriteriaBuilder()
                        .equal(root.get(StanziamentoEntity_.TARGA), _stanziamentoLogicKey.getLicenseId()));
    andPredicates.add(em.getCriteriaBuilder()
                        .equal(root.get(StanziamentoEntity_.INVOICE_TYPE), _stanziamentoLogicKey.getInvoiceType()));
    if (filterNotAlreadySentToNav) {
      andPredicates.add(em.getCriteriaBuilder()
                          .isNull(root.get(StanziamentoEntity_.DATE_QUEUING)));
    }
    criteriaQuery.where(em.getCriteriaBuilder()
                          .and(andPredicates.toArray(new Predicate[0])));
    TypedQuery<StanziamentoEntity> query = em.createQuery(criteriaQuery);

    return query.getResultList()
                .stream()
                .map(stanziamentoEntity -> {
                  Stanziamento stanziamento = null;
                  if (stanziamentiDetailsType == null) {
                    stanziamento = stanziamentoEntityMapper.toDomain(stanziamentoEntity);
                  } else {
                    Set<DettaglioStanziamento> det = dettaglioStanziamantiRepository.findByCodiceStanziamento(stanziamentoEntity.getCode(),
                                                                                                              stanziamentiDetailsType);
                    stanziamento = stanziamentoEntityMapper.toDomain(stanziamentoEntity, det);
                  }
                  return stanziamento;
                })
                .collect(toSet());
  }

  @Override
  public StanziamentoEntity create(@NotNull final StanziamentoEntity _allocationEntity) {
    final var allocationDetailsEntity = _allocationEntity.getDettaglioStanziamenti();
    _allocationEntity.getDettaglioStanziamenti()
                     .addAll(allocationDetailsEntity);
    _allocationEntity.getDettaglioStanziamenti()
                     .forEach(itm -> {
                       itm.getStanziamenti()
                          .add(_allocationEntity);
                     });

    em.persist(_allocationEntity);
    em.flush();

    _log.debug(" Persisted : {}", _allocationEntity);

    return _allocationEntity;
  }

  // @Override
  // public Stanziamento create(@NotNull final Stanziamento _allocation) {
  // final var allocationEntity = stanziamentoEntityMapper.toEntity(_allocation)
  // .incrementCounter();
  // final var allocationEntityPersisted = create(allocationEntity);
  //
  // return stanziamentoEntityMapper.toDomain(allocationEntityPersisted);
  // }

  @Override
  public StanziamentoEntity updateEntity(@NotNull StanziamentoEntity _entity) {
    _log.debug(" Updating entity: {}", _entity);
    em.flush();
    return _entity;
  }

  /*
   * @Override public void updateInvoiceData(@NotNull Stanziamento _stanziamento) {
   * _log.debug(" Updating Invoice Data on : {}", _stanziamento); final var entity = find(_stanziamento.getCode());
   * entity.setNumeroFattura(_stanziamento.getNumeroFattura()); entity.setDataFattura(_stanziamento.getDataFattura());
   * entity.setCodiceClienteFatturazione(_stanziamento.getCodiceClienteFatturazione()); em.flush(); //return
   * stanziamentoEntityMapper.toDomain(entity); }
   */

  @Override
  public void updateInvoiceData(@NotNull Stanziamento _stanziamento) {
    String _code = _stanziamento.getCode();
    _log.debug(" InvoiceData updating on code {}", _code);

    Query query = em.createQuery("UPDATE StanziamentoEntity SET numeroFattura = :numeroFattura, codiceClienteFatturazione = :codiceClienteFatturazione, dataFattura = :dataFattura WHERE code = :code");
    int updateCount = query.setParameter("code", _stanziamento.getCode())
                           .setParameter("numeroFattura", _stanziamento.getNumeroFattura())
                           .setParameter("codiceClienteFatturazione", _stanziamento.getCodiceClienteFatturazione())
                           .setParameter("dataFattura", _stanziamento.getDataFattura())
                           .executeUpdate();
    if (updateCount > 0) {
      _log.debug(" InvoiceData updated on {} ", _code);
    } else {
      _log.warn(" InvoiceData not updated (id not found {})", _code);
    }
  }

  @Override
  public void updateDateNavSent(@NotNull final Set<String> _codes, final Instant _date) {
    _codes.forEach(code -> updateDateNavSent(code, _date));
  }

  @Override
  public void updateDateQueuing(@NotNull final Set<String> _codes, final Instant _date) {
    _codes.forEach(code -> updateDateQueuing(code, _date));
  }

  private StanziamentoEntity find(final String _code) {
    return em.find(StanziamentoEntity.class, _code);
  }

  private void updateDateNavSent(@NotNull final String _code, final Instant _date) {
    _log.debug(" DateNavSent updating on code {} and date {}", _code, _date);

    Query query = em.createQuery("UPDATE StanziamentoEntity SET dateNavSent = :date WHERE code = :code");
    int updateCount = query.setParameter("code", _code)
                           .setParameter("date", _date)
                           .executeUpdate();
    if (updateCount > 0) {
      _log.debug(" DateNavSent updated on {} : {}", _code, _date);
    } else {
      _log.warn(" DateNavSent not updated (code not found {})", _code);
    }
  }

  private void updateDateQueuing(@NotNull final String _code, final Instant _date) {
    _log.debug(" DateQueuing updating on code {} and date {}", _code, _date);

    Query query = em.createQuery("UPDATE StanziamentoEntity SET dateQueuing = :date WHERE code = :code");
    int updateCount = query.setParameter("code", _code)
                           .setParameter("date", _date)
                           .executeUpdate();
    if (updateCount > 0) {
      _log.debug(" DateQueuing updated on {} : {}", _code, _date);
    } else {
      _log.warn(" DateQueuing not updated (id not found {})", _code);
    }
  }

  @Override
  public List<Stanziamento> find100NotQueuedToNav() {
    List<Stanziamento> stanziamentoList = null;

    try {
      CriteriaBuilder cb = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentoEntity> query = cb
        .createQuery(StanziamentoEntity.class);
      final Root<StanziamentoEntity> root = query.from(StanziamentoEntity.class);
      query.select(root);
      query.where(cb.isNull(root.get(StanziamentoEntity_.DATE_QUEUING)));

      stanziamentoList = em.createQuery(query)
        .setMaxResults(100)
        .getResultStream()

        .peek(entity -> _log.debug(" Found {}", entity))
        .map(entity -> stanziamentoEntityMapper.toDomain(entity))
        .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      stanziamentoList = new ArrayList<>();
      _log.warn(" No {} found", StanziamentoEntity.class.getSimpleName());
    }

    return stanziamentoList;
  }

  @Override
  public List<Stanziamento> find100NotQueuedToNav(Instant dataAcquisizioneFlusso) {
    List<Stanziamento> stanziamentoList = null;

    try {
      CriteriaBuilder cb = em.getCriteriaBuilder();
      final CriteriaQuery<StanziamentoEntity> query = cb
                                                        .createQuery(StanziamentoEntity.class);
      final Root<StanziamentoEntity> root = query.from(StanziamentoEntity.class);
      query.select(root);
      query.where(
        cb.and(
          cb.isNull(root.get(StanziamentoEntity_.DATE_QUEUING)),
          cb.equal(root.get(StanziamentoEntity_.DATA_ACQUISIZIONE_FLUSSO), dataAcquisizioneFlusso)
        )
      );

      stanziamentoList = em.createQuery(query)
        .setMaxResults(100)
        .getResultStream()

                           .peek(entity -> _log.debug(" Found {}", entity))
                           .map(entity -> stanziamentoEntityMapper.toDomain(entity))
                           .collect(toList());

    } catch (@SuppressWarnings("unused") final NoResultException _e) {
      stanziamentoList = new ArrayList<>();
      _log.warn(" No {} found", StanziamentoEntity.class.getSimpleName());
    }

    return stanziamentoList;
  }
}
