package it.fai.ms.consumi.service.petrolpump.consumer;

import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;

public interface PriceTableIntervalConsumer {

  public final static String OPEN_INTERVAL_PROCESSOR = "openInteval";

  public final static String CLOSED_INTERVAL_PROCESSOR = "closedInterval";

  void managePriceTable(PriceTableDTO priceTable) throws Exception;
  
}
