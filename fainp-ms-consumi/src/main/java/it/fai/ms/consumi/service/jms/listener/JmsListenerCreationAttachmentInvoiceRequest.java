package it.fai.ms.consumi.service.jms.listener;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.dto.document.allegatifattura.AllegatiFattureDTO;
import it.fai.ms.consumi.service.consumer.CreationAttachmentInvoiceConsumer;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsListenerCreationAttachmentInvoiceRequest implements JmsQueueListener {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final CreationAttachmentInvoiceConsumer consumerCreationAttachmentInvoice;

  @Autowired
  public JmsListenerCreationAttachmentInvoiceRequest(final CreationAttachmentInvoiceConsumer _consumerCreationAttachmentInvoice) {
    consumerCreationAttachmentInvoice = _consumerCreationAttachmentInvoice;
  }

  public void onMessage(Message _message) {
    AllegatiFattureDTO attachInvoiceDTO = null;
    try {
      try {
        Serializable object = ((ObjectMessage) _message).getObject();
        attachInvoiceDTO = (AllegatiFattureDTO) object;
      } catch (JMSException e) {
        log.error("JMS ObjectMessage isn't {}", AllegatiFattureDTO.class.getSimpleName());
        throw new RuntimeException(e);
      }
      log.info("Received JMS message {}", attachInvoiceDTO);
      consumerCreationAttachmentInvoice.consume(attachInvoiceDTO);
    } catch (Exception _e) {
      log.error("Error consuming message "+_message, _e);
      throw new RuntimeException(_e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.CREAZIONE_ALLEGATI_FATTURE_REQUEST;
  }
}
