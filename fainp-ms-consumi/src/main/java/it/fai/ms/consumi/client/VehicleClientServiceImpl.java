package it.fai.ms.consumi.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.vehicle.CentroDiCostroSearchDTO;
import it.fai.ms.consumi.config.ApplicationProperties;

@Service
public class VehicleClientServiceImpl implements VehicleClientService {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final VehicleClient         vehicleClient;
  private final ApplicationProperties applicationProperties;

  public VehicleClientServiceImpl(final VehicleClient vehicleClient, final ApplicationProperties applicationProperties) {
    this.vehicleClient = vehicleClient;
    this.applicationProperties = applicationProperties;
  }

  @Override
  public String getCentroDiCosto(final CentroDiCostroSearchDTO dto) {
    log.debug("Search centro di costo by DTO: {}", dto);
    String authorizationToken = getAuthorizationToken();
    String centroDiCosto = vehicleClient.getCentroDiCosto(authorizationToken, dto);
    log.debug("Found centro di costo by DTO: {}", centroDiCosto);
    return centroDiCosto;
  }

  private String getAuthorizationToken() {
    String authorizationHeader = applicationProperties.getAuthorizationHeader();
    return authorizationHeader;
  }

}
