package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.GlobalIdentifierType;

public class TrackyCardPedaggiStandardGlobalIdentifier extends GlobalIdentifier {
 
 
  public TrackyCardPedaggiStandardGlobalIdentifier(final String _id) {
    super(_id, GlobalIdentifierType.INTERNAL);
  }

  @Override
  public GlobalIdentifier newGlobalIdentifier() {
    throw new UnsupportedOperationException("not implemented");
  }

}
    
  
 
