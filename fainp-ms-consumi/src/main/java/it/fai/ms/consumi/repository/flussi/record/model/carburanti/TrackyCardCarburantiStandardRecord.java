package it.fai.ms.consumi.repository.flussi.record.model.carburanti;

import java.util.UUID;

import javax.money.MonetaryAmount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;

public class TrackyCardCarburantiStandardRecord
  extends CommonRecord
  implements GlobalIdentifierBySupplier {

  private static final long serialVersionUID = 156762402739692302L;

  private String recordCode                  = "";
  private String counter                     = "";
  private String pointCode                   = "";
  private String panField1IsoCode            = "";
  private String panField2GroupCode          = "";
  private String panField3CustomerCode       = "";
  private String panField4DeviceNumber       = "";
  private String panField5ControlDigit       = "";
  //AAAAMMGG
  private String date                        = "";
  //HHMMSS
  private String time                        = "";
  private String pumpNumber                  = "";
  //NNNNNN 4 interi + 2 decimali
  private String litres                      = "";
  private String kilometers                  = "";
  private String ticket                      = "";
  private String productCode                 = "";
  private String priceFaiReservedVatIncluded = "";
  private String priceExposedVatIncluded     = "";
  private String priceBaseVatIncluded        = "";

  private String vatPercentage               = "";
  private String currency                    = "";
  private String partnerCode                 = "";
  private String unitOfMeasure               = "";

  // From vatPercentage
  private Float          vat_percentage_float;

  private final String globalIdentifier;

  @JsonIgnore
  MonetaryAmount monetaryPriceReferenceVatIncluded;

  @JsonCreator
  public TrackyCardCarburantiStandardRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID().toString();
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }

  public Float getVatPercentageFloat() {
    return vat_percentage_float;
  }

  @JsonIgnore
  public MonetaryAmount getMonetaryPriceReferenceVatIncluded() {
    if(monetaryPriceReferenceVatIncluded == null) {
      if(StringUtils.isBlank(this.currency)) {
        this.currency = "EUR";
      }
      monetaryPriceReferenceVatIncluded = MonetaryUtils.strToMonetary(priceFaiReservedVatIncluded, -5, this.currency);
      if(monetaryPriceReferenceVatIncluded.isZero()) {
        monetaryPriceReferenceVatIncluded = MonetaryUtils.strToMonetary(priceExposedVatIncluded, -5, this.currency);
      }
    }
    return monetaryPriceReferenceVatIncluded;
  }


  public void setPriceReferenceVatIncluded(String unitPrice, String currency) {
    this.monetaryPriceReferenceVatIncluded = MonetaryUtils.strToMonetary(unitPrice, currency);
  }

  public void setPriceFaiReservedVatIncluded(String priceFaiReservedVatIncluded) {
    this.priceFaiReservedVatIncluded = priceFaiReservedVatIncluded;
  }

  public void setPriceExposedVatIncluded(String priceExposedVatIncluded) {
    this.priceExposedVatIncluded = priceExposedVatIncluded;
  }

  public void setVatPercentage(String vatPercentage) {
    setVatPercentage(vatPercentage, 100);
  }

  public void setVatPercentage(String vatPercentage, int factor) {
    this.vatPercentage = vatPercentage;
    this.vat_percentage_float = NumericUtils.strToFloat(vatPercentage, factor);
  }

  @Override
  public String getRecordCode() {
    return recordCode;
  }

  @Override
  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }

  public String getCounter() {
    return counter;
  }

  public void setCounter(String counter) {
    this.counter = counter;
  }

  public String getPointCode() {
    return pointCode;
  }

  public void setPointCode(String pointCode) {
    this.pointCode = pointCode;
  }

  public String getPanField1IsoCode() {
    return panField1IsoCode;
  }

  public void setPanField1IsoCode(String panField1IsoCode) {
    this.panField1IsoCode = panField1IsoCode;
  }

  public String getPanField2GroupCode() {
    return panField2GroupCode;
  }

  public void setPanField2GroupCode(String panField2GroupCode) {
    this.panField2GroupCode = panField2GroupCode;
  }

  public void setPriceBaseVatIncluded(String priceBaseVatIncluded) {
    this.priceBaseVatIncluded = priceBaseVatIncluded;
  }

  public String getPanField3CustomerCode() {
    return panField3CustomerCode;
  }

  public void setPanField3CustomerCode(String panField3CustomerCode) {
    this.panField3CustomerCode = panField3CustomerCode;
  }

  public String getPanField4DeviceNumber() {
    return panField4DeviceNumber;
  }

  public void setPanField4DeviceNumber(String panField4DeviceNumber) {
    this.panField4DeviceNumber = panField4DeviceNumber;
  }

  public String getPanField5ControlDigit() {
    return panField5ControlDigit;
  }

  public void setPanField5ControlDigit(String panField5ControlDigit) {
    this.panField5ControlDigit = panField5ControlDigit;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getPumpNumber() {
    return pumpNumber;
  }

  public void setPumpNumber(String pumpNumber) {
    this.pumpNumber = pumpNumber;
  }

  public String getLitres() {
    return litres;
  }

  public void setLitres(String litres) {
    this.litres = litres;
  }

  public String getKilometers() {
    return kilometers;
  }

  public void setKilometers(String kilometers) {
    this.kilometers = kilometers;
  }

  public String getTicket() {
    return ticket;
  }

  public void setTicket(String ticket) {
    this.ticket = ticket;
  }

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public String getPriceFaiReservedVatIncluded() {
    return priceFaiReservedVatIncluded;
  }

  public String getPriceExposedVatIncluded() {
    return priceExposedVatIncluded;
  }

  public String getPriceBaseVatIncluded() {
    return priceBaseVatIncluded;
  }

  public String getVatPercentage() {
    return vatPercentage;
  }

  public String getCurrency() {
    return (currency == null ? null : currency.trim().toUpperCase());
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Float getVat_percentage_float() {
    return vat_percentage_float;
  }

  public void setVat_percentage_float(Float vat_percentage_float) {
    this.vat_percentage_float = vat_percentage_float;
  }

  public String getPartnerCode() {
    return partnerCode;
  }

  public void setPartnerCode(String partnerCode) {
    this.partnerCode = partnerCode;
  }

  public String getUnitOfMeasure() {
    return unitOfMeasure;
  }

  public void setUnitOfMeasure(String unitOfMeasure) {
    this.unitOfMeasure = unitOfMeasure;
  }

  @Override
  public String toString() {
    return "TrackyCardCarburantiStandardRecord{" + "recordCode='" + recordCode + '\'' + ", counter='" + counter + '\'' + ", pointCode='"
           + pointCode + '\'' + ", partnerCode='" + partnerCode + '\'' + ", panField1IsoCode='" + panField1IsoCode + '\''
           + ", panField2GroupCode='" + panField2GroupCode + '\'' + ", panField3CustomerCode='" + panField3CustomerCode + '\''
           + ", panField4DeviceNumber='" + panField4DeviceNumber + '\'' + ", panField5ControlDigit='" + panField5ControlDigit + '\''
           + ", date='" + date + '\'' + ", time='" + time + '\'' + ", pumpNumber='" + pumpNumber + '\'' + ", litres='" + litres + '\''
           + ", kilometers='" + kilometers + '\'' + ", ticket='" + ticket + '\'' + ", productCode='" + productCode + '\''
           + ", priceFaiReservedVatIncluded='" + priceFaiReservedVatIncluded + '\'' + ", priceExposedVatIncluded='"
           + priceExposedVatIncluded + '\'' + ", priceBaseVatIncluded='" + priceBaseVatIncluded + '\'' + ", vatPercentage='" + vatPercentage
           + '\'' + ", currency='" + currency + '\'' + ", vat_percentage_float="
           + vat_percentage_float + "} " + super.toString();
  }

}
