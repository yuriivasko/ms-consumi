package it.fai.ms.consumi.service.jms.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.publisher.JmsQueueSenderUtil;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.service.dto.TransactionFuelDetailDTO;

@Service
@Transactional
public class DettaglioCarburanteJmsProducer {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final JmsProperties jmsProperties;

  public DettaglioCarburanteJmsProducer(final JmsProperties _jmsProperties) {
    jmsProperties = _jmsProperties;
  }

  public void sendDetailFuelMessage(DettaglioStanziamentoCarburanteEntity entity) {
    log.trace("Send detail fuel: {}", entity);
    if (isNotStorno(entity)) {
      TransactionFuelDetailDTO dto = mapToTransactionFuelDetail(entity);
      JmsQueueSenderUtil.publish(jmsProperties, JmsQueueNames.TRANSACTION_FUEL_DETAIL, dto);

      log.debug("{} was sent in queue...", dto);
    } else {
      log.debug("Skip send detail Fuel message, because detail is STORNO...");
    }
  }

  private boolean isNotStorno(DettaglioStanziamentoCarburanteEntity entity) {
    return !entity.isStorno();
  }

  private TransactionFuelDetailDTO mapToTransactionFuelDetail(DettaglioStanziamentoCarburanteEntity entity) {
    TransactionFuelDetailDTO dto = new TransactionFuelDetailDTO();
    dto.setGlobalIdentifier(entity.getGlobalIdentifier());
    dto.setDetailId(entity.getId());
    dto.setPrice(entity.getPrezzoUnitarioCalcolatoNoiva());
    dto.setCost(entity.getCostoUnitarioCalcolatoNoiva());
    dto.setQuantity(entity.getQuantita());
    dto.setPercIva(entity.getPercIva());
    dto.setTipoSorgente(entity.getTipoSorgente());
    dto.setDataOraUtilizzo(entity.getDataOraUtilizzo());
    dto.setCodiceTrackyCard(entity.getCodiceTrackycard());
    dto.setPuntoErogazione(entity.getPuntoErogazione());
    return dto;
  }

}
