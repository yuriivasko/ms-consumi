package it.fai.ms.consumi.service.processor.consumi;

import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.GlobalGate;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.TollPoint;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleConsumer;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.VehicleSupplier;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.DeviceConsumer;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.consumi.SecondaryDeviceSupplier;
import it.fai.ms.consumi.domain.consumi.carburante.ConsumoCarburante;
import it.fai.ms.consumi.domain.consumi.generici.ConsumoGenerico;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.repository.CaselliEntityRepository;
import it.fai.ms.consumi.repository.caselli.model.CaselliEntity;

public abstract class ConsumoDettaglioStanziamentoMapper<C extends Consumo,DS extends DettaglioStanziamento> {

  public abstract DS mapConsumoToDettaglioStanziamento(@NotNull final C consumo);

  protected DettaglioStanziamentoPedaggio toDettaglioStanziamentoPedaggio(final ConsumoPedaggio _consumo,
                                                                  final GlobalIdentifier _globalIdentifier) {

    var dettaglioStanziamento = new DettaglioStanziamentoPedaggio(_globalIdentifier);

    setGenericAttributes(dettaglioStanziamento, _consumo);
    dettaglioStanziamento.setTollPointExit(_consumo.getTollPointExit());
    dettaglioStanziamento.setTollPointEntry(_consumo.getTollPointEntry());
    dettaglioStanziamento.setNetworkCode(_consumo.getNetworkCode());
    String routeName = _consumo.getRouteName();
    if (StringUtils.isBlank(routeName)){
      dettaglioStanziamento.setRouteName(_consumo.getRoute());
    }else
      dettaglioStanziamento.setRouteName(routeName);
    dettaglioStanziamento.setImponibileAdr(_consumo.getImponibileAdr());
    return dettaglioStanziamento;
  }

  protected DettaglioStanziamentoCarburante toDettaglioStanziamentoCarburanti(final ConsumoCarburante _consumo,
                                                                    final GlobalIdentifier _globalIdentifier) {
    var dettaglioStanziamento = new DettaglioStanziamentoCarburante(_globalIdentifier);

    setGenericAttributes(dettaglioStanziamento, _consumo);

    dettaglioStanziamento.setFuelStation(_consumo.getFuelStation());
    dettaglioStanziamento.setNavSupplierCode(_consumo.getNavSupplierCode());
    dettaglioStanziamento.setEntryDateTime(_consumo.getEntryDateTime());
    dettaglioStanziamento.setOtherSupport(_consumo.getOtherSupport());
    dettaglioStanziamento.setCounter(_consumo.getCounter());
    dettaglioStanziamento.setQuantity(_consumo.getQuantity());
    dettaglioStanziamento.setKilometers(_consumo.getKilometers());
    dettaglioStanziamento.setTicket(_consumo.getTicket());
    dettaglioStanziamento.setProductCode(_consumo.getProductCode());
    dettaglioStanziamento.setPriceReference(_consumo.getPriceReference());
    dettaglioStanziamento.setPriceComputed(_consumo.getPriceComputed());
    dettaglioStanziamento.setCostComputed(_consumo.getCostComputed());

    return dettaglioStanziamento;
  }

  protected DettaglioStanziamento toDettaglioStanziamentoGenerico(final ConsumoGenerico _consumo,
                                                                  final GlobalIdentifier _globalIdentifier) {

    var dettaglioStanziamento = new DettaglioStanziamentoGenerico(_globalIdentifier);

    setGenericAttributes(dettaglioStanziamento, _consumo);
    dettaglioStanziamento.setPartnerCode(_consumo.getPartnerCode());
    dettaglioStanziamento.setStartDate(_consumo.getStartDate());
    dettaglioStanziamento.setQuantity(_consumo.getQuantity());
    dettaglioStanziamento.setDeviceCode(_consumo.getDeviceCode());
    dettaglioStanziamento.setTipoDispositivo(_consumo.getTipoDispositivo());
    dettaglioStanziamento.setPuntoErogazione(_consumo.getPuntoErogazione());
    dettaglioStanziamento.setKm(_consumo.getKm());
    dettaglioStanziamento.setVehicle(_consumo.getVehicle());
    dettaglioStanziamento.setLicensePlateOnRecord(_consumo.getLicencePlateOnRecord());

    dettaglioStanziamento.setRoute(_consumo.getRoute());
    dettaglioStanziamento.setAdditionalInformation(_consumo.getAdditionalInformation());
    
    return dettaglioStanziamento;
  }

  private void setGenericAttributes(final DettaglioStanziamento _dettaglioStanziamento, final Consumo _consumo) {

    if (_consumo.getContract().isPresent()) {
      _dettaglioStanziamento.setContract(_consumo.getContract().get());
    }
    final Transaction transaction = _consumo.getTransaction();
    _dettaglioStanziamento.setTransaction(transaction);

    final Amount amount = _consumo.getAmount();
    if (transaction != null && "-".equals(transaction.getSign()) && amount.isPositiveOrZero()){
      amount.negate();
    }
    _dettaglioStanziamento.setAmount(amount);

    _dettaglioStanziamento.setDate(_consumo.getDate());
    _dettaglioStanziamento.setSource(_consumo.getSource());
    _dettaglioStanziamento.setInvoiceType(_consumo.getInvoiceType());
    _dettaglioStanziamento.setRegion(_consumo.getRegion());
    _dettaglioStanziamento.setArticlesGroup(_consumo.getGroupArticlesNav());
    final Supplier supplier = new Supplier(_consumo.getNavSupplierCode());
    supplier.setDocument(_consumo.getSupplierInvoiceDocument());
    _dettaglioStanziamento.setSupplier(supplier);
    _dettaglioStanziamento.setServicePartner(_consumo.getServicePartner());
    _dettaglioStanziamento.setRecordCode(_consumo.getRecordCode());

    if (_dettaglioStanziamento instanceof DeviceConsumer && _consumo instanceof DeviceSupplier) {
      setDeviceOnDettaglio((DeviceConsumer) _dettaglioStanziamento, (DeviceSupplier) _consumo);
    }
    if (_dettaglioStanziamento instanceof VehicleConsumer && _consumo instanceof VehicleSupplier) {
      Vehicle vehicle = ((VehicleSupplier) _consumo).getVehicle();
      if(vehicle!=null&&vehicle.getLicensePlate()!=null&& VehicleLicensePlate.NO_COUNTRY.equals(vehicle.getLicensePlate().getCountryId()))
        vehicle.setLicensePlate(new VehicleLicensePlate(vehicle.getLicensePlate().getLicenseId(), ""));
      ((VehicleConsumer) _dettaglioStanziamento).setVehicle(vehicle);

    }

  }

  private void setDeviceOnDettaglio(DeviceConsumer deviceConsumer, DeviceSupplier deviceSupplier) {
    Device device = deviceSupplier.getDevice();
    if (device == null && deviceSupplier instanceof SecondaryDeviceSupplier) {
      Optional<Device> secondaryDevice = ((SecondaryDeviceSupplier) deviceSupplier).getSecondaryDevice();
      if (secondaryDevice.isPresent()){
        device = secondaryDevice.get();
      }
    }
    deviceConsumer.setDevice(device);
  }

  protected void updateDescriptionTollPointGateByCaselli(TollPoint tollPointEntry, CaselliEntityRepository caselliEntityRepository) {
    if (tollPointEntry!=null && tollPointEntry.getGlobalGate()!=null) {
         GlobalGate gg = tollPointEntry.getGlobalGate();
      if (!StringUtils.isEmpty(gg.getId()) && StringUtils.isEmpty(gg.getDescription())){
        Optional<CaselliEntity> caselliEntity = caselliEntityRepository.findOneByGlobalGateIdentifier(gg.getId());
        if (caselliEntity.isPresent()) {
          gg.setDescription(caselliEntity.get().getDescription());
        }
      }
    }

  }

}
