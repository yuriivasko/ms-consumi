package it.fai.ms.consumi.repository.storico_dml.model;

import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "storico_contratto")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StoricoContratto extends DmlBaseEntity {

  private static final long serialVersionUID = 1L;

  @Size(max = 255)
  @Column(name = "contratto_numero", length = 255)
  private String contrattoNumero;

  @Size(max = 255)
  @Column(name = "codice_azienda", length = 255)
  private String codiceAzienda;

  @Size(max = 255)
  @Column(name = "stato_contratto", length = 255)
  private String statoContratto;

  @Column(name = "data_variazione")
  private Instant dataVariazione;

  @Column(name = "primario")
  private Boolean primario = true;

  public String getContrattoNumero() {
    return contrattoNumero;
  }

  public StoricoContratto contrattoNumero(String contrattoNumero) {
    this.contrattoNumero = contrattoNumero;
    return this;
  }

  public void setContrattoNumero(String contrattoNumero) {
    this.contrattoNumero = contrattoNumero;
  }

  public String getCodiceAzienda() {
    return codiceAzienda;
  }

  public StoricoContratto codiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
    return this;
  }

  public void setCodiceAzienda(String codiceAzienda) {
    this.codiceAzienda = codiceAzienda;
  }

  public String getStatoContratto() {
    return statoContratto;
  }

  public StoricoContratto statoContratto(String statoContratto) {
    this.statoContratto = statoContratto;
    return this;
  }

  public void setStatoContratto(String statoContratto) {
    this.statoContratto = statoContratto;
  }

  public Instant getDataVariazione() {
    return dataVariazione;
  }

  public StoricoContratto dataVariazione(Instant dataVariazione) {
    this.dataVariazione = dataVariazione;
    return this;
  }

  public void setDataVariazione(Instant dataVariazione) {
    this.dataVariazione = dataVariazione;
  }

  public Boolean getPrimario() {
    return primario;
  }

  public StoricoContratto primario(Boolean primario) {
    setPrimario(primario);
    return this;
  }

  public void setPrimario(Boolean primario) {
    if (primario == null) {
      throw new IllegalArgumentException("Primario field cannot be null");
    }
    this.primario = primario;
  }
}
