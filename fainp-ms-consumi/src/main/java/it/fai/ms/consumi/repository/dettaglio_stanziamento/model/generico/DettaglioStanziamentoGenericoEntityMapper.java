package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.generico;

import java.math.RoundingMode;

import javax.money.Monetary;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Supplier;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;

@Component
@Validated
public class DettaglioStanziamentoGenericoEntityMapper {

  public DettaglioStanziamentoGenericoEntityMapper() {
  }

  public DettaglioStanziamentoGenerico toDomain(@NotNull final DettaglioStanziamentoGenericoEntity _entity) {
    var domain = new DettaglioStanziamentoGenerico(new GlobalIdentifier(GlobalIdentifier.decodeType(_entity.getGlobalIdentifier())) {
      @Override
      public String getId() {
        return decodeId(_entity.getGlobalIdentifier());
      }

      @Override
      public GlobalIdentifier newGlobalIdentifier() {
        return newGlobalIdentifier();
      }
    });

    domain.setSource(new Source(_entity.getIngestionTime(), _entity.getDataAcquisizioneFlusso(), _entity.getSourceType()));


    domain.setInvoiceType(_entity.getInvoiceType());

    domain.setRecordCode(_entity.getRecordCode());
    domain.getSource()
            .setFileName(_entity.getFileName());

    domain.setCustomer(new Customer(_entity.getCustomerId()));

    domain.setContract(Contract.newUnsafeContract(_entity.getContractCode(), _entity.getCustomerId(), null));
    
    domain.setPartnerCode(_entity.getPartnerCode());
    domain.setArticlesGroup(_entity.getArticlesGroup());
    domain.setStartDate(_entity.getStartDate());
    domain.setDate(_entity.getEndDate());
    domain.setQuantity(_entity.getQuantity());

    domain.setAmount(new Amount());
    domain.getAmount()
            .setExchangeRate(_entity.getExchangeRate());
    domain.getAmount()
            .setVatRate(_entity.getAmountVatRate());
    domain.getAmount()
            .setAmountExcludedVat(Monetary.getDefaultAmountFactory()
                                          .setCurrency(_entity.getCurrencyCode())
                                          .setNumber(_entity.getAmountExcludedVat()
                                                           .setScale(5, RoundingMode.HALF_UP))
                                          .create());

    domain.setDeviceCode(_entity.getDeviceCode());
    domain.setRoute(_entity.getTratta());

    domain.setTransaction(new Transaction());
    domain.getTransaction()
            .setDetailCode(_entity.getTransactionDetailCode());
    domain.getTransaction()
            .setSign(_entity.getTransactionSign());

    domain.setLicensePlateOnRecord(_entity.getLicensePlateOnRecord());

    if (StringUtils.isNotBlank(_entity.getVehicleLicenseId()) && StringUtils.isNotBlank(_entity.getVehicleLicenseCountryId())) {
      domain.setVehicle(new Vehicle());
      domain.getVehicle().setFareClass(_entity.getVehicleFareClass());
      domain.getVehicle().setLicensePlate(new VehicleLicensePlate(_entity.getVehicleLicenseId(), _entity.getVehicleLicenseCountryId()));
      domain.getVehicle().setEuroClass(_entity.getVehicleEuroClass());
    }
    domain.setAdditionalInformation(
      _entity.getAdditionalInformation()
    );

    domain.setSupplier(new Supplier(_entity.getSupplierCode()));
//    domain.getSupplier()
//            .setCodeLegacy(_entity.getSupplierCodeLegacy());
    domain.getSupplier()
           .setDocument(_entity.getSupplierDocument());

    return domain;
  }

  public DettaglioStanziamentoGenericoEntity toEntity(@NotNull final DettaglioStanziamentoGenerico _domain) {
    var entity = new DettaglioStanziamentoGenericoEntity(_domain.getGlobalIdentifier()
                                                                                .encode());
    Source source = _domain.getSource();
    entity.setSourceType(source.getType());

    entity.setIngestionTime(_domain.getSource().getIngestionTime());
    entity.setDataAcquisizioneFlusso(_domain.getSource().getAcquisitionDate());

    entity.setInvoiceType(_domain.getInvoiceType());

    entity.setRecordCode(_domain.getRecordCode());
    entity.setContractCode(_domain.getContract()!=null ? _domain.getContract().getId():null );
    
    entity.setSourceRowNumber(_domain.getSource().getRowNumber());
    entity.setFileName(source.getFileName());

    entity.setCustomerId(_domain.getCustomer().getId());

    final Supplier supplier = _domain.getSupplier();
    entity.setSupplierCode(supplier.getCode());

    entity.setPartnerCode(_domain.getPartnerCode());
    entity.setArticlesGroup(_domain.getArticlesGroup());
    entity.setStartDate(_domain.getStartDate());
    entity.setEndDate(_domain.getDate());
    entity.setQuantity(_domain.getQuantity());

    Amount amount = _domain.getAmount();
    entity.setExchangeRate(amount.getExchangeRate());
    entity.setAmountVatRate(amount.getVatRateBigDecimal());
    entity.setAmountExcludedVat(amount.getAmountExcludedVatBigDecimal());

    entity.setDeviceCode(_domain.getDeviceCode());
    entity.setTratta(_domain.getRoute());
    
    Transaction transaction = _domain.getTransaction();
    entity.setTransactionDetailCode(transaction.getDetailCode());
    entity.setTransactionSign(transaction.getSign());

    entity.setLicensePlateOnRecord(_domain.getLicensePlateOnRecord());

    Vehicle vehicle = _domain.getVehicle();
    if (vehicle != null) {
      entity.setVehicleFareClass(vehicle.getFareClass());
      entity.setVehicleLicenseId(vehicle.getLicensePlate().getLicenseId());
      entity.setVehicleLicenseCountryId(vehicle.getLicensePlate().getCountryId());
      entity.setVehicleEuroClass(vehicle.getEuroClass());
    }
    entity.setAdditionalInformation(
      _domain.getAdditionalInformation()
    );

    entity.setSupplierDocument(supplier.getDocument());
    
    entity.setTipoDispositivo(_domain.getTipoDispositivo());
    entity.setPuntoErogazione(_domain.getPuntoErogazione());
    entity.setKm(_domain.getKm());

    return entity;
  }

}
