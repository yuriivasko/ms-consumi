package it.fai.ms.consumi.domain.consumi.stcon;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.DummyRecord;

import java.time.Instant;
import java.util.Optional;

public class Stcon extends Consumo {

  public Stcon(Source _source, Optional<GlobalIdentifier> _optionalGlobalIdentifier, CommonRecord record) {
    super(_source, "", _optionalGlobalIdentifier, record);
  }

  @Override
  public InvoiceType getInvoiceType() {
    return null;
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    return null;
  }

  @Override
  public Instant getDate() {
    return null;
  }

}
