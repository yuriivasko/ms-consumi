package it.fai.ms.consumi.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import feign.Headers;
import it.fai.ms.common.jms.fattura.canoni.EsitoCanoni;

@FeignClient(name = "navcrm", url = "${application.restclient.url}", path = "/Exage")
public interface CanoniApiClient {
  final static String REQUEST_HEADER = "Authorization";
  @PostMapping("/Api/Canoni/Dispositivi")
  @Headers({ "Content-Type: application/json", "Accept: application/json", })
  public void postCanoniDispositivi(@RequestHeader(REQUEST_HEADER) String authorizationToken,EsitoCanoni esito);

  @PostMapping("/Api/Canoni/Servizi")
  @Headers({ "Content-Type: application/json", "Accept: application/json", })
  public void postCanoniServizi(@RequestHeader(REQUEST_HEADER) String authorizationToken,EsitoCanoni esito);

}
