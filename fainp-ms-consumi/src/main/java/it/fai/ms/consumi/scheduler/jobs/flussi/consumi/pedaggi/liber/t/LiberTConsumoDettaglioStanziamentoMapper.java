package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Service
public class LiberTConsumoDettaglioStanziamentoMapper extends ConsumoDettaglioStanziamentoMapper<LiberTConsumo, DettaglioStanziamentoPedaggio> {

  @Override
  public DettaglioStanziamentoPedaggio mapConsumoToDettaglioStanziamento(@NotNull LiberTConsumo consumo) {
    return toDettaglioStanziamentoPedaggio(consumo, consumo.getGlobalIdentifier());
  }

}
