package it.fai.ms.consumi.service.jms.listener;

import java.util.List;

import javax.inject.Inject;
import javax.jms.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.JmsUtils;
import it.fai.ms.consumi.client.StanziamentiPostException;
import it.fai.ms.consumi.service.consumer.StanziamentiToNavConsumer;
import it.fai.ms.consumi.service.jms.model.StanziamentoMessage;

@Service
@Transactional(noRollbackFor=StanziamentiPostException.class, isolation=Isolation.READ_UNCOMMITTED)
@Profile("!no-jmslistener")
public class StanziamentiToNavJmsListener implements JmsQueueListener {

  private Logger _log = LoggerFactory.getLogger(getClass());

  private final StanziamentiToNavConsumer stanziamentiToNavConsumer;

  @Inject
  public StanziamentiToNavJmsListener(final StanziamentiToNavConsumer _stanziamentiToNavConsumer) {
    stanziamentiToNavConsumer = _stanziamentiToNavConsumer;
  }

  @Override
  public void onMessage(final Message _message) {
    _log.trace("Received jms message : {}", _message);
    List<StanziamentoMessage> stanziamentoMessages = null;
    stanziamentoMessages = convertJmsMessageToStanziamentoMessages(_message);
    stanziamentiToNavConsumer.consume(stanziamentoMessages);
  }

  @SuppressWarnings("unchecked")
  private static List<StanziamentoMessage> convertJmsMessageToStanziamentoMessages(final Message _message) {
    try {
      return (List<StanziamentoMessage>) JmsUtils.getMessageObject(_message);
    } catch (Exception e) {
      throw new RuntimeException("Unable to convert message to List<StanziamentoMessage>",e);
    }
  }

  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.STANZIAMENTI_TO_NAV;
  }

}
