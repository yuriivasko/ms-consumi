package it.fai.ms.consumi.service.record.carburanti;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

import javax.money.MonetaryAmount;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiJolly;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiJollyRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class TrackyCardCarbJollyRecordConsumoMapper
  extends AbstractRecordConsumoMapper
  implements RecordConsumoMapper<TrackyCardCarburantiJollyRecord, TrackyCardCarburantiJolly> {

  public static final String TRACKYCARD_JOLLY_SOURCE = "trackycard_jolly";

  TrackyCardCarbStdRecordConsumoMapper trackyCardCarbStdRecordConsumoMapper;

  public TrackyCardCarbJollyRecordConsumoMapper(TrackyCardCarbStdRecordConsumoMapper trackyCardCarbStdRecordConsumoMapper) {
    this.trackyCardCarbStdRecordConsumoMapper = trackyCardCarbStdRecordConsumoMapper;
  }

  @Override
  public TrackyCardCarburantiJolly mapRecordToConsumo(TrackyCardCarburantiJollyRecord recordJolly) throws Exception {

    //Riutilizzo il mapper standard modificando solo le proprietà necessarie ad identificare il consumo come proveniente da tracciato JOLLY
    TrackyCardCarburantiJolly consumoJolly = getTrackyCardCarburantiJolly(recordJolly);

    Customer customer = new Customer(recordJolly.getCustomerCode() != null ? recordJolly.getCustomerCode() : "");
    consumoJolly.setCustomer(customer);

    Vehicle vehicle = Vehicle.newUnsafeVehicle(
      "", //Verificare - Non disponibile nel passaggio Record -> Consumo
      recordJolly.getEuroClass(),
      recordJolly.getLicencePlate(),
      recordJolly.getCountry()
    );
    consumoJolly.setVehicle(vehicle);


    final Float vatRate = recordJolly.getVat_percentage_float();

    //    descriptor02.addItem("priceBaseVatIncluded", 94, 100); //base_unit_price_vat_included
    MonetaryAmount priceVatIncluded = recordJolly.getMonetaryPriceReferenceVatIncluded();
    if ((priceVatIncluded != null) && !priceVatIncluded.isZero()) {
      Amount priceComputed = new Amount();
      priceComputed.setVatRate(new BigDecimal(vatRate));
      priceComputed.setAmountIncludedVat(priceVatIncluded);
      priceComputed.setExchangeRate(null);// Non disponibile nel passaggio Record -> Consumo
      consumoJolly.setPriceComputed(priceComputed);
    }

    return consumoJolly;
  }

  private TrackyCardCarburantiJolly getTrackyCardCarburantiJolly(TrackyCardCarburantiJollyRecord jollyRecord) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(jollyRecord, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant ingestionTime = jollyRecord.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(jollyRecord.getIngestion_time());
    Source source = new Source(ingestionTime, acquisitionDate, TRACKYCARD_JOLLY_SOURCE);

    TrackyCardCarburantiStandard recordConsumo = trackyCardCarbStdRecordConsumoMapper.mapRecordToConsumo(jollyRecord);

    TrackyCardCarburantiJolly recordConsumoJolly = new TrackyCardCarburantiJolly(source,
                                                                                 jollyRecord.getRecordCode(),
                                                                                 Optional.of(new TrackyCardGlobalIdentifier(jollyRecord.getGlobalIdentifier())), jollyRecord);

    BeanUtils.copyProperties(recordConsumo, recordConsumoJolly);
    return recordConsumoJolly;
  }


}
