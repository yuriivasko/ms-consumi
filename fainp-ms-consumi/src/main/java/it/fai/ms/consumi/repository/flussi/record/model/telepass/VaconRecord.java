package it.fai.ms.consumi.repository.flussi.record.model.telepass;

import java.util.Optional;
import java.util.UUID;

import javax.money.MonetaryAmount;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.EuropeanNetworkCode;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import it.fai.ms.consumi.repository.flussi.record.model.MonetaryRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.NumericUtils;

public class VaconRecord extends CommonRecord implements MonetaryRecord, GlobalIdentifierBySupplier,EuropeanNetworkCode {

  private static final long serialVersionUID = -5219096913003613212L;

  private String partnerCountryCode                  = "";
  private String partnerNumber                       = "";
  private String documentNumber                      = "";
  private String documentId                          = "";
  private String invoiceType                         = "";
  private String invoiceId                           = "";
  private String tollType                            = "";
  private String tspRespCountryCode                  = "";
  private String tspRespNumber                       = "";
  private String tspExitCountryCode                  = "";
  private String tspExitNumber                       = "";
  private String transactionId                       = "";
  private String splitNumber                         = "";
  private String compensationNumber                  = "";
  private String signOfTransaction                   = "";
  private String telepassPanNumber                   = "";
  private String discountSchemeNumber                = "";
  private String entryTimestamp                      = "";
  private String entryGateCode                       = "";
  private String entryAcquisitionType                = "";
  private String exitTimestamp                       = "";
  private String exitGateCode                        = "";
  private String exitAcquisitionType                 = "";
  private String pricingClass                        = "";
  private String km                                  = "";
  private String priceType                           = "";
  private String priceCode                           = "";
  private String currencyCode                        = "";
  private String amountWithoutVAT                    = "";
  private String amountWithoutVATOfBase              = "";
  private String amountWithoutVATExcludedForDiscount = "";
  private String VATRate                             = "";
  private String amountIncludingVAT                  = "";
  private String observationCode                     = "";
  private String externalOBUIdentifier               = "";
  private String typeOfRecordingPoint                = "";
  private String flagForTransactionNotConfirmed      = "";
  private String tollCharger                         = "";
  private String tollGate                            = "";
  private String laneId                              = "";
  private String networkCode                         = "";
  private String pan                                 = "";
  private String exitTransitDateTime                 = "";
  private String transitType                         = "";
  private String sign                                = "";
  private String additionalInfo                      = "";
  private String externalCostAir                     = "";
  private String externalCostNoise                   = "";
  private String transactionAggregationNumber        = "";
  private String notConfirmedTransaction             = "";
  private String tripId                              = "";
  private String entryTime                           = "";
  private String entryDate                           = "";
  private String exitTime                            = "";
  private String exitDate                            = "";
  private String obu                                 = "";
  private String plateNumber                         = "";
  private String countryPlateNumber                  = "";
  private String vehicleClass                        = "";
  private String euroClass                           = "";
  private String totalAmount                         = "";
  private String claimId                             = "";
  private String crossingDate                        = "";
  private String crossingTime                        = "";
  private String sectionAmount                       = "";
  private String region                              = "";
  private String roadType                            = "";
  private String route                               = "";
  private String sectionGateId                       = "";
  private String rateForFeeCalculation               = "";
  private String baseAmountForFeeCalculation         = "";
  private String externalCostBelgium                 = "";
  private String serviceType                         = "";
  private String telepassCustomerID                  = "";
  private String otherID                             = "";
  private String TSPCountryCode                      = "";
  private String TSPNr                               = "";
  private String CCPNr                               = "";
  private String invoiceLineNr                       = "";
  private String descriptionText                     = "";
  private String signOfQuantity                      = "";
  private String quantity                            = "";
  private String signOfBaseWithoutVAT                = "";
  private String baseWithoutVAT                      = "";
  private String rateSign                            = "";
  private String rate                                = "";
  private String signOfAmountWithoutVAT              = "";
  private String descriptionTextDetail               = "";
  private String signOfAmountIncludingVAT            = "";

  private String externalCost = "";
  private String trafficClassification = "";
  private String weightCategoryDescription = "";

  private MonetaryAmount amount_vat                     = null;
  private MonetaryAmount amount_novat                   = null;
  private MonetaryAmount amount_novat_base              = null;
  private MonetaryAmount amount_novat_base_discount     = null;
  private MonetaryAmount amount_novat_excluded_discount = null;
  
  private TipoServizioEnum service_type                 = null;

  @JsonCreator
  public VaconRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }
  
  @Override
  public boolean toBeSkipped() {
	  // FAISD-86  : skippare righe che inziano per 13 e alla linea 101 hanno E
    return getRecordCode().equals("13") && getTransitType().startsWith("E");
  }

  public String getExternalCostBelgium() {
    return externalCostBelgium;
  }

  public void setExternalCostBelgium(String externalCostBelgium) {
    this.externalCostBelgium = externalCostBelgium;
  }

  public String getBaseAmountForFeeCalculation() {
    return baseAmountForFeeCalculation;
  }

  public void setBaseAmountForFeeCalculation(String baseAmountForFeeCalculation) {
    this.baseAmountForFeeCalculation = baseAmountForFeeCalculation;
  }

  public String getRateForFeeCalculation() {
    return rateForFeeCalculation;
  }

  public void setRateForFeeCalculation(String rateForFeeCalculation) {
    this.rateForFeeCalculation = rateForFeeCalculation;
  }

  public String getFlagForTransactionNotConfirmed() {
    return flagForTransactionNotConfirmed;
  }

  public void setFlagForTransactionNotConfirmed(String flagForTransactionNotConfirmed) {
    this.flagForTransactionNotConfirmed = flagForTransactionNotConfirmed;
  }

  public String getServiceType() {
    return serviceType;
  }

  public void setServiceType(String serviceType) {
    this.serviceType = serviceType;
  }

  public String getTelepassCustomerID() {
    return telepassCustomerID;
  }

  public void setTelepassCustomerID(String telepassCustomerID) {
    this.telepassCustomerID = telepassCustomerID;
  }

  public String getOtherID() {
    return otherID;
  }

  public void setOtherID(String otherID) {
    this.otherID = otherID;
  }

  public String getTSPCountryCode() {
    return TSPCountryCode;
  }

  public void setTSPCountryCode(String tSPCountryCode) {
    TSPCountryCode = tSPCountryCode;
  }

  public String getTSPNr() {
    return TSPNr;
  }

  public void setTSPNr(String tSPNr) {
    TSPNr = tSPNr;
  }

  public String getCCPNr() {
    return CCPNr;
  }

  public void setCCPNr(String cCPNr) {
    CCPNr = cCPNr;
  }

  public String getInvoiceLineNr() {
    return invoiceLineNr;
  }

  public void setInvoiceLineNr(String invoiceLineNr) {
    this.invoiceLineNr = invoiceLineNr;
  }

  public String getDescriptionText() {
    return descriptionText;
  }

  public void setDescriptionText(String descriptionText) {
    this.descriptionText = descriptionText;
  }

  public String getSignOfQuantity() {
    return signOfQuantity;
  }

  public void setSignOfQuantity(String signOfQuantity) {
    this.signOfQuantity = signOfQuantity;
  }

  public String getQuantity() {
    return quantity;
  }

  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  public String getSignOfBaseWithoutVAT() {
    return signOfBaseWithoutVAT;
  }

  public void setSignOfBaseWithoutVAT(String signOfBaseWithoutVAT) {
    this.signOfBaseWithoutVAT = signOfBaseWithoutVAT;
  }

  public String getBaseWithoutVAT() {
    return baseWithoutVAT;
  }

  public void setBaseWithoutVAT(String value) {
    this.baseWithoutVAT = value;
    this.amount_novat_base = MonetaryUtils.strToMonetaryIsoNumeric(value, -2, currencyCode);
  }

  public String getRateSign() {
    return rateSign;
  }

  public void setRateSign(String rateSign) {
    this.rateSign = rateSign;
  }

  public String getRate() {
    return rate;
  }

  public void setRate(String rate) {
    this.rate = rate;
  }

  public String getSignOfAmountWithoutVAT() {
    return signOfAmountWithoutVAT;
  }

  public void setSignOfAmountWithoutVAT(String signOfAmountWithoutVAT) {
    this.signOfAmountWithoutVAT = signOfAmountWithoutVAT;
  }

  public String getDescriptionTextDetail() {
    return descriptionTextDetail;
  }

  public void setDescriptionTextDetail(String descriptionTextDetail) {
    this.descriptionTextDetail = descriptionTextDetail;
  }

  public String getSignOfAmountIncludingVAT() {
    return signOfAmountIncludingVAT;
  }

  public void setSignOfAmountIncludingVAT(String value) {
    this.signOfAmountIncludingVAT = value;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getRoadType() {
    return roadType;
  }

  public void setRoadType(String roadType) {
    this.roadType = roadType;
  }

  public String getRoute() {
    return route;
  }

  public void setRoute(String route) {
    this.route = route;
  }

  public String getSectionAmount() {
    return sectionAmount;
  }

  public void setSectionAmount(String sectionAmount) {
    this.sectionAmount = sectionAmount;
  }

  public String getSectionGateId() {
    return sectionGateId;
  }

  public void setSectionGateId(String sectionGateId) {
    this.sectionGateId = sectionGateId;
  }

  public String getClaimId() {
    return claimId;
  }

  public void setClaimId(String claimId) {
    this.claimId = claimId;
  }

  public String getCrossingDate() {
    return crossingDate;
  }

  public void setCrossingDate(String crossingDate) {
    this.crossingDate = crossingDate;
  }

  public String getCrossingTime() {
    return crossingTime;
  }

  public void setCrossingTime(String crossingTime) {
    this.crossingTime = crossingTime;
  }

  public String getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(String totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getEuroClass() {
    return euroClass;
  }

  public void setEuroClass(String euroClass) {
    this.euroClass = euroClass;
  }

  public String getVehicleClass() {
    return vehicleClass;
  }

  public void setVehicleClass(String vehicleClass) {
    this.vehicleClass = vehicleClass;
  }

  public String getCountryPlateNumber() {
    return countryPlateNumber;
  }

  public void setCountryPlateNumber(String countryPlateNumber) {
    this.countryPlateNumber = countryPlateNumber;
  }

  public String getPlateNumber() {
    return plateNumber;
  }

  public void setPlateNumber(String plateNumber) {
    this.plateNumber = plateNumber;
  }

  public String getObu() {
    return obu;
  }

  public void setObu(String obu) {
    this.obu = obu;
  }

  public String getEntryTime() {
    return entryTime;
  }

  public void setEntryTime(String entryTime) {
    this.entryTime = entryTime;
  }

  public String getEntryDate() {
    return entryDate;
  }

  public void setEntryDate(String entryDate) {
    this.entryDate = entryDate;
  }

  public String getExitTime() {
    return exitTime;
  }

  public void setExitTime(String exitTime) {
    this.exitTime = exitTime;
  }

  public String getExitDate() {
    return exitDate;
  }

  public void setExitDate(String exitDate) {
    this.exitDate = exitDate;
  }

  public String getTripId() {
    return tripId;
  }

  public void setTripId(String tripId) {
    this.tripId = tripId;
  }

  public String getExternalCostAir() {
    return externalCostAir;
  }

  public void setExternalCostAir(String externalCostAir) {
    this.externalCostAir = externalCostAir;
  }

  public String getExternalCostNoise() {
    return externalCostNoise;
  }

  public void setExternalCostNoise(String externalCostNoise) {
    this.externalCostNoise = externalCostNoise;
  }

  public String getAdditionalInfo() {
    return additionalInfo;
  }

  public void setAdditionalInfo(String additionalInfo) {
    this.additionalInfo = additionalInfo;
  }

  public String getSign() {
    return sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getTransitType() {
    return transitType;
  }

  public void setTransitType(String transitType) {
    this.transitType = transitType;
  }

  public String getExitTransitDateTime() {
    return exitTransitDateTime;
  }

  public void setExitTransitDateTime(String exitTransitDateTime) {
    this.exitTransitDateTime = exitTransitDateTime;
  }

  public String getPan() {
    return pan;
  }

  public void setPan(String pan) {
    this.pan = pan;
  }

  public String getNetworkCode() {
    return networkCode;
  }

  public void setNetworkCode(String networkCode) {
    this.networkCode = networkCode;
  }

  public String getLaneId() {
    return laneId;
  }

  public void setLaneId(String laneId) {
    this.laneId = laneId;
  }

  public String getTollGate() {
    return tollGate;
  }

  public void setTollGate(String tollGate) {
    this.tollGate = tollGate;
  }

  public String getTollCharger() {
    return tollCharger;
  }

  public void setTollCharger(String tollCharger) {
    this.tollCharger = tollCharger;
  }

  public String getNotConfirmedTransaction() {
    return notConfirmedTransaction;
  }

  public void setNotConfirmedTransaction(String notConfirmedTransaction) {
    this.notConfirmedTransaction = notConfirmedTransaction;
  }

  public String getObservationCode() {
    return observationCode;
  }

  public void setObservationCode(String observationCode) {
    this.observationCode = observationCode;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }

  public String getDocumentId() {
    return documentId;
  }

  public void setDocumentId(String documentId) {
    this.documentId = documentId;
  }

  public String getInvoiceType() {
    return invoiceType;
  }

  public void setInvoiceType(String invoiceType) {
    this.invoiceType = invoiceType;
  }

  public String getInvoiceId() {
    return invoiceId;
  }

  public void setInvoiceId(String invoiceId) {
    this.invoiceId = invoiceId;
  }

  public String getAmountWithoutVATExcludedForDiscount() {
    return amountWithoutVATExcludedForDiscount;
  }

  public void setAmountWithoutVATExcludedForDiscount(String amountWithoutVATExcludedForDiscount) {
    this.amountWithoutVATExcludedForDiscount = amountWithoutVATExcludedForDiscount;
    this.amount_novat_excluded_discount = MonetaryUtils.strToMonetaryIsoNumeric(amountWithoutVATExcludedForDiscount, -2, currencyCode);
  }

  public String getTollType() {
    return tollType;
  }

  public void setTollType(String tollType) {
    this.tollType = tollType;
  }

  public String getPartnerCountryCode() {
    return partnerCountryCode;
  }

  public void setPartnerCountryCode(String partnerCountryCode) {
    this.partnerCountryCode = partnerCountryCode;
  }

  public String getPartnerNumber() {
    return partnerNumber;
  }

  public void setPartnerNumber(String partnerNumber) {
    this.partnerNumber = partnerNumber;
  }

  public String getTspRespCountryCode() {
    return tspRespCountryCode;
  }

  public void setTspRespCountryCode(String tspRespCountryCode) {
    this.tspRespCountryCode = tspRespCountryCode;
  }

  public String getTspRespNumber() {
    return tspRespNumber;
  }

  public void setTspRespNumber(String tspRespNumber) {
    this.tspRespNumber = tspRespNumber;
  }

  public String getTspExitCountryCode() {
    return tspExitCountryCode;
  }

  public void setTspExitCountryCode(String tspExitCountryCode) {
    this.tspExitCountryCode = tspExitCountryCode;
  }

  public String getTspExitNumber() {
    return tspExitNumber;
  }

  public void setTspExitNumber(String tspExitNumber) {
    this.tspExitNumber = tspExitNumber;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public String getSplitNumber() {
    return splitNumber;
  }

  public void setSplitNumber(String splitNumber) {
    this.splitNumber = splitNumber;
  }

  public String getCompensationNumber() {
    return compensationNumber;
  }

  public void setCompensationNumber(String compensationNumber) {
    this.compensationNumber = compensationNumber;
  }

  public String getSignOfTransaction() {
    return signOfTransaction;
  }

  public void setSignOfTransaction(String signOfTransaction) {
    this.signOfTransaction = signOfTransaction;
  }

  public String getTelepassPanNumber() {
    return telepassPanNumber;
  }

  public void setTelepassPanNumber(String telepassPANNumber) {
    this.telepassPanNumber = telepassPANNumber;
  }

  public String getDiscountSchemeNumber() {
    return discountSchemeNumber;
  }

  public void setDiscountSchemeNumber(String discountSchemeNumber) {
    this.discountSchemeNumber = discountSchemeNumber;
  }

  public String getEntryGateCode() {
    return entryGateCode;
  }

  public void setEntryGateCode(String entryGateCode) {
    this.entryGateCode = entryGateCode;
  }

  public String getEntryAcquisitionType() {
    return entryAcquisitionType;
  }

  public void setEntryAcquisitionType(String entryAcquisitionType) {
    this.entryAcquisitionType = entryAcquisitionType;
  }

  public String getExitGateCode() {
    return exitGateCode;
  }

  public void setExitGateCode(String exitGateCode) {
    this.exitGateCode = exitGateCode;
  }

  public String getExitAcquisitionType() {
    return exitAcquisitionType;
  }

  public void setExitAcquisitionType(String exitAcquisitionType) {
    this.exitAcquisitionType = exitAcquisitionType;
  }

  public String getPricingClass() {
    return pricingClass;
  }

  public void setPricingClass(String pricingClass) {
    this.pricingClass = pricingClass;
  }

  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }

  public String getPriceType() {
    return priceType;
  }

  public void setPriceType(String priceType) {
    this.priceType = priceType;
  }

  public String getPriceCode() {
    return priceCode;
  }

  public void setPriceCode(String priceCode) {
    this.priceCode = priceCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getAmountWithoutVAT() {
    return amountWithoutVAT;
  }

  public void setAmountWithoutVAT(String value) {
    this.amountWithoutVAT = value;
    this.amount_novat = MonetaryUtils.strToMonetaryIsoNumeric(value, -2, currencyCode);
  }
  public void setAmountWithoutVAT(String value, int scale, int postProcessScale) {
    this.amountWithoutVAT = value;
    this.amount_novat = MonetaryUtils.strToMonetaryIsoNumeric(value, scale, currencyCode,postProcessScale);
  }

  public String getVATRate() {
    return VATRate;
  }

  public void setVATRate(String value) {
    VATRate = value;
    this.vatRateBigDecimal = NumericUtils.strToBigDecimal(value,-2);
  }

  public String getAmountIncludingVAT() {
    return amountIncludingVAT;
  }

  public void setAmountIncludingVAT(String value) {
    this.amountIncludingVAT = value;
    this.amount_vat = MonetaryUtils.strToMonetaryIsoNumeric(value, -2, currencyCode);
  }
  public void setAmountIncludingVAT(String value,int scale, int scalePostProcess) {
    this.amountIncludingVAT = value;
    this.amount_vat = MonetaryUtils.strToMonetaryIsoNumeric(value, scale, currencyCode,scalePostProcess);
  }

  @Override
  public MonetaryAmount getAmount_vat() {
    return amount_vat;
  }

  public String getExternalOBUIdentifier() {
    return externalOBUIdentifier;
  }

  public void setExternalOBUIdentifier(String externalOBUIdentifier) {
    this.externalOBUIdentifier = externalOBUIdentifier;
  }

  public String getTypeOfRecordingPoint() {
    return typeOfRecordingPoint;
  }

  public void setTypeOfRecordingPoint(String typeOfRecordingPoint) {
    this.typeOfRecordingPoint = typeOfRecordingPoint;
  }

  public String getAmountWithoutVATOfBase() {
    return amountWithoutVATOfBase;
  }

  public void setAmountWithoutVATOfBase(String amountWithoutVATOfBase) {
    this.amountWithoutVATOfBase = amountWithoutVATOfBase;
    this.amount_novat_base_discount = MonetaryUtils.strToMonetaryIsoNumeric(amountWithoutVATOfBase, -2, currencyCode);
  }

  public String getTransactionAggregationNumber() {
    return transactionAggregationNumber;
  }

  public void setTransactionAggregationNumber(String transactionAggregationNumber) {
    this.transactionAggregationNumber = transactionAggregationNumber;
  }

  public String getEntryTimestamp() {
    return entryTimestamp;
  }

  public void setEntryTimestamp(String entryTimestamp) {
    this.entryTimestamp = entryTimestamp;
  }

  public String getExitTimestamp() {
    return exitTimestamp;
  }

  public void setExitTimestamp(String exitTimestamp) {
    this.exitTimestamp = exitTimestamp;
  }

  public String getExternalCost() {
    return externalCost;
  }

  public void setExternalCost(String externalCost) {
    this.externalCost = externalCost;
  }

  public String getTrafficClassification() {
    return trafficClassification;
  }

  public void setTrafficClassification(String trafficClassification) {
    this.trafficClassification = trafficClassification;
  }

  public String getWeightCategoryDescription() {
    return weightCategoryDescription;
  }

  public void setWeightCategoryDescription(String weightCategoryDescription) {
    this.weightCategoryDescription = weightCategoryDescription;
  }

  @Override
  public MonetaryAmount getAmount_novat() {
    return amount_novat;
  }

  public MonetaryAmount getAmount_novat_base() {
    return amount_novat_base;
  }

  @Override
  public MonetaryAmount getAmount_novat_base_discount() {
    return amount_novat_base_discount;
  }

  @Override
  public MonetaryAmount getAmount_novat_excluded_discount() {
    return amount_novat_excluded_discount;
  }
  

  @Override
  public String getGlobalIdentifier() {
    String gi = null;
    switch (Optional.ofNullable(getRecordCode())
                    .orElse("na")) {
    case "07":
      gi = String.join("§", getTspRespCountryCode(), getTspRespNumber(), getTspExitCountryCode(), getTspExitNumber(), getTransactionId(),
                       getSplitNumber(), getCompensationNumber(), getSignOfTransaction()).replaceAll("null", "").replaceAll("null", "");
      break;

    case "09":
    case "11":
      gi = String.join("§", getTspRespCountryCode(), getTspRespNumber(), getTspExitCountryCode(), getTspExitNumber(), getTransactionId(),
                       getSignOfTransaction()).replaceAll("null", "");
      break;

    case "12":
      gi = String.join("§", getTspRespCountryCode(), getTspRespNumber(), getTspExitCountryCode(), getTspExitNumber(), getTransactionId()).replaceAll("null", "");
      break;

    case "13":
    case "14":
      gi = String.join("§", getTollCharger(), getTollGate(), getLaneId(), getNetworkCode(), getPan(), getExitTransitDateTime(), " ",
                       getTransitType(), getSign(), getAdditionalInfo()).replaceAll("null", "").replaceAll("null", "");
      break;

    case "17":
      gi = String.join("§", getObu(), getRegion(), getRoadType(), getRoute(), getEntryTime(), getEntryDate()).replaceAll("null", "");
      break;

    case "15":
      gi = getTripId();
      break;

    case "20":
      gi = UUID.randomUUID()
               .toString();
      break;

    default:
      break;
    }
    return gi;
  }

  public TipoServizioEnum getService_type() {
    return service_type;
  }

  public void setService_type(TipoServizioEnum service_type) {
    this.service_type = service_type;
  }
  
  
  @Override
  public String getEuropeanNetworkCode() {
    String recordCode = getRecordCode();
    if (recordCode != null) {
      switch (recordCode) {
      case "07":
      case "09":
      case "11":
      case "12":
        return StringUtils.defaultString(getTspRespCountryCode())+StringUtils.defaultString(getTspRespNumber());
      case "13":
      case "14":
        return StringUtils.defaultString(getTollCharger());
      default:
        return null;
      }
    }
    return null;
  }


}
