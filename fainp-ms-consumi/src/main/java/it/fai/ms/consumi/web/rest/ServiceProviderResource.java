package it.fai.ms.consumi.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.web.rest.errors.BadRequestAlertException;
import it.fai.ms.consumi.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ServiceProvider.
 */
@RestController
@RequestMapping("/api")
public class ServiceProviderResource {

  private final Logger log = LoggerFactory.getLogger(ServiceProviderResource.class);

  private static final String ENTITY_NAME = "serviceProvider";

  private final ServiceProviderRepository serviceProviderRepository;

  public ServiceProviderResource(ServiceProviderRepository serviceProviderRepository) {
    this.serviceProviderRepository = serviceProviderRepository;
  }

  /**
   * POST /service-providers : Create a new serviceProvider.
   *
   * @param serviceProvider
   *          the serviceProvider to create
   * @return the ResponseEntity with status 201 (Created) and with body the new serviceProvider, or with status 400 (Bad
   *         Request) if the serviceProvider has already an ID
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PostMapping("/service-providers")
  public ResponseEntity<ServiceProvider> createServiceProvider(@Valid @RequestBody ServiceProvider serviceProvider) throws URISyntaxException {
    log.debug("REST request to save ServiceProvider : {}", serviceProvider);
    if (serviceProvider.getId() != null) {
      throw new BadRequestAlertException("A new serviceProvider cannot already have an ID", ENTITY_NAME, "idexists");
    }
    ServiceProvider result = serviceProviderRepository.save(serviceProvider);
    return ResponseEntity.created(new URI("/api/service-providers/" + result.getId()))
                         .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()
                                                                                          .toString()))
                         .body(result);
  }

  /**
   * PUT /service-providers : Updates an existing serviceProvider.
   *
   * @param serviceProvider
   *          the serviceProvider to update
   * @return the ResponseEntity with status 200 (OK) and with body the updated serviceProvider, or with status 400 (Bad
   *         Request) if the serviceProvider is not valid, or with status 500 (Internal Server Error) if the
   *         serviceProvider couldn't be updated
   * @throws URISyntaxException
   *           if the Location URI syntax is incorrect
   */
  @PutMapping("/service-providers")
  public ResponseEntity<ServiceProvider> updateServiceProvider(@Valid @RequestBody ServiceProvider serviceProvider) throws URISyntaxException {
    log.debug("REST request to update ServiceProvider : {}", serviceProvider);
    if (serviceProvider.getId() == null) {
      throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
    }
    ServiceProvider result = serviceProviderRepository.save(serviceProvider);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, serviceProvider.getId()
                                                                                                 .toString()))
                         .body(result);
  }

  /**
   * GET /service-providers : get all the serviceProviders.
   *
   * @return the ResponseEntity with status 200 (OK) and the list of serviceProviders in body
   */
  @GetMapping("/service-providers")
  public List<ServiceProvider> getAllServiceProviders() {
    log.debug("REST request to get all ServiceProviders");
    return serviceProviderRepository.findAll();
  }

  /**
   * GET /service-providers/:id : get the "id" serviceProvider.
   *
   * @param id
   *          the id of the serviceProvider to retrieve
   * @return the ResponseEntity with status 200 (OK) and with body the serviceProvider, or with status 404 (Not Found)
   */
  @GetMapping("/service-providers/{id}")
  public ResponseEntity<ServiceProvider> getServiceProvider(@PathVariable Long id) {
    log.debug("REST request to get ServiceProvider : {}", id);
    Optional<ServiceProvider> serviceProvider = serviceProviderRepository.findById(id);
    return ResponseUtil.wrapOrNotFound(serviceProvider);
  }

  /**
   * DELETE /service-providers/:id : delete the "id" serviceProvider.
   *
   * @param id
   *          the id of the serviceProvider to delete
   * @return the ResponseEntity with status 200 (OK)
   */
  @DeleteMapping("/service-providers/{id}")
  public ResponseEntity<Void> deleteServiceProvider(@PathVariable Long id) {
    log.debug("REST request to delete ServiceProvider : {}", id);

    serviceProviderRepository.deleteById(id);
    return ResponseEntity.ok()
                         .headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
                         .build();
  }
}
