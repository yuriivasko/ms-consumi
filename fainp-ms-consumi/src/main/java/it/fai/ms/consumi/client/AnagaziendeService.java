package it.fai.ms.consumi.client;

import java.util.List;

import it.fai.ms.consumi.dto.ArticoloDTO;
import it.fai.ms.consumi.dto.FornitoreDTO;

public interface AnagaziendeService {

  List<ArticoloDTO> getAllArticoli();

  List<FornitoreDTO> getAllFornitori();
}
