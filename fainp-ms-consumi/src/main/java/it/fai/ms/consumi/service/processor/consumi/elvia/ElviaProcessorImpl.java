package it.fai.ms.consumi.service.processor.consumi.elvia;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.ESE_TOT_STATUS;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.Elvia;
import it.fai.ms.consumi.domain.consumi.pedaggi.telepass.ElviaGlobalIdentifier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class ElviaProcessorImpl extends ConsumoAbstractProcessor implements ElviaProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final StanziamentiProducer                            stanziamentiProducer;
  private final ElviaConsumoDettaglioStanziamentoPedaggioMapper mapper;
  
  @Autowired
  private PlatformTransactionManager  transactionManager;
  
  private static final TransactionDefinition REQUIRES_NEW_TRANSACTION = new DefaultTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRES_NEW);


  
  static Object lockValidationPhase = new Object();
  static Object lockStanziamentoPhase = new Object();

  @Inject
  public ElviaProcessorImpl(final ConsumoValidatorService _consumoValidatorService,
                            final StanziamentiProducer _stanziamentiProducer,
                            final ElviaConsumoDettaglioStanziamentoPedaggioMapper _mapper,
                            final VehicleService _vehicleService,
                            final CustomerService _customerService) {

    super(_consumoValidatorService, _vehicleService, _customerService);
    stanziamentiProducer = _stanziamentiProducer;
    mapper = _mapper;
  }

  @Override
  public ProcessingResult validateAndProcess(final Elvia _elvia, final Bucket _bucket) {
    return executeProcess(_elvia, _bucket);
  }

  private ProcessingResult executeProcess(final Elvia _consumo, final Bucket _bucket) {

    _log.debug(">");
    _log.debug("> processing start {}", _bucket.getFileName());
    _log.debug("  --> {}", _consumo);

    _log.debug("maybe _consumo need to be duplicated to generate exemption ");

    //duplico eventuali DettaglioStanziamento se esente IVA (caso ELCIT / ELVIA)
    final List<Stanziamento> resultingStanziamenti = new ArrayList<>();
    
    ElviaWithOptionalExemption elviaWithOptionalExemption = null;

    if (_bucket.isRUNNING_IN_PARALLEL()) {
      synchronized (lockValidationPhase) {
    	if(_log.isDebugEnabled()) _log.debug("START Async on lockValidationPhase: RowNumber {}",_consumo!=null&&_consumo.getSource()!=null?_consumo.getSource().getRowNumber():"n/a");
        elviaWithOptionalExemption = manageExempitonRecordsIfNeeded(_consumo);
        if(_log.isDebugEnabled()) _log.debug("STOP Async on lockValidationPhase: RowNumber {}",_consumo!=null&&_consumo.getSource()!=null?_consumo.getSource().getRowNumber():"n/a");
      }
      final ElviaWithOptionalExemption elviaWithOptionalExemptionFinal = elviaWithOptionalExemption;
      synchronized (lockStanziamentoPhase) {
    	  new TransactionTemplate(transactionManager, REQUIRES_NEW_TRANSACTION).execute(new TransactionCallback<String>() {
    	        @Override
    	        public String doInTransaction(TransactionStatus status) {
    	          if(_log.isDebugEnabled()) _log.debug("START Async on lockStanziamentoPhase: RowNumber {}",_consumo != null && _consumo.getSource() != null ? _consumo.getSource().getRowNumber() : "n/a");
    			  _elaborateConsumo(elviaWithOptionalExemptionFinal,resultingStanziamenti);
    			  if(_log.isDebugEnabled()) _log.debug("STOP Async on lockStanziamentoPhase: RowNumber {}",_consumo != null && _consumo.getSource() != null ? _consumo.getSource().getRowNumber() : "n/a");
    			  return "true";
    	        }
    	      });
    	  elviaWithOptionalExemption = elviaWithOptionalExemptionFinal;
      }
    } else {
      elviaWithOptionalExemption = manageExempitonRecordsIfNeeded(_consumo);
	  _elaborateConsumo(elviaWithOptionalExemption,resultingStanziamenti);
    }

    var processingResult = elviaWithOptionalExemption.getProcessingResult();
    processingResult.getStanziamenti().addAll(resultingStanziamenti);

    _log.debug("  --> {}", processingResult);
    _log.debug("< processing completed {}", _consumo.getSource());
    _log.debug("<");

    return processingResult;
  }
  
  private void _elaborateConsumo(ElviaWithOptionalExemption elviaWithOptionalExemption,List<Stanziamento> resultingStanziamenti){
    if (elviaWithOptionalExemption.totDettaglioStanziamentoCanBeProcessed()) {
      final Optional<StanziamentiParams> optionalStanziamentiParams = elviaWithOptionalExemption.getTotProcessingResult().getStanziamentiParams();
      final Consumo totConsumo = elviaWithOptionalExemption.getTotConsumo();
      elaborateConsumo(totConsumo, optionalStanziamentiParams, resultingStanziamenti);
    } else {
      _log.warn(" - {} can't be processed", elviaWithOptionalExemption.getTotConsumo());
    }

    if (elviaWithOptionalExemption.isEseConsumo()) {
      if (elviaWithOptionalExemption.eseDettaglioStanziamentoCanBeProcessed()){
        final Optional<StanziamentiParams> optionalStanziamentiParams = elviaWithOptionalExemption.getEseProcessingResult().getStanziamentiParams();
        final Elvia eseConsumo = elviaWithOptionalExemption.getEseConsumo();
        elaborateConsumo(eseConsumo, optionalStanziamentiParams, resultingStanziamenti);
      } else {
        _log.warn(" - {} can't be processed", elviaWithOptionalExemption.getEseConsumo());
      }
    }
  }

  private ElviaWithOptionalExemption manageExempitonRecordsIfNeeded(Elvia consumo) {
    ElviaWithOptionalExemption elviaWithOptionalExemption =  new ElviaWithOptionalExemption(consumo);
    var processingResult = validateAndNotify(elviaWithOptionalExemption.getTotConsumo());
    elviaWithOptionalExemption.setTotProcessingResult(processingResult);

    if (elviaWithOptionalExemption.isEseConsumo()) {
      var eseProcessingResult = validateAndNotify(elviaWithOptionalExemption.getEseConsumo());
      elviaWithOptionalExemption.setEseProcessingResult(eseProcessingResult);
    }

    return elviaWithOptionalExemption;
  }

  private void elaborateConsumo(Consumo consumo, Optional<StanziamentiParams> optionalStanziamentiParams, List<Stanziamento> resultStanziamenti) {
    _log.debug(" - {} processing validation completed", consumo);
    optionalStanziamentiParams.ifPresentOrElse(stanziamentiParams -> {
      _log.trace("Found StanziamentiParams for this consumo {}! Going on with process...", consumo);
      final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
      final var stanziamenti = stanziamentiProducer.produce(dettaglioStanziamento, stanziamentiParams);
      resultStanziamenti.addAll(stanziamenti);
    }, () -> {
      _log.warn("empty StanziamentiParams for this consumo {}! Cannot process!", consumo);
    });
  }

  private DettaglioStanziamento mapConsumoToDettaglioStanziamento(final Consumo _consumo) {
    return mapConsumoToDettaglioStanziamento(_consumo, mapper);
  }

  private class ElviaWithOptionalExemption {

    private Elvia elviaOriginal;

    private Elvia elviaTOT;
    private Elvia elviaESE;

    private ESE_TOT_STATUS eseTotStatus;
    private ProcessingResult totProcessingResult;
    private ProcessingResult eseProcessingResult;

    public ElviaWithOptionalExemption(Elvia elviaOriginal) {
      this.elviaOriginal = elviaOriginal;
      this.eseTotStatus = elviaOriginal.getEseTotStatus();
      if (isEseConsumo()){
        //necessario per gestione getAmount
        elviaOriginal.setEseTotStatus(ESE_TOT_STATUS.TOT);
        elviaTOT = new Elvia(elviaOriginal.getSource(), elviaOriginal.getRecordCode(), Optional.of(elviaOriginal.getGlobalIdentifier()), elviaOriginal.getRecord());
        BeanUtils.copyProperties(elviaOriginal, elviaTOT);

        elviaOriginal.setEseTotStatus(ESE_TOT_STATUS.ESE);
        elviaESE = new Elvia(elviaOriginal.getSource(), elviaOriginal.getRecordCode(), Optional.of(new ElviaGlobalIdentifier(elviaOriginal.getEseGlobalIdentifier())), elviaOriginal.getRecord());
        BeanUtils.copyProperties(elviaOriginal, elviaESE);

      } else {
        elviaTOT = elviaOriginal;
      }
    }

    private Elvia getTotConsumo(){
//      if (elviaTOT == null){
//        //necessario per gestione getAmount
//        elviaOriginal.setEseTotStatus(ESE_TOT_STATUS.TOT);
//
//        elviaTOT = new Elvia(elviaOriginal.getSource(), elviaOriginal.getRecordCode(), Optional.of(elviaOriginal.getGlobalIdentifier()));
//        BeanUtils.copyProperties(elviaOriginal, elviaTOT);
//
//        elviaESE = new Elvia(elviaOriginal.getSource(), elviaOriginal.getRecordCode(), Optional.of(new ElviaGlobalIdentifier(elviaOriginal.getEseGlobalIdentifier())));
//        elviaESE.setEseTotStatus(ESE_TOT_STATUS.ESE);
//
////        //ripristino lo stato originale per gestione getAmount
////        elviaOriginal.setEseTotStatus(eseTotStatus);
//      }
      return elviaTOT;
    }

    private Elvia getEseConsumo(){
      return elviaESE;
    }

    public boolean isEseConsumo() {
      return eseTotStatus == ESE_TOT_STATUS.ESE;
    }

    public void setTotProcessingResult(ProcessingResult totProcessingResult) {
      this.totProcessingResult = totProcessingResult;
    }

    public ProcessingResult getTotProcessingResult() {
      return totProcessingResult;
    }

    public void setEseProcessingResult(ProcessingResult eseProcessingResult) {
      this.eseProcessingResult = eseProcessingResult;
    }

    public ProcessingResult getEseProcessingResult() {
      return eseProcessingResult;
    }

    public boolean totDettaglioStanziamentoCanBeProcessed() {
      return totProcessingResult != null  && totProcessingResult.dettaglioStanziamentoCanBeProcessed() && (eseProcessingResult == null || eseProcessingResult.dettaglioStanziamentoCanBeProcessed());
    }

    public ProcessingResult getProcessingResult() {
      return isEseConsumo() ? eseProcessingResult : totProcessingResult;
    }

    public boolean eseDettaglioStanziamentoCanBeProcessed() {
      return totProcessingResult != null  && totProcessingResult.dettaglioStanziamentoCanBeProcessed() && eseProcessingResult != null && eseProcessingResult.dettaglioStanziamentoCanBeProcessed();
    }
  }
}
