package it.fai.ms.consumi.service.processor;

import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.ContractSupplier;
import it.fai.ms.consumi.domain.consumi.DeviceSupplier;
import it.fai.ms.consumi.domain.consumi.SecondaryDeviceSupplier;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;
import java.util.Optional;

import static it.fai.ms.consumi.domain.Vehicle.newEmptyVehicleWithFareClass;

public abstract class ConsumoAbstractProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  protected final ConsumoValidatorService consumoValidatorService;
  protected final CustomerService         customerService;
  protected final VehicleService          vehicleService;

  public ConsumoAbstractProcessor(final ConsumoValidatorService _consumoValidatorService,
                                  final VehicleService _vehicleService,
                                  final CustomerService _customerService) {
    consumoValidatorService = _consumoValidatorService;
    vehicleService = _vehicleService;
    customerService = _customerService;
  }

//  private Optional<Customer> findCustomerByDevice(final DeviceSupplier _deviceSupplier) {
//    if (_deviceSupplier.getDevice() == null || StringUtils.isBlank(_deviceSupplier.getDevice().getId()) || _deviceSupplier.getDevice().getType() == null){
//      _log.warn("Cannot find customer! Current DeviceSupplier object not have Device, or Device not have Id or Type! _deviceSupplier:=[{}]", _deviceSupplier);
//      return Optional.empty();
//    }
//
//    return customerService.findCustomerByDeviceSeriale(_deviceSupplier.getDevice()
//                                                                 .getId(),_deviceSupplier.getDevice()
//                                                                 .getType()
//                                                                 );
//  }
//
//  private Optional<Customer> findCustomerByContract(final ContractSupplier _contractSupplier) {
//    if (_contractSupplier.getContract() == null || !_contractSupplier.getContract().isPresent() || StringUtils.isBlank(_contractSupplier.getContract().get().getId())){
//      _log.warn("Cannot find customer! Current ContractSupplier object not have Contract, or Contract not have Id! _deviceSupplier:=[{}]", _contractSupplier);
//      return Optional.empty();
//    }
//    return customerService.findCustomerByContrattoNumero(_contractSupplier.getContract().get().getId());
//  }

  protected void addCustomer(final Consumo _consumo, final DettaglioStanziamento _allocationDetail) {
    if (_consumo instanceof TrustedCustomerSupplier) {
      _allocationDetail.setCustomer(((TrustedCustomerSupplier) _consumo).getCustomer());
    } else {

//      Optional<Customer> customer = Optional.empty();
//      if (_consumo instanceof DeviceSupplier) {
//        customer = findCustomerByDevice((DeviceSupplier) _consumo);
//      }
//      if (!customer.isPresent() && _consumo instanceof ContractSupplier) {
//        customer = findCustomerByContract(_consumo);
//      }

      //Il contratto deve esistere, altrimenti il consumo sarebbe stato bloccato dalla validazione
      if (_consumo.getContract() != null && _consumo.getContract().isPresent() && StringUtils.isNotBlank(_consumo.getContract().get().getCompanyCode())){
        String customerCode = _consumo.getContract().get().getCompanyCode();
        _allocationDetail.setCustomer(new Customer(customerCode));
      }
    }
    if (_allocationDetail.getCustomer() == null) {
      _log.error("Detail [{}] from consumo [{}[ not have customer! It should not happen...", _allocationDetail);
    }
  }

  protected void addVehicle(final Consumo _consumo, final DettaglioStanziamento _allocationDetail) {
    if (_allocationDetail instanceof VehicleConsumer && _consumo instanceof TrustedVechicleSupplier) {
      _log.debug("Vehicle in Consumo {} is trusted : get Vehicle from it : ", _consumo, ((TrustedVechicleSupplier) _consumo).getVehicle());
      ((VehicleConsumer) _allocationDetail).setVehicle(Optional.ofNullable(((TrustedVechicleSupplier) _consumo).getVehicle())
                                                               .orElse(Vehicle.newEmptyVehicleWithFareClass("")));
    } else {
      if (_allocationDetail instanceof VehicleConsumer && _consumo instanceof VehicleAssociatedWithDeviceSupplier) {
        final VehicleAssociatedWithDeviceSupplier vehicleAssociatedWithDeviceSupplier = (VehicleAssociatedWithDeviceSupplier) _consumo;
        final var vehicle = findVehicleForTransactionDetail(vehicleAssociatedWithDeviceSupplier);
        Optional.ofNullable(vehicleAssociatedWithDeviceSupplier.getVehicle()).ifPresent(
          v -> Optional.ofNullable(v.getFareClass()).ifPresent(
            fareClass -> vehicle.setFareClass(fareClass)
          )
        );
        ((VehicleConsumer) _allocationDetail).setVehicle(vehicle);

        _log.debug("Vehicle for {} is : {}", _consumo, vehicle);
      } else {
        _log.warn("Isn't possible to get vehicle from {} (it needs to be a {}) or isn't possible to add vehicle to {} (it needs to be a {})",
                  _consumo, VehicleAssociatedWithDeviceSupplier.class.getSimpleName(), _allocationDetail,
                  VehicleConsumer.class.getSimpleName());
      }
    }
  }

  //FIXME valutare se ha senso sovrascrivere il Vechicle con uno vuoto se il consumo non ha il dispositivo
  protected Vehicle findVehicleForTransactionDetail(final VehicleAssociatedWithDeviceSupplier vSupplier) {
    String fareClass = null;
    if (vSupplier.getVehicle() != null && vSupplier.getVehicle()
                                                                                                         .getFareClass() != null) {
      fareClass = vSupplier.getVehicle()
                                                      .getFareClass();
    } else {
      _log.warn(" FareClass not available for {}", vSupplier);
      fareClass = "";
    }

    Vehicle vehicle = null;

    if (vSupplier.getDevice() == null) {
      if (vSupplier instanceof SecondaryDeviceSupplier && vSupplier.getVehicle() != null && vSupplier.getVehicle().hasLicenceId()){
        vehicle = vSupplier.getVehicle();
      } else {
        vehicle = newEmptyVehicleWithFareClass(fareClass);
      }
    }else {
      vehicle = vehicleService.newVehicleByVehicleUUID_WithFareClass(vSupplier.getDevice(),
                             vSupplier.getDate(), fareClass).orElse(newEmptyVehicleWithFareClass(fareClass));
    }
    return vehicle;
  }

  protected ValidationOutcome validate(final Consumo _consumo) {
    return consumoValidatorService.validate(_consumo);
  }

  protected <C extends Consumo>  ProcessingResult<C> validateAndNotify(final C _consumo) {

    ProcessingResult<C> processingResult;

    var validationOutcome = validate(_consumo);
    {
      if (validationOutcome.isValidationOk()) {

        _log.debug(" Validation successfull : {}", _consumo);

        // process

        processingResult = new ProcessingResult<C>(_consumo, validationOutcome);

      } else {

          _log.debug(" Validation failure  - isFatal={} : {}", validationOutcome.isFatalError(), _consumo);


          processingResult = new ProcessingResult<C>(_consumo, validationOutcome);

          // notify
          consumoValidatorService.notifyProcessingResult(processingResult);
      }
    }
    if (processingResult.getStanziamentiParams()
                        .isPresent()) {
      _consumo.setGroupArticlesNav(processingResult.getStanziamentiParams()
                                                   .get()
                                                   .getArticle()
                                                   .getGrouping());
      _consumo.setNavSupplierCode(processingResult.getStanziamentiParams()
                                               .get()
                                               .getSupplier()
                                               .getCode());
    }

    return processingResult;
  }

  

  protected <DS extends DettaglioStanziamento,C extends Consumo> DS mapConsumoToDettaglioStanziamento(final C _consumo,
                                                                    final ConsumoDettaglioStanziamentoMapper<C,DS> _mapper) {
    var dettaglioStanziamento = _mapper.mapConsumoToDettaglioStanziamento(_consumo);
    addVehicle(_consumo, dettaglioStanziamento);
    addCustomer(_consumo, dettaglioStanziamento);
    _log.trace("Mapped {}", dettaglioStanziamento);
    return dettaglioStanziamento;
  }

  protected void setVatRateIfNotPresent(Consumo consumo,
                                        Optional<MonetaryAmount> amountExcludedVat,
                                        Optional<MonetaryAmount> amountIncludedVat,
                                        StanziamentiParams stanziamentiParams) {
    BigDecimal vatRate = consumo.getAmount().getVatRateBigDecimal();
    if (vatRate == null){
      setVatRateFromStanziamentiParams(consumo, amountExcludedVat, amountIncludedVat, stanziamentiParams);
    }
  }

  protected void setVatRateFromStanziamentiParams(Consumo consumo, Optional<MonetaryAmount> amountExcludedVat, Optional<MonetaryAmount> amountIncludedVat, StanziamentiParams stanziamentiParams) {
    Float vatRate = stanziamentiParams.getArticle().getVatRate();
    if (vatRate != null) {
      final AmountBuilder amountBuilder = AmountBuilder.builder();
      amountExcludedVat.ifPresent(amount -> amountBuilder.amountExcludedVat(amount));
      amountIncludedVat.ifPresent(amount -> amountBuilder.amountIncludedVat(amount));
      amountBuilder.vatRate(new BigDecimal(stanziamentiParams.getArticle().getVatRate()));
      Amount newAmount = amountBuilder.build();
      consumo.setAmount(newAmount);
    }
  }

}
