package it.fai.ms.consumi.repository.storico_dml.model;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import it.fai.common.enumeration.TipoDispositivoEnum;

@Entity
@Table(name = "view_storico_dispositivo_veicolo")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class ViewStoricoDispositivoVeicoloContratto implements Serializable {

  private static final long serialVersionUID = 7283415906582638103L;
  
  @Id
  @Column(name= "id")
  private String id;

  @Column(name = "dispositivo_dml_unique_identifier")
  private String dispositivoDmlUniqueIdentifier;

  @Column(name = "seriale_dispositivo", length = 255)
  private String serialeDispositivo;

  @Size(max = 255)
  @Column(name = "contratto_numero", length = 255)
  private String contrattoNumero;

  @Size(max = 255)
  @Column(name = "stato", length = 255)
  private String stato;

  @Size(max = 255)
  @Column(name = "contratto_dml_unique_identifier", length = 255)
  private String contrattoDmlUniqueIdentifier;
  
  @Size(max = 255)
  @Column(name = "veicolo_targa", length = 255)
  private String veicoloTarga;

  @Column(name = "veicolo_nazione", length = 255)
  private String veicoloNazione;

  @Size(max = 255)
  @Column(name = "veicolo_classe_euro", length = 255)
  private String veicoloClasseEuro;

  @Column(name = "tipo_dispositivo")
  @Enumerated(EnumType.STRING)
  private TipoDispositivoEnum tipoDispositivo;
  
  @Size(max = 255)
  @Column(name = "veicolo_dml_unique_identifier", length = 255)
  private String veicoloDmlUniqueIdentifier;
  
  @Column(name = "data_variazione")
  private Instant dataVariazione;
  
  @Column(name = "contratto_dml_revision_timestamp")
  private Instant contrattoDmlRevisionTimestamp;
  
  @Column(name = "data_associazione")
  private Instant dataAssociazioneVeicolo;

  @Column(name = "veicolo_dml_revision_timestamp")
  private Instant veicoloDmlRevisionTimestamp;

  public String getSerialeDispositivo() {
    return serialeDispositivo;
  }

  public void setSerialeDispositivo(String serialeDispositivo) {
    this.serialeDispositivo = serialeDispositivo;
  }

  public String getContrattoNumero() {
    return contrattoNumero;
  }

  public ViewStoricoDispositivoVeicoloContratto contrattoNumero(String contrattoNumero) {
    this.contrattoNumero = contrattoNumero;
    return this;
  }

  public void setContrattoNumero(String contrattoNumero) {
    this.contrattoNumero = contrattoNumero;
  }

  public String getStato() {
    return stato;
  }

  public ViewStoricoDispositivoVeicoloContratto stato(String stato) {
    this.stato = stato;
    return this;
  }

  public void setStato(String stato) {
    this.stato = stato;
  }

  public Instant getDataVariazione() {
    return dataVariazione;
  }

  public ViewStoricoDispositivoVeicoloContratto dataVariazione(Instant dataVariazione) {
    this.dataVariazione = dataVariazione;
    return this;
  }

  public void setDataVariazione(Instant dataVariazione) {
    this.dataVariazione = dataVariazione;
  }

  public String getVeicoloTarga() {
    return veicoloTarga;
  }

  public ViewStoricoDispositivoVeicoloContratto veicoloTarga(String veicoloTarga) {
    this.veicoloTarga = veicoloTarga;
    return this;
  }

  public String getVeicoloNazione() {
    return veicoloNazione;
  }

  public void setVeicoloNazione(String veicoloNazione) {
    this.veicoloNazione = veicoloNazione;
  }

  public void setVeicoloTarga(String veicoloTarga) {
    this.veicoloTarga = veicoloTarga;
  }

  public String getVeicoloClasseEuro() {
    return veicoloClasseEuro;
  }

  public ViewStoricoDispositivoVeicoloContratto veicoloClasseEuro(String veicoloClasseEuro) {
    this.veicoloClasseEuro = veicoloClasseEuro;
    return this;
  }

  public void setVeicoloClasseEuro(String veicoloClasseEuro) {
    this.veicoloClasseEuro = veicoloClasseEuro;
  }

  public TipoDispositivoEnum getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(TipoDispositivoEnum tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getDispositivoDmlUniqueIdentifier() {
    return dispositivoDmlUniqueIdentifier;
  }

  public void setDispositivoDmlUniqueIdentifier(String dispositivoDmlUniqueIdentifier) {
    this.dispositivoDmlUniqueIdentifier = dispositivoDmlUniqueIdentifier;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
  

  public String getContrattoDmlUniqueIdentifier() {
    return contrattoDmlUniqueIdentifier;
  }

  public void setContrattoDmlUniqueIdentifier(String contrattoDmlUniqueIdentifier) {
    this.contrattoDmlUniqueIdentifier = contrattoDmlUniqueIdentifier;
  }

  public String getVeicoloDmlUniqueIdentifier() {
    return veicoloDmlUniqueIdentifier;
  }

  public void setVeicoloDmlUniqueIdentifier(String veicoloDmlUniqueIdentifier) {
    this.veicoloDmlUniqueIdentifier = veicoloDmlUniqueIdentifier;
  }
  

  public Instant getContrattoDmlRevisionTimestamp() {
    return contrattoDmlRevisionTimestamp;
  }

  public void setContrattoDmlRevisionTimestamp(Instant contrattoDmlRevisionTimestamp) {
    this.contrattoDmlRevisionTimestamp = contrattoDmlRevisionTimestamp;
  }

  public Instant getVeicoloDmlRevisionTimestamp() {
    return veicoloDmlRevisionTimestamp;
  }

  public void setVeicoloDmlRevisionTimestamp(Instant veicoloDmlRevisionTimestamp) {
    this.veicoloDmlRevisionTimestamp = veicoloDmlRevisionTimestamp;
  }
  
  public Instant getDataAssociazioneVeicolo() {
    return dataAssociazioneVeicolo;
  }

  public void setDataAssociazioneVeicolo(Instant dataAssociazioneVeicolo) {
    this.dataAssociazioneVeicolo = dataAssociazioneVeicolo;
  }

  @Override
  public String toString() {
    return "ViewStoricoDispositivoVeicoloContratto [id=" + id + ", dispositivoDmlUniqueIdentifier=" + dispositivoDmlUniqueIdentifier
           + ", serialeDispositivo=" + serialeDispositivo + ", contrattoNumero=" + contrattoNumero + ", stato=" + stato
           + ", contrattoDmlUniqueIdentifier=" + contrattoDmlUniqueIdentifier + ", veicoloTarga=" + veicoloTarga + ", veicoloNazione="
           + veicoloNazione + ", veicoloClasseEuro=" + veicoloClasseEuro
           + ", tipoDispositivo=" + tipoDispositivo + ", veicoloDmlUniqueIdentifier=" + veicoloDmlUniqueIdentifier + ", dataVariazione="
           + dataVariazione + ", contrattoDmlRevisionTimestamp=" + contrattoDmlRevisionTimestamp + ", dataAssociazioneVeicolo="
           + dataAssociazioneVeicolo + ", veicoloDmlRevisionTimestamp=" + veicoloDmlRevisionTimestamp + "]";
  }
}
