package it.fai.ms.consumi.scheduler.jobs.flussi;

import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RecordDescriptorChecker {

  private Class<? extends RecordDescriptor> descriptorClass;
  private Class<? extends BaseRecord> recordClass;

  public RecordDescriptorChecker(Class<? extends RecordDescriptor> descriptorClass, Class<? extends BaseRecord> recordClass) {
    this.descriptorClass = descriptorClass;
    this.recordClass = recordClass;
  }

  public void checkConfiguration() {
    Logger logger = LoggerFactory.getLogger(descriptorClass);
    List<FileDescriptor> fileDescriptors = Arrays.asList(descriptorClass.getDeclaredFields())
      .stream()
      .filter(field -> Modifier.isStatic(field.getModifiers()))
      .filter(field -> field.getType().isAssignableFrom(FileDescriptor.class))
    .map(field -> {
      try {
        field.setAccessible(true);
        return (FileDescriptor) field.get(descriptorClass);
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
      return null;
    }).collect(Collectors.toList());

    checkConfiguration(logger, fileDescriptors);
  }

  private void checkConfiguration(Logger logger, List<FileDescriptor> fileDescriptors){
    logger.info("Check record description configuration");

    List<StringItem> stringItemList = fileDescriptors
      .stream()
      .flatMap(fileDescriptor -> fileDescriptor.getItems().stream())
      .collect(Collectors.toList());

    for (final StringItem item : stringItemList) {

      final String methodName = "set" + item.getMethod()
        .substring(0, 1)
        .toUpperCase()
        + item.getMethod()
        .substring(1);
      try {
        final Method method = recordClass.getMethod(methodName, String.class);
        if (method == null) {
          logger.warn("Mapping problem with configured item [{}]: cannot find method:=[{}] in record class!", item, methodName);
        }
      } catch (NoSuchMethodException e) {
        logger.warn("Mapping problem with configured item [{}]: cannot find method:=[{}] in record class!", item, methodName);
      } catch (Exception e) {
        logger.error("Unexpected exception during mapping checks!", e);
      }
    }
    logger.info("End record description configuration");
  }

}
