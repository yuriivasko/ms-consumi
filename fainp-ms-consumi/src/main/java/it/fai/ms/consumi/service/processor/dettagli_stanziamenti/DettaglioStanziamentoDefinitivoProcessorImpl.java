package it.fai.ms.consumi.service.processor.dettagli_stanziamenti;

import java.util.HashMap;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamentoCollection;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.pedaggi.StanziamentoProcessorPedaggio;

@Service
public class DettaglioStanziamentoDefinitivoProcessorImpl implements DettaglioStanziamentoDefinitivoProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamantiRepository allocationDetailRepository;
  private final StanziamentoProcessorPedaggio   allocationProcessor;
  private final NotificationService             notificationService;


  @Inject
  public DettaglioStanziamentoDefinitivoProcessorImpl(final DettaglioStanziamantiRepository _allocationDetailRepository,
                                                      final StanziamentoProcessorPedaggio _allocationProcessor,
                                                      NotificationService notificationService) {
    allocationDetailRepository = _allocationDetailRepository;
    allocationProcessor = _allocationProcessor;
    this.notificationService = notificationService;
  }

  @Override
  public Optional<DettaglioStanziamento> process(final DettaglioStanziamento _incomingFromComsumoAllocationDetail,
                                                 final StanziamentiParams _allocationParams) {

    Optional<DettaglioStanziamento> processedAllocationDetail;

    final var globalIdentifier = _incomingFromComsumoAllocationDetail.getGlobalIdentifier();

    _log.debug(" - {} - processing gId {}", _incomingFromComsumoAllocationDetail.getInvoiceType(), globalIdentifier);

    final var allocationDetailsFoundByGID = allocationDetailRepository.findByGlobalIdentifierId(globalIdentifier,_allocationParams!=null ? _allocationParams.getStanziamentiDetailsType() : null);

    if (allocationDetailsFoundByGID.isEmpty()) {
      final var allocationDetail = process_case_1___noPersistedAllocationDetailsFound(_incomingFromComsumoAllocationDetail, _allocationParams);
      processedAllocationDetail = Optional.ofNullable(allocationDetail);
    } else {
      final var allocationDetail = process_case_2___foundPersistedAllocationDetails(_incomingFromComsumoAllocationDetail,
                                                                                    new DettaglioStanziamentoCollection(allocationDetailsFoundByGID),
                                                                                    _allocationParams);
      processedAllocationDetail = Optional.ofNullable(allocationDetail);
    }

    return processedAllocationDetail;
  }

  private DettaglioStanziamento saveAllocationDetail(final DettaglioStanziamento _allocationDetail) {
    allocationDetailRepository.save(_allocationDetail);
    return _allocationDetail;
  }

  private DettaglioStanziamento cloneAllocationDetailAndChangeSign(final DettaglioStanziamento _allocationDetail) {
      return allocationDetailRepository.cloneAndChangeSign(_allocationDetail);
  }

  private DettaglioStanziamento createAllocationDetailWithNewGlobalIdentifier(final DettaglioStanziamento _allocationDetail) {
    var allocationDetailWithNewGlobalIdentifier = _allocationDetail.generateNewGlobalIdentifier();
    allocationDetailRepository.save(allocationDetailWithNewGlobalIdentifier);
    return allocationDetailWithNewGlobalIdentifier;
  }


  private DettaglioStanziamento process_case_1___noPersistedAllocationDetailsFound(final DettaglioStanziamento _allocationDetail,
                                                                                   final StanziamentiParams _allocationParams) {

    DettaglioStanziamento allocationDetailToReturn = null;

    _log.debug(" case 1 (no allocation details): {} {} not found for gId {}", _allocationDetail.getClass()
                                                                                              .getSimpleName(),
              _allocationDetail.getInvoiceType(), _allocationDetail.getGlobalIdentifier());

    final var allocations = allocationProcessor.processStanziamento(_allocationDetail, _allocationParams);
    _allocationDetail.getStanziamenti()
                     .addAll(allocations);

    allocationDetailToReturn = saveAllocationDetail(_allocationDetail);

    _log.debug(" case 1 (no allocation details): {} {} created  with gId {}", _allocationDetail.getClass()
                                                                                              .getSimpleName(),
              _allocationDetail.getInvoiceType(), _allocationDetail.getGlobalIdentifier());

    return allocationDetailToReturn;
  }

  private DettaglioStanziamento process_case_2___foundPersistedAllocationDetails(final DettaglioStanziamento _incomingFromComsumoAllocationDetail,
                                                                                 final DettaglioStanziamentoCollection _persistedAllocationDetailsFoundByGID,
                                                                                 final StanziamentiParams _allocationParams) {

    DettaglioStanziamento allocationDetailToReturn = null;

    
    if(_log.isDebugEnabled()) {
      _log.debug(" case 2 (found allocation details): found {} persisted {}", _persistedAllocationDetailsFoundByGID.size(),
                _incomingFromComsumoAllocationDetail.getClass()
                .getSimpleName());
      _persistedAllocationDetailsFoundByGID.getDettaglioStanziamentoSet()
      .forEach(allocationDetail -> {
        _log.debug(" case 2 (found allocation details): {} {}", allocationDetail.getInvoiceType(),
                  allocationDetail.getGlobalIdentifier());
      });
    }

    if (_persistedAllocationDetailsFoundByGID.thereAreOnlyDettaglioStanziamentoProvvisorio()) {

      allocationDetailToReturn = process_case_2_a_found_onlyTemp(_incomingFromComsumoAllocationDetail, _persistedAllocationDetailsFoundByGID,
                                                                 _allocationParams);

    } else if (_persistedAllocationDetailsFoundByGID.thereAreOnlyDettaglioStanziamentoDefinitivo()) {

      allocationDetailToReturn = process_case_2_b_found_onlyLast(_incomingFromComsumoAllocationDetail, _persistedAllocationDetailsFoundByGID,
                                                                 _allocationParams);

    } else if (_persistedAllocationDetailsFoundByGID.thereAreBothDettaglioStanziamentoTypes()) {

      allocationDetailToReturn = process_case_2_c_found_both(_incomingFromComsumoAllocationDetail);

    }

    return allocationDetailToReturn;
  }

  private DettaglioStanziamento process_case_2_a_found_onlyTemp(final DettaglioStanziamento _incomingFromComsumoAllocationDetail,
                                                                final DettaglioStanziamentoCollection _persistedTempAllocationDetailsFoundByGID,
                                                                final StanziamentiParams _allocationParams) {

    DettaglioStanziamento allocationDetailToReturn;

    _log.debug("  found only {} of type {} - skip processing -", _incomingFromComsumoAllocationDetail.getClass()
                                                                                 .getSimpleName(),
                                                                                 InvoiceType.P);

    final var persistedAllocationDetail = _persistedTempAllocationDetailsFoundByGID.getFirst();
    persistedAllocationDetail.changeInvoiceTypeToD();

    if (_incomingFromComsumoAllocationDetail.hasTheSameAmount(persistedAllocationDetail)) {
      _log.debug("   dettaglio stanziamento amount is the same");

      final var allocations = allocationProcessor.processStanziamentoForCase2aWithSameAmount(persistedAllocationDetail, _allocationParams);
      _incomingFromComsumoAllocationDetail.getStanziamenti().addAll(allocations);

      final var allocationDetailUpdatedToD = persistedAllocationDetail;
      allocationDetailUpdatedToD.getStanziamenti().addAll(allocations);
      saveAllocationDetail(allocationDetailUpdatedToD);
      allocationDetailToReturn = allocationDetailUpdatedToD;

    } else {
      _log.debug("   dettaglio stanziamento amount is different");

      final var allocationDetailUpdatedToD = persistedAllocationDetail;
      saveAllocationDetail(allocationDetailUpdatedToD);

      final var newAllocationDetailDCloned = cloneAllocationDetailAndChangeSign(allocationDetailUpdatedToD);

      final var allocations1 = allocationProcessor.processClonedDStanziamentoForCase2aWithDiffAmount(newAllocationDetailDCloned,
                                                                                                     _allocationParams);
      final var allocations2 = allocationProcessor.processCreatedStanziamentoForCase2aWithDiffAmount(_incomingFromComsumoAllocationDetail, _allocationParams);
      _incomingFromComsumoAllocationDetail.getStanziamenti()
                       .addAll(allocations1);
      _incomingFromComsumoAllocationDetail.getStanziamenti()
                       .addAll(allocations2);

      final var newPersistedAllocationDetail = saveAllocationDetail(_incomingFromComsumoAllocationDetail);

      allocationDetailToReturn = newPersistedAllocationDetail;
    }

    return allocationDetailToReturn;
  }

  private DettaglioStanziamento process_case_2_b_found_onlyLast(final DettaglioStanziamento _allocationDetail,
                                                                final DettaglioStanziamentoCollection _persistedAllocationDetailCollection,
                                                                final StanziamentiParams _allocationParams) {

    DettaglioStanziamento allocationDetailToReturn;

    final var allocationDetail = _persistedAllocationDetailCollection.getFirst();
    final var globalIdentifierType = allocationDetail.getGlobalIdentifier()
                                                     .getType();

    final String simpleName = _allocationDetail.getClass().getSimpleName();
    switch (globalIdentifierType) {
    case EXTERNAL: {

      _log.debug("  found only {} of type {} with external gId - skip processing -", simpleName,
                InvoiceType.D);
//      _log.info("  found only {} of type {} with external gId   --> notify error", _allocationDetail.getClass()
//                                                                                                  .getSimpleName(),
//                InvoiceType.D);

      // FIXME verificare messaggio
      _log.warn("SENDING ANOMALIA: Attesi {} di tipo {}, ma trovati solo di tipo {}: ",
                simpleName, InvoiceType.P.extendedName, InvoiceType.D.extendedName);

      var messageTextKV = new HashMap<String, Object>();
      messageTextKV.put("simpleName", simpleName);
      messageTextKV.put("expected", InvoiceType.P.extendedName);
      messageTextKV.put("actual", InvoiceType.D.extendedName);
      messageTextKV.put("dettaglio", _allocationDetail.toString());

      //FIXME portare qui recodUuid?
      notificationService.notify("DSP-001", messageTextKV);

      allocationDetailToReturn = null;
      break;
    }
    case INTERNAL: {

      _log.debug("  found only {} of type {} with internal gId --> regenerate gId", simpleName,
                InvoiceType.D);

      final var allocations = allocationProcessor.processStanziamento(_allocationDetail, _allocationParams);
      _allocationDetail.getStanziamenti()
                       .addAll(allocations);

      allocationDetailToReturn = createAllocationDetailWithNewGlobalIdentifier(_allocationDetail);

      _log.debug(" case 2 : persisted {} with new generated gId {}", allocationDetailToReturn.getClass()
                                                                                            .getSimpleName(),
                allocationDetailToReturn.getGlobalIdentifier());

      break;
    }
    default:
      allocationDetailToReturn = null;
      _log.warn("Global identifier type unknow: {}", globalIdentifierType);
    }

    return allocationDetailToReturn;
  }

  private DettaglioStanziamento process_case_2_c_found_both(final DettaglioStanziamento _allocationDetail) {
    _log.debug("  found both {} of type {} and {} - skip processing -", _allocationDetail.getClass()
                                                                                        .getSimpleName(),
              InvoiceType.P, InvoiceType.D);
//    _log.info("  found only {} of type {} and {} - skip processing -", _allocationDetail.getClass()
//                                                                                     .getSimpleName(),
//              InvoiceType.P, InvoiceType.D);

    // FIXME verificare messaggio
    final String simpleName = _allocationDetail.getClass().getSimpleName();

    _log.warn("SENDING ANOMALIA: Attesi {} di tipo {}, ma trovati solo di entrmbi i tipi {}, {}: ",
              simpleName, InvoiceType.P.extendedName, InvoiceType.P.extendedName, InvoiceType.D.extendedName);

    var messageTextKV = new HashMap<String, Object>();
    messageTextKV.put("simpleName", simpleName);
    messageTextKV.put("expected", InvoiceType.P.extendedName);
    messageTextKV.put("both", InvoiceType.P.extendedName + ", " + InvoiceType.D.extendedName);
    messageTextKV.put("dettaglio", _allocationDetail.toString());

    //FIXME portare qui recodUuid?
    notificationService.notify("DSP-002", messageTextKV);

    return null;
  }

}
