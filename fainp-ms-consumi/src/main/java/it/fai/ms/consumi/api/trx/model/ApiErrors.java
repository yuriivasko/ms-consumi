package it.fai.ms.consumi.api.trx.model;

import java.util.Objects;

import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * ApiErrors
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-11-20T16:08:12.419Z")

public class ApiErrors   {
  @JsonProperty("errType")
  private String errType = null;

  @JsonProperty("errCode")
  private Integer errCode = null;

  @JsonProperty("errMessage")
  private String errMessage = null;

  public ApiErrors errType(String errType) {
    this.errType = errType;
    return this;
  }

  /**
   * The error's short description (e.g. 'Access Denied').
   * @return errType
  **/
  @ApiModelProperty(required = true, value = "The error's short description (e.g. 'Access Denied').")
  @NotNull


  public String getErrType() {
    return errType;
  }

  public void setErrType(String errType) {
    this.errType = errType;
  }

  public ApiErrors errCode(Integer errCode) {
    this.errCode = errCode;
    return this;
  }

  /**
   * The error's HTTP code (e.g. 403).
   * @return errCode
  **/
  @ApiModelProperty(required = true, value = "The error's HTTP code (e.g. 403).")
  @NotNull


  public Integer getErrCode() {
    return errCode;
  }

  public void setErrCode(Integer errCode) {
    this.errCode = errCode;
  }

  public ApiErrors errMessage(String errMessage) {
    this.errMessage = errMessage;
    return this;
  }

  /**
   * The error's long description (e.g. 'Bad vendor_key').
   * @return errMessage
  **/
  @ApiModelProperty(required = true, value = "The error's long description (e.g. 'Bad vendor_key').")
  @NotNull


  public String getErrMessage() {
    return errMessage;
  }

  public void setErrMessage(String errMessage) {
    this.errMessage = errMessage;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiErrors apiErrors = (ApiErrors) o;
    return Objects.equals(this.errType, apiErrors.errType) &&
        Objects.equals(this.errCode, apiErrors.errCode) &&
        Objects.equals(this.errMessage, apiErrors.errMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(errType, errCode, errMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiErrors {\n");
    
    sb.append("    errType: ").append(toIndentedString(errType)).append("\n");
    sb.append("    errCode: ").append(toIndentedString(errCode)).append("\n");
    sb.append("    errMessage: ").append(toIndentedString(errMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

