package it.fai.ms.consumi.scheduler.jobs.flussi.consumi;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.consumi.repository.flussi.record.model.telepass.ElcitRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.repository.flussi.record.model.telepass.VaconRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class VaconDescriptor implements RecordDescriptor<VaconRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(VaconDescriptor.class);

  public final static String HEADER_BEGIN      = "01";
  public final static String FOOTER_CODE_BEGIN = "90";
  public final static String BEGIN_07          = "07";
  public final static String BEGIN_09          = "09";
  public final static String BEGIN_11          = "11";
  public final static String BEGIN_12          = "12";
  public final static String BEGIN_13          = "13";
  public final static String BEGIN_14          = "14";
  public final static String BEGIN_15          = "15";
  public final static String BEGIN_17          = "17";
  public final static String BEGIN_20          = "20";
  public final static String BEGIN_30          = "30";

  // Header
  public static FileDescriptor descriptor01 = new FileDescriptor();

  static {
    descriptor01.addItem("recordCode", 1, 2);
    descriptor01.addItem("partnerCountryCode", 3, 5);
    descriptor01.skipItem("transmitter", 6, 10);
    descriptor01.skipItem("transmitterCountryCode", 11, 13);
    descriptor01.skipItem("receiver", 14, 18);
    descriptor01.addItem("creationDateInFile", 19, 26);
    descriptor01.addItem("creationTimeInFile", 27, 32);
    descriptor01.skipItem("fileNumber", 33, 36);
    descriptor01.skipItem("dataType", 37, 46);
    descriptor01.skipItem("fileReleaseNumber", 47, 52);
  }

  // TIS-PL Details
  public static FileDescriptor descriptor07 = new FileDescriptor();

  static {
    descriptor07.addItem("recordCode", 1, 2);
    descriptor07.addItem("partnerCountryCode", 3, 5);
    descriptor07.addItem("partnerNumber", 6, 10);
    descriptor07.addItem("documentNumber", 11, 29); // v2
    descriptor07.addItem("documentId", 30, 44); // v2
    descriptor07.addItem("invoiceType", 45, 45);
    descriptor07.addItem("invoiceId", 46, 54);
    descriptor07.addItem("tollType", 55, 55);
    descriptor07.addItem("tspRespCountryCode", 56, 58);
    descriptor07.addItem("tspRespNumber", 59, 63);
    descriptor07.addItem("tspExitCountryCode", 64, 66);
    descriptor07.addItem("tspExitNumber", 67, 71);
    descriptor07.addItem("transactionId", 72, 100);
    descriptor07.addItem("splitNumber", 101, 101);
    descriptor07.addItem("compensationNumber", 102, 102);
    descriptor07.addItem("signOfTransaction", 103, 103);
    descriptor07.addItem("telepassPanNumber", 104, 122);
    descriptor07.addItem("discountSchemeNumber", 123, 134);
    descriptor07.addItem("entryTimestamp", 135, 148);
    descriptor07.addItem("entryGateCode", 149, 157);
    descriptor07.addItem("entryAcquisitionType", 158, 159);
    descriptor07.addItem("exitTimestamp", 160, 173);
    descriptor07.addItem("exitGateCode", 174, 182);
    descriptor07.addItem("exitAcquisitionType", 183, 184);
    descriptor07.addItem("pricingClass", 185, 186);
    descriptor07.addItem("km", 187, 191);
    descriptor07.addItem("priceType", 192, 196);
    descriptor07.addItem("priceCode", 197, 201);
    descriptor07.addItem("currencyCode", 202, 204);
    descriptor07.addItem("amountWithoutVATOfBase", 205, 215);
    descriptor07.addItem("amountWithoutVATExcludedForDiscount", 216, 226);
    descriptor07.addItem("VATRate", 227, 230);
    descriptor07.addItem("amountIncludingVAT", 231, 241);
    descriptor07.addItem("observationCode", 242, 243);
    descriptor07.addItem("externalOBUIdentifier", 244, 263);
    descriptor07.addItem("typeOfRecordingPoint", 264, 265); // v2
    descriptor07.addItem("flagForTransactionNotConfirmed", 266, 266);
    descriptor07.addItem("transactionDetail", 267, 268); // v2
  }

  // VIA-T Details
  // VIA-T PT Details
  // Tunnel Belgium
  public static FileDescriptor descriptor09_11_12 = new FileDescriptor();

  static {
    descriptor09_11_12.addItem("recordCode", 1, 2);
    descriptor09_11_12.addItem("partnerCountryCode", 3, 5);
    descriptor09_11_12.addItem("partnerNumber", 6, 10);
    descriptor09_11_12.addItem("documentNumber", 11, 29); // v2
    descriptor09_11_12.addItem("documentId", 30, 44); // v2
    descriptor09_11_12.addItem("invoiceType", 45, 45);
    descriptor09_11_12.addItem("invoiceId", 46, 54);
    descriptor09_11_12.addItem("tollType", 55, 55);
    descriptor09_11_12.addItem("tspRespCountryCode", 56, 58);
    descriptor09_11_12.addItem("tspRespNumber", 59, 63);
    descriptor09_11_12.addItem("tspExitCountryCode", 64, 66);
    descriptor09_11_12.addItem("tspExitNumber", 67, 71);
    descriptor09_11_12.addItem("transactionId", 72, 100);
    descriptor09_11_12.addItem("signOfTransaction", 101, 101);
    descriptor09_11_12.addItem("telepassPanNumber", 102, 120);
    descriptor09_11_12.addItem("entryTimestamp", 121, 134);
    descriptor09_11_12.addItem("entryGateCode", 135, 143);
    descriptor09_11_12.addItem("exitTimestamp", 144, 157);
    descriptor09_11_12.addItem("exitGateCode", 158, 166);
    descriptor09_11_12.addItem("km", 167, 171);
    descriptor09_11_12.addItem("currencyCode", 172, 174);
    descriptor09_11_12.addItem("amountWithoutVATOfBase", 175, 185);
    descriptor09_11_12.addItem("amountWithoutVATExcludedForDiscount", 186, 196);
    descriptor09_11_12.addItem("VATRate", 197, 200);
    descriptor09_11_12.addItem("amountIncludingVAT", 201, 211);
    descriptor09_11_12.addItem("observationCode", 212, 213);
    descriptor09_11_12.addItem("externalOBUIdentifier", 214, 233);
    descriptor09_11_12.addItem("transactionDetail", 234, 235); // v2
  }

  // Austria
  public static FileDescriptor descriptor13 = new FileDescriptor();

  static {
    descriptor13.addItem("recordCode", 1, 2);
    descriptor13.addItem("partnerCountryCode", 3, 5);
    descriptor13.addItem("partnerNumber", 6, 10);
    descriptor13.addItem("documentNumber", 11, 29); // v2
    descriptor13.addItem("documentId", 30, 44); // v2
    descriptor13.addItem("invoiceType", 45, 45); // v2
    descriptor13.addItem("invoiceId", 46, 54); // v2
    descriptor13.addItem("tollType", 55, 55); // v2
    descriptor13.addItem("tollCharger", 56, 63); // v2
    descriptor13.addItem("tollGate", 64, 67); // v2
    descriptor13.addItem("laneId", 68, 71); // v2
    descriptor13.addItem("networkCode", 72, 72); // v2
    descriptor13.addItem("pan", 73, 85); // v2
    descriptor13.addItem("exitTransitDateTime", 86, 99); // v2
    // descriptor13.addItem("blank", 100, 100); //v2
    descriptor13.addItem("transitType", 101, 102); // v2
    descriptor13.addItem("sign", 103, 103); // v2
    descriptor13.addItem("additionalInfo", 104, 119); // v2
    descriptor13.addItem("signOfTransaction", 120, 120);
    descriptor13.addItem("telepassPanNumber", 121, 139);
    descriptor13.addItem("entryTimestamp", 140, 153);
    descriptor13.addItem("entryGateCode", 154, 162);
    descriptor13.addItem("exitTimestamp", 163, 176);
    descriptor13.addItem("exitGateCode", 177, 185);
    descriptor13.addItem("externalCostAir", 186, 196); // v2
    descriptor13.addItem("externalCostNoise", 197, 207); // v2
    descriptor13.addItem("km", 208, 212);
    descriptor13.addItem("currencyCode", 213, 215);
    descriptor13.addItem("amountWithoutVATOfBase", 216, 226);
    descriptor13.addItem("amountWithoutVATExcludedForDiscount", 227, 237); // v2
    descriptor13.addItem("VATRate", 238, 241);
    descriptor13.addItem("amountIncludingVAT", 242, 252);
    descriptor13.addItem("observationCode", 253, 254); // v2
    descriptor13.addItem("externalOBUIdentifier", 255, 274);
    descriptor13.addItem("transactionAggregationNumber", 275, 290); // v2
    descriptor13.addItem("transactionDetail", 291, 292);
  }

  // Telepass SAM Poland
  public static FileDescriptor descriptor14 = new FileDescriptor();

  static {
    descriptor14.addItem("recordCode", 1, 2);
    descriptor14.addItem("partnerCountryCode", 3, 5);
    descriptor14.addItem("partnerNumber", 6, 10);
    descriptor14.addItem("documentNumber", 11, 29); // v2
    descriptor14.addItem("documentId", 30, 44); // v2
    descriptor14.addItem("invoiceType", 45, 45); // v2
    descriptor14.addItem("invoiceId", 46, 54); // v2
    descriptor14.addItem("tollType", 55, 55); // v2
    descriptor14.addItem("tollCharger", 56, 63); // v2
    descriptor14.addItem("tollGate", 64, 67); // v2
    descriptor14.addItem("laneId", 68, 71); // v2
    descriptor14.addItem("networkCode", 72, 72); // v2
    descriptor14.addItem("pan", 73, 85); // v2
    descriptor14.addItem("exitTransitDateTime", 86, 99); // v2
    // descriptor14.addItem("blank", 100, 100); //v2
    descriptor14.addItem("transitType", 101, 102); // v2
    descriptor14.addItem("sign", 103, 103); // v2
    descriptor14.addItem("additionalInfo", 104, 119); // v2
    descriptor14.addItem("signOfTransaction", 120, 120);
    descriptor14.addItem("telepassPanNumber", 121, 139);
    descriptor14.addItem("entryTimestamp", 140, 153);
    descriptor14.addItem("entryGateCode", 154, 162);
    descriptor14.addItem("exitTimestamp", 163, 176);
    descriptor14.addItem("exitGateCode", 177, 185);
    descriptor14.addItem("km", 186, 190);
    descriptor14.addItem("currencyCode", 191, 193);
    descriptor14.addItem("amountWithoutVAT", 194, 204);
    descriptor14.addItem("VATRate", 205, 208);
    descriptor14.addItem("amountIncludingVAT", 209, 219);
    descriptor14.addItem("observationCode", 220, 221); // v2
    descriptor14.addItem("externalOBUIdentifier", 222, 241);
    descriptor14.addItem("transactionAggregationNumber", 242, 257); // v2
    descriptor14.addItem("transactionDetail", 258, 259);
  }

  // EETS Germany
  public static FileDescriptor descriptor15 = new FileDescriptor();

  static {
    descriptor15.addItem("recordCode", 1, 2);
    descriptor15.addItem("partnerCountryCode", 3, 5);
    descriptor15.addItem("partnerNumber", 6, 10);
    descriptor15.addItem("documentNumber", 11, 29);
    descriptor15.addItem("documentId", 30, 44);
    descriptor15.addItem("tripId", 45, 60);
    descriptor15.addItem("obu", 61, 80);
    descriptor15.addItem("entryTime", 81, 86);
    descriptor15.addItem("entryDate", 87, 94);
    descriptor15.addItem("entryGateCode", 95, 103);
    descriptor15.addItem("exitGateCode", 104, 112);
    descriptor15.addItem("signOfTransaction", 113, 113);
    descriptor15.addItem("telepassPanNumber", 114, 132);
    descriptor15.addItem("exitDate", 133, 140);
    descriptor15.addItem("exitTime", 141, 146);
    descriptor15.addItem("currencyCode", 147, 149);
    descriptor15.addItem("plateNumber", 150, 169);
    descriptor15.addItem("countryPlateNumber", 170, 172);
    descriptor15.addItem("vehicleClass", 173, 174);
    descriptor15.addItem("euroClass", 175, 179);
    descriptor15.addItem("totalAmount", 177, 187);
    descriptor15.addItem("claimId", 188, 203);
    descriptor15.addItem("crossingDate", 204, 211);
    descriptor15.addItem("crossingTime", 212, 217);
    descriptor15.addItem("sectionAmount", 218, 228);
    descriptor15.addItem("transactionDetail", 229, 230);
    descriptor15.addItem("sectionGateId", 231, 239);

    //TODO? external cost: documento TP dice Mandatory only for Trip (9 INT 2 DEC)
    descriptor15.addItem("externalCost", 248, 258);
    /*
    TODO? trafficClassification: documento TP dice

    Mandatoryonly for section
    VVVVVOOZAGSSBBB iscomposed by:
    VVVVV= Tarifversion
    OO= Ortsklasse
    Z=Zeitklasse
    A=Achsanzahl
    G=Gewichtsklasse
    SS= EURO- Schastoffklasse
    BBb= Straßenbetreiber
    */
    descriptor15.addItem("trafficClassification", 254, 268);
    descriptor15.addItem("weightCategoryDescription", 269, 298);

  }

  // EETS Belgium
  public static FileDescriptor descriptor17 = new FileDescriptor();

  static {
    descriptor17.addItem("recordCode", 1, 2);
    descriptor17.addItem("partnerCountryCode", 3, 5);
    descriptor17.addItem("partnerNumber", 6, 10);
    descriptor17.addItem("documentNumber", 11, 29); // v2
    descriptor17.addItem("documentId", 30, 44); // v2
    descriptor17.addItem("invoiceType", 45, 45); // v2
    descriptor17.addItem("obu", 46, 65); // v2
    descriptor17.addItem("region", 66, 71); // v2
    descriptor17.addItem("roadType", 72, 75); // v2
    descriptor17.addItem("route", 76, 100); // v2
    descriptor17.addItem("entryTime", 101, 106); // v2
    descriptor17.addItem("entryDate", 107, 114);
    descriptor17.addItem("signOfTransaction", 115, 115);
    descriptor17.addItem("telepassPanNumber", 116, 134);
    descriptor17.addItem("exitDate", 135, 142); // v2
    descriptor17.addItem("exitTime", 143, 148); // v2
    descriptor17.addItem("km", 149, 158);
    descriptor17.addItem("currencyCode", 159, 161);
    descriptor17.addItem("plateNumber", 162, 181); // v2
    descriptor17.addItem("countryPlateNumber", 182, 184); // v2
    descriptor17.addItem("vehicleClass", 185, 186); // v2
    descriptor17.addItem("euroClass", 187, 188); // v2
    descriptor17.addItem("amountWithoutVAT", 189, 199);
    descriptor17.addItem("VATRate", 200, 203);
    descriptor17.addItem("amountIncludingVAT", 204, 214);
    descriptor17.addItem("transactionDetail", 215, 216);
  }

  // Invoice Line Detail
  public static FileDescriptor descriptor20 = new FileDescriptor();

  static {
    descriptor20.addItem("recordCode", 1, 2);
    descriptor20.addItem("documentId", 3, 17); // v2
    descriptor20.addItem("documentNumber", 18, 36); // v2
    descriptor20.addItem("partnerCountryCode", 37, 39);
    descriptor20.addItem("partnerNumber", 40, 44);
    descriptor20.addItem("invoiceType", 45, 45);
    descriptor20.addItem("serviceType", 46, 48);
    descriptor20.addItem("invoiceId", 49, 57);
    descriptor20.addItem("telepassCustomerID", 58, 66);
    descriptor20.addItem("otherID", 67, 80);
    descriptor20.addItem("telepassPanNumber", 81, 99);
    descriptor20.addItem("TSPCountryCode", 100, 102);
    descriptor20.addItem("TSPNr", 103, 107);
    descriptor20.addItem("CCPNr", 108, 119);
    descriptor20.addItem("invoiceLineNr", 120, 129);
    descriptor20.addItem("transactionDetail", 130, 139); //lineType
    descriptor20.addItem("descriptionText", 140, 169);
    descriptor20.addItem("currencyCode", 170, 172);
    descriptor20.addItem("signOfQuantity", 173, 173);
    descriptor20.addItem("quantity", 174, 180);
    descriptor20.addItem("signOfBaseWithoutVAT", 181, 181);
    descriptor20.addItem("baseWithoutVAT", 182, 192);
    descriptor20.addItem("rateSign", 193, 193);
    descriptor20.addItem("rate", 194, 197);
    descriptor20.addItem("signOfAmountWithoutVAT", 198, 198);
    descriptor20.addItem("amountWithoutVAT", 199, 209);
    descriptor20.addItem("VATRate", 210, 213);
    descriptor20.addItem("signOfAmountIncludingVAT", 214, 214);
    descriptor20.addItem("amountIncludingVAT", 215, 225);
    descriptor20.addItem("externalOBUIdentifier", 226, 245);
    descriptor20.addItem("externalCostBelgium", 246, 256); // v2
    descriptor20.addItem("descriptionTextDetail", 257, 286);
    descriptor20.addItem("baseAmountForFeeCalculation", 287, 297); // v2
    descriptor20.addItem("rateForFeeCalculation", 298, 301); // v2
  }

  // Invoice Line Total
  public static FileDescriptor descriptor30 = new FileDescriptor();

  static {
    descriptor30.addItem("recordCode", 1, 2);
    descriptor30.addItem("documentId", 3, 17); // v2
    descriptor30.addItem("documentNumber", 18, 36); // v2
    descriptor30.addItem("partnerCountryCode", 37, 39);
    descriptor30.addItem("partnerNumber", 40, 44);
    descriptor30.addItem("invoiceType", 45, 45);
    descriptor30.addItem("serviceType", 46, 48);
    descriptor30.addItem("invoiceId", 49, 57);
    descriptor30.addItem("telepassCustomerID", 58, 66);
    descriptor30.addItem("otherID", 67, 80);
    descriptor30.addItem("TSPCountryCode", 81, 83);
    descriptor30.addItem("TSPNr", 84, 88);
    descriptor30.addItem("discountSchemeNumber", 89, 100);
    descriptor30.addItem("invoiceLineNr", 101, 110);
    descriptor30.addItem("currencyCode", 111, 113);
    descriptor30.addItem("lineType", 114, 123);
    descriptor30.addItem("VATRate", 124, 127);
    descriptor30.addItem("signOfAmountIncludingVAT", 128, 128);
    descriptor30.addItem("amountIncludingVAT", 129, 139);
    descriptor30.addItem("externalOBUIdentifier", 140, 159);
  }

  // Trailer
  public static FileDescriptor descriptor90          = new FileDescriptor();
  static final String          numberOfDetailRecords = "numberOfDetailRecords";
  static {
    descriptor90.addItem("recordCode", 1, 2);
    descriptor90.addItem("transmitterCode", 3, 10);
    descriptor90.addItem("receiverCode", 11, 18);
    descriptor90.addItem("fileCreationDate", 19, 26);
    descriptor90.addItem("fileCreationTime", 27, 32);
    descriptor90.addItem("fileNumber", 33, 36);
    descriptor90.addItem("dataType", 37, 46);
    descriptor90.addItem(numberOfDetailRecords, 47, 54);
  }

  @Override
  public VaconRecord decodeRecordCodeAndCallSetFromString(final String _data, final String _recordCode, String fileName, long rowNumber) {
    var entity = new VaconRecord(fileName,rowNumber);
    switch (_recordCode) {

    case HEADER_BEGIN:
      entity.setFromString(VaconDescriptor.descriptor01.getItems(), _data);
      break;

    case BEGIN_07:
      entity.setFromString(VaconDescriptor.descriptor07.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_FRANCIA);
      break;

    case BEGIN_09:
      entity.setFromString(VaconDescriptor.descriptor09_11_12.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_SPAGNA);
      break;
    case BEGIN_11:
      entity.setFromString(VaconDescriptor.descriptor09_11_12.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_PORTOGALLO);
      break;
    case BEGIN_12:
      entity.setFromString(VaconDescriptor.descriptor09_11_12.getItems(), _data);
      entity.setService_type(TipoServizioEnum.TUNNEL_LIEFKENSHOEK);
      break;
    case BEGIN_13:
      entity.setFromString(VaconDescriptor.descriptor13.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_AUSTRIA);
      break;
    case BEGIN_14:
      entity.setFromString(VaconDescriptor.descriptor14.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_POLONIA);
      break;
    case BEGIN_15:
      entity.setFromString(VaconDescriptor.descriptor15.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_GERMANIA);
      entity.setVATRate("0");
      entity.setAmountWithoutVAT(entity.getTotalAmount());
      break;
    case BEGIN_17:
      entity.setFromString(VaconDescriptor.descriptor17.getItems(), _data);
      entity.setService_type(TipoServizioEnum.PEDAGGI_BELGIO);
      String amountIncludingVatStr = entity.getAmountIncludingVAT();
      entity.setAmountIncludingVAT(amountIncludingVatStr,-6,5);
      String amountExcludedVatStr = entity.getAmountWithoutVAT();
      entity.setAmountWithoutVAT(amountExcludedVatStr,-6,5);
      break;
    case BEGIN_20:
      entity.setFromString(VaconDescriptor.descriptor20.getItems(), _data);
      //FIXME il tipo di servizio derivato dal transaction detail dovrebbe essere guidato dalla param stanziamenti --> sara' CR da implementare
      entity.setService_type(getServizioByTransactionDetailHardcoded(entity.getTransactionDetail()));
      break;

    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }



  @Override
  public boolean skipWellKnownRecordCodeToIgnore(String recordCode) {
    return ("30").equals(recordCode); //"aggregation record" must be skipped
  }



  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    Optional<StringItem> stringItem = VaconDescriptor.descriptor90.getItem(VaconDescriptor.numberOfDetailRecords);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in VaconDescriptor!");
    }
    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());
    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return VaconDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return VaconDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 2;
  }


  private static TipoServizioEnum getServizioByTransactionDetailHardcoded(String transactionDetail) {

    if (transactionDetail!=null && !transactionDetail.isEmpty()) {
      switch(transactionDetail) {
      case "IG" : return TipoServizioEnum.PEDAGGI_FRANCIA;
      case "IH" : return TipoServizioEnum.PEDAGGI_FRANCIA;
      case "IP" : return TipoServizioEnum.PEDAGGI_FRANCIA;
      case "IU" : return TipoServizioEnum.PEDAGGI_FRANCIA;
      case "IY" : return TipoServizioEnum.PEDAGGI_FRANCIA;
      case "75" : return TipoServizioEnum.PEDAGGI_FRANCIA;
      case "79" : return TipoServizioEnum.PEDAGGI_FRANCIA;
      case "I7" : return TipoServizioEnum.PEDAGGI_AUSTRIA;
      case "I3" : return TipoServizioEnum.PEDAGGI_BELGIO;
      case "I0" : return TipoServizioEnum.PEDAGGI_SPAGNA;
      case "78" : return TipoServizioEnum.PEDAGGI_SPAGNA;
      case "80" : return TipoServizioEnum.PEDAGGI_SPAGNA;
      case "L1" : return TipoServizioEnum.PEDAGGI_SPAGNA;
      case "IW" : return TipoServizioEnum.PEDAGGI_POLONIA;
      case "I5" : return TipoServizioEnum.PEDAGGI_PORTOGALLO;
      default: return null;
      }

    }
    return null;

  }

  public void checkConfiguration(){
    new RecordDescriptorChecker(VaconDescriptor.class, VaconRecord.class).checkConfiguration();
  }

}
