package it.fai.ms.consumi.service.record.telepass;

import java.util.Optional;

import it.fai.common.enumeration.TipoDispositivoEnum;

public interface TelepassDeviceType {

  String TESSERA_VIACARD =  "TV";
  String APPARATO_TELEPASS = "AT";
  String BUONO_VIAGGIO = "BV"; //-- non esiste piu'
  String TESSERA_TELEPASS = "TT"; //-- non esiste piu'
  String PLATE = "TG";
  String DUAL = "AD";
  String REETS = "AZ"; //--> TELEPASS EUROPEO DM03
  String ARIANNA2 = "AA";


  static Optional<TipoDispositivoEnum> getTipoDispositivoByTypeOfSupport(String typeOfSupport) {
    switch (typeOfSupport) {
    case TESSERA_VIACARD:
      return Optional.of(TipoDispositivoEnum.VIACARD);
    case APPARATO_TELEPASS:
    case PLATE: //Implementato mappando device null + dati veicolo e modifica del mapper..
      return Optional.of(TipoDispositivoEnum.TELEPASS_ITALIANO);
    case ARIANNA2:
      return Optional.of(TipoDispositivoEnum.TELEPASS_EUROPEO_SAT);
    case DUAL:
    case REETS:
      return Optional.of(TipoDispositivoEnum.TELEPASS_EUROPEO);
    default:
      return Optional.empty();
    }
  }

}
