package it.fai.ms.consumi.service.jms.producer.invoice;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import it.fai.ms.common.jms.dto.document.allegatifattura.RaggruppamentoArticoliDTO;
import it.fai.ms.common.jms.fattura.GenericoAllegatoFattureMessage;
import it.fai.ms.common.jms.fattura.GenericoDTO;
import it.fai.ms.common.jms.fattura.GenericoHeaderDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.fatturazione.TemplateAllegati;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamantiRepository;
import it.fai.ms.consumi.repository.viewallegati.ViewAllegatoGenericoSezione2Entity;
import it.fai.ms.consumi.service.ViewAllegatiGenericoService;

@Component
public class GenericoInvoiceAttachment implements IGenerateInvoiceAttachment {

  private final ViewAllegatiGenericoService     service;
  private final DettaglioStanziamantiRepository dettagliRepository;

  public GenericoInvoiceAttachment(ViewAllegatiGenericoService _service, DettaglioStanziamantiRepository dettagliRepository) {
    this.service = _service;
    this.dettagliRepository = dettagliRepository;
  }

  @Override
  public Object generateDtoToAttachmentInvoice(DettaglioStanziamentoType tipoDettaglio,
                                               RequestsCreationAttachments requestCreationAttachments,
                                               final RaggruppamentoArticoliDTO raggruppamentoArticoliDTO, final TemplateAllegati template) {
    long t1 = System.currentTimeMillis();
    if (!getDettaglioStanziamentoType().equals(tipoDettaglio)) {
      return null;
    }

    log.info("Start generate message to create Allegati Fattura {} ...", tipoDettaglio);
    GenericoAllegatoFattureMessage message = new GenericoAllegatoFattureMessage(getNomeDocumento(requestCreationAttachments,
                                                                                                 raggruppamentoArticoliDTO));
    message.setHeader(createHeaderMessage(requestCreationAttachments, raggruppamentoArticoliDTO));
    message.setCodiceRaggruppamentoArticolo(requestCreationAttachments.getCodiceRaggruppamentoArticolo());

    final List<String> codiciStanziamenti = raggruppamentoArticoliDTO.getCodiciStanziamenti();
    List<UUID> dettagliStanziamentoId = dettagliRepository.findDettagliStanziamentoIdByCodiciStanziamento(codiciStanziamenti);
    List<ViewAllegatoGenericoSezione2Entity> dataAllegatiPedaggi = service.getDettagliStanziamento(dettagliStanziamentoId);
    List<GenericoDTO> body = service.createBodyMessage(dataAllegatiPedaggi, requestCreationAttachments, template);
    message.setBody(body);
    message.setLang(requestCreationAttachments.getLang()
                                              .toLowerCase());
    log.info("Finished generate message to create Allegati Fattura {} in {} ms", tipoDettaglio, System.currentTimeMillis() - t1);
    printToJsonFormat(message);
    return message;
  }

  @Override
  public DettaglioStanziamentoType getDettaglioStanziamentoType() {
    return DettaglioStanziamentoType.GENERICO;
  }

  private GenericoHeaderDTO createHeaderMessage(RequestsCreationAttachments requestCreationAttachments,
                                                RaggruppamentoArticoliDTO raggruppamentoArticoliDTO) {

    GenericoHeaderDTO header = new GenericoHeaderDTO();
    header.setNumeroFattura(requestCreationAttachments.getCodiceFattura());

    LocalDate localDateFattura = LocalDate.ofInstant(requestCreationAttachments.getDataFattura(), ZoneOffset.UTC);
    header.setDataFattura(localDateFattura);

    String codiceClienteFatturazione = raggruppamentoArticoliDTO.getCodiceClienteFatturazione();
    String ragioneSocialeClienteFatturazione = raggruppamentoArticoliDTO.getRagioneSocialeClienteFatturazione();
    String clienteHeader = StringUtils.isNoneBlank(ragioneSocialeClienteFatturazione) ? codiceClienteFatturazione + " - "
                                                                                        + ragioneSocialeClienteFatturazione
                                                                                      : codiceClienteFatturazione;
    header.setCodiceCliente(codiceClienteFatturazione);
    header.setCliente(clienteHeader);
    header.setRagioneSociale(ragioneSocialeClienteFatturazione);
    // header.setNomeIntestazioneTessere(requestCreationAttachments.getNomeIntestazioneTessere());

    header.setDettaglio(requestCreationAttachments.getDescrRaggruppamentoArticolo());
    return header;
  }

}
