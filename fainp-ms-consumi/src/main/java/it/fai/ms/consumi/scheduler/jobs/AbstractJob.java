package it.fai.ms.consumi.scheduler.jobs;

import static it.fai.ms.consumi.service.jms.producer.StanziamentiToNavJmsProducer.QUEUE;
import static java.util.stream.Collectors.groupingBy;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.jms.notification_v2.ConsumoNotReprocessableNotification;
import it.fai.ms.consumi.jms.notification_v2.ConsumoReprocessableNotification;
import it.fai.ms.consumi.repository.flussi.record.model.BaseRecord;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.FileUtils;
import it.fai.ms.consumi.repository.flussi.record.utils.RecordUtils;
import it.fai.ms.consumi.scheduler.jobs.flussi.FlussoConsumoJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.ElviaJob;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.SingleRecordProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.util.ComparatorFactoryService;
import it.fai.ms.consumi.service.util.IteratorToListConverter;
import it.fai.ms.consumi.service.validator.ConsumoNoBlockingData;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;

public abstract class AbstractJob<T extends CommonRecord, K extends Consumo> implements FlussoConsumoJob {

  protected final transient Logger _log = LoggerFactory.getLogger(getClass());

  public static final String ISO_8859_1 = "ISO-8859-1";

  private static final TransactionDefinition REQUIRES_NEW_TRANSACTION = new DefaultTransactionAttribute(TransactionDefinition.PROPAGATION_REQUIRES_NEW);

  protected String                    source;
  protected StanziamentiParamsValidator stanziamentiParamsValidator;
  protected NotificationService       notificationService;
  protected RecordPersistenceService  persistenceService;
  protected RecordDescriptor<T>       recordDescriptor;
  protected ConsumoProcessor<K>       consumoProcessor;
  protected RecordConsumoMapper<T, K> recordConsumoMapper;
  private StanziamentiToNavPublisher  stanziamentiToNavJmsProducer;
  private PlatformTransactionManager  transactionManager;

  @Value("${abstractjob.parallel.devconf.skipesperisting}")
  public boolean DEVCONF_SKIP_ESPERSISTING = false;

  @Value("${abstractjob.parallel.devconf.limtto}")
  public int DEVCONF_LIMIT_TO = -1;

  @Value("${abstractjob.parallel.devconf.skippostprocessing}")
  public boolean DEVCONF_SKIP_POSTPROCESSING = false;

//  public static boolean RUN_IN_PARALLEL_ENABLED = false;
  @Value("${abstractjob.parallel.logtracenumber}")
  public int LOG_TRACE_NUMEBER = 10000;

  @Value("${abstractjob.parallel.threadpoolsize}")
  public int THREAD_POOL_SIZE = 1;

  @PostConstruct
  public void init() {
    
    if (_log.isInfoEnabled()){
      _log.info(" *** RUN PARALLEL CONFIG ***");

      _log.info("DEVCONF_SKIP_ESPERSISTING:=[" + DEVCONF_SKIP_ESPERSISTING + "]");
      _log.info("DEVCONF_LIMIT_TO:=[" + DEVCONF_LIMIT_TO + "]");
      _log.info("DEVCONF_SKIP_POSTPROCESSING:=[" + DEVCONF_SKIP_POSTPROCESSING + "]");
      _log.info("LOG_TRACE_NUMEBER:=[" + LOG_TRACE_NUMEBER + "]");
      _log.info("THREAD_POOL_SIZE:=[" + THREAD_POOL_SIZE + "]");

      _log.info(" *** END PARALLEL CONFIG ***");
    }
    
    if(THREAD_POOL_SIZE < 1) {
      throw new IllegalArgumentException("THREAD_POOL_SIZE cannot be zero!");
    }
    if(DEVCONF_LIMIT_TO  == 0) {
      throw new IllegalArgumentException("DEVCONF_LIMIT_TO cannot be zero!");
    }
    if(LOG_TRACE_NUMEBER  == 0) {
      throw new IllegalArgumentException("LOG_TRACE_NUMEBER cannot be zero!");
    }
  }


  @Autowired
  CacheManager cacheManager;

  public AbstractJob(final String _source, final PlatformTransactionManager transactionManager,
                     final StanziamentiParamsValidator stanziamentiParamsValidator, final NotificationService notificationService,
                     final RecordPersistenceService persistenceService, final RecordDescriptor<T> recordDescriptor,
                     final ConsumoProcessor<K> consumoProcessor, final RecordConsumoMapper<T, K> recordConsumoMapper,
                     final StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {

    this.source = _source;
    this.transactionManager = transactionManager;
    this.stanziamentiParamsValidator = stanziamentiParamsValidator;
    this.notificationService = notificationService;
    this.persistenceService = persistenceService;
    this.recordDescriptor = recordDescriptor;
    this.consumoProcessor = consumoProcessor;
    this.recordConsumoMapper = recordConsumoMapper;
    this.stanziamentiToNavJmsProducer = stanziamentiToNavJmsProducer;

    _log.info("Created {} ", getClass().getSimpleName());
    _log.info(" source : {}", _source);
  }

  @Override
  public String getSource() {
    return source;
  }

  public abstract Format getJobQualifier();

  @Override
  public String process(final String _filename, final long _startTime, final Instant _ingestionTime, ServicePartner servicePartner) {

    if (!FileUtils.Exist(_filename)) {
      _log.error("File {} does not exist. cannot proceed.", _filename);
      return source;
    }
    _log.info(" processing {} {} {} {}", _filename, _startTime, _ingestionTime, servicePartner);

    allCacheEviction();

    boolean RUN_IN_PARALLEL = (THREAD_POOL_SIZE > 1) && (this instanceof ElviaJob);
    final Map<String, Stanziamento> stanziamentiDaSpedireANAV = RUN_IN_PARALLEL ? Collections.synchronizedMap(new HashMap<>()) : new HashMap<>();
    List<String> errorList = RUN_IN_PARALLEL ? Collections.synchronizedList(new ArrayList<String>()) : new ArrayList<>();
    List<ConsumoNoBlockingData> noBlockingValidationData =  RUN_IN_PARALLEL ? Collections.synchronizedList(new ArrayList<ConsumoNoBlockingData>()) : new ArrayList<>();
    try {

      if (!checkParamStanziamentiConsistency(_filename)) {
        _log.error("checkParamStanziamentiConsistency not passed (see notification service)");
        return source;
      }
      _log.info("checkParamStanziamentiConsistency ok");

      RecordsFileInfo recordsFileInfo = validateFile(_filename, _startTime, _ingestionTime);
      if (recordsFileInfo.fileIsValid()) {
        _log.info("File is valid!");

        _log.info("Start file elaboration at {}",Instant.now());
        fileElaboration(_filename, _ingestionTime, servicePartner, stanziamentiDaSpedireANAV, errorList, recordsFileInfo,
                        noBlockingValidationData,RUN_IN_PARALLEL);
        _log.info("End file elaboration at {}",Instant.now());

        _log.info("Start post elaboration...");
        postFileElaboration(_filename, _ingestionTime, servicePartner, stanziamentiDaSpedireANAV, errorList, recordsFileInfo);
        _log.info("End post elaboration...");

      } else {
        _log.error("File validation failed! Sending notification! recordsFileInfo:=[{}]", recordsFileInfo);
        recordsFileInfo.getValidationMessages().forEach(message ->
//          sendNotification(message.getCode(),_filename, message.getText())
          notificationService.notify(new ConsumoNotReprocessableNotification(
            source,
            _filename,
            message.getCode(),
            message.getText()
          ))
        );
      }
    } catch (final Exception _e) {
      _log.error("Unhandled excepion (won't be rethrown) when processing file : {}", _e);
//      sendNotification("FCJOB-500", _filename, _e.getMessage());
      notificationService.notify(new ConsumoNotReprocessableNotification(
        source,
        _filename,
        "FCJOB-500",
        _e.getMessage()
      ));
    } 

    if (!errorList.isEmpty()) {
      String messageText = String.format("error in  %s rows %s", errorList.size(), String.join("\n", errorList));
//      sendNotification("FCJOB-199", _filename, messageText);
      notificationService.notify(new ConsumoNotReprocessableNotification(
        source,
        _filename,
        "FCJOB-199",
        messageText
      ));
    }

    if(!DEVCONF_SKIP_POSTPROCESSING) {
      _log.info("Start sendComputedStanziamenti...");
      sendComputedStanziamenti(stanziamentiDaSpedireANAV);
  
      _log.info("Start checkNoBlockingValidation...");
      checkNoBlockingValidation(noBlockingValidationData, _filename, _ingestionTime != null ? _ingestionTime.toString() : "",
                                servicePartner != null ? servicePartner.toString() : "");
  
      _log.info("Start checkNoBlockingValidationOnStanziamento...");
      checkNoBlockingValidationOnStanziamento(stanziamentiDaSpedireANAV);
    }else {
      _log.warn("SKIPPING POSTPROCESSING");
    }
    
    allCacheEviction();
    return source;
  }

  public void allCacheEviction() {
    _log.info("starting allCacheEviction");
    Collection<String> cacheNames = Optional.ofNullable(cacheManager)
                                            .map(CacheManager::getCacheNames)
                                            .orElse(Collections.emptyList());
    _log.info("CacheNames: " + cacheNames);
    for (String name : cacheNames) {
      Cache cache = cacheManager.getCache(name);
      Object nativeCache = cache.getNativeCache();
      if (nativeCache instanceof com.hazelcast.map.impl.proxy.MapProxyImpl) {
        com.hazelcast.map.impl.proxy.MapProxyImpl<?, ?> ehCache = (com.hazelcast.map.impl.proxy.MapProxyImpl<?, ?>) nativeCache;
        _log.info("Cache        " + name + " stat=" + ehCache.getLocalMapStats());
        cache.clear();
        _log.info("Cache Evicted" + name + " stat=" + ehCache.getLocalMapStats());
      } else {
        cache.clear();
        _log.error("NativeCache " + name + " Evicted is " + nativeCache.getClass());
      }
    }
  }

  public void sendComputedStanziamenti(Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
    if (!stanziamentiDaSpedireANAV.isEmpty()) {
      sendStanziamentiListPaginated(stanziamentiDaSpedireANAV.values());
    }
    afterStanziamentiSent(source.toLowerCase(), stanziamentiDaSpedireANAV);
  }

  public void checkNoBlockingValidationOnStanziamento(Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
    if (!stanziamentiDaSpedireANAV.isEmpty()) {
      stanziamentiParamsValidator.checkNoBlockingValidationOnStanziamentoPaginated(stanziamentiDaSpedireANAV.keySet());
    }
  }

  public void checkNoBlockingValidation(List<ConsumoNoBlockingData> noBlockingValidationData, String fileName, String ingestion_time,
                                        String servicePartner) {
    if (!noBlockingValidationData.isEmpty()) {
      stanziamentiParamsValidator.checkNoBlockingValidationPaginated(noBlockingValidationData, fileName, ingestion_time, servicePartner);
    }
  }

  protected boolean checkParamStanziamentiConsistency(String _filename) {
    ValidationOutcome validationOutcome = stanziamentiParamsValidator.checkConsistency();
    if (!validationOutcome.isValidationOk()) {
      if (validationOutcome.getMessages() != null) {
        validationOutcome.getMessages().stream().forEach(message -> {
          if (_log.isInfoEnabled()) {
            _log.info("checkParamStanziamentiConsistency error: {} : {}", message.getCode(), message.getText());
          }
//          var messageTextKV = new HashMap<String, Object>();
//          messageTextKV.put("job_name", source);
//          messageTextKV.put("original_message", message.getText());
//          notificationService.notify("CHKPS-" + message.getCode(), messageTextKV);

          notificationService.notify(
            new ConsumoNotReprocessableNotification(
              source,
              _filename,
              "CHKPS-" + message.getCode(),
              message.getText()
            )
          );
        });
      }
      return false;
    }
    return true;
  }

  protected RecordsFileInfo validateFile(String _filename, long _startTime, Instant _ingestionTime) {
    final RecordsFileInfo recordsFileInfo = new RecordsFileInfo(_filename, _startTime, _ingestionTime);
    try {
      // vanno messe in configurazione
      long detailRecordsToRead = RecordUtils.checkFileConsistency(_filename, Charset.forName(ISO_8859_1),
                                                                  recordDescriptor.getFooterCodeBegin(),
                                                                  recordDescriptor.getStartEndPosTotalRows(),
                                                                  recordDescriptor.getLinesToSubtractToMatchDeclaration());
      recordsFileInfo.setDetailRecordsToRead(detailRecordsToRead);
    } catch (RecordUtils.NotValidFileException e) {
      recordsFileInfo.addMessage("FCJOB-500", e.getMessage());
    }
    return recordsFileInfo;
  }
  
  private ThreadPoolTaskExecutor initializeThreadPoolTaskExecutor(String _filename) {
    ThreadPoolTaskExecutor parallelExecutor = new ThreadPoolTaskExecutor();
    parallelExecutor.setCorePoolSize(THREAD_POOL_SIZE);
    parallelExecutor.setMaxPoolSize(THREAD_POOL_SIZE);
    parallelExecutor.setWaitForTasksToCompleteOnShutdown(true);
    parallelExecutor.setThreadNamePrefix(_filename+"-");
    parallelExecutor.initialize();
    return parallelExecutor;
  }

  protected void fileElaboration(String _filename, Instant _ingestionTime, ServicePartner servicePartner,
                                 Map<String, Stanziamento> stanziamentiDaSpedireANAV, List<String> errorList,
                                 RecordsFileInfo recordsFileInfo,
                                 List<ConsumoNoBlockingData> noBlockingValidationData, boolean RUN_IN_PARALLEL) throws IOException, InterruptedException, ExecutionException {

    _log.info("File elaboration status: " + "filename:=[{}], " + "ingestionTime:=[{}], " + "servicePartner:=[{}], "
               + "stanziamentiDaSpedireANAV:=[{}], " + "errorList:=[{}], " + "recordsFileInfo:=[{}]", _filename, _ingestionTime,
               servicePartner, stanziamentiDaSpedireANAV, errorList, recordsFileInfo);
    
    long START = System.currentTimeMillis();

    final Bucket bucket = Bucket.createBucket(_filename, _ingestionTime, RUN_IN_PARALLEL);
    _log.info("Ok, file {} is ok according to footer !", _filename);
    
    final ThreadPoolTaskExecutor parallelExecutor =  RUN_IN_PARALLEL ?  initializeThreadPoolTaskExecutor(_filename) : null;
    Semaphore s = new Semaphore(THREAD_POOL_SIZE);
    
    if (parallelExecutor!=null){
      _log.info("STARTING IN PARALLEL MODE");
    }

    recordDescriptor.checkConfiguration();

    AtomicLong                        detailIndex   = new AtomicLong(0);
    final AtomicReference<BaseRecord> cachePrevious = new AtomicReference<>();
    try (final Stream<String> streamLine = Files.lines(Paths.get(_filename), Charset.forName(ISO_8859_1))) {

      Stream<String> stream = streamLine.filter(line -> !line.trim().isEmpty() && !recordDescriptor.isFooterLine(line));
      if(DEVCONF_LIMIT_TO>0) stream = stream.limit(DEVCONF_LIMIT_TO);
      
      stream.forEach(line -> {
        _log.debug("START ROW ELABORATION {}", detailIndex);
        final String recordCode = recordDescriptor.extractRecordCode(line);
        if (recordDescriptor.isHeaderLine(line)) {
          var record = recordDescriptor.decodeRecordCodeAndCallSetFromString(line, recordCode, _filename, detailIndex.get());
          recordsFileInfo.setHeader(record);
          cachePrevious.set(record);
          return;
        }
        _log.debug("Processing line : {}", line);
        long indexDetail = detailIndex.incrementAndGet();

        if(parallelExecutor!=null) {
          try {
            s.acquire();
          } catch (InterruptedException e) {
            throw new RuntimeException("InterruptedException",e);
          }
          parallelExecutor.execute(new Runnable() {
            @Override
            public void run() {
              try {
                processBodyRow(line, _filename, indexDetail, recordsFileInfo, _ingestionTime, cachePrevious, servicePartner, bucket, stanziamentiDaSpedireANAV, errorList, noBlockingValidationData);
              }finally {
                s.release();
              }
            }
          });
        }else {
          processBodyRow(line, _filename, indexDetail, recordsFileInfo, _ingestionTime, cachePrevious, servicePartner, bucket, stanziamentiDaSpedireANAV, errorList, noBlockingValidationData);
        }
      });
      if(parallelExecutor!=null) {
        s.acquire(THREAD_POOL_SIZE);
        parallelExecutor.shutdown();
        s.release();
      }
      
      long DIFFTIME = (System.currentTimeMillis() - START) / 1000;
      DIFFTIME = DIFFTIME > 1 ? DIFFTIME : 1;

      _log.info("Filename : {}, Got end of all detail lines : {}", Paths.get(_filename).getFileName(),
                recordsFileInfo.getDetailRecordsToRead());
      _log.info("STATISTICS -> processed {} rows in {} minutes ({} seconds): {} records per second",detailIndex,DIFFTIME/60,DIFFTIME,detailIndex.longValue()/DIFFTIME);
    }
  }
  
  private void processBodyRow(String line, String _filename, long detailIndex, RecordsFileInfo recordsFileInfo, Instant _ingestionTime,
                       AtomicReference<BaseRecord> cachePrevious, ServicePartner servicePartner, Bucket bucket,
                       Map<String, Stanziamento> stanziamentiDaSpedireANAV, List<String> errorList,
                       List<ConsumoNoBlockingData> noBlockingValidationData) {
    Instant start = Instant.now();
    AtomicReference<T> record = new AtomicReference<>(null);
    try {
      new TransactionTemplate(transactionManager, REQUIRES_NEW_TRANSACTION).execute(new TransactionCallback<T>() {
        
        @Override
        public T doInTransaction(TransactionStatus status) {
          T rowElaboration = rowElaboration(line, _filename, detailIndex, recordsFileInfo, _ingestionTime, cachePrevious,
                                            servicePartner, bucket, stanziamentiDaSpedireANAV, errorList,
                                            noBlockingValidationData);
          record.set(rowElaboration);
          return rowElaboration;
        }
      });
    } catch (ExceptionWithRecord t) {
      _log.error("Error processing detail index {} of file {} : {} ", detailIndex, _filename, t.getMessage());
      notifyExceptionWithRecord(t);
    } catch (Throwable t) {
      _log.error("Error processing detail index {} of file {} : {} ", detailIndex, _filename, t.getMessage());
      if (record.get() != null)
        notifyExceptionWithRecord(new ExceptionWithRecord(record.get(), t));
      var exceptioWithrecordCause = getExceptionWithRecordFromCause(t);
      if (exceptioWithrecordCause != null) {
        notifyExceptionWithRecord(exceptioWithrecordCause);
      } else {
        String errorMessage = String.format("error on row index %s : %s", detailIndex, t.getMessage());
        errorList.add(errorMessage);
        if (_log.isInfoEnabled()) {
          _log.info("Detail error (stacktrace): ", t);
        }
      }
    }

    if (_log.isInfoEnabled() && (detailIndex % LOG_TRACE_NUMEBER == 0)) {
      _log.info("Processed  {} / {} detail lines of file", detailIndex, recordsFileInfo.getDetailRecordsToRead());
    }else {
      _log.debug("Processed  {} / {} detail lines of file", detailIndex, recordsFileInfo.getDetailRecordsToRead());
    }
    Instant stop = Instant.now();
    _log.debug("STOP ROW ELABORATION {} : difftime -> {}", detailIndex, Duration.between(start, stop).toNanos());
  }

  private ExceptionWithRecord getExceptionWithRecordFromCause(Throwable t) {
    Throwable cause = t.getCause();
    int deep = 1;
    while (cause != null && deep <= 10) {
      if (cause instanceof ExceptionWithRecord)
        return (ExceptionWithRecord) cause;
      cause = cause.getCause();
      deep++;
    }
    return null;
  }

  private T rowElaboration(String line, String _filename, long detailIndex, RecordsFileInfo recordsFileInfo, Instant _ingestionTime,
                           AtomicReference<BaseRecord> cachePrevious, ServicePartner servicePartner, Bucket bucket,
                           Map<String, Stanziamento> stanziamentiDaSpedireANAV, List<String> errorList,
                           List<ConsumoNoBlockingData> noBlockingValidationData) {
    // composing record from file
    final String recordCode = recordDescriptor.extractRecordCode(line);

    // skip if well known record code to be skipped
    if (recordDescriptor.skipWellKnownRecordCodeToIgnore(recordCode)) {
      _log.debug("Skipping detail line index {} because starting with known record code to ignore {}", detailIndex, recordCode);
      return null;
    }
    var record = recordDescriptor.decodeRecordCodeAndCallSetFromString(line, recordCode, _filename, detailIndex);

    try {
      record.setCreationDateInFile(recordsFileInfo.getHeader()
                                                  .getCreationDateInFile());
      record.setCreationTimeInFile(recordsFileInfo.getHeader()
                                                  .getCreationTimeInFile());
      record.setFileName(new File(_filename).getName());
      record.setIngestion_time(_ingestionTime);
      record.setServicePartner(servicePartner);

      manageHierarchy(cachePrevious, record);

      if (isRecordToBeSkipped(line, detailIndex, record)) {
        _log.debug("Skipping detail line index {}  {}", detailIndex, line);
        return record;
      }
      if (!DEVCONF_SKIP_ESPERSISTING) {
        _log.debug("Persisting on ES");
        persistenceService.persist(record);
      } else {
        _log.debug("NO Persisting on ES");
      }
      _log.debug("starting processSingleRecord {}: ",detailIndex);
      SingleRecordProcessingResult singleRecordProcessingResult = processSingleRecord(servicePartner, bucket, stanziamentiDaSpedireANAV,
                                                                                      record);
      if (singleRecordProcessingResult.getConsumoNoBlockingData() != null) {
        noBlockingValidationData.add(singleRecordProcessingResult.getConsumoNoBlockingData());
      }
    } catch (Throwable e) {
      _log.info("ERROR ON ROW ELABORATION {}", detailIndex);
      throw new ExceptionWithRecord(record, e);
    }
    _log.debug("STOP ROW ELABORATION {}", detailIndex);
    return record;
  }

  protected boolean isRecordToBeSkipped(String line, long detailIndex, T record) {
    if (record.toBeSkipped()) {
      _log.debug("Skipping detail line index {}  {}", detailIndex, line);
      return true;
    }
    return false;
  }

  private void manageHierarchy(AtomicReference<BaseRecord> cachePrevious, T record) {
    BaseRecord previousRecord = cachePrevious.get();
    int previousLevel = previousRecord == null ? -1 : previousRecord.getNestedLevel();
    int currentLevel = record.getNestedLevel();

    while (previousLevel >= currentLevel) {
      previousRecord = previousRecord.getParentRecord();
      previousLevel = previousRecord == null ? -1 : previousRecord.getNestedLevel();
      cachePrevious.set(previousRecord);
    }

    if ((previousLevel + 1) != currentLevel) {
      _log.warn("cannot nest record {} into {} level mismatch expected {} but was {}", record, previousRecord, previousLevel + 1,
                currentLevel);
    }
    record.setParentRecord(previousRecord);
    cachePrevious.set(record);

  }

  public SingleRecordProcessingResult processSingleRecord(ServicePartner servicePartner, Bucket bucket,
                                                          Map<String, Stanziamento> stanziamentiDaSpedireANAV, T record) throws Exception {
    var _recordConsumoMapper = getRecordConsumoMapperBy(record);

    // composing consumo from record
    Consumo consumo = _recordConsumoMapper.mapRecordToConsumo(record);
    consumo.setServicePartner(servicePartner);

    // composing stanziamento and dettaglio stanziamento from consumo
    ProcessingResult<K> processingResult = consumoValidateAndProcess(bucket, consumo);

    List<Stanziamento> stanziamenti = processingResult.getStanziamenti();
    stanziamenti.forEach(stanziamento -> {
      _log.debug("stanziamentiDaSpedireANAV has {} elements", stanziamentiDaSpedireANAV.size());
      stanziamentiDaSpedireANAV.put(stanziamento.getCode(), stanziamento);
    });

    return new SingleRecordProcessingResult(processingResult, consumo.getConsumoNoBlockingData());
  }

  // Use default mapper, overrides if job needs to produce multiple type of consumo
  protected RecordConsumoMapper<T, ? extends Consumo> getRecordConsumoMapperBy(T record) {
    return recordConsumoMapper;
  }

  // Use default processor, overrides if job needs to produce multiple type of consumo
  @SuppressWarnings("unchecked")
  protected ProcessingResult<K> consumoValidateAndProcess(Bucket bucket, Consumo consumo) {
    return consumoProcessor.validateAndProcess((K) consumo, bucket);
  }

  protected void postFileElaboration(String filename, Instant ingestionTime, ServicePartner servicePartner,
                                     Map<String, Stanziamento> stanziamentiDaSpedireANAV, List<String> errorList,
                                     RecordsFileInfo recordsFileInfo) throws IOException {
    _log.info("Post elaboration status: " + "filename:=[{}], " + "ingestionTime:=[{}], " + "servicePartner:=[{}], "
              + "stanziamentiDaSpedireANAV:=[{}], " + "errorList:=[{}], " + "recordsFileInfo:=[{}]", filename, ingestionTime,
              servicePartner, stanziamentiDaSpedireANAV.size(), errorList.size(), recordsFileInfo);
  }

//  protected void sendNotification(String messageCode, String filename, String messageText) {
////    var errorMessage = new HashMap<String, Object>();
////    errorMessage.put("job_name", source);
////    errorMessage.put("original_message", messageText);
////    errorMessage.put("filename", filename);
////    notificationService.notify(messageCode, errorMessage);
//    notificationService.notify(new EnrichedConsumoNotificationMessage(
//      source,
//      filename,
//      NotificationConstants.NO_ROW,
//      NotificationConstants.NO_RECORD_CODE,
//      NotificationConstants.NO_RECORD_UUID,
//      messageCode,
//      messageText
//    ));
//  }

  private void sendStanziamentiListPaginated(final Collection<Stanziamento> _stanziamentiToSend) {

    final int BATCH_STANZIAMENTO_TO_NAV_SIZE = 100;

    _log.info("Putting into queue {} StanziamentoMessage {} elements. Only missing DataInvioNav will be sent {} elements at time", QUEUE,
              _stanziamentiToSend.size(), BATCH_STANZIAMENTO_TO_NAV_SIZE);

    final var counter = new AtomicInteger(0);
    final var stanziamentiPaginated = _stanziamentiToSend.stream()
                                                         .collect(groupingBy(stanziamento -> counter.getAndIncrement()
                                                                                             / BATCH_STANZIAMENTO_TO_NAV_SIZE))
                                                         .values();

    stanziamentiPaginated.forEach(singleBath -> {

      final var stanziamentiDTO = new StanziamentoDTOMapper().mapToStanziamentoDTO(singleBath);
      stanziamentiToNavJmsProducer.publish(stanziamentiDTO);
      _log.debug("Sent stanziamento single page (item per page {}, tot pages {})", stanziamentiDTO.size(), stanziamentiPaginated.size());
    });
  }

  protected void afterStanziamentiSent(String sourceType, Map<String, Stanziamento> stanziamentiDaSpedireANAV) {
    _log.trace("Callback after stanziamenti. sourceType:=[{}],  stanziamentiDaSpedireANAV.size():=[{}]", sourceType,
               stanziamentiDaSpedireANAV == null ? 0 : stanziamentiDaSpedireANAV.size());
  }

  public List<Path> orderFileList(DirectoryStream<Path> directoryStream) {
    Comparator<Path> comparator = ComparatorFactoryService.getPathComparatorByLabel("FILENAME_ASC");
    return new IteratorToListConverter<>(directoryStream.iterator(), comparator).toOrderedList();
  }

  private void notifyExceptionWithRecord(ExceptionWithRecord e) {
    String originalMessage = null;

    var messageTextKV = new HashMap<String, Object>();
    messageTextKV.put("job_name", this.getClass().getSimpleName());
    try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {
      e.printStackTrace(pw);
//      messageTextKV.put("original_message", sw.toString());
      originalMessage = sw.toString();

    } catch (IOException e1) {
      _log.error("Error on closing String Writer", e1);
      throw new RuntimeException(e1);
    }
    CommonRecord record = e.getRecord();
//    messageTextKV.put("original_record", record);
    _log.warn("Error processing record=[{}]", record.toNotificationMessage(), e.getCause());

//    notificationService.notify("FCJOB-200", messageTextKV);
    notificationService.notify(new ConsumoReprocessableNotification(
      source,
      record.getFileName(),
      record.getRowNumber(),
      record.getClass().getSimpleName(),
      record.getUuid(),
      record.getRecordCode(),
      record.getTransactionDetail(),
      record.toNotificationMessage(),
      "FCJOB-200",
      originalMessage
    ));

  }

}

class ExceptionWithRecord extends RuntimeException {
  private static final long serialVersionUID = 1L;
  private CommonRecord      record;

  public ExceptionWithRecord(CommonRecord record, Throwable cause) {
    this(record, cause.getMessage(), cause);
  }

  public ExceptionWithRecord(CommonRecord record, String message, Throwable cause) {
    super(message, cause);
    this.record = record;
  }

  @Override
  public String getMessage() {
    if (getCause() != null)
      return getCause().getMessage();
    return super.getMessage();
  }

  public CommonRecord getRecord() {
    return record;
  }

}
