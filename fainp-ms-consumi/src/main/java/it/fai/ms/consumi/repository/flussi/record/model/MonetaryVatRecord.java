package it.fai.ms.consumi.repository.flussi.record.model;

import javax.money.MonetaryAmount;

public interface MonetaryVatRecord
  extends MonetaryBaseRecord {

  MonetaryAmount getVatAmount();

  MonetaryAmount getTotalAmountVatExcluded();
  MonetaryAmount getTotalVatAmount();
  MonetaryAmount getTotalAmountVatIncluded();
}
