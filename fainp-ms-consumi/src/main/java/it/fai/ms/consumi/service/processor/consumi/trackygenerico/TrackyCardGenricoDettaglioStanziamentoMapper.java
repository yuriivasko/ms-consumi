package it.fai.ms.consumi.service.processor.consumi.trackygenerico;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.carburante.TrackyCardCarburantiStandard;
import it.fai.ms.consumi.domain.consumi.generici.trackycard.TrackyCardGenerico;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;
import org.springframework.stereotype.Component;

@Component
public class TrackyCardGenricoDettaglioStanziamentoMapper
  extends ConsumoDettaglioStanziamentoMapper {

  public TrackyCardGenricoDettaglioStanziamentoMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(Consumo _consumo) {
    DettaglioStanziamento dettaglioStanziamentoGenerico = null;
    if (_consumo instanceof TrackyCardGenerico) {
      TrackyCardGenerico trkCardGen = (TrackyCardGenerico) _consumo;
      dettaglioStanziamentoGenerico = toDettaglioStanziamentoGenerico(trkCardGen, trkCardGen.getGlobalIdentifier());
    }
    return dettaglioStanziamentoGenerico;
  }

}
