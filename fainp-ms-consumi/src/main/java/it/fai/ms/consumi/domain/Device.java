package it.fai.ms.consumi.domain;

import java.io.Serializable;
import java.util.Objects;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;

public class Device implements Serializable{
  private static final long serialVersionUID = -8805565747750659505L;

  private String       contractNumber;
  private String       vehicleUUID;
  private String       seriale;
  private String       pan;
  private TipoDispositivoEnum       type;
  private TipoServizioEnum serviceType;
  private String       deviceUUID;
  
  private boolean       virtuale = false;

  public void setServiceType(TipoServizioEnum serviceType) {
    this.serviceType = serviceType;
  }

  public Device(final TipoDispositivoEnum _deviceType) {
    type = Objects.requireNonNull(_deviceType, "Parameter deviceType is mandatory");
  }

  public Device(final String _seriale, final TipoDispositivoEnum _deviceType) {
    seriale = Objects.requireNonNull(_seriale, "Parameter seriale is mandatory");
    type = Objects.requireNonNull(_deviceType, "Parameter deviceType is mandatory");
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Device obj = getClass().cast(_obj);
      res = Objects.equals(obj.seriale, seriale) && Objects.equals(obj.type, type) && Objects.equals(obj.pan, pan)
            && Objects.equals(obj.contractNumber, contractNumber);
    }
    return res;
  }

  public String getContractNumber() {
    return contractNumber;
  }

  public String getId() {
    return seriale;
  }

  public String getSeriale() {
    return seriale;
  }

  public void setSeriale(String seriale) {
    this.seriale = seriale;
  }

  public String getPan() {
    return pan;
  }

  public TipoDispositivoEnum getType() {
    return type;
  }
  
  public boolean isVirtuale() {
    return virtuale;
  }

  @Override
  public int hashCode() {
    return Objects.hash(seriale, type, pan, contractNumber);
  }

  public void setContractNumber(final String _contractId) {
    contractNumber = _contractId;
  }

  public void setPan(final String _pan) {
    pan = _pan;
  }

  public void setType(TipoDispositivoEnum type) {
    this.type = type;
  }

  public TipoServizioEnum getServiceType() {
    return serviceType;
  }

  public String getVehicleUUID() {
    return vehicleUUID;
  }

  public void setVehicleUUID(String vehicleUUID) {
    this.vehicleUUID = vehicleUUID;
  }

  public String getDeviceUUID() {
    return deviceUUID;
  }

  public void setDeviceUUID(String deviceUUID) {
    this.deviceUUID = deviceUUID;
  }
  
  public void setVirtuale(boolean virtuale) {
    this.virtuale = virtuale;
  }

  @Override
  public String toString() {
    return "Device [contractId=" + contractNumber + ", seriale=" + seriale + ", pan=" + pan + ", type=" + type + ", serviceType=" + serviceType + ", vehicleUUID=" + vehicleUUID + ", virtuale=" + virtuale
           + "]";
  }

}
