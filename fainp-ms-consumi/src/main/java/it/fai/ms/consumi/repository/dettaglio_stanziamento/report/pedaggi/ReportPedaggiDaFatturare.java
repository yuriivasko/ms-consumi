package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi;

import static it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric_.CURRENCY_CODE;
import static it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric_.CUSTOMER_ID;
import static it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric_.EXCHANGE_RATE;
import static it.fai.ms.consumi.repository.dettaglio_stanziamento.DettaglioStanziamentoEntityGeneric_.RECORD_CODE;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.pedaggio.DettaglioStanziamentoPedaggioEntity_;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.ReportWidgetHomepage;
import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;
import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity_;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity_;
import it.fai.ms.consumi.util.SqlUtil;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
public class ReportPedaggiDaFatturare extends ReportWidgetHomepage {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private EntityManager em;

	public ReportPedaggiDaFatturare(EntityManager em) {
		this.em = em;
	}

	public List<AllegatiConfigurationEntity> getRaggruppamentiByAllegatiConfigurationAndDetails(String codClienteNav) {
		if (StringUtils.isBlank(codClienteNav)) {
			return new ArrayList<>();
		}
		List<String> articlesCode = getRaggruppamentoArticoliPedaggiDaFatturare(codClienteNav);
		if (articlesCode == null || articlesCode.isEmpty()) {
			log.warn("Not found articles code in ViewReportConsumiPedaggi by ClientCode: {}", codClienteNav);
			return new ArrayList<>();
		}

		var cb = em.getCriteriaBuilder();
		var query = em.getCriteriaBuilder().createQuery(AllegatiConfigurationEntity.class);
		Root<AllegatiConfigurationEntity> root = query.from(AllegatiConfigurationEntity.class);

		query.select(root);
		Predicate pCodeRaggruppamentoIn = root.get(AllegatiConfigurationEntity_.CODICE_RAGGRUPPAMENTO_ARTICOLO)
				.in(articlesCode);
		query.where(pCodeRaggruppamentoIn);

		Order orderByArticleCode = cb.asc(root.get(AllegatiConfigurationEntity_.CODICE_RAGGRUPPAMENTO_ARTICOLO));
		query.orderBy(orderByArticleCode);

		return em.createQuery(query).getResultList();
	}

	private List<String> getRaggruppamentoArticoliPedaggiDaFatturare(String codClienteNav) {
		// @formatted:off
		String nativeQueryStr = "" + " SELECT DISTINCT raggruppamento_articoli_nav "
				+ " FROM view_all_dettaglio_stanziamento_pedaggi " + " WHERE " + " codice_cliente_nav = '"
				+ codClienteNav + "'" + " ORDER BY raggruppamento_articoli_nav;";
		// @formatted:on

		Query query = em.createNativeQuery(nativeQueryStr);
		List<String> articlesGroup = (List<String>) query.getResultList();
		log.debug("Found articles Group: {} by codice cliente: {}", articlesGroup, codClienteNav);
		return articlesGroup;
	}

	public List<DispositiviDto> getDispositiviPedaggio(String codClienteNav, String raggruppamentoArticoliNav,
			Boolean invoiced, Boolean notInvoiced, LocalDate consumptionDateFrom, LocalDate consumptionDateTo,
			LocalDate invoiceDateFrom, LocalDate invoiceDateTo) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		var query = cb.createQuery(DispositiviDto.class);
		var root = query.from(ViewReportConsumiPedaggiEntity.class);

		query.multiselect(root.get(ViewReportConsumiPedaggiEntity_.CUSTOMER_ID),
				root.get(ViewReportConsumiPedaggiEntity_.ARTICLES_GROUP),
				root.get(ViewReportConsumiPedaggiEntity_.VEICOLO_TARGA),
				root.get(ViewReportConsumiPedaggiEntity_.VEICOLO_NAZIONE_TARGA),
				root.get(ViewReportConsumiPedaggiEntity_.DEVICE_TYPE),
				root.get(ViewReportConsumiPedaggiEntity_.DEVICE_OBU));

		List<Predicate> predicates = new ArrayList<>();
		predicates.add(cb.equal(root.get(ViewReportConsumiPedaggiEntity_.CUSTOMER_ID), codClienteNav));
		predicates.add(cb.equal(root.get(ViewReportConsumiPedaggiEntity_.ARTICLES_GROUP), raggruppamentoArticoliNav));
		if (invoiced && !notInvoiced) {
			predicates.add(cb.isNotNull(root.get(ViewReportConsumiPedaggiEntity_.NUMERO_FATTURA)));
		}
		if (notInvoiced && !invoiced) {
			predicates.add(cb.isNull(root.get(ViewReportConsumiPedaggiEntity_.NUMERO_FATTURA)));
		}
		if (invoiceDateFrom != null && invoiceDateTo != null) {
			Instant invoiceInstantFrom = invoiceDateFrom.atStartOfDay().toInstant(ZoneOffset.UTC);
			// in order to include all the minutes in the days, select the next days and
			// subtracts 1 sec
			Instant invoiceInstantTo = invoiceDateTo.plus(1l, ChronoUnit.DAYS).atStartOfDay()
					.minus(1, ChronoUnit.SECONDS).toInstant(ZoneOffset.UTC);
			predicates.add(cb.between(root.get(ViewReportConsumiPedaggiEntity_.DATA_FATTURA), invoiceInstantFrom,
					invoiceInstantTo));
		}
		if (consumptionDateFrom != null && consumptionDateTo != null) {
			predicates.add(cb.between(root.get(ViewReportConsumiPedaggiEntity_.DATA_EROGAZIONE_SERVIZIO),
					consumptionDateFrom, consumptionDateTo));
		}
		query.where(cb.and(predicates.toArray(new Predicate[0])

		));

		query.groupBy(Arrays.asList(root.get(ViewReportConsumiPedaggiEntity_.CUSTOMER_ID),
				root.get(ViewReportConsumiPedaggiEntity_.ARTICLES_GROUP),
				root.get(ViewReportConsumiPedaggiEntity_.VEICOLO_TARGA),
				root.get(ViewReportConsumiPedaggiEntity_.VEICOLO_NAZIONE_TARGA),
				root.get(ViewReportConsumiPedaggiEntity_.DEVICE_OBU),
				root.get(ViewReportConsumiPedaggiEntity_.DEVICE_TYPE)));

		List<DispositiviDto> resultList = em.createQuery(query).getResultList();

		// set id for frontend
		AtomicLong l = new AtomicLong(0);
		resultList.stream().forEach(d -> d.setId(l.getAndIncrement()));
		return resultList;
	}

	public List<PadaggiDaFatturareCSVDto> getDetailPedaggiNonFatturatiCSV(String codClienteNav,
			List<DispositiviDto> data, Instant startDate, Instant endDate) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		var query = cb.createQuery(PadaggiDaFatturareCSVDto.class);
		var dettaglio = query.from(DettaglioStanziamentoPedaggioEntity.class);

		// var stanziamento = query.from(StanziamentoEntity.class);

		query.multiselect(dettaglio.get(RECORD_CODE), // Tipo record
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.SOURCE_ROW_NUMBER), // Contatore riga
				cb.literal(""), // Tipo movimento
				dettaglio.get(CUSTOMER_ID), // Codice utente principale
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.DEVICE_TYPE), // Tipo supporto principale
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.DEVICE_OBU), // Tessera/Supporto principale
				cb.literal(""), // Riservato/Libero
				cb.literal(""), // Codice utente secondario
				cb.literal(""), // Tipo supporto secondario
				cb.literal(""), // Tessera/Supporto secondario
				cb.literal(""), // Riservato/Libero
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.ENTRY_TIMESTAMP), // Data ingresso
				// Ora ingresso
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.EXIT_TIMESTAMP), // Data uscita
				// Ora uscita
				cb.literal(""), // Codice autostrada
				cb.literal(DettaglioStanziamentoPedaggioEntity_.TRATTA), // Descrizione autostrada
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.ENTRY_GLOBAL_GATE_IDENTIFIER_DESCRIPTION), // Descrizione
																												// casello
																												// ingresso
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.ENTRY_GLOBAL_GATE_IDENTIFIER), // Codice
																									// casello
																									// ingresso
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.EXIT_GLOBAL_GATE_IDENTIFIER_DESCRIPTION), // Descrizione
																												// casello
																												// uscita
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.EXIT_GLOBAL_GATE_IDENTIFIER), // Codice casello
																									// uscita
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.AMOUNT_EXCLUDED_VAT), // Importo esente I.V.A.
																							// (imponibile)
				dettaglio.get(CURRENCY_CODE), // Valuta
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.AMOUNT_INCLUDED_VAT), // Importo esente I.V.A.
																							// (imponibile)
				dettaglio.get(CURRENCY_CODE), // Valuta
				cb.literal(""), // Sconto/Maggiorazione esente I.V.A. (imponibile)
				dettaglio.get(CURRENCY_CODE), // Valuta
				cb.literal(""), // Sconto/Maggiorazione lordo soggetto I.V.A. (imponibile + imposta)
				dettaglio.get(CURRENCY_CODE), // Valuta
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.AMOUNT_VAT_RATE), // Aliquota I.V.A.
				dettaglio.get(EXCHANGE_RATE), // Cambio
				dettaglio.get(CURRENCY_CODE), // Valuta
				cb.literal(""), // Classe del veicolo principale
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.VEHICLE_LICENSE_COUNTRY_ID), // Codice nazione
																								// targa
				// principale
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.VEHICLE_LICENSE_LICENSE_ID), // Targa
																								// principale
				dettaglio.get(DettaglioStanziamentoPedaggioEntity_.VEHICLE_EURO_CLASS), // Classificazione Euro
																						// principale
				cb.literal(""), // Codice nazione targa secondaria
				cb.literal(""), // Targa secondaria
				cb.literal(""), // Classificazione Euro secondaria
				cb.literal(""), // Codice autorizzazione/Numero transazione
				cb.literal("") // Riservato/Libero

		// non usati
		// FILE_NAME
		// TRANSACTION_DETAIL_CODE
		// PRICING_CLASS
		// DOCUMENTO_DA_FORNITORE
		// IMPONIBILE_ADR
		// DATA_ACQUISIZIONE_FLUSSO
		// RAGGRUPPAMENTO_ARTICOLI_NAV
		// INVOICE_TYPE
		// RECORD_CODE??????????????????????
		// NETWORD_CODE
		// CODICE_FORNITORE_LEGACY
		// CODICE_PARTNER
		// DEVICE_TYPE
		// PAN_NUMBER
		// CODICE_FORNITORE_NAV
		// PROCESSED
		// TOTAL_AMOUNT
		// SIGN_OF_TRANSACTION
		// INGESTION_TIME
		// SOURCE_TYPE
		// TRATTA
		// REGION
		// REGION
		);
		query.where(cb.and(
				// filterDispositivi(codClienteNav, data, cb, dettaglio),
				nonFatturato(cb, query, dettaglio), filterDate(startDate, endDate, cb, dettaglio)));

		List<PadaggiDaFatturareCSVDto> resultList = em.createQuery(query).getResultList();

		return resultList;
	}

	public List<ViewReportConsumiPedaggiEntity> queryDetailPedaggi(String codClienteNav,
			List<it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto> dataAll, Boolean invoiced,
			Boolean notInvoiced, LocalDate consumptionDateFrom, LocalDate consumptionDateTo, LocalDate invoiceDateFrom,
			LocalDate invoiceDateTo) {

		List<List<it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto>> splittedData = SqlUtil.splitToSubList(dataAll,250);
		
		List<ViewReportConsumiPedaggiEntity> rsAll = new ArrayList<>();

		for (List<it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto> data : splittedData) {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			var query = cb.createQuery(ViewReportConsumiPedaggiEntity.class);
			var view = query.from(ViewReportConsumiPedaggiEntity.class);

			List<Predicate> where = new ArrayList<>();
			where.add(filterDispositivi(codClienteNav, data, cb, view));
			where.addAll(fillFilters(invoiced, notInvoiced, consumptionDateFrom, consumptionDateTo, invoiceDateFrom, invoiceDateTo, cb, view));
			query.where(where.toArray(new Predicate[0]));
			query.orderBy(cb.asc(view.get(ViewReportConsumiPedaggiEntity_.EXIT_TIMESTAMP)));

			log.debug("Starting queryDetailPedaggi for codCli {} with {} dispositivi", codClienteNav, data.size());
			List<ViewReportConsumiPedaggiEntity> rs = em.createQuery(query).getResultList();
			log.debug("Founded pedaggi {} for codCli {} ", rs.size(), codClienteNav);

			rsAll.addAll(rs);
		}
		
		return rsAll;

	}

	public List<PedaggiDaFatturareDto> getDetailPedaggi(String codClienteNav,
			List<it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto> data, Boolean invoiced,
			Boolean notInvoiced, LocalDate consumptionDateFrom, LocalDate consumptionDateTo, LocalDate invoiceDateFrom,
			LocalDate invoiceDateTo) {

		return queryDetailPedaggi(codClienteNav, data, invoiced, notInvoiced, consumptionDateFrom, consumptionDateTo,
				invoiceDateFrom, invoiceDateTo)
						.stream()
						.map(entity -> new PedaggiDaFatturareDto(entity.getExitTimestamp(),
								entity.getEntryGlobalGateIdentifierDescription(),
								entity.getExitGlobalGateIdentifierDescription(), entity.getAmountExcludedVat(),
								entity.getCurrencyCode(), entity.getDeviceObu()))
						.collect(Collectors.toList());
	}

	private List<Predicate> fillFilters(Boolean invoiced, Boolean notInvoiced, LocalDate consumptionDateFrom,
			LocalDate consumptionDateTo, LocalDate invoiceDateFrom, LocalDate invoiceDateTo, CriteriaBuilder cb,
			Root<ViewReportConsumiPedaggiEntity> root) {
		List<Predicate> predicates = new ArrayList<>();
		if (invoiced && !notInvoiced) {
			predicates.add(cb.isNotNull(root.get(ViewReportConsumiPedaggiEntity_.NUMERO_FATTURA)));
		}
		if (notInvoiced && !invoiced) {
			predicates.add(cb.isNull(root.get(ViewReportConsumiPedaggiEntity_.NUMERO_FATTURA)));
		}

		if (consumptionDateFrom != null && consumptionDateTo != null) {
			predicates.add(cb.between(root.get(ViewReportConsumiPedaggiEntity_.DATA_EROGAZIONE_SERVIZIO),
					consumptionDateFrom, consumptionDateTo));
		}
		if (invoiceDateFrom != null && invoiceDateTo != null) {
			Instant invoiceInstantFrom = invoiceDateFrom.atStartOfDay().toInstant(ZoneOffset.UTC);
			// in order to include all the minutes in the days, select the next days and
			// subtracts 1 sec
			Instant invoiceInstantTo = invoiceDateTo.plus(1l, ChronoUnit.DAYS).atStartOfDay()
					.minus(1, ChronoUnit.SECONDS).toInstant(ZoneOffset.UTC);
			predicates.add(cb.between(root.get(ViewReportConsumiPedaggiEntity_.DATA_FATTURA), invoiceInstantFrom,
					invoiceInstantTo));
		}
		return predicates;
	}

	private Predicate filterDispositivi(String codClienteNav,
			List<it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto> dataList,
			CriteriaBuilder cb, Root<ViewReportConsumiPedaggiEntity> dettaglio) {
		Predicate or = cb.isTrue(cb.literal(false));
		for (it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto data : dataList) {

			Predicate predicate = cb.and(
					cb.equal(dettaglio.get(ViewReportConsumiPedaggiEntity_.CUSTOMER_ID), codClienteNav),
					cb.equal(dettaglio.get(ViewReportConsumiPedaggiEntity_.ARTICLES_GROUP),
							data.getRaggruppamentoArticoliNav()),
					cb.equal(dettaglio.get(ViewReportConsumiPedaggiEntity_.VEICOLO_TARGA), data.getLicensePlate()),
					cb.equal(dettaglio.get(ViewReportConsumiPedaggiEntity_.VEICOLO_NAZIONE_TARGA),
							data.getLicensePlateCountry()));

			// FIXME ID325 - inclusione dispositivi con obuId a null. da verificare
			if (data.getObu() != null)
				predicate = cb.and(predicate,
						cb.equal(dettaglio.get(ViewReportConsumiPedaggiEntity_.DEVICE_OBU), data.getObu()));
			else
				predicate = cb.and(predicate, cb.isNull(dettaglio.get(ViewReportConsumiPedaggiEntity_.DEVICE_OBU)));

			if (data.getDeviceType() != null)
				predicate = cb.and(predicate,
						cb.equal(dettaglio.get(ViewReportConsumiPedaggiEntity_.DEVICE_TYPE), data.getDeviceType()));
			else
				predicate = cb.and(predicate, cb.isNull(dettaglio.get(ViewReportConsumiPedaggiEntity_.DEVICE_TYPE)));
			or = cb.or(or, predicate);
		}
		return or;

	}

	private Predicate filterDate(Instant startDate, Instant endDate, CriteriaBuilder cb,
			Root<DettaglioStanziamentoPedaggioEntity> dettaglio) {

		Predicate predicate = cb.isTrue(cb.literal(true));
		if (startDate != null)
			predicate = cb.and(predicate,
					cb.greaterThan(dettaglio.get(DettaglioStanziamentoPedaggioEntity_.EXIT_TIMESTAMP), startDate));
		if (endDate != null)
			predicate = cb.and(predicate,
					cb.lessThan(dettaglio.get(DettaglioStanziamentoPedaggioEntity_.EXIT_TIMESTAMP), endDate));
		return predicate;
	}

	private Predicate nonFatturato(CriteriaBuilder cb, CriteriaQuery<?> query,
			Root<DettaglioStanziamentoPedaggioEntity> root) {

		Subquery<StanziamentoEntity> subquery = query.subquery(StanziamentoEntity.class);
		Root<StanziamentoEntity> stanziamento = subquery.from(StanziamentoEntity.class);

		Path<Collection<DettaglioStanziamentoPedaggioEntity>> dettagli = stanziamento
				.get(StanziamentoEntity_.DETTAGLIO_STANZIAMENTI);
		Predicate nonfatturati = cb.not(cb.exists(
				subquery.select(stanziamento.get(StanziamentoEntity_.CODE)).where(cb.and(cb.isMember(root, dettagli),
						cb.isNotNull(stanziamento.get(StanziamentoEntity_.NUMERO_FATTURA))))));
		return nonfatturati;
	}

	public List<String> getTotalTollToInvoice(String codiceCliente, Set<String> article) {
		return getTotalToInvoice(em, codiceCliente, article);
	}

}
