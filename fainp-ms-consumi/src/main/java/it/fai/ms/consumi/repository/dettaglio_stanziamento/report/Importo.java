package it.fai.ms.consumi.repository.dettaglio_stanziamento.report;

import java.math.BigDecimal;

public class Importo{
	private BigDecimal valore;
	private String valuta;
	public Importo(BigDecimal valore, String valuta) {
		super();
		this.setValore(valore);
		this.setValuta(valuta);
	} 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getValore() == null) ? 0 : getValore().hashCode());
		result = prime * result + ((getValuta() == null) ? 0 : getValuta().hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Importo other = (Importo) obj;
		if (getValore() == null) {
			if (other.getValore() != null)
				return false;
		} else if (!getValore().equals(other.getValore()))
			return false;
		if (getValuta() == null) {
			if (other.getValuta() != null)
				return false;
		} else if (!getValuta().equals(other.getValuta()))
			return false;
		return true;
	}
	public BigDecimal getValore() {
		return valore;
	}
	public void setValore(BigDecimal valore) {
		this.valore = valore;
	}
	public String getValuta() {
		return valuta;
	}
	public void setValuta(String valuta) {
		this.valuta = valuta;
	}
	
}