package it.fai.ms.consumi.config;

import static io.github.jhipster.config.JHipsterConstants.SPRING_PROFILE_NO_LIQUIBASE;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import io.github.jhipster.config.liquibase.AsyncSpringLiquibase;
import liquibase.integration.spring.SpringLiquibase;

@Configuration
@EnableConfigurationProperties({ LiquibaseProperties.class })
@EnableJpaRepositories({ "it.fai.ms.consumi.repository", "it.fai.common.notification.repository" })
@EntityScan(basePackages = { "it.fai.ms.consumi", "it.fai.common.notification.domain" })
@EnableTransactionManagement
public class DatabaseConfiguration {

  private final Logger _log = LoggerFactory.getLogger(DatabaseConfiguration.class);

  private final Environment env;

  @Inject
  public DatabaseConfiguration(final Environment _env) {
    env = _env;
  }

  @Bean
  @Primary
  public LiquibaseProperties liquibaseProperties() {
    final var liquibaseProperties = new LiquibaseProperties();
    liquibaseProperties.setChangeLog("classpath:/config/liquibase/master.xml");
    _log.info("Liquibase properties 'changelog'     : {}", liquibaseProperties.getChangeLog());
    _log.info("Liquibase properties 'contexts'      : {}", liquibaseProperties.getContexts());
    _log.info("Liquibase properties 'def schema'    : {}", liquibaseProperties.getDefaultSchema());
    _log.info("Liquibase properties 'labels'        : {}", liquibaseProperties.getLabels());
    _log.info("Liquibase properties 'parameters'    : {}", liquibaseProperties.getParameters());
    _log.info("Liquibase properties 'user'          : {}", liquibaseProperties.getUser());
    _log.info("Liquibase properties 'password'      : {}", liquibaseProperties.getPassword());
    _log.info("Liquibase properties 'url'           : {}", liquibaseProperties.getUrl());
    _log.info("Liquibase properties 'rollback file' : {}", liquibaseProperties.getRollbackFile());
    return liquibaseProperties;
  }

  @Bean
  @DependsOn(value = "entityManagerFactory")
  @Primary
  public SpringLiquibase liquibase(@Qualifier("taskExecutor") final TaskExecutor _taskExecutor, final DataSource _dataSource,
                                   final LiquibaseProperties _liquibaseProperties,
                                   @Value("#{new Boolean('${liquibase.enabled:false}')}") final boolean _liquibaseEnabled) {

    final var liquibase = new AsyncSpringLiquibase(_taskExecutor, env);
    liquibase.setDataSource(_dataSource);
    liquibase.setChangeLog(_liquibaseProperties.getChangeLog());
    liquibase.setContexts(_liquibaseProperties.getContexts());
    liquibase.setDefaultSchema(_liquibaseProperties.getDefaultSchema());
    liquibase.setDropFirst(_liquibaseProperties.isDropFirst());
    liquibase.setLabels(_liquibaseProperties.getLabels());
    liquibase.setChangeLogParameters(_liquibaseProperties.getParameters());
    if (env.acceptsProfiles(SPRING_PROFILE_NO_LIQUIBASE)) {
      _log.info("Liquibase disabled by spring profile {}", SPRING_PROFILE_NO_LIQUIBASE);
      liquibase.setShouldRun(false);
    } else {
      _log.info("Liquibase property 'liquibase.enabled' : {}", _liquibaseEnabled);
      liquibase.setShouldRun(_liquibaseEnabled);
    }
    return liquibase;
  }
}
