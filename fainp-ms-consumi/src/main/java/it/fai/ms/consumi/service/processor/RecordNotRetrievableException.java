package it.fai.ms.consumi.service.processor;

public class RecordNotRetrievableException extends RuntimeException {

  public static final String RECORD_NOT_RETRIEVED = "Record with type:=[$type] and uuid:=[$uuid] not found on elastic ElasticSearch";

  public RecordNotRetrievableException(String recordType, String uuid) {
    super(composeMessage(RECORD_NOT_RETRIEVED, recordType, uuid));
  }

  public RecordNotRetrievableException(String recordType, String uuid, Throwable t) {
    super(composeMessage(RECORD_NOT_RETRIEVED, recordType, uuid), t);
  }

  private static String composeMessage(String template, String recordType, String uuid) {
    return template.replace("$type", recordType).replace("$uuid", uuid);
  }

}

