package it.fai.ms.consumi.repository.viewdettaglistanziamenti;

import static java.util.stream.Collectors.toSet;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Repository
@Validated
@Transactional
public class ViewDettaglioStanziamentiCounterRepository {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final EntityManager                     em;

  @Inject
  public ViewDettaglioStanziamentiCounterRepository(final EntityManager _entityManager) {
    em = _entityManager;
  }

  public Set<ViewDettaglioStanziamentiCounter> findDettaglioStanziamentiCounterGreaterThan1ByCodiceStanzimento(String codiceStanziamento) {    
    
    final var criteriaBuilder = em.getCriteriaBuilder();
    final var query = criteriaBuilder.createQuery(ViewDettaglioStanziamentiCounter.class);
    final Root<ViewDettaglioStanziamentiCounter> root = query.from(ViewDettaglioStanziamentiCounter.class);
    query.select(root);

    if(_log.isDebugEnabled())
      _log.debug(" Searching {} by codiceStanziamento : {}", ViewDettaglioStanziamentiCounter.class.getSimpleName(), codiceStanziamento);

    final var predicates = List.of(criteriaBuilder.equal(root.get(ViewDettaglioStanziamentiCounter_.CODICE_STANZIAMENTO), codiceStanziamento),
                                   criteriaBuilder.greaterThan(root.get(ViewDettaglioStanziamentiCounter_.COUNTER), 1))
                               .toArray(new Predicate[0]);
    query.where(predicates);

    return em.createQuery(query)
                          .getResultStream()
                          .collect(toSet());
  }

}
