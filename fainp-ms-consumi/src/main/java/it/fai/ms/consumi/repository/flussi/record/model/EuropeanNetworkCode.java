package it.fai.ms.consumi.repository.flussi.record.model;

public interface EuropeanNetworkCode {
  
  String getEuropeanNetworkCode();

}
