package it.fai.ms.consumi.repository.parametri_stanziamenti.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class StanziamentiParamsSupplierEntity {

  @Column(name = "codice_fornitore", nullable = false)
  @NotNull
  private final String code;

  @Column(name = "codice_fornitore_legacy")
  private String legacyCode;

  protected StanziamentiParamsSupplierEntity() {
    code = null;
  }

  public StanziamentiParamsSupplierEntity(final String _code) {
    code = _code;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final StanziamentiParamsSupplierEntity obj = getClass().cast(_obj);
      res = Objects.equals(obj.getCode(), getCode());
    }
    return res;
  }

  public String getCode() {
    return code;
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCode());
  }

  public String getLegacyCode() {
    return legacyCode;
  }

  public void setLegacyCode(final String _legacyCode) {
    legacyCode = _legacyCode;
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("code=")
                              .append(code)
                              .append(",legacyCode=")
                              .append(legacyCode)
                              .append("]")
                              .toString();
  }

}
