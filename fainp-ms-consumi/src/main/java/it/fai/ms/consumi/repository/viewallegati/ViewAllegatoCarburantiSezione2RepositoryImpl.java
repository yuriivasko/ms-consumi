package it.fai.ms.consumi.repository.viewallegati;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ViewAllegatoCarburantiSezione2RepositoryImpl implements ViewAllegatoCarburantiSezione2Repository {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final EntityManager em;

  public ViewAllegatoCarburantiSezione2RepositoryImpl(final EntityManager _em) {
    em = _em;
  }

  @Override
  public List<ViewAllegatoCarburantiSezione2Entity> findByDettaglioStanziamentoId(List<UUID> dettagliStanziamentoId) {
    final var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ViewAllegatoCarburantiSezione2Entity.class);
    Root<ViewAllegatoCarburantiSezione2Entity> root = cq.from(ViewAllegatoCarburantiSezione2Entity.class);

    cq.select(root);
    Predicate pId = root.get(ViewAllegatoPedaggioSezione2Entity_.ID)
                        .in(dettagliStanziamentoId);
    cq.where(pId);

    List<Order> orderList = new ArrayList<>();
    orderList.add(cb.asc(root.get(ViewAllegatoCarburantiSezione2Entity_.CODICE_CONTRATTO)));
    orderList.add(cb.asc(root.get(ViewAllegatoCarburantiSezione2Entity_.TIPO_DISPOSITIVO)));
    orderList.add(cb.asc(root.get(ViewAllegatoCarburantiSezione2Entity_.SERIAL_NUMBER)));

    cq.orderBy(orderList);

    TypedQuery<ViewAllegatoCarburantiSezione2Entity> query = em.createQuery(cq);
    logQuery(query);
    return query.getResultList();
  }

  private void logQuery(TypedQuery<?> query) {
    log.info("Query: {}", query.unwrap(Query.class)
                               .getQueryString());
  }

}
