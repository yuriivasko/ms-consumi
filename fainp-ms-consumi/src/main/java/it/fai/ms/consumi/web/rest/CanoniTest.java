package it.fai.ms.consumi.web.rest;

import java.time.Instant;
import java.time.ZoneId;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.service.canoni.CanoniService;
import it.fai.ms.consumi.service.canoni.DispositivoForCanoni;
import it.fai.ms.consumi.service.report.ReportCsv;

@RestController
@RequestMapping(CanoniTest.BASE_PATH)
public class CanoniTest {
  private static final Logger log       = LoggerFactory.getLogger(CanoniTest.class);
  public final static String  BASE_PATH = "api/canoniTest";

  private final CanoniService canoniService;
  private final ReportCsv     reportCsv;

  public CanoniTest(CanoniService canoniService, ReportCsv reportCsv) {
    this.canoniService = canoniService;
    this.reportCsv     = reportCsv;
  }

  @GetMapping("/canoniDispositivo")
  public String getCanoniDispositivi(@ApiParam(value = "dataDa es: 2007-12-03T10:15:30.00Z.", required = true) @RequestParam String dataDaString,@ApiParam(value = "dataA es: 2007-12-03T10:15:30.00Z.", required = true) @RequestParam String dataAString, TipoDispositivoEnum tipoDispositivo,
                                     String codiceCliente) {

    Instant                      dataDa            = Instant.parse(dataDaString);
    Instant                      dataA             = Instant.parse(dataAString);
    Stream<DispositivoForCanoni> canoniDispositivi = canoniService.canoniDispositivi(dataDa, dataA, tipoDispositivo,
                                                                                     codiceCliente);
    Locale                       locale            = Locale.getDefault();
    ZoneId                       zoneId            = ZoneId.systemDefault();
    String                       csv               = reportCsv
      .toCsv(DispositivoForCanoni.class, canoniDispositivi.collect(Collectors.toList()), locale, zoneId);
    log.warn("Canoni dispositivi result \n {}", csv);
    return csv;
  }

  @GetMapping("/canoniServizio")
  public String getCanoniServizi(@ApiParam(value = "dataDa es: 2007-12-03T10:15:30.00Z.", required = true) @RequestParam String dataDaString,@ApiParam(value = "dataA es: 2007-12-03T10:15:30.00Z.", required = true) @RequestParam String dataAString, TipoServizioEnum tipoServizio, String codiceCliente) {
    Instant                      dataDa            = Instant.parse(dataDaString);
    Instant                      dataA             = Instant.parse(dataAString);
    Stream<DispositivoForCanoni> canoniDispositivi = canoniService.canoniServizi(dataDa, dataA, tipoServizio, codiceCliente);
    Locale                       locale            = Locale.getDefault();
    ZoneId                       zoneId            = ZoneId.systemDefault();
    String                       csv               = reportCsv
      .toCsv(DispositivoForCanoni.class, canoniDispositivi.collect(Collectors.toList()), locale, zoneId);
    log.warn("Canoni servizi result \n {}", csv);
    return csv;
  }

}
