package it.fai.ms.consumi.repository.flussi.record.model.stcon;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

public class StconRecord extends CommonRecord {

  private static final long serialVersionUID = 1L;

  private String recordCode;
  private String gateIdentifier;
  private String motorways;
  private String gateNumber;
  private String laneCode;
  private String description;
  private String endDate;
  private String mainTypeOfGate;
  private String subTypeOfGate;
  private String distance;
  private String globalGateIdentifier;

  @JsonCreator
  public StconRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
  }

  public String getRecordCode() {
    return recordCode;
  }

  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }

  public String getGateIdentifier() {
    return gateIdentifier;
  }

  public void setGateIdentifier(String gateIdentifier) {
    this.gateIdentifier = gateIdentifier;
  }

  public String getMotorways() {
    return motorways;
  }

  public void setMotorways(String motorways) {
    this.motorways = motorways;
  }

  public String getGateNumber() {
    return gateNumber;
  }

  public void setGateNumber(String gateNumber) {
    this.gateNumber = gateNumber;
  }

  public String getLaneCode() {
    return laneCode;
  }

  public void setLaneCode(String laneCode) {
    this.laneCode = laneCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getMainTypeOfGate() {
    return mainTypeOfGate;
  }

  public void setMainTypeOfGate(String mainTypeOfGate) {
    this.mainTypeOfGate = mainTypeOfGate;
  }

  public String getSubTypeOfGate() {
    return subTypeOfGate;
  }

  public void setSubTypeOfGate(String subTypeOfGate) {
    this.subTypeOfGate = subTypeOfGate;
  }

  public String getDistance() {
    return distance;
  }

  public void setDistance(String distance) {
    this.distance = distance;
  }

  public String getGlobalGateIdentifier() {
    return globalGateIdentifier;
  }

  public void setGlobalGateIdentifier(String globalGateIdentifier) {
    this.globalGateIdentifier = globalGateIdentifier;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("StconRecord [recordCode=")
           .append(recordCode)
           .append(", gateIdentifier=")
           .append(gateIdentifier)
           .append(", motorways=")
           .append(motorways)
           .append(", gateNumber=")
           .append(gateNumber)
           .append(", laneCode=")
           .append(laneCode)
           .append(", description=")
           .append(description)
           .append(", endDate=")
           .append(endDate)
           .append(", mainTypeOfGate=")
           .append(mainTypeOfGate)
           .append(", subTypeOfGate=")
           .append(subTypeOfGate)
           .append(", distance=")
           .append(distance)
           .append(", globalGateIdentifier=")
           .append(globalGateIdentifier)
           .append("]");
    return builder.toString();
  }

}
