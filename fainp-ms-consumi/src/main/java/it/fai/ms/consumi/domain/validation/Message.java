package it.fai.ms.consumi.domain.validation;

import java.util.Objects;
import java.util.function.Consumer;

public class Message {

  private String code = "";
  private String text = "";

  public Message(final String _code) {
    code = Objects.requireNonNull(_code, "Code parameter is mandatory");
  }

  public Message code(final String _code) {
    code = _code;
    return this;
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Message obj = getClass().cast(_obj);
      res = Objects.equals(obj.code, code);
    }
    return res;
  }

  public String getCode() {
    return code;
  }

  public String getText() {
    return text;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }

  public Message text(final String _text) {
    text = _text;
    return this;
  }

  public Message withText(Consumer<StringBuilder> consumer){
    final StringBuilder stringBuilder = new StringBuilder();
    consumer.accept(stringBuilder);
    return this.withText(stringBuilder.toString());
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("code=")
                              .append(code)
                              .append(",text=")
                              .append(text)
                              .append("]")
                              .toString();
  }

  public Message withText(final String _text) {
    text = _text;
    return this;
  }

}
