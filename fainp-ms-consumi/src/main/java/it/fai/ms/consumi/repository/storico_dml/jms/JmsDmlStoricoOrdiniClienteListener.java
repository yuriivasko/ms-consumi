package it.fai.ms.consumi.repository.storico_dml.jms;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.AbstractJmsDmlListener;
import it.fai.ms.common.dml.efservice.dto.OrdiniClienteDMLDTO;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.consumi.repository.storico_dml.StoricoOrdiniClienteRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoOrdiniCliente;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsDmlStoricoOrdiniClienteListener extends AbstractJmsDmlListener<OrdiniClienteDMLDTO, StoricoOrdiniCliente>  implements JmsTopicListener {

  public JmsDmlStoricoOrdiniClienteListener(StoricoOrdiniClienteRepository repository,ApplicationEventPublisher publisher) throws Exception {
    super(OrdiniClienteDMLDTO.class, repository,publisher,false);
  }

  @Override
  public StoricoOrdiniCliente getEntityToPersist(final OrdiniClienteDMLDTO dto, final StoricoOrdiniCliente _rf) {
    final StoricoOrdiniCliente rf = _rf!=null ? _rf : new StoricoOrdiniCliente();

    //rf.setAgente(dto.getCodiceAgente());
    // TODO rf.setCapoArea(capoArea);
    rf.setClienteAssegnatario(dto.getRagioneSocialeAssegnatario());
    //rf.setClienteFatturazione(dto.getCodiceClienteFatturazione());
    rf.setCodiceCliente(dto.getCodiceClienteAssegnatario());
    //rf.setConcorzio(dto.getConsorzioDescrizione());
    rf.setConsumoMensilePrevisto(dto.getConsumoPrevisto());
    //rf.setDataCreazioneCarrello(dto.getDataCreazione() != null ? LocalDateTime.ofInstant(dto.getDataCreazione(), ZoneOffset.UTC) : null);
    rf.setDataUltimaVariazione(dto.getDataModificaStato());
    rf.setDmlRevisionTimestamp(dto.getDmlRevisionTimestamp());
    rf.setOrdineClienteDmlUniqueIdentifier(dto.getDmlUniqueIdentifier());
    rf.setNumeroOrdine(dto.getNumero());
    //rf.setPartitaIva(dto.getPartitaIva());
    rf.setRagioneSociale(dto.getRagioneSocialeAssegnatario());
    rf.setRichiedente(dto.getRichiedente());
    rf.setStatoOrdine(dto.getStato());
    rf.setTipoOrdine(dto.getTipo());
    
    return rf;
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.DML_ORDINICLIENTE_SAVE;
  }
  
  
}
