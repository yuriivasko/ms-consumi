package it.fai.ms.consumi.service.impl;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.repository.storico_dml.StoricoContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoDispositivoRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoOrdiniClienteRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoRichiestaRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoVeicoloRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoDispositivo;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoOrdiniCliente;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoRichiesta;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;
import it.fai.ms.consumi.service.DmlBulkService;

@Service
@Transactional
public class DmlBulkServiceImpl implements DmlBulkService {
  
  private final StoricoDispositivoRepository storicoDispositivoRepository;
  private final StoricoOrdiniClienteRepository storicoOrdiniClienteRepository;
  private final StoricoRichiestaRepository storicoRichiestaRepository;
  private final StoricoContrattoRepository storicoContrattoRepository;
  private final StoricoVeicoloRepository storicoVeicoloRepository;

  public DmlBulkServiceImpl(StoricoDispositivoRepository _storicoDispositivoRepository,
                            StoricoOrdiniClienteRepository _storicoOrdiniClienteRepository,
                            StoricoRichiestaRepository _storicoRichiestaRepository,
                            StoricoContrattoRepository _storicoContrattoRepository,
                            StoricoVeicoloRepository _storicoVeicoloRepository) {
    storicoDispositivoRepository = _storicoDispositivoRepository;
    storicoOrdiniClienteRepository = _storicoOrdiniClienteRepository;
    storicoRichiestaRepository = _storicoRichiestaRepository;
    storicoContrattoRepository = _storicoContrattoRepository; 
    storicoVeicoloRepository = _storicoVeicoloRepository;
  }

  public Page<StoricoDispositivo> findAllDevices(Pageable pageable) {
    _log.debug("Request to find all from StoricoDispositivo : {}", pageable);
    return storicoDispositivoRepository.findAll(pageable);
  }

  public List<StoricoDispositivo> findAllDevicesByCustomerCode(String code) {
    _log.debug("Request to find all from StoricoDispositivo by customer code : {}", code);
    return storicoDispositivoRepository.findAllByCustomerCode(code);
  }

  @Override
  public Page<StoricoOrdiniCliente> findAllCustomerOrder(Pageable pageable) {
    _log.debug("Request to find all from StoricoOrdiniCliente : {}", pageable);
    return storicoOrdiniClienteRepository.findAll(pageable);
  }

  @Override
  public List<StoricoOrdiniCliente> findAllCustomerOrderByCustomerCode(String code) {
    _log.debug("Request to find all from StoricoOrdiniCliente by customer code : {}", code);
    return storicoOrdiniClienteRepository.findAllByCodiceCliente(code);
  }

  @Override
  public Page<StoricoRichiesta> findAllRequest(Pageable pageable) {
    _log.debug("Request to find all from StoricoRichiesta : {}", pageable);
    return storicoRichiestaRepository.findAll(pageable);
  }

  @Override
  public List<StoricoRichiesta> findAllRequestByCustomerCode(String code) {
    _log.debug("Request to find all from StoricoRichiesta by customer code : {}", code);
    return storicoRichiestaRepository.findAllByCodiceCliente(code);
  }

  @Override
  public Page<StoricoContratto> findAllContractStatus(Pageable pageable) {
    _log.debug("Request to find all from StoricoContratto : {}", pageable);
    return storicoContrattoRepository.findAll(pageable);
  }

  @Override
  public List<StoricoContratto> findAllContractStatusByCustomerCode(String code) {
    _log.debug("Request to find all from StoricoContratto by customer code : {}", code);
    return storicoContrattoRepository.findAllByCodiceAzienda(code);
  }

  @Override
  public Page<StoricoVeicolo> findAllVehicle(Pageable pageable) {
    _log.debug("Request to find all from StoricoVeicolo : {}", pageable);
    return storicoVeicoloRepository.findAll(pageable);
  }

  @Override
  public List<StoricoVeicolo> findAllEuroClassByCustomerCode(String code) {
    _log.debug("Request to find all from StoricoVeicolo by customer code : {}", code);
    return storicoVeicoloRepository.findAllByCustomerCode(code);
  }
  
}
