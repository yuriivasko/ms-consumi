package it.fai.ms.consumi.service.consumer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.common.jms.dto.consumi.invoice.AttachmentInvoiceCreatedDTO;
import it.fai.ms.consumi.domain.fatturazione.RequestsCreationAttachments;
import it.fai.ms.consumi.domain.navision.PutAllegatiByNavReturnDTO;
import it.fai.ms.consumi.repository.fatturazione.RequestsCreationAttachmentsRepository;
import it.fai.ms.consumi.service.navision.PutAllegatiToNavService;

@Service
@Transactional
public class AttachmentInvoiceCreatedConsumer {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final String charSequence = "###############################################################################";

  private final RequestsCreationAttachmentsRepository requestCreationAttachmentRepo;

  private final PutAllegatiToNavService invoiceToNavService;

  private final NotificationService notificationService;

  public AttachmentInvoiceCreatedConsumer(final RequestsCreationAttachmentsRepository _requestCreationAttachmentRepo,
                                          final PutAllegatiToNavService _invoiceToNavService,
                                          final NotificationService _notificationService) {
    requestCreationAttachmentRepo = _requestCreationAttachmentRepo;
    invoiceToNavService = _invoiceToNavService;
    notificationService = _notificationService;
  }

  public void consume(AttachmentInvoiceCreatedDTO attachmentInvoice) {
    log.debug("Received: {}", attachmentInvoice);
    String numeroFattura = attachmentInvoice.getNumeroFattura();
    String codiceRaggruppamentoArticoli = attachmentInvoice.getCodiceRaggruppamentoArticoli();
    String uuidDocumento = attachmentInvoice.getUuidDocumento();

    RequestsCreationAttachments requestToUpdateWithDocument = null;
    List<RequestsCreationAttachments> requestsCreationAttachments = requestCreationAttachmentRepo.findByCodiceFattura(numeroFattura);
    if (requestsCreationAttachments.isEmpty()) {
      RuntimeException e = new RuntimeException("No Fattura found with code " + numeroFattura);
      log.error(e.getMessage(), e);
      sendAnomaly(numeroFattura, Collections.emptyList(), e);
      throw e;
    }

    List<RequestsCreationAttachments> requestsWithoutDocument = new ArrayList<>();
    for (RequestsCreationAttachments request : requestsCreationAttachments) {
      String uuidDocument = request.getUuidDocumento();
      if (StringUtils.isBlank(uuidDocument)) {
        requestsWithoutDocument.add(request);
      }

      String codeRaggrArticle = request.getCodiceRaggruppamentoArticolo();
      if (codeRaggrArticle != null && codeRaggrArticle.equals(codiceRaggruppamentoArticoli)) {
        requestToUpdateWithDocument = request;
      }
    }

    Long idRequestUpdated = null;
    if (requestToUpdateWithDocument != null) {
      log.info("Update [{}: {}] with uuidDocumento: {}", requestToUpdateWithDocument.getClass()
                                                                                    .getSimpleName(),
               requestToUpdateWithDocument.getId(), uuidDocumento);
      RequestsCreationAttachments requestUpdated = requestCreationAttachmentRepo.save(requestToUpdateWithDocument.uuidDocumento(uuidDocumento));
      idRequestUpdated = requestUpdated.getId();
    } else {
      RuntimeException e = new RuntimeException("Not found request creation attachments for invoice code: " + numeroFattura
                                                + " and ArticlesGroupCode: " + codiceRaggruppamentoArticoli);
      log.error(e.getMessage(), e);
      sendAnomaly(numeroFattura, Collections.emptyList(), e);
      throw e;
    }

    boolean skipSendToNav = false;
    if (requestsWithoutDocument.size() > 1 || (requestsWithoutDocument.size() == 1 && requestsWithoutDocument.get(0)
                                                                                                             .getId() != requestToUpdateWithDocument.getId())) {
      skipSendToNav = true;
    }

    if (skipSendToNav) {
      if (log.isDebugEnabled()) {
        for (RequestsCreationAttachments req : requestsWithoutDocument) {
          if (req.getId() != idRequestUpdated) {
            log.debug("The UUID document of this request: [ Fattura: {} - ArticleCode: {} ] has not set", numeroFattura,
                      req.getCodiceRaggruppamentoArticolo());
          }
        }
      }
      log.info("Any UUID document of the requests has not set");
      return;
    } else {
      // Send notification to NAV
      List<String> codiciAllegati = new ArrayList<>();
      for (RequestsCreationAttachments invoiceRequest : requestsCreationAttachments) {
        String uuid = invoiceRequest.getUuidDocumento();
        if (invoiceRequest.getId() == idRequestUpdated) {
          uuid = uuidDocumento;
        }
        codiciAllegati.add(uuid);
      }

      try {
        log.info("\n{}\nSend to NAV created attachments invoice from Invoice Number: {} \n{}", charSequence, numeroFattura, charSequence);
        PutAllegatiByNavReturnDTO response = invoiceToNavService.putAllegatiByNav(numeroFattura, codiciAllegati);
        log.info("Response on send Invoice: {} => {}", numeroFattura, response);
        if (response != null && response.getResultCode()
                                        .equals(CodeResponseAllegatiNav.KO.name())) {
          throw new RuntimeException("Response of the API putAllegatiByNav is KO with message: " + response.getResultMessage());
        }
      } catch (Exception e) {
        log.error("Exception on Send Invoice to NAV: ", e);
        sendAnomaly(numeroFattura, codiciAllegati, e);
        throw e;
      }
    }
  }

  public void sendAnomaly(String numeroFattura, List<String> codiciAllegati, Exception e) {
    final HashMap<String, Object> templateParameters = new HashMap<>();
    templateParameters.put("numFattura", numeroFattura);
    templateParameters.put("codiciDocumenti", String.join(",", codiciAllegati));
    templateParameters.put("error", e);
    notificationService.notify("INVNAV-002", templateParameters);
  }

  public enum CodeResponseAllegatiNav {
    OK, KO;
  }

}
