package it.fai.ms.consumi.repository.stanziamento.model;

import static java.util.stream.Collectors.toSet;

import java.math.RoundingMode;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.DettaglioStanziamentoEntityMapper;

@Component
@Validated
public class StanziamentoEntityMapper {

  private final DettaglioStanziamentoEntityMapper dsMapper;
  
//  protected StanziamentoEntityMapper(){
//      dsMapper = null;
//  }
  public StanziamentoEntityMapper(final DettaglioStanziamentoEntityMapper _dsMapper) {
    dsMapper = _dsMapper;
  }

  public Set<Stanziamento> toDomain(@NotNull final Set<StanziamentoEntity> _stanziamentiEntity) {
    return _stanziamentiEntity.stream()
                              .map(stanziamentoEntity -> toDomain(stanziamentoEntity))
                              .collect(toSet());
  }

  public Stanziamento toDomain(final StanziamentoEntity _entity) {
    return toDomain(_entity,null);
  }
  
  public Stanziamento toDomainWithoutDetttagli(final StanziamentoEntity _entity) {
	    return toDomain(_entity,null,false);
	  }
  
  public Stanziamento toDomain(StanziamentoEntity _entity, Set<DettaglioStanziamento> dettagliStanziamento) {
	  return toDomain(_entity,dettagliStanziamento,true);
  }

  private Stanziamento toDomain(StanziamentoEntity _entity, Set<DettaglioStanziamento> dettagliStanziamento,boolean withDettaglio) {
    
    final var domain = new Stanziamento(_entity.getCode());
    domain.setInvoiceType(_entity.getStatoStanziamento());
    domain.setTipoFlusso(_entity.getTipoFlusso());
    domain.setDataErogazioneServizio(_entity.getDataErogazioneServizio());
    domain.setDataAcquisizioneFlusso(_entity.getDataAcquisizioneFlusso());
    domain.setNumeroFornitore(_entity.getNumeroFornitore());
    domain.setNumeroCliente(_entity.getNumeroCliente());
    domain.setArticleCode(_entity.getArticleCode());
    domain.setPaese(_entity.getPaese());
    domain.setYear(_entity.getAnnoStanziamento());
    domain.setVehicleLicensePlate(_entity.getTarga());
    domain.setVehicleEuroClass(_entity.getClasseVeicoloEuro());
    domain.setVehicleCountry(_entity.getNazioneTarga());
    domain.setPrezzo(_entity.getPrezzo() != null ? _entity.getPrezzo()
                                                          .setScale(5, RoundingMode.HALF_UP)
                                                 : null);
    domain.setCosto(_entity.getCosto() != null ? _entity.getCosto()
                                                        .setScale(5, RoundingMode.HALF_UP)
                                               : null);
    domain.setQuantity(_entity.getQuantity());
    domain.setAmountCurrencyCode(_entity.getValuta());
    domain.getDescriptions()[0] = _entity.getDescription1();
    domain.getDescriptions()[1] = _entity.getDescription2();
    domain.getDescriptions()[2] = _entity.getDescription3();
    domain.setGeneraStanziamento(_entity.getGeneraStanziamento());
    domain.setStorno(_entity.getStorno());
    domain.setProvvigioniAquisite(_entity.getProvvigioniAquisite());
    domain.setConguaglio(_entity.isConguaglio());
    domain.setCodiceStanziamentoProvvisorio(_entity.getCodiceStanziamentoProvvisorio());
    domain.setSupplierDocument(_entity.getSupplierDocument());

    if(withDettaglio) {
	    if(dettagliStanziamento==null) {
	      domain.getDettaglioStanziamenti()
	          .addAll(_entity.getDettaglioStanziamenti()
	                         .stream()
	                         .map(dse -> dsMapper.toDomain(dse))
	                         .collect(toSet()));
	    }else {
	      domain.getDettaglioStanziamenti()
	        .addAll(dettagliStanziamento);
	    }
    }

    domain.setNumeroFattura(_entity.getNumeroFattura());
    domain.setDataFattura(_entity.getDataFattura());
    domain.setCodiceClienteFatturazione(_entity.getCodiceClienteFatturazione());
    domain.setTotalAllocationDetails(_entity.getTotalAllocationDetails());
    domain.setSentToNav(_entity.getDateQueuing()!=null);
    return domain;
  }

  public Set<StanziamentoEntity> toEntity(@NotNull final Set<Stanziamento> _stanziamenti) {
    return _stanziamenti.stream()
                        .map(stanziamento -> toEntity(stanziamento))
                        .collect(toSet());
  }

  public StanziamentoEntity toEntity(final Stanziamento _domain) {

    final var entity = new StanziamentoEntity(_domain.getCode());
    entity.setStatoStanziamento(_domain.getInvoiceType());
    entity.setTipoFlusso(_domain.getTipoFlusso());
    entity.setDataErogazioneServizio(_domain.getDataErogazioneServizio());
    entity.setDataAcquisizioneFlusso(_domain.getDataAcquisizioneFlusso());
    entity.setNumeroFornitore(_domain.getNumeroFornitore());
    entity.setNumeroCliente(_domain.getNumeroCliente());
    entity.setNumeroArticolo(_domain.getArticleCode());
    entity.setPaese(_domain.getPaese());
    entity.setAnnoStanziamento(_domain.getYear());
    entity.setTarga(_domain.getVehicleLicensePlate());
    entity.setVehicleEuroClass(_domain.getVehicleEuroClass());
    entity.setNazioneTarga(_domain.getVehicleCountry());
    entity.setPrezzo(_domain.getPrezzo());
    entity.setCosto(_domain.getCosto());
    entity.setQuantity(_domain.getQuantita());
    entity.setCurrency(_domain.getAmountCurrencyCode());
    entity.setDescription1(_domain.getDescriptions()[0]);
    entity.setDescription2(_domain.getDescriptions()[1]);
    entity.setDescription3(_domain.getDescriptions()[2]);
    entity.setGeneraStanziamento(_domain.getGeneraStanziamento());
    entity.setStorno(_domain.getStorno());
    entity.setProvvigioniAquisite(_domain.getProvvigioniAquisite());
    entity.setConguaglio(_domain.isConguaglio());
    entity.setCodiceStanziamentoProvvisorio(_domain.getCodiceStanziamentoProvvisorio());
    entity.setDocumentoDaFornitore(_domain.getDocumentoDaFornitore());
    entity.getDettaglioStanziamenti()
          .addAll(_domain.getDettaglioStanziamenti()
                         .stream()
                         .map(dse -> dsMapper.toEntity(dse))
                         .collect(toSet()));
    entity.setNumeroFattura(_domain.getNumeroFattura());
    entity.setDataFattura(_domain.getDataFattura());
    entity.setCodiceClienteFatturazione(_domain.getCodiceClienteFatturazione());
    entity.setTotalAllocationDetails(_domain.getTotalAllocationDetails());
    return entity;
  }

}
