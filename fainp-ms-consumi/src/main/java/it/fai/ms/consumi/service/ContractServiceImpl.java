package it.fai.ms.consumi.service;

import java.time.Instant;
import java.util.Optional;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.repository.contract.ContractRepository;
import it.fai.ms.consumi.repository.storico_dml.StoricoContrattoRepository;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@Service
@Validated
public class ContractServiceImpl implements ContractService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final ContractRepository contractRepository;
  private final StoricoContrattoRepository storicoStatoContrattoRepository;
  
  private final static String STATO_CONTRATTO_ATTIVO = "ATTIVO";


  @Inject
  public ContractServiceImpl(final ContractRepository _contractRepository, StoricoContrattoRepository _storicoStatoContrattoRepository) {
    contractRepository = _contractRepository;
    storicoStatoContrattoRepository = _storicoStatoContrattoRepository;
  }

  @Override
  public Optional<Contract> findContractByNumber(@NotNull final String _contractUuid) {

    _log.debug("Searching contract for uuid {} ...", _contractUuid);

    final var optionalContract = contractRepository.findContractByUuid(_contractUuid);

    optionalContract.ifPresentOrElse(contract -> _log.debug("... found {}", contract),
                                     () -> _log.info("... contract uuid {} not found", _contractUuid));

    return optionalContract;
  }

  @Override
  public boolean isContractActiveInDate(String _contractNumber, Instant _date) {
    Optional<StoricoContratto> contract = storicoStatoContrattoRepository.findFirstByContrattoNumeroAndDataVariazioneBeforeOrderByDataVariazioneDesc(_contractNumber,_date);
    return contract.isPresent() && STATO_CONTRATTO_ATTIVO.equals(contract.get().getStatoContratto());
  }

}
