package it.fai.ms.consumi.service.jms.listener;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.jms.JmsQueueListener;
import it.fai.ms.common.jms.JmsQueueNames;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoni;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoniDispositivi;
import it.fai.ms.common.jms.fattura.canoni.RichiestaCanoniServizi;
import it.fai.ms.consumi.repository.canoni.CanoniRepository;
import it.fai.ms.consumi.service.jms.listener.canoni.RichiestaCanoniClientiMessage;
import it.fai.ms.consumi.service.jms.producer.CanoniClienteJmsProducer;

@Service
@Transactional
@Profile("!no-jmslistener")
public class CanoniFatturazioneJmsListener implements JmsQueueListener {

  private Logger                         _log = LoggerFactory.getLogger(getClass());
  private final CanoniRepository         canoniRepository;
  private final CanoniClienteJmsProducer sender;

  @Inject
  public CanoniFatturazioneJmsListener(CanoniRepository canoniRepository,CanoniClienteJmsProducer sender) {
    this.canoniRepository    = canoniRepository;
    this.sender = sender;
  }

  @Override
  public void onMessage(final Message _msg) {
    _log.trace("Received jms message : {}", _msg);
    // String identificativo;
    try {
      if (_msg instanceof ObjectMessage) {

        Serializable data;
        data = ((ObjectMessage) _msg).getObject();

        if (data instanceof RichiestaCanoniDispositivi) {
          RichiestaCanoniDispositivi msg = (RichiestaCanoniDispositivi) data;
          _log.info("execute canoni dispositivi for {} ", msg);

          Instant dataDa = msg.getDataDa()
            .toInstant();
          Instant dataA  = msg.getDataA()
            .toInstant();
          process(msg, dataDa, dataA);
        } else if (data instanceof RichiestaCanoniServizi) {
          _log.info("execute canoni servizi for {} ", data);
          RichiestaCanoniServizi msg = (RichiestaCanoniServizi) data;

          Instant dataDa = msg.getDataDa()
            .toInstant();
          Instant dataA  = msg.getDataA()
            .toInstant();
          process(msg, dataDa, dataA);
        } else {
          _log.error("Invalid Message Datafor canoni: {}", data.getClass());
          return;
        }

      } else {
        _log.error("Invalid Message for canoni: {}:{}", _msg.getClass(), _msg);
        return;
      }
    } catch (JMSException e) {
      _log.error("Invalid Message for canoni: {}", e);
    }
  }



  public void process(RichiestaCanoniDispositivi msg, Instant dataDa, Instant dataA) {
    canoniRepository.elencoClienti(dataDa, dataA, msg.getIdentificativo()).forEach(codiceCliente -> {
      sender.sendRichiestaCanoniCliente(new RichiestaCanoniClientiMessage( msg,codiceCliente));
    });
    _log.info("End Canoni Dispositivi {}", msg);
  }



  public void process(RichiestaCanoniServizi msg, Instant dataDa, Instant dataA) {
    canoniRepository.elencoClienti(dataDa, dataA, msg.getIdentificativo()).forEach(codiceCliente -> {
      sender.sendRichiestaCanoniCliente(new RichiestaCanoniClientiMessage( msg,codiceCliente));
    });
    _log.info("End Canoni Servizi {} ", msg);
  }




  @Override
  public JmsQueueNames getQueueName() {
    return JmsQueueNames.NAV_RICHIESTA_CANONI;
  }

  protected Map<String, Object> toMessageMap(String tipo, RichiestaCanoni msg, Exception e, String codiceCliente) {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("tipo", tipo);
    map.put("cliente", codiceCliente);
    map.put("identificativo", msg.getTransactionId());
    map.put("dettagli", e.getMessage());
    return map;
  }

}
