package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.SanBernardoViaggiRecord;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.processor.StanziamentiToNavPublisher;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import it.fai.ms.consumi.service.validator.StanziamentiParamsValidator;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service(SanBernardoJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class SanBernardoJob extends AbstractJob<SanBernardoViaggiRecord, SanBernardo> {

  public static final String QUALIFIER = "SanBernardoJob";

  public SanBernardoJob(
    PlatformTransactionManager transactionManager,
    StanziamentiParamsValidator stanziamentiParamsValidator,
    NotificationService notificationService,
    RecordPersistenceService persistenceService,
    RecordDescriptor<SanBernardoViaggiRecord> recordDescriptor,
    ConsumoProcessor<SanBernardo> consumoProcessor,
    RecordConsumoMapper<SanBernardoViaggiRecord,
      SanBernardo> recordConsumoMapper,
    StanziamentiToNavPublisher stanziamentiToNavJmsProducer) {
    super("SAN_BERNARDO", transactionManager, stanziamentiParamsValidator, notificationService, persistenceService, recordDescriptor, consumoProcessor, recordConsumoMapper, stanziamentiToNavJmsProducer);
  }

  @Override
  protected RecordsFileInfo validateFile(String _filename, long _startTime, Instant _ingestionTime) {
    Map<String, String> mapBuoniOrdini = new HashMap<>();
    return new SanBernardoRecordFileInfo(_filename, _startTime, _ingestionTime,mapBuoniOrdini);
  }

  @Override
  public Format getJobQualifier() {
    return Format.SAN_BERNARDO;
  }

}
