package it.fai.ms.consumi.web.rest;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.fai.ms.consumi.dto.AsyncJobCreatedDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.DispositiviDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.CarburantiDetailDto;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti.ReportCarburantiDaFatturare;
import it.fai.ms.consumi.service.async.ReportAsyncService;
import it.fai.ms.consumi.service.jms.producer.report.JmsReportProducer;
import it.fai.ms.consumi.service.report.ReportCsv;
import it.fai.ms.consumi.web.rest.util.HeaderUtil;
import it.fai.ms.consumi.web.rest.util.SearchReportParameterValidator;

@RestController
@RequestMapping(SearchReportCarburantiResource.BASE_PATH)
public class SearchReportCarburantiResource {

  private final Logger log = LoggerFactory.getLogger(getClass());

  static final String BASE_PATH = "/api/carburanti";

  private final ReportCarburantiDaFatturare reportCarburantiDaFatturare;
  private final ReportAsyncService          reportAsyncService;

  private final JmsReportProducer jmsReportProducer;

  private final ReportCsv reportCsv;


  public SearchReportCarburantiResource(ReportCarburantiDaFatturare reportPedaggiDaFatturare, ReportAsyncService reportAsyncService,
                                        JmsReportProducer jmsReportProducer,ReportCsv reportCsv) {
    this.reportCarburantiDaFatturare = reportPedaggiDaFatturare;
    this.reportAsyncService = reportAsyncService;
    this.jmsReportProducer = jmsReportProducer;
    this.reportCsv = reportCsv;
  }

  @PostMapping("/{codClienteNav}/dispositivi")
  @ResponseBody
  public ResponseEntity<List<DispositiviDto>> searchDispositivi(@PathVariable String codClienteNav,
                                                                @RequestBody(required = true) SearchReportFilterDTO filter) {

    log.debug("REST request to search report for type CarburantiDaFatturare");
    Optional<SearchReportParameterValidator.ValidationResult> validationResult = SearchReportParameterValidator.validateParameters(filter.getInvoiced(),
                                                                                                                                   filter.getNotInvoiced(),
                                                                                                                                   filter.getConsumptionDateFrom(),
                                                                                                                                   filter.getConsumptionDateTo(),
                                                                                                                                   filter.getInvoiceDateFrom(),
                                                                                                                                   filter.getInvoiceDateTo());
    if (validationResult.isPresent()) {
      return ResponseEntity.badRequest()
                           .headers(validationResult.map(v -> HeaderUtil.createAlert(v.getMessage(), v.getParam()))
                                                    .get())
                           .build();
    }
    return ResponseEntity.ok(reportCarburantiDaFatturare.getDispositivi(codClienteNav,replaceIfNull(filter.getInvoiced(),false),replaceIfNull(filter.getNotInvoiced(),false),
                                                                        filter.getConsumptionDateFrom(), filter.getConsumptionDateTo(),
                                                                        filter.getInvoiceDateFrom(), filter.getInvoiceDateTo()));
  }

  @PostMapping("/{codClienteNav}/detail")
  @ResponseBody
  public ResponseEntity<List<CarburantiDaFatturareResponseDto>> searchCustomerPedaggiDetail(@PathVariable String codClienteNav,
                                                                                            @RequestBody(
                                                                                                         required = true) SearchReportFilterDTO filter) {
    List<DispositiviDto> dispositivi = filter.getDispositivi();
    log.debug("REST request to search report for type CarburantiDaFatturare {} {}", codClienteNav, dispositivi);
    Optional<SearchReportParameterValidator.ValidationResult> validationResult = SearchReportParameterValidator.validateParameters(filter.getInvoiced(),
                                                                                                                                   filter.getNotInvoiced(),
                                                                                                                                   filter.getConsumptionDateFrom(),
                                                                                                                                   filter.getConsumptionDateTo(),
                                                                                                                                   filter.getInvoiceDateFrom(),
                                                                                                                                   filter.getInvoiceDateTo());
    if (validationResult.isPresent()) {
      return ResponseEntity.badRequest()
                           .headers(validationResult.map(v -> HeaderUtil.createAlert(v.getMessage(), v.getParam()))
                                                    .get())
                           .build();
    }
    List<CarburantiDaFatturareResponseDto> response = new ArrayList<>();
    for (DispositiviDto dispositivo : dispositivi) {
      response.add(new CarburantiDaFatturareResponseDto(reportCarburantiDaFatturare.getDetail(dispositivo, codClienteNav,
                                                                                              filter.getInvoiced(), filter.getNotInvoiced(),
                                                                                              filter.getConsumptionDateFrom(),
                                                                                              filter.getConsumptionDateTo(),
                                                                                              filter.getInvoiceDateFrom(),
                                                                                              filter.getInvoiceDateTo())));
    }
    return ResponseEntity.ok(response);

  }

  @PostMapping("/{codClienteNav}/report/{lang}")
  @ResponseBody
  public ResponseEntity<AsyncJobCreatedDto> launchCsvReport(@PathVariable String codClienteNav, @PathVariable String lang,
                                                            @RequestParam(value = "zoneId", required = false) String zoneId,
                                                            @RequestBody(required = true) SearchReportFilterDTO filter) throws Exception {

    log.debug("REST to process customer report for type {} {}", codClienteNav, filter.getDispositivi());

    Locale locale = lang != null ? Locale.forLanguageTag(lang) : null;
    ZoneId idZone = zoneId != null ? ZoneId.of(zoneId) : null;
    Optional<SearchReportParameterValidator.ValidationResult> validationResult = SearchReportParameterValidator.validateParameters(filter.getInvoiced(),
                                                                                                                                   filter.getNotInvoiced(),
                                                                                                                                   filter.getConsumptionDateFrom(),
                                                                                                                                   filter.getConsumptionDateTo(),
                                                                                                                                   filter.getInvoiceDateFrom(),
                                                                                                                                   filter.getInvoiceDateTo());
    if (validationResult.isPresent()) {
      return ResponseEntity.badRequest()
                           .headers(validationResult.map(v -> HeaderUtil.createAlert(v.getMessage(), v.getParam()))
                                                    .get())
                           .build();
    }

    Long keyJob = reportAsyncService.generateKeyJob();
    try {
      List<CarburantiDetailDto> response = new ArrayList<>();
      for (DispositiviDto dispositivo : filter.getDispositivi()) {
        response.addAll(reportCarburantiDaFatturare.getDetail(dispositivo, codClienteNav, replaceIfNull(filter.getInvoiced(),false), replaceIfNull(filter.getNotInvoiced(),false),
                                                              filter.getConsumptionDateFrom(), filter.getConsumptionDateTo(),
                                                              filter.getInvoiceDateFrom(), filter.getInvoiceDateTo()));
      }
      if (response.size() == 0)
        throw new RuntimeException("no data");

      String csv = reportCsv.toCsv(CarburantiDetailDto.class, response, locale, idZone);

      jmsReportProducer.sendMessage(keyJob, "report_carburante_", csv);
    } catch (Exception e) {
      log.error("Exception on generation report: ", e);
      throw new RuntimeException(e);
    }

    return ResponseEntity.ok(new AsyncJobCreatedDto(keyJob.toString()));
  }

  private static <T> T replaceIfNull(T value,T replacement) {
    if(value != null)
      return value;
    else
      return replacement;
  }
  
}
