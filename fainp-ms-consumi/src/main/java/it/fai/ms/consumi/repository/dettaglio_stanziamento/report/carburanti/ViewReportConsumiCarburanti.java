package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.carburanti;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;

import it.fai.ms.consumi.domain.InvoiceType;

@Entity(name = "view_report_consumi_carburanti")
@Immutable
public class ViewReportConsumiCarburanti {

  @Id
  private String id;

  @Column(name = "data_fattura")
  private Instant dataFattura;

  @Column(name = "data_erogazione_servizio")
  private LocalDate dataErogazioneServizio;

  @Column(name = "numero_fattura")
  private String numeroFattura;

  @Column(name = "veicolo_classe")
  private String veicoloClasse;

  @Column(name = "veicolo_nazione_targa")
  private String veicoloNazioneTarga;

  @Column(name = "veicolo_targa")
  private String veicoloTarga;

  @Column(name = "codice_trackycard")
  private String codiceTrackycard;

  @Column(name = "erogatore")
  private String erogatore;

  @Column(name = "data_ora_utilizzo")
  private Instant dataOraUtilizzo;

  @Column(name = "articolo_descrizione")
  private String articoloDescrizione;

  @Column(name = "unita_di_misura", nullable = true)
  private String measurementUnit;

  @Column(name = "quantita", precision = 16, scale = 5)
  private BigDecimal quantita;

  @Column(name = "prezzo_unitario_calcolato_noiva", precision = 16, scale = 5)
  private BigDecimal prezzoUnitarioCalcolatoNoiva;

  @Column(name = "codice_cliente_nav", nullable = false)
  private String customerId;

  @Column(name = "punto_erogazione")
  private String puntoErogazione;

  @Column(name = "tipo_consumo")
  @Enumerated(EnumType.STRING)
  private InvoiceType tipoConsumo;

  @Column(name = "nr_articolo")
  private String codiceArticolo;

  public ViewReportConsumiCarburanti() {
    super();
  }

  public Instant getDataFattura() {
    return dataFattura;
  }

  public void setDataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
  }

  public String getNumeroFattura() {
    return numeroFattura;
  }

  public void setNumeroFattura(String numeroFattura) {
    this.numeroFattura = numeroFattura;
  }

  public String getVeicoloClasse() {
    return veicoloClasse;
  }

  public void setVeicoloClasse(String veicoloClasse) {
    this.veicoloClasse = veicoloClasse;
  }

  public String getVeicoloNazioneTarga() {
    return veicoloNazioneTarga;
  }

  public void setVeicoloNazioneTarga(String veicoloNazioneTarga) {
    this.veicoloNazioneTarga = veicoloNazioneTarga;
  }

  public String getVeicoloTarga() {
    return veicoloTarga;
  }

  public void setVeicoloTarga(String veicoloTarga) {
    this.veicoloTarga = veicoloTarga;
  }

  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public LocalDate getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public void setDataErogazioneServizio(LocalDate dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCodiceTrackycard() {
    return codiceTrackycard;
  }

  public void setCodiceTrackycard(String codiceTrackycard) {
    this.codiceTrackycard = codiceTrackycard;
  }

  public String getErogatore() {
    return erogatore;
  }

  public void setErogatore(String erogatore) {
    this.erogatore = erogatore;
  }

  public Instant getDataOraUtilizzo() {
    return dataOraUtilizzo;
  }

  public void setDataOraUtilizzo(Instant dataOraUtilizzo) {
    this.dataOraUtilizzo = dataOraUtilizzo;
  }

  public String getArticoloDescrizione() {
    return articoloDescrizione;
  }

  public void setArticoloDescrizione(String articoloDescrizione) {
    this.articoloDescrizione = articoloDescrizione;
  }

  public String getMeasurementUnit() {
    return measurementUnit;
  }

  public void setMeasurementUnit(String measurementUnit) {
    this.measurementUnit = measurementUnit;
  }

  public BigDecimal getQuantita() {
    return quantita;
  }

  public void setQuantita(BigDecimal quantita) {
    this.quantita = quantita;
  }

  public BigDecimal getPrezzoUnitarioCalcolatoNoiva() {
    return prezzoUnitarioCalcolatoNoiva;
  }

  public void setPrezzoUnitarioCalcolatoNoiva(BigDecimal prezzoUnitarioCalcolatoNoiva) {
    this.prezzoUnitarioCalcolatoNoiva = prezzoUnitarioCalcolatoNoiva;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }

  public InvoiceType getTipoConsumo() {
    return tipoConsumo;
  }

  public void setTipoConsumo(InvoiceType tipoConsumo) {
    this.tipoConsumo = tipoConsumo;
  }

  public String getCodiceArticolo() {
    return codiceArticolo;
  }

  public void setCodiceArticolo(String codiceArticolo) {
    this.codiceArticolo = codiceArticolo;
  }
}
