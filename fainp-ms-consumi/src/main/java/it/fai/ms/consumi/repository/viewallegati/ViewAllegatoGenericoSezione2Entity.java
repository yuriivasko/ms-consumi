package it.fai.ms.consumi.repository.viewallegati;

import java.time.LocalDateTime;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "allegati_fatture_generico_sezione2")
@Immutable
public class ViewAllegatoGenericoSezione2Entity {

  @Id
  @Column(name = "id")
  private String id;

  @Column(name = "codice_contratto")
  private String codiceContratto;

  @Column(name = "tipo_dispositivo")
  private String tipoDispositivo;
  
  @Column(name = "pan_number")
  private String panNumber;

  @Column(name = "serial_number")
  private String serialNumber;
  
  @Column(name = "targa")
  private String targaVeicolo;

  @Column(name = "nazione_targa")
  private String nazioneVeicolo;

  @Column(name = "classificazione_euro")
  private String classificazioneEuro;

  @Column(name = "classe_tariffaria")
  private String classeTariffaria;
  
  @Column(name = "codice_prodotto")
  private String codiceProdotto;
  
  @Column(name = "descrizione_aggiuntiva")
  private String descrizioneAggiuntiva;

  @Column(name = "data_inizio")
  private LocalDateTime dataInizio;

  @Column(name = "data_fine")
  private LocalDateTime dataFine;

  @Column(name = "tratta")
  private String tratta;

  @Column(name = "quantita")
  private Double quantita;

  @Column(name = "importo")
  private Double importo;

  @Column(name = "imponibile")
  private Double imponibile;

  @Column(name = "valuta")
  private String valuta;

  @Column(name = "cambio")
  private Double cambio;

  @Column(name = "perc_iva")
  private Double percIva;

  @Column(name = "documento_da_fornitore")
  private String documentoDaFornitore;
  
  @Column(name = "raggruppamento_articoli")
  private String raggruppamentoArticoli;
  
  @Column(name = "codice_fornitore_nav")
  private String codiceFornitoreNav;
  
  @Column(name = "record_code")
  private String recordCode;
  
  @Column(name = "punto_erogazione")
  private String puntoErogazione;
  
  @Column(name = "sign_transaction")
  private String signTransaction;
  
  @Column(name = "tipo_consumo")
  private String tipoConsumo;

  public Double getPercIva() {
    return percIva;
  }

  public void setPercIva(Double percIva) {
    this.percIva = percIva;
  }

  public String getRaggruppamentoArticoli() {
    return raggruppamentoArticoli;
  }

  public void setRaggruppamentoArticoli(String raggruppamentoArticoli) {
    this.raggruppamentoArticoli = raggruppamentoArticoli;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public String getTipoDispositivo() {
    return tipoDispositivo;
  }

  public void setTipoDispositivo(String tipoDispositivo) {
    this.tipoDispositivo = tipoDispositivo;
  }

  public String getPanNumber() {
    return panNumber;
  }

  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getNazioneVeicolo() {
    return nazioneVeicolo;
  }

  public void setNazioneVeicolo(String nazioneVeicolo) {
    this.nazioneVeicolo = nazioneVeicolo;
  }

  public String getTargaVeicolo() {
    return targaVeicolo;
  }

  public void setTargaVeicolo(String targaVeicolo) {
    this.targaVeicolo = targaVeicolo;
  }

  public String getClassificazioneEuro() {
    return classificazioneEuro;
  }

  public void setClassificazioneEuro(String classificazioneEuro) {
    this.classificazioneEuro = classificazioneEuro;
  }

  public String getClasseTariffaria() {
    return classeTariffaria;
  }

  public void setClasseTariffaria(String classeTariffaria) {
    this.classeTariffaria = classeTariffaria;
  }

  public LocalDateTime getDataInizio() {
    return dataInizio;
  }

  public void setDataInizio(LocalDateTime dataInizio) {
    this.dataInizio = dataInizio;
  }

  public LocalDateTime getDataFine() {
    return dataFine;
  }

  public void setDataFine(LocalDateTime dataFine) {
    this.dataFine = dataFine;
  }

  public String getCodiceProdotto() {
    return codiceProdotto;
  }

  public void setCodiceProdotto(String prodotto) {
    this.codiceProdotto = prodotto;
  }

  public Double getQuantita() {
    return quantita;
  }

  public void setQuantita(Double quantita) {
    this.quantita = quantita;
  }

  public Double getImporto() {
    return importo;
  }

  public void setImporto(Double importo) {
    this.importo = importo;
  }

  public Double getImponibile() {
    return imponibile;
  }

  public void setImponibile(Double imponibile) {
    this.imponibile = imponibile;
  }

  public String getValuta() {
    return valuta;
  }

  public void setValuta(String valuta) {
    this.valuta = valuta;
  }

  public Double getCambio() {
    return cambio;
  }

  public void setCambio(Double cambio) {
    this.cambio = cambio;
  }

  public String getTratta() {
    return tratta;
  }

  public void setTratta(String tratta) {
    this.tratta = tratta;
  }

  public String getDescrizioneAggiuntiva() {
    return descrizioneAggiuntiva;
  }

  public void setDescrizioneAggiuntiva(String descrizioneAggiuntiva) {
    this.descrizioneAggiuntiva = descrizioneAggiuntiva;
  }

  public String getDocumentoDaFornitore() {
    return documentoDaFornitore;
  }

  public void setDocumentoDaFornitore(String documentoDaFornitore) {
    this.documentoDaFornitore = documentoDaFornitore;
  }

  public String getCodiceFornitoreNav() {
    return codiceFornitoreNav;
  }

  public void setCodiceFornitoreNav(String codiceFornitoreNav) {
    this.codiceFornitoreNav = codiceFornitoreNav;
  }

  public String getRecordCode() {
    return recordCode;
  }

  public void setRecordCode(String recordCode) {
    this.recordCode = recordCode;
  }

  public String getPuntoErogazione() {
    return puntoErogazione;
  }

  public void setPuntoErogazione(String puntoErogazione) {
    this.puntoErogazione = puntoErogazione;
  }
  
  public String getSignTransaction() {
    return signTransaction;
  }

  public void setSignTransaction(String signTransaction) {
    this.signTransaction = signTransaction;
  }
  
  

  public String getTipoConsumo() {
    return tipoConsumo;
  }

  public void setTipoConsumo(String tipoConsumo) {
    this.tipoConsumo = tipoConsumo;
  }

  //this method is used only from the builder
  void populate(String codiceContratto, String tipoDispositivo, String panNumber, String serialNumber, String nazioneVeicolo, String targaVeicolo, String classificazioneEuro, String classeTariffaria, LocalDateTime dataInizio, LocalDateTime dataFine, String causale, String prodotto, String descrizione, Double quantita, Double importo, Double importoConIva, Double imponibile, String valuta, Double cambio, String tratta, Double percIva, String raggruppamentoArticoli, String codiceFornitoreNav, String recordCode, String puntoErogazione) {
//    this.id = id;
    this.codiceContratto = codiceContratto;
    this.tipoDispositivo = tipoDispositivo;
    this.panNumber = panNumber;
    this.serialNumber = serialNumber;
    this.nazioneVeicolo = nazioneVeicolo;
    this.targaVeicolo = targaVeicolo;
    this.classificazioneEuro = classificazioneEuro;
    this.classeTariffaria = classeTariffaria;
    this.dataInizio = dataInizio;
    this.dataFine = dataFine;
    this.codiceProdotto = prodotto;
    this.descrizioneAggiuntiva = descrizione;
    this.quantita = quantita;
    this.importo = importo;
    this.imponibile = imponibile;
    this.valuta = valuta;
    this.cambio = cambio;
    this.tratta = tratta;
    this.percIva = percIva;
    this.raggruppamentoArticoli = raggruppamentoArticoli;
    this.codiceFornitoreNav = codiceFornitoreNav;
    this.recordCode = recordCode;
    this.puntoErogazione = puntoErogazione;
    signTransaction = "+";
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ViewAllegatoGenericoSezione2Entity [id=");
    builder.append(id);
    builder.append(", codiceContratto=");
    builder.append(codiceContratto);
    builder.append(", tipoDispositivo=");
    builder.append(tipoDispositivo);
    builder.append(", panNumber=");
    builder.append(panNumber);
    builder.append(", serialNumber=");
    builder.append(serialNumber);
    builder.append(", targaVeicolo=");
    builder.append(targaVeicolo);
    builder.append(", nazioneVeicolo=");
    builder.append(nazioneVeicolo);
    builder.append(", classificazioneEuro=");
    builder.append(classificazioneEuro);
    builder.append(", classeTariffaria=");
    builder.append(classeTariffaria);
    builder.append(", codiceProdotto=");
    builder.append(codiceProdotto);
    builder.append(", descrizioneAggiuntiva=");
    builder.append(descrizioneAggiuntiva);
    builder.append(", dataInizio=");
    builder.append(dataInizio);
    builder.append(", dataFine=");
    builder.append(dataFine);
    builder.append(", tratta=");
    builder.append(tratta);
    builder.append(", quantita=");
    builder.append(quantita);
    builder.append(", importo=");
    builder.append(importo);
    builder.append(", imponibile=");
    builder.append(imponibile);
    builder.append(", valuta=");
    builder.append(valuta);
    builder.append(", cambio=");
    builder.append(cambio);
    builder.append(", percIva=");
    builder.append(percIva);
    builder.append(", documentoDaFornitore=");
    builder.append(documentoDaFornitore);
    builder.append(", raggruppamentoArticoli=");
    builder.append(raggruppamentoArticoli);
    builder.append(", codiceFornitoreNav=");
    builder.append(codiceFornitoreNav);
    builder.append(", recordCode=");
    builder.append(recordCode);
    builder.append(", puntoErogazione=");
    builder.append(puntoErogazione);
    builder.append(", signTransaction=");
    builder.append(signTransaction);
    builder.append(", tipoConsumo=");
    builder.append(tipoConsumo);
    builder.append("]");
    return builder.toString();
  }

}
