package it.fai.ms.consumi.service.impl;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.repository.CaselliEntityRepository;
import it.fai.ms.consumi.repository.caselli.model.CaselliEntity;
import it.fai.ms.consumi.repository.caselli.model.CaselliEntityMapper;
import it.fai.ms.consumi.repository.flussi.record.model.stcon.StconRecord;
import it.fai.ms.consumi.service.CaselliService;

@Service
@Transactional
public class CaselliServiceImpl implements CaselliService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final CaselliEntityRepository caselliRepository;
  private final CaselliEntityMapper caselliEntityMapper;

  public final static String DEFAULT_END_DATE = "20991231";
  private final static String DATE_FORMAT = "yyyyMMdd";

  public CaselliServiceImpl(CaselliEntityRepository _caselliRepository,
                            CaselliEntityMapper _caselliEntityMapper) {
    caselliRepository = _caselliRepository;
    caselliEntityMapper = _caselliEntityMapper;
  }

  //  public Optional<CaselliEntity> findOneByGateIdentifier(String gateIndentifier) {
  //    log.debug("Request to get Caselli by gateIndentifier : {}", gateIndentifier);
  //    return caselliRepository.findOneByGateIdentifier(gateIndentifier);
  //  }
  //
  //  public Optional<CaselliEntity> findOneByGlobalGateIdentifier(String globalGateIndentifier) {
  //    log.debug("Request to get Caselli by globalGateIndentifier : {}", globalGateIndentifier);
  //    return caselliRepository.findOneByGlobalGateIdentifier(globalGateIndentifier);
  //  }

  @Override
  public CaselliEntity save(final StconRecord record) {
    _log.debug("Save to CaselliEntity from : {}", record);
    Optional<CaselliEntity> temp = caselliRepository.findOneByGlobalGateIdentifier(record.getGlobalGateIdentifier());
    CaselliEntity entity = caselliEntityMapper.toEntity(record);
    entity.setId(temp.isPresent() ? temp.get().getId() : 0L);
    return caselliRepository.save(entity);
  }

  @Override
  public CaselliEntity save(StconRecord record, String firstLine) {
    _log.debug("Save to CaselliEntity from : {}", record);
    String network = firstLine.substring(46, 54).trim();
    String endDate = StringUtils.isBlank(record.getEndDate()) ? DEFAULT_END_DATE : record.getEndDate();
    Optional<CaselliEntity> temp = caselliRepository.findOneByGlobalGateIdentifierAndEndDateAndNetwork(record.getGlobalGateIdentifier(), endDate, network);
    CaselliEntity entity = caselliEntityMapper.toEntity(record, network, endDate);
    entity.setId(temp.isPresent() ? temp.get().getId() : 0L);
    return caselliRepository.save(entity);
  }

  @Override
  public CaselliEntity findByGateIdentifier(String code, Instant dtm) {
    _log.debug("Find by Gate Identifier : {} with end date : {}", code, dtm);
    CaselliEntity retValue = null;

    try {
      List<CaselliEntity> arr = caselliRepository.findByGateIdentifierOrderByEndDateAsc(code);
      if (arr == null || arr.size() < 1) {
        _log.warn("Not found gate with identifier : {}", code);
        return null;
      }

      for (CaselliEntity casello : arr) {
        if (StringUtils.isBlank(casello.getEndDate())) {
          casello.setEndDate(DEFAULT_END_DATE);
        }

        Date date = new SimpleDateFormat(DATE_FORMAT).parse(casello.getEndDate());
        Instant endDate = date.toInstant();

        if (dtm.isBefore(endDate)) {
          if (retValue == null) {
            retValue = casello;
          } else if (casello.getEndDate().compareTo(retValue.getEndDate()) < 0) {
            retValue = casello;
          }
        }
      }

      if (retValue == null) {
        _log.warn("Not found gate with identifier : {} and end date before : {}", code, dtm);
      }
      return retValue;
    } catch (Exception e) {
      _log.error("FindByGateIdentifier exception", e);
    }

    return null;
  }

  @Override
  public CaselliEntity findByGlobalGateIdentifier(String code, Instant dtm) {
    _log.debug("Find by Global Gate Identifier : {} with end date : {}", code, dtm);
    CaselliEntity retValue = null;

    try {
      List<CaselliEntity> arr = caselliRepository.findByGlobalGateIdentifierOrderByEndDateAsc(code);
      if (arr == null || arr.size() < 1) {
        _log.warn("Not found global gate with identifier : {}", code);
        return null;
      }

      for (CaselliEntity casello : arr) {
        if (StringUtils.isBlank(casello.getEndDate())) {
          casello.setEndDate(DEFAULT_END_DATE);
        }

        Date date = new SimpleDateFormat(DATE_FORMAT).parse(casello.getEndDate());
        Instant endDate = date.toInstant();

        if (dtm.isBefore(endDate)) {
          if (retValue == null) {
            retValue = casello;
          } else if (casello.getEndDate().compareTo(retValue.getEndDate()) < 0) {
            retValue = casello;
          }
        }
      }

      if (retValue == null) {
        _log.warn("Not found global gate with identifier : {} and end date before : {}", code, dtm);
      }
      return retValue;
    } catch (Exception e) {
      _log.error("findByGlobalGateIdentifier exception", e);
    }

    return null;
  }

  @Override
  public List<CaselliEntity> findByGateIdentifier(String code) {
    _log.debug("Request to get list of Caselli by gate Indentifier : {}", code);
    return caselliRepository.findByGateIdentifier(code);
  }

  @Override
  public List<CaselliEntity> findByGlobalGateIdentifier(String code) {
    _log.debug("Request to get list of Caselli by global Gate Indentifier : {}", code);
    return caselliRepository.findByGlobalGateIdentifier(code);
  }

}
