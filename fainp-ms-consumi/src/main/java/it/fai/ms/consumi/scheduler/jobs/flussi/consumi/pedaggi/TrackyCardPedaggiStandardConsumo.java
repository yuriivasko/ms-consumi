package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import it.fai.ms.consumi.domain.GlobalIdentifier;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.consumi.pedaggi.ConsumoPedaggio;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;

import java.util.Optional;

public class TrackyCardPedaggiStandardConsumo extends ConsumoPedaggio {

  public TrackyCardPedaggiStandardConsumo(Source _source, String _recordCode, Optional<GlobalIdentifier> _optionalGlobalIdentifier, final CommonRecord record) {
    super(_source, _recordCode, _optionalGlobalIdentifier, record);
  }

  @Override
  public InvoiceType getInvoiceType() {
    return InvoiceType.D;
  }

  @Override
  protected GlobalIdentifier makeInternalGlobalIdentifier() {
    throw new UnsupportedOperationException();
  }
}
