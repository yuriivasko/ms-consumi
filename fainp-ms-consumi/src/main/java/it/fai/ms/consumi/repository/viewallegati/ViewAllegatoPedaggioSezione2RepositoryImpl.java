package it.fai.ms.consumi.repository.viewallegati;

import static java.util.stream.Collectors.toSet;
import static  it.fai.ms.consumi.util.SqlUtil.MAX_SIZE_PARAMETER;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import it.fai.common.enumeration.TipoDispositivoEnum;

@Repository
public class ViewAllegatoPedaggioSezione2RepositoryImpl implements ViewAllegatoPedaggioSezione2Repository {

  private final Logger log = LoggerFactory.getLogger(getClass());

  private final EntityManager em;

  public ViewAllegatoPedaggioSezione2RepositoryImpl(final EntityManager _em) {
    em = _em;
  }

  @Override
  public Set<ViewAllegatoPedaggioSezione2Entity> findDettagliBy(TipoDispositivoEnum deviceType, String licensePlate, String country,
                                                                String panNumber, String euroClass) {
    final var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ViewAllegatoPedaggioSezione2Entity.class);
    Root<ViewAllegatoPedaggioSezione2Entity> root = cq.from(ViewAllegatoPedaggioSezione2Entity.class);

    List<Predicate> predicates = new ArrayList<>();
    predicates.add(createPredicate(cb, root.get(ViewAllegatoPedaggioSezione2Entity_.TIPO_DISPOSITIVO), deviceType.name()));
    predicates.add(createPredicate(cb, root.get(ViewAllegatoPedaggioSezione2Entity_.PAN_NUMBER), panNumber));
    predicates.add(createPredicate(cb, root.get(ViewAllegatoPedaggioSezione2Entity_.TARGA_VEICOLO), licensePlate));
    predicates.add(createPredicate(cb, root.get(ViewAllegatoPedaggioSezione2Entity_.NAZIONE_VEICOLO), country));
    predicates.add(createPredicate(cb, root.get(ViewAllegatoPedaggioSezione2Entity_.CLASSIFICAZIONE_EURO), euroClass));

    cq.select(root);
    cq.where(predicates.toArray(new Predicate[] {}));

    List<ViewAllegatoPedaggioSezione2Entity> resultList = null;
    TypedQuery<ViewAllegatoPedaggioSezione2Entity> query = em.createQuery(cq);
    logQuery(query);
    resultList = query.getResultList();
    return resultList.stream()
                     .collect(toSet());
  }

  @Override
  public List<ViewAllegatoPedaggioSezione2Entity> findByDettaglioStanziamentoId(List<UUID> dettagliStanziamentoId) {
    int size = dettagliStanziamentoId.size();
    int chunkSize = size / MAX_SIZE_PARAMETER;

    List<ViewAllegatoPedaggioSezione2Entity> rs = new ArrayList<>(size);
    for (int i = 0; i <= chunkSize; i++) {
      int start = i * MAX_SIZE_PARAMETER;
      int end = (start + MAX_SIZE_PARAMETER) > size ? size : start + MAX_SIZE_PARAMETER;
      log.debug("Generate SubList of DettaglioStanziamentoId from {} to {}", start, end);
      List<UUID> subList = dettagliStanziamentoId.subList(start, end);
      if (subList != null && !subList.isEmpty() && subList.size() > 0) {
        rs.addAll(_findByDettaglioStanziamentoId(subList));
      }
    }
    return rs;
  }

  private List<ViewAllegatoPedaggioSezione2Entity> _findByDettaglioStanziamentoId(List<UUID> dettagliStanziamentoId) {
    if(dettagliStanziamentoId == null || dettagliStanziamentoId.isEmpty()) {
      log.warn("Cannot receive list NULL or EMPTY, so return ArrayList empty...");
      return new ArrayList<>();
    }
    
    final var cb = em.getCriteriaBuilder();
    var cq = cb.createQuery(ViewAllegatoPedaggioSezione2Entity.class);
    Root<ViewAllegatoPedaggioSezione2Entity> root = cq.from(ViewAllegatoPedaggioSezione2Entity.class);

    cq.select(root);
    Predicate pId = root.get(ViewAllegatoPedaggioSezione2Entity_.ID)
                        .in(dettagliStanziamentoId);
    cq.where(pId);

    List<Order> orderList = new ArrayList<>();
    orderList.add(cb.asc(root.get(ViewAllegatoPedaggioSezione2Entity_.CODICE_CONTRATTO)));
    orderList.add(cb.asc(root.get(ViewAllegatoPedaggioSezione2Entity_.TIPO_DISPOSITIVO)));
    orderList.add(cb.asc(root.get(ViewAllegatoPedaggioSezione2Entity_.PAN_NUMBER)));
    orderList.add(cb.asc(root.get(ViewAllegatoPedaggioSezione2Entity_.SERIAL_NUMBER)));

    cq.orderBy(orderList);

    TypedQuery<ViewAllegatoPedaggioSezione2Entity> query = em.createQuery(cq);
    logQuery(query);
    return query.getResultList();
  }

  private Predicate createPredicate(CriteriaBuilder cb, Path<Object> path, Object param) {
    Predicate p = null;
    if (param == null) {
      p = cb.isNull(path);
    } else {
      p = cb.equal(path, param);
    }
    return p;
  }

  private void logQuery(TypedQuery<?> query) {
    if (log.isDebugEnabled()) {
      log.debug("Query: {}", query.unwrap(Query.class)
                                  .getQueryString());
    }
  }

}
