package it.fai.ms.consumi.service.validator;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.inject.Inject;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.enumeration.Format;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.domain.TrustedCustomerSupplier;
import it.fai.ms.consumi.domain.consumi.ParamStanziamentoMatchingParamsSupplier;
import it.fai.ms.consumi.domain.parametri_stanziamenti.CostoRicavo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsQuery;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.service.navision.NavArticlesService;
import it.fai.ms.consumi.service.navision.NavSupplierCodesService;
import it.fai.ms.consumi.service.parametri_stanziamenti.StanziamentiParamsService;

@Service
@Validated
public class StanziamentiParamsValidatorImpl implements StanziamentiParamsValidator {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final StanziamentiParamsService stanziamentiParamsService;
  private final NavArticlesService        navArticlesService;
  private final NavSupplierCodesService   navSupplierCodesService;
  private final ApplicationProperties     applicationProperties;

  private final boolean validatorConsistencyEnabled;
  
  final ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService;



  @Inject
  public StanziamentiParamsValidatorImpl(final StanziamentiParamsService _stanziamentiParamsService,
                                         final NavArticlesService _navArticlesService,
                                         final NavSupplierCodesService _navSupplierCodesService,
                                         final ConsumoNoBlockingValidatorService consumoNoBlockingValidatorService,
                                         final ApplicationProperties _applicationProperties,
                                         final @Value("#{new Boolean(${validator.stanziamentiParams.consistency:false})}") boolean _validatorConsistencyEnabled) {
    stanziamentiParamsService = _stanziamentiParamsService;
    navArticlesService = _navArticlesService;
    navSupplierCodesService = _navSupplierCodesService;
    applicationProperties = _applicationProperties;
    validatorConsistencyEnabled = _validatorConsistencyEnabled;
    this.consumoNoBlockingValidatorService = consumoNoBlockingValidatorService;
    _log.info("Validator enabled 'validator.stanziamentiParams.consistency' : {}", _validatorConsistencyEnabled);
  }
  
  public StanziamentiParamsValidatorImpl(final StanziamentiParamsService _stanziamentiParamsService,
                                         final NavArticlesService _navArticlesService,
                                         final NavSupplierCodesService _navSupplierCodesService,
                                         final ApplicationProperties _applicationProperties,
                                         final @Value("#{new Boolean(${validator.stanziamentiParams.consistency:false})}") boolean _validatorConsistencyEnabled) {
    stanziamentiParamsService = _stanziamentiParamsService;
    navArticlesService = _navArticlesService;
    navSupplierCodesService = _navSupplierCodesService;
    applicationProperties = _applicationProperties;
    validatorConsistencyEnabled = _validatorConsistencyEnabled;
    this.consumoNoBlockingValidatorService = null;
    _log.info("Validator enabled 'validator.stanziamentiParams.consistency' : {}", _validatorConsistencyEnabled);
  }

  @Override
  public ValidationOutcome checkConsistency() throws StanziamentiParamsNotFoundException {
    ValidationOutcome validationOutcome = new ValidationOutcome();
    final var stanziamentiParams = stanziamentiParamsService.getAll();
    if (stanziamentiParams.isEmpty()) {
      throw notFoundException();
    }

    try {

      final Set<String> navProviderCodes = navSupplierCodesService.getAllCodes();
      final boolean isSubsetOfNavProviderCodes = stanziamentiParams.isSubsetOfProviderCodes(navProviderCodes);

      final List<Article> navArticles = navArticlesService.getAllArticlesAndUpdateStanziamentiParams(stanziamentiParams);
      final boolean isSubsetOfNavArticles = stanziamentiParams.isSubsetOfArticles(navArticles);


      if (!isSubsetOfNavProviderCodes) {
        validationOutcome.addMessage(new Message("001").withText("Consistency failed for provider codes"));
        validationOutcome.setFatalError(true);
      }
      if (!isSubsetOfNavArticles) {
        validationOutcome.addMessage(new Message("002").withText("Consistency failed for articles"));
        validationOutcome.setFatalError(true);
      }

    } catch (Exception e) {
      StringBuilder sb = new StringBuilder();

      final String exception = e.getClass().getSimpleName();
      final String message = e.getMessage();

      sb.append("Exception :=[");
      sb.append(exception);
      sb.append(" : ");
      sb.append(message);
      sb.append("]");

      validationOutcome.addMessage(new Message("003").withText("Consistency failed with exception! " + sb.toString()));
      validationOutcome.setFatalError(true);

    }

    _log.info("Consistency check result : {}", validationOutcome);
    if (!validatorConsistencyEnabled) {
      validationOutcome = new ValidationOutcome();
      _log.info("Consistency check disabled, response overrided : {}", validationOutcome);
    }

    return validationOutcome;
  }

  private static StanziamentiParamsNotFoundException notFoundException() {
    return new StanziamentiParamsNotFoundException(StanziamentiParams.class.getSimpleName() + " not found");
  }

  @Override
  public ValidationOutcome existsParamsStanziamento(final ParamStanziamentoMatchingParamsSupplier _consumoForMatchingParams) {

    var validationOutcome = new ValidationOutcome();

    if (_consumoForMatchingParams != null) {


      final StanziamentiParamsQuery stanziamentiParamsQuery = new StanziamentiParamsQuery(_consumoForMatchingParams.getSourceType(),
                                                                                          _consumoForMatchingParams.getRecordCodeToLookInParamStanz());

      stanziamentiParamsQuery.setVatRate(_consumoForMatchingParams.getVatRate().orElse(null));
      stanziamentiParamsQuery.setVatMandatory(
        isVatMandatoryForThisConsumo(_consumoForMatchingParams)
      );

      Optional.ofNullable(_consumoForMatchingParams.getTransactionDetailCode())
              .ifPresent(transactionDetailCode -> stanziamentiParamsQuery.setTransactionDetail(transactionDetailCode));

      stanziamentiParamsQuery.setFareClass(_consumoForMatchingParams.getSupplierVehicleFareClass().orElse(null));
      
      Optional.ofNullable(_consumoForMatchingParams.getServicePartner())
              .ifPresent(fareClass -> stanziamentiParamsQuery.setCodiceFornitoreLegacy(_consumoForMatchingParams.getServicePartner()
                                                                                                                .getPartnerCode()));

      final Optional<StanziamentiParams> optionalStanziamentiParams = stanziamentiParamsService.findByQuery(stanziamentiParamsQuery);
      if (!optionalStanziamentiParams.isPresent()) {
        validationOutcome.addMessage(new Message("404").withText(String.format("Params used: %s ",
                                                                               stanziamentiParamsQuery.toString())));
      } else {
        validationOutcome.setStanziamentiParams(optionalStanziamentiParams.get());
      }

    } else {
      validationOutcome.addMessage(new Message("405").withText("Consumo is null"));
    }

    if(validationOutcome.getStanziamentiParamsOptional().isPresent())
      checkAndProcessCodiceClienteFai(validationOutcome,_consumoForMatchingParams);

    return validationOutcome;
  }

  private boolean isVatMandatoryForThisConsumo(ParamStanziamentoMatchingParamsSupplier _consumoForMatchingParams) {
    String sourceType = _consumoForMatchingParams.getSourceType();

    return !(
      Optional.ofNullable(applicationProperties)
        .map(a-> a.getOptionalVatConfiguration())
        .map(optionalVatConfiguration -> optionalVatConfiguration.get(sourceType.toUpperCase()))
        .filter(legacyCodes -> legacyCodes.contains(_consumoForMatchingParams.getServicePartner().getPartnerCode()))
        .isPresent()
    );
  }

  // Il codice cliente FAI genera solo COSTI:
  // * I CR vengono trasformati in C
  // * Gli R vengono scartati
  private void checkAndProcessCodiceClienteFai(ValidationOutcome validationOutcome, final ParamStanziamentoMatchingParamsSupplier _consumoForMatchingParams) {

    if(_consumoForMatchingParams instanceof TrustedCustomerSupplier) {

      Customer customer = ((TrustedCustomerSupplier) _consumoForMatchingParams).getCustomer();

      if(customer!=null && applicationProperties !=null && applicationProperties.getCodiceClienteFai()!=null
          &&  applicationProperties.getCodiceClienteFai().equals(customer.getId())) {
        switch (validationOutcome.getStanziamentiParamsOptional().get().getCostoRicavo()) {
        case CR:
          validationOutcome.getStanziamentiParamsOptional().get().setCostoRicavo(CostoRicavo.C);
          break;
        case R:
          validationOutcome.addMessage(new Message("406").withText("For codice cliente FAI, Solo Ricavo must be skipped"));
          validationOutcome.setStanziamentiParams(null);
          break;
        default:
          break;
        }
      }
    }
  }
  
  @Override
  public void checkNoBlockingValidationPaginated(List<ConsumoNoBlockingData> noBlockingValidationData, String fileName, String ingestion_time, String servicePartner) {
    //TODO mettere in coda e processare in asyncrono
    for (ConsumoNoBlockingData consumoNoBlockingData : noBlockingValidationData) {
      consumoNoBlockingValidatorService.validateAndNotify(consumoNoBlockingData, fileName, ingestion_time, servicePartner);
    }
  }
  
  @Override
  public void checkNoBlockingValidationOnStanziamentoPaginated(Set<String> codiciStanziamento) {
    //TODO mettere in coda e processare in asyncrono
    consumoNoBlockingValidatorService.validateStanziamenti(codiciStanziamento);
  }

  @Override
  @Cacheable(value = "findConsumoTypeByTracciatoAndTransactionDetail", key = "{#root.methodName, #root.args[0], #root.args[1]}")
  public Optional<StanziamentiDetailsType> findConsumoTypeByTracciatoAndTransactionDetail(Format format, String transactionDetail){
    return stanziamentiParamsService.findConsumoTypeByTracciatoAndTransactionDetail(format, transactionDetail);
//    _log.debug("findConsumoTypeByTracciatoAndTransactionDetail based on format:=[{}], record:=[{}]", format, transactionDetail);
//    switch (transactionDetail) {
//      case "58":
//      case "60":
//      case "0A":
//      case "0B":
//        _log.info("Chosen StanziamentiDetailsType.GENERICO");
//        return Optional.of(StanziamentiDetailsType.GENERICO);
//      default:
//        _log.info("Chosen StanziamentiDetailsType.PEDAGGI");
//        return Optional.of(StanziamentiDetailsType.PEDAGGI);
//    }
  }
}
