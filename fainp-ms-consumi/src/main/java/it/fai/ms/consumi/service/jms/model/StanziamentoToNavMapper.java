package it.fai.ms.consumi.service.jms.model;

import it.fai.ms.consumi.domain.navision.Stanziamento;
import it.fai.ms.consumi.domain.navision.Stanziamento.GeneraStanziamentoEnum;
import it.fai.ms.consumi.domain.navision.Stanziamento.StatoStanziamentoEnum;
import it.fai.ms.consumi.domain.navision.Stanziamento.TipoFlussoEnum;
import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Component
public class StanziamentoToNavMapper {

  public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";
  private DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DATE_FORMAT_PATTERN);

  public StanziamentoToNavMapper() {
  }

  public Stanziamento toDomainNav(final StanziamentoMessage _allocationMessage) {

    final var stanziamento = new Stanziamento();

    final var dataErogazione = dtf.format(_allocationMessage.getDataErogazioneServizio());
    final var dataRegistrazione = dtf.format(now());
    final var dataAcquisizioneFlusso = FaiConsumiDateUtil.formatInstant(DATE_FORMAT_PATTERN, _allocationMessage.getDataAcquisizioneFlusso());

    stanziamento.codiceStanziamento(_allocationMessage.getCode())
                .daRifatturare(true)
                .annoStanziamento(_allocationMessage.getAnnoStanziamento())
                .classeVeicoloEuro(_allocationMessage.getClasseEuroVeicolo())
                .costo(Optional.ofNullable(_allocationMessage.getCosto()).orElse(0d))
                .descrizioneAggiuntiva(_allocationMessage.getDescriptions()[0])
                .generaStanziamento(getGeneraStanziamentoEnum(_allocationMessage.getGeneraStanziamento()))
                .nr_(_allocationMessage.getNr() * 1.0)
                .nrArticolo(_allocationMessage.getNrArticolo())
                .nrCliente(_allocationMessage.getNrCliente())
                .nrFornitore(_allocationMessage.getNrFornitore())
                .paese(_allocationMessage.getPaese())
                .prezzo(Optional.ofNullable(_allocationMessage.getPrezzo()).orElse(0d))
                .quantit(_allocationMessage.getQuantita())
                .targa(_allocationMessage.getTarga())
                .valuta(("EUR".equals(_allocationMessage.getValuta())
                         || _allocationMessage.getValuta() == null) ? "" : _allocationMessage.getValuta()) // se EUR o
                .statoStanziamento(StatoStanziamentoEnum.fromValue(String.valueOf(_allocationMessage.getStatoStanziamento())))
                .tipoFlusso(TipoFlussoEnum.fromValue(String.valueOf(_allocationMessage.getTipoFlusso())))
                .dataErogazioneServizio(dataErogazione)
                .dataRegistrazione(dataRegistrazione)
                .conguaglio(_allocationMessage.isConguaglio())
                .cdStanzProv(_allocationMessage.getCodicestanziamentoProvvisorio())
                .nrDocFor(_allocationMessage.getDocumentoDaFornitore())
                .nazioneTarga(_allocationMessage.getNazioneTarga())
                .dataAcquisizioneFlusso(dataAcquisizioneFlusso);
    return stanziamento;
  }

  private GeneraStanziamentoEnum getGeneraStanziamentoEnum(int value) {
    GeneraStanziamentoEnum rs = GeneraStanziamentoEnum.fromValue(String.valueOf(value));
    return (rs == null ) ?  GeneraStanziamentoEnum.NUMBER_1 : rs;
  }

  private static LocalDate now() {
    return LocalDate.ofInstant(Instant.now(), ZoneOffset.UTC);
  }

}
