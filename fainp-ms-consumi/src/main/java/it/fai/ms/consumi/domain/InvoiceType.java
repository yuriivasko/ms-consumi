package it.fai.ms.consumi.domain;

public enum InvoiceType {

  P("PROVVISORIO"), D("DEFINITIVO");

  public final String extendedName;

  InvoiceType(String extendedName) {
    this.extendedName = extendedName;
  }
}
