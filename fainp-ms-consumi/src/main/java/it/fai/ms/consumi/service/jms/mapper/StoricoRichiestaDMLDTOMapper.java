package it.fai.ms.consumi.service.jms.mapper;

import org.springframework.stereotype.Component;

import it.fai.ms.common.dml.efservice.dto.StoricoRichiestaDMLDTO;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoRichiesta;

@Component
public class StoricoRichiestaDMLDTOMapper {
  
  // TODO : campi non asssegnati

  public StoricoRichiestaDMLDTO toDTO(final StoricoRichiesta data) {
    StoricoRichiestaDMLDTO dto = new StoricoRichiestaDMLDTO();
    dto.setAnomalia(data.getAnomalia());
    dto.setAssociazione(data.getAssociazione());
    // data.getDataUltimaVariazione();
    dto.setDispositivoDmlUniqueIdentifier(data.getDispositivoDmlUniqueIdentifier());
    dto.setDmlRevisionTimestamp(data.getDmlRevisionTimestamp());
    dto.setRichiestaDmlUniqueIdentifier(data.getRichiestaDmlUniqueIdentifier());
    dto.setOrdineClienteDmlUniqueIdentifier(data.getOrdineClienteDmlUniqueIdentifier());
    dto.setStatoRichiesta(data.getStatoRichiesta());
    dto.setTipoDispositivo(data.getTipoDispositivo());
    dto.setTipoRichiesta(data.getTipoRichiesta());
    return dto;
  }

//  public RichiestaDMLDTO toDTO(final StoricoRichiesta data) {
//    RichiestaDMLDTO dto = new RichiestaDMLDTO();
//    dto.setAnomalia(data.getAnomalia());
//    dto.setAssociazione(data.getAssociazione());
//    // data.getDataUltimaVariazione();
//    dto.setDispositivoDmlUniqueIdentifier(data.getDispositivoDmlUniqueIdentifier());
//    dto.setDmlRevisionTimestamp(data.getDmlRevisionTimestamp());
//    //dto.setRichiestaDmlUniqueIdentifier(data.getRichiestaDmlUniqueIdentifier());
//    dto.setUuid(data.getRichiestaDmlUniqueIdentifier());
//    dto.setOrdineClienteDmlUniqueIdentifier(data.getOrdineClienteDmlUniqueIdentifier());
//    dto.setStatoRichiesta(data.getStatoRichiesta());
//    dto.setTipoDispositivo(data.getTipoDispositivo());
//    dto.setTipoRichiesta(data.getTipoRichiesta());
//    return dto;
//  }

}
