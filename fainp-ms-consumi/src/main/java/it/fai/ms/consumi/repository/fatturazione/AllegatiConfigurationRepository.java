package it.fai.ms.consumi.repository.fatturazione;

import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.repository.fatturazione.model.AllegatiConfigurationEntity;

public interface AllegatiConfigurationRepository {

  AllegatiConfigurationEntity findByCodiceRaggruppamentoArticoli(@NotNull String codiceRaggrArticoli);

  AllegatiConfigurationEntity save(@NotNull AllegatiConfigurationEntity allegatiConfig);

}
