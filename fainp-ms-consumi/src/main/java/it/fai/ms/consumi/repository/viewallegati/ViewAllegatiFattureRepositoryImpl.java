package it.fai.ms.consumi.repository.viewallegati;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.util.SqlUtil;

@Repository
public class ViewAllegatiFattureRepositoryImpl implements ViewAllegatiFattureRepository {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final EntityManager em;

	public ViewAllegatiFattureRepositoryImpl(final EntityManager _em) {
		em = _em;
	}

	@Override
	public Set<ViewAllegatiFattureEntity> findByCodiceStanziamentoIn(List<String> codiciStanziamenti) {
		final var cb = em.getCriteriaBuilder();
		var query = cb.createQuery(ViewAllegatiFattureEntity.class);
		var root = query.from(ViewAllegatiFattureEntity.class);

		query.select(root);
		query.where(root.get(ViewAllegatiFattureEntity_.CODICE_STANZIAMENTO).in(codiciStanziamenti));

		return em.createQuery(query).getResultStream().collect(toSet());
	}

	@Override
	public Map<String, ViewAllegatiFattureEntity> findByCodiceStanziamentoInAndRangeDate(
			List<String> codiciStanziamenti, Instant dateFrom, Instant dateTo) {
		final var cb = em.getCriteriaBuilder();
		var query = cb.createQuery(ViewAllegatiFattureEntity.class);
		var root = query.from(ViewAllegatiFattureEntity.class);

		query.select(root);
		List<Predicate> predicates = new ArrayList<>();
		Predicate p = root.get(ViewAllegatiFattureEntity_.CODICE_STANZIAMENTO).in(codiciStanziamenti);
		predicates.add(p);

		Predicate pDate = null;
		if (dateFrom == null) {
			pDate = cb.lessThanOrEqualTo(root.get(ViewAllegatiFattureEntity_.DATA_EROGAZIONE_SERVIZIO), dateTo);
		} else {
			Predicate pDateFrom = cb.greaterThanOrEqualTo(root.get(ViewAllegatiFattureEntity_.DATA_EROGAZIONE_SERVIZIO),
					dateFrom);
			Predicate pDateTo = cb.lessThanOrEqualTo(root.get(ViewAllegatiFattureEntity_.DATA_EROGAZIONE_SERVIZIO),
					dateTo);
			pDate = cb.and(pDateFrom, pDateTo);
		}
		predicates.add(pDate);

		query.where(predicates.toArray(new Predicate[] {}));

		return em.createQuery(query).getResultStream()
				.collect(toMap(ViewAllegatiFattureEntity::getCodiceStanziamento, v -> v));
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(isolation = Isolation.READ_UNCOMMITTED, readOnly = true, propagation = Propagation.NOT_SUPPORTED)
	public BigDecimal findSumAmountNoVatDettagliStaziamentoByCodiciStanziamenti(List<String> _codiciStanziamento) {

		if (_codiciStanziamento.isEmpty()) {
			return BigDecimal.ZERO;
		}

		List<List<String>> splittedData = SqlUtil.splitToSubList(_codiciStanziamento, 10000);
		BigDecimal result = new BigDecimal(0);

		for (List<String> codiciStanziamento : splittedData) {

			// @formatter:off
			String query = "SELECT SUM( CASE prezzo WHEN NULL  THEN 0 ELSE prezzo END * quantita) AS total_price FROM stanziamento WHERE codice_stanziamento IN (§params§);";
			// @formatter:on

			List<String> list = codiciStanziamento.stream().map(code -> "'" + code + "'").collect(toList());
			query = query.replaceAll("§params§", String.join(",", list));

			log.debug("Query: {}", query);

			Query nativeQuery = em.createNativeQuery(query);
			List<BigDecimal> resultList = null;
			try {
				resultList = nativeQuery.getResultList();
				if (resultList != null && !resultList.isEmpty()) {
					BigDecimal rs = resultList.get(0);
					if (rs != null) {
						result = result.add(rs);
					}
				} else {
					log.warn("Not found result");
				}
			} catch (Exception e) {
				log.error("Error DB: ", e);
			}
		}
		return result;
	}

}
