package it.fai.ms.consumi.repository.storico_dml.jms;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.common.dml.mappable.AbstractMappableDmlListener;
import it.fai.ms.common.dml.vehicle.dto.VehicleDMLDTO;
import it.fai.ms.common.jms.JmsTopicListener;
import it.fai.ms.common.jms.JmsTopicNames;
import it.fai.ms.consumi.repository.storico_dml.StoricoVeicoloRepository;
import it.fai.ms.consumi.repository.storico_dml.jms.mapper.StoricoVeicoloDmlListenerMapper;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoVeicolo;

@Service
@Transactional
@Profile("!no-jmslistener")
public class JmsDmlStoricoVeicoloListener
  extends AbstractMappableDmlListener<VehicleDMLDTO, StoricoVeicolo>  implements JmsTopicListener {
  
  public JmsDmlStoricoVeicoloListener(StoricoVeicoloRepository repository,
                                     StoricoVeicoloDmlListenerMapper mapper,
                                      ApplicationEventPublisher publisher){
    super(VehicleDMLDTO.class, repository, mapper,publisher,false);
  }

  @Override
  public JmsTopicNames getTopicName() {
    return JmsTopicNames.DML_VEHICLE_SAVE;
  }
}
