package it.fai.ms.consumi.service.validator.internal;

import it.fai.common.enumeration.StatoDispositivo;
import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.common.jms.JmsProperties;
import it.fai.ms.common.jms.dto.notification.NotificationAziendaDTO;
import it.fai.ms.common.jms.dto.notification.NotificationDispositivoDTO;
import it.fai.ms.common.jms.notification_v2.DispositiviBloccatiFattureMessage;
import it.fai.ms.common.jms.notification_v2.NotificationMessageClient;
import it.fai.ms.consumi.domain.Contract;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoStatoServizio;
import it.fai.ms.consumi.repository.storico_dml.model.ViewStoricoDispositivoVeicoloContratto;
import it.fai.ms.consumi.service.DeviceService;
import it.fai.ms.consumi.service.util.FaiConsumiDateUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.function.BooleanSupplier;

@Service
public class DeviceValidatorImpl
  implements DeviceValidator {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DeviceService             deviceService;
  private final boolean                   validatorEnabled;
  private final NotificationMessageClient notificationClient;

  @Inject
  public DeviceValidatorImpl(final DeviceService _deviceService, final JmsProperties _jmsProperties,
                             @Value("#{new Boolean(${validator.device:false})}") final boolean _validatorEnabled) {
    deviceService = _deviceService;
    validatorEnabled = _validatorEnabled;
    _log.debug("Validator enabled 'validator.device' : {}", _validatorEnabled);
    notificationClient = new NotificationMessageClient(_jmsProperties);
  }

  @Override
  /**
   * If device is null or empty or device pan and both device pan and device serial number are null, then it will
   * return null.
   * Otherwise it performs the
   */
  public ValidationOutcome findAndSetDevice(final Device device) {


    var validationOutcome = new ValidationOutcome();
    boolean deviceSerialeEmpty = areNotAvailableDeviceData(device);
    boolean devicePanEmpty = areNotAvailablePANData(device);
    if (device == null || devicePanEmpty && deviceSerialeEmpty) {
      return null;
    }


    final String _deviceSeriale = device.getId();
    final TipoDispositivoEnum _deviceType = device.getType();

    final String _pan = device.getPan();
    final TipoServizioEnum _serviceType = device.getServiceType();
    
    final String _contract = device.getContractNumber();


    Optional<Device> optionalDevice = devicePanEmpty ?
      deviceService.findDeviceBySeriale(_deviceSeriale,_deviceType, _contract) :
      deviceService.findDeviceByPan(_pan, _serviceType, _contract);


    if (!optionalDevice.isPresent()) {
    	if(devicePanEmpty) {
    		validationOutcome.addMessage(new Message("b001").withText("Device " + _deviceType + " with serialCode "+_deviceSeriale
                                                                  + (_contract!=null ? " and contract "+_contract : "") + " not found"));
    	}else {
    		validationOutcome.addMessage(new Message("b001").withText("Device " + _deviceType + " with pan/service "+_pan+"/"+_serviceType
                    + (_contract!=null ? " and contract "+_contract : "") + " not found"));
    	}
    }else {
      validationOutcome.addMessage(
        validateAndSetDeviceFields(device, optionalDevice, () ->
          device.getType() == null || device.getSeriale() == null || device.getSeriale().isEmpty()
        ));
      _log.debug("Device exists validator result : {}", validationOutcome);
    }

    if (!validatorEnabled) {
      validationOutcome = new ValidationOutcome();
      _log.debug("Device exists validator disabled, response overrided : {}", validationOutcome);
    }

    return validationOutcome;
  }

  private Message validateAndSetDeviceFields(Device device, Optional<Device> optionalDevice, BooleanSupplier match) {
    if (optionalDevice.isPresent()) {
      device.setType(optionalDevice.get()
        .getType());
      device.setContractNumber(optionalDevice.get()
        .getContractNumber());
      device.setSeriale(optionalDevice.get()
        .getSeriale());
      device.setVehicleUUID(optionalDevice.get()
        .getVehicleUUID());
      device.setDeviceUUID(optionalDevice.get()
        .getDeviceUUID());

      if (match.getAsBoolean()) {
        return deviceNotValidFieldMessage(device);
      }
    }
    return null;
  }

  private Message deviceNotValidFieldMessage(Device device) {
    return new Message("b001").withText("Device hasn't valid fields: " + device);
  }

  private boolean areNotAvailableDeviceData(Device device) {
    return device.getType() == null || StringUtils.isBlank(device.getId());
  }

  private boolean areNotAvailablePANData(Device device) {
    return device.getServiceType() == null || StringUtils.isBlank(device.getPan());
  }

  @Override
  public ValidationOutcome findAndSetDevice(final Vehicle vehicle, final Device device, final Instant date) {
    var validationOutcome = new ValidationOutcome();

    if (vehicle.getLicensePlate() == null
        || vehicle.getLicensePlate().getLicenseId() == null
        || vehicle.getLicensePlate().getLicenseId().isEmpty()
        || device == null
        || device.getType() == null
        || date == null) {
      return null;
    }

    final var optionalDevice = deviceService.findDeviceByLicensePlate(vehicle.getLicensePlate().getLicenseId(), device.getType(), date);

    if (!optionalDevice.isPresent()) {
      validationOutcome.addMessage(new Message("b001").withText("Device " + device.getType() + " with plate "
                                                                + vehicle.getLicensePlate()
                                                                                           .getLicenseId()
                                                                + " at date " + date + " not found"));
    }

    validationOutcome.addMessage(
      validateAndSetDeviceFields(device, optionalDevice, ()->
        device.getType() == null || device.getSeriale() == null || device.getSeriale().isEmpty()|| device.getContractNumber() == null || device.getContractNumber().isEmpty()
        )
    );

    _log.debug("Device exists validator result : {}", validationOutcome);
    if (!validatorEnabled) {
      validationOutcome = new ValidationOutcome();
      _log.debug("Device exists validator disabled, response overrided : {}", validationOutcome);
    }

    return validationOutcome;
  }

  @Override
  @Deprecated
  public ValidationOutcome deviceIsAssociatedWithContract(final Device _device, final Contract _contract) {

    _log.trace("Check if device {} has a transaction for contract {}", _device, _contract);

    ValidationOutcome rs = new ValidationOutcome();
    if (_device.getContractNumber() == null || !_device.getContractNumber()
                                                   .equals(_contract.getId())) {
     String msg = "Device " + _device.getSeriale() + " with contract "
             + _device.getContractNumber() + " is different from record contract number "
             + _contract.getId();
      _log.debug(msg);
      rs.addMessage(new Message("b005").withText(msg));

    }
    return rs;
  }

  @Override
  public ValidationOutcome deviceWasActiveAndAssociatedWithContractInDate(final String _deviceSeriale,
                                                                          final TipoDispositivoEnum _tipoDispositivo,
                                                                          final String _contractUuid,
                                                                          final Instant date,
                                                                          final Duration offset) {
    ValidationOutcome validationOutcome = new ValidationOutcome();

    _log.trace("Check if device {} was active and assocaited with contract {} in date {}", _deviceSeriale, _contractUuid, date);
    Optional<ViewStoricoDispositivoVeicoloContratto> storicoStatoDispositivo = deviceService.findStoricoDispositivo(_deviceSeriale, _tipoDispositivo,
                                                                                                                    date.plus(offset));
    final String formattedDate = FaiConsumiDateUtil.formatInstant(date, offset);
    if (storicoStatoDispositivo.isPresent()) {
      if (!StatoDispositivo.isActiveStatus(storicoStatoDispositivo.get().getStato())) {
        validationOutcome.addMessage(new Message("w003")
                                       .withText("Device " + _deviceSeriale + "/" + _tipoDispositivo + " is not active in date " + formattedDate));
      }
      return checkContractOnStorico(storicoStatoDispositivo.get(), _deviceSeriale, _tipoDispositivo, _contractUuid, formattedDate);
    } else {
      validationOutcome.addMessage(new Message("w004")
                                     .withText("Device " + _deviceSeriale + "/" + _tipoDispositivo + " not found in date " + formattedDate));
    }

    return validationOutcome;
  }

  @Override
  public ValidationOutcome deviceWasAssociatedWithContractInDate(final String _deviceSeriale,
                                                                 final TipoDispositivoEnum _tipoDispositivo,
                                                                 final String _contractUuid,
                                                                 final Instant date,
                                                                 final Duration offset) {
    final String formattedDate = FaiConsumiDateUtil.formatInstant(date, offset);
    ValidationOutcome validationOutcome = new ValidationOutcome();

    _log.trace("Check if device {} has a transaction for contract {} in date {}", _deviceSeriale, _contractUuid, date);
    Optional<ViewStoricoDispositivoVeicoloContratto> storicoStatoDispositivo = deviceService.findStoricoDispositivo(_deviceSeriale, _tipoDispositivo,
                                                                                                                    date.plus(offset));
    if (storicoStatoDispositivo.isPresent()) {
      return checkContractOnStorico(storicoStatoDispositivo.get(), _deviceSeriale, _tipoDispositivo, _contractUuid, formattedDate);
    } else {
      validationOutcome.addMessage(new Message("w004")
                                     .withText("Device " + _deviceSeriale + "/" + _tipoDispositivo + " not found in date " + formattedDate));
    }
    return validationOutcome;
  }

  private ValidationOutcome checkContractOnStorico(final ViewStoricoDispositivoVeicoloContratto storicoStatoDispositivo,
                                                   final String _deviceSeriale,
                                                   final TipoDispositivoEnum _tipoDispositivo,
                                                   final String _contractUuid,
                                                   final String formattedDate) {
    ValidationOutcome validationOutcome = new ValidationOutcome();

    if (!_contractUuid.equals(storicoStatoDispositivo.getContrattoNumero())) {
      validationOutcome.addMessage(new Message("w002").withText("Device " + _deviceSeriale + "/" + _tipoDispositivo
                                                                + " in date " + formattedDate + " have contract "
                                                                + storicoStatoDispositivo.getContrattoNumero()
                                                                + ", while consumo contract is " + _contractUuid));
    }
    return validationOutcome;
  }

  public ValidationOutcome serviceWasActiveInDate(Device device, Instant date, Duration offset) {
    Optional.ofNullable(device).orElseThrow(() -> new IllegalArgumentException("Cannot check if serviceWasActiveInDate, device is null!"));
    Optional.ofNullable(date).orElseThrow(() -> new IllegalArgumentException("Cannot check if serviceWasActiveInDate, date cannot be null!"));

    final ValidationOutcome validationOutcome = new ValidationOutcome();
    final String formattedDate = FaiConsumiDateUtil.formatInstant(date, offset);

    _log.trace("Check if device {} had service active in date {}", device, date);

    if (StringUtils.isBlank(device.getDeviceUUID()) || device.getServiceType() == null) {
      validationOutcome
        .addMessage("w005")
        .withText(stringBuilder -> {
          stringBuilder.append("Cannot search if service was active in date [" + formattedDate + "]");
          stringBuilder.append("cause missing device uuid or service type!");
          stringBuilder.append("Device [" + device.getSeriale() + "/" + device.getType() + "]");
          if (StringUtils.isNotBlank(device.getPan())){
            stringBuilder.append("with pan [" + device.getPan() + "]");
          }
          stringBuilder.append("service [" + device.getServiceType() + "]");
        });
      return validationOutcome;
    }

    Optional<StoricoStatoServizio> storicoStatoServizio = deviceService.findStoricoStatoServizio(device.getDeviceUUID(), device.getServiceType(), date.plus(offset));
    if (!storicoStatoServizio.isPresent() || !"ATTIVO".equals(storicoStatoServizio.get().getStato())) {
      validationOutcome.addMessage(new Message("w005").withText(
        "Service [" + device.getServiceType() + "] with pan [" + device.getPan() + "] for device ["+ device.getSeriale() + "/" + device.getType() + "] in date [" + formattedDate + "] was not active"
      ));
    }

    return validationOutcome;
  }


  @Override
  public void sendDispositiviBloccatiFattureMessage(final Consumo _consumo, final String _deviceSeriale,
                                                    final TipoDispositivoEnum _tipoDispositivo) {

    Optional<ViewStoricoDispositivoVeicoloContratto> lastStoricoDispositivo = deviceService.findStoricoDispositivo(_deviceSeriale,
                                                                                                              _tipoDispositivo,
                                                                                                              Instant.now());

    DispositiviBloccatiFattureMessage msg = new DispositiviBloccatiFattureMessage();
    msg.setDataTransito(LocalDate.ofInstant(_consumo.getDate(), ZoneOffset.UTC));
    msg.setDataUltimaVariazioneStatoDispositivo(lastStoricoDispositivo.map(s -> LocalDate.ofInstant(s.getDataVariazione(),
                                                                                                         ZoneOffset.UTC))
                                                                           .orElse(null));
    msg.setImportoTransito(null);
    if (_consumo.getAmount() != null && _consumo.getAmount()
                                                .getAmountExcludedVat() != null) {
      msg.setImportoTransito(_consumo.getAmount()
                                     .getAmountExcludedVat()
                                     .getNumber()
                                     .numberValueExact(Float.class));
    }
    msg.setNotificationAziendaDTO(new NotificationAziendaDTO());
    msg.getNotificationAziendaDTO()
       .setCodiceCliente(_consumo.getContract()
                                 .map(Contract::getCompanyCode)
                                 .orElse(null));
    msg.setNotificationDispositivoDTO(lastStoricoDispositivo.map(s -> mapTo(s))
                                                                 .orElse(null));
    ;

    msg.setServizioTransito(_consumo.getGroupArticlesNav());
    msg.setText("Dispostivo " + _deviceSeriale + "/" + _tipoDispositivo + " non era attivo in data " + _consumo.getDate());

    notificationClient.sendNotification(msg);
  }
  
  @Override
  public void sendDispositiviBloccatiFattureMessage(final String _deviceSeriale, final TipoDispositivoEnum _tipoDispositivo, Instant consumoDate, String companyCode, String groupArticlesNav, Float amountExcludedVat) {
    
    

    Optional<ViewStoricoDispositivoVeicoloContratto> lastStoricoDispositivo = deviceService.findStoricoDispositivo(_deviceSeriale,
                                                                                                              _tipoDispositivo,
                                                                                                              Instant.now());

    DispositiviBloccatiFattureMessage msg = new DispositiviBloccatiFattureMessage();
    msg.setDataTransito(LocalDate.ofInstant(consumoDate, ZoneOffset.UTC));
    msg.setDataUltimaVariazioneStatoDispositivo(lastStoricoDispositivo.map(s -> LocalDate.ofInstant(s.getDataVariazione(),
                                                                                                         ZoneOffset.UTC))
                                                                           .orElse(null));
    msg.setImportoTransito(amountExcludedVat);
    
    msg.setNotificationAziendaDTO(new NotificationAziendaDTO());
    msg.getNotificationAziendaDTO().setCodiceCliente(companyCode);
    msg.setNotificationDispositivoDTO(lastStoricoDispositivo.map(s -> mapTo(s))
                                                                 .orElse(null));

    msg.setServizioTransito(groupArticlesNav);
    msg.setText("Dispostivo " + _deviceSeriale + "/" + _tipoDispositivo + " non era attivo in data " + consumoDate);

    notificationClient.sendNotification(msg);
  }

  private NotificationDispositivoDTO mapTo(ViewStoricoDispositivoVeicoloContratto disp) {
    NotificationDispositivoDTO dto = new NotificationDispositivoDTO();
    dto.setAssociazione(disp.getVeicoloTarga());
    dto.setSeriale(disp.getSerialeDispositivo());
    dto.setTipoDispositivo(disp.getTipoDispositivo());
    dto.setUuidDispositivo(disp.getDispositivoDmlUniqueIdentifier());
    return dto;
  }

}
