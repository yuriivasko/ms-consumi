package it.fai.ms.consumi.client;

import java.time.Instant;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.common.jms.dto.petrolpump.PriceTableDTO;
import it.fai.ms.consumi.config.ApplicationProperties;

@Service
public class PetrolPumpServiceImpl
 implements PetrolPumpService{

  private final PetrolPumpClient petrolPumpClient;
  private final ApplicationProperties applicationProperties;



  public PetrolPumpServiceImpl(PetrolPumpClient petrolPumpClient, final ApplicationProperties applicationProperties) {
    this.petrolPumpClient = petrolPumpClient;
    this.applicationProperties = applicationProperties;
  }

  @Override
  public List<PriceTableDTO> getPriceTableList(String authorizationToken, String codiceFornitore, String codiceArticolo, Instant date) {
    return petrolPumpClient.getPriceTableList(authorizationToken, codiceFornitore, codiceArticolo, date);
  }

  @Override
  @Cacheable(value = "pointAllegati", key = "{#root.methodName, #root.args[0]}")
  public PointDTO getPoint(String code) {
    return petrolPumpClient.getPoint(applicationProperties.getAuthorizationHeader(), code);
  }

}
