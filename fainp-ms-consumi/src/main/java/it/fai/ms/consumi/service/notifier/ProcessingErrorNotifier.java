package it.fai.ms.consumi.service.notifier;

import it.fai.ms.consumi.domain.validation.ValidationOutcome;

public interface ProcessingErrorNotifier {

  void notify(ValidationOutcome validationOutcome);

}
