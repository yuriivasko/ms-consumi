package it.fai.ms.consumi.service.navision;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.client.AuthorizationApiService;
import it.fai.ms.consumi.client.StanziamentiApiService;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.config.RestClientProperties;
import it.fai.ms.consumi.domain.navision.PutAllegatiByNavReturnDTO;

@Service
@Transactional
public class PutAllegatiToNavService {

  private Logger log = LoggerFactory.getLogger(getClass());

  @Autowired
  private StanziamentiApiService stanziamentiApi;

  @Autowired
  private AuthorizationApiService authorizationApiService;

  private ApplicationProperties applicationProperties;

  public PutAllegatiToNavService(final ApplicationProperties applicationProperties) {
    this.applicationProperties = applicationProperties;
  }

  public PutAllegatiByNavReturnDTO putAllegatiByNav(String numeroFattura, List<String> codiciAllegati) {
    log.debug("putAllegatiByNav: \nNumeroFattura: {} \nCodiciAllegati: {} ", numeroFattura, codiciAllegati);

    RestClientProperties restclient = applicationProperties.getRestclient();
    String authorizationToken = authorizationApiService.getAuthorizationToken(restclient.getGrant_type(), restclient.getUsername(),
                                                                              restclient.getPassword());
    log.info("Send codici allegati {} to Invoice {} to NAV.", codiciAllegati, numeroFattura);
    PutAllegatiByNavReturnDTO res = stanziamentiApi.putAllegatiByNav(authorizationToken, numeroFattura, codiciAllegati);

    log.debug("putAllegatiByNavPut Res {}", res);
    return res;
  }

}
