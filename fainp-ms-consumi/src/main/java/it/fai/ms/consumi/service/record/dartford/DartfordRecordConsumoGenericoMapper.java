package it.fai.ms.consumi.service.record.dartford;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.*;
import it.fai.ms.consumi.domain.consumi.generici.dartford.Dartford;
import it.fai.ms.consumi.domain.consumi.generici.dartford.DartfordGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.generici.DartfordRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

import static it.fai.ms.consumi.domain.VehicleLicensePlate.NO_COUNTRY;

@Component
public class DartfordRecordConsumoGenericoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<DartfordRecord, Dartford> {

  /*
   * (non-Javadoc)
   * @see it.fai.ms.consumi.service.record.telepass.RecordConsumoMapper#mapRecordToConsumo(it.fai.ms.consumi.repository.
   * flussi.record.model.CommonRecord)
   */
  @Override
  public Dartford mapRecordToConsumo(DartfordRecord record) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(record, yyyy_MM_dd_dateformat, null);
    Instant ingestionTime = record.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(record.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, "dartford");
    source.setFileName(record.getFileName());

    source.setRowNumber(record.getRowNumber());

    Dartford consumo = new Dartford(source, record.getRecordCode(), Optional.of(new DartfordGlobalIdentifier(record.getGlobalIdentifier())), record);

    Optional<Instant> dataTransazione = calculateInstantByDateAndTime(record.getCrossingDate(), record.getTime(), "dd/MM/yyyy", "HH:mm");

    dataTransazione.ifPresent(it -> {
      consumo.setStartDate(it);
      consumo.setEndDate(it);
    });

    Vehicle vehicle = new Vehicle();
    vehicle.setLicensePlate(new VehicleLicensePlate(record.getVehicle(), NO_COUNTRY));
    consumo.setVehicle(vehicle);
    consumo.setLicencePlateOnRecord(record.getVehicle());

    String customerId = record.getGroupName().trim();
    Customer customer = new Customer(customerId);
    consumo.setCustomer(customer);

    consumo.setQuantity(BigDecimal.ONE);

    // CONSUMO.AMOUNT
    Amount amount = getAmount(record.getDefaultCurrency());
    consumo.setAmount(amount);

    Transaction _transaction = new Transaction();
    _transaction.setSign("+");
    consumo.setTransaction(_transaction);

//    Contract contract = Contract.newUnsafeContract(null, customerId, null);
//    consumo.setContract(Optional.ofNullable(contract));

    consumo.setRoute(record.getDirection());

    Device device = new Device(TipoDispositivoEnum.DARTFORD_CROSSING);
    device.setServiceType(TipoServizioEnum.PEDAGGI_INGHILTERRA);
    consumo.setDevice(device);

    return consumo;

  }

  private Amount getAmount(String currency) {
    Amount amount = new Amount();
    var importo = MonetaryUtils.strToMonetary("0",currency );
    amount.setVatRate(Optional.empty());
    amount.setAmountIncludedVat(importo);
    return amount;
  }


}
