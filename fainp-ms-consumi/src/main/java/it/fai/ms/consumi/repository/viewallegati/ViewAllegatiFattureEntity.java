package it.fai.ms.consumi.repository.viewallegati;

import java.time.Instant;

import javax.annotation.concurrent.Immutable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import it.fai.common.enumeration.TipoDispositivoEnum;

@Entity
@Table(name = "view_allegati_fatture")
@IdClass(ViewAllegatiFattureId.class)
@Immutable
public class ViewAllegatiFattureEntity {

  @Id
  @Column(name = "codice_stanziamento")
  private String codiceStanziamento;

  @Id
  @Column(name = "codice_contratto")
  private String codiceContratto;

  @Id
  @Column(name = "device_type")
  private TipoDispositivoEnum deviceType;

  @Id
  @Column(name = "pan_number")
  private String panNumber;
  
  @Id
  @Column(name = "serial_number")
  private String serialNumber;

  @Id
  @Column(name = "veicolo_nazione_targa")
  private String countryLicensePlate;

  @Id
  @Column(name = "veicolo_targa")
  private String licensePlate;

  @Column(name = "data_erogazione_servizio")
  private Instant dataErogazioneServizio;

  @Column(name = "amount")
  private Double totale;

  public String getCodiceStanziamento() {
    return codiceStanziamento;
  }

  public void setCodiceStanziamento(String codiceStanziamento) {
    this.codiceStanziamento = codiceStanziamento;
  }

  public String getCodiceContratto() {
    return codiceContratto;
  }

  public void setCodiceContratto(String codiceContratto) {
    this.codiceContratto = codiceContratto;
  }

  public TipoDispositivoEnum getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(TipoDispositivoEnum deviceType) {
    this.deviceType = deviceType;
  }

  public String getPanNumber() {
    return panNumber;
  }

  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }

  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public String getCountryLicensePlate() {
    return countryLicensePlate;
  }

  public void setCountryLicensePlate(String countryLicensePlate) {
    this.countryLicensePlate = countryLicensePlate;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  public Instant getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public void setDataErogazioneServizio(Instant dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }

  public Double getTotale() {
    return totale;
  }

  public void setTotale(Double totale) {
    this.totale = totale;
  }

  @Override
  public String toString() {
    return "ViewAllegatiFattureEntity [codiceStanziamento=" + codiceStanziamento + ", codiceContratto=" + codiceContratto + ", deviceType="
           + deviceType + ", panNumber=" + panNumber + ", serialNumber=" + serialNumber + ", countryLicensePlate=" + countryLicensePlate + ", licensePlate=" + licensePlate
           + ", dataErogazioneServizio=" + dataErogazioneServizio + ", totale=" + totale + "]";
  }

}
