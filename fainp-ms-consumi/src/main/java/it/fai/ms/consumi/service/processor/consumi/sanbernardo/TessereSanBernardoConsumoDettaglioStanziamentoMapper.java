package it.fai.ms.consumi.service.processor.consumi.sanbernardo;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.sanbernardo.SanBernardo;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class TessereSanBernardoConsumoDettaglioStanziamentoMapper extends ConsumoDettaglioStanziamentoMapper {

    @Override
    public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull Consumo consumo) {
        DettaglioStanziamentoPedaggio dettaglioStanziamentoPedaggio = null;
        if( consumo instanceof SanBernardo){
            var tessereSanBernardo = (SanBernardo)consumo;
            dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggio)toDettaglioStanziamentoPedaggio(tessereSanBernardo,tessereSanBernardo.getGlobalIdentifier());
            dettaglioStanziamentoPedaggio.setRouteName(dettaglioStanziamentoPedaggio.getRouteName());
            dettaglioStanziamentoPedaggio.getSupplier().setDocument(tessereSanBernardo.getAdditionalInfo());
        }
        return dettaglioStanziamentoPedaggio;
    }
}
