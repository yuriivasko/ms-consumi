package it.fai.ms.consumi.repository.customer;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import it.fai.ms.consumi.domain.Customer;
import it.fai.ms.consumi.repository.storico_dml.model.StoricoContratto;

@Component
@Validated
public class CustomerEntityMapper {

  public CustomerEntityMapper() {
  }

  public Customer toDomain(@NotNull final StoricoContratto _storicoStatoContratto) {
    final var customer = new Customer(_storicoStatoContratto.getCodiceAzienda());
    return customer;
  }

}
