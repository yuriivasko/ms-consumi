package it.fai.ms.consumi.service.record.hgv;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

import org.springframework.stereotype.Component;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.client.CountryService;
import it.fai.ms.consumi.client.CountryServiceFactory;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.Vehicle;
import it.fai.ms.consumi.domain.VehicleLicensePlate;
import it.fai.ms.consumi.domain.consumi.generici.hgv.Hgv;
import it.fai.ms.consumi.domain.consumi.generici.hgv.HgvGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.generici.HgvRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class HgvRecordConsumoGenericoMapper
  extends AbstractRecordConsumoMapper implements RecordConsumoMapper<HgvRecord, Hgv> {

  private final CountryServiceFactory countryServiceFactory;

  public HgvRecordConsumoGenericoMapper(CountryServiceFactory countryServiceFactory) {
    this.countryServiceFactory = countryServiceFactory;
  }

  /*
   * (non-Javadoc)
   * @see it.fai.ms.consumi.service.record.telepass.RecordConsumoMapper#mapRecordToConsumo(it.fai.ms.consumi.repository.
   * flussi.record.model.CommonRecord)
   */
  @Override
  public Hgv mapRecordToConsumo(HgvRecord record) throws Exception {

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(record, yyyy_MM_dd_dateformat, null);
    Instant ingestionTime = record.getIngestion_time();
    Instant acquisitionDate = optionalCreationDateInFile.orElse(record.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, "HGV");
    source.setFileName(record.getFileName());

    source.setRowNumber(record.getRowNumber());

    Hgv hgv = new Hgv(source, record.getRecordCode(), Optional.of(new HgvGlobalIdentifier(record.getGlobalIdentifier())), record);

    final Device device = new Device(TipoDispositivoEnum.HGV);
    device.setServiceType(TipoServizioEnum.PEDAGGI_INGHILTERRA);
    hgv.setDevice(device);

    Transaction transaction = new Transaction();
    transaction.setDetailCode(record.getTransactionDetail());
    hgv.setTransaction(transaction);


    //lo fa il validatore
    //hgv.setSupplierCode(...);

    //l'analisi dice vuoto
    //hgv.setServicePartnerCode(record.getServicePartnerCode());

    //lo fa il validatore
    //hgv.setGroupArticlesNav(...);

    Instant startDate = calculateInstantByDateAndTime(record.getDataInizio(),null,
                                                         yyyy_MM_dd_dateformat,null).get();

    Instant endDate = calculateInstantByDateAndTime(record.getDataFine(), null,
                                                    yyyy_MM_dd_dateformat, null).get();

    hgv.setStartDate(startDate);
    hgv.setEndDate(endDate);

    hgv.setQuantity(BigDecimal.ONE);

    // CONSUMO.AMOUNT
    Amount amount = new Amount();
    amount.setVatRate(Optional.empty());
    amount.setAmountExcludedVat(record.getTotal_amount_value());

    hgv.setAmount(amount);
    hgv.setProcessed(record.isProcessed());

    // CONSUMO altri campi

    final Vehicle vehicle = new Vehicle();
    vehicle.setLicensePlate(new VehicleLicensePlate(record.getNumeroImmatricolazioneVeicolo(), transcodePlateCountry(record)));
    vehicle.setEuroClass(transcodeEuroClass(record));
    hgv.setVehicle(vehicle);

    //hgv.setRoute("VUOTO");

    //non presente in HGV
    //hgv.setLicencePlateOnRecord(...);

    //non presente in HGV
    //hgv.setAdditionalInformation(...);

    //non presente in HGV
//    hgv.setSupplierInvoiceDocument(...);

    return hgv;
  }

  public String transcodePlateCountry(HgvRecord record) {
    //FIXME - should be created one per file processing
    CountryService countryService = countryServiceFactory.getCountryService();
    return countryService.getBySiglaAuto(record.getPaese()).getCodiceIso2();
  }

  public String transcodeEuroClass(HgvRecord record) {
    //FIXME
    return record.getCategoriaEuro().replace("EC", "");
  }

}
