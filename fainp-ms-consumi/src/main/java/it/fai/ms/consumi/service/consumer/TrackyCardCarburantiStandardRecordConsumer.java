package it.fai.ms.consumi.service.consumer;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.consumi.trackycardcarbstd.TrackyCardCarbStdProcessor;
import it.fai.ms.consumi.service.record.carburanti.TrackyCardCarbStdConsumerRecordConsumoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.UUID;

@Service
public class TrackyCardCarburantiStandardRecordConsumer {

  private Logger log = LoggerFactory.getLogger(getClass());

  private final TrackyCardCarbStdConsumerRecordConsumoMapper trackyCardCarbStdRecordConsumoMapper;
  private final TrackyCardCarbStdProcessor                   trackyCardCarbStdProcessor;
  private final NotificationService                          notificationService;

  public TrackyCardCarburantiStandardRecordConsumer(TrackyCardCarbStdConsumerRecordConsumoMapper trackyCardCarbStdRecordConsumoMapper,
                                                    TrackyCardCarbStdProcessor trackyCardCarbStdProcessor,
                                                    NotificationService notificationService) {
    this.trackyCardCarbStdRecordConsumoMapper = trackyCardCarbStdRecordConsumoMapper;
    this.trackyCardCarbStdProcessor = trackyCardCarbStdProcessor;
    this.notificationService = notificationService;
  }

  public void consume(StanziamentiParams stanziamentiParams, ServicePartner servicePartner, TrackyCardCarburantiStandardRecord tccStandardRecord) {
    log.debug("Consuming record TrackyCardCarburantiStandardRecord:=[{}] of ServicePartner:=[{}]", tccStandardRecord, servicePartner);

    final Bucket bucket = new Bucket(UUID.randomUUID().toString());
    bucket.setDate(tccStandardRecord.getIngestion_time());
    bucket.setFileName(tccStandardRecord.getFileName());

    try {
      log.debug("Mapping record on TrackyCardCarburantiStandard");
      var consumo = trackyCardCarbStdRecordConsumoMapper.mapRecordToConsumo(stanziamentiParams, servicePartner, tccStandardRecord);
      consumo.setServicePartner(servicePartner);
      log.debug("Resulting consumo:=[{}]", consumo);

      log.debug("Validating and processing consumo...");
      ProcessingResult processingResult = trackyCardCarbStdProcessor.validateAndProcess(consumo, bucket);
      log.debug("ProcessingResult:=[{}]", processingResult);

    } catch (Exception e) {
      log.error("Problem on consuming TrackyCardCarburantiStandardRecord:=[{}]", tccStandardRecord,  e);
      sendErrorNotification(servicePartner, tccStandardRecord);
    }
  }

  public void sendErrorNotification(ServicePartner servicePartner,
                                    TrackyCardCarburantiStandardRecord tccStandardRecord) {

    StringBuilder sb = new StringBuilder("error processing message");
    sb.append(tccStandardRecord);

    var errorMessage = new HashMap<String, Object>();
    errorMessage.put("job_name", servicePartner.getFormat().toString());
    errorMessage.put("original_message", sb.toString());

    final String code = "FCJOB-199";

    log.error("Sending notification code:=[{}], errorMessage:=[{}]", code, errorMessage);
    notificationService.notify(code, errorMessage);
  }

}
