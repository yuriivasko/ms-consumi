package it.fai.ms.consumi.client.efservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import feign.FeignException;
import it.fai.ms.consumi.client.efservice.dto.ClienteFaiDTO;
import it.fai.ms.consumi.config.ApplicationProperties;

@Service(EfserviceRestClient.QUALIFIER)
public class EfserviceRestClient {

  public final static String QUALIFIER = "efserviceRestClient";

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final EfserviceServiceClient efserviceServiceClient;

  private final ApplicationProperties applicationProperties;

  public EfserviceRestClient(EfserviceServiceClient _efserviceServiceClient, final ApplicationProperties _applicationProperties) {
    efserviceServiceClient = _efserviceServiceClient;
    applicationProperties = _applicationProperties;
  }

  public ClienteFaiDTO findByCodiceCliente(String codiceCliente) {
    _log.debug("Find ClienteFai By Codice Cliente: {}", codiceCliente);

    try {
      String authorizationHeader = applicationProperties.getAuthorizationHeader();
      return efserviceServiceClient.getClienteFaiByCodice(authorizationHeader, codiceCliente);
    } catch (FeignException e) {
      _log.error("Error FeignException : {}", e.getMessage(), e);
    } catch (Exception e) {
      _log.error("Error : {}", e.getMessage(), e);
    }

    return new ClienteFaiDTO();
  }

  public ClienteFaiDTO findByCodiceClienteFatturazione(String codiceClienteFatturazione) {
    _log.debug("Find ClienteFai By Codice Cliente: {}", codiceClienteFatturazione);

    try {
      String authorizationHeader = applicationProperties.getAuthorizationHeader();
      return efserviceServiceClient.getClienteFaiByCodiceFatturazione(authorizationHeader, codiceClienteFatturazione);
    } catch (Exception e) {
      _log.error("Error : {}", e.getMessage(), e);
    }

    return new ClienteFaiDTO();
  }

  @Cacheable(value = "tipoSupportoFix", key = "{#root.methodName, #root.args[0]}")
  public String getTipoSupportoByTipoDispositivo(String tipoDispositivo) {
    _log.debug("Find TipoSupporto By tipo dispositivo: {}", tipoDispositivo);

    try {
      String authorizationHeader = applicationProperties.getAuthorizationHeader();
      return efserviceServiceClient.getTipoSupportoByTipoDispositivo(authorizationHeader, tipoDispositivo);
    } catch (FeignException e) {
      _log.error("Error FeignException : {}", e.getMessage(), e);
    } catch (Exception e) {
      _log.error("Error : {}", e.getMessage(), e);
    }

    return "";
  }

}
