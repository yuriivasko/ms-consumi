package it.fai.ms.consumi.service.validator;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import it.fai.ms.consumi.domain.consumi.ParamStanziamentoMatchingParamsSupplier;

public interface StanziamentiParamsValidator {

  ValidationOutcome checkConsistency() throws StanziamentiParamsNotFoundException;

  ValidationOutcome existsParamsStanziamento(ParamStanziamentoMatchingParamsSupplier consumo);

  void checkNoBlockingValidationPaginated(List<ConsumoNoBlockingData> noBlockingValidationData, String fileName, String ingestion_time,
                                          String servicePartner);

  void checkNoBlockingValidationOnStanziamentoPaginated(Set<String> codiciStanziamento);

  Optional<StanziamentiDetailsType> findConsumoTypeByTracciatoAndTransactionDetail(Format format, String transactionDetail);
}
