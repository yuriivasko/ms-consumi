package it.fai.ms.consumi.service.parametri_stanziamenti;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toSet;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.domain.fatturazione.enumeration.DettaglioStanziamentoType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.StanziamentiDetailsType;
import it.fai.ms.consumi.domain.parametri_stanziamenti.AllStanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParamsQuery;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante.DettaglioStanziamentoCarburanteEntity;
import it.fai.ms.consumi.repository.parametri_stanziamenti.StanziamentiParamsRepository;
import it.fai.ms.consumi.repository.stanziamento.model.StanziamentoEntity;

@Service
public class StanziamentiParamsServiceImpl implements StanziamentiParamsService {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final StanziamentiParamsRepository stanziamentiParamsRepo;

  @Inject
  public StanziamentiParamsServiceImpl(final StanziamentiParamsRepository _stanziamentiParamsRepository) {
    stanziamentiParamsRepo = _stanziamentiParamsRepository;
  }

  @Override
  public Optional<StanziamentiParams> findByQuery(final StanziamentiParamsQuery _query) {

    Optional<StanziamentiParams> optionalStanziamentiParams;

    final var allStanziamentiParams = stanziamentiParamsRepo.findBySourceAndRecordCode(_query.getSource(), _query.getRecordCode());

    if (allStanziamentiParams.isEmpty()) {

      _log.error("No {} found for source {} and record code {}", StanziamentiParams.class.getSimpleName(), _query.getSource(),
                 _query.getRecordCode());
      return Optional.empty();
    }

    List<StanziamentiParams> paramStanziamentiFiltered = allStanziamentiParams;

    // https://exagespa.atlassian.net/wiki/spaces/FAIMYS/pages/176685072/Parametri+Stanziamenti
    // filter VAT if needed
    boolean isVatRateFromFile = _query.getVatRate() != null && _query.isVatMandatory();
    if (isVatRateFromFile) {
      _log.trace("filter for Vat Rate {}", _query.getVatRate());

      paramStanziamentiFiltered = allStanziamentiParams.stream()
                                                       .filter(p -> p.matchVatRate(_query.getVatRate()))
                                                       .collect(Collectors.toList());
      if (paramStanziamentiFiltered.isEmpty()) {
        _log.trace("no results for vat rate {} but is mandatory! Returning error!", _query.getVatRate());
        return Optional.empty();
      }
    }

    // filter CodiceFornitoreLegacy if needed
    if (_query.getCodiceFornitoreLegacy() != null && paramStanziamentiFiltered.stream()
                                                                              .anyMatch(p -> p.getSupplier() != null && p.getSupplier()
                                                                                                                         .getCodeLegacy() != null)) {
      _log.trace("filter for Cod Fornitore Legacy {}", _query.getCodiceFornitoreLegacy());
      paramStanziamentiFiltered = paramStanziamentiFiltered.stream()
                                                           .filter(p -> p.matchCodiceFornitoreLegacy(_query.getCodiceFornitoreLegacy()))
                                                           .collect(Collectors.toList());
      if (paramStanziamentiFiltered.isEmpty()) {
        _log.trace("no results for vat rate {} and cod fornitore legacy {}", _query.getVatRate(), _query.getCodiceFornitoreLegacy());
        return Optional.empty();
      }

    }

    // find by TransactionDetail && FareClass
    optionalStanziamentiParams = paramStanziamentiFiltered.stream()
                                                          .filter(stanziamentiParams -> stanziamentiParams.matchTransactionDetail(_query.getTransactionDetail())
                                                                                        && stanziamentiParams.matchFareClass(_query.getFareClass()))
                                                          .findFirst();

    if (optionalStanziamentiParams.isPresent()) {
      _log.debug("{} found for vatRate {} tx detail {} and fareClass {}", StanziamentiParams.class.getSimpleName(), _query.getVatRate(),
                 _query.getTransactionDetail(), _query.getFareClass());
      return optionalStanziamentiParams;
    }
    _log.trace("no results for vatRate {} tx detail {} and fareClass {}", _query.getVatRate(), _query.getTransactionDetail(),
               _query.getFareClass());

    // find by TransactionDetail
    optionalStanziamentiParams = paramStanziamentiFiltered.stream()
                                                          .filter(stanziamentiParams -> stanziamentiParams.matchTransactionDetail(_query.getTransactionDetail()))
                                                          .findFirst();

    if (optionalStanziamentiParams.isPresent()) {
      _log.debug(" {} found for vatRate {} tx detail {}", StanziamentiParams.class.getSimpleName(), _query.getVatRate(),
                 _query.getTransactionDetail());
      return optionalStanziamentiParams;
    }
    // if isVatRateFromFile don't check only for recordcode
    if (isVatRateFromFile) {
      return Optional.empty();
    }
    optionalStanziamentiParams = paramStanziamentiFiltered.stream()
                                                          .filter(stanziamentiParams -> stanziamentiParams.matchCodiceFornitoreLegacy(_query.getCodiceFornitoreLegacy()))
                                                          .findFirst();

    if (optionalStanziamentiParams.isPresent()) {
      _log.debug("{} found for vatRate {} cod fornitore legacy {}", StanziamentiParams.class.getSimpleName(), _query.getVatRate(),
                 _query.getCodiceFornitoreLegacy());
      return optionalStanziamentiParams;
    }
    _log.trace(" no results for cod fornitore legacy {}", _query.getCodiceFornitoreLegacy());

    // find by one with same CodiceFornitoreLegacy
    optionalStanziamentiParams = paramStanziamentiFiltered.stream()
                                                          .findFirst();

    if (optionalStanziamentiParams.isPresent()) {
      _log.debug("Found {} matching query : {}", StanziamentiParams.class.getSimpleName(), optionalStanziamentiParams.get());
    }

    return optionalStanziamentiParams;
  }

  @Override
  public AllStanziamentiParams getAll() {
    final AllStanziamentiParams allStanziamentiParams = stanziamentiParamsRepo.getAll();
    _log.debug("All {} : {}", StanziamentiParams.class.getSimpleName(), allStanziamentiParams);
    return allStanziamentiParams;
  }

  @Override
  public Optional<StanziamentiParams> findByDettaglioStanziamentoCarburante(DettaglioStanziamentoCarburanteEntity dettaglio,
                                                                            StanziamentoEntity stanziamento) {
    Optional<StanziamentiParams> optStanziamentiParams = null;
    optStanziamentiParams = stanziamentiParamsRepo.findByArticleAndRecordCodeAndSupplierCodeAndTransactionalDetailAndTipoDettaglio(stanziamento.getArticleCode(),
                                                                                                                                   dettaglio.getRecordCode(),
                                                                                                                                   dettaglio.getCodiceFornitoreNav(),
                                                                                                                                   dettaglio.getTransactionDetailCode(),
                                                                                                                                   StanziamentiDetailsType.CARBURANTI);
    return optStanziamentiParams;
  }

  @Override
  public Optional<String> findDescriptionArticleByRaggruppamentoArticleCode(String groupArticleCode) {
    Optional<String> descriptionOpt = stanziamentiParamsRepo.findDescriptionByRaggruppamentoArticleCode(groupArticleCode);
    return descriptionOpt;
  }

  @Override
  public void updateStanziamentiParams(StanziamentiParams stanziamentiParams) {
    _log.trace("Called updateStanziamentiParams for update [{}] ", stanziamentiParams);
    stanziamentiParamsRepo.updateStanziamentiParams(stanziamentiParams);
  }

  @Override
  public Optional<StanziamentiParams> findByTrx(String routeType, // OIL, NO_OIL //non usato attualmente
                                                StanziamentiDetailsType type, // CARBURANTI, GENERICO
                                                @NotNull String articleCode, // GASOLIO, BIODIESEL
                                                @NotNull String vendorCode, // IT063
                                                String unit, // LT
                                                @NotNull String currency, // EUR
                                                BigDecimal vatPercentage, // 22
                                                @NotNull String productCode // LAVAGGI, CARBURANTI... non usato
                                                                            // attualmente
  ) {

    Optional<StanziamentiParams> optStanziamentiParams = Optional.empty();
    // ArticleCode passed by ARNETOLI is TransactionDetail;

    _log.trace("Search stanziamentiParams BySourceAndRecordCode [API, DEFAULT]");
    List<StanziamentiParams> stanziamentiParams = stanziamentiParamsRepo.findBySourceAndRecordCode("API", "DEFAULT");
    _log.trace("found [{}] stanziamentiParams, filtering for finding one", stanziamentiParams.size());

    if (!stanziamentiParams.isEmpty()) {
      // List<StanziamentiParams> stanziamentiParams = stanziamentiParamsByArticleCode.get();
      optStanziamentiParams = stanziamentiParams.stream()

                                                .filter(sp -> ofNullable(articleCode).equals(ofNullable(sp.getTransactionDetail())))
                                                .peek(sps -> _log.debug("Filtered by Transaction detail [{}]: {}", articleCode, sps))

                                                .filter(sp -> ofNullable(type).equals(ofNullable(sp.getStanziamentiDetailsType())))
                                                .peek(sps -> _log.debug("Filtered by Stanziamenti Details Type [{}]: {}", type, sps))

                                                .filter(sp -> ofNullable(vendorCode).equals(ofNullable(sp.getSupplier()).map(supplier -> supplier.getCodeLegacy())))
                                                .peek(sps -> _log.debug("Filtered by VendorCode [{}]: {}", vendorCode, sps))

                                                .filter(sp -> ofNullable(unit).equals(ofNullable(sp.getArticle()).map(article -> article.getMeasurementUnit())))
                                                .peek(sps -> _log.debug("Filtered by unit [{}]: {}", unit, sps))

                                                // FIXME trovare un modo migliore per valutare l'equals dei due
                                                // valori...
                                                .filter(sp -> ofNullable(vatPercentage).map(vatBigDecimal -> vatBigDecimal.floatValue())
                                                                                       .equals(ofNullable(sp.getArticle()).map(article -> article.getVatRate())))
                                                .peek(sps -> _log.debug("Filtered by Vat [{}]: {}", vatPercentage, sps))

                                                .filter(sp -> ofNullable(currency).equals(ofNullable(sp.getArticle()).map(article -> article.getCurrency())
                                                                                                                     .map(currency1 -> currency1.getCurrencyCode())))
                                                .peek(sps -> _log.debug("Actual list: + " + stanziamentiParams.toString()))
                                                .findFirst();

      // Optional<List<StanziamentiParams>> stanziamentiParamsByArticleCode =
      // stanziamentiParamsRepo.findByTransactionDetail(articleCode);
      // if (stanziamentiParamsByArticleCode.isPresent()) {
      // List<StanziamentiParams> stanziamentiParams = stanziamentiParamsByArticleCode.get();
      // optStanziamentiParams = stanziamentiParams.stream()
      // .map(sp -> {
      // if (sp.getSource() == null || !sp.getSource()
      // .equals(routeType)) {
      // _log.error("Not match routeType: {} - {}", sp.getSource(), routeType);
      // return null;
      // }
      //
      // if (sp.getStanziamentiDetailsType()
      // .compareTo(StanziamentiDetailsType.valueOf(productCode)) != 0) {
      // _log.error("Not match StanziamentiDetailsType: {} - {}",
      // sp.getStanziamentiDetailsType(), productCode);
      // return null;
      // }
      //
      // if (sp.getSupplier() == null || sp.getSupplier()
      // .getCodeLegacy() == null
      // || !sp.getSupplier()
      // .getCodeLegacy()
      // .equals(vendorCode)) {
      // _log.error("Not match VendorCode: {}", vendorCode);
      // return null;
      // }
      //
      // if (sp.getArticle() == null || !unit.equals(sp.getArticle()
      // .getMeasurementUnit())) {
      // _log.error("Not match Unit: {}", unit);
      // return null;
      // }
      //
      // if (sp.getArticle() == null || vatPercentage.compareTo(new BigDecimal(sp.getArticle()
      // .getVatRate())) != 0) {
      // _log.error("Not match vatPercentage: {}", vatPercentage);
      // return null;
      // }
      //
      // if (sp.getArticle() == null || !currency.equals(sp.getArticle()
      // .getCurrency()
      // .getCurrencyCode())) {
      // _log.error("Not match Currency: {}", currency);
      // return null;
      // }
      // return sp;
      // })
      // .findFirst();

      if (!optStanziamentiParams.isPresent()) {
        _log.error("Not found StanziamentiParams...");
      }
    }
    return optStanziamentiParams;
  }

  @Override
  public Optional<StanziamentiDetailsType> findConsumoTypeByTracciatoAndTransactionDetail(Format format, String transactionDetail) {
    return stanziamentiParamsRepo.findByTracciatoAndTransactionDetail(format.name(), transactionDetail);
  }

  @Override
  public Set<String> findArticleCodesByTipoDettaglioStanziamento(StanziamentiDetailsType type) {
    List<StanziamentiParams> stanziamentiParams = stanziamentiParamsRepo.findByTipoDettaglio(type);
    Set<String> articleCodes = stanziamentiParams.stream()
                                                   .filter(sp -> sp.getArticle() != null)
                                                   .map(sp -> sp.getArticle()
                                                                .getCode())
                                                   .collect(toSet());
    return articleCodes;
  }

}
