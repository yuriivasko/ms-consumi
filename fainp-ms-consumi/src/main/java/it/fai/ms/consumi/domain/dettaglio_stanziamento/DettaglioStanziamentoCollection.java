package it.fai.ms.consumi.domain.dettaglio_stanziamento;

import java.util.Objects;
import java.util.Set;

import it.fai.ms.consumi.domain.InvoiceType;

public class DettaglioStanziamentoCollection {

  private final Set<DettaglioStanziamento> allocationDetails;

  public DettaglioStanziamentoCollection(final Set<DettaglioStanziamento> _allocationDetails) {
    allocationDetails = Objects.requireNonNull(_allocationDetails);
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final DettaglioStanziamentoCollection obj = getClass().cast(_obj);
      res = Objects.equals(obj.allocationDetails, allocationDetails);
    }
    return res;
  }

  public Set<DettaglioStanziamento> getDettaglioStanziamentoSet() {
    return allocationDetails;
  }

  public DettaglioStanziamento getFirst() {
    return allocationDetails.stream()
                            .findFirst()
                            .get();
  }

  @Override
  public int hashCode() {
    return Objects.hash(allocationDetails);
  }

  public boolean isEmpty() {
    return allocationDetails.isEmpty();
  }

  public int size() {
    return allocationDetails.size();
  }

  public boolean thereAreBothDettaglioStanziamentoTypes() {
    return thereAreAnyDettaglioStanziamentoProvvisorio() && thereAreAnyDettaglioStanziamentoDefinitivo();
  }

  public boolean thereAreOnlyDettaglioStanziamentoDefinitivo() {
    return allocationDetails.stream()
                            .allMatch(allocationDetail -> allocationDetail.getInvoiceType()
                                                                          .equals(InvoiceType.D));
  }

  public boolean thereAreOnlyDettaglioStanziamentoProvvisorio() {
    return allocationDetails.stream()
                            .allMatch(allocationDetail -> allocationDetail.getInvoiceType()
                                                                          .equals(InvoiceType.P));
  }

  @Override
  public String toString() {
    return new StringBuilder().append(getClass().getSimpleName())
                              .append("[")
                              .append("")
                              .append(allocationDetails)
                              .append("]")
                              .toString();
  }

  private boolean thereAreAnyDettaglioStanziamentoDefinitivo() {
    return allocationDetails.stream()
                            .anyMatch(allocationDetail -> allocationDetail.getInvoiceType()
                                                                          .equals(InvoiceType.D));
  }

  private boolean thereAreAnyDettaglioStanziamentoProvvisorio() {
    return allocationDetails.stream()
                            .anyMatch(allocationDetail -> allocationDetail.getInvoiceType()
                                                                          .equals(InvoiceType.P));
  }

}
