package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.TrackyCardPedaggiStandardRecord;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.TrackyCardPedaggiStandardRecordDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TrackyCardCarburantiStandardRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.repository.flussi.record.model.util.StringItem;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;

@Component
public class TrackyCardCarburantiStandardDescriptor
  implements RecordDescriptor<TrackyCardCarburantiStandardRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(TrackyCardCarburantiStandardDescriptor.class);
  public final static String HEADER_BEGIN      = "H0";
  public final static String BEGIN_D0          = "D0";
  public final static String FOOTER_CODE_BEGIN = "T0";

  // Descrittore HEADER
  public final static FileDescriptor descriptor01 = new FileDescriptor();

  static {
    descriptor01.addItem("recordCode", 1, 2);
    descriptor01.skipItem("counter", 3, 7);
    descriptor01.skipItem("partnerCode", 8, 17);
    descriptor01.addItem("creationDateInFile", 18, 25); // AAAAMMGG
    descriptor01.addItem("creationTimeInFile", 26, 31); // HHMMSS
    descriptor01.skipItem("reservedFree", 32, 200);
  }

  // Descrittore RECORD
  public final static FileDescriptor descriptor02 = new FileDescriptor();

  static {
    descriptor02.addItem("recordCode", 1, 2);
    descriptor02.addItem("counter", 3, 7);
    descriptor02.addItem("pointCode", 8, 17);
    descriptor02.addItem("panField1IsoCode", 18, 23);
    descriptor02.addItem("panField2GroupCode", 24, 25);
    descriptor02.addItem("panField3CustomerCode", 26, 31);
    descriptor02.addItem("panField4DeviceNumber", 32, 35);
    descriptor02.addItem("panField5ControlDigit", 36, 36);
    descriptor02.addItem("date", 37, 44); // AAAAMMGG
    descriptor02.addItem("time", 45, 50); // HHMMSS
    descriptor02.addItem("pumpNumber", 51, 52);
    descriptor02.addItem("litres", 53, 58);
    descriptor02.addItem("kilometers", 59, 64);
    descriptor02.addItem("ticket", 65, 74);
    descriptor02.addItem("productCode", 75, 79);
    descriptor02.addItem("currency", 105, 107); //lasciare la currency PRIMA (in ordine di parsing) dei prezzi!!!
    descriptor02.addItem("priceFaiReservedVatIncluded", 80, 86); //unit_cost_vat_included
    descriptor02.addItem("priceExposedVatIncluded", 87, 93); //unit_price_vat_included
    descriptor02.addItem("priceBaseVatIncluded", 94, 100); //base_unit_price_vat_included
    descriptor02.addItem("vatPercentage", 101, 104);
    
    // Nota: da
    // https://exage.sharepoint.com/:w:/r/sites/FAI/_layouts/15/Doc.aspx?sourcedoc=%7B15C23D0A-00E9-4F0C-B50A-EAA6D9F62F9F%7D&file=FaiPass_Informazioni_Tecniche1.1.3IT.doc&action=default&mobileredirect=true
    // c'è un errore nel mapping del campo "Riservato/Libero", indicato come 101..200, mentre è 108..200
    // descriptor02.addItem("reservedFree", 108, 200);
  }

  // Descrittore FOOTER
  public final static FileDescriptor descriptor03 = new FileDescriptor();
  public static final String         counter      = "counter";

  static {
    descriptor03.addItem("recordCode", 1, 2);
    descriptor03.addItem(counter, 3, 7);
    descriptor03.addItem("partnerCode", 8, 17);
    descriptor03.addItem("date", 18, 25); // AAAAMMGG
    descriptor03.addItem("time", 26, 31); // HHMMSS
    // descriptor03.addItem("reservedFree", 32, 200);
  }

  @Override
  public TrackyCardCarburantiStandardRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {

    final TrackyCardCarburantiStandardRecord entity = new TrackyCardCarburantiStandardRecord(fileName,rowNumber);
    switch (_recordCode) {
    case HEADER_BEGIN:
      entity.setFromString(TrackyCardCarburantiStandardDescriptor.descriptor01.getItems(), _data);
      break;

    case BEGIN_D0:
      entity.setFromString(TrackyCardCarburantiStandardDescriptor.descriptor02.getItems(), _data);
      break;
    default:
      log.warn("RecordCode {} not supported", _recordCode);
      break;
    }
    return entity;
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    Map<Integer, Integer> startEndPosTotalRows = new HashMap<>();
    Optional<StringItem> stringItem = TrackyCardCarburantiStandardDescriptor.descriptor03.getItem(TrackyCardCarburantiStandardDescriptor.counter);
    if (!stringItem.isPresent()) {
      throw new IllegalStateException("Fatal error in TrackyCardCarburantiStandardDescriptor!");
    }
    startEndPosTotalRows.put(stringItem.get()
                                       .getStart(),
                             stringItem.get()
                                       .getEnd());

    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return TrackyCardCarburantiStandardDescriptor.HEADER_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return TrackyCardCarburantiStandardDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 0;
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(TrackyCardPedaggiStandardRecordDescriptor.class, TrackyCardPedaggiStandardRecord.class).checkConfiguration();
  }
}
