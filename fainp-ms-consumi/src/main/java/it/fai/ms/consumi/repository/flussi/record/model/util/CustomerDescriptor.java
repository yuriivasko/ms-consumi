package it.fai.ms.consumi.repository.flussi.record.model.util;

public class CustomerDescriptor {

  private String codice_cliente     = "";
  private String targa_veicolo      = "";
  private String codice_fornitore   = "";
  private String codice_articolo    = "";
  private String targa_country_code = "";

  public CustomerDescriptor() {
    super();
  }

  public CustomerDescriptor(String codice_cliente, String targa_veicolo, String codice_fornitore, String codice_articolo) {
    super();
    this.codice_cliente = codice_cliente;
    this.targa_veicolo = targa_veicolo;
    this.codice_fornitore = codice_fornitore;
    this.codice_articolo = codice_articolo;
  }

  public CustomerDescriptor(String codice_cliente, String targa_veicolo, String codice_fornitore, String codice_articolo,
                            String targa_country_code) {
    super();
    this.codice_cliente = codice_cliente;
    this.targa_veicolo = targa_veicolo;
    this.codice_fornitore = codice_fornitore;
    this.codice_articolo = codice_articolo;
    this.targa_country_code = targa_country_code;
  }

  public String getTargaCountryCode() {
    return targa_country_code;
  }

  public void setTargaCountryCode(String targaCountryCode) {
    this.targa_country_code = targaCountryCode;
  }

  public String getCodice_cliente() {
    return codice_cliente;
  }

  public void setCodice_cliente(String codice_cliente) {
    this.codice_cliente = codice_cliente;
  }

  public String getTarga_veicolo() {
    return targa_veicolo;
  }

  public void setTarga_veicolo(String targa_veicolo) {
    this.targa_veicolo = targa_veicolo;
  }

  public String getCodice_fornitore() {
    return codice_fornitore;
  }

  public void setCodice_fornitore(String codice_fornitore) {
    this.codice_fornitore = codice_fornitore;
  }

  public String getCodice_articolo() {
    return codice_articolo;
  }

  public void setCodice_articolo(String codice_articolo) {
    this.codice_articolo = codice_articolo;
  }

}
