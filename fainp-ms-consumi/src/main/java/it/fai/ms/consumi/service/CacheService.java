package it.fai.ms.consumi.service;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CacheService {

  private CacheManager cacheManager;

  public CacheService(CacheManager cacheManager) {
    this.cacheManager = cacheManager;
  }

  @Nullable
  public Cache getCache(String s) {
    return cacheManager.getCache(s);
  }

  public Collection<String> getCacheNames() {
    return cacheManager.getCacheNames();
  }
}
