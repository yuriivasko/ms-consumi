package it.fai.ms.consumi.service.record.stcon;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.stcon.Stcon;
import it.fai.ms.consumi.repository.flussi.record.model.stcon.StconRecord;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Component
public class StconRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<StconRecord, Stcon> {

  public boolean mayNeedFollowingRecord(StconRecord commonRecord) throws Exception {
    return false;
  }

  @Override
  public Stcon mapRecordToConsumo(StconRecord commonRecord) throws Exception {
    return null;
  }

}
