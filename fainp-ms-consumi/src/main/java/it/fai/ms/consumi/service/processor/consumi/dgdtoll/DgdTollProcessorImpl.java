package it.fai.ms.consumi.service.processor.consumi.dgdtoll;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class DgdTollProcessorImpl extends ConsumoAbstractProcessor implements it.fai.ms.consumi.service.processor.consumi.dgdtoll.DgdTollProcessor {

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final StanziamentiProducer                              stanziamentiProducer;
  private final DgdTollConsumoDettaglioStanziamentoPedaggioMapper mapper;

  @Inject
  public DgdTollProcessorImpl(final ConsumoValidatorService _consumoValidatorService, final StanziamentiProducer _stanziamentiProducer,
                              final DgdTollConsumoDettaglioStanziamentoPedaggioMapper _mapper,
                              final VehicleService _vehicleService,
                              final CustomerService _customerService) {
    super(_consumoValidatorService, _vehicleService, _customerService);
    stanziamentiProducer = _stanziamentiProducer;
    mapper = _mapper;
  }

  @Override
  public ProcessingResult validateAndProcess(final DgdToll _dgdToll, final Bucket _bucket) {
    return executeProcess(_dgdToll, _bucket);
  }

  private ProcessingResult executeProcess(final Consumo _consumo, final Bucket _bucket) {

    _log.info(">");
    _log.info("{} processing start {}", _bucket, _consumo);
    _log.info("  --> {}", _consumo);

    var processingResult = validateAndNotify(_consumo);

    _log.debug(" - {} processing validation completed", _consumo);

    final var consumo = processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
                      .ifPresent(stanziamentiParams -> {
                        final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
                        final var stanziamenti = stanziamentiProducer.produce(dettaglioStanziamento, stanziamentiParams);
                        processingResult.getStanziamenti()
                                        .addAll(stanziamenti);
                      });
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }

    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", _consumo.getSource());
    _log.info("<");

    return processingResult;
  }

  private DettaglioStanziamento mapConsumoToDettaglioStanziamento(final Consumo _consumo) {
    return mapConsumoToDettaglioStanziamento(_consumo, mapper);
  }

}
