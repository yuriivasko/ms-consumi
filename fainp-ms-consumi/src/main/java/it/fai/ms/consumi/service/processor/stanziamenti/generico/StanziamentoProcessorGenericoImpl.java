package it.fai.ms.consumi.service.processor.stanziamenti.generico;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.generico.DettaglioStanziamentoGenerico;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.repository.stanziamento.StanziamentoRepository;
import it.fai.ms.consumi.service.processor.stanziamenti.StanziamentoProcessorAbstract;

@Service
@Transactional
public class StanziamentoProcessorGenericoImpl
  extends StanziamentoProcessorAbstract {

  // private final transient Logger _log = LoggerFactory.getLogger(getClass());

  @Inject
  public StanziamentoProcessorGenericoImpl(final StanziamentoRepository _stanziamentiRepository,
                                           final StanziamentoGenericoMapper _mapper) {
    super(_stanziamentiRepository, _mapper);
  }

  @Override
  public List<Stanziamento> processStanziamento(@NotNull DettaglioStanziamento _allocationDetail,
                                                @NotNull StanziamentiParams _stanziamentiParams) {

    final List<Stanziamento> allocationsToReturn = new LinkedList<>();
    final var allocationDetail = (DettaglioStanziamentoGenerico) _allocationDetail;
    Stanziamento createdAllocation = null;

    if (allocationDetail.hasCustomer()) {

      //FIXME raggruppare o creare lo stanziameto

      // Creo lo stanzimento dal dettaglio
      createdAllocation = allocationHasToBeCreated(allocationDetail, _stanziamentiParams);

      if (createdAllocation != null)
        allocationsToReturn.add(createdAllocation);

    } else {
      throw new IllegalStateException("DettaglioStanziamento must contain a cutomer");
    }

    return allocationsToReturn;
  }

  @Override
  protected Stanziamento allocationHasToBeCreated(final DettaglioStanziamento _allocationDetail, final StanziamentiParams _params) {

    final var allocation = getMapper().toStanziamento(_allocationDetail, _params);

    allocation.getDettaglioStanziamenti()
              .add(_allocationDetail);

    // stanziamento.setCodiceStanziamentoProvvisorio(null);

    return allocation;
  }

  protected Stanziamento allocationHasToBeUpdated(final Stanziamento _allocation, final DettaglioStanziamento _allocationDetail) {

    final var allocationDetail = (DettaglioStanziamentoCarburante) _allocationDetail;
    final var allocation = _allocation;

    if (allocationDetail.getCostComputed() != null) {
      allocation.getCosto()
                .add(allocationDetail.getCostComputed()
                                     .getAmountExcludedVatBigDecimal());
    }
    if (allocationDetail.getPriceComputed() != null) {
      allocation.getPrezzo()
                .add(allocationDetail.getPriceComputed()
                                     .getAmountExcludedVatBigDecimal());
    }

    return allocation;
  }

  @Override
  protected Stanziamento allocationHasToBeUpdated(DettaglioStanziamento _allocationDetail, Stanziamento _allocation) {
    throw new UnsupportedOperationException("This class doesn't support allocation update");
  }

  @Override
  public List<Stanziamento> processStanziamentoForCase2aWithSameAmount(@NotNull DettaglioStanziamento _allocationDetail,
                                                                       @NotNull StanziamentiParams _stanziamentiParams) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Stanziamento> processClonedDStanziamentoForCase2aWithDiffAmount(@NotNull DettaglioStanziamento _allocationDetailDCloned,
                                                                              @NotNull StanziamentiParams _allocationParams) {
    throw new UnsupportedOperationException();
  }

  @Override
  public List<Stanziamento> processCreatedStanziamentoForCase2aWithDiffAmount(@NotNull DettaglioStanziamento _allocationDetailCreated,
                                                                              @NotNull StanziamentiParams _allocationParams) {
    throw new UnsupportedOperationException();
  }

}
