package it.fai.ms.consumi.repository.flussi.record.model.pedaggi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import it.fai.ms.consumi.repository.flussi.record.model.CommonRecord;
import it.fai.ms.consumi.repository.flussi.record.model.GlobalIdentifierBySupplier;
import org.apache.commons.csv.CSVFormat;

import java.util.Objects;
import java.util.UUID;

public class SanBernardoViaggiRecord extends CommonRecord
  implements GlobalIdentifierBySupplier {

  @JsonCreator
  public SanBernardoViaggiRecord(@JsonProperty("fileName") String fileName,@JsonProperty("rowNumber") long rowNumber) {
    super(fileName, rowNumber);
    globalIdentifier = UUID.randomUUID().toString();
  }

  private String globalIdentifier;
  private String tipoTitolo;
  private String panNumber;
  private String piazzale;
  private String pista;
  private String data;
  private String classe;
  private String numeroPacchetto;

  public String getTipoTitolo() {
    return tipoTitolo;
  }

  public void setTipoTitolo(String tipoTitolo) {
    this.tipoTitolo = tipoTitolo;
  }

  public String getPanNumber() {
    return panNumber;
  }

  public void setPanNumber(String panNumber) {
    this.panNumber = panNumber;
  }

  public String getPiazzale() {
    return piazzale;
  }

  public void setPiazzale(String piazzale) {
    this.piazzale = piazzale;
  }

  public String getPista() {
    return pista;
  }

  public void setPista(String pista) {
    this.pista = pista;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public String getClasse() {
    return classe;
  }

  public void setClasse(String classe) {
    this.classe = classe;
  }

  public String getNumeroPacchetto() {
    return numeroPacchetto;
  }

  public void setNumeroPacchetto(String numeroPacchetto) {
    this.numeroPacchetto = numeroPacchetto;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    SanBernardoViaggiRecord that = (SanBernardoViaggiRecord) o;
    return Objects.equals(tipoTitolo, that.tipoTitolo) &&
      Objects.equals(panNumber, that.panNumber) &&
      Objects.equals(piazzale, that.piazzale) &&
      Objects.equals(pista, that.pista) &&
      Objects.equals(data, that.data) &&
      Objects.equals(classe, that.classe) &&
      Objects.equals(numeroPacchetto, that.numeroPacchetto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tipoTitolo, panNumber, piazzale, pista, data, classe, numeroPacchetto);
  }

  @Override
  public String toString() {
    return "SanBernardoViaggiRecord{" +
      "tipoTitolo='" + tipoTitolo + '\'' +
      ", panNumber='" + panNumber + '\'' +
      ", piazzale='" + piazzale + '\'' +
      ", pista='" + pista + '\'' +
      ", data='" + data + '\'' +
      ", classe='" + classe + '\'' +
      ", numeroPacchetto='" + numeroPacchetto + '\'' +
      ", rowNumber=" + rowNumber +
      ", fileName='" + fileName + '\'' +
      ", creationDateInFile='" + creationDateInFile + '\'' +
      ", creationTimeInFile='" + creationTimeInFile + '\'' +
      ", processed=" + processed +
      ", rtdtm=" + rtdtm +
      ", rttime=" + rttime +
      ", anno=" + anno +
      ", vatRateBigDecimal=" + vatRateBigDecimal +
      ", ingestion_time=" + ingestion_time +
      ", id=" + id +
      '}';
  }

  @Override
  public CSVFormat getCsvFormat() {
    return CSVFormat.RFC4180.withDelimiter(';');
  }

  @Override
  public String getGlobalIdentifier() {
    return globalIdentifier;
  }
}
