package it.fai.ms.consumi.service.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.Article;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;

@Service
public class ArticleToStanziamentiParamsMapper {

  public StanziamentiParams toStanziamentiParams(Article article, StanziamentiParams stanziamentiParams) {

    if (stanziamentiParams == null) {
      throw new IllegalArgumentException("Cannot map on null stanziamentiParams!");
    }

    Article currentArticle = stanziamentiParams.getArticle();
    BeanUtils.copyProperties(article, currentArticle);
    return stanziamentiParams;
  }
}
