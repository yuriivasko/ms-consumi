package it.fai.ms.consumi.client;

import static it.fai.ms.consumi.security.jwt.JWTConfigurer.AUTHORIZATION_HEADER;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.codahale.metrics.annotation.Timed;



@FeignClient(name = "faidocument")
public interface DocumentClient {


  String BASE_PATH = "/api";
  String READ_CSV = "/public/readcsv";
  String DELIVERY_PRINT = "/delivery/print";
  String DELIVERY_PRINT_PRODUTTORE = "/delivery/printProduttore";
  String READ_CSV_COLUMN = BASE_PATH + READ_CSV + "/column";
  String READ_CSV_DISPOSITIVI_DEPOSITO = BASE_PATH + READ_CSV + "/dispositivideposito";
  String API_CSV_DISPOSITIVI_DEPOSITO = BASE_PATH + READ_CSV_DISPOSITIVI_DEPOSITO;
  String API_DELIVERY_PRINT = BASE_PATH + DELIVERY_PRINT;

  String REPORT_BASE = "/report";
  String API_REPORT_SAVE = BASE_PATH + REPORT_BASE + "/saveCSV";
  String API_REPORT_DOWNLOAD = REPORT_BASE + "/download";
  public static final String API_REPORT_SAVE_TEXT = BASE_PATH + REPORT_BASE + "/saveCSVText";

  @GetMapping(API_REPORT_SAVE_TEXT + "/{fileName}")
  @Timed
  public String saveReportCsvFromText(
                                            @RequestHeader(AUTHORIZATION_HEADER) String authorizationToken,
                                            @PathVariable("fileName") String fileName,
                                            @RequestBody String csv
      );
}
