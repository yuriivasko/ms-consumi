package it.fai.ms.consumi.domain.stanziamento;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.InvoiceType;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;

public class Stanziamento {

  private String                     amountCurrencyCode;
  private String                     code;
  private String                     codeTempAllocation;
  private String                     codiceClienteFatturazione;
  private boolean                    conguaglio             = false;
  private BigDecimal                 costo                  = BigDecimal.ZERO;
  private LocalDate                  dataErogazioneServizio;
  private Instant                    dataAcquisizioneFlusso;
  private Instant                    dataFattura;
  private final String[]             descriptions           = new String[] { "", "", "" };
  private Set<DettaglioStanziamento> dettaglioStanziamenti  = new HashSet<>();
  private GeneraStanziamento         generaStanziamento;
  private InvoiceType                invoiceType;
  private String                     articleCode;
  private String                     numeroCliente;
  private String                     numeroFattura;
  private String                     numeroFornitore;
  private String                     paese;
  private BigDecimal                 prezzo                 = BigDecimal.ZERO;
  private int                        provvigioniAquisite    = 0;
  private BigDecimal                 quantita               = new BigDecimal("1");
  private String                     storno;
  private String                     supplierDocument;
  private TypeFlow                   tipoFlusso             = TypeFlow.MYFAI;
  private int                        totalAllocationDetails = 1;
  private String                     vehicleEuroClass;
  private String                     vehicleLicensePlate;
  private String                     vehicleCountry;
  private Integer                    year;
  private boolean                    sentToNav;

  public Stanziamento(final String _code) {
    code = _code;
  }

  public void addAmountExcludedVat(final Amount _amount) {
    if (_amount != null) {
      costo = costo.add(_amount.getAmountExcludedVatBigDecimal());
      prezzo = prezzo.add(_amount.getAmountExcludedVatBigDecimal());
    }
  }

  public void addImponibileAdr(BigDecimal imponibileAdr) {
   if(imponibileAdr != null) {
     costo = costo.add(imponibileAdr);
     prezzo = prezzo.add(imponibileAdr);
   }

  }


  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final Stanziamento obj = getClass().cast(_obj);
      res = Objects.equals(obj.code, code);
    }
    return res;
  }

  public String getAmountCurrencyCode() {
    return amountCurrencyCode;
  }

  public String getVehicleEuroClass() {
    return vehicleEuroClass;
  }

  public String getCode() {
    return code;
  }

  public String getCodiceClienteFatturazione() {
    return codiceClienteFatturazione;
  }

  public String getCodiceStanziamentoProvvisorio() {
    return codeTempAllocation;
  }

  public BigDecimal getCosto() {
    return costo;
  }

  public LocalDate getDataErogazioneServizio() {
    return dataErogazioneServizio;
  }

  public Instant getDataFattura() {
    return dataFattura;
  }

  public String[] getDescriptions() {
    return descriptions;
  }

  public Set<DettaglioStanziamento> getDettaglioStanziamenti() {
    return dettaglioStanziamenti;
  }

  public String getDocumentoDaFornitore() {
    return supplierDocument;
  }

  public GeneraStanziamento getGeneraStanziamento() {
    return generaStanziamento;
  }

  public InvoiceType getInvoiceType() {
    return invoiceType;
  }

  public int getTotalAllocationDetails() {
    return totalAllocationDetails;
  }

  public String getArticleCode() {
    return articleCode;
  }

  public String getNumeroCliente() {
    return numeroCliente;
  }

  public String getNumeroFattura() {
    return numeroFattura;
  }

  public String getNumeroFornitore() {
    return numeroFornitore;
  }

  public String getPaese() {
    return paese;
  }

  public BigDecimal getPrezzo() {
    return prezzo;
  }

  public int getProvvigioniAquisite() {
    return provvigioniAquisite;
  }

  public BigDecimal getQuantita() {
    return quantita;
  }

  public String getStorno() {
    return storno;
  }

  public String getVehicleLicensePlate() {
    return vehicleLicensePlate;
  }

  public TypeFlow getTipoFlusso() {
    return tipoFlusso;
  }

  public Integer getYear() {
    return year;
  }

  @Override
  public int hashCode() {
    return Objects.hash(code);
  }

  public void incrementTotalAllocationDetails() {
    totalAllocationDetails = totalAllocationDetails + 1;
  }

  public boolean isConguaglio() {
    return conguaglio;
  }

  public void setAmountCurrencyCode(final String _amountCurrencyCode) {
    amountCurrencyCode = _amountCurrencyCode;
  }

  public void setCodiceClienteFatturazione(String codiceClienteFatturazione) {
    this.codiceClienteFatturazione = codiceClienteFatturazione;
  }

  public void setCodiceStanziamento(String codiceStanziamento) {
    this.code = codiceStanziamento;
  }

  public void setCodiceStanziamentoProvvisorio(final String _codeTempAllocation) {
    codeTempAllocation = _codeTempAllocation;
  }

  public void setConguaglio(final boolean _conguaglio) {
    conguaglio = _conguaglio;
  }

  public void setCosto(BigDecimal costo) {
    this.costo = costo;
  }

  public void setDataErogazioneServizio(LocalDate dataErogazioneServizio) {
    this.dataErogazioneServizio = dataErogazioneServizio;
  }

  public void setDataFattura(Instant dataFattura) {
    this.dataFattura = dataFattura;
  }

  public void setGeneraStanziamento(GeneraStanziamento generaStanziamento) {
    this.generaStanziamento = generaStanziamento;
  }

  public void setArticleCode(final String _articleCode) {
    articleCode = _articleCode;
  }

  public void setNumeroCliente(final String _numeroCliente) {
    numeroCliente = _numeroCliente;
  }

  public void setNumeroFattura(String numeroFattura) {
    this.numeroFattura = numeroFattura;
  }

  public void setNumeroFornitore(String numeroFornitore) {
    this.numeroFornitore = numeroFornitore;
  }

  public void setPaese(String paese) {
    this.paese = paese;
  }

  public void setPrezzo(final BigDecimal prezzo) {
    this.prezzo = prezzo;
  }

  public void setProvvigioniAquisite(final int _provvigioniAquisite) {
    provvigioniAquisite = _provvigioniAquisite;
  }

  public void setQuantity(final BigDecimal _quantity) {
    quantita = _quantity;
  }

  public void setInvoiceType(final InvoiceType _invoiceType) {
    invoiceType = _invoiceType;
  }

  public void setStorno(String storno) {
    this.storno = storno;
  }

  public void setSupplierDocument(final String _supplierDocument) {
    supplierDocument = _supplierDocument;
  }

  public void setVehicleLicensePlate(final String _vehicleLicensePlate) {
    vehicleLicensePlate = _vehicleLicensePlate;
  }

  public void setTipoFlusso(TypeFlow tipoFlusso) {
    this.tipoFlusso = tipoFlusso;
  }

  public void setTotalAllocationDetails(final int _totalAllocationDetails) {
    totalAllocationDetails = _totalAllocationDetails;
  }

  public void setVehicleEuroClass(final String _vehicleEuroClass) {
    vehicleEuroClass = _vehicleEuroClass;
  }

  public void setYear(final Integer _year) {
    year = _year;
  }

  public boolean isSentToNav() {
    return sentToNav;
  }

  public void setSentToNav(boolean sentToNav) {
    this.sentToNav = sentToNav;
  }

  public String getVehicleCountry() {
    return vehicleCountry;
  }

  public void setVehicleCountry(String vehicleCountry) {
    this.vehicleCountry = vehicleCountry;
  }

  public Instant getDataAcquisizioneFlusso() {
    return dataAcquisizioneFlusso;
  }

  public void setDataAcquisizioneFlusso(Instant dataAcquisizioneFlusso) {
    this.dataAcquisizioneFlusso = dataAcquisizioneFlusso;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Stanziamento [");
    builder.append("code=");
    builder.append(code);
    builder.append(", invoiceType=");
    builder.append(invoiceType);
    builder.append(", tipoFlusso=");
    builder.append(tipoFlusso);
    builder.append(", dataErogazioneServizio=");
    builder.append(dataErogazioneServizio);
    builder.append(", dataAcquisizioneFlusso=");
    builder.append(dataAcquisizioneFlusso);
    builder.append(", numeroFornitore=");
    builder.append(numeroFornitore);
    builder.append(", numeroCliente=");
    builder.append(numeroCliente);
    builder.append(", articleCode=");
    builder.append(articleCode);
    builder.append(", paese=");
    builder.append(paese);
    builder.append(", year=");
    builder.append(year);
    builder.append(", numeroFattura=");
    builder.append(numeroFattura);
    builder.append(", dataFattura=");
    builder.append(dataFattura);
    builder.append(", vehicleLicensePlate=");
    builder.append(vehicleLicensePlate);
    builder.append(", vehicleCountry=");
    builder.append(vehicleCountry);
    builder.append(", vehicleEuroClass=");
    builder.append(vehicleEuroClass);
    builder.append(", prezzo=");
    builder.append(prezzo);
    builder.append(", costo=");
    builder.append(costo);
    builder.append(", quantita=");
    builder.append(quantita);
    builder.append(", totalAllocationDetails=");
    builder.append(totalAllocationDetails);
    builder.append(", amountCurrencyCode=");
    builder.append(amountCurrencyCode);
    builder.append(", descriptions=");
    builder.append(Arrays.toString(descriptions));
    builder.append(", generaStanziamento=");
    builder.append(generaStanziamento);
    builder.append(", storno=");
    builder.append(storno);
    builder.append(", provvigioniAquisite=");
    builder.append(provvigioniAquisite);
    builder.append(", conguaglio=");
    builder.append(conguaglio);
    builder.append(", codeTempAllocation=");
    builder.append(codeTempAllocation);
    builder.append(", supplierDocument=");
    builder.append(supplierDocument);
    builder.append(", sentToNav=");
    builder.append(sentToNav);
    builder.append("]");
    return builder.toString();
  }

}
