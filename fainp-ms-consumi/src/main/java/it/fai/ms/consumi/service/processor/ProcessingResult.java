package it.fai.ms.consumi.service.processor;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.parametri_stanziamenti.StanziamentiParams;
import it.fai.ms.consumi.domain.stanziamento.Stanziamento;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;

public class ProcessingResult<C extends Consumo> {

  private final C            consumo;
  private final List<Stanziamento> stanziamenti = new LinkedList<>();
  private final ValidationOutcome  validationOutcome;

  public ProcessingResult(final C _consumo, final ValidationOutcome _validationOutcome) {
    consumo = Objects.requireNonNull(_consumo, "Parameter consumo is mandatory");
    validationOutcome = _validationOutcome;
  }

  public boolean dettaglioStanziamentoCanBeProcessed() {
    return validationOutcome != null && validationOutcome.isValidationOk() || !validationOutcome.isFatalError();
  }

  @Override
  public boolean equals(final Object _obj) {
    boolean res = false;
    if (getClass().isInstance(_obj)) {
      final ProcessingResult<?> obj = getClass().cast(_obj);
      res = Objects.equals(obj.consumo, consumo) && Objects.equals(obj.validationOutcome, validationOutcome);
    }
    return res;
  }

  public C getConsumo() {
    return consumo;
  }

  public List<Stanziamento> getStanziamenti() {
    return stanziamenti;
  }

  public Optional<StanziamentiParams> getStanziamentiParams() {
    return validationOutcome != null ? validationOutcome.getStanziamentiParamsOptional() : Optional.empty();
  }

  public ValidationOutcome getValidationOutcome() {
    return validationOutcome;
  }

  @Override
  public int hashCode() {
    return Objects.hash(consumo, validationOutcome);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("ProcessingResult [consumo=");
    builder.append(consumo);
    builder.append(", dettaglioStanziamentoCanBeProcessed=");
    builder.append(dettaglioStanziamentoCanBeProcessed());
    builder.append(", stanziamentiParamsIsPresent=");
    builder.append(getStanziamentiParams().isPresent());
    builder.append(", validationOutcome=");
    builder.append(validationOutcome);
    builder.append(", stanziamenti=");
    builder.append(stanziamenti);
    builder.append("]");
    return builder.toString();
  }

}
