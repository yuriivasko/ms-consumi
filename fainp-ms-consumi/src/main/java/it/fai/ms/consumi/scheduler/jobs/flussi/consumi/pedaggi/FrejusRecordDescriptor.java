package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi;

import java.util.Map;

import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptorChecker;
import it.fai.ms.consumi.scheduler.jobs.flussi.consumi.carburanti.TrackyCardCarburantiStandardDescriptor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import it.fai.ms.consumi.repository.flussi.record.model.pedaggi.FrejusRecord;
import it.fai.ms.consumi.repository.flussi.record.model.util.FileDescriptor;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.frejus.FrejusCustomerProvider;

@Component
public class FrejusRecordDescriptor implements RecordDescriptor<FrejusRecord> {

  private static final transient Logger log = LoggerFactory.getLogger(FrejusRecordDescriptor.class);
  private final FrejusCustomerProvider  frejusCustomerProvider;

  public final static String HEADER_CODE_BEGIN = "0";
  public final static String DETAIL_CODE_BEGIN = "1";
  public final static String FOOTER_CODE_BEGIN = "9";

  // Descrittore Header
  public final static FileDescriptor header = new FileDescriptor();
  static {
    int start = 0;
    header.addItem("recordCode", 1, start += 1);
    // (aaaammggoomiss)
    header.addItem("creationDateInFile", 2, start += 8);
    header.addItem("creationTimeInFile", start + 1, start += 6);
    header.addItem("numeroCliente", 16, start += 6);
    header.skipItem("riservato", 22, start += 129);
  }
  // Descrittore Detail
  public final static FileDescriptor detail = new FileDescriptor();
  static {
    int start = 0;
    detail.addItem("recordCode", 1, start += 1);
    // detail.addItem("isoEmettitore", 2, start += 6);
    // detail.addItem("tipoTitolo", 8, start += 1);
    // detail.addItem("numeroGruppo", 9, start += 2);
    // detail.addItem("numeroSocieta", 11, start += 4);
    // detail.addItem("numeroTessera", 15, start += 5);
    // detail.addItem("controlChar", 20, start += 1);
    detail.addItem("numeroTessera", 2, start += 19);
    detail.addItem("dataTransazione", 21, start += 14);
    detail.addItem("segnoImporto", 35, start += 1);
    detail.addItem("importo", 36, start += 6);
    detail.skipItem("importoNoVat", 42, start += 6);
    detail.addItem("segnoImportoPericolose", 48, start += 1);
    detail.skipItem("importoPericolose", 49, start += 6);
    detail.addItem("importoPericoloseNoVat", 55, start += 6);
    detail.addItem("percIva", 61, start += 4);
    detail.addItem("numeroFattura", 65, start += 20);
    detail.addItem("numeroSocietaInGruppo", 85, start += 5);
    detail.addItem("senso", 90, start += 1);
    detail.addItem("modalitaTransito", 91, start += 2);
    detail.addItem("pista", 93, start += 1);
    detail.addItem("categoriaVeicolo", 94, start += 1);
    detail.addItem("classePedaggio", 95, start += 1);
    detail.addItem("classificaECO", 96, start += 2);
    detail.addItem("stazioneTransito", 98, start += 8);
    detail.addItem("numeroBuonoFrancia", 106, start += 15);
    detail.skipItem("transitiConTis", 121, start += 19);
    detail.skipItem("riservato2", 140, start += 11);

    // intestazioni.addItem("filler", 64, 183);
  }
  // Descrittore Footer
  public final static FileDescriptor footer = new FileDescriptor();
  static final Map<Integer, Integer> startEndPosTotalRows;
  static {
    int start = 0;
    footer.addItem("recordCode", 1, start += 1);
    // (aaaammggoomiss)
    footer.addItem("creationDateInFile", 2, start += 8);
    footer.addItem("creationTimeInFile", start + 1, start += 6);
    footer.addItem("numeroCliente", 16, start += 6);
    footer.addItem("segnoImportoPlus", 22, start += 1);
    footer.addItem("numeroTransitiPlus", 23, start += 6);
    footer.addItem("importoTransitiPlus", 29, start += 9);
    footer.addItem("numeroPericolosePlus", 38, start += 6);
    footer.addItem("importoPericolosePlus", 44, start += 9);
    footer.addItem("segnoImportoMinus", 53, start += 1);
    footer.addItem("numeroTransitiMinus", 54, start += 6);
    footer.addItem("importoTransitiMinus", 60, start += 9);
    footer.addItem("numeroPericoloseMinus", 69, start += 6);
    footer.addItem("importoPericoloseMinus", 75, start += 9);
    footer.addItem("riservato", 54, start += 67);

    startEndPosTotalRows = RecordDescriptor.getStartEndForFields(footer,
                                                                "numeroTransitiPlus"/* ,"numeroPericolosePlus" */,
                                                                "numeroTransitiMinus"/* ,"numeroPericoloseMinus" */ );
  }

  public FrejusRecordDescriptor(FrejusCustomerProvider frejusCustomerProvider) {
    this.frejusCustomerProvider = frejusCustomerProvider;
  }

  @Override
  public String extractRecordCode(String _data) {
    return StringUtils.left(_data, 1);
  }

  @Override
  public FrejusRecord decodeRecordCodeAndCallSetFromString(String _data, String _recordCode, String fileName, long rowNumber) {

    final FrejusRecord entity = new FrejusRecord(fileName, rowNumber);
    switch (_recordCode) {
      case HEADER_CODE_BEGIN:
        entity.setFromString(FrejusRecordDescriptor.header.getItems(), _data);
        frejusCustomerProvider.loadCustomerData(entity);
        break;
      case DETAIL_CODE_BEGIN:
        entity.setFromString(FrejusRecordDescriptor.detail.getItems(), _data);
        break;
      case FOOTER_CODE_BEGIN:
        entity.setFromString(FrejusRecordDescriptor.footer.getItems(), _data);
        break;
      default:
        log.warn("RecordCode {} not supported", _recordCode);
        break;
    }
    return entity;
  }

  @Override
  public Map<Integer, Integer> getStartEndPosTotalRows() {
    log.debug("StartEndPosTotalRows: {}", startEndPosTotalRows);
    return startEndPosTotalRows;
  }

  @Override
  public String getHeaderBegin() {
    return FrejusRecordDescriptor.HEADER_CODE_BEGIN;
  }

  @Override
  public String getFooterCodeBegin() {
    return FrejusRecordDescriptor.FOOTER_CODE_BEGIN;
  }

  @Override
  public int getLinesToSubtractToMatchDeclaration() {
    return 2;
  }

  public void checkConfiguration() {
    new RecordDescriptorChecker(FrejusRecordDescriptor.class, FrejusRecord.class).checkConfiguration();
  }
}
