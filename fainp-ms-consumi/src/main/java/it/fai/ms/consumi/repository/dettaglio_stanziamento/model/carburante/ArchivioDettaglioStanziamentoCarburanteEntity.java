package it.fai.ms.consumi.repository.dettaglio_stanziamento.model.carburante;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

import it.fai.ms.consumi.repository.dettaglio_stanziamento.ArchivioDettaglioStanziamentoEntity;
import it.fai.ms.consumi.repository.dettaglio_stanziamento.EmbeddableDettaglioStanziamentoEntityGeneric;

@Entity
@Table(name = "archivio_dettaglio_stanziamento_carburanti")
@DiscriminatorValue(value = "CARBURANTI")
@Immutable
public class ArchivioDettaglioStanziamentoCarburanteEntity extends ArchivioDettaglioStanziamentoEntity {

  @Embedded
  private EmbeddableDettaglioStanziamentoEntityGeneric genericDettaglioStanziamento;

  @Embedded
  private EmbeddableDettaglioStanziamentoCarburanteEntity genericDettaglioStanziamentoCarburante;

  
  public EmbeddableDettaglioStanziamentoCarburanteEntity getGenericDettaglioStanziamentoCarburante() {
    return genericDettaglioStanziamentoCarburante;
  }


  public void setGenericDettaglioStanziamentoCarburante(EmbeddableDettaglioStanziamentoCarburanteEntity genericDettaglioStanziamentoCarburante) {
    this.genericDettaglioStanziamentoCarburante = genericDettaglioStanziamentoCarburante;
  }


  public EmbeddableDettaglioStanziamentoEntityGeneric getGenericAttributes() {
    return genericDettaglioStanziamento;
  }
  
}
