package it.fai.ms.consumi.scheduler.jobs.flussi.consumi.pedaggi.liber.t;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.StanziamentiProducer;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class LiberTProcessor extends ConsumoAbstractProcessor implements ConsumoProcessor<LiberTConsumo> {

  public final transient Logger _log = LoggerFactory.getLogger(getClass());
  private LiberTConsumoDettaglioStanziamentoMapper mapper;
  private StanziamentiProducer stanziamentiProducer;

  @Inject
  public LiberTProcessor(final ConsumoValidatorService _consumoValidatorService,
                         final StanziamentiProducer stanziamentiProducer,
                         final LiberTConsumoDettaglioStanziamentoMapper _mapper,
                         final VehicleService _vehicleService,
                         final CustomerService _customerService) {

    super(_consumoValidatorService, _vehicleService, _customerService);
    this.stanziamentiProducer = stanziamentiProducer;
    this.mapper = _mapper;
  }

  @Override
  public ProcessingResult<LiberTConsumo> validateAndProcess(LiberTConsumo consumo, Bucket bucket) {
    return executeProcess(consumo, bucket);
  }


  private ProcessingResult<LiberTConsumo> executeProcess(final LiberTConsumo _consumo, final Bucket _bucket) {

    _log.info(">");
    _log.info("> processing start {}", _bucket.getFileName());
    _log.info("  --> {}", _consumo);

    var processingResult = validateAndNotify(_consumo);

    _log.debug(" - {} processing validation completed", _consumo);

    final var consumo = processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
                      .ifPresent(stanziamentiParams -> {
                        final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
                        final var stanziamenti = stanziamentiProducer.produce(dettaglioStanziamento, stanziamentiParams);
                        processingResult.getStanziamenti()
                                        .addAll(stanziamenti);
                      });
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }

    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", _consumo.getSource());
    _log.info("<");

    return processingResult;
  }

  private DettaglioStanziamentoPedaggio mapConsumoToDettaglioStanziamento(final LiberTConsumo _consumo) {
    var dettaglioStanziamento =  mapper.mapConsumoToDettaglioStanziamento(      _consumo);
    addVehicle(_consumo, dettaglioStanziamento);
    addCustomer(_consumo, dettaglioStanziamento);
    _log.info("Mapped {}", dettaglioStanziamento);
    return dettaglioStanziamento;
  }



}
