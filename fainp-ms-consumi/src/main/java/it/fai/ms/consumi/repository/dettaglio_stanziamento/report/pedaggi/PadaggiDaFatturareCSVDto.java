package it.fai.ms.consumi.repository.dettaglio_stanziamento.report.pedaggi;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.Currency;
import java.util.Locale;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.ms.common.utils.report.CsvHeaderTranslate;

public class PadaggiDaFatturareCSVDto {

  @CsvHeaderTranslate(messageKey = "report.header.tipoRecord")
  public String              tipoRecord;                // tutti a D0
  @CsvHeaderTranslate(messageKey = "report.header.contatoreRiga")
  public String              contatoreRiga;             // 1,2,..
  @CsvHeaderTranslate(messageKey = "report.header.tipoMoivimento")
  public String              tipoMovimento;
  @CsvHeaderTranslate(messageKey = "report.header.codiceUtentePrincipale")
  public String              codiceUtentePrincipale;
  @CsvHeaderTranslate(messageKey = "report.header.tipoSupportoPrincipale")
  public TipoDispositivoEnum tipoSupportoPrincipale;
  @CsvHeaderTranslate(messageKey = "report.header.tessera_supportoPrincipale")
  public String              tessera_supportoPrincipale;// tutti 777048489
  @CsvHeaderTranslate(messageKey = "report.header.riservato_Libero1")
  public String              riservato_Libero1;
  @CsvHeaderTranslate(messageKey = "report.header.codiceUtenteSecondario")
  public String              codiceUtenteSecondario;
  @CsvHeaderTranslate(messageKey = "report.header.tipoSupportoSecondario")
  public String              tipoSupportoSecondario;
  @CsvHeaderTranslate(messageKey = "report.header.tesseraSupportoSecondario")
  public String              tesseraSupportoSecondario;
  @CsvHeaderTranslate(messageKey = "report.header.riservato_Libero2")
  public String              riservato_Libero2;

  public String codiceAutostrada;
  @CsvHeaderTranslate(messageKey = "report.header.descrizioneAutostrada")
  public String descrizioneAutostrada;
  @CsvHeaderTranslate(messageKey = "report.header.codiceCaselloIngresso")
  public String codiceCaselloIngresso;                 // DIREZ. USC. |DIREZ. ENTR.|MARENE|CASTEL.STURA|PARCHEGGIO AR
  @CsvHeaderTranslate(messageKey = "report.header.descrizioneCaselloIngresso")
  public String descrizioneCaselloIngresso;
  @CsvHeaderTranslate(messageKey = "report.header.codiceCaselloUscita")
  public String codiceCaselloUscita;
  @CsvHeaderTranslate(messageKey = "report.header.descrizioneCaselloUscita")
  public String descrizioneCaselloUscita;              // GOVONE|CHERASCO|...
  @CsvHeaderTranslate(messageKey = "report.header.importoEsenteIva")
  public String importoEsenteIva;                      // Importo esente I.V.A. (imponibile) <vuoto>
  @CsvHeaderTranslate(messageKey = "report.header.valuta1")
  public String valuta1;
  @CsvHeaderTranslate(messageKey = "report.header.importoLordoSoggettoIva")
  public String importoLordoSoggettoIva;               // Importo lordo soggetto I.V.A. (imponibile + imposta) 2,5|0,7
  @CsvHeaderTranslate(messageKey = "report.header.valuta2")
  public String valuta2;                               // EUR
  @CsvHeaderTranslate(messageKey = "report.header.scontoMaggiorazioneEsenteIva")
  public String scontoMaggiorazioneEsenteIva;          // Sconto/Maggiorazione esente I.V.A. (imponibile) <vuoto>
  @CsvHeaderTranslate(messageKey = "report.header.valuta3")
  public String valuta3;
  @CsvHeaderTranslate(messageKey = "report.header.scontoMaggiorazioneLordoSoggettoIva")
  public String scontoMaggiorazioneLordoSoggettoIva;   // Sconto/Maggiorazione lordo soggetto I.V.A. (imponibile +
                                                       // imposta)
  @CsvHeaderTranslate(messageKey = "report.header.valuta4")
  public String valuta4;
  @CsvHeaderTranslate(messageKey = "report.header.aliquotaIva")
  public String aliquotaIva;
  @CsvHeaderTranslate(messageKey = "report.header.cambio")
  public String cambio;
  @CsvHeaderTranslate(messageKey = "report.header.valuta5")
  public String valuta5;
  @CsvHeaderTranslate(messageKey = "report.header.classeVicolo")
  public String classeVicolo;
  @CsvHeaderTranslate(messageKey = "report.header.codiceNazioneTargaPrincipale")
  public String codiceNazioneTargaPrincipale;          // Codice nazione targa principale I
  @CsvHeaderTranslate(messageKey = "report.header.targaPrincipale")
  public String targaPrincipale;                       // EW941SF|<vuoto>
  @CsvHeaderTranslate(messageKey = "report.header.classificazioneEuroPrincipale")
  public String classificazioneEuroPrincipale;         // 6|<vuoto>
  @CsvHeaderTranslate(messageKey = "report.header.codiceNazioneTargaSecondaria")
  public String codiceNazioneTargaSecondaria;
  @CsvHeaderTranslate(messageKey = "report.header.targaSecondaria")
  public String targaSecondaria;
  @CsvHeaderTranslate(messageKey = "report.header.classificazioneEuroSecondaria")
  public String classificazioneEuroSecondaria;         // <vuoto>
  @CsvHeaderTranslate(messageKey = "report.header.codiceAutorizzazione_NumeroTransazione")
  public String codiceAutorizzazione_NumeroTransazione;
  @CsvHeaderTranslate(messageKey = "report.header.riservato_Libero3")
  public String riservato_Libero3;

  @CsvHeaderTranslate(
                       messageKey = "report.header.dataOraIngresso",
                       splitDateTime = true,
                       timeMessageKey = "report.header.oraIngresso",
                       formatDate = "dd-MM-yyyy")
  public Instant instantIngresso;
  @CsvHeaderTranslate(
                       messageKey = "report.header.dataUscita",
                       splitDateTime = true,
                       timeMessageKey = "report.header.codiceAutostrada",
                       formatDate = "dd-MM-yyyy")
  public Instant instantUscita;


  public PadaggiDaFatturareCSVDto(ViewReportConsumiPedaggiEntity consumo) {
    this.tipoRecord                             = consumo.getRecordCode();
    this.instantIngresso                        = consumo.getEntryTimestamp();
    this.instantUscita                          = consumo.getExitTimestamp();
    this.contatoreRiga                          = "" + consumo.getSourceRowNumber();
    this.codiceUtentePrincipale                 = consumo.getContractCode();
    this.descrizioneAutostrada                  = consumo.getTratta();
    this.codiceCaselloIngresso                  = consumo.getEntryGlobalGateIdentifier();
    this.descrizioneCaselloIngresso             = consumo.getEntryGlobalGateIdentifierDescription();
    this.codiceCaselloUscita                    = consumo.getExitGlobalGateIdentifier();
    this.descrizioneCaselloUscita               = consumo.getExitGlobalGateIdentifierDescription();
    this.importoEsenteIva                       = amountToString(consumo.getAmountExcludedVat());
    this.valuta1                                = consumo.getCurrencyCode();
    this.importoLordoSoggettoIva                = amountToString(consumo.getAmountIncludedVat());
    this.valuta2                                = consumo.getCurrencyCode();
    this.valuta3                                = consumo.getCurrencyCode();
    this.valuta4                                = consumo.getCurrencyCode();
    this.aliquotaIva                            = amountToString(consumo.getAmountVatRate());
    this.cambio                                 = amountToString(consumo.getExchangeRate());
    this.valuta5                                = consumo.getCurrencyCode();
    this.codiceNazioneTargaPrincipale           = consumo.getVeicoloNazioneTarga();
    this.targaPrincipale                        = consumo.getVeicoloTarga();
    this.classificazioneEuroPrincipale          = consumo.getVeicoloClasse();
    this.tipoSupportoPrincipale                 = consumo.getDeviceType();
    this.tipoMovimento                          = consumo.getTipoMovimento();
    this.tessera_supportoPrincipale             = consumo.getDeviceObu();
    this.riservato_Libero1                      = "";
    this.codiceUtenteSecondario                 = "";
    this.tipoSupportoSecondario                 = "";
    this.tesseraSupportoSecondario              = "";
    this.riservato_Libero2                      = "";
    this.codiceAutostrada                       = "";
    this.scontoMaggiorazioneEsenteIva           = "";
    this.scontoMaggiorazioneLordoSoggettoIva    = "";
    this.classeVicolo                           = "";
    this.codiceNazioneTargaSecondaria           = "";
    this.targaSecondaria                        = "";
    this.classificazioneEuroSecondaria          = "";
    this.codiceAutorizzazione_NumeroTransazione = "";
    this.riservato_Libero3                      = "";
  }

  private String amountToString(BigDecimal amount) {
    if ( amount == null ) {
      return null;
    }  
    DecimalFormat df = new DecimalFormat("#,##0.00");  
    df.setCurrency(Currency.getInstance(Locale.ITALY));
    return df.format(amount);
    
  }   
  
}
