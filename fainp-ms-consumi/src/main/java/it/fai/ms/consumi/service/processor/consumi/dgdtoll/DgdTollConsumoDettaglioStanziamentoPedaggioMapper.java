package it.fai.ms.consumi.service.processor.consumi.dgdtoll;

import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import it.fai.ms.consumi.domain.consumi.Consumo;
import it.fai.ms.consumi.domain.consumi.pedaggi.dgdtoll.DgdToll;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.DettaglioStanziamento;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.pedaggio.DettaglioStanziamentoPedaggio;
import it.fai.ms.consumi.service.processor.consumi.ConsumoDettaglioStanziamentoMapper;

@Component
public class DgdTollConsumoDettaglioStanziamentoPedaggioMapper extends ConsumoDettaglioStanziamentoMapper {

  
  
  public DgdTollConsumoDettaglioStanziamentoPedaggioMapper() {
    super();
  }

  @Override
  public DettaglioStanziamento mapConsumoToDettaglioStanziamento(@NotNull final Consumo _consumo) {
    DettaglioStanziamentoPedaggio dettaglioStanziamentoPedaggio = null;
    if (_consumo instanceof DgdToll) {
      var dgdToll = (DgdToll) _consumo;
      dettaglioStanziamentoPedaggio = (DettaglioStanziamentoPedaggio) toDettaglioStanziamentoPedaggio(dgdToll, dgdToll.getGlobalIdentifier());
      dettaglioStanziamentoPedaggio.setRouteName(dgdToll.getRouteName());

      dettaglioStanziamentoPedaggio.getSupplier().setDocument(dgdToll.getAdditionalInfo());
      dettaglioStanziamentoPedaggio.setTollPointEntry(dgdToll.getTollPointEntry());
      dettaglioStanziamentoPedaggio.setTollPointExit(dgdToll.getTollPointExit());
    }
    return dettaglioStanziamentoPedaggio;
  }

}
