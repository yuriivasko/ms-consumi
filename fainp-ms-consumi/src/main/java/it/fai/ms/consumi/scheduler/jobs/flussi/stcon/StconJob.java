package it.fai.ms.consumi.scheduler.jobs.flussi.stcon;

import it.fai.common.notification.service.NotificationService;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.consumi.ServicePartner;
import it.fai.ms.consumi.domain.consumi.stcon.Stcon;
import it.fai.ms.consumi.domain.enumeration.Format;
import it.fai.ms.consumi.jms.notification_v2.ConsumoNotReprocessableNotification;
import it.fai.ms.consumi.repository.flussi.record.model.stcon.StconRecord;
import it.fai.ms.consumi.repository.flussi.record.utils.FileUtils;
import it.fai.ms.consumi.scheduler.jobs.AbstractJob;
import it.fai.ms.consumi.scheduler.jobs.flussi.RecordDescriptor;
import it.fai.ms.consumi.service.CaselliService;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordPersistenceService;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;

@Service(StconJob.QUALIFIER)
@EnableIntegration
@IntegrationComponentScan
public class StconJob extends AbstractJob<StconRecord, Stcon> {

  public final static String QUALIFIER = "stconJob";

  private final CaselliService caselliService;

  public StconJob(final PlatformTransactionManager transactionManager,
                  final ApplicationProperties _appProperties,
                  final NotificationService notificationService,
                  final RecordPersistenceService persistenceService,
                  final RecordDescriptor<StconRecord> recordDescriptor,
                  final RecordConsumoMapper<StconRecord, Stcon> recordConsumoMapper,
                  final CaselliService caselliService) {
    super("STCON", transactionManager, null, notificationService, persistenceService, recordDescriptor,null, recordConsumoMapper, null);
    this.caselliService = caselliService;
  }

  @Override
  public Format getJobQualifier() {
    return Format.STCON;
  }

  private String firstLine = "";

  @Override
  public synchronized String process(final String _filename, final long _startTime, final Instant _ingestionTime, ServicePartner servicePartner) {

    if (!FileUtils.Exist(_filename)) {
      _log.error("File {} does not exist. cannot proceed.", _filename);
      return source;
    }
    _log.info(" processing filename {}", _filename);
    AtomicLong detailIndex = new AtomicLong(0);
    ArrayList<String> errorList = new ArrayList<>();
    try {
      firstLine = "";

      RecordsFileInfo recordsFileInfo = validateFile(_filename, _startTime, _ingestionTime);
      if (recordsFileInfo.fileIsValid()) {
        _log.info("File {} is valid!", _filename);

        try (final Stream<String> stream = Files.lines(Paths.get(_filename), Charset.forName(ISO_8859_1))) {

          stream.filter(line -> !line.trim().isEmpty() && !line.startsWith(recordDescriptor.getFooterCodeBegin())).forEach(line -> {
            try {
              if (line.startsWith(recordDescriptor.getHeaderBegin())) {
                //var record =
                recordDescriptor.decodeRecordCodeAndCallSetFromString(line, recordDescriptor.getHeaderBegin(), _filename, detailIndex.get());
                //            recordsFileInfo.putMetadata("creationDateInFile", record.getCreationDateInFile());
                //            recordsFileInfo.putMetadata("creationTimeInFile", record.getCreationTimeInFile());
                firstLine = line;
                return;
              }
              detailIndex.incrementAndGet();
              final String recordCode = recordDescriptor.extractRecordCode(line);
              StconRecord record = recordDescriptor.decodeRecordCodeAndCallSetFromString(line, recordCode, _filename, detailIndex.incrementAndGet());
              caselliService.save(record, firstLine);
            } catch (Exception e) {
              _log.error("Error processing detail index {} of file {} : {} ", detailIndex.get(), _filename, e.getMessage());
              String errorMessage = String.format("index %s : %s", detailIndex.get(), e.getMessage());
              errorList.add(errorMessage);
              if (_log.isInfoEnabled()) {
                _log.info("Detail error (stacktrace): ", e);
              }
            }
            if (_log.isInfoEnabled() && (detailIndex.get() % 50 == 0)) {
              _log.info("Processed and persisted {} / {} detail lines of file", detailIndex.get(), recordsFileInfo.getDetailRecordsToRead());
            }
          });
        }
        _log.info("Filename : {}, nr detail lines : {}", Paths.get(_filename).getFileName(), recordsFileInfo.getDetailRecordsToRead());
      } else {
        _log.error("File validation failed! Sending notification! recordsFileInfo:=[{}]", recordsFileInfo);
        recordsFileInfo.getValidationMessages().forEach(message ->
//          sendNotification(message.getCode(),_filename, message.getText())
          notificationService.notify(new ConsumoNotReprocessableNotification(
            source,
            _filename,
            message.getCode(),
            message.getText()
          ))
        );
      }
    } catch (final Exception _e) {
      _log.error("Unhandled excepion (won't be rethrown) when processing file : {}", _e);
//      sendNotification("FCJOB-500",_filename, _e.getMessage());
      notificationService.notify(new ConsumoNotReprocessableNotification(
        source,
        _filename,
        "FCJOB-500",
        _e.getMessage()
      ));
    } 
    if (!errorList.isEmpty()) {
      String messageText = String.format("error in %s rows %s", errorList.size(), String.join("\n", errorList));

//      sendNotification("FCJOB-199",_filename, messageText);
      notificationService.notify(new ConsumoNotReprocessableNotification(
        source,
        _filename,
        "FCJOB-199",
        messageText
      ));
    }

    return source;
  }
}
