package it.fai.ms.consumi.repository.flussi.record.model;

import java.math.BigDecimal;

import javax.money.MonetaryAmount;

public interface MonetaryRecord {

  public MonetaryAmount getAmount_vat();

  public MonetaryAmount getAmount_novat();

  public MonetaryAmount getAmount_novat_base_discount();

  public MonetaryAmount getAmount_novat_excluded_discount();

  public BigDecimal getVatRateBigDecimal();

}
