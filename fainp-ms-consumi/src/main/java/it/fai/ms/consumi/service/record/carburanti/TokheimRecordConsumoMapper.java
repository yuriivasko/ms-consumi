package it.fai.ms.consumi.service.record.carburanti;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import it.fai.common.enumeration.TipoDispositivoEnum;
import it.fai.common.enumeration.TipoServizioEnum;
import it.fai.ms.consumi.domain.Amount;
import it.fai.ms.consumi.domain.AmountBuilder;
import it.fai.ms.consumi.domain.Device;
import it.fai.ms.consumi.domain.RecordsFileInfo;
import it.fai.ms.consumi.domain.Source;
import it.fai.ms.consumi.domain.Transaction;
import it.fai.ms.consumi.domain.consumi.FuelStation;
import it.fai.ms.consumi.domain.consumi.carburante.TokheimConsumo;
import it.fai.ms.consumi.domain.consumi.carburante.trackycard.TrackyCardGlobalIdentifier;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0201;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0301;
import it.fai.ms.consumi.repository.flussi.record.model.carburanti.TokheimRecord0302;
import it.fai.ms.consumi.repository.flussi.record.utils.MonetaryUtils;
import it.fai.ms.consumi.service.record.AbstractRecordConsumoMapper;
import it.fai.ms.consumi.service.record.RecordConsumoMapper;

@Service
public class TokheimRecordConsumoMapper extends AbstractRecordConsumoMapper implements RecordConsumoMapper<TokheimRecord, TokheimConsumo> {

  private static final String TRACKYCARD_TOKHEIM_SOURCE = "trackycard_tokheim";

  @Override
  public TokheimConsumo mapRecordToConsumo(TokheimRecord record) throws Exception {
    TokheimRecord0302 r0302 = (TokheimRecord0302) record;
    TokheimRecord0301 r0301 = r0302.get0301();
    TokheimRecord0201 r0201 = r0302.get0201();

    Optional<Instant> optionalCreationDateInFile = calculateInstantCreationInFile(record, yyyyMMdd_dateformat, HHmmss_timeformat);
    Instant           ingestionTime              = record.getIngestion_time();
    Instant           acquisitionDate            = optionalCreationDateInFile.orElse(record.getIngestion_time());

    Source source = new Source(ingestionTime, acquisitionDate, TRACKYCARD_TOKHEIM_SOURCE);
    source.setFileName(record.getFileName());
    source.setRowNumber(record.getRowNumber());

    TokheimConsumo consumo = new TokheimConsumo(source, record.getRecordCode(),
                                                Optional.of(new TrackyCardGlobalIdentifier(record.getGlobalIdentifier())), record);

    consumo.setTransaction(init(new Transaction(), t -> {
      t.setDetailCode(r0302.getCodiceArticoloFornitore());
      t.setSign(r0302.getSegnoUnitario());
    }));

    consumo.setFuelStation(init(new FuelStation(r0201.getCodiceEsternoPoint()), fs -> {
      fs.setDescription(r0201.getNomePoint());
      fs.setFuelStationCode(r0302.get0201()
                                 .getCodiceEsternoPoint());
      fs.setPumpNumber(r0302.getNrErogatore());
    }));

    consumo.setDevice(init(new Device(TipoDispositivoEnum.TRACKYCARD), d -> {
      d.setSeriale(r0301.getNrTrackycard());
      d.setServiceType(TipoServizioEnum.CARBURANTI);
    }));

    calculateInstantByDateAndTime(r0301.getDataErogazione(), r0301.getOraErogazione(), yyyyMMdd_dateformat,
                                  HHmmss_timeformat).ifPresent(consumo::setEntryDateTime);

    consumo.setKilometers(BigDecimal.ZERO);
    consumo.setQuantity(parseBigDecimal(r0302.getSegno(), r0302.getQuantita(), 100));

    String     currency = r0301.getValuta();
    BigDecimal vatRate  = parseBigDecimal(r0302.getPercentualeIva(), 100);
    consumo.setPriceReference(parseAmountIncludedVat(r0302.getSegnoUnitario(), r0302.getPrezzoUnitario(), -3, currency, vatRate));
    return consumo;
  }

  private BigDecimal parseBigDecimal(String segno, String value, int scale) {

    BigDecimal result = parseBigDecimal(value, scale);
    if ("-".equals(segno))
      result = result.negate();
    return result;
  }

  private BigDecimal parseBigDecimal(String value, int scale) {
    if (StringUtils.isEmpty(value))
      return null;
    return new BigDecimal(value).divide(new BigDecimal(scale));
  }

  private Amount parseAmountIncludedVat(String segno, String value, int scale, String currency, BigDecimal vatRate) {
    if ("-".equals(segno))
      value = segno + value;
    var amountWithVat = MonetaryUtils.strToMonetary(value, scale, currency);


    return AmountBuilder.builder()
                        .amountIncludedVat(amountWithVat)
                        .vatRate(vatRate)
                        .build();

  }



  public static <T> T init(T obj, Consumer<T> consumer) {
    consumer.accept(obj);
    return obj;
  }

}
