package it.fai.ms.consumi.service.canoni;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.fai.ms.common.jms.fattura.canoni.EsitoCanoni;
import it.fai.ms.consumi.adapter.nav.auth.NavAuthorizationTokenService;
import it.fai.ms.consumi.client.CanoniApiClient;

public class CanoniClientServiceImpl implements CanoniClientService {

  private Logger        _log = LoggerFactory.getLogger(getClass());

  private final NavAuthorizationTokenService navAuthorizationTokenService;
  private final CanoniApiClient client;

  public CanoniClientServiceImpl(NavAuthorizationTokenService navAuthorizationTokenService, CanoniApiClient client) {
    this.navAuthorizationTokenService = navAuthorizationTokenService;
    this.client = client;
  }


  final static String REQUEST_HEADER = "Authorization";
  /* (non-Javadoc)
   * @see it.fai.ms.consumi.service.canoni.CanoniClientService#postCanoniDispositivi(it.fai.ms.common.jms.fattura.canoni.EsitoCanoni)
   */
  @Override
  public void postCanoniDispositivi(EsitoCanoni esito) {
    _log.info("post canoni Dispositivi {}",esito);
    client.postCanoniDispositivi(navAuthorizationTokenService.getAuthorizationToken(), esito);
  }

  /* (non-Javadoc)
   * @see it.fai.ms.consumi.service.canoni.CanoniClientService#postCanoniServizi(it.fai.ms.common.jms.fattura.canoni.EsitoCanoni)
   */
  @Override
  public void postCanoniServizi(EsitoCanoni esito) {
    _log.info("post canoni Servizi {}",esito);
    client.postCanoniServizi(navAuthorizationTokenService.getAuthorizationToken(), esito);
  }
}
