package it.fai.ms.consumi.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.fai.ms.consumi.repository.TollChargerEntityRepository;
import it.fai.ms.consumi.repository.tollcharger.model.TollChargerEntity;
import it.fai.ms.consumi.service.TollChargerService;

@Service
@Transactional
public class TollChargerServiceImpl implements TollChargerService {

  private final Logger _log = LoggerFactory.getLogger(getClass());

  private final TollChargerEntityRepository tollChargerEntityRepository;

  public TollChargerServiceImpl(TollChargerEntityRepository _tollChargerEntityRepository) {
    tollChargerEntityRepository = _tollChargerEntityRepository;
  }

  public Optional<TollChargerEntity> findById(Long id) {
    _log.debug("Request toll charger by id : {}", id);
    return tollChargerEntityRepository.findById(id);
  }

  @Cacheable(value = "networkDescription", key = "{#root.methodName, #root.args[0]}")
  public Optional<TollChargerEntity> findOneByCode(String code) {
    _log.debug("Request toll charger by code : {}", code);
    return tollChargerEntityRepository.findOneByCode(code);
  }

  public TollChargerEntity lastInsertId() {
    _log.debug("Request last insert toll charger");
    return tollChargerEntityRepository.findFirstByOrderByIdDesc();
  }

}
