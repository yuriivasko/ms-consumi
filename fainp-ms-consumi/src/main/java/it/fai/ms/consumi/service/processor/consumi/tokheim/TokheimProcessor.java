package it.fai.ms.consumi.service.processor.consumi.tokheim;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import it.fai.ms.common.jms.dto.petrolpump.PointDTO;
import it.fai.ms.consumi.client.PetrolPumpClient;
import it.fai.ms.consumi.config.ApplicationProperties;
import it.fai.ms.consumi.domain.ServiceProvider;
import it.fai.ms.consumi.domain.consumi.Bucket;
import it.fai.ms.consumi.domain.consumi.carburante.TokheimConsumo;
import it.fai.ms.consumi.domain.dettaglio_stanziamento.carburante.DettaglioStanziamentoCarburante;
import it.fai.ms.consumi.domain.validation.Message;
import it.fai.ms.consumi.domain.validation.ValidationOutcome;
import it.fai.ms.consumi.repository.ServiceProviderRepository;
import it.fai.ms.consumi.service.CustomerService;
import it.fai.ms.consumi.service.VehicleService;
import it.fai.ms.consumi.service.processor.ConsumoAbstractProcessor;
import it.fai.ms.consumi.service.processor.ProcessingResult;
import it.fai.ms.consumi.service.processor.consumi.ConsumoProcessor;
import it.fai.ms.consumi.service.processor.dettagli_stanziamenti.carburanti.DettaglioStanziamentoCarburantiProcessor;
import it.fai.ms.consumi.service.validator.ConsumoValidatorService;

@Service
public class TokheimProcessor  extends ConsumoAbstractProcessor implements ConsumoProcessor<TokheimConsumo>{

  private final transient Logger _log = LoggerFactory.getLogger(getClass());

  private final DettaglioStanziamentoCarburantiProcessor     dettaglioStanziamentoProcessor;
  private final TokehimConsumoDettaglioStanziamentoMapper mapper;

  private PetrolPumpClient petrolPumpClient;

  private ApplicationProperties applicationProperties;

  private ServiceProviderRepository serviceProviderRepository;


  @Inject
  public TokheimProcessor(final ConsumoValidatorService _consumoValidatorService,
                                        final DettaglioStanziamentoCarburantiProcessor _dettaglioStanziamentoProcessor,
                                        final TokehimConsumoDettaglioStanziamentoMapper _mapper,
                                        final VehicleService _vehicleService,
                                        final CustomerService _customerService,
                                        final PetrolPumpClient petrolPumpClient,
                                        final ApplicationProperties applicationProperties,
                                        final ServiceProviderRepository serviceProviderRepository
      ) {

    super(_consumoValidatorService,  _vehicleService, _customerService);
    dettaglioStanziamentoProcessor = _dettaglioStanziamentoProcessor;
    mapper = _mapper;
    this.petrolPumpClient = petrolPumpClient;
    this.applicationProperties = applicationProperties;
    this.serviceProviderRepository = serviceProviderRepository;
  }

  @Override
  public ProcessingResult<TokheimConsumo> validateAndProcess(final TokheimConsumo _trackyCardCarburantiStandard, final Bucket _bucket) {
    return executeProcess(_trackyCardCarburantiStandard, _bucket);
  }

  private ProcessingResult<TokheimConsumo> executeProcess(final TokheimConsumo _consumo, final Bucket _bucket) {

    _log.info(">");
    _log.info("> processing start {}", _bucket.getFileName());
    _log.info("  --> {}", _consumo);

    var validationOutcome = setServiceProvider(_consumo);
    if(validationOutcome!=null && validationOutcome.isFatalError()){
      ProcessingResult<TokheimConsumo> tokheimConsumoProcessingResult = new ProcessingResult<>(_consumo, validationOutcome);
      consumoValidatorService.notifyProcessingResult(tokheimConsumoProcessingResult);
      return tokheimConsumoProcessingResult;
    }

    var processingResult = validateAndNotify(_consumo);

    _log.debug(" - {} processing validation completed", _consumo);

    final var consumo =(TokheimConsumo) processingResult.getConsumo();

    if (processingResult.dettaglioStanziamentoCanBeProcessed()) {
      processingResult.getStanziamentiParams()
                      .ifPresent(stanziamentiParams -> {
                        final var dettaglioStanziamento = mapConsumoToDettaglioStanziamento(consumo);
                        final var stanziamenti = dettaglioStanziamentoProcessor.produce(dettaglioStanziamento, stanziamentiParams);
                        processingResult.getStanziamenti()
                                        .addAll(stanziamenti);
                      });
    } else {
      _log.warn(" - {} can't be processed", consumo);
    }

    _log.info("  --> {}", processingResult);
    _log.info("< processing completed {}", _consumo.getSource());
    _log.info("<");

    return processingResult;
  }

  private ValidationOutcome setServiceProvider(TokheimConsumo _consumo) {
    ValidationOutcome _validationOutcome = null;

    String externalCode = getExternalCode(_consumo);
    try {
      PointDTO pointFromPetrolPump = petrolPumpClient.searchPoint(applicationProperties.getAuthorizationHeader(), externalCode);
//      _consumo.getServicePartner().setPartnerCode(pointFromPetrolPump.getProviderCode());

      _consumo.getFuelStation().setFuelStationCode(pointFromPetrolPump.getCode());
      _consumo.getFuelStation().setDescription(pointFromPetrolPump.getDescription());

      ServiceProvider serviceProvider = serviceProviderRepository.findFirstByProviderCode(pointFromPetrolPump.getProviderCode());
      _consumo.setServicePartner(serviceProvider.toServicePartner());

    }catch(Exception e){
      _validationOutcome = new ValidationOutcome();
      _validationOutcome.setFatalError(true);
      _log.warn("Cannot find point for externalCode: {} on consumo {}", externalCode,_consumo);
      _validationOutcome.addMessage(new Message("b008").withText(String.format("Cannot find point for externalCode: %s ",externalCode)));
    }
    return _validationOutcome;
  }

  private String getExternalCode(TokheimConsumo _consumo) {
    String externalCode = _consumo.getFuelStation().getFuelStationCode();
    //if(externalCode.startsWith("0"))externalCode=externalCode.substring(1);
    return externalCode;
  }

  private DettaglioStanziamentoCarburante mapConsumoToDettaglioStanziamento(final TokheimConsumo _consumo) {
    var dettaglioStanziamento = (DettaglioStanziamentoCarburante) mapper.mapConsumoToDettaglioStanziamento(      _consumo);
    addVehicle(_consumo, dettaglioStanziamento);
    addCustomer(_consumo, dettaglioStanziamento);
    _log.info("Mapped {}", dettaglioStanziamento);
    return dettaglioStanziamento;
  }
}
